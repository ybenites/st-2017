var $ = require('jquery');

var ScrollMagic = require('scrollmagic');
require("scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js");
// require("scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js");


var controller = new ScrollMagic.Controller();

$(function() { // wait for document ready
    // build scene
    var ev, eb;
    ev = create_scene_pinvideo("#content-live-video");
    eb = create_scene_pinblog("#content-fix-blogging");



    // build tween
    var tween_y = TweenMax.fromTo("#content-fix-blogging", 1, { y: 0 }, { y: -$(".content-fix-blogging").height() / 2, ease: Linear.easeNone });
    var scene_y = new ScrollMagic.Scene({
            triggerElement: ".st-content-live-blog",
            duration: $(window).height(),
            offset: $(".content-fix-blogging").height() / 2,
            triggerHook: "onLeave"
        })
        .update(true)
        .setTween(tween_y)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);


    $(window).on('resize', function() {
        if (!is_mobile()) {
            ev.destroy(true);
            eb.destroy(true);
            ev = create_scene_pinvideo("#content-live-video");
            eb = create_scene_pinblog("#content-fix-blogging");
            desactivate_controller(controller);

            var tween_y = TweenMax.fromTo("#content-fix-blogging", 1, { y: 0 }, { y: -$(".content-fix-blogging").height() / 2, ease: Linear.easeNone });
            scene_y.offset($(".content-fix-blogging").height() / 2).duration($(window).height()).setTween(tween_y);

        }
    });



    $(window).on("orientationchange", function() {
        if (is_mobile()) {
            ev.destroy(true);
            eb.destroy(true);
            ev = create_scene_pinvideo("#content-live-video");
            eb = create_scene_pinblog("#content-fix-blogging");
            desactivate_controller(controller);

            var tween_y = TweenMax.fromTo("#content-fix-blogging", 1, { y: 0 }, { y: -$(".content-fix-blogging").height() / 2, ease: Linear.easeNone });
            scene_y.offset($(".content-fix-blogging").height() / 2).duration($(window).height()).setTween(tween_y);
        }
    });
    $(window).on('load', function() {
        if (ev !== undefined) {
            ev.destroy(true);
            eb.destroy(true);
            ev = create_scene_pinvideo("#content-live-video");
            eb = create_scene_pinblog("#content-fix-blogging");
            desactivate_controller(controller);

            var tween_y = TweenMax.fromTo("#content-fix-blogging", 1, { y: 0 }, { y: -$(".content-fix-blogging").height() / 2, ease: Linear.easeNone });
            scene_y.offset($(".content-fix-blogging").height() / 2).duration($(window).height()).setTween(tween_y);
        }
    });
});



function desactivate_controller(controller) {
    if ($(window).width() < 768 && controller.enabled()) {
        controller.enabled(false);
    } else if ($(window).width() >= 768 && !controller.enabled()) {
        controller.enabled(true);
    }
    controller.update(true);
}

function is_mobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
}

function create_scene_pinvideo(id, v_class) {
    var pin_option = {};
    pin_option.pushFollowers = false;
    if (v_class !== undefined) pin_option.spacerClass = v_class;

    var scene = new ScrollMagic.Scene({
            triggerElement: ".st-content-live-blog",
            duration: get_duration_live_video,
            triggerHook: "onLeave"
        })
        .update(true)
        .setPin(id, pin_option)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);
    return scene;
}

function create_scene_pinblog(id, v_class) {
    var pin_option = {};
    pin_option.pushFollowers = false;
    if (v_class !== undefined) pin_option.spacerClass = v_class;

    var scene = new ScrollMagic.Scene({
            triggerElement: ".st-content-live-blog",
            duration: get_duration_live_blog,
            triggerHook: "onLeave"
        })
        .update(true)
        .setPin(id, pin_option)
        // .addIndicators() // add indicators (requires plugin)
        .addTo(controller);

    return scene;
}

function get_duration_live_video() {
    return $(".st-content-live-blog").height() - $(window).height() + ($(window).height() - $("#content-live-video").height());
}

function get_duration_live_blog() {
    return $(".st-content-live-blog").height() - $(window).height() + ($(window).height() - $(".content-fix-blogging").height());
}
