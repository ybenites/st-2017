(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["historicalpolls"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="justify-content-center d-flex row">\n        <div class="col-md-10">\n            <h4 class="text-uppercase mb-0 mt-1 font-weight-bold party-text text-sm-center ">' + ((__t = 'Historical polls') == null ? '' : __t) + '</h4>\n            <p class="bottomline-text  text-sm-center">' + ((__t = 'Voting intentions for federal elections') == null ? '' : __t) + '</p>\n            <br class="hidden-md-up">\n            <div id="reutersGraphic-historypoll" class="reuters-chart"></div>        \n        </div>\n</div>';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["historyresults"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="justify-content-center d-flex row">\n        <div class="col-md-10">\n            <p class="mb-0 mt-1 graphic-chart-label text-sm-center  ">' + ((__t = 'Previous elections') == null ? '' : __t) + '</p>\n            <p class="bottomline-text  text-sm-center">' + ((__t = "Leading party in each voting constituency") == null ? '' : __t) + '</p>\n            <br class="hidden-md-up">    \n        </div>\n</div>\n\n<!-- legend -->\n<div class="row">\n	<div class="legend col-sm-12 text-sm-center mb-1">\n		<div class="legend-box cdu"></div>\n			<span class="text-uppercase mb-0">' + ((__t = 'CDU') == null ? '' : __t) + '</span>\n		<div class="legend-box csu"></div>\n			<span class="text-uppercase mb-0">' + ((__t = 'CSU') == null ? '' : __t) + '</span>\n		<div class="legend-box spd"></div> \n			<span class="text-uppercase mb-0">' + ((__t = 'SPD') == null ? '' : __t) + '</span>\n		<div class="legend-box dielinke"></div> \n			<span class="text-uppercase mb-0">' + ((__t = 'Left') == null ? '' : __t) + '</span>\n		<div class="legend-box green"></div>\n			<span class="text-uppercase mb-0">' + ((__t = 'Greens') == null ? '' : __t) + '</span>\n		<div class="legend-box pds"></div>\n			<span class="text-uppercase mb-0">' + ((__t = 'PDS') == null ? '' : __t) + '</span>\n	</div>\n\n</div>\n\n<!-- maps -->\n<div class="row justify-content-center">\n		<div class="col-10 col-sm-4 col-md-3">\n			<p class="text-uppercase mb-0 font-weight-bold text-center">' + ((__t = '2005') == null ? '' : __t) + '</p>\n			<img class="" src="images/maps/maps_2005.png" width="100%">\n		</div>\n		<div class="col-10 col-sm-4 col-md-3">\n			<p class="text-uppercase mb-0 font-weight-bold text-center">' + ((__t = '2009') == null ? '' : __t) + '</p>\n			<img class="" src="images/maps/maps_2009.png" width="100%">\n		</div>\n		<div class="col-10 col-sm-4 col-md-3">\n			<p class="text-uppercase mb-0 text-center font-weight-bold">' + ((__t = '2013') == null ? '' : __t) + '</p>\n			<img class="" src="images/maps/maps_2013.png" width="100%">\n		</div>\n</div>\n\n<!-- donut -->\n<div class="justify-content-center d-flex row mt-2">\n        <div class="col-md-8 col-lg-6">\n            <p class="bottomline-text  mt-2 text-sm-center">' + ((__t = "Number of seats in the Bundestag after each federal election") == null ? '' : __t) + '</p>\n            <br class="hidden-md-up">\n            <div id="reutersGraphic-historyresults" class="reuters-chart"></div>        \n        </div>\n</div>';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["issueContent"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    t.election.issues.forEach(function(issue, i) {;
      __p += '\n\n    <div class="justify-content-center d-flex row">\n        <div class="">\n            <img src="images/icons/' + ((__t = issue) == null ? '' : __t) + '.png" class="candidate-image mt-2 mx-auto d-block" width="100%">\n        </div>\n    </div>\n\n    <div class="row">\n              <div class="col-sm-6 offset-sm-3">\n                    <p class="text-center graphic-chart-label mb-1 mt-1">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</p>\n              </div>\n\n    </div>\n\n  ';
      t.election.parties.forEach(function(d) {;
        __p += '\n      <div class="row">\n\n                <div class="col-lg-1">\n                </div>\n\n                <div class="col-md-4 col-lg-2 text-sm-center text-md-left issue-content-header">\n                    <p class="t font-weight-bold">' + ((__t = d.party) == null ? '' : __t) + '</p>\n                </div>\n\n\n                <div class="col-md-8  text-xs-left">\n                    <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>\n                </div>\n\n                <div class="col-lg-1">\n                </div>\n\n      </div>\n\n   ';
      });
      __p += '\n\n        ';
      if (i != t.election.issues.length - 1) {;
        __p += '\n            <hr class="mb-1 mt-1">\n         ';
      };
      __p += '\n\n';
    });
    __p += '\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["parties"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="party-buttons navContainer text-sm-center">\n    <div class="btn-group nav-options horizontal" data-toggle="buttons" id="party-buttons">\n            <label dataid="party-sort" data-id="key-parties" class="btn btn-primary active smaller">\n                <input type="radio" name="nav-options"  autocomplete="off">\n                ' + ((__t = 'KEY PARTIES') == null ? '' : __t) + '\n            </label>\n            <label dataid="issue-sort" data-id="key-issues" class="btn btn-primary smaller">\n                <input type="radio" name="nav-options" autocomplete="off">\n                ' + ((__t = 'ISSUES') == null ? '' : __t) + '\n            </label>\n    </div>\n</div>\n<div id="party-sort" class="party-table selected"></div>\n<div id="issue-sort" class="party-table mt-2"></div>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["partyContent"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    t.election.parties.forEach(function(d, i) {;
      __p += '\n    <div class="justify-content-center d-flex row">\n        <div class="">\n            <img src="images/candidates/' + ((__t = d.imageid) == null ? '' : __t) + '.png" class="candidate-image mt-2 mx-auto d-block" width="100%">\n        </div>\n    </div>\n\n    <div class="justify-content-center d-flex row">\n        <div class="col-md-12">\n            <h4 class="mt-1 text-sm-center mb-0 font-weight-normal">' + ((__t = d.leader) == null ? '' : __t) + '</h4> \n            <p class="text-sm-center bottomline-text">' + ((__t = d.leaderstatus) == null ? '' : __t) + '</p>\n\n            <h5 class="text-uppercase mb-0 font-weight-bold party-text text-sm-center">' + ((__t = d.party) == null ? '' : __t) + '</h5>  <p class="text-sm-center bottomline-text">Won ' + ((__t = d.lastresults) == null ? '' : __t) + '% in 2013 federal election</p>     \n        </div>\n    </div>\n    <div class="justify-content-center d-flex row">\n        <div class="col-md-6">       \n            <p class="bottomline-text mb-2  text-sm-center">' + ((__t = d.bottomline) == null ? '' : __t) + '</p>\n       </div>\n    </div>\n    \n    <div class="justify-content-center d-flex row">\n        ';
      t.election.issues.forEach(function(issue) {;
        __p += '\n            <div class="col-md-3">\n                <p class="text-uppercase mb-0 font-weight-bold">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</p>\n                <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>\n            </div>  \n        ';
      });
      __p += '        \n    </div>\n\n        ';
      if (i != t.election.parties.length - 1) {;
        __p += '\n            <hr>\n         ';
      };
      __p += ' \n\n';
    });
    __p += '\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["piesetup"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '<div class="d-block">\n';
    if (t.self.hasLegend) {;
      __p += '\n    <div class="pie-legend d-flex flex-wrap justify-content-center">\n        ';
      t.self.colorDomain.forEach(function(d, i) {;
        __p += '\n            <div class="legend-item d-flex flex-row mr-1">\n                <div class="pie-legend-box" style="background-color:' + ((__t = t.self.colorRange[i]) == null ? '' : __t) + ';"></div>\n                <div class="pie-legend-text text-uppercase">' + ((__t = d) == null ? '' : __t) + '</div>\n            </div>\n        ';
      });
      __p += '\n    </div>\n';
    };
    __p += '\n<div id="' + ((__t = t.self.targetDiv + '-chart') == null ? '' : __t) + '" class="pie-chart"></div>\n</div>';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["pietooltip"] = function(t) {
    var __t,
      __p = '';
    __p += '<p class="muted text-uppercase">' + ((__t = t.d.year) == null ? '' : __t) + ' ' + ((__t = ' election') == null ? '' : __t) + '</p>\n<p class="tooltip-title">' + ((__t = t.d.partyname) == null ? '' : __t) + '</p>\n<p class="">' + ((__t = t.d.seats) == null ? '' : __t) + '' + ((__t = ' seats') == null ? '' : __t) + '</p>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["pollslatest"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '\n\n	<div class="row mt-1 mb-1">\n		<div class="col-sm-12">\n			<p class="graphic-chart-label mt-2 text-sm-center">' + ((__t = 'Latest Polls') == null ? '' : __t) + '</p>\n			<p class="bottomline-text  text-sm-center">' + ((__t = "Voting intentions from each pollster's most recent survey. Margin of errors shown.") == null ? '' : __t) + '</p>\n		</div>\n	</div>\n	\n	';
    t.pollsters.forEach(function(key, i) {
      if (i < 3) {

        var value = t.latestPollsData[key];
        var samplesizeFormat = d3.format(",.0f");
        var marginFormat = d3.format(",.1f");

        ;
        __p += '\n		<div class="row">\n<!-- 			<div class="col-sm-2 col-md-2 poll-text">\n				<p class="pollster">' + ((__t = key) == null ? '' : __t) + '</p>\n				';
        if (t.self.marginFormat(value[0].marginoferror) == "NaN" || t.self.marginFormat(value[0].marginoferror) == 0) {;
          __p += '\n						<p class="moe ">' + ((__t = 'Survey of ') == null ? '' : __t) + '' + ((__t = t.self.samplesizeFormat(value[0].samplesize)) == null ? '' : __t) + ' ' + ((__t = 'respondents conducted ') == null ? '' : __t) + '' + ((__t = value[0].daterange) == null ? '' : __t) + '</p>\n				';
        } else {;
          __p += '\n					\n					<p class="moe ">' + ((__t = 'Survey of ') == null ? '' : __t) + '' + ((__t = t.self.samplesizeFormat(value[0].samplesize)) == null ? '' : __t) + ' ' + ((__t = 'respondents conducted ') == null ? '' : __t) + '' + ((__t = value[0].daterange) == null ? '' : __t) + '' + ((__t = '; Margin of error: +/- ') == null ? '' : __t) + '' + ((__t = t.self.marginFormat(value[0].marginoferror)) == null ? '' : __t) + '' + ((__t = ' pct. pts.') == null ? '' : __t) + '</p>\n				';
        };
        __p += '\n				\n			</div>\n -->			<div class="col-sm-2 col-md-2 poll-text">\n				<p class="pollster">' + ((__t = key) == null ? '' : __t) + '</p>\n				<p class="moe ">' + ((__t = 'Survey') == null ? '' : __t);
        if (t.self.samplesizeFormat(value[0].samplesize) == "NaN" || t.self.samplesizeFormat(value[0].samplesize) == 0) {} else {;
          __p += ((__t = ' of ') == null ? '' : __t) + '' + ((__t = t.self.samplesizeFormat(value[0].samplesize)) == null ? '' : __t) + ' ' + ((__t = 'respondents') == null ? '' : __t);
        }
        if (value[0].daterange == "NaN" || value[0].daterange == 0) {} else {;
          __p += ((__t = ' conducted ') == null ? '' : __t) + '' + ((__t = value[0].daterange) == null ? '' : __t);
        }
        if (t.self.marginFormat(value[0].marginoferrorhighend) == "NaN" || t.self.marginFormat(value[0].marginoferrorhighend) == 0) {} else {;
          __p += ((__t = '; Margin of error: +/- ') == null ? '' : __t) + '' + ((__t = value[0].marginoferror) == null ? '' : __t) + '' + ((__t = ' pct. pts.') == null ? '' : __t) + '</p> ';
        };
        __p += '\n				\n			</div>\n\n			<div class="col-sm-10 col-md-10">\n<!-- 		        ';
        var id = key.split(' ')[0].split('/')[0];
        if (key == "ICM-Sun on Sunday") {
          id = "ICM-SunSunday";
        };
        __p += ' -->\n\n		        <div id="reutersGraphic-latest-poll-' + ((__t = id) == null ? '' : __t) + '" class=""></div>\n			</div>\n	\n			\n		</div>\n	';
      } else {
        return;
      }
    });
    __p += '\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["results2017"] = function(t) {
    var __t,
      __p = '';
    __p += '\n<div class="justify-content-center results-holder-percent d-flex row">\n        <div class="col-md-10">\n            <p class="mb-0 mt-1 graphic-chart-label text-sm-center ">' + ((__t = '2017 results') == null ? '' : __t) + '</p>\n        </div>\n</div>\n\n\n<div class="justify-content-center results-holder-percent d-flex row">\n        <div class="col-md-6">\n            <p class="bottomline-text text-sm-center mb-0">' + ((__t = "Per cent of vote") == null ? '' : __t) + '</p>\n              <p id="timestamp-first" class="bottomline-text  text-sm-center mt-0"></p>\n            <div id="reutersGraphic-results-percent" class="reuters-chart "></div>\n        </div>\n</div>\n\n\n<div class="justify-content-center d-flex row results-holder-seats">\n        <div class="col-md-6">\n            <p class="bottomline-text text-sm-center mb-0">' + ((__t = "Number of seats") == null ? '' : __t) + '</p>\n            <p id="timestamp-second" class="bottomline-text  text-sm-center mt-0"></p>\n            <div id="reutersGraphic-results-seats" class="reuters-chart"></div>\n        </div>\n</div>\n\n<br>\n<Br>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["sample"] = function(t) {
    var __t,
      __p = '';
    __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["scenarios"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    t.data.forEach(function(d, i) {;
      __p += '\n	<div class="row mt-2 mb-0 ">\n		 <div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">\n       		<p class="graphic-chart-label mt-2 text-sm-center">' + ((__t = d.name) == null ? '' : __t) + '</p>\n		    <p class="bottomline-text text-sm-center">' + ((__t = d.parties) == null ? '' : __t) + '</p>\n		    <p class="bottomline-text  text-sm-center">' + ((__t = d.intro) == null ? '' : __t) + '</p>\n		</div>\n	</div>\n	<div class="row mt-0 ">\n		 <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">\n		    <div id="reutersGraphic-' + ((__t = d.id) == null ? '' : __t) + '" class="reuters-chart"></div>\n		</div>\n	</div>\n\n\n';
    });
    __p += '\n\n	<div class="row mt-0 mb-3">\n		 <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">\n		     <p class="graphic-source">' + ((__t = '*Number of seats based on INSA survey of 2,054 respondents conducted Sept. 9-11; margin of error: +/-2.5 percentage points') == null ? '' : __t) + '</p>\n		</div>\n	</div>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["suiteLayout"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="mb-1 mt-2 container" id="package-container">\n    <div class="row">\n        <div class="col-sm-12 text-sm-center masthead-nav text-uppercase">\n            <div class="btn-group nav-options horizontal" data-toggle="buttons" id="section-buttons">\n\n        <label class="btn btn-link active" data-id="results">\n                    <input type="radio" name="results" autocomplete="off">\n                    ' + ((__t = 'Results') == null ? '' : __t) + '\n                </label>\n    <label class="btn btn-link" data-id="scenarios">\n                    <input type="radio" name="scenarios" autocomplete="off">\n                    ' + ((__t = 'Scenarios') == null ? '' : __t) + '\n                </label>\n    <label class="btn btn-link" data-id="parties">\n                    <input type="radio" name="parties" autocomplete="off" checked>\n                    ' + ((__t = 'Parties') == null ? '' : __t) + '\n                </label>\n                <label class="btn btn-link" data-id="polls">\n                    <input type="radio" name="polls" autocomplete="off">\n                    ' + ((__t = 'Polls') == null ? '' : __t) + '\n                </label>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class="container graphic-section-container">\n\n    <section class="graphic-section selected" id="results">\n         <div class="justify-content-center d-flex row">\n            <div class="col-md-6">\n                    <h2 class="mobile-section-headers text-sm-center hidden-sm-up text-uppercase mt-1">' + ((__t = 'Results') == null ? '' : __t) + '</h2>\n                    <p class="graphic-subhead text-sm-center">' + ((__t = "The conservative Christian Democrats and its Bavarian sister party Christian Social Union recorded their worst showing in almost 70 years.") == null ? '' : __t) + '</p>\n            </div>\n        </div>\n        <div id="results2017" class="results2017"></div>\n        <div id="historyresults" class="historyresults"></div>\n    </section>\n\n    <section class="graphic-section" id="parties">\n         <div class="justify-content-center d-flex  row">\n            <div class="col-md-6">\n                    <h2 class="mobile-section-headers text-sm-center hidden-sm-up text-uppercase mt-1">' + ((__t = 'Parties') == null ? '' : __t) + '</h2>\n                    <p class="graphic-subhead text-sm-center">' + ((__t = "Concerns about integration and security have pushed Merkel's popularity down and fuelled the rise of the anti-immigrant Alternative for Germany (AfD) party. Here are the top candidates and their party programmes:") == null ? '' : __t) + '</p>\n            </div>\n        </div>\n         <div id="party-profiles" class="party-profiles"></div>\n    </section>\n\n    <section class="graphic-section" id="polls">\n         <div class="justify-content-center d-flex  row">\n            <div class="col-md-6">\n                    <h2 class="mobile-section-headers text-sm-center hidden-sm-up text-uppercase mt-1">' + ((__t = 'Polls') == null ? '' : __t) + '</h2>\n                    <p class="graphic-subhead text-sm-center">' + ((__t = "Merkel's conservative Christian Democrats (CDU) and their Bavarian sister party, the Christian Social Union (CSU), hold a comfortable lead over her challenger Martin Schulz and his center-left Social Democrats (SPD).") == null ? '' : __t) + '</p>\n            </div>\n        </div>\n         <div id="historicalpolls" class="historicalpolls"></div>\n         <div id="pollslatest" class="pollslatest"></div>\n    </section>\n\n    <section class="graphic-section" id="scenarios">\n         <div class="justify-content-center d-flex  row">\n            <div class="col-md-6">\n                    <h2 class="mobile-section-headers text-sm-center hidden-sm-up text-uppercase mt-1">' + ((__t = 'Scenarios') == null ? '' : __t) + '</h2>\n                    <p class="graphic-subhead text-sm-center">' + ((__t = "If, as expected, Merkel's conservatives win the most seats in the Bundestag lower house, she will start seeking potential coalition partners immediately.") == null ? '' : __t) + '<br>' + ((__t = "Here are the main scenarios and the number of seats, according to a recent poll*, each coalition could garner:") == null ? '' : __t) + '</p>\n\n            </div>\n        </div>\n         <div id="scenario-charts-id" class="scenario-charts-class"></div>\n    </section>\n\n    <div class="row">\n        <div class="col-12 mt-1">\n            <p class="graphic-source none">' + ((__t = 'Sources: Party programmes and declarations; the pollsters; Federal Returning Officer (constituency results); ParlGov (parliament results); Bild (scenario poll)') == null ? '' : __t) + '<br />' + ((__t = 'By Michael Ovaska, Gustavo Cabrera, Thomas Escritt, Michelle Martin, Paul Carrel, and Jessia Wang | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>\n\n</div>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["tooltipdots"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.data[0].displayDate) {;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
    } else {;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
    };
    __p += '\n';
    var groupData = _.groupBy(t.data, "pollster");
    var keys = _.keys(groupData);

    keys.forEach(function(key, i) {;
      __p += '\n		<div class=\'pollsterTip\'> ' + ((__t = key) == null ? '' : __t) + ' </div>\n		';
      groupData[key].forEach(function(d, i) {;
        __p += '\n			<div class="tipHolder">\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
        print(t.self.colorScale(d.name));
        __p += ';\'></div>\n				<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n				<div class=\'valueTip\'>\n					';
        if (t.self.chartLayout == "stackPercent") {;
          __p += '\n						';
          print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
          __p += '				\n					';
        } else {;
          __p += '\n						';
          print(t.self.tipNumbFormat(d[t.self.dataType]));
          __p += '				\n					';
        };
        __p += '\n				</div>\n		\n			</div>\n\n			\n		';
      });
      __p += '\n';
    });
    __p += '	\n';
    if (t.self.timelineData) {
      var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
      print(t.self.timelineTemplate({
        data: timelineData,
        self: t.self
      }));
    };
    __p += '	';
    return __p;
  };
})();
//for translations.
window.gettext = function(text) {
  return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphics = window.Reuters.Graphics || {};
window.Reuters.Graphics.Model = window.Reuters.Graphics.Model || {};
window.Reuters.Graphics.View = window.Reuters.Graphics.View || {};
window.Reuters.Graphics.Collection = window.Reuters.Graphics.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

Reuters.addCommas = function(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function(hashBased) {
  var query = void 0;
  if (hashBased) {
    var pos = location.href.indexOf('?');
    if (pos == -1) return [];
    query = location.href.substr(pos + 1);
  } else {
    query = location.search.substr(1);
  }
  var result = {};
  query.split('&').forEach(function(part) {
    if (!part) return;
    part = part.split('+').join(' '); // replace every + with space, regexp-free version
    var eq = part.indexOf('=');
    var key = eq > -1 ? part.substr(0, eq) : part;
    var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

    //convert true / false to booleans.
    if (val == 'false') {
      val = false;
    } else if (val == 'true') {
      val = true;
    }

    var f = key.indexOf('[');
    if (f == -1) {
      result[decodeURIComponent(key)] = val;
    } else {
      var to = key.indexOf(']');
      var index = decodeURIComponent(key.substring(f + 1, to));
      key = decodeURIComponent(key.substring(0, f));
      if (!result[key]) {
        result[key] = [];
      }
      if (!index) {
        result[key].push(val);
      } else {
        result[key][index] = val;
      }
    }
  });
  return result;
};

Reuters.trackEvent = function(category, type, id) {
  category = category || 'Page click';
  //console.log(category, type, id);
  var typeString = type;
  if (id) {
    typeString += ': ' + id;
  }
  var gaOpts = {
    'nonInteraction': false,
    'page': PAGE_TO_TRACK
  };

  ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function() {
  $('[data-slider]').each(function() {
    var $el = $(this);
    var getPropArray = function getPropArray(value) {
      if (!value) {
        return [0];
      }
      var out = [];
      var values = value.split(',');
      values.forEach(function(value) {
        out.push(parseFloat(value));
      });
      return out;
    };
    var pips = undefined;
    var start = getPropArray($el.attr('data-start'));
    var min = getPropArray($el.attr('data-min'));
    var max = getPropArray($el.attr('data-max'));
    var orientation = $el.attr('data-orientation') || 'horizontal';
    var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
    var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
    var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
    var snap = $el.attr('data-snap') === 'true' ? true : false;
    var pipMode = $el.attr('data-pip-mode');
    var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
    var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
    var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
    if (pipMode === 'count') {
      pipValues = pipValues[0];
    }

    if (pipMode) {
      pips = {
        mode: pipMode,
        values: pipValues,
        stepped: pipStepped,
        density: pipDensity
      };
    }

    if (connect) {
      var cs = [];
      connect.split(',').forEach(function(c) {
        c = c === 'true' ? true : false;
        cs.push(c);
      });
      connect = cs;
    }

    noUiSlider.create(this, {
      start: start,
      range: {
        min: min,
        max: max
      },
      snap: snap,
      orientation: orientation,
      step: step,
      tooltips: tooltips,
      connect: connect,
      pips: pips
    });
    //This probably doesn't belong here, but will fix the most common use-case.
    $(this).find('div.noUi-marker-large:last').addClass('last');
    $(this).find('div.noUi-marker-large:first').addClass('first');
  });
};

Reuters.getRealImageSize = function(img, type) {
  var $img = $(img);
  var width = void 0,
    height = void 0;
  if (type === 'image') {
    if ($img.prop('naturalWidth') == undefined) {
      var $tmpImg = $('<img/>').attr('src', $img.attr('src'));
      $img.prop('naturalWidth', $tmpImg[0].width);
      $img.prop('naturalHeight', $tmpImg[0].height);
    }
    width = $img.prop('naturalWidth');
    height = $img.prop('naturalHeight');
  } else if (type === 'video') {
    width = $img.prop('videoWidth');
    height = $img.prop('videoHeight');
  }

  return {
    width: width,
    height: height
  };
};

Reuters.autoCropMedia = function($container, $offsetElement, additionalOffset) {
  var autoResize = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

  var $img = $container.find('img:first');
  var type = 'image';
  if (!$img.length) {
    $img = $container.find('video:first');
    type = 'video';
  }
  var $caption = $container.find('.caption-container');
  var winH = window.innerHeight;
  var winW = window.innerWidth;
  var width = winW;
  var height = winH;
  if ($offsetElement && $offsetElement.length) {
    height = height - $offsetElement.outerHeight();
  }
  if (additionalOffset) {
    height = height - additionalOffset;
  }

  if ($caption.length) {
    height = height - $caption.outerHeight();
  }

  var targetHeight = height;
  var realSize = Reuters.getRealImageSize($img, type);
  var ratio = realSize.width / realSize.height;
  //should check again later, no?
  if (realSize.width == 0) {
    _.delay(function() {
      Reuters.autoCropMedia($container, $offsetElement, additionalOffset, autoResize);
    }, 800);
    return;
  }

  if (autoResize) {
    var resizer = function resizer() {
      Reuters.autoCropMedia($container, $offsetElement, additionalOffset, false);
    };

    $(window).on('resize', resizer);
  }

  var left = (winW - height * ratio) / 2;
  var top = 0;
  if (height < 400 || winW < 768) {
    //console.log(height, width);
    $container.css({
      'height': 'auto',
      'width': '100%'
    });
    $img.css({
      'width': '100%',
      'height': 'auto',
      'margin-top': 0,
      'margin-left': 0
    });
    return;
  }

  $container.height(height);

  if (left > 0) {
    left = 0;
    width = winW;
    height = width / ratio;
    top = (targetHeight - height) / 2;
    //console.log('width', width, 'height', height, 'top', top, 'ratio', mastheadRatio);
  } else {
    width = height * ratio;
  }

  //console.log($img, 'width', width, 'height', height, 'top', top, 'left', left, 'ratio',ratio);

  $img.height(height);
  $img.width(width);
  $img.css({
    'margin-left': left + 'px',
    'margin-top': top + 'px',
    'opacity': 1
  });
};

Reuters.centerFullSizeMedia = function($container, $media, mediaType) {
  var autoResize = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

  //flexbox makes this almost simple. Still need to decide which to make 100%.
  var realSize = Reuters.getRealImageSize($media, mediaType);
  //should check again later, no?
  if (realSize.width == 0) {
    _.delay(function() {
      Reuters.centerFullSizeMedia($container, $media, mediaType, autoResize);
    }, 800);
    return;
  }
  if (autoResize) {

    var resizer = function resizer() {
      Reuters.centerFullSizeMedia($container, $media, mediaType, false);
    };

    $(window).on('resize', resizer);
  }

  var $parent = $media.parent();
  $parent.removeClass('wide tall');
  var ratio = realSize.width / realSize.height;

  if ($container.width() / $container.height() < ratio) {
    $parent.addClass('wide');
  } else {
    $parent.addClass('tall');
  }
};

Reuters.popupGallery = function() {
  var _this = this;

  var resizer = function resizer() {
    var $item = $('.popup-gallery .media-item.selected');
    if (!$item.length) {
      return;
    }
    var $mediaItem = $item.find('img:first');
    var type = 'image';
    if (!$mediaItem.length) {
      $mediaItem = $item.find('video:first');
      type = 'video';
    }
    Reuters.centerFullSizeMedia($item.find('.media-container'), $mediaItem, type, false);
  };

  var checkButtons = function checkButtons() {
    $item = $('.popup-gallery .media-item.selected');
    $gallery.find('.next, .prev, .page-button').prop('disabled', false);
    if (!$item.next().length) {
      $gallery.find('.next').prop('disabled', true);
    }
    if (!$item.prev().length) {
      $gallery.find('.prev').prop('disabled', true);
    }
  };

  var showPage = function showPage(id) {
    id = id.split('/')[id.split('/').length - 1];
    console.log(id);
    $gallery.children().find('.selected').removeClass('selected');
    var $new = $gallery.find('img[src*="' + id + '"], video[src*="' + id + '"]').parents('.media-item');
    $new.addClass('selected');
    resizer();
    checkButtons($new);
  };

  var mediaHtml = '';

  var $media = $('.popup-gallery-item');
  $media.each(function(index) {
    var $item = $(this); // I wish JQuery didn't work this way.
    $item.on('click', function() {
      $('.popup-gallery').addClass('show-gallery');
      var id = $item.find('img').prop('src');
      if (!id) {
        id = $item.find('video').prop('src');
      }
      showPage(id);
    });
    //now we want to screw with the html
    $item = $item.clone();
    $item.find('.caption').append('<span class="count">' + (index + 1) + ' / ' + $media.length + '</span>');

    mediaHtml += $('<div />').append($item.clone()).html();
  });

  var galleryHtml = '\n        <div class="popup-gallery">\n            <div class="controls hidden-lg-up">\n                <div class="btn-group flex-row d-flex justify-content-end" role="group">\n                    <button type="button" class="btn btn-primary prev">\n                        <i class="fa fa-arrow-left"></i>\n                    </button>\n                    <button type="button" class="btn btn-primary next">\n                        <i class="fa fa-arrow-right"></i>\n                    </button>\n                    <button type="button" class="btn btn-primary close-button">\n                        <i class="fa fa-times"></i>\n                    </button>\n                </div>\n            </div>\n            <div class="row">\n                <div class="media-items-container">\n                    ' + mediaHtml + '\n                </div>\n                <div class="controls hidden-md-down">\n                    <div class="btn-group-vertical col-12" role="group">\n                        <button type="button" class="btn btn-primary close-button">\n                            <i class="fa fa-times"></i>\n                        </button>\n                        <button type="button" class="btn btn-primary next">\n                            <i class="fa fa-arrow-right"></i>\n                        </button>\n                        <button type="button" class="btn btn-primary prev">\n                            <i class="fa fa-arrow-left"></i>\n                        </button>\n                    </div>\n                    \n                </div>\n            </div>\n        </div>\n    ';
  var $gallery = $(galleryHtml);
  $('body').append($gallery);

  $gallery.find('.close-button').on('click', function() {
    $gallery.removeClass('show-gallery');
  });

  $gallery.find('.prev').on('click', function() {
    var $selected = $gallery.children().find('.selected');
    var $new = $selected.prev();
    if (!$new.length) {
      return;
    }
    $new.addClass('selected');
    $selected.removeClass('selected');
    resizer();
    checkButtons($new);
  });

  $gallery.find('.next').on('click', function() {
    var $selected = $gallery.children().find('.selected');
    var $new = $selected.next();
    if (!$new.length) {
      return;
    }
    $new.addClass('selected');

    $selected.removeClass('selected');
    resizer();
    checkButtons($new);
  });

  $gallery.find('.page-button').on('click', function() {
    var $page = $(_this);
    var pageId = $page.attr('data-id');
    $page.addClass('selected').siblings.removeClass('selected');
    showPage(pageId);
  });

  window.$gallery = $gallery;
  $(window).on('resize', resizer);

  $gallery.find('.media-item:first').addClass('selected');
  checkButtons();
};

Reuters.enableTooltips = function() {
  $('[data-toggle="tooltip"]').tooltip();
};

Reuters.initAds = function() {
  var $ad = $('iframe.lazy-ad');
  $ad.prop('src', $ad.attr('data-src'));
};

Reuters.initStatTracking = function() {
  //stats
  var $els = $('article[id], section[id]');
  var elements = [];
  _.each($els, function(el) {
    try {
      //should toss an error if ScrollDepth will bomb trying to track this element.
      //ad ids are not formatted correctly, apparently. Because of course.
      $(el.tagName.toLowerCase() + '#' + el.id);
      elements.push(el.tagName.toLowerCase() + '#' + el.id);
    } catch (e) {
      //pass.
    }
  });

  var scrollDepthOpts = {
    elements: elements
  };
  var rivetedOpts = {
    reportInterval: 20,
    idleTimeout: 60,
    nonInteraction: false
  };
  try {
    //why throwing undefined if checking for it?
    if (PAGE_TO_TRACK !== undefined) {
      scrollDepthOpts.page = PAGE_TO_TRACK;
      rivetedOpts.page = PAGE_TO_TRACK;
    }
  } catch (e) {}

  try {
    $.scrollDepth(scrollDepthOpts);
    riveted.init(rivetedOpts);
  } catch (e) {
    console.log('scrolldepth or rivited undefined');
  }

  $('.social.navbar-nav a, .share.share-in-article a').on('click', function() {
    var $el = $(this);
    var type = $el.attr('data-id');
    Reuters.trackEvent('Article Event', 'Share Clicked', type);
  });
};

Reuters.hasPym = false;
try {
  Reuters.pymChild = new pym.Child({
    polling: 500
  });
  if (Reuters.pymChild.id) {
    Reuters.hasPym = true;
    $("body").addClass("pym");
  }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
  $("html").addClass("media-flat");
}
if (Reuters.Graphics.Parameters.eikon) {
  $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
  $("html").addClass("remove-header");
}
//# sourceMappingURL=utils.js.map

Reuters = Reuters || {};
Reuters.Graphics = Reuters.Graphics || {};
Reuters.Graphics.LineChartDots = Reuters.Graphics.ChartBase.extend({
  defaults: _.defaults({
    someNewDefault: "yes"
  }, Reuters.Graphics.ChartBase.prototype.defaults),
  //setup the scales.  You have to do this in the specific view, it will be called in the Reuters.Graphics.ChartBase.
  chartType: "line",
  xScaleMin: function xScaleMin() {
    return d3.min(this.jsonData, function(c) {
      return d3.min(c.values, function(v) {
        return v.date;
      });
    });
  },
  xScaleMax: function xScaleMax() {
    return d3.max(this.jsonData, function(c) {
      return d3.max(c.values, function(v) {
        return v.date;
      });
    });
  },
  getXScale: function getXScale() {
    return d3.time.scale().domain([this.xScaleMin(), this.xScaleMax()]).range([0, this.width]);
  },
  yScaleMin: function yScaleMin() {
    var theValues = this.dataType;
    if (this.chartLayout == "stackTotal") {
      theValues = "stackTotal";
    }
    var min = d3.min(this.jsonData, function(c) {
      return d3.min(c.values, function(v) {
        return v[theValues];
      });
    });
    if (this.chartlayout == "fillLines") {
      if (min > 0) {
        min = 0;
      }
    }
    if (this.chartLayout == "stackTotal" || this.chartLayout == "stackPercent") {
      min = 0;
    }
    return min;
  },
  yScaleMax: function yScaleMax() {
    var theValues = this.dataType;
    if (this.chartLayout == "stackTotal") {
      theValues = "stackTotal";
    }
    var max = d3.max(this.jsonData, function(c) {
      return d3.max(c.values, function(v) {
        return v[theValues];
      });
    });
    if (this.chartLayout == "stackPercent") {
      max = 100;
    }
    return max;
  },
  getYScale: function getYScale() {
    var self = this;
    if (!self.yScaleVals || this.hasZoom) {
      return d3.scale.linear().domain([this.yScaleMin(), this.yScaleMax()]).nice(this.yScaleTicks).range([this.height, 0]);
    } else {
      return d3.scale.linear().domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]]).nice(this.yScaleTicks).range([this.height, 0]);
    }
  },
  renderChart: function renderChart() {
    // create a variable called "self" to hold a reference to "this"
    var self = this;
    self.trigger("renderChart:start");

    if (self.hasZoom) {
      self.zoomChart();
    }

    //will draw the line
    self.line = d3.svg.line().x(function(d) {
      return self.scales.x(d.date);
    }).y(function(d) {
      if (self.chartLayout == "stackTotal") {
        return self.scales.y(d.y1Total);
      } else {
        if (self.chartLayout == "stackPercent") {
          return self.scales.y(d.y1Percent);
        } else {
          return self.scales.y(d[self.dataType]);
        }
      }
    }).interpolate(self.lineType).defined(function(d) {
      return !isNaN(d[self.dataType]);
    });

    self.area = d3.svg.area().x(function(d) {
      return self.scales.x(d.date);
    }).y0(function(d) {
      if (self.chartLayout == "stackTotal") {
        return self.scales.y(d.y0Total);
      } else {
        if (self.chartLayout == "stackPercent") {
          return self.scales.y(d.y0Percent);
        } else {
          return self.scales.y(0);
        }
      }
    }).y1(function(d) {
      if (self.chartLayout == "stackTotal") {
        return self.scales.y(d.y1Total);
      } else {
        if (self.chartLayout == "stackPercent") {
          return self.scales.y(d.y1Percent);
        } else {
          return self.scales.y(d[self.dataType]);
        }
      }
    }).interpolate(self.lineType).defined(function(d) {
      return !isNaN(d[self.dataType]);
    });

    //bind the data and put in some G elements with their specific mouseover behaviors.
    self.lineChart = self.svg.selectAll(".lineChart").data(self.jsonData, function(d) {
      return d.name;
    }).enter().append("g").attr({
      "clip-path": "url(#clip" + self.targetDiv + ")",
      class: "lineChart",
      id: function id(d) {
        return self.targetDiv + d.displayName.replace(/\s/g, '') + "-line";
      }
    }).on("mouseover", function(d) {
      //put the line we've hovered on on top=
      self.lineChart.sort(function(a, b) {
        if (a.name == d.name) {
          return 1;
        } else {
          return -1;
        }
      }).order();

      //class all other lines to be lighter
      d3.selectAll("#" + self.targetDiv + " .lineChart").classed('notSelected', true);
      d3.select(this).classed("notSelected", false);
    }).on("mouseout", function(d) {
      d3.selectAll(".lineChart").classed('notSelected', false);
    });

    self.lineChart.selectAll(".tipCircle").data(function(d) {
        return d.values;
      }).enter().append("circle").attr("class", "tipCircle").attr("cx", function(d, i) {
        return self.scales.x(d.date);
      }).attr("cy", function(d, i) {
        if (self.chartLayout == "stackTotal") {
          return self.scales.y(d.y1Total);
        } else {
          if (self.chartLayout == "stackPercent") {
            return self.scales.y(d.y1Percent);
          } else {
            return self.scales.y(d[self.dataType]);
          }
        }
      }).attr("r", function(d, i) {
        if (isNaN(d[self.dataType])) {
          return 0;
        }
        return 5;
      }).style('opacity', function(d) {
        if (self.markDataPoints) {
          return 1;
        }
        return 0;
      }).style("fill", function(d) {
        return self.colorScale(d.name);
      }) //1e-6
      .classed("timeline", function(d) {
        if (self.timelineDataGrouped) {
          if (self.timelineDataGrouped[self.timelineDate(d.date)]) {
            return true;
          }
        }
        return false;
      });

    //add teh zero line on top.
    self.makeZeroLine();

    self.trigger("renderChart:end");
    self.trigger("chart:loaded");
    self.trigger("chart:loaded");

    //end chart render
  },
  update: function update() {
    var self = this;

    self.baseUpdate();
    self.trigger("update:start");

    self.exitLine = d3.svg.line().x(function(d) {
      return self.scales.x(d.date);
    }).y(function(d) {
      return self.margin.bottom + self.height + self.margin.top + 10;
    }).interpolate(self.lineType);

    self.exitArea = d3.svg.area().x(function(d) {
      return self.scales.x(d.date);
    }).y0(function(d) {
      return self.margin.bottom + self.height + self.margin.top + 10;
    }).y1(function(d) {
      return self.margin.bottom + self.height + self.margin.top + 10;
    }).interpolate(self.lineType);

    self.lineChart.data(self.jsonData, function(d) {
      return d.name;
    }).exit().selectAll(".tipCircle").transition().attr("r", 0);

    //the circles
    self.lineChart.data(self.jsonData, function(d) {
      return d.name;
    }).selectAll(".tipCircle").data(function(d) {
      return d.values;
    }).transition().duration(1000).attr("cy", function(d, i) {
      if (self.chartLayout == "stackTotal") {
        return self.scales.y(d.y1Total);
      } else {
        if (self.chartLayout == "stackPercent") {
          return self.scales.y(d.y1Percent);
        } else {
          return self.scales.y(d[self.dataType]);
        }
      }
    }).attr("cx", function(d, i) {
      return self.scales.x(d.date);
    }).attr("r", function(d, i) {
      if (isNaN(d[self.dataType])) {
        return 0;
      }
      return 5;
    });

    self.lineChart.data(self.jsonData, function(d) {
      return d.name;
    }).selectAll(".tipCircle").data(function(d) {
      return d.values;
    }).exit().transition().duration(1000).attr("r", 0).each("end", function(d) {
      d3.select(this).remove();
    });

    self.lineChart.data(self.jsonData, function(d) {
        return d.name;
      }).selectAll(".tipCircle").data(function(d) {
        return d.values;
      }).enter().append("circle").attr("class", "tipCircle").attr("cx", function(d, i) {
        return self.scales.x(d.date);
      }).attr("cy", function(d, i) {
        if (self.chartLayout == "stackTotal") {
          return self.scales.y(d.y1Total);
        } else {
          if (self.chartLayout == "stackPercent") {
            return self.scales.y(d.y1Percent);
          } else {
            return self.scales.y(d[self.dataType]);
          }
        }
      }).style('opacity', function(d) {
        if (self.markDataPoints) {
          return 1;
        }
        return 0;
      }).style("fill", function(d) {
        return self.colorScale(d.name);
      }) //1e-6
      .attr("r", 0).transition().duration(1000).attr("r", function(d, i) {
        if (isNaN(d[self.dataType])) {
          return 0;
        }
        return 5;
      });

    self.trigger("update:end");

    //end of update
  }
  //end model
});
//# sourceMappingURL=LineChartDots.js.map

Reuters.Graphics.FeaturePage = function(_Backbone$View) {
  babelHelpers.inherits(FeaturePage, _Backbone$View);

  function FeaturePage() {
    babelHelpers.classCallCheck(this, FeaturePage);
    return babelHelpers.possibleConstructorReturn(this, (FeaturePage.__proto__ || Object.getPrototypeOf(FeaturePage)).apply(this, arguments));
  }

  babelHelpers.createClass(FeaturePage, [{
    key: 'preinitialize',
    value: function preinitialize() {
      this.events = {
        'change .nav-options .btn': 'onSectionChange'
      };
      this.router = new Reuters.Graphics.FeaturePageRouter();
    }
  }, {
    key: 'initialize',
    value: function initialize(options) {
      var self = this;
      this.$el.html(Reuters.Graphics.Template.suiteLayout());
      d3.queue()
        //english language version
        .defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-parties-en").defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-polls-en").defer(d3.csv, "data/resultshistory.csv").defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-election-scenario-text-en").defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-scenarios-en").await(render);

      function render(error, parties, polls, resultshistory, scenariochart, scenariostext) {
        if (error) {
          $("html").addClass("has_error");
        }

        self.partiesData = parties;
        self.pollsData = polls;
        self.scenariosTextData = scenariostext;
        self.scenarioChartData = scenariochart;
        self.resultshistoryData = resultshistory;
        self.render();
      }
      this.listenTo(this.router, 'route:section', this.changeSection);
    }
  }, {
    key: 'render',
    value: function render() {
      this.parseDate = d3.time.format("%d/%m/%Y").parse;
      this.formatDate = d3.time.format("%b %e");
      if (Reuters.Graphics.Parameters.eikon) {
        black = "#888";
      }

      Backbone.history.start();

      this.partiesArray = ["cducsu", "spd", "green", "fdp", "left", "afd", "other"];
      this.partyLookup = {
        cducsu: "CDU/CSU",
        spd: "SPD",
        green: "Green",
        fdp: "FDP",
        left: "Left",
        afd: "AfD",
        other: "Other"
      };
      this.partyColours = {
        cducsu: black,
        spd: red4,
        green: lime5,
        fdp: yellow4,
        left: violet4,
        afd: cyan5,
        other: grey2
      };

      this.parties();
      this.pollshistory();
      this.latestPolls();
      this.scenarios();
      this.resultshistory();
      return this;
    }
  }, {
    key: 'parties',
    value: function parties() {
      var self = this;

      $("#party-profiles").html(Reuters.Graphics.Template.parties({
        self: self
      }));

      var election = {
        issues: ["economy", "securityimmigration", "europeworld", "societyinequality"],
        parties: self.partiesData,
        issuelookup: {
          economy: "Economy",
          securityimmigration: "Security and immigration",
          europeworld: "Europe and the world",
          societyinequality: "Society and inequality"
        }
      };

      $("#party-sort").html(Reuters.Graphics.Template.partyContent({
        election: election
      }));
      $("#issue-sort").html(Reuters.Graphics.Template.issueContent({
        election: election
      }));

      $(".navContainer.party-buttons .btn").on("click", function(evt) {
        var thisID = $(this).attr("dataid");
        $(".party-table").removeClass("selected");
        $("#" + thisID).addClass("selected");
      });
    }
  }, {
    key: 'pollshistory',
    value: function pollshistory() {
      var self = this;
      $("#historicalpolls").html(Reuters.Graphics.Template.historicalpolls({
        self: self
      }));

      // Reuters.Graphics.sharePrice = new Reuters.Graphics.LineChartDots({

      Reuters.Graphics.sharePrice = new Reuters.Graphics.LineChartDots({
        el: "#reutersGraphic-historypoll",
        dataURL: self.pollsData,
        height: 350, //if < 10 - ratio , if over 10 - hard height.  undefined - square
        columnNames: this.partyLookup, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
        colors: this.partyColours,
        yScaleVals: [0, 10, 20, 30, 40, 50],
        YTickLabel: [
          [gettext(""), "%"]
        ], //  \u00A0  - use that code for a space.
        dateFormat: d3.time.format("%b %d, %Y"),
        numbFormat: d3.format(",.1f"),
        showTip: true,
        parseDate: d3.time.format("%d/%m/%Y").parse,
        markDataPoints: true,
        tipTemplate: Reuters.Graphics.Template.tooltipdots
      });
    }
  }, {
    key: 'latestPolls',
    value: function latestPolls() {
      var self = this;

      var groupedlatestPolls = _.groupBy(self.pollsData, "pollster");

      var latestPollsData = {};
      var largestFirstPollValue = 0;
      var latestPollsHistorical = {};

      _.each(groupedlatestPolls, function(array, key) {

        // if (key == "YouGov"){
        // 	latestPollsHistorical[key] = array;
        // }

        var lastItem = array[array.length - 1];

        latestPollsData[key] = self.partiesArray.map(function(party) {
          largestFirstPollValue = parseFloat(lastItem[party]) > largestFirstPollValue ? parseFloat(lastItem[party]) : largestFirstPollValue;
          return {
            category: party,
            value: lastItem[party],
            pollster: lastItem.pollster,
            marginoferror: lastItem.marginoferror,
            marginoferrorhighend: lastItem.marginoferrorhighend,
            samplesize: lastItem.samplesize,
            polldate: lastItem.date,
            daterange: lastItem.daterange
          };
        });
      });

      largestFirstPollValue = Math.ceil(largestFirstPollValue / 10) * 10;

      var pollsters = _.keys(latestPollsData).sort(function(a, b) {
        var aValue = self.parseDate(latestPollsData[a][0].polldate);
        var bValue = self.parseDate(latestPollsData[b][0].polldate);
        if (aValue > bValue) {
          return -1;
        }
        if (aValue < bValue) {
          return 1;
        }
        return 0;
      });
      self.samplesizeFormat = d3.format(",.0f");
      self.marginFormat = d3.format(",.1f");

      $("#pollslatest").html(Reuters.Graphics.Template.pollslatest({
        pollsters: pollsters,
        latestPollsData: latestPollsData,
        self: this
      }));

      pollsters.forEach(function(key, i) {

        console.log(key);
        if (i < 3) {

          var value = latestPollsData[key];

          var id = key.split(' ')[0].split('/')[0];
          if (key == "Forschungsgruppe Wahlen") {
            id = "Forschungsgruppe";
          }
          if (key == "Infratest Dimap") {
            id = "Infratest";
          }

          value.forEach(function(d) {
            d.category = self.partyLookup[d.category];
          });

          var sortOrder = self.partiesArray.map(function(d) {
            return self.partyLookup[d];
          });

          Reuters.Graphics[id] = new Reuters.Graphics.BarChart({
            el: "#reutersGraphic-latest-poll-" + id,
            dataURL: value,
            height: 125, //if < 10 - ratio , if over 10 - hard height.  undefined - square
            columnNames: {
              value: "Value"
            }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
            color: [black, red4, lime5, yellow4, violet4, cyan5, grey2],
            // color:self.partyColours,
            orient: "right",
            YTickLabel: [
              [gettext(""), "%"]
            ], //  \u00A0  - use that code for a space.
            hasLegend: false,
            numbFormat: d3.format(",.1f"),
            horizontal: true,
            yScaleMax: function yScaleMax() {
              return largestFirstPollValue;
            },
            categorySort: sortOrder,
            barFill: function barFill(d) {

              var self = this;
              return self.color[self.categorySort.indexOf(d.category)];
            },
            parseDate: d3.time.format("%d/%m/%Y").parse
          });
          Reuters.Graphics[id].on("renderChart:end", function(evt) {
            self.addMoe(this);
          });

          Reuters.Graphics[id].on("update:start", function(evt) {
            self.updateMoe(this);
          });
          if (i = pollsters.length - 1) {
            $("#reutersGraphic-latest-poll-" + id).addClass("hidden-x-axis");
          }
        } else {
          return;
        }
      });
    }
  }, {
    key: 'scenarios',
    value: function scenarios() {

      var self = this;

      var scenariostextloop = self.scenariostextData;

      d3.json("//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-election-scenario-text-en", function(data) {
        $("#scenario-charts-id").html(Reuters.Graphics.Template.scenarios({
          data: data
        }));

        Reuters.Graphics.grand = new Reuters.Graphics.BarChart(babelHelpers.defineProperty({
          el: "#reutersGraphic-grand",
          dataURL: "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-scenarios-en",
          height: 35, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {
            cducsu: "CDU/CSU",
            spd: "SPD",
            othergrand: "Others"
          }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          colors: [black, red4, gray1], //array or mapped object
          yScaleVals: [0, 598],
          horizontal: true,
          numbFormat: d3.format(",.0f"),
          chartBreakPoint: 2000,
          margin: {
            top: 0,
            right: 1,
            bottom: 0,
            left: 0
          },
          tipNumbFormat: function tipNumbFormat(d) {
            var self = this;
            if (isNaN(d) === true) {
              return "N/A";
            } else {
              return self.dataLabels[0] + self.numbFormat(d) + " seats" + self.dataLabels[1];
            }
          },
          chartLayout: "stackTotal", // basic,stackTotal, stackPercent, fillLines, sideBySide, onTopOf,
          yTickFormat: function yTickFormat(d) {
            var numbFormat = d3.format(".0f");
            if (numbFormat(d) == 299) {
              return "299";
            } else {}
          },
          groupSort: ["othersgrand", "spd", "cducsu"]
        }, 'chartLayout', "stackTotal"));

        Reuters.Graphics.blackyellow = new Reuters.Graphics.BarChart(babelHelpers.defineProperty({
          el: "#reutersGraphic-blackyellow",
          dataURL: "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-scenarios-en",
          height: 35, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {
            cducsu: "CDU/CSU",
            fdp: "FDP",
            otherblackyellow: "Others"
          }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          colors: [black, yellow4, gray1], //array or mapped object
          yScaleVals: [0, 598],
          horizontal: true,
          numbFormat: d3.format(",.0f"),
          chartBreakPoint: 2000,
          margin: {
            top: 0,
            right: 1,
            bottom: 0,
            left: 0
          },
          tipNumbFormat: function tipNumbFormat(d) {
            var self = this;
            if (isNaN(d) === true) {
              return "N/A";
            } else {
              return self.dataLabels[0] + self.numbFormat(d) + " seats" + self.dataLabels[1];
            }
          },
          chartLayout: "stackTotal", // basic,stackTotal, stackPercent, fillLines, sideBySide, onTopOf,
          yTickFormat: function yTickFormat(d) {
            var numbFormat = d3.format(".0f");
            if (numbFormat(d) == 299) {
              return "299";
            } else {}
          },
          groupSort: ["otherblackyellow", "fdp", "cducsu"]
        }, 'chartLayout', "stackTotal"));

        Reuters.Graphics.blackgreen = new Reuters.Graphics.BarChart(babelHelpers.defineProperty({
          el: "#reutersGraphic-blackgreen",
          dataURL: "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-scenarios-en",
          height: 35, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {
            cducsu: "CDU/CSU",
            green: "Greens",
            otherblackgreen: "Others"
          }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          colors: [black, lime5, gray1], //array or mapped object
          yScaleVals: [0, 598],
          horizontal: true,
          numbFormat: d3.format(",.0f"),
          chartBreakPoint: 2000,
          margin: {
            top: 0,
            right: 1,
            bottom: 0,
            left: 0
          },
          tipNumbFormat: function tipNumbFormat(d) {
            var self = this;
            if (isNaN(d) === true) {
              return "N/A";
            } else {
              return self.dataLabels[0] + self.numbFormat(d) + " seats" + self.dataLabels[1];
            }
          },
          chartLayout: "stackTotal", // basic,stackTotal, stackPercent, fillLines, sideBySide, onTopOf,
          yTickFormat: function yTickFormat(d) {
            var numbFormat = d3.format(".0f");
            if (numbFormat(d) == 299) {
              return "299";
            } else {}
          },
          groupSort: ["otherblackgreen", "green", "cducsu"]
        }, 'chartLayout', "stackTotal"));

        Reuters.Graphics.jamaica = new Reuters.Graphics.BarChart(babelHelpers.defineProperty({
          el: "#reutersGraphic-jamaica",
          dataURL: "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-scenarios-en",
          height: 35, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {
            cducsu: "CDU/CSU",
            fdp: "FDP",
            green: "Greens",
            otherjamaica: "Others"
          }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          colors: [black, yellow4, lime5, gray1], //array or mapped object
          yScaleVals: [0, 598],
          horizontal: true,
          numbFormat: d3.format(",.0f"),
          chartBreakPoint: 2000,
          margin: {
            top: 0,
            right: 1,
            bottom: 0,
            left: 0
          },
          tipNumbFormat: function tipNumbFormat(d) {
            var self = this;
            if (isNaN(d) === true) {
              return "N/A";
            } else {
              return self.dataLabels[0] + self.numbFormat(d) + " seats" + self.dataLabels[1];
            }
          },
          chartLayout: "stackTotal", // basic,stackTotal, stackPercent, fillLines, sideBySide, onTopOf,
          yTickFormat: function yTickFormat(d) {
            var numbFormat = d3.format(".0f");
            if (numbFormat(d) == 299) {
              return "299";
            } else {}
          },
          groupSort: ["otherjamaica", "green", "fdp", "cducsu"]
        }, 'chartLayout', "stackTotal"));

        Reuters.Graphics.redredgreen = new Reuters.Graphics.BarChart(babelHelpers.defineProperty({
          el: "#reutersGraphic-redredgreen",
          dataURL: "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-2017-german-scenarios-en",
          height: 35, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {
            spd: "SPD",
            left: "Left",
            green: "Greens",
            otherredredgreen: "Others"
          }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          colors: [red4, violet4, lime5, gray1], //array or mapped object
          yScaleVals: [0, 598],
          horizontal: true,
          numbFormat: d3.format(",.0f"),
          chartBreakPoint: 2000,
          margin: {
            top: 0,
            right: 1,
            bottom: 0,
            left: 0
          },
          tipNumbFormat: function tipNumbFormat(d) {
            var self = this;
            if (isNaN(d) === true) {
              return "N/A";
            } else {
              return self.dataLabels[0] + self.numbFormat(d) + " seats" + self.dataLabels[1];
            }
          },
          chartLayout: "stackTotal", // basic,stackTotal, stackPercent, fillLines, sideBySide, onTopOf,
          yTickFormat: function yTickFormat(d) {
            var numbFormat = d3.format(".0f");
            if (numbFormat(d) == 299) {
              return "299";
            } else {}
          },
          groupSort: ["otherredredgreen", "green", "left", "spd"]
        }, 'chartLayout', "stackTotal"));
      });
    }
  }, {
    key: 'resultshistory',
    value: function resultshistory() {
      var self = this;
      $("#results2017").html(Reuters.Graphics.Template.results2017({
        self: self
      }));
      $("#historyresults").html(Reuters.Graphics.Template.historyresults({
        self: self
      }));

      d3.json("//d3sl9l9bcxfb5q.cloudfront.net/json/mo-german-election-results-percent", function(resultDataBar) {

        $("#timestamp-first").html(resultDataBar[0].timestamp);

        if (resultDataBar[0].publish != "yes") {
          $(".results-holder-percent").addClass("hidden");
        }
        if (Reuters.Graphics.Parameters.test) {
          $(".results-holder-percent").removeClass("hidden");
        }

        console.log(resultDataBar);

        Reuters.Graphics.finalResultsBar = new Reuters.Graphics.BarChart({
          el: "#reutersGraphic-results-percent",
          dataURL: resultDataBar,
          YTickLabel: [
            [gettext(""), "%"]
          ],
          height: 200,
          columnNames: {
            value: "Value"
          },
          categorySort: "descending",
          hasLegend: false,
          horizontal: true,
          numbFormat: d3.format(",.1f"),
          barFill: function barFill(d) {
            var self = this;
            if (d.category === "CDU/CSU") {
              return black;
            } else if (d.category === "SPD") {
              return red4;
            } else if (d.category === "Left") {
              return violet4;
            } else if (d.category === "FDP") {
              return yellow4;
            } else if (d.category === "AfD") {
              return cyan5;
            } else if (d.category === "Greens") {
              return lime5;
            } else if (d.category === "Other") {
              return gray2;
            }
            return gray1;
          }

        });
      });

      d3.json("//d3sl9l9bcxfb5q.cloudfront.net/json/mo-german-election-results-seats", function(resultDataPie) {

        $("#timestamp-second").html(resultDataPie[0].timestamp);

        if (resultDataPie[0].publish != "yes") {
          $(".results-holder-seats").addClass("hidden");
        }
        if (Reuters.Graphics.Parameters.test) {
          $(".results-holder-seats").removeClass("hidden");
        }

        console.log(resultDataPie, "PIE");

        Reuters.Graphics.finalResultsDonut = new Reuters.Graphics.donut({
          el: "#reutersGraphic-results-seats",
          margin: {
            top: 0,
            left: 4,
            right: 4,
            bottom: 0
          },
          dataURL: resultDataPie,
          height: 1,
          colorRange: [black, red4, violet4, yellow4, cyan5, lime5],
          colorDomain: ["CDU/CSU", "SPD", "Left", "FDP", "AfD", "Greens"], // array of values (or will auto pull based on color value
          plotValue: "seats",
          colorValue: "partyname",
          donutHoleSize: 4,
          hasLegend: true,
          tooltipTemplate: Reuters.Graphics.Template.pietooltip,
          setupTemplate: Reuters.Graphics.Template.piesetup
        });
      });

      Reuters.Graphics.topdonut = new Reuters.Graphics.donut({
        el: "#reutersGraphic-historyresults",
        margin: {
          top: 0,
          left: 4,
          right: 4,
          bottom: 0
        },
        dataURL: self.resultshistoryData,
        height: 1,
        colorRange: [violet4, lime5, red4, yellow4, black, gray5],
        colorDomain: ["LEFT/PDS", "GREENS", "SPD", "FDP", "CDU", "CSU"], // array of values (or will auto pull based on color value
        plotValue: "seats",
        colorValue: "partyname",
        multiArcs: "year",
        multiSort: ["2013", "2009", "2005", "2002", "1998", "1994", "1990"],
        donutHoleSize: 4,
        hasLegend: true,
        tooltipTemplate: Reuters.Graphics.Template.pietooltip,
        setupTemplate: Reuters.Graphics.Template.piesetup

      });

      Reuters.Graphics.topdonut.on("baseRender:end", function() {
        var self = this;
        self.svg.selectAll(".arcLabels").classed("hidden", function(d, i) {
          // put in your logic here for whether or not it will show up
          if (i % 3 != 0) {
            return true;
          }
          return false;
        });
      });
    }
  }, {
    key: 'addMoe',
    value: function addMoe(self) {

      self.t = textures.lines().size(5).orientation("2/8").stroke("#C3C4C6");

      self.svg.call(self.t);

      self.addMoe = self.barChart.selectAll(".moebar").data(function(d) {
        return d.values;
      }).enter().append("rect").attr("class", ".moebar").style("fill", function(d) {
        return self.t.url();
      }).attr("height", function(d, i, j) {
        return self.barWidth(d, i, j);
      }).attr("y", function(d, i, j) {
        return self.xBarPosition(d, i, j);
      }).attr("x", function(d) {
        return self.scales.y(d.value) - self.scales.y(parseFloat(d.marginoferrorhighend)) / 2;
      }).attr("width", function(d) {
        return self.scales.y(parseFloat(d.marginoferrorhighend));
      });
    }
  }, {
    key: 'updateMoe',
    value: function updateMoe(self) {

      self.addMoe.transition().duration(1000).attr("height", function(d, i, j) {
        return self.barWidth(d, i, j);
      }).attr("y", function(d, i, j) {
        return self.xBarPosition(d, i, j);
      }).attr("x", function(d) {
        return self.scales.y(d.value) - self.scales.y(parseFloat(d.marginoferrorhighend)) / 2;
      }).attr("width", function(d) {
        return self.scales.y(parseFloat(d.marginoferrorhighend));
      });
    }
  }, {
    key: 'onSectionChange',
    value: function onSectionChange(event) {
      var $el = $(event.currentTarget);
      var id = $el.attr('data-id');
      this.changeSection(id, $el);
    }
  }, {
    key: 'changeSection',
    value: function changeSection(id, $button) {
      var $el = this.$('#' + id);
      if ($el.hasClass('selected')) {
        return;
      }
      if (!$button) {
        $button = this.$('.nav-options .btn[data-id="' + id + '"]');
        $button.addClass('active').siblings().removeClass('active');
        $button.find('input').addClass('active').attr('checked', 'checked');
      }
      $el.addClass('selected').siblings().removeClass('selected');
      this.router.navigate('section/' + id, {
        trigger: false
      });
      // 		_.invoke(this.charts, 'update');
    }
  }]);
  return FeaturePage;
}(Backbone.View);

Reuters.Graphics.FeaturePageRouter = function(_Backbone$Router) {
  babelHelpers.inherits(FeaturePageRouter, _Backbone$Router);

  function FeaturePageRouter() {
    babelHelpers.classCallCheck(this, FeaturePageRouter);
    return babelHelpers.possibleConstructorReturn(this, (FeaturePageRouter.__proto__ || Object.getPrototypeOf(FeaturePageRouter)).apply(this, arguments));
  }

  babelHelpers.createClass(FeaturePageRouter, [{
    key: 'preinitialize',
    value: function preinitialize() {
      this.routes = {
        'section/:id': 'section'
      };
    }
  }]);
  return FeaturePageRouter;
}(Backbone.Router);

$(document).ready(function() {
  window.featurePageExample = new Reuters.Graphics.FeaturePage({
    el: '.main'
  });
  // Backbone.history.start();
});
//# sourceMappingURL=main.js.map
