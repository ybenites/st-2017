import $ from "jquery";

import * as d3 from "d3";

import "fullpage.js";

import swipe from "jquery-touchswipe";

import './libraries/iphone-inline-video';

var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

var f_click = 0,
    a_videos = [],
    delaytime = 3000,
    delaytimefor6 = 1000,
    delaytimefor4 = 4000,
    delaytimefor11 = 6000,
    intervalTime = 0;

var ua = navigator.userAgent.toLowerCase();
var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");

// Animate loader off screen
$(window).load(function() {
    $(".loading-icon ").fadeOut("slow");
});

// in IE show the msg
if (!detectIE()) document.getElementsByClassName("box-info-ie")[0].style.display = "none";

function detectIE() {
    var ua = window.navigator.userAgent;
    var ie = ua.search(/(MSIE|Trident|Edge)/);

    return ie > -1;
}

// add autoplay and muted for android, else no
$(function() {
    if (isAndroid) {
        $("#section1").prepend('<video autoplay muted playsinline class="myVideo" id="v1"><source src="videos/cards/animae-frame-cover_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-cover_H264_Max.webm" type="video/webm"></video>');
        $("#section2").prepend('<video autoplay muted loop playsinline class="myVideo" id="v2" poster="videos/posters/anim-2.png"><source src="videos/cards/animae-frame-park-geun-hye-p1-H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-park-geun-hye-p1-H264_Max.webm" type="video/webm"></video>');
        $("#section5").prepend('<video autoplay muted loop playsinline class="myVideo" id="v5" poster="videos/posters/anim-3.png"><source src="videos/cards/animae-frame-park-geun-hye-p2-H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-park-geun-hye-p2-H264_Max.webm" type="video/webm"></video>');
        $("#section3").prepend('<video autoplay muted loop playsinline class="myVideo" id="v3" poster="videos/posters/anim-4.png"><source src="videos/cards/animae-frame-choi-soon-sil-p1_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-choi-soon-sil-p1_H264_Max.webm" type="video/webm"></video>');
        $("#section4").prepend('<video autoplay muted loop playsinline class="myVideo" id="v4" poster="videos/posters/anim-5.png"><source src="videos/cards/animae-frame-choi-soon-sil-p2_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-choi-soon-sil-p2_H264_Max.webm" type="video/webm"></video>');
        $("#section6").prepend('<video autoplay muted loop playsinline class="myVideo" id="v6" poster="videos/posters/anim-6.png"><source src="videos/cards/animae-frame-chung-yoo-ra_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-chung-yoo-ra_H264_Max.webm" type="video/webm"></video>');
        $("#section7").prepend('<video autoplay muted loop playsinline class="myVideo" id="v7" poster="videos/posters/anim-7.png"><source src="videos/cards/animae-frame-ko-young-tae_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-ko-young-tae_H264_Max.webm" type="video/webm"></video>');
        $("#section8").prepend('<video autoplay muted loop playsinline class="myVideo" id="v8" poster="videos/posters/anim-8.png"><source src="videos/cards/animae-frame-puppy_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-puppy_H264_Max.webm" type="video/webm"></video>');
        $("#section9").prepend('<video autoplay muted loop playsinline class="myVideo" id="v9" poster="videos/posters/anim-9.png"><source src="videos/cards/animae-frame-lee-jae-yong_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-lee-jae-yong_H264_Max.webm" type="video/webm"></video>');
        $("#section10").prepend('<video autoplay muted loop playsinline class="myVideo" id="v10" poster="videos/posters/anim-10.png"><source src="videos/cards/animae-frame-choi-soon-sil-jail_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-choi-soon-sil-jail_H264_Max.webm" type="video/webm"></video>');
        $("#section11").prepend('<video autoplay muted loop playsinline class="myVideo" id="v11" poster="videos/posters/anim-11.png"><source src="videos/cards/animae-frame-park-geun-hye-ending_H264_Med.mp4" type="video/mp4"><source src="videos/cards/animae-frame-park-geun-hye-ending_H264_Med.webm" type="video/webm"></video>');
    } else {
        $("#section1").prepend('<video playsinline class="myVideo" id="v1"><source src="videos/cards/animae-frame-cover_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-cover_H264_Max.webm" type="video/webm"></video>');
        $("#section2").prepend('<video loop playsinline class="myVideo" id="v2"><source src="videos/cards/animae-frame-park-geun-hye-p1-H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-park-geun-hye-p1-H264_Max.webm" type="video/webm"></video>');
        $("#section5").prepend('<video loop playsinline class="myVideo" id="v5"><source src="videos/cards/animae-frame-park-geun-hye-p2-H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-park-geun-hye-p2-H264_Max.webm" type="video/webm"></video>');
        $("#section3").prepend('<video loop playsinline class="myVideo" id="v3"><source src="videos/cards/animae-frame-choi-soon-sil-p1_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-choi-soon-sil-p1_H264_Max.webm" type="video/webm"></video>');
        $("#section4").prepend('<video loop playsinline class="myVideo" id="v4"><source src="videos/cards/animae-frame-choi-soon-sil-p2_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-choi-soon-sil-p2_H264_Max.webm" type="video/webm"></video>');
        $("#section6").prepend('<video loop playsinline class="myVideo" id="v6"><source src="videos/cards/animae-frame-chung-yoo-ra_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-chung-yoo-ra_H264_Max.webm" type="video/webm"></video>');
        $("#section7").prepend('<video loop playsinline class="myVideo" id="v7"><source src="videos/cards/animae-frame-ko-young-tae_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-ko-young-tae_H264_Max.webm" type="video/webm"></video>');
        $("#section8").prepend('<video loop playsinline class="myVideo" id="v8"><source src="videos/cards/animae-frame-puppy_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-puppy_H264_Max.webm" type="video/webm"></video>');
        $("#section9").prepend('<video loop playsinline class="myVideo" id="v9"><source src="videos/cards/animae-frame-lee-jae-yong_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-lee-jae-yong_H264_Max.webm" type="video/webm"></video>');
        $("#section10").prepend('<video loop playsinline class="myVideo" id="v10"><source src="videos/cards/animae-frame-choi-soon-sil-jail_H264_Max.mp4" type="video/mp4"><source src="videos/cards/animae-frame-choi-soon-sil-jail_H264_Max.webm" type="video/webm"></video>');
        $("#section11").prepend('<video loop playsinline class="myVideo" id="v11"><source src="videos/cards/animae-frame-park-geun-hye-ending_H264_Med.mp4" type="video/mp4"><source src="videos/cards/animae-frame-park-geun-hye-ending_H264_Med.webm" type="video/webm"></video>');

    }
});

var showicon = false;
$(document).ready(function() {
    if (isMobile) {
        $(window).on("touchstart click", function(e) {
            if (f_click === 0 && a_videos.length > 0) {
                f_click = 1;
                a_videos.forEach(function(v) {
                    v.play();
                    v.pause();
                });
            }
        });

        // show first video poster in mobile only
        $('#section1').find("video").attr('poster', 'videos/cards/still-cover-M-idea2.png');

        // make sound icon hide first and appear at 2nd card in mobile
        $("#soundIcon").hide();
    }

    // last card layout
    $(".selectIcon a").each(function(i) {
        var winwidth = $(window).width();
        if (winwidth < 768) {
            $("#fordesktop").css("display", "none");
            $("#formobile").css("display", "block");
        } else {
            $("#fordesktop").css("display", "block");
            $("#formobile").css("display", "none");
            $(this).hover(function() {
                $(this).next(".profileName").children().css("display", "block");
            }, function() {
                $(this).next(".profileName").children().css("display", "none");
            });
        }
    });

    $('#fullpage').fullpage({
        navigation: true,
        afterRender: function() {
            $('video').each(function() {
                enableInlineVideo(this);
                a_videos.push(this);
            });
        },
        onLeave: function(index, nextIndex, direction) {

            // pause video before leaving card
            var vd = $(this).find('video');
            if (vd.length > 0 && !vd.paused) {

                vd[0].pause();
                // vd[0].muted = true;
            }

            // nav button colors
            if (direction == 'down') {
                $("#fp-nav ul li").eq(index - 1).find("span").css({
                    border: "1px solid #C0CAC9",
                    background: "#C0CAC9"
                }); //nav button to be gray after visited
                $("#fp-nav ul li").eq(index).find("span").css({
                    border: "1px solid #ffffff",
                    background: "#ffffff"
                }); //nav button to be white for current visit
            } else if (direction == 'up') {
                $("#fp-nav ul li").eq(index - 2).find("span").css({
                    border: "1px solid #ffffff",
                    background: "#ffffff"
                }); //nav button to be white for current visit
                $("#fp-nav ul li").eq(index - 1).find("span").css({
                    border: "1px solid #ffffff",
                    background: "transparent"
                });
            }
        },

        afterLoad: function(anchorLink, index) {
            if (index == 2) {
                playIndex2();
                if (isMobile && !showicon) {
                    // console.log(showicon);
                    $("#soundIcon").show();
                    showicon = true;
                }
            } else if (index == 3) playIndex5();
            else if (index == 4) playIndex3();
            else if (index == 5) playIndex4();
            else if (index == 6) playIndex6();
            else if (index == 7) playIndex7();
            else if (index == 8) playIndex8();
            else if (index == 9) playIndex9();
            else if (index == 10) playIndex10();
            else if (index == 11) playIndex11();

            var f_video = $(this).find('video');
            if (f_video.length > 0) {
                // start video from beginning at every time card reached
                f_video[0].currentTime = 0;
                f_video[0].play();

                // wait few sec then loop
                // if (index != 1 && index != 12) {
                //     intervalTime = delaytime;
                //     if (index == 6) intervalTime = delaytimefor6;
                //     else if (index == 4) intervalTime = delaytimefor4;
                //     else if (index == 11) intervalTime = delaytimefor11;
                //     f_video[0].onended = function() {
                //         // console.log(f_video[0].duration);
                //         f_video[0].currentTime = f_video[0].duration - 0.08;
                //         var delay = setTimeout(function() {
                //             f_video[0].pause();
                //             f_video[0].currentTime = 0;
                //             f_video[0].play();
                //             clearTimeout(delay);
                //         }, intervalTime);
                //     };
                // }
            }
        }
    });
});

function playIndex2() {
    $("#sequent23").stop(true).hide();
    $("#sequent21").stop(true).hide().fadeIn();
    $("#sequent22").stop(true).hide().delay(1000).fadeIn();
    $("#sequent21,#sequent22").delay(3000).fadeOut().queue(function() {
        $("#sequent23").delay(500).fadeIn();
        // $("#sequent23").delay(3000).fadeOut().delay(delaytime + 2000).hide(playIndex2);
        $("#sequent23").delay(3000).fadeOut().delay(2000).hide(playIndex2);
        $("#scrolldown2").delay(5000).fadeIn();
    });
}

function playIndex3() {
    $("#sequent32").stop(true).hide();
    $("#sequent31").stop(true).hide().fadeIn();
    $("#sequent31").delay(3500).fadeOut().queue(function() {
        $("#sequent32").delay(1000).fadeIn();
        // $("#sequent32").delay(3000).fadeOut().delay(delaytime).hide(playIndex3);
        $("#sequent32").delay(3000).fadeOut().hide(playIndex3);
        $("#scrolldown3").delay(2000).fadeIn();
    });

}

function playIndex4() {
    $("#sequent43").stop(true).hide();
    $("#sequent42").stop(true).hide().fadeIn();
    $("#sequent42").delay(6000).fadeOut().queue(function() {
        $("#sequent43").delay(2000).fadeIn();
        // $("#sequent43").delay(5000).fadeOut().delay(delaytime + 1000).hide(playIndex4);
        $("#sequent43").delay(5000).fadeOut().delay(1000).hide(playIndex4);
        $("#scrolldown4").delay(7000).fadeIn();
    });

}

function playIndex5() {
    $("#sequent52").stop(true).hide();
    $("#sequent51").stop(true).hide().fadeIn();
    $("#sequent51").delay(5000).fadeOut().queue(function() {
        $("#sequent52").delay(1000).fadeIn();
        // $("#sequent52").delay(2000).fadeOut().delay(delaytime).hide(playIndex5);
        $("#sequent52").delay(2000).fadeOut().hide(playIndex5);
        $("#scrolldown5").delay(2000).fadeIn();
    });
}

function playIndex6() {
    $("#sequent62").fadeIn();
    $("#scrolldown6").delay(6000).fadeIn();
}

function playIndex7() {
    $("#sequent72").fadeIn();
    $("#scrolldown7").delay(6000).fadeIn();
}

function playIndex8() {
    $("#sequent81").fadeIn();
    $("#scrolldown8").delay(3000).fadeIn();
}

function playIndex9() {
    $("#sequent92").fadeIn();
    $("#scrolldown9").delay(6000).fadeIn();
}

function playIndex10() {
    $("#sequent101").fadeIn();
    $("#scrolldown10").delay(7000).fadeIn();
}

function playIndex11() {
    $("#sequent112").stop(true).hide();
    $("#sequent113").stop(true).hide();
    $("#sequent114").stop(true).hide();
    $("#sequent111").stop(true).hide().fadeIn();
    $("#sequent111").delay(2000).fadeOut().queue(function() {
        $("#sequent112").delay(1000).fadeIn();
        $("#sequent112").delay(2000).fadeOut().queue(function() {
            $("#sequent113").delay(500).fadeIn();
            $("#sequent113").delay(4000).fadeOut();
            $("#sequent114").delay(2000).fadeIn();
            $("#sequent114").delay(3000).fadeOut().hide(playIndex11);
            $("#scrolldown11").delay(2000).fadeIn();
            // $("#sequent113").delay(3000).fadeOut().queue(function() {
            //     $("#sequent114").fadeIn();
            //     // $("#sequent114").delay(3000).fadeOut().delay(delaytime - 1000).hide(playIndex11);
            //     $("#sequent114").delay(3000).fadeOut().hide(playIndex11);
            //     $("#scrolldown11").delay(2000).fadeIn();
            // });


        });
    });

}


var executed = false;

$(function() {


    // $("video").prop('muted', true);
    // mute videos and change icons
    // if (isAndroid) $(".sounddiv img").hide();
    $(".sounddiv img").on("click", function() {
        $(".sounddiv img").toggle();
        // if (!isAndroid) {
        $('.section video').each(function() {
            if (this.muted === false) this.muted = 1;
            else this.muted = 0;

        });
        // }
    });


    //swipe up in first page to ummuted the sound for android
    //appear sound icon at 2nd card for mobile
    //only execute once
    $("#section1").swipe({
        //Generic swipe handler for all directions
        swipeUp: function(event, direction, distance, duration, fingerCount) {
            if (isAndroid && !executed) {
                // console.log(executed);
                $('.section video').each(function() {
                    if (this.muted === true) this.muted = 0;
                    // else this.muted = 0 ;
                });

            }
            // if (isMobile && !showicon) $("#soundIcon").show();

            executed = true;
            // showicon = true;
        }
    });



    // 1st page: transform title image & show scrolldown icon
    if (isMobile) {
        setTimeout(function() {
            $(".titleImg").css('transform', 'scale(1)');
        }, 1000);
        setTimeout(function() {
            $("#scrolldown1").fadeIn();
        }, 3000);
    } else {
        setTimeout(function() {
            $(".titleImg").css('transform', 'scale(1)');
        }, 4000);
        setTimeout(function() {
            $("#scrolldown1").fadeIn();
        }, 5000);
    }


    //disable nav click
    $("#fp-nav ul li").click(false);

    // remove right class for top nav buttons
    $("#fp-nav").removeClass("right");
    $("#fp-nav").removeAttr("style");

    // click to replay story
    $(".backtotopBtn").click(function() {
        $.fn.fullpage.silentMoveTo(1);
        $("#fp-nav ul li").each(function(i) {
            if (i === 0) {
                $(this).find("span").css({
                    border: "1px solid #ffffff",
                    background: "#ffffff"
                });
            } else {
                $(this).find("span").css({
                    border: "1px solid #ffffff",
                    background: "transparent"
                });
            }
        });
    });
});
