// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);




// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);


// require("../precss/app.css");

var colorAray = [],
    themeTitle = [],
    correctoption = [],
    colour = [],
    count = 0;
//<---------------clue section-------------->


// import xx from "./library/fix-svg-size";


$(function () {
  d3.csv("data/smart-nation-quiz-info.csv", function (data) {

    data.forEach(function (d, i) {
      themeTitle[i] = d.topic;
      correctoption[i] = d.correctoption;
      colour[i] = d.colour;
      d.topic = d.topic;
      d.hint_title = d.hint_title;
      d.hint_text = d.hint_text;
      d.topic_img = "images/" + d.topic_img;

      var hintcontent = '<div class="content-card-info"><div class="infoimg"><img src="' + d.topic_img + '"><h1>' + d.topic +
      '</h1>' + '<h2><b>' + d.hint_title +
      '</b></h2><p>' + d.hint_text + '</p></div></div>';

      $('.clueSection').append(hintcontent);
    });
  });
});

$(function () {
  d3.csv("data/smart-nation-quiz-info.csv", function (data) {

    data.forEach(function (d) {
      d.topic = d.topic;
      d.topic_img = "images/" + d.topic_img;
      d.ques_id = d.ques_id;
      d.ques_img = "images/" + d.ques_img;
      d.quiz = d.quiz;
      d.quiz_img = "images/" + d.quiz_img;
      d.ques_title = d.ques_title;
      d.ques_text = d.ques_text;
      d.opA = d.opA;
      d.opB = d.opB;
      d.opC = d.opC;
      d.answer1 = d.answer1;
      d.answer2 = d.answer2;
      d.answer3 = d.answer3;
      d.opid = d.opid;
      d.opid1 = d.opid1;
      d.opid2 = d.opid2;
      d.img1 = d.img1;
      d.img2 = d.img2;
      d.img3 = d.img3;
      var quizcontent = '  <div class="content-card-quiz"><div class="quizimg"><img src="' + d.quiz_img +
      '"></div><div class="quiztext"><img src="' + d.ques_img + '"><div class="quiztitle"><h1>' + d.topic +
      '</h1></div><hr><div class="title">' + d.ques_title + '<p>' + d.ques_text + '</p></div><div id="' + d.opid +
      '" class="option">' + '<img class= "image" src="images/' + d.img1 + '" ><p>' + d.opA +
      '</p></div>' + '<br>' + '<div id="' + d.opid1 + '" class="option">' +
      '<img class="image" src="images/' + d.img2 + '"><p>' + d.opB +
      '</p></div><br>' + '<div id="' + d.opid2 + '" class="option">' +
      '<img class="image" src="images/' + d.img3 + '"><p>' + d.opC + '</p></div>' +
      '<div id="' + d.topic + '"class="bottom"><div id="bottomtext"></div><div id="bottomtext1"></div><div id="bottomtext2"></div>' +
      '<div id="bottomtext3"></div><hr><div id="continue">continue</div></div>' + '</div></div><br><br><br>';

      $('.quizSection').append(quizcontent);
    });
  });
});



$(function () {
$(document).on("click", '#continue', function () {
  $(".option").prop('disabled', false);
  });
});
//<------button -------->


$(function () {

  $(document).on("click", '.option', function () {
    var tmp = $(this).attr("id");
    $(".option").prop('disabled', true);
    var one = 1;
    var two = 2;
    var three = 3;
    var four = 4;
    var five = 5;

    var optionid = tmp.substring(4);
    var questionid = tmp.substring(2, 3);

    var correct = correctoption[questionid - 1];
    var topic = themeTitle[questionid - 1];
    var colours = colour[questionid - 1];

    if (optionid == correct) {
    $('#' + tmp).addClass('rightAnswer');

      $("#" + topic + " #bottomtext").html("<b>Congratulations!");
      $("#" + topic + " #bottomtext1").html("Your earned the");
      $("#" + topic + " #bottomtext2").html(topic);
      $("#" + topic + " #bottomtext3").html("badge!");
      $("#" + topic + " #continue").html("<p>continue</p>");
      $("#bottomtext2").css("display", "block");
      $("#bottomtext3").css("display", "block");
      $("#" + topic + " #continue").fadeIn();
      $("#" + topic + " #bottomtext2").css("background-color", colours);
      count++;
    } else {
      $('#' + tmp).addClass('wrongAnswer');
      $("#" + topic + " #bottomtext").html("<b>Incorrect!");
      $("#" + topic + " #bottomtext1").html("Please try again!");
      $("#" + topic + " #continue").html("<p>continue</p>");
			$("#" + topic + " #continue").fadeIn();
    }

    if (questionid == one && optionid == correct) {
      $(".badge2").css("-webkit-filter", "none");
      $(".badge2").css(" transition", "all 0.5s ease");
    } else if (questionid == two && optionid == correct) {

      $(".badge4").css("-webkit-filter", "none");
      $(".badge4").css(" transition", "all 0.5s ease");
    } else if (optionid == correct && questionid == three) {
      $(".badge1").css("-webkit-filter", "none");
      $(".badge1").css(" transition", "all 0.5s ease");
    } else if (optionid == correct && questionid == four) {
      $(".badge3").css("-webkit-filter", "none");
      $(".badge3").css(" transition", "all 0.5s ease");
    } else if (optionid == correct && questionid == five) {
      $(".badge5").css("-webkit-filter", "none");
      $(".badge5").css(" transition", "all 0.5s ease");
    }

    if(count== 1) {
		 document.getElementById("endingtext").innerHTML = "Nice, you got one badge.";
	 	 document.getElementById("share").innerHTML ="BRAG";
		 document.getElementById("tryagainbutton").innerHTML ="<h3>TRY FOR MORE</h3>";
 }
	else if(count== 2) {
		document.getElementById("endingtext").innerHTML = "Nice, you got two badge.";
	  document.getElementById("share").innerHTML ="BRAG";
	  document.getElementById("tryagainbutton").innerHTML ="<h3>TRY FOR MORE</h3>";
}
	else if(count== 3) {
		document.getElementById("endingtext").innerHTML = "Nice, you got three badge.";
	  document.getElementById("share").innerHTML ="BRAG";
	  document.getElementById("tryagainbutton").innerHTML ="<h3>TRY FOR MORE</h3>";
}
	else if(count== 4) {
		document.getElementById("endingtext").innerHTML = "Nice, you got four badge.";
	  document.getElementById("share").innerHTML ="BRAG";
	  document.getElementById("tryagainbutton").innerHTML ="<h3>TRY FOR MORE</h3>";
}
	else if(count== 5) {
		document.getElementById("endingtext").innerHTML = "Nice, you got all five badge.";
	  document.getElementById("share").innerHTML ="BRAG";
	  document.getElementById("tryagainbutton").innerHTML ="<h3>FLY AGAIN</h3>";
}
	else{
		document.getElementById("endingtext").innerHTML = "Oh well, the badges aren't all that shiny anyway… or are they?";
		document.getElementById("share").innerHTML ="SHARE… REALLY?";
		document.getElementById("tryagainbutton").innerHTML ="<h3>TRY AGAIN</h3>";
	};

    document.getElementById("score").innerHTML = count;
  });
});
