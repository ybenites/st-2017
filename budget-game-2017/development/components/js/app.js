import $ from "jquery";
import * as d3 from "d3";

var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

var budget_csv = "csv/Budget-2017.csv";
var cashierValue = [0],
    item_value = [],
    item_name = [],
    item_img = [],
    long_name = [],
    long_des = [],
    btnID, baseBar = 100,
    org_revTotal = 100.00,
    org_expTotal = 107.29,
    revTotal = 100,
    expTotal = 107.29,
    putintoCart = "PUT IN CART",
    remove = "REMOVE",
    cartTotal = 0,
    appendExpDiv = [],
    appendRevDiv = [],
    info = [],
    min_money = [],
    max_money = [],
    fb_img = [],
    fb_share = [],
    fb_share2 = [],
    tw_share = [],
    tw_share2 = [],
    summaryHeight = 0;

// get csv data
$(function() {
    d3.csv(budget_csv, function(data) {
        data.forEach(function(d, index) {
            d.item_value = +d.item_value;
            var tmpimgsrc = '<img class="st-image-responsive" src="images/grocery/' + d.img_name + '">';

            // first page, animate items
            if (d.category == "expend") {
                $(".AnimatedImages").append('<img id="grocery_' + index + '" src="images/grocery/' + d.img_name + '">');
                $("#grocery_" + index).addClass("grocery");
                cashierValue[index - 6] = d.item_value;
            }

            // Enter Game, display intro
            $(".introDes").append(d.introduction);

            // Enter Game, dispay Expenditure or Revenue
            if (d.category == "expend") {
                btnID = "expBtn_" + index;

                var $expCell = '<div id="cardExp_' + (index - 6) + '" class="indCard"><div class="indexCard"><strong>' + (index - 5) + '</strong>/6</div><div id="' + btnID + '" class="cartBtn">' + putintoCart + '</div><div id="img_' + index + '" class="cardImg"></div><div id="text_' + index + '" class="cardText"></div></div>';
                $("#expCard").append($expCell);

                $("#img_" + index).append(tmpimgsrc);
                if (d.item_value < 0) {
                    $("#text_" + index).append('<p class="EtextTitle">Expenditure</p><p class="textName">' + d.item_name + '?</p><p class="EtextValue">-$' + (d.item_value * (-1)).toFixed(2) + '</p><p class="textDes">' + d.short_des + '</p>');
                } else {
                    $("#text_" + index).append('<p class="EtextTitle">Expenditure</p><p class="textName">' + d.item_name + '?</p><p class="EtextValue">$' + d.item_value.toFixed(2) + '</p><p class="textDes">' + d.short_des + '</p>');
                }
            } else if (d.category == "revenue") {
                if (d.item_value > 0) baseBar += d.item_value;
                btnID = "revBtn_" + index;

                var $revCell = '<div id="cardRev_' + index + '" class="indCard"><div class="indexCard"><strong>' + (index + 1) + '</strong>/6</div><div id="' + btnID + '" class="cartBtn">' + putintoCart + '</div><div id="img_' + index + '" class="cardImg"></div><div id="text_' + index + '" class="cardText"></div></div>';
                $("#revCard").append($revCell);

                $("#img_" + index).append(tmpimgsrc);
                if (d.item_value < 0) {
                    $("#text_" + index).append('<p class="RtextTitle">Revenue</p><p class="textName">' + d.item_name + '?</p><p class="RtextValue">-$' + (d.item_value * (-1)).toFixed(2) + '</p><p class="textDes">' + d.short_des + '</p>');
                } else {
                    $("#text_" + index).append('<p class="RtextTitle">Revenue</p><p class="textName">' + d.item_name + '?</p><p class="RtextValue">$' + d.item_value.toFixed(2) + '</p><p class="textDes">' + d.short_des + '</p>');
                }

            }

            // Enter summary,
            $(".summaryExplain").append(d.summaryExplain);

            // use putintoCart button id as key, and d.xxx as value
            item_value[btnID] = d.item_value;
            item_name[btnID] = d.item_name;
            item_img[btnID] = tmpimgsrc;
            long_name[btnID] = d.long_name;
            long_des[btnID] = d.long_des;

            // info buttons
            info[index] = d.info_button;

            // final result
            min_money[index] = d.min_money;
            max_money[index] = d.max_money;
            fb_img[index] = d.fb_img;
            fb_share[index] = d.fb_share;
            fb_share2[index] = d.fb_share_2;
            tw_share[index] = d.twitter_share;
            tw_share2[index] = d.twitter_share_2;

        });
        slideGroceries();

        // info buttons
        $("#revInfo strong").after(info[1]);
        $("#expInfo strong").after(info[2]);
        $("#difInfo strong").after(info[0]);

        // to control bar progress
        $("#revBar").width(org_revTotal / baseBar.toFixed(2) * 100 + "%");
        $("#expBar").width(org_expTotal / baseBar.toFixed(2) * 100 + "%");
    });
});

// default value and text
$(function() {
    $(".revValue").html("$" + org_revTotal.toFixed(2));
    $(".expValue").html("$" + org_expTotal.toFixed(2));
    $(".cartValue").html(cartTotal);
    calculateTotal(org_revTotal, org_expTotal);

    if (isMobile === true) {
        $(".instruction").html("Swipe left to browse the items. Use the buttons to switch between revenue and expenditure.");
    } else {
        $(".instruction").html("Use the buttons to switch between revenue and expenditure.");
    }

    $("#expCard").css("display", "none");

    // cashier-value
    var mHeight = $("#machine").height();
    if (isMobile === true) $("#cashier-value").css("bottom", mHeight * 0.79);
    else $("#cashier-value").css("bottom", mHeight * 0.82);


    // hide help icon in mobile
    if (isMobile === true) $(".helpIcon").css("display", "none");
    else $(".hand").css("display", "none");

});


//all click function
$(function() {
    $('.playBtn').click(function() {
        $('.st-header-page').fadeOut();
        stopSlideGroceries();

        // auto scroll to top
        $('body').scrollTop(0);

        // display game page
        if (isMobile === true) $("header").fadeOut();
        $("article").css("display", "block");
        $("article").css("bottom", $("article").height() * (-1));
        $("article").animate({
            bottom: "0"
        }, 1000, "swing");


        //intro
        if (isMobile === true) {
            $(".introTitle").html("<strong>PLAY</strong>");
            $(".beginBtn").html("LET'S BEGIN");
        } else {
            $(".introTitle").html("<strong>HOW TO PLAY</strong>");
            $(".beginBtn").html("CONTINUE");
        }
        $('.intro').css("left", "-100%");
        $('.intro').delay(500).fadeIn();
        $('.intro').animate({
            left: "0"
        }, 300, "swing");

        // hand sliding
        if (isMobile === true) {
            setInterval(function() {
                $(".hand").stop(true, true).animate({
                        left: 100
                    }, 1000,
                    function() {
                        $(this).stop(true, true).animate({
                            left: 0
                        }, 1000);
                    });
            }, 2000);
        }

    });
    $('.beginBtn').click(function() {
        $('.intro').animate({
            left: "-100%"
        }, 1000, "swing");
        // $('.intro').fadeOut();
    });

    // see intro again
    $(".helpIcon").click(function() {
        $('.intro').css("left", "-100%");

        $('.intro').animate({
            left: "0"
        }, 500, "swing");
    });

    // switch btwn exp and rev tag
    $('#expBtn').click(function(e) {
        $('#revCard').css("display", "none");
        $('#expCard').css("display", "block");


        $(this).css({
            color: "#ffffff",
            backgroundColor: "#5A9DDC"
        });
        $('#revBtn').css({
            color: "#333",
            backgroundColor: "#EFF3F4"
        });
    });
    $('#revBtn').click(function(e) {
        $('#expCard').css("display", "none");
        $('#revCard').css("display", "block");

        $(this).css({
            color: "#ffffff",
            backgroundColor: "#DBB205"
        });
        $('#expBtn').css({
            color: "#333",
            backgroundColor: "#EFF3F4"
        });
    });

    // Add or remove items in cart
    $(document).on("click", '.cartBtn', function() {
        var getID = $(this).attr("id");
        // Expenditure
        if (getID.indexOf("exp") != -1) {
            if ($(this).text() == putintoCart) {
                $(this).html(remove);
                $(this).css({
                    backgroundColor: "#999999",
                    color: "#ffffff"
                });
                expTotal = expTotal + item_value[getID];
                cartTotal++;
                $(".expCart").append('<div id="cart_' + getID + '" class="cartCard"></div>');
                if (item_value[getID] < 0) $("#cart_" + getID).append('<div class="cartImg">' + item_img[getID] + '</div><div class="cartText"><p>' + item_name[getID] + '</p><strong>-$' + (item_value[getID] * (-1)).toFixed(2) + '</strong></div><div class="removeItem"></div>');
                else $("#cart_" + getID).append('<div class="cartImg">' + item_img[getID] + '</div><div class="cartText"><p>' + item_name[getID] + '</p><strong>$' + (item_value[getID]).toFixed(2) + '</strong></div><div class="removeItem"></div>');

                $(".removeItem").html('<img src="images/icons/remove-item-icon.svg" class="st-image-responsive">');

            } else if ($(this).text() == remove) {
                $(this).html(putintoCart);
                $(this).css({
                    backgroundColor: "#ffffff",
                    color: "#999999"
                });
                expTotal = expTotal - item_value[getID];
                cartTotal--;
                $(".cartCard").each(function() {
                    if ($(this).attr("id") == "cart_" + getID) {
                        $(this).remove();
                    }
                });
            }
            $(".expValue").html("$" + expTotal.toFixed(2));
            $("#expBar").width(expTotal / baseBar.toFixed(2) * 100 + "%");

            // Revenue
        } else if (getID.indexOf("rev") != -1) {
            if ($(this).text() == putintoCart) {
                $(this).html(remove);
                $(this).css({
                    backgroundColor: "#999999",
                    color: "#ffffff"
                });
                revTotal = revTotal + item_value[getID];
                cartTotal++;
                $(".revCart").append('<div id="cart_' + getID + '" class="cartCard"></div>');
                if (item_value[getID] < 0) $("#cart_" + getID).append('<div class="cartImg">' + item_img[getID] + '</div><div class="cartText"><p>' + item_name[getID] + '</p><strong>-$' + (item_value[getID] * (-1)).toFixed(2) + '</strong></div><div class="removeItem"></div>');
                else $("#cart_" + getID).append('<div class="cartImg">' + item_img[getID] + '</div><div class="cartText"><p>' + item_name[getID] + '</p><strong>$' + (item_value[getID]).toFixed(2) + '</strong></div><div class="removeItem"></div>');

                $(".removeItem").html('<img src="images/icons/remove-item-icon.svg" class="st-image-responsive">');


            } else if ($(this).text() == remove) {
                $(this).html(putintoCart);
                $(this).css({
                    backgroundColor: "#ffffff",
                    color: "#999999"
                });
                revTotal = revTotal - item_value[getID];
                cartTotal--;
                $(".cartCard").each(function() {
                    if ($(this).attr("id") == "cart_" + getID) {
                        $(this).remove();
                    }
                });

            }
            $(".revValue").html("$" + revTotal.toFixed(2));
            $("#revBar").width(revTotal / baseBar.toFixed(2) * 100 + "%");
        }

        calculateTotal(revTotal, expTotal);
        checkHasChildren();
        $(".cartValue").html(cartTotal);

    });

    /////// END Add or remove items in cart /////////

    // show cart summary
    $("#checkcartBtn").click(function() {
        $(this).css("display", 'none');
        $("#viewClose").css("display", "block");
        $("#closecartBtn").css("display", "block");
        if (isMobile === false) {
            $('.Game').animate({
                'marginLeft': '-10%'
            }, 500);
        }


        var winHeight = $(window).height();
        var headerHeight = $(".st-content-menu").height();
        if (isMobile === true) $('.cartSummary').height(winHeight - headerHeight);
        else $('.cartSummary').height(winHeight - headerHeight - 50 + 2);
        $('.cartSummary').css("display", "block");
        $('.cartSummary').animate({
            right: "0"
        }, 500, "swing");

        checkHasChildren();

    });

    // close cart summary
    $("#closecartBtn").click(function() {
        var centerMargin = ($(window).width() / 2) - ($('.Game').width() / 2) + 'px';
        $(this).css("display", 'none');
        $("#checkcartBtn").css("display", "block");
        if (isMobile === false) {
            $('.Game').animate({
                'marginLeft': centerMargin
            }, 500);
            $('.cartSummary').animate({
                right: "-320px"
            }, 500, "swing", function() {
                $('.cartSummary').css("display", "none");
            });
        } else {
            $('.cartSummary').animate({
                right: "-100%"
            }, 500, "swing", function() {
                $('.cartSummary').css("display", "none");
            });
        }

    });

    // remove individual item from cart list
    $(document).on("click", '.removeItem', function() {
        var _this = $(this);
        _this.html('<img src="images/icons/remove-item-icon-tapped.svg" class="st-image-responsive">');
        var cardBtnID = $(this).parent().attr("id").substring(5);
        var tmpValue = item_value[cardBtnID];
        cartTotal -= 1;

        $(".cartValue").html(cartTotal);
        setTimeout(function() {
            _this.parent().remove();
            checkHasChildren();
        }, 100);

        $("#" + cardBtnID).html(putintoCart);
        $("#" + cardBtnID).css({
            backgroundColor: "#ffffff",
            color: "#C0CAC9"
        });

        if (cardBtnID.indexOf("rev") != -1) {
            revTotal = revTotal - tmpValue;
        } else if (cardBtnID.indexOf("exp") != -1) {
            expTotal = expTotal - tmpValue;
        }

        $(".revValue").html("$" + revTotal.toFixed(2));
        $("#revBar").width(revTotal / baseBar.toFixed(2) * 100 + "%");
        $(".expValue").html("$" + expTotal.toFixed(2));
        $("#expBar").width(expTotal / baseBar.toFixed(2) * 100 + "%");
        calculateTotal(revTotal, expTotal);

    });

    // remove all items from cart list
    $(".clearItems").click(function() {
        revTotal = org_revTotal;
        expTotal = org_expTotal;
        calculateTotal(revTotal, expTotal);
        cartTotal = 0;

        $(".revCart").children(".cartCard").remove();
        $(".expCart").children(".cartCard").remove();

        $(".cartValue").html(cartTotal);
        $(".revValue").html("$" + revTotal.toFixed(2));
        $("#revBar").width(revTotal / baseBar.toFixed(2) * 100 + "%");
        $(".expValue").html("$" + expTotal.toFixed(2));
        $("#expBar").width(expTotal / baseBar.toFixed(2) * 100 + "%");
        $(".indCard").each(function() {
            var $this = $(this).children(".cartBtn");
            $this.html(putintoCart);
            $this.css({
                backgroundColor: "#ffffff",
                color: "#C0CAC9"
            });
        });

        checkHasChildren();
    });

    /////// END cart content ///////

    //// click for more info //////

    $("#infoRev").click(function() {
        $(".ExtraInfo").css("display", "none");
        $(".infoIcon").attr("src", "images/icons/info-icon.svg");
        $(this).attr("src", "images/icons/info-icon-tapped.svg");
        $("#revInfo").fadeIn();
    });
    $("#infoExp").click(function() {
        $(".ExtraInfo").css("display", "none");
        $(".infoIcon").attr("src", "images/icons/info-icon.svg");
        $(this).attr("src", "images/icons/info-icon-tapped.svg");
        $("#expInfo").fadeIn();
    });
    $("#infoDif").click(function() {
        $(".ExtraInfo").css("display", "none");
        $(".infoIcon").attr("src", "images/icons/info-icon.svg");
        $(this).attr("src", "images/icons/info-icon-tapped.svg");
        $("#difInfo").fadeIn();
    });

    // close info windows
    $(".closeBtn").click(function() {
        $(".infoIcon").attr("src", "images/icons/info-icon.svg");
        $(".ExtraInfo").fadeOut();
    });

    //// END more info ////////

    // go back to select page
    $(".revise").click(function() {
        $(this).children("img").attr('src', 'images/icons/up-icon-tapped.svg');

        var centerMargin = ($(window).width() / 2) - ($('.Game').width() / 2) + 'px';
        $(".summaryReport").animate({
            bottom: $(".summaryReport").height() * (-1)
        }, 500, "swing", function() {
            $(".summaryReport").css("display", "none");
            $('footer').css("display", "none");

        });

        setTimeout(function() {

            $(".Game").fadeIn(300);
            if (isMobile === true) {
                $(".Game").css("margin-left", "0");
                $(".st-content-menu").css("display", "none");
            } else {
                $(".Game").css("margin-left", centerMargin);
                $(".helpIcon").css("display", "block");
            }
            $("#checkcartBtn").css("display", "block");

        }, 400);

    });

    // show receipt
    $(".showRec").click(function() {
        $(".receipt").fadeIn();
        var sID, shortsID, tmpTotal,
            tmprev = org_revTotal,
            tmpexp = org_expTotal;

        $("#orgRev").html("$" + org_revTotal.toFixed(2));
        $("#orgExp").html("$" + org_expTotal.toFixed(2));
        $(".receipt_rev_list").empty();
        $(".receipt_exp_list").empty();
        $(".summaryItem").each(function() {
            sID = $(this).attr("id");
            shortsID = sID.substring(8);
            if (sID.indexOf("rev") != -1) {
                if (item_value[shortsID] < 0) {
                    $(".receipt_rev_list").append('<div class="list_text">' + item_name[shortsID] + '</div><div class="list_value">-$' + (item_value[shortsID] * (-1)).toFixed(2) + '</div>');
                } else {
                    $(".receipt_rev_list").append('<div class="list_text">' + item_name[shortsID] + '</div><div class="list_value">+$' + item_value[shortsID].toFixed(2) + '</div>');
                }
                tmprev += item_value[shortsID];
            } else if (sID.indexOf("exp") != -1) {
                if (item_value[shortsID] < 0) {
                    $(".receipt_exp_list").append('<div class="list_text">' + item_name[shortsID] + '</div><div class="list_value">-$' + (item_value[shortsID] * (-1)).toFixed(2) + '</div>');
                } else {
                    $(".receipt_exp_list").append('<div class="list_text">' + item_name[shortsID] + '</div><div class="list_value">+$' + item_value[shortsID].toFixed(2) + '</div>');
                }
                tmpexp += item_value[shortsID];
            }
        });

        $("#subRev").html("$" + tmprev.toFixed(2));
        $("#subExp").html("$" + tmpexp.toFixed(2));
        tmpTotal = tmprev - tmpexp;
        $(".GTotal_value").html("<strong>$" + tmpTotal.toFixed(2) + "</strong>");

        if (isMobile === true) {
            var fHeight = $("footer").height();
            $(".receipt").css("margin-bottom", fHeight);
            $(".underBigImage").css("margin-bottom", "20px");
            $("body").animate({
                scrollTop: $(document).height()
            }, 1000);
        }
    });

    // close receipt
    $(".infoClose").click(function() {
        $(".receipt").fadeOut(500);
        if (isMobile === true) {
            var ffHeight = $("footer").height();
            var thrbHeight = $(".threeButtons").height();
            $(".underBigImage").height(thrbHeight + 30);
            $(".underBigImage").css("margin-bottom", ffHeight - 20);
            $("body").animate({
                scrollTop: summaryHeight * 1.7
            }, 500);
        }
    });

});

var interval = setInterval(slideGroceries, 10000);

function slideGroceries() {

    // display how much on the cashier machine
    $("#cashier-value span").html("$0");
    setTimeout(function() {
        $.each(cashierValue, function(key, value) {
            setTimeout(function() {
                if (value > 0) $("#cashier-value span").html("$" + value.toFixed(2));
                else $("#cashier-value span").html("-$" + (value * (-1)).toFixed(2));
            }, key * 1000);
        });
    }, 4000);

    // slide groceries on the cashier counter
    var $this = $('.grocery');
    $this.css({
        'display': 'none',
        'right': '5%'
    });
    $this.each(function(i) {
        $(this).delay(i++ * 1000).fadeIn();
        $(this).animate({
            right: "80%"
        }, 4000, "swing");
        $(this).fadeOut();
    });
}

function stopSlideGroceries() {
    clearInterval(interval);
}

function checkOutClick() {
    var tmpID, shortmpID;

    $(".revise").children("img").attr('src', 'images/icons/up-icon.svg');

    // hide previous page
    $(".cartSummary").css("display", "none");
    $(".Game").fadeOut();
    $("#checkcartBtn").fadeOut();

    //hide help icons
    $(".helpIcon").css("display", "none");

    // display summary page
    $(".st-content-menu").fadeIn();
    $(".summaryReport").css("display", "block");
    $(".summaryReport").css("bottom", $(".summaryReport").height() * (-1));
    $(".summaryReport").animate({
        bottom: "0"
    }, 1000, "swing", function() {
        $('footer').css("display", "block");
    });
    summaryHeight = $(".summaryReport").height();

    // auto scroll to top
    $('body').scrollTop(0);

    // 3 icons value
    $("#originalRev").html("$" + revTotal.toFixed(2));
    $("#originalExp").html("$" + expTotal.toFixed(2));
    var tmp = revTotal - expTotal;
    if (tmp < 0) $("#originalSup").html("-$" + (tmp * (-1)).toFixed(2));
    else $("#originalSup").html("+$" + tmp.toFixed(2));


    // summary item list
    $(".summaryItem").remove();
    $(".cartCard").each(function() {
        tmpID = $(this).attr("id");
        shortmpID = tmpID.substring(5);
        if (tmpID.indexOf("exp") != -1) {
            $(".expList").append('<div id="summary_' + shortmpID + '" class="summaryItem"></div>');
            $("#summary_" + shortmpID).append('<div class="summaryImg">' + item_img[shortmpID] + '</div><div class="summaryText"><strong>' + long_name[shortmpID] + '</strong>' + long_des[shortmpID] + '</div>');
        } else if (tmpID.indexOf("rev") != -1) {
            $(".revList").append('<div id="summary_' + shortmpID + '" class="summaryItem"></div>');
            $("#summary_" + shortmpID).append('<div class="summaryImg">' + item_img[shortmpID] + '</div><div class="summaryText"><strong>' + long_name[shortmpID] + '</strong>' + long_des[shortmpID] + '</div>');
        }
    });

    //Big image src
    if (isMobile === true) {
        $(".BigImage").html('<img src="images/icons/checkout-counter-M-420W.svg" class="st-image-responsive" alt="">');
        var ffHeight = $("footer").height();
        var thrbHeight = $(".threeButtons").height();
        $(".underBigImage").height(thrbHeight + 30);
        $(".underBigImage").css("margin-bottom", ffHeight - 20);

    } else {
        $(".BigImage").html('<img src="images/icons/summary-counter-graphic.svg" class="st-image-responsive" alt="">');
    }


    // final result
    for (var i = 0; i < 3; i++) {
        if (min_money[i] <= tmp && tmp <= max_money[i]) {
            $("#fbText").html(fb_share[i] + " $" + tmp.toFixed(2) + ". " + fb_share2[i]);
            $("#fbImg").html("images/" + fb_img[i]);
            $("#twText").html(tw_share[i] + " $" + tmp.toFixed(2) + ". " + tw_share2[i]);
            return;
        }
    }
}

function calculateTotal(r, e) {
    var tmpTotal = r - e;

    // when surplus
    if (tmpTotal >= 0) {
        $("#totalValue").html("<strong>+$" + tmpTotal.toFixed(2) + "</strong>");
        $("#totalValue").css("color", "#68A94D");
        $("#totalResult").html("Surplus");
        $("#totalResult").css("color", "#333");
        $(".checkoutBtn").css({
            backgroundColor: "#333",
            cursor: "pointer"
        });
        $('.checkoutBtn').click(checkOutClick);

        // when deficit
    } else {
        $("#totalValue").html("<strong>-$" + (tmpTotal * -1).toFixed(2) + "</strong>");
        $("#totalValue").css("color", "#F44E4E");
        $("#totalResult").html("Deficit");
        $("#totalResult").css("color", "#F44E4E");
        $(".checkoutBtn").css({
            backgroundColor: "#DAE3E5",
            cursor: "default"
        });
        $('.checkoutBtn').unbind("click", checkOutClick);
    }
}

function checkHasChildren() {
    // if there are items then show the title and clear button
    if ($(".revCart").children().length > 1) {
        $(".revCart").children("span").css("display", "block");
    } else {
        $(".revCart").children("span").css("display", "none");
    }

    if ($(".expCart").children().length > 1) {
        $(".expCart").children("span").css("display", "block");
    } else {
        $(".expCart").children("span").css("display", "none");
    }

    if ($(".revCart").children().length > 1 || $(".expCart").children().length > 1) {
        $(".clearItems").css("display", "block");
    } else {
        $(".clearItems").css("display", "none");
    }
}
