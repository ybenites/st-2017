import $ from "jquery";

import swipe from "jquery-touchswipe";

var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

var Lwidth = "296px",
    Lheight = "210px",
    oldwidth = "260px",
    oldheight = "190px",
    oldMarginTop = "10px",
    swipeDist = "-230px",
    oldcartBtnMargin = "6px 0 6px 144px",
    newcartBtnMargin = "10px 0px 15px 175px",
    oldtextNameSize = "18px",
    oldtextValueSize = "22px",
    newtextNameSize = "20px",
    newtextValueSize = "24px";

// swiper in mobile
$(function() {
    if (isMobile === true) {
        $(".playBtn").click(function() {

            //move the last list item before the first item. The purpose of this is if the user clicks previous he will be able to see the last item.
            $("#expCard .indCard:first").before($("#expCard .indCard:last"));
            $("#revCard .indCard:first").before($("#revCard .indCard:last"));

            // for (var i = 0; i < $("#revCard .indCard").length; i++) {
            //     $("#revBullets").append('<div class="indBullet" id="rbu_' + i + '"></div>');
            // }
            // for (var ii = 0; ii < $("#expCard .indCard").length; ii++) {
            //     $("#expBullets").append('<div class="indBullet" id="ebu_' + ii + '"></div>');
            // }

            revBullet();
            $('#revBtn').click(revBullet);
            $('#expBtn').click(expBullet);
        });
    }
});

function revBullet() {

    // to show swiping bullets
    var tmpid = $("#revCard > .indCard:eq(1)").attr("id");
    // alert(tmpid);
    tmpid = tmpid[tmpid.length - 1];
    $(".indBullet").css("background-color", "#DDDDDD");
    $("#rbu_" + tmpid).css("background-color", "#999999");
    // $("#expBullets").css("display", "none");
    // $("#revBullets").fadeIn();

    // make centered card large
    revlargeNoAnimate(1);

    $("#revCarousel_section").swipe({
        //Single swipe handler for right swipes
        swipeRight: function(event, direction, distance, duration, fingerCount) {

            // remove hand sliding
            $("#revCarousel_section .hand").remove();
            //get the width of the items
            var item_width = $('#revCard > .indCard').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#revCard').css('left')) + item_width;

            $('#revCard').animate({
                'left': left_indent
            }, 400, "swing", function() {
                /* when sliding to left we are moving the last item before the first item */
                $("#revCard > .indCard:first").before($("#revCard > .indCard:last"));
                // $("#revCard").prepend($("#revCard .indCard:last"));

                /* and again, when we make that change we are setting the left indent of our unordered list to the default -240px */
                $('#revCard').css({
                    'left': swipeDist
                });

                // highlight bullets
                // var tmpid = $("#revCard > .indCard:eq(1)").attr("id");
                // tmpid = tmpid[tmpid.length - 1];
                // $(".indBullet").css("background-color", "#DDDDDD");
                // $("#rbu_" + tmpid).css("background-color", "#999999");

            });
            // make centered card large
            revCardContent(0);

        },
        //Single swipe handler for left swipes
        swipeLeft: function(event, direction, distance, duration, fingerCount) {

            // remove hand sliding
            $("#revCarousel_section .hand").remove();

            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#revCard > .indCard').outerWidth() + 10;

            //calculate the new left indent of the unordered list
            var left_indent = parseInt($('#revCard').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('#revCard').animate({
                'left': left_indent
            }, 400, "swing", function() {
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $("#revCard > .indCard:last").after($("#revCard > .indCard:first"));

                //and get the left indent to the default -240px
                $('#revCard').css({
                    'left': swipeDist
                });

                // highlight bullets
                // var tmpid = $("#revCard > .indCard:eq(1)").attr("id");
                // tmpid = tmpid[tmpid.length - 1];
                // $(".indBullet").css("background-color", "#DDDDDD");
                // $("#rbu_" + tmpid).css("background-color", "#999999");


            });
            // make centered card large
            revCardContent(2);

        }
    });

}

function expBullet() {

    // to show swiping bullets
    var tmpid = $("#expCard > .indCard:eq(1)").attr("id");
    tmpid = tmpid[tmpid.length - 1];
    $(".indBullet").css("background-color", "#DDDDDD");
    $("#ebu_" + tmpid).css("background-color", "#999999");
    // $("#expBullets").css("display", "none");
    // $("#revBullets").fadeIn();

    // to show swiping bullets
    // $("#revBullets").css("display", "none");
    // $("#expBullets").fadeIn();

    // make centered card large
    explargeNoAnimate(1);

    $("#expCarousel_section").swipe({
        //Single swipe handler for right swipes
        swipeRight: function(event, direction, distance, duration, fingerCount) {

            // remove hand sliding
            $("#revCarousel_section .hand").remove();

            //get the width of the items
            var item_width = $('#expCard > .indCard').outerWidth() + 10;

            /* same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width) */
            var left_indent = parseInt($('#expCard').css('left')) + item_width;

            $('#expCard').animate({
                'left': left_indent
            }, 400, "swing", function() {
                /* when sliding to left we are moving the last item before the first item */
                $("#expCard > .indCard:first").before($("#expCard > .indCard:last"));

                /* and again, when we make that change we are setting the left indent of our unordered list to the default -240px */
                $('#expCard').css({
                    'left': swipeDist
                });

                // highlight bullets
                // var tmpid = $("#expCard > .indCard:eq(1)").attr("id");
                // tmpid = tmpid[tmpid.length - 1];
                // $(".indBullet").css("background-color", "#DDDDDD");
                // $("#ebu_" + tmpid).css("background-color", "#999999");



            });
            // make centered card large
            expCardContent(0);

        },
        //Single swipe handler for left swipes
        swipeLeft: function(event, direction, distance, duration, fingerCount) {

            // remove hand sliding
            $("#revCarousel_section .hand").remove();

            //get the width of the items ( i like making the jquery part dynamic, so if you change the width in the css you won't have o change it here too ) '
            var item_width = $('#expCard > .indCard').outerWidth() + 10;

            //calculate the new left indent of the unordered list
            var left_indent = parseInt($('#expCard').css('left')) - item_width;

            //make the sliding effect using jquery's anumate function '
            $('#expCard').animate({
                'left': left_indent
            }, 400, "swing", function() {
                //get the first list item and put it after the last list item (that's how the infinite effects is made) '
                $("#expCard > .indCard:last").after($("#expCard > .indCard:first"));

                //and get the left indent to the default -240px
                $('#expCard').css({
                    'left': swipeDist
                });

                // highlight bullets
                // var tmpid = $("#expCard > .indCard:eq(1)").attr("id");
                // tmpid = tmpid[tmpid.length - 1];
                // $(".indBullet").css("background-color", "#DDDDDD");
                // $("#ebu_" + tmpid).css("background-color", "#999999");



            });
            // make centered card large
            expCardContent(2);

        }
    });
}

function expCardContent(index) {

    // the rest remains the same
    $("#expCard > .indCard").each(function() {
        $(this).css({
            width: oldwidth,
            height: oldheight,
            marginTop: oldMarginTop
        });
    });
    $("#expCard .indCard > .cartBtn").css("margin", oldcartBtnMargin);
    $("#expCard .indCard > .cardText > .textName").css("font-size", oldtextNameSize);
    $("#expCard .indCard > .cardText > .EtextValue").css("font-size", oldtextValueSize);


    // make centered card large
    $("#expCard .indCard:eq(" + index + ")").animate({
        width: Lwidth,
        height: Lheight,
        marginTop: 0
    });
    $("#expCard .indCard:eq(" + index + ") > .cartBtn").css("margin", newcartBtnMargin);
    $("#expCard .indCard:eq(" + index + ") > .cardText > .textName").css("font-size", newtextNameSize);
    $("#expCard .indCard:eq(" + index + ") > .cardText > .EtextValue").css("font-size", newtextValueSize);

}

function revCardContent(index) {

    // the rest remains the same
    $("#revCard > .indCard").each(function() {
        $(this).css({
            width: oldwidth,
            height: oldheight,
            marginTop: oldMarginTop
        });
    });
    $("#revCard .indCard > .cartBtn").css("margin", oldcartBtnMargin);
    $("#revCard .indCard > .cardText > .textName").css("font-size", oldtextNameSize);
    $("#revCard .indCard > .cardText > .RtextValue").css("font-size", oldtextValueSize);


    // make centered card large
    $("#revCard .indCard:eq(" + index + ")").animate({
        width: Lwidth,
        height: Lheight,
        marginTop: 0
    });
    $("#revCard .indCard:eq(" + index + ") > .cartBtn").css("margin", newcartBtnMargin);
    $("#revCard .indCard:eq(" + index + ") > .cardText > .textName").css("font-size", newtextNameSize);
    $("#revCard .indCard:eq(" + index + ") > .cardText > .RtextValue").css("font-size", newtextValueSize);
}

function revlargeNoAnimate(index) {
    // make centered card large
    $("#revCard .indCard:eq(" + index + ")").css({
        width: Lwidth,
        height: Lheight,
        marginTop: 0
    });
    $("#revCard .indCard:eq(" + index + ") > .cartBtn").css("margin", newcartBtnMargin);
    $("#revCard .indCard:eq(" + index + ") > .cardText > .textName").css("font-size", newtextNameSize);
    $("#revCard .indCard:eq(" + index + ") > .cardText > .RtextValue").css("font-size", newtextValueSize);
}

function explargeNoAnimate(index) {
    $("#expCard .indCard:eq(" + index + ")").css({
        width: Lwidth,
        height: Lheight,
        marginTop: 0
    });
    $("#expCard .indCard:eq(" + index + ") > .cartBtn").css("margin", newcartBtnMargin);
    $("#expCard .indCard:eq(" + index + ") > .cardText > .textName").css("font-size", newtextNameSize);
    $("#expCard .indCard:eq(" + index + ") > .cardText > .EtextValue").css("font-size", newtextValueSize);
}
