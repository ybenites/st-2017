"use strict";

var gulp = require('gulp'),
  gutil = require('gulp-util'),
  streamify = require('gulp-streamify'),
  browserify = require('browserify'),
  compass = require('gulp-compass'),
  connect = require('gulp-connect'),
  open = require("gulp-open"),
  gulpif = require('gulp-if'),
  uglify = require('gulp-uglify'),
  htmlmin = require('gulp-htmlmin'),
  source = require('vinyl-source-stream'),
  path = require('path'),
  ftp = require('vinyl-ftp'),
  // reactify = require('reactify'),
  babelify = require("babelify"),
  lint = require('gulp-eslint'),
  imagemin = require('gulp-imagemin'),
  infoserver = require('../infoserver.json');

var env,
  jsSources,
  sassSources,
  htmlSources,
  outputDir,
  sassStyle, jsLibrary;

// values for ENV development,test,production
env = 'development';

var env2 = env;
env = (env === "test" || env === "production") ? "production" : "development";
if (env === 'development') {
  outputDir = 'development/';
  sassStyle = 'expanded';
} else {
  outputDir = 'production/';
  sassStyle = 'compressed';
}

/*Start define all html, scss and js files*/
jsSources = [
  'development/components/js/functions.js',
  'development/components/js/app.js'
];

// jsLibrary = [''];
jsLibrary = ['development/components/js/vendor.js'];

sassSources = ['development/components/scss/app.scss', 'development/components/scss/components.scss', 'development/components/scss/stcssie.scss'];
htmlSources = [outputDir + '*.html'];
/*End define all html, scss and js files*/


gulp.task('js', function() {
  var b = browserify();
  b.add(jsSources);
  // b.transform(reactify)

  b
    .external('jquery')
    .external("d3");
  // .external('scrollmagic')
  // .external('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap');


  b.transform(babelify, {
      presets: ["es2015", "react"],
      plugins: ["transform-object-assign"]
    })
    .bundle()
    .pipe(source('app.js'))
    .on('error', console.error.bind(console))
    .pipe(gulpif(env === 'production', streamify(uglify())))
    .pipe(gulp.dest(outputDir + 'js'))
    .pipe(connect.reload());


});


gulp.task('library', function() {
  var b = browserify();

  b
    .require('jquery')
    .require("d3");
  // .require('scrollmagic')
  // .require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap');

  b.transform(babelify, {
      presets: ["es2015", "react"]
    })
    .bundle()
    .pipe(source('components.js'))
    .on('error', console.error.bind(console))
    .pipe(gulpif(env === 'production', streamify(uglify())))
    .pipe(gulp.dest(outputDir + 'js'))
    .pipe(connect.reload());

});

gulp.task('compass', function() {
  var path_font = (env2 === "development" || env2 === "test") ? "/" : "http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/commons/";
  gulp.src(sassSources)
    .pipe(compass({
        sass: 'development/components/scss',
        css: outputDir + 'css',
        image: outputDir + 'images',
        style: sassStyle,
        // font: outputDir + 'fonts',
        font: 'fonts',
        http_path: path_font,
        relative: false,
        require: ['susy', 'breakpoint']
      })
      .on('error', gutil.log))
    //    .pipe(gulp.dest( outputDir + 'css'))
    .pipe(connect.reload());
});


gulp.task('lint', function() {
  return gulp.src(outputDir + 'js')
    .pipe(lint({
      config: 'eslint.config.json'
    }))
    .pipe(lint.format());
});

gulp.task('watch', function() {
  gulp.watch(jsSources, ['js', 'lint']);
  gulp.watch(['development/components/scss/*.scss', 'development/components/scss/*/*.scss'], ['compass']);
  gulp.watch('development/*.html', ['html']);
  // gulp.watch('../production/*', ['deploy']);
});

gulp.task('connect', function() {
  connect.server({
    root: outputDir,
    livereload: true,
    port: 8080
  });
});


gulp.task('open', function() {
  var o = "http://localhost/2017/" + path.parse(__dirname).base + "/" + outputDir;
  gulp.src(outputDir + '/index.html')
    .pipe(open({
      uri: o
    }));
});


gulp.task('html', function() {
  gulp.src('development/*.html')
    .pipe(gulpif(env === 'production', htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      collapseInlineTagWhitespace: true
    })))
    .pipe(gulpif(env === 'production', gulp.dest(outputDir)))
    .pipe(connect.reload());
});

gulp.task('svg', function() {
  gulp.src('development/svg/**/*.svg')
    .pipe(gulpif(env === 'production', htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      collapseInlineTagWhitespace: true
    })))
    .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'svg')))
    .pipe(connect.reload());
});

// Copy images to production
gulp.task('move', function() {
  // gulp.src('./../fonts/**/*.*')
  //     .pipe(gulpif(env === 'development',gulp.dest(outputDir + 'fonts')));

  gulp.src('development/images/**/*.*')
    .pipe(gulpif(env === 'production', imagemin()))
    .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'images')));
  gulp.src('development/csv/**/*.*')
    .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'csv')));
  // gulp.src('development/svg/**/*.*')
  //     .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'svg')));
  gulp.src('development/videos/**/*.*')
    .pipe(gulpif(env === 'production', gulp.dest(outputDir + 'videos')));
});

gulp.task('deploy', function() {

  var conn = ftp.create({
    host: infoserver.servers.development.serverhost,
    user: infoserver.servers.development.username,
    password: infoserver.servers.development.password,
    log: gutil.log
  });

  var globs = [
    outputDir + '*.html',
    outputDir + 'css/*',
    outputDir + 'csv/*',
    outputDir + 'fonts/**/*',
    outputDir + 'images/**/*',
    outputDir + 'js/*',
    outputDir + 'svg/*',
    outputDir + 'videos/*',
    outputDir + 'babylon/*'
  ];

  return gulp.src(globs, {
      base: outputDir,
      buffer: false
    })
    .pipe(conn.newerOrDifferentSize('infographics/' + path.parse(__dirname).base)) // only upload newer files
    .pipe(conn.dest('infographics/' + path.parse(__dirname).base));

  // .pipe(conn.newerOrDifferentSize('infographics/' + path.parse(path.dirname(path.normalize(__dirname))).base)) // only upload newer files
  // .pipe(conn.dest('infographics/' + path.parse(path.dirname(path.normalize(__dirname))).base));
});


gulp.task('default', ['watch', 'html', 'svg', 'js', 'library', 'lint', 'compass', 'move', 'open']);
