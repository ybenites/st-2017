"use strict";
var React = require('react');
var Section1 = require('../section/section1');


var Article = React.createClass({
    render: function() {
        return (
        	<article>
        		<Section1 />
        		<section className="credits-section">
        		</section>
        		<section className="content-fb-comments">
                    <div className="fb-comments" data-href="http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/02/budget-2017-statement/index.html" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
        		</section>
        	</article>
        );
    }
});

module.exports = Article;
