"use strict";
var React = require('react');
var $=require('jquery');


var Footer = React.createClass({
	getInitialState:function(){
		return {
			height_footer:0
		}
	},
	handleResize:function(){
		var height_footer = $("footer.st-content-footer").outerHeight();
    	$("body").css("margin", "0 0 " + height_footer + "px");
	},
	componentDidMount:function(){
		var height_footer = $("footer.st-content-footer").outerHeight();
		$("body").css("margin", "0 0 " + height_footer + "px");
		$(window).on('resize',this.handleResize);
	},
	componentWillUnmount: function() {
		 $(window).off('resize',this.handleResize);
	},
    render: function() {
        return (
        	<footer className="st-content-footer">
        		<p className="st-footer">
        			SPH Digital News / Copyright © 2017 Singapore Press Holdings Ltd. Co. Regn No. 198402868E. All rights reserved.
        		</p>
        	</footer>
        );
    }
});

module.exports = Footer;
