"use strict";
var React = require('react');

var Banner=require('../head/banner');
var Headline=require('../head/headline');


var Head = React.createClass({
    render: function() {
        return ( 
        	<header>
        		<Banner />
        		<Headline />
        	</header>
        );
    }
});

module.exports = Head;
