"use strict";
var React = require('react');
var $ = require('jquery');
var jQuery = $;
var d3 = require('d3');


var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
    FB.init({
        appId: fbAppId, // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the
        // server to access the session
        xfbml: true, // parse page for xfbml or html5
        // social plugins like login button below
        version: 'v2.0', // Specify an API version
    });

    // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



var Menu = React.createClass({
    getInitialState: function() {
        return {
            items_menu: [ {
                url: 'http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/02/budget-2017-glance/index.html',
                name: 'AT A GLANCE',
                active: 0
            },{
                url: '',
                name: 'THE STATEMENT',
                active: 1
            }, {
                url: 'http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/02/singapore-budget-revenue-and-spending-breakdown-2017/index.html',
                name: 'REVENUE & SPENDING',
                active: 0
            }, {
                url: 'http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/02/Budget-2017-transformations/index.html',
                name: 'HISTORICAL',
                active: 0
            }, , {
                url: 'http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/02/budget-2017-whats-on-your-wish-list/index.html',
                name: 'WISH LIST',
                active: 0
            }, {
                url: 'http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/02/budget-2017-live-blog/index.html',
                name: 'LIVE BLOG',
                active: 0
            }]
        }
    },
    render: function() {
        return ( <
            header role = "banner"
            className = "st-content-menu" >
            <
            Menu_desktop items_menu = {
                this.state.items_menu
            }
            /> <
            Menu_mobile items_menu = {
                this.state.items_menu
            }
            /> < /
            header >
        );
    }
});

var Menu_desktop = React.createClass({
    render: function() {
        var li_menu = [];
        var target = ""
        this.props.items_menu.forEach(function(d, i) {
            var active = (d.active === 1) ? 'current' : '';
            if (d.active !== 1) target = "_self";
            li_menu.push( < li key = {
                    i
                } > < a href = {
                    d.url
                }
                target = {
                    target
                }
                className = {
                    "st_option_menu_desktop " + active
                }
                title = {
                    d.name
                } > {
                    d.name
                } < /a></li > );
        });

        return ( <nav role = "navigation"
            className = "st-content-menu-fixed" >
            <div className = "navbar" >
            <figure className = "st-logo"
            itemProp = "associatedMedia image"
            itemScope itemType = "http://schema.org/ImageObject"
            data-component = "image" >
            <a href = "http://www.straitstimes.com/"
            target = "_blank" >
            <img src = "images/st-logo.svg"
            className = "st-image-responsive"
            alt = "Straitstimes logo"
            title = "Straitstimes logo" />
            </a>
            </figure >
            <ul> {li_menu} </ul>
            <SocialMedia class = "st-social-desktop" />
            </div>
            </nav>
        );
    }
});


var Menu_mobile = React.createClass({
    handleClick: function() {
        $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
        $(".st-content-menu-fixed").toggleClass('st_dialogIsOpen');
        this.eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
    },
    eventCloseMenu: function(event) {
        if (event) {
            $(".st_menu_mobile").css('right', '10px');

            d3.select(".first_line").transition().duration(500).attr("x1", 12.8).attr("y1", 12.2).attr("x2", 23.5).attr("y2", 22.8);
            d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 0).attr("x2", 0);
            d3.select(".second_line").transition().duration(500).attr("x1", 12.9).attr("y1", 22.9).attr("x2", 23.4).attr("y2", 12.1);
            $('.st_content_menu_fixed').hide().slideDown('500').addClass('fixed_menu_mobile');
            $('body').css({
                'overflow': 'hidden',
                'position': 'relative'
            });
        } else {
            $(".st-menu-mobile").css('right', '0');
            d3.select(".first_line").transition().duration(500).attr("x1", 10.5).attr("y1", 13.2).attr("x2", 26.1).attr("y2", 13.2);
            d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 1).attr("x2", 26.1);
            d3.select(".second_line").transition().duration(500).attr("x1", 10.5).attr("y1", 21.9).attr("x2", 26.1).attr("y2", 21.9);
            $('.st_content_menu_fixed').slideUp('500', function() {
                $(this).show().removeClass('fixed_menu_mobile');
            });
            $('body').removeAttr('style');
            var height_footer = $("footer.st-content-footer").outerHeight();
            $("body").css("margin", "0 0 " + height_footer + "px");
        }
    },
    render: function() {
        var li_menu_mobile = [];
        this.props.items_menu.forEach(function(d, i) {
            var active = (d.active === 1) ? 'selected-menu' : '';
            var target = "";
            if (d.active !== 1) target = "_self";
            li_menu_mobile.push( <
                div className = "detail-menu-mobile"
                key = {
                    i
                } >
                <
                div className = {
                    "title-menu-mobile " + active
                } >
                <
                a href = {
                    d.url
                }
                target = {
                    target
                }
                title = {
                    d.name
                } > {
                    d.name
                } < /a> < /
                div > <
                div className = "transform-border-hairline" >
                <
                /div> < /
                div >
            );
        });

        return ( <
            div className = "st-menu-mobile" >
            <
            div className = "st-button-menu-mobile"
            onClick = {
                this.handleClick
            } >
            <
            Button_menu_mobile />
            <
            /div> <
            div className = "modal-menu-mobile" >
            <
            div className = "st-content-detail-menu-mobile" > {
                li_menu_mobile
            } <
            /div> <
            div className = "st-content-shared-social" >
            <
            SocialMedia class = "st-social-mobile" />
            <
            /div> < /
            div > <
            /div>
        );
    }
});


var SocialMedia = React.createClass({
    handleShareFacebook: function(e) {
        e.preventDefault();
        var image = 'http://graphics.straitstimes.com/STI/STIMEDIA/facebook_images/budget-2017-statement/statement.png';
        var name = "Singapore Budget 2017: Analysing the Finance Minister’s Budget statement";
        var description = " Finance Minister Heng Swee Keat delivered his Budget statement on Feb 20, 2017. Read the full speech below and compare it with his speech from last year.";
        var url = "http://str.sg/budget17statement";

        this.share_face_book(image, name, description, url);
    },
    handleShareTwitter: function(e) {
        e.preventDefault();
        var text = " Check out this word analysis tool for %23HengSweeKeat’s %23sgbudget2017 speech";
        var via = 'STcom';
        var url = 'http://str.sg/budget17statement';

        this.share_twitter(text, via, url);
        return false;
    },
    share_face_book: function(image, name, description, url) {
        FB.ui({
            method: 'feed',
            link: url === undefined ? window.location.href : url,
            caption: 'www.straitstimes.com',
            picture: image,
            name: name,
            description: description
        });
    },
    share_twitter: function(text, via, url) {
        window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
    },
    render: function() {
        return ( <figure className = {
                this.props.class
            }
            itemProp = "associatedMedia image"
            itemScope itemType = "http://schema.org/ImageObject"
            data-component = "image" >
            <img onClick = {
                this.handleShareFacebook
            }
            src = "images/st-button-facebook.svg"
            className = "st-image-responsive"
            alt = "st button facebook"
            title = "st button facebook" />
            <img onClick = {
                this.handleShareTwitter
            }
            src = "images/st-button-twitter.svg"
            className = "st-image-responsive"
            alt = "st button twitter"
            title = "st button twitter" />
            </figure>
        );
    }
});
var Button_menu_mobile = React.createClass({
    render: function() {
        return ( <
            svg version = "1.1"
            xmlns = "http://www.w3.org/2000/svg"
            x = "0px"
            y = "0px"
            width = "32px"
            height = "32px"
            viewBox = "0 0 35 35" >
            <
            circle fill = "#0C2B57"
            stroke = "#FFFFFF"
            strokeMiterlimit = "10"
            cx = "18.2"
            cy = "17.6"
            r = "15.5" > < /circle> <
            line className = "first_line"
            fill = "none"
            stroke = "#FFFFFF"
            strokeWidth = "1.7"
            strokeMiterlimit = "10"
            x1 = "10.5"
            y1 = "13.2"
            x2 = "26.1"
            y2 = "13.2" > < /line> <
            line className = "menu_mobile_line_center"
            fill = "none"
            stroke = "#FFFFFF"
            strokeWidth = "1.7"
            strokeMiterlimit = "10"
            x1 = "10.5"
            y1 = "17.6"
            x2 = "26.1"
            y2 = "17.6" > < /line> <
            line className = "second_line"
            fill = "none"
            stroke = "#FFFFFF"
            strokeWidth = "1.7"
            strokeMiterlimit = "10"
            x1 = "10.5"
            y1 = "21.9"
            x2 = "26.1"
            y2 = "21.9" > < /line> < /
            svg >
        );
    }
});



module.exports = Menu;
