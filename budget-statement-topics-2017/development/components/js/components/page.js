"use strict";
var React = require('react');


var Menu = require('./menu');

var Head=require('./head');
var Article= require('./article');

var Footer =require('./footer');

var page = React.createClass({
    render: function() {
        return (  
        	<div className="st-content-graphic" itemScope itemType={"http://schema.org/NewsArticle"} >
	        	<meta itemProp={"datePublished"} content={"2016-01-30"} />
	        	<Menu />
                <main>
                    <Head />
                    <Article />
                </main>
	        	<Footer />
        	</div>
        );
    }
});

module.exports = page;


