window.$ = require('jquery');
global.jQuery = $;
var d3 = require('d3');
var Hammer = require('./libraries/hammer.min');
require('./libraries/chosen.jquery.min');
require('./libraries/autolink-min');



var data = {};
data.speechs = {
    2015: [{
        person: "Read the paragraphs from PM xxx xxx's speech transcript in 2013 which feature your selected word/phrase",
        speech: "Madam Speaker, I beg to move, that Parliament approves the financial policy of the Government for the Financial Year 1st April 2016 to 31st March 2017.\nLast year, we celebrated all that we achieved together in our first 50 years. This year, we start our journey for the next 50 years.\nOur first 50 years tell an extraordinary story of diverse people overcoming great odds together.\nOur pioneers had no textbook for success. Every move – whether it was a government policy, a company’s investment, or a household’s family decision – was uncharted.\nThrough sheer grit, our pioneers braved the odds, brought up families, and built a home and a nation.\nOn 13 December 1965, Mr Lim Kim San presented the first Budget of the independent Republic of Singapore.\nThis was the world on that day.\nThe major concerns then were: Konfrontasi, the Communist threat and the Vietnam War.\nFew in the world paid attention to little Singapore. Against this backdrop, our pioneers debated how to make a living, by making our place in this region and in the world.\nIn that first Budget, Mr Lim said: “Whilst independence has… given us greater freedom of action …, we also acquire new responsibilities and new dimensions are added to our problems.”\nOur pioneers took on these new responsibilities and new dimensions seriously. Our founding Prime Minister, Mr Lee Kuan Yew, made the rousing call that we must never fear; one day we would be a modern metropolis. Singaporeans rallied to that call. Year by year, we made progress; and successive generations built on what was achieved. So we had much to celebrate in SG50.\nToday, we are in a much better position than our pioneers. But we also face new responsibilities, new challenges.\nIn 1965, we raced to teach “123s” and “ABCs” to our young, and to create factory jobs to soak up unemployment for a young population. Today, over 95% of our students progress beyond secondary school. Our challenge now is to take education beyond schools, to lifelong learning, and to grow the economy to create quality jobs.\nIn 1965, we were fighting childhood mortality with mass immunisation, and distributing milk to under-nourished children. Today, the problem has flipped. Our challenge is to find new ways to care for our elderly, to manage chronic diseases and to tackle childhood obesity.\nIn 1965, we were faced with Konfrontasi and the Vietnam War, and were at risk of social discord stemming from a legacy of 6 racial conflict and pro-communism. Today, we have to guard ourselves against a more complex and diverse array of threats, from conventional threats to terrorism and cyber warfare.\nMr Lim Kim San’s words 50 years ago resonate to this day.\nWe face a new world that brings its own freedoms and responsibilities, its own challenges.\nOur mission remains unchanged – it is to find the ways to create opportunities for Singaporeans to grow, and defend our home and our people.\nTaking inspiration from our pioneers, we must plan ahead, stay united, be creative, and build our future together.\nThis is the context in which President Tony Tan set out five key aims for this term of Government: Renewing our economy; Fostering a more caring society; Transforming our urban landscape; Keeping Singapore safe and secure; Engaging and partnering with Singaporeans in nation building.\nTo achieve these goals, we must grow our economy and invest collectively for the long term – in our people, our home, our security. To grow our economy, we will invest in building stronger enterprises and nurturing innovative industries.\nFor our people, we will invest in education and health. Our spending on education in FY16 is expected to be $12.8 billion, almost double what it was 10 years ago. We must continue investing in our people through education and SkillsFuture. Compared to a decade ago, our healthcare spending has increased almost six-fold to $11 billion. This will rise further. Now, 1 in 8 Singaporeans is aged 65 and above. This will rise to 1 in 6 by 2020, and 1 in 4 by 2030.\nFor our home, we must invest for better liveability and connectivity. This year, our transport spending is expected to hit $10.1 billion – more than five times what we spent a decade ago. At the same time, we are embarking on new infrastructure of unprecedented scale, such as Changi Airport Terminal 5.\nFor our security, we will also have to invest more in intelligence, operational capabilities, technology and systems to keep our home safe and secure.\nAll these translate into higher spending needs. Our expenditures have grown to almost two and a half times of what they were 10 years ago. We were able to fund these increased investments because we had planned ahead and remained prudent. Growth had also been fairly strong as our GDP almost doubled over the past decade.\nIn this term, we are starting from a position of strength, as we have anticipated our needs and made provisions in the last term. We have no natural resources, no oil wells, but we benefit significantly from the returns of the reserves accumulated by our forefathers through hard work and thrift. We must continue to strike a careful balance between what we spend today, and what we save for future generations.\nIn the longer term, to meet Singapore’s many needs, especially in healthcare, our expenditures will grow. But GDP growth will slow as our economy and workforce mature. We will find ourselves in a tighter fiscal position.\nWe must plan ahead. We must grow our capabilities in order to grow our economy and create good jobs for our people. We must remain prudent in our expenditures, and ensure every additional dollar spent is spent responsibly. We must encourage every Singaporean to contribute towards caring for our fellow citizens and building for the future.\nMuch work lies ahead. I am confident that if we have the will, we will find the way.\nCore for us to succeed is the spirit of partnership, where Singaporeans work together in new ways to transform our economy and strengthen our society.\nWe have inherited a strong spirit of togetherness from our kampong days. Over the years, it has taken on new forms and new vitality, from the shared values of Our Singapore Conversation, to the ground-up active citizenry of SG50, to the collective commitment in The Future of Us and SGfuture.\nGoing forward, this partnership that is at the heart of the Singapore spirit will become even more critical for tackling big challenges together.\nEveryone has a role, and together, we are weaving a rich tapestry – each thread a different colour and texture, but woven together to give strength and resilience to our economy and our society.\nWe must come together as partners to transform our economy through enterprise and deeper innovation. The global economy is becoming more interconnected, more diverse and complex. Change is accelerating, with rapid advances in knowledge and technology. Yet the potential for collaboration has never been greater. We must work together to muster our resources, to innovate, scale up and internationalise. Our economic transformation will draw on the strengths of individual schemes, businesses, unions and trade associations, but bring everyone together in a more tightly woven tapestry to meet the specific needs of our industries.\nLikewise, we must deepen partnerships to build a caring and resilient society. Over the last decade, the Government has stepped up social support significantly. But we must continue to build both individual and collective responsibility, for it is together that they make for a stronger society. In our next phase, we will strengthen partnerships with community groups to build a rich tapestry of caring communities in all our neighbourhoods, with multiple layers of support, drawing on different threads of care and kindness, but integrated coherently.\nLeading up to this Budget, we received thousands of suggestions and feedback. Let me thank all who have contributed.\nBudget 2016 is the beginning of a journey towards SG100. With the spirit of partnership, we will transform our economy through enterprise and innovation, and build a caring and resilient society.\nLet me first speak about the economy.\nOver the past five years, our economic restructuring journey has focused on raising productivity to achieve quality growth. We tightened the inflow of foreign workers; We invested significantly in broad-based measures such as the Productivity and Innovation Credit (or PIC); and, We introduced the Transition Support Package to help firms adjust to economic restructuring and rising business costs.\nProgress has been promising. More firms are engaging in productivity efforts. For instance, a survey has shown that around 9 in 10 of our SMEs embarked on productivity initiatives in 2015. The net inflow of foreign workers has slowed significantly from nearly 80,000 in 2011 to less than 23,000 in 2015. With a tighter labour market, we managed to sustain real median income growth for Singaporeans at an average of 2.9% per year over 2009 to 2015. Productivity growth has not been as strong as we would like. While productivity has grown by an average of 2.7% per year over 2009 to 2015, most of this increase was due to the cyclical rebound in 2010 and 2011. Productivity growth has remained relatively flat over the past three years. We must keep working on this.\nIn 2015, the economy grew by 2%. The performance across sectors was varied: wholesale and retail trade, and finance and insurance did well, while manufacturing, in particular the marine and offshore segment, declined, as we can see from this chart.\nI am aware that current business conditions are difficult and uncertain. Many of our firms are facing weaker top-line growth, rising manpower costs and tighter financing. Workers are anxious as retrenchment has increased, including among professionals.\nIn the coming year, given our economy’s heavy dependence on external demand, the weaknesses in the global economy will pose strong headwinds.\nThe pace of global economic recovery is uneven, with the US being the most advanced, while Europe and Japan will only see modest growth aided by monetary stimulus. Closer to home, China is going through a transition towards a more sustainable growth path. It is a complex transition and any short-term setbacks may create volatility in the financial markets.\nWe therefore expect externally-oriented sectors such as manufacturing to continue to face subdued demand. The extended downturn in oil and other commodity prices is affecting commodity-related activities, particularly the marine and offshore sector. Weak global demand in electronics will spill over to related sectors such as precision engineering.\nBut while overall growth is subdued, our business landscape is varied. There are pockets of growth and resilience. Even within manufacturing, the medtech and chemicals sectors are growing. Exports of services, including tourism, financial services, Information and Communications Technology (or ICT) and consultancy are benefiting from regional demand. Domestic-oriented sectors such as retail, healthcare and education have been, and should remain, stable. Construction, too, will be supported by a large expansion in public infrastructure and housing projects, even as private residential demand has ebbed.\nWithin sectors, prospects vary across firms. Some are becoming more competitive and gaining market share, while others are seeking to relocate to cheaper destinations.\nSimilarly, prospects in the labour market are mixed. Overall, redundancies increased in 2015 as global demand slowed and restructuring continued. Some of those made redundant took longer to find jobs. At the same time, however, unemployment remained low at 1.9%. While some sectors such as the offshore and marine and manufacturing are retrenching staff, others such as healthcare, education, and ICT are hiring.\nIn summary, while we face weaker prospects overall, MTI expects GDP to grow at 1% to 3% for the year, not very different from the 2.0% in 2015. So while conditions are difficult, we should not be overly pessimistic.\nEven as we tackle immediate cyclical weaknesses, we must be alert to major structural changes abroad and at home.\nMajor economies are continuing to restructure, and will alter the global competitive landscape in the process. China is rebalancing towards consumption and services- led growth, and developing innovation-intensive industries. India is building on its strengths in ICT, and seeking to attract manufacturing investments. These ongoing changes are rapidly changing the patterns of trade and specialisation in Asia.\nTechnological changes, especially in robotics, automation, artificial intelligence and ICT, are disrupting business models across all sectors. Some call the coming changes “Industrial Revolution 4.0.”\nAt home, manpower growth is slowing, and our population is ageing. Real wage increases over the past few years have benefited workers and households. But unless productivity improves in tandem, we will be less competitive, and both businesses and workers will be worse off.\nAll these changes pose intense challenges for our businesses, which will have to succeed in a more competitive environment while contending with tighter labour constraints. The need to restructure is both urgent and critical.\nWhile there are challenges, there are also growing opportunities. We are in the centre of the Asian growth story: China, India and ASEAN are expected to grow at 6.3% per year over the next five years, accounting for about one-third of global growth. The ASEAN Economic Community, the Trans-Pacific Partnership, and China’s One Belt One Road initiative will open up new opportunities.\nWe are well placed to benefit from technological changes – our investments in education, R&D and digital infrastructure will enable us to seize new opportunities.\nWe also started restructuring early and our firms, including SMEs, are embarking on change. Our people are valued for their integrity, adaptability, and multi-cultural sensitivity. Singapore is a highly-connected, trusted node.\nIn summary, we face near-term cyclical weaknesses, which affect various sectors in different degrees, as well as medium-term structural challenges. But we are well positioned to manage these challenges.\nBudget 2016 has three key thrusts to address the challenges in our economy: First, to address cyclical weaknesses, we will adopt an expansionary fiscal stance to provide some counter. Taking into account the higher expenditures and additional measures introduced in this Budget, our projection is that this will amount to a positive fiscal impulse of slightly over 1% of GDP. This, together with relief measures targeted at SMEs, will provide support in the near term. Second, as increased spending alone cannot address structural issues, we will target resources towards enabling firms to build deeper capabilities, develop their people, scale up and internationalise. We will launch the Industry Transformation Programme to strengthen enterprises and industry, and to drive growth through innovation. The aim is to enable our firms to emerge stronger to benefit from the broader global recovery when it takes place. Third, we will support our people through change, by enabling them to learn new skills, especially in new, fast growing sectors, and in facilitating employment and job-matching.\nTo tackle medium to long-term issues, we have convened the Committee on the Future Economy (or CFE). Though the CFE has just begun work, the inputs have been valuable for the measures in this Budget, and as the deliberations continue, we will update our plans.\nLet me speak on the support we are providing to address immediate concerns, while encouraging restructuring.\nThe first source of support for firms comes from existing measures and public spending, including public infrastructure projects. This year, total spending is expected to be $5.0 billion (7.3%) higher than in FY2015. The increases are mainly in healthcare, education, security and urban development.\nThe Transition Support Package that was introduced in FY2013 will also continue to support our firms in raising productivity. In particular, this month, firms will receive a total of $1.9 billion for qualifying wage increases given under the Wage Credit Scheme, the largest payout to date.\nIn addition, public sector demand for construction projects is expected to increase significantly in 2016, helping to mitigate a decline in private sector construction demand. This includes more than $2.5 billion of public sector contracts for smaller projects, which will benefit smaller construction firms.\nSecond, to help companies, especially SMEs, I will raise the existing Corporate Income Tax (CIT) Rebate, from 30% of tax payable to 50% of tax payable, with a cap of $20,000 rebate each year for YAs 2016 and 2017. The last time we had this 50% rebate was in YA 2001.\nThe higher percentage rebate is targeted at SMEs.\nThe increased support is expected to cost an additional $180 million over two years, bringing the total support given to companies under the CIT rebate to close to a billion dollars over two years.\nOur third measure to support companies is the Special Employment Credit (or SEC). The SEC is due to expire this year. I will modify and extend the SEC for three years, to the end of 2019, to provide employers with a wage offset for workers aged 55 and above earning up to $4,000 a month.\nEmployers with Singaporean workers aged 65 and above will continue to receive a wage offset of up to 8%. This is in addition to the wage offset of 3% for the re-employment of workers aged 65 and above till the re-employment age is raised in 2017.\nThe SEC will be up to 5% for workers aged 60 to 64 and up to 3% for those aged 55 to 59.\nThe SEC will cover about 340,000 workers, or about three in four older Singaporean workers. To support this extension, I will top up the SEC Fund by $1.1 billion.\nThe fourth measure will support viable SMEs that may have cash flow concerns or wish to continue growing their business. We will introduce an SME Working Capital Loan scheme, for loans of up to $300,000 per SME. Under this scheme, the government will co-share 50% of the default risk of such loans with participating financial institutions, to encourage lending to our SMEs.\nThe SME Working Capital Loan will be available for three years. This could catalyse more than $2 billion of loans over this period.\nFifth, we will help our heartland shops to be more vibrant, as these shops give our neighbourhoods a sense of community. MND will enhance the Revitalisation of Shops package, to better support promotional activities and upgrading projects in HDB town centres and neighbourhood centres.\nSPRING will also work with the Federation of Merchants’ Associations and local merchant associations to strengthen their capabilities to support heartland businesses.\nThis enhanced initiative is expected to cost about $15 million annually.\nFinally, in view of challenging business conditions in the Marine and Process sectors and the reduction in the number of Work Permit holders in these sectors, we will defer levy increases for Work Permit holders in these sectors for one year. Manufacturing Work Permit levies will remain unchanged for another year, as announced at Budget 2015.\nWe will proceed with levy increases for Services and Construction Work Permit holders, as well as S Pass holders in every sector, as announced in Budget 2015. This is in view that the foreign workforce has continued to grow in these areas over the past year. Details are in the Annex.\nTaken together, this calibrated set of measures is appropriate to address the near-term concerns of our firms, especially SMEs, while enabling restructuring. Some have asked for a repeat of support measures we saw in 2009. But that was when the economy was already in deep recession, and facing huge uncertainty. For now, while the outlook is soft, MTI expects positive growth in 2016. We must not let pessimism take hold, lest it creates self-fulfilling expectations. The Government will continue to monitor the situation, and stands ready to act if conditions warrant.\nSome have asked about the range of measures that we have introduced to stabilise the property market since 2010. These are intended to keep the market stable and sustainable. Based on the price level and current market conditions, our assessment is that it is premature to relax these measures. We will continue to monitor developments in the property market closely.\nEven as we provide immediate relief and support amid the current cyclical slowdown, we must press on with economic transformation. The global economic landscape is changing, and our challenges are pressing. We have a narrow window. We must find every opportunity to transform, to emerge stronger in the coming years.\nIn Budget 2016, we will launch a new Industry Transformation Programme to take us into the next phase of our development. This builds on our efforts under the Quality Growth Programme, which was introduced in Budget 2013 to achieve inclusive growth driven by innovation and higher productivity.\nThe Industry Transformation Programme will help firms and industries to create new value and drive growth in four ways: It will involve integrating our different restructuring efforts. Our efforts to raise productivity, develop our people, and drive research and innovation are working, but we can maximise impact by pulling these together.\nWe will take a more targeted and sector-focused approach to better meet the needs of firms in each sector.\nWe will deepen partnerships between government and the industry, and among industry players to identify challenges, and develop solutions to support transformation.\nAnd we will place a stronger emphasis on technology adoption and innovation.\nThe Food Manufacturing sector is a good example of how such an approach can succeed: Individually, our Singapore food companies have put in place many innovations. Some, such as Tan Seng Kee, have used technology to develop a whole range of products which stay fresh for longer. This enabled Tan Seng Kee to be the first company in Singapore to export fresh noodles that can be easily prepared with its special sauce mixes in flavours such as Laksa and Curry Mee. Others, such as Foodgnostic, have transformed their business models to enhance business growth through internationalisation and food exports.\nAs an industry, our food manufacturers built on their innovations and Singapore’s trusted reputation for high quality and safe food to jointly create the “Tasty Singapore” brand. Using this brand, they are internationalising and selling to China, India, the Middle East, and even Africa. They are now expanding through e-commerce, using websites such as Tmall.com.\nThe food industry integrated their efforts. The combined novelty and quality of their individual products allowed them to stand out as a group. The sharing of common facilities gave them economies of scale. And building the Singapore brand together won them global recognition. In turn, their success creates a virtuous cycle of investments.\nOur food industry has also been investing in their people. These include 280 management associates and interns who they see as future leaders.\nThere have been strong partnerships across firms, as well as between firms and the Singapore Food Manufacturing Association (SFMA), the Food Innovation Resource Centre in Singapore Polytechnic and various Government agencies.\nAs a result, what once appeared to be a sunset industry – because of low margins and high labour cost – is now a thriving sector. Real value-add per worker has grown by an average of 9.2% a year from 2010 to 2014.\nThe Food Manufacturing sector shows the importance of mindset – as one of the industry’s leaders told me: “There is no such thing as a sunset industry, only sunset thinking!”\nIt shows that transformation comes not only from individual firms but the industry as a whole working together. This year, we will set out three key thrusts under the Industry Transformation Programme: First, we will support the transformation of enterprises, to build deep capabilities, deploy technology, develop scale and internationalise.\nSecond, we will support the transformation of industries, to adopt technology and innovate faster, come up with common industry solutions, seek new markets overseas, and deepen industry partnerships.\nThird, we will drive transformation through innovation.\nLet me start with transforming enterprises.\nEnterprises are the building blocks of our economy. Vibrant enterprises that change and transform with the times create growth and buzz in the economy.\nTake the example of Xin Ming Hua Private Limited. Started as a small machinery and repair shop in 1955, it is now one of the largest distributors of engines and power systems in Asia. It has been active in innovating and building up in-house design and manufacturing capabilities; adopting automation to achieve productivity gains of up to 75% with the help of Republic Polytechnic; and internationalising and scaling up its operations to serve customers better.\nTo support many more firms to transform successfully, we will introduce several measures in this Budget.\nToday, we have a wide range of schemes administered by various government agencies to help enterprises19 of different sizes, across industries.\nTo help SMEs access these schemes, we established SME Centres and stepped up our outreach efforts.\nStill, many firms found the range of incentive schemes and agencies confusing. Indeed, it is an alphabet soup, as shown on this slide: it is scheme-centric, not enterprise-centric.\nTo be more enterprise-centric, we will launch the Business Grants Portal in the fourth quarter of this year. This portal will be organised along core business needs of capability building, training and international expansion. Firms will not need to go from agency to agency to figure out which schemes apply to them.\nThe portal will also pre-populate details that are available in the ACRA database. The Business Grants Portal will start with grants from IE Singapore, SPRING, STB and Design Singapore and progressively include grants from other government agencies.\nWe will also continue to review and simplify the various support schemes, to improve access, and to strengthen assistance for enterprises at various stages of growth.\nTo support companies to automate, drive productivity and scale up, we will offer a new Automation Support Package for an initial period of 3 years.\nCurrently, there is no support for the scaling up of automation projects. As these involve significant financial outlays, some companies find it difficult to commit.\nThe new Automation Support Package from SPRING will comprise four components: First, we will provide a grant to support the roll-out or scaling up of automation projects. We will fund these projects at up to 50% of project cost, with a maximum grant of $1 million.\nSecond, for qualifying projects, there will be a new 100% Investment Allowance for automation equipment.\nThird, we will improve SMEs’ access to loans for qualifying projects. To do so, government will enhance our risk-share with participating financial institutions from 50% to 70% for such projects.\nFourth, as such large-scale automation projects enable firms to scale and internationalise, IE Singapore will work together with SPRING in relevant cases to help these businesses to access overseas markets.\nThis comprehensive package offered by the Automation Support Package will be useful to firms seeking to grow.\nWe anticipate that the Automation Support Package will provide support of over $400 million over the next 3 years. Details are in the Annex.\nNext, I will expand the SME Mezzanine Growth Fund from the current fund size of $100 million to a total fund size of up to $150 million. We will do so by matching up to $25 million of new private sector investment on a 1:1 basis. This will provide more capital to support our SMEs to scale up and internationalise.\nTo support more mergers and acquisitions (M&As), I will grant the M&A allowance on up to $40 million of the value of the deal, instead of the current cap of $20 million. With the enhancement of the M&A allowance to 25% of the value of the deal as announced in Budget 2015, companies can now enjoy up to $10 million of M&A tax allowances per Year of Assessment.\nTo provide upfront certainty to companies for their corporate restructuring, I will extend the non-taxation of companies’ gains on disposal of their equity investments, based on existing scheme parameters, until 31 May 2022. More details on these tax incentives can be found in the Annex.\nOur firms, especially SMEs, are eager to seek new markets and new growth opportunities overseas.\nIn the coming years, IE Singapore will support more firms in their internationalisation efforts. In 2015, IE supported 34,000 companies in their internationalisation efforts, a 21% increase over 2014. In 2016, IE expects to help around 35,000 to 40,000 companies of all sizes to venture overseas.\nIn addition, I will extend the Double Tax Deduction for Internationalisation scheme, till 31 March 2020. This covers qualifying expenses incurred for activities such as participation in overseas business development and investment study trips.\nI have outlined how we will support individual enterprises to transform. They will find it easier to transform if their entire industry also undertakes transformation. By doing it together, firms achieve scale, drive down cost, incentivise service providers to step forward, and expand mindshare.\nThe second thrust of our Industry Transformation Programme is thus to support industry-level transformation.\nFor example, robots can handle metal fabrication processes more efficiently, from steel cutting to welding to machine tending. But these may be too expensive or complicated for many SMEs. Recently, a number of solution providers including Lincoln Electric came forward to design and deliver more affordable, modular systems for SMEs. Over 1,000 SMEs could benefit, if this is scaled up across the manufacturing sector.\nIndustry-level transformation works best if firms partner one another, or if industry associations lead the effort, because they know their needs best. Government agencies can support by initialising lead demand or through regulatory changes. We need to form close partnerships among firms, industry associations and Government to drive industry-level transformation.\nLet me highlight three specific measures. First, we will develop a National Trade Platform as the next-generation platform to support firms, particularly in the logistics and trade finance sectors. This will eventually replace our current TradeNet and TradeXchange systems.\nAs a one-stop trade information management system, the National Trade Platform will enable electronic data sharing among businesses and government. Firms only have to provide trade information once and authorise its use by logistics providers as well as business partners. The information can also be used for Customs and other trade regulatory approvals. This will be especially helpful for SMEs, to cut costs and streamline processes.\nThe National Trade Platform is not just an IT system. We will develop it as an open innovation platform, so that other service providers can develop value-added services and apps in areas such as operations, visibility and trade finance.\nThe National Trade Platform is expected to cost more than $100 million to develop. It has the potential to bring over $600 million worth of man-hour savings each year for our firms.\nA second way to drive industry-level transformation is to develop and deploy new technologies to solve problems that are relevant for the entire industry. Robotics is a case in point.\nRobotics technology can enable us to work more effectively in a tight labour market. It can also create more high value-added jobs.\nIn our hospitals, robotic technology is used in the pharmacy to pick medication accurately and quickly. This frees up time for pharmacy staff to provide advice on medication. Autonomous transporters are used to move supplies efficiently in the hospital. Our hospitals are also trying power-assisted bed transport technologies to reduce the manual effort in moving patients. All these technologies can improve efficiency and transform the healthcare industry.\nApart from healthcare, robotics and automation technology can be applied widely to transform sectors such as Construction, Manufacturing and Logistics. We announced the National Robotics Programme last year. We will now scale up our efforts, and in particular, work with solution providers to offer packaged solutions to SMEs at a reasonable cost.\nI will make available over $450 million to support the National Robotics Programme over the next three years.\nThe third key way to drive industry-level transformation is through our Trade Associations and Chambers (or TACs). TACs have intimate knowledge of the needs and potential of their specific sectors. They are well-placed to reach out to many firms, especially SMEs.\nWe will build on the existing Local Enterprise and Association Development (LEAD) programme to help our TACs strengthen their outreach through the new LEAD-Plus programme. This will provide wider funding support for TACs to attract talent, develop their capabilities, and strengthen their processes and services.\nTo forge a closer partnership, and enable public officers to better understand the needs of our firms, we will also second up to 20 public officers to interested TACs as part of the LEAD-Plus programme over the next five years. Various TACs have already expressed interest.\nIn addition, TACs can support firms to build capabilities, and lead the development of industry-wide solutions for common challenges. SPRING will partner TACs to drive 30 such projects over the next three years, to reach out to more than 3,000 SMEs.\nWe will set aside up to $30 million over the next five years to support TACs in developing their capabilities, with additional funding for industry-wide transformation projects. I encourage TACs to step forward. Some have already expressed interest and SPRING will announce these soon.\nI just spoke about how we can drive transformation at the industry-level by leveraging platform solutions and new technologies, and increasing industry outreach through TACs. This builds on enterprise-level transformation, and strengthens our economy.\nLet me now speak about how firms and industry as a whole must transform through innovation.\nInnovation is critical to the Industry Transformation Programme. Many of the successful enterprise and industry-level transformations I described earlier have some elements of innovation in the form of new products or services, new processes, or new business or organisation models.\nInnovation is enabled and enhanced by the use of technology but innovation goes beyond that. It is fundamentally about new ways of doing things to meet the needs of people and industries better.\nInnovation is the engine of value creation and growth. We must make innovation pervasive in our society. This is a long-term endeavour but let me outline some initial steps.\nFirst, we will continue to deepen industry capabilities in innovation and R&D.\nOf the commitment announced for the Research, Innovation and Enterprise (RIE) 2020 Plan, up to $4 billion will be directed to industry-research collaboration. This represents a concerted shift towards innovation and enterprise, to capture the economic and social value of R&D.\nLet me give an illustration. Procter and Gamble (P&G) has worked with A*STAR and EDB to set up the Singapore Innovation Centre (SgIC). It is one of P&G’s major innovation centres, focusing on research and product development for its global business units in Home Care, Healthcare, Grooming and Skin Care. The SgIC will be a key open innovation hub to accelerate and facilitate collaborations between P&G and partners in Asia, including Singapore enterprises.\nTo support the RIE 2020 effort, I will provide a top-up of $1.5 billion to the National Research Fund this year.\nThe ownership of Intellectual Property (or IP) allows companies to realise the value of its efforts. To encourage companies to acquire IP, I will now allow businesses the flexibility to write down the cost of acquiring IP over different periods of 5, 10 or 15 years, instead of the current 5 years only.\nA second way to transform through innovation is to promote start- ups, in new and existing industries.\nWe have been nurturing our start-up ecosystem, including venture funding and support for accelerators and incubators24. The start-up scene today is more vibrant, with growing funding support from venture and private equity funds.\nTo give these efforts a further boost, we will set up a new entity called “SG-Innovate”. SG-Innovate will match budding entrepreneurs with mentors, introduce them to venture capital firms, help them to access talent in research institutes, and open up new markets. SG-Innovate will build on what has been done by the Infocomm Investments Private Limited (IIPL), and work with SPRING and EDB to expand the accelerator programmes to new and emerging sectors such as Smart Energy, Digital Manufacturing, Fintech, Digital Health, and Internet-of-Things.\nA third way to transform through innovation is create an open and innovative urban environment. We will launch an exciting new development: the Jurong Innovation District. It will be the future of innovation for enterprise, learning and living.\nFifty years ago, we transformed Jurong from swampland into a thriving hub for the manufacturing industry that powered Singapore’s economic growth. Now, we will make another leap to create the industrial park of the future.\nOur earlier industrial estates were developed for specific industries, focused mostly on production. Today, however, learning, research, innovation and production are closely intertwined.\nThe Jurong Innovation District will create an environment to house these different activities within a single, next-generation industrial district. This has the potential to transform how we live, work, play, learn and create.\nLet me show you a video of what the Jurong Innovation District could look like when it is completed: JTC is currently constructing Launchpad @ JID, to serve as a space for entrepreneurs, researchers and students to design, prototype, and test-bed their new innovations.\nJTC has also launched an Open Innovation Call to invite private sector technology owners to test-bed and develop innovative and sustainable infrastructure solutions within the District. In parallel, we will be building Jurong Innovation District progressively, with the first phase targeted for completion around 2022.\nSimilarly, we are investing in infrastructure for the future such as Changi Airport Terminal 5 to better connect us to the world, and to test innovative solutions. This year, I will make a further $1 billion top-up to the Changi Airport Development Fund to support this effort.\nLet me summarise. We are launching the Industry Transformation Programme to transform our enterprises, transform industries and transform through innovation. These three thrusts will take concerted partnership to realise. I have highlighted a few examples of how enterprises and entire industries can embark on this transformation journey, but these are just a beginning.\nAs a government, we must adopt a more integrated approach to support transformation. Our agencies will work more closely together, integrating their different support schemes to take a more targeted approach to developing each industry. We will work closely with enterprises and at the industry-level to develop transformation maps for each sector. These will help us allocate the resources to develop each sector appropriately.\nAs a whole I will set aside a total of $4.5 billion under the Industry Transformation Programme, to support enterprises and industries, on top of the amounts for R&D and National Productivity Fund. This includes the next tranche of increased funding to SPRING, IE Singapore and EDB to support economic development, as well as new resources for measures announced in this Budget. These resources will support the sectors under the Industry Transformation Programme, as well as to cater for the growth of new industries over the next five years. The actual spending will depend on the take-up rates of the various schemes.\nFor example, we will make available over $300 million to the Food Manufacturing sector over the next five years. This includes support to firms and TACs, as well as initiatives such as specialised industrial infrastructure.\nWe will take this more integrated and more targeted approach, in partnership with industry and unions, across more than 20 sectors, covering 80% of our GDP. The work with sectors such as precision engineering and logistics is at a more advanced stage. We will systemically develop transformation maps, sector-by- sector.\nThese maps will be ‘live’ documents that we have to adjust as we navigate our way. We may not have all the answers today, but we must work together – our people, firms, unions and government agencies – in a spirit of partnership, and to keep experimenting and trying, in the spirit of enterprise and innovation.\nAs we move towards more targeted measures under the Industry Transformation Programme, I will continue the tapering of broad- based measures. PIC currently has two components: cash payout, and tax deductions. In line with our move towards more targeted measures, I will lower the cash payout rate under PIC from the current 60% to 40% for expenditures incurred on or after 1 August 2016. The 400% tax deductions under the scheme remain unchanged. The PIC scheme which has been extended for YA 2016 to YA 2018 will expire thereafter.\nEven with the tapering of these broad-based measures, we will continue to provide significant support for firms to restructure in the coming years.\nIn addition to the Industry Transformation Programme, I will also extend and strengthen tax incentives to encourage higher industrial land productivity, and to enhance activities in the areas of finance and treasury, global trading and maritime activities. The details of these four changes are in the Annex.\nThis Budget sets the direction, taking into account feedback from businesses during the initial phase of the work of the Committee on the Future Economy. As the CFE deliberates and consults more widely, we will adjust, but we must start to move in the new direction early.\nLet me now go on to speak about how we will support our people to overcome challenges and seize opportunities.\nAs firms face weaknesses in cyclical demand and structural challenges, our people too will be directly affected. Some Singaporeans have been retrenched, while others are anxious about their future. Even where new jobs are being created, they wonder if they can keep up.\nIndeed, as economic cycles shorten and changes occur faster, the pressure on our people to adapt will rise. People all over the world are facing this. I understand Singaporeans’ anxieties and concerns and we must give every support to our people.\nOne major way is for our firms to restructure, so that our workers have better prospects. This chart shows that across selected industries, firms with higher productivity tend to pay higher wages.\nIt is important for our firms to raise productivity, or else our workers would remain in low value-added jobs with weak prospects. We must aim for a virtuous cycle of higher skills, higher productivity and higher wages. The Industry Transformation Programme I outlined earlier will be a major effort to achieve this.\nEven in new sectors that are growing, such as ICT, new skills are constantly needed as technologies change rapidly. Today, with the exponential growth of social and mobile offerings by businesses, employers are looking for software engineers with skills in User Interface and Design, Android/iOS development, Application Programming Interfaces, on top of the commonly used programming languages like Python, C++ and Java.\nIn short, like firms, our people too are facing cyclical and structural changes. I appreciate that it is not easy for those who are retrenched to learn new skills and find new jobs. But if we remain adaptable, learn, unlearn and relearn quickly, we can stay relevant and seek new careers.\nWith the robust attitude of Singaporeans towards changes, and our willingness to learn, we are better placed than many others elsewhere to cope with change. We must retain our confidence.\nOur people are increasingly better educated, and we are investing significantly in life-long learning and skills development under SkillsFuture.\nOur polytechnics and universities have responded to the changing job market. For instance, our polytechnics now offer new courses such as cybersecurity and digital forensics. SIT also now trains food technologists.\nDealing with change will be a long-term endeavour, and for this Budget, the government will, together with employers and unions, support our people in three ways.\nFirst, through SkillsFuture, we will support our people to keep learning new skills; Second, we will help those who have been laid off, to find new work and build a new career; Third, we will pilot new approaches to job matching and training, to enable more people to seize new opportunities in growth sectors.\nSkillsFuture is our long-term game plan. Since we launched SkillsFuture in November 2014, we have put in place many initiatives. These will enable our people to identify and pursue their interests at every stage of life, and to broaden and deepen their skills with better education and training.\nFor example, we have announced SkillsFuture Earn and Learn Programmes in 12 sectors. Since January, Singaporeans have also started using the SkillsFuture Credit to learn new skills.\nThe SkillsFuture Study Awards, which support individuals to develop specialist skills, is taking off nicely. Since October last year, the response has been good and we have rolled out the study awards covering 12 areas of specialisation.\nTake for example Ms Yap Chui Hoon, who will be using the SkillsFuture Study Award to deepen her skills as a social worker. She has been a lifelong learner. After graduating with an ITE certificate, she worked for more than ten years. She learnt on the job with the help of multiple part-time courses, including diplomas in social service and disability studies. I am happy to say that she is now embarking on a degree programme in social work to better support her clients with special needs at the APSN Centre for Adults.\nWe are off to a good start, and must continue to expand and deepen SkillsFuture.\nTo support our people amidst softening economic conditions and ongoing restructuring, MOM will enhance employment support through the “Adapt and Grow” initiative. This will help our people adapt to changing job demands and grow their skills.\nFor workers who may face greater difficulty in finding jobs, we will expand our wage support schemes to encourage firms to hire them. This will benefit more workers who may be affected by retrenchments or business restructuring.\nFor mid-career jobseekers including retrenched professionals, we will step up professional conversion programmes. New programmes will be launched in sectors such as Design, and ICT. We will also enhance efforts to help match them in jobs with SMEs.\nWe expect to more than double the current outreach for PMETs from 2,000 to over 4,000. MOM will commit an additional $35 million a year from the Lifelong Learning Endowment Fund and Skills Development Fund to support these initiatives.\nThe final thrust of our efforts is to support our people to acquire new skills and match them to new opportunities in growing sectors.\nWe will start with the ICT sector. The demand for ICT professionals is growing because it can enable many new businesses as well as disrupt existing ones. To drive our Smart Nation effort, we will need many more of our own experts, in a wide variety of skills – programmers and coders, cybersecurity specialists, user experience designers. The current shortage is driving up pay – the average starting pay of fresh computer engineering graduates increased by over 14% last year to around $4,000.IDA estimates that we will have about 30,000 new positions by 2020.\nTo enable our people to learn new ICT skills quickly, we will set up the TechSkills Accelerator, a new skills development and job placement hub for the ICT sector. MajorIT employers and associations including SiTF, SCS and ITMA will partner IDA in this effort.\nTechSkills Accelerator will pioneer a new way of enabling our people to acquire expertise and skills, so as to engage in the fast growing ICT sector. It will: Identify specific skills in demand and work with specialised training providers to meet these complex demands in start- ups and in sectors such as finance and healthcare.\nDevelop industry-recognised skills standards and certification. This will guide ICT professionals to acquire industry-relevant skills and help employers assess skills proficiency of their employees.\nWork with anchor employers to commit to hiring and paying based on certified skills proficiency rather than academic qualifications alone. Several employers have agreed to come on board, including the new Government Technology Agency.\nTechSkills Accelerator will pioneer new ways of enabling Singaporeans to be at the frontiers of learning and knowledge, to seize opportunities in new growth areas. We will expand these efforts to more sectors in the coming years.\nAs announced at Budget 2015, we estimate that our spending on SkillsFuture and related initiatives will average over $1 billion per year till 2020.\nThe Ministers for Communications and Information, Education, Manpower, National Development, and Trade and Industry will elaborate on the various measures that I have spoken about, at the Committee of Supply.\nThrough all these measures, Budget 2016 seeks a new way to transform our economy through enterprise and innovation, with stronger partnerships among stakeholders to support our people to manage change.\nMadam Speaker, may I have your permission to distribute materials to the Members in this House?\nI have spent some time on how we will transform our economy through enterprise and innovation. We seek to transform our economy so that we continue to have the resources and opportunities for our home to be safe, and our people to find fulfilment. Together with a strong economy, we need to build a caring and resilient society.\nWe want a Singapore which is a great place to raise a family, and where we bring out the best in every Singaporean; a society that takes care of those who have special needs, who are less well-off or who have fallen on hard times; a society where our seniors age with energy and dignity.\nOver the last decade, the Government has invested more in our schools and preschools. We have done more for families with children, through measures like the Baby Bonus and additional leave for parents. We have strengthened the four pillars of our social security system – home ownership, healthcare assurance, CPF and Workfare. We have also enhanced ComCare, and made help more accessible through the Social Service Offices.\nThis Budget builds on what we have done, and looks at new ways to build a caring and resilient society. We will forge a deeper partnership between individuals, the community and the Government, catalyse ground-up initiatives to build caring and cohesive neighbourhoods, and pilot new ways to reach out to our seniors.\nI will first speak about the measures for our young – First Step, KidSTART and Fresh Start.\nWe will introduce a new Child Development Account (CDA) First Step grant for all Singaporean children. Parents will automatically receive $3,000 in their child’s CDA, which they can use for their children’s healthcare and childcare needs. This will apply to eligible babies born from today.\nParents who save more will continue to receive dollar-for-dollar matching from the Government, up to the co-savings cap. For example, if they put $3,000 into their first or second child’s CDA, they will receive an additional $3,000 in matching grants, bringing the total Government CDA grant to $6,000, for a total of $9,000 in the account.\nSimilarly, parents with more children will receive the CDA First Step grant. They can receive higher matching grants, as shown in the table.\nWe will also double the Medisave withdrawal limit for pre-delivery medical expenses, from $450 to $900, with immediate effect. Details are in the Annex. The Senior Minister of State in charge of the National Population and Talent Division will elaborate on further measures to make Singapore a great place for families at COS.\nNext, we will pilot a new initiative, called KidSTART, for children in their first six years. There is extensive research which shows that experiences in the early years of a child’s life significantly influence his or her physical, cognitive, and social development.\nWe have been enhancing development programmes through our preschools and primary schools. However, there is a small group of parents who may need more support to give their children a good start in life.\nKidSTART will draw together government and community resources, to help these children receive appropriate learning, developmental, and health support. We will develop approaches that work best in the Singapore context.\nAbout 1,000 children are expected to benefit. This pilot is expected to cost more than $20 million. The Minister for Social and Family Development will elaborate on this at COS.\nWe will also do more to help families with children in rental housing. The Prime Minister mentioned the Fresh Start Housing Scheme at last year’s National Day Rally. There are some families who previously bought a flat, but sold it, and are now living in public rental flats. These families are not eligible for housing grants for first-timers, as they had received a housing subsidy before. For those who are determined to work hard to own a home again, we want to give them a fresh start.\nThe Fresh Start Housing Scheme will provide a grant of up to $35,000 to help such families with young children to own a 2- room flat, with a shorter lease, which will be more affordable for them. Families will need to demonstrate effort, for example, by staying employed and making sure their children attend school.\nThe Minister for National Development will provide more details at COS.\nTo thrive, our young need a sense of adventure, resilience, and be ready to challenge themselves to be their best. To help our students develop these attributes, we will expand outdoor adventure education for all students, through a new National Outdoor Adventure Education Masterplan. As part of this effort, we will build a new Outward Bound Singapore (OBS) campus on Coney Island. This is an artist’s impression of the campus.\nMany more youths will have the chance to go for an expedition with OBS. These activities will help them build confidence, and develop camaraderie with students across different schools.\nThe OBS campus on Coney Island is expected to be ready around 2020, and cost about $250 million. Just like the OBS campus on Pulau Ubin, it will be rustic, and blend in with the rest of the island. Coney Island remains open for everyone to enjoy. MCCY and MOE will provide more details later.\nFor our children and youth, we concentrate on providing them opportunities through education. For adults, we focus on opportunities through work – through which we learn and contribute, and derive personal pride and dignity. There are three groups who may need more support in the workforce: seniors, low wage workers, and persons with disabilities. We can do more together to help them.\nI spoke earlier about the extension of the SEC, to encourage employers to hire seniors. I will now speak about helping our low wage workers, and persons with disabilities.\nThe Workfare Income Supplement (WIS) scheme has encouraged workers who are 35 years old and above to join the workforce, by supplementing their income and CPF savings. They can also upgrade their skills through the Workfare Training Support scheme and SkillsFuture.\nWe will further improve WIS for work done from January 2017.\nFirst, we will raise the qualifying income ceiling from the current average wage of $1,900 a month, to $2,000 a month. WIS will continue to help the bottom 20% of workers, with some support also provided to those in the 30th income percentile. In total, we expect WIS to benefit about 460,000 Singaporeans.\nNext,we will increase WIS payouts. Eligible workers will receive higher payouts. Payouts will vary depending on their age and income. For example, workers earning $1,000 to $1,600 a month will receive increases in payouts of $100 to $500. Workers will continue to receive 40% of WIS in cash and 60% in CPF.\nTo illustrate, an older worker aged 55 earning $1,200 a month will now get a total of $2,900 a year from WIS, with about $1,200 in cash, and $1,700 in CPF a year. He will then have $3,500 more in his CPF account at age 65.\nWe will also simplify the qualifying criteria for WIS. Today, to receive WIS, one has to work 2 out of 3 consecutive months, or 3 out of 6 consecutive months, or 6 out of 12 months in a year. We will now pay WIS for every month worked.\nBy simplifying the qualifying criteria, workers can also look forward to receiving WIS payments more quickly – monthly rather than quarterly.\nWith these enhancements to WIS, the total budget will be about $770 million a year.\nMany persons with disabilities also want the opportunity to contribute through work. We should support them. Today, those who meet the WIS eligibility criteria receive WIS, even if they are under 35 years old. They will benefit from the WIS enhancements I spoke about earlier.\nEmployers who hire persons with disabilities who earn up to $4,000 a month will continue to receive the SEC. They get a credit of up to 16% of the employee’s wages, twice as large as the SEC for older workers.\nCurrently, only low wage workers 35 years old and above are eligible for the Workfare Training Support scheme. We will now also enable persons with disabilities who earn low wages and are under 35 years old, to be eligible for the Workfare Training Support scheme, so that we can better support them in their learning.\nThe Public Service will play its part in expanding job opportunities for persons with disabilities, with support from SGEnable. The Minister for Social and Family Development will speak on the review of the national Enabling Masterplan at the Committee of Supply.\nThere will be many more seniors amongst us in the coming years. The proportion of Singaporeans aged 65 and above will double from 1 in 8 today, to 1 in 4 by 2030. How we adjust, and turn this into a source of strength, will shape our society. Together, we must, and will make Singapore a model for successful ageing, and empower all to age with dignity and vitality.\nIn this Budget, we will increase retirement support for seniors who have lesser means. We will also build strong communities, to support active ageing and engagement in the community.\nAt Budget 2015, we spoke about the introduction of the Silver Support Scheme. It is a major new feature in our social security system. Some of our older Singaporeans have fewer resources in their retirement years than others, because they earned low wages even after working consistently throughout their lives, or because they stayed home to raise their families.\nThe aim of Silver Support is to support the bottom 20% of Singaporeans aged 65 and above, with a smaller degree of support extended to cover up to 30% of seniors. It can be a modest but meaningful supplement to their retirement incomes. It is not intended to substitute for other sources of support, whether from their own savings or family support. Silver Support also complements other forms of assistance they may receive from existing schemes, whether it is Workfare, healthcare subsidies or the GST Voucher.\nSilver Support is not only for the neediest of our seniors. For the truly needy, we have the safety net of Public Assistance (PA), which I will speak about shortly.\nOver the past year or so, we have deliberated with care on the fairest way of identifying those who are the bottom 20% to 30% of our seniors. As no single criterion allows us to assess needs and operationalise the scheme effectively, we will use three criteria in combination – lifetime wages, housing type, and the level of household support.\nThe first criterion, lifetime wages, is measured by the amount the individual has contributed into his CPF accounts in total32 by the age of 55. Seniors who have not more than $70,000 in total CPF contributions by age 55 will meet the first criterion. We do not look at CPF contributions after 55, to encourage older Singaporeans to continue working.\nThe second criterion is housing type. Silver Support will cover the seniors who live in and own a 4-room or smaller HDB flat33. Seniors who live in 5-room HDB flats, but do not own the flat, will also qualify if they meet the other criteria.\nThe third criterion is household support. Seniors in the bottom one-third of households where, on average, each member earns not more than $1,100 per month, can benefit from Silver Support.\nTogether, these three criteria allow us to make sure Silver Support goes to those who have lower incomes over their lifetimes and less retirement support. Eligible seniors will receive between $300 to $750 every quarter, depending on their flat type. The majority of seniors living in 1- and 2-room flats will receive Silver Support. About half of the seniors living in 3-room flats will also receive Silver Support.\nTo illustrate, take a retired couple living in a 3-room flat. Each had put less than $70,000 into their CPF accounts by the age of 55, perhaps because they were in low-wage jobs throughout their working lives. By all three counts, the couple qualify for Silver Support. The couple would receive $600 each in Silver Support payouts every three months, or $4,800 together over the whole year. This will not change or stop any other support they are receiving – such as the GST Voucher or healthcare subsidies through CHAS. For such couples, Silver Support can be very meaningful.\nThere may also be some seniors in lower-income households who qualify for Silver Support, but subsequently no longer need it, for example because their children are able to provide stronger financial support. The converse may also happen, resulting in the senior needing more support. So we will make an assessment annually to take into account changes in circumstances, to make sure we target Silver Support at those with lesser means.\nWe earlier announced that we would implement Silver Support around the first quarter of 2016. As this is a new and extensive scheme, we needed more time to operationalise it. We will make the first payout in July this year, and it will be a double payout for two quarters – the two quarters of April to June 2016, and July to September 2016. The next two payouts will be made in end-September and end-December. Each one is a payout for the coming quarter. In subsequent years, payouts will be made in March, June, September and December.\nCPF Board will notify eligible seniors before the first payout is made. There is no need for seniors to apply; this will happen automatically.\nSilver Support will benefit more than 140,000 seniors, and will cost close to $320 million in the first year. The cost will likely increase over time as our population ages.\nThe majority of our seniors, even if they do not qualify for Silver Support, will continue to benefit from substantial existing support schemes. These include the GST Voucher, higher healthcare subsidies through MediShield Life premium subsidies and the Pioneer Generation Package, foreign domestic worker levy concessions, and eldercare subsidies. Some could also choose to realise the value of the property they own, through renting it out, or making use of the enhanced Lease Buyback Scheme or the Silver Housing Bonus. This is on top of the support from children which many receive.\nLet me now turn to successful ageing. We care about enabling our seniors to stay active and healthy, to stay meaningfully engaged.\nI recently met Mrs Lucy Tan, who is the Cluster Director of PEACE-Connect. PEACE-Connect has been taking care of needy seniors in Kampong Glam for the last 20 years.\nLucy related to me how she has seen the social and medical needs of the seniors evolve over the years - they grew ever more complex, affecting more and more seniors. Lucy and her organisation had to evolve too. She realised PEACE-Connect would have to work with more partners if they wanted to support seniors better. She has been developing partnerships with other organisations, and with the support of her Advisor, Ms Denise Phua, she has integrated these resources well. For instance, she works with Kampong Glam CC to provide subsidised meals daily to some residents in the area, and links seniors up with services that they need at town councils and Social Service Offices.\nLucy shows what is possible when we strengthen partnerships. To better support leaders like Lucy, we will pilot Community Networks for Seniors. These networks will comprise local stakeholders, such as Voluntary Welfare Organisations (VWOs), community volunteers, schools and businesses. At the core, the network will have a small team of full-time officers who will study the health and social needs of seniors and draw together stakeholders to provide coordinated support.\nWe hope to help seniors discover health conditions earlier and manage them well, while connecting those who are healthy and mobile to a wide range of activities to encourage seniors to stay active, healthy and engaged in the community.\nUnder this pilot, our Pioneer Generation Ambassadors will, through their outreach, encourage seniors to stay active and connect them to relevant support.\nThere will be a group of seniors who may require more help, such as frail elderly living alone, who have limited family support. These networks will provide more targeted and coordinated health and social support for them.\nBy integrating the resources and efforts of government agencies, VWOs and local volunteers, these networks can make a big difference to support seniors to stay active and engaged. It will also be a new way of service delivery, and a meaningful way to realise the Action Plan for Successful Ageing launched last year. We will pilot these Community Networks for Seniors in a few areas. If successful, this pilot can be scaled up later.\nI have spoken about our care measures for the young, low-wage workers and those with disabilities, and seniors. We will also provide some additional support to households in general.\nThose who are permanently unable to work, and have little or no means of income and family support, currently receive free or highly subsidised social services and free medical treatment in polyclinics and public hospitals.\nIn addition, they receive a basic monthly cash allowance through the Public Assistance scheme. Additional amounts are provided for families with children, and for needs such as medical supplies and household appliances. Public Assistance recipients also often receive assistance in cash, food delivery and other vouchers from community-based agencies and grassroots volunteers.\nWe will raise the basic monthly cash allowance. For example, a two-person household, where both are on public assistance, will now receive an additional $80 a month, bringing the amount of cash assistance per month to $870. The Minister for Social and Family Development will announce more details at COS.\nTo help government pensioners who draw lower pensions, the Government will increase the Singapore Allowance and monthly pension ceiling by $20 per month each – to $300 and $1,230 respectively. This will benefit about 10,000 pensioners.\nTo support households amid current economic conditions, we will provide a one-off GST Voucher – Cash Special Payment of up to $200 for eligible GST Voucher – Cash recipients. In total, eligible households can receive up to $500 in GSTV – Cash in 2016. If our recipients spend some of these in our neighbourhood shops, it will support our local businesses as well!\nThe one-off GSTV – Cash special payment will cost an additional $280 million in 2016 and benefit 1.4 million Singaporeans.\nWe will also provide one to three months of Service & Conservancy Charges (S&CC) rebate. 1- and 2-room HDB households will receive a total of three months of rebates for this year, while 3- and 4-room households will receive two months of rebates.\nThis will cost the Government $86 million and benefit about 840,000 HDB households.\nWe will also make a tax change. We currently have 15 personal income tax reliefs. We have enhanced many of these over the years. Each tax relief serves a worthy objective, such as to supplement retirement savings, encourage mothers to work after having children, or take up a course.\nHowever, taken together, the tax reliefs may unduly reduce total taxable incomes, for a small proportion of individuals.\nI will introduce a cap on the total amount of personal income tax relief an individual can claim, at $80,000 per Year of Assessment. At this threshold, 99% of tax-resident individuals will not be affected. Many can still continue to enjoy reliefs. For instance, among those currently claiming the Working Mother’s Child Relief, 9 out of 10 are expected to continue to claim it fully, without being affected by this cap.\nThis cap will make our personal income tax system more progressive. Nevertheless, our personal income tax burden remains low. Our personal income tax structure must allow us to continue to stay competitive.\nThe personal income tax relief cap will take effect from YA 2018 and is expected to raise an additional $100 million a year.\nLet me now speak of what we will do to bring Singaporeans together to build a caring society.\nToday, our schools have meaningful values-in-action programmes for our students to help others. At work, businesses are well placed to run impactful programmes. We should give a boost to Corporate Social Responsibility and make it easier for employees to contribute through their workplaces. This Budget will support the National Volunteer and Philanthropy Centre’s efforts to promote and build capacity for giving among corporates.\nI have seen some inspiring examples. For instance, Samsui Supplies and Service, a food retailer, uses their kitchen facilities to prepare 2000 free meals a day for the needy. We must encourage more businesses to step forward.\nThis Budget will also catalyse more ground-up initiatives, to better tap on the creativity and energy of Singaporeans, many of whom want to give back. Such citizen-initiated efforts can drive community-based and neighbourhood-based initiatives. Let me speak about these.\nCurrently,we allow for 250% tax deduction for donations of cash, and qualifying in-kind donations such as land and computers, to certain Institutions of a Public Character (or IPCs). This has seen good results, with cash donations rising each year.\nMany employees also want to volunteer and we hope businesses will support them. To do so, I will introduce a pilot Business and IPC Partnership Scheme. From 1 July 2016 till the end of 2018, businesses that organise their employees to volunteer and provide services to IPCs, including secondments, will receive a 250% tax deduction on associated cost incurred, subject to the receiving IPC’s agreement. This deduction will be subject to a yearly cap of $250,000 per business and $50,000 per IPC.\nCommunity Chest (ComChest) plays a critical role in raising funds for about 80 VWOs across the entire social sector, including the Family Service Centres and special education schools. ComChest’s monthly donation programme, SHARE, enables it to receive regular donations, through payroll deductions or GIRO. Some businesses have been active in supporting SHARE.\nTo give a boost to ComChest’s good work, I will provide dollar- for-dollar matching for any additional donations through SHARE, over and above the FY2015 level. We will do this for three years, starting from April this year. Where businesses encourage their staff to donate regularly, we will allow part of the matching funds to be used by them to organise Corporate Social Responsibility activities. The Minister for Social and Family Development will elaborate at COS.\nFor SG50, we started the SG50 Celebration Fund. We had a very good response, and supported close to 400 projects initiated by Singaporeans from different walks of life, to celebrate our Jubilee year, and to build a sense of community.\nThese are Ms Seema and Harsha Dadlani, two Singaporean sisters who are passionate about writing and helping children learn. With support from the SG50 Celebration Fund, they wrote and published six children’s books about Singapore, which were very well received.\nAlso, I recently met Mr Dennis Quek and Mr Wilson Ang.\nThey organised a project to take 100 wheelchair users on a tour of Pulau Ubin, getting help from over 600 people to do so – including help from the Singapore Navy to ferry the participants on navy vessels. When I met them recently, I asked them why they did what they did. They told me they just wanted to share the parts of Singapore they love with fellow Singaporeans who may not be able to have the same experiences they do. When I told them it was kind of them to do so, they said they got a lot more out of the project than they put into it - in terms of learning, new friendships, appreciation for what can be achieved when many people come together for a common cause. They were amazed at how many people came forward, to realise the power of partnership.\nTo support passionate citizens like Seema, Harsha, Dennis and Wilson, and many others like them who created a whole array of meaningful SG50 projects, we will set up a new fund, called Our Singapore Fund. It is Our Singapore Fund because it is about how we all can come together in partnership to share our strengths, share our loves, create something more and better together, to build our Singapore together. The Fund will support projects that build the spirit of caring and resilience, nurture our can-do spirit, and promote unity and our sense of being Singaporean.\nThe total fund size will be up to $25 million and it will be set up by the second half of 2016. The Minister for Culture, Community, and Youth will elaborate at COS.\nI hope that these three measures will catalyse efforts for a kinder and more caring society. The details of these measures are in the Annex.\nI would like to share a story which shows how working together can make a big difference. This is Ms Norashidah binte Mohd Yassin, who is 39 years old this year. Three years ago, she was staying in interim rental housing with her two young children, without family support or income.\nWith the support of her MP, Dr Maliki Osman, who coordinated the help from a welfare organisation, PAVE, grassroots volunteers and various Government agencies, she gradually got back on her feet. She first took on ironing of clothes for her neighbours, then baking, after getting a baking certificate. She later got a job as an administrative assistant and was soon promoted. Today, her children are well adjusted, and she is waiting for her flat.\nHer story is inspiring not just because of her remarkable progress, but also because she is now reaching out to help others who are in the same situation that she used to be in, counselling them and teaching them baking skills.\nThis is the spirit of the society that we are building. It is one where we rise above our circumstances, to build a better life for ourselves and our children. It is a society that cares for those in need, and where those who are helped do their part to help others. It is a society that we are all proud to be a part of.\nMadam Speaker, let me now summarise our budget position.\nFor FY2015, our Budget is expected to record a deficit of $4.9 billion (1.2% of GDP). This is lower than the deficit of $6.7 billion (1.7% of GDP) we had budgeted a year ago.\nIn FY2016, total spending is expected to be $5.0 billion (7.3%) higher than in FY2015.\nAs we spend more, we must also spend right – To both: Provide immediate relief to businesses, and support longer term economic transformation;\nTo both: Provide targeted support to those in need, and invest in developing our people.\nThe rise in expenditure in FY2016 is supported by increases in both Operating Revenue and higher Net Investment Returns (NIR) Contributions. We are benefitting from an increase in operating revenue this year due to one-off factors which we do not expect to be sustained. In addition, this year, Temasek will be added onto our NIR framework and this will be a source of revenue for the long term.\nAs earlier mentioned, the longer term picture will grow more challenging as we expect expenditure needs to grow faster than revenues. Even as we plan for rising expenditures, we must spend only when it is needed and where it best achieves our social and economic objectives. We will review the major expenditure items we expect ahead, to ensure efficiency and effectiveness.\nWe expect an overall budget surplus of $3.4 billion (0.8% of GDP) in this first year of the term. This position seeks to strike a balance between being prudent given the continued rise in expenditures we expect in the years ahead, and being accommodating to support enterprises in the current economic climate even as we continue our restructuring efforts. Should economic conditions turn, we stand ready to adjust and respond.\nMadam Speaker, let me conclude.\nI started today by recalling the first Budget of the independent Republic of Singapore.\nEight days after that first Budget was presented, our founding Prime Minister Lee Kuan Yew said that we must keep Singapore here a thousand years from now. And he said, “That is your job and mine.”\nA thousand years from now. Singapore was only 135 days old when he said that. But he had that faith.\nAnd more importantly, an indomitable will: For it is your job and mine, he said, to make sure we survive. If Singapore is to have a future, we have to work hard at it – together.\nIt is poignant to recall these words one year and one day after Mr Lee’s passing. He attended almost every Budget Day since 1959. Each of us has only so many years on this earth to do our best for one another and for our country. How do we make these years count? Together, we can make our time count. We can dream, plan, and build for a thousand years. We must try.\nBudget 2016 is the first step of the next lap in what we hope will be a long, successful journey.\nFor our economy, we will tackle immediate pressures head-on. We will also set our sights on the long term, position ourselves to stand out.\nWe will transform our economy through the Industry Transformation Programme, emphasising enterprise and innovation. This marks a new phase in our economic restructuring. Through targeted efforts and close partnerships, we must drive scale and create value. We will build resilience in workers, enterprises and industries so they can weather changes and emerge stronger.\nThe Committee on the Future Economy will dive deep into what we must do to get our economy ready for the future.\nAnd we will build a caring and resilient society. Silver Support is a major new step to help seniors with lesser means. The Community Networks for Seniors will forge a new way to care for our seniors. First Step, KidSTART, the Fresh Start Programme, OBS@Coney – these are our investment in our young.\nWe will find new ways of encouraging people and companies to come forward to help the vulnerable. We will find new ways of collaboration and social innovation. We will not leave any Singaporean behind.\nJust like our pioneers had no textbooks to show them how Singapore could thrive in its first 50 years, we have no instruction manual for our future. What we have are our shared values, our common vision, a sense of duty, and the conviction that we are one people.\nTogether, we must harness the creativity and energy of every one of us to transform our economy and strengthen our society.\nTogether, we must be prepared to change. We can’t always know the future, and we won’t always have ready answers. But we must have the imagination to forge new paths, the courage to try new ways, and the tenacity to persevere if we don’t succeed on the first try.\nAs long as we stand by our values, keep faith with one another, think long-term, dare to try and be open to change, we will move in the right direction.\nWhen the new Cabinet was sworn in last year, the Prime Minister said, “The Singapore Story belongs to all of us. If we have faith that Singapore will endure and thrive, and put our heart and soul into building Singapore, then we will prevail, and secure our place in history.”\nThat is your duty and mine. The Singapore Story belongs to all of us. There are many chapters yet to be written in that story. Let us put our heart and soul to write our story – together, in partnership.\nMadam Speaker, I beg to move."
    }],
    2016: [{
        person: "Read the paragraphs from PM Lee Hsien Loong's speech transcript in 2013 which feature your selected word/phrase",
        speech: "Madam Speaker, I beg to move, that Parliament approves the financial policy of the Government for the Financial Year (FY) from 1st April 2017 to 31st March 2018.\nWhen I presented the last Budget, Brexit seemed remote and the US had just started the process of electing their new President. Events since then are a stark reminder of how quick and unpredictable change can be.\nThere is an inward-looking mood in several of the most advanced economies. Should this translate into protectionist actions, it will slow global trade and investments.\nMeanwhile, advances in technology are picking up pace, disrupting traditional businesses and jobs. But they also present opportunities for countries and people who can learn and adapt quickly.\nThese deep shifts around the world will create new challenges but also open up new opportunities for many years to come. We must understand these shifts and do our best to adapt and thrive.\nSingapore is also undergoing a key transition as our economy matures. With falling birth rates and a rapidly ageing population, labour force growth will eventually fall to zero. Many developed economies going through this same experience have seen their annual GDP growth decelerate to 1% or lower. We can aim for quality growth of 2% to 3%, if we press on in our drive for higher productivity and work hard to help everyone who wishes to work find a place in the labour force.\nSingapore should develop strong capabilities in our firms and workers, so they can adapt to the changes in economic structures and technology. Digitalisation, innovation and highly skilled workers will enable cities and regions to prosper while staying open and connected to the world.\nIn addition, we must forge deep partnerships in our economy. The Government’s role is not to plan every move, but to forge a common understanding of the changes, and foster partnerships with businesses, unions, firms and workers, with each playing a key role. We need to pool our resources and ideas, and solve problems together. Such networks of trust will allow us to seize opportunities and respond to unexpected challenges.\nSimilarly, we need to foster partnerships in the wider society so we can maintain a good balance between state action and community initiative. Government can do good in many instances, but so can individual citizens. When friends, neighbours and fellow citizens come together in support of a cause, or one another, we forge deep bonds and grow our social capital.\nIt is critical that we take decisive action to re-position ourselves for the future. Budget 2017 will outline measures for our economy and our society, together with our fiscal policies. We will take a learning and adaptive approach, try new methods, continue with them when they work well, cut losses when they do not, and draw on feedback and experience to adjust and refine our plans. That is the Singapore way.\nLet me start with the economy and jobs.\nLast year, we achieved GDP growth of 2.0%. This is similar to the 1.9 % achieved in 2015. This is within range, but at the lower end of our potential growth over the medium term.\nBut this aggregate growth figure belies the uneven performance across sectors. Sectors such as Electronics, Information & Communications, and Education, Health & Social Services, did well. Some sectors have been harder hit by cyclical weaknesses, including Marine & Offshore, and to some extent, Construction. Other sectors like retail are facing structural shifts.\nThe picture of the labour market is similarly mixed. Overall, unemployment rate remained low at 2.1% in 2016, but redundancies have been increasing and more workers are taking longer to find jobs. Sectors such as healthcare and education offered more jobs, while others shed them.\nIn 2016, resident employment increased while foreign employment contracted.\nGiven the uneven performance across different sectors, we need to go beyond general stimulus, and target the specific issues faced by different sectors.\nFor firms and sectors that are doing well, we must focus on the long term and build on the momentum to seize new opportunities. For example, firms in the manufacturing sector should adopt advanced manufacturing technologies to build competitive advantage. In last year’s Budget, I announced the $450 million National Robotics Programme. This year, I will be announcing measures to help firms with good prospects scale up and internationalise. I will talk about these measures later.\nFor sectors facing cyclical weaknesses, we have introduced specific support measures like the Bridging Loan for Marine and Offshore Engineering companies, which provides access to working capital to help them bridge short-term cash flow gaps. I will introduce specific measures to address continued cyclical weaknesses in the Marine and Process sectors, and the Construction sector.\nLast year, we deferred foreign worker levy increases in the Marine and Process sectors. In view of continued weakness, we will defer the earlier announced levy increases in these two sectors by one more year.\nTo support the Construction sector, we will also bring forward $700 million worth of public sector infrastructure projects to start in FY2017 and FY2018. Our construction firms will be able to bid for and participate in these projects, which include the upgrading of community clubs and sports facilities. To sustain the momentum for productivity improvement, we will proceed with the foreign worker levy increases for Construction announced in 2015.\nFirms in sectors that are facing structural shifts will need to dig deep to change their business models to stay viable. For example, firms and workers in Retail will need to embrace digital capabilities to access new markets through online-marketing, and e-commerce platforms.\nThe Government will help workers adapt to structural shifts in the economy, especially those who seek to move to a different sector or industry. Last year, the Ministry of Manpower (MOM) launched the “Adapt and Grow” initiative to help workers looking to take on new jobs. We will strengthen the support this year.\nWe will increase wage and training support provided under the Career Support Programme, the Professional Conversion Programme, and the Work Trial Programme.\nWe will introduce an “Attach and Train” initiative for sectors that have good growth prospects, but where companies may not be ready to hire yet. Instead, industry partners can send participants for training and work attachments. This will increase the chances of these workers to find a job in the sector later.\nAn additional sum of up to $26 million a year will be committed from the Lifelong Learning Endowment Fund and the Skills Development Fund to support these initiatives.\nThe Minister for Manpower will elaborate on the initiatives at the Committee of Supply (COS).\nOver the next two to three years, the different sectors of our economy will be in transition, repositioning themselves for the future economy. Some firms may need help to manage cost or cash flow. They will continue to receive support from schemes announced previously. Let me mention three.\nThe Wage Credit Scheme will continue to help firms cope with rising wages. We expect to pay over $600 million to businesses this March.\nRoughly 70% of this amount will be to SMEs.\nThe Special Employment Credit will continue to provide employers with support for the wages of older workers till 2019. Over $300 million, which will benefit 370,000 workers, will be paid out in FY2017.\nThe SME Working Capital Loan will continue to be available for the next two years. This is where Government co-shares 50% of the default risk for loans of up to $300,000 per SME. There has been good take-up for this scheme. Since its launch in June 2016, the scheme has catalysed more than $700 million of loans.\nI will introduce two more measures to support firms.\nFirst, I will enhance the Corporate Income Tax (CIT) Rebate. Last year, I enhanced the CIT rebate from 30% to 50% of tax payable, capped at $20,000 each year for Year of Assessment (YA) 2016 and YA2017.\nThis year, I will further enhance the CIT rebate by raising the cap from $20,000 to $25,000 for YA2017. The rebate will remain at 50% of tax payable.\nI will also extend the CIT rebate for another year to YA2018, at a reduced rate of 20% of tax payable, capped at $10,000. The enhancement and extension will cost an additional $310 million over YA2017 and YA2018.\nSecond, we will provide more support for firms hiring older workers. MOM will raise the re-employment age from 65 to 67 years, with effect from 01 July 2017. This will apply to workers younger than 65 on that day.\nTo encourage employers to continue hiring workers who are not covered, we will extend the Additional Special Employment Credit till end-2019.\nUnder this scheme, employers will receive wage offsets of up to 3% for workers who earn under $4,000 per month, and who are not covered by the new re-employment age of 67 years old.\nTaken together with the Special Employment Credit, employers will receive support of up to 11% for the wages of their eligible older workers.\nThe extension of the Additional Special Employment Credit will benefit about 120,000 workers and 55,000 employers, and will cost about $160 million. This helps to extend the employability of older Singaporeans. Details are in the Annex.\nThese additional near term support measures, with the existing Wage Credit Scheme and Special Employment Credit, will give businesses support of over $1.4 billion over the next year.\nThe measures I have described will help our firms and workers, especially in sectors that are facing cyclical or structural weaknesses. Even more important is how we increase our growth in the medium to long term, so as to sustain our potential growth of 2% to 3%. Let me now speak about measures to build our capacity for the future economy. These measures largely respond to the ideas put forth by the Committee on the Future Economy, or CFE.\nThe CFE has laid out seven mutually reinforcing strategies to tackle the challenges ahead. These strategies are not prescriptive blueprints but focus on developing adaptability and resilience. These qualities will keep Singapore relevant even as the world changes.\nEmerging economies are now able to produce rather than import higher value components. There is also a growing consumer class in many Asian cities. Even as we stay open and connected, we have to understand our global partners and customers much better and more deeply. Our enterprises and people will need to venture overseas and to immerse themselves in these markets to gain deep insights.\nTechnology is reshaping businesses, jobs and lifestyles across the world. We must spot the opportunities in the digital economy, and make the most of our strengths as a nimble, well-educated, tech-savvy society.\nAs we mature as an economy, we must compete on the quality and novelty of our ideas, and our ability to create value. We need to build a strong innovation and enterprise engine, to complement our traditional strengths in efficiency and speed.\nThese moves will entail building capabilities of our enterprises, the capabilities of our people, and bringing all parts together in partnership to act as one agile, adaptable whole. This, in essence, is the key thrust of the CFE recommendations.\nLet me start with enterprise development.\nEnterprises are the heart of vibrant economies. For our enterprises to stay competitive and grow, they will need to develop deep capabilities. Which capabilities matter – this depends on the industry that the firm is in, and the firm’s own stage of growth. But there are three capabilities that many firms will need in common – being able to use digital technology, embrace innovation, and scale up.\nDigital technology has unique potential to transform businesses, large and small, across the economy. The first way to strengthen our enterprises, especially SMEs, is to help them adopt digital solutions.\nWe will introduce the SMEs Go Digital Programme to help SMEs build digital capabilities. The Info-communications Media Development Authority (IMDA) will work with SPRING and other sector lead agencies in this effort. The SMEs Go Digital Programme will have three components:\nFirst, SMEs will get step-by-step advice on the technologies to use at each stage of their growth through the sectoral Industry Digital Plans. We will start with sectors where digital technology can significantly improve productivity. These include Retail, Food Services, Wholesale Trade, Logistics, Cleaning and Security.\nSecond, SMEs will get in-person help at SME Centres and a new SME Technology Hub to be set up by IMDA. SMEs can approach business advisors at SME Centres for advice on off-the-shelf technology solutions that are pre-approved for funding support, or connect to Info-communications and Technology (ICT) vendors and consultants. The more digitally advanced firms can get specialist advice from the SME Technology Hub.\nThird, SMEs that are ready to pilot emerging ICT solutions can receive advice and funding support.\nWe will work with consortiums of large and small firms to help them adopt impactful, interoperable ICT solutions, to level up whole sectors.\nWe will also strengthen our capabilities in data and cybersecurity. With increased digitalisation, data will become an important asset for firms, and strong cybersecurity is needed for our networks to function smoothly. The Cyber Security Agency (CSA) of Singapore will work with professional bodies to train cybersecurity professionals.\nI will make available more than $80 million for these programmes. The Minister for Communications and Information will elaborate at the COS.\nThe second way to strengthen our enterprises is to support firms in their broader efforts to tap on innovation and technology. With our consistent investment in R&D, we have built up excellent research institutes. We want to help companies better tap on these resources.\nA*STAR currently works with firms to conduct operation and technology road-mapping, to identify how technology can help them innovate and compete. A*STAR will expand its efforts to support 400 companies over the next four years.\nFor companies seeking access to intellectual property, Intellectual Property Intermediary, a SPRING affiliate, matches them with IP that meets their needs. It will work with the Intellectual Property Office of Singapore (IPOS) to analyse and bundle complementary IP from Singapore and overseas. A*STAR also partners SMEs through the Headstart Programme. The Headstart programme allows SMEs that co-develop IP with A*STAR to enjoy royalty-free and exclusive licences for 18 months in the first instance. In response to industry feedback, this will be extended to 36 months.\nWe will also support companies in the use of advanced machine tools for prototyping and testing, which may require costly specialised equipment. A*STAR will provide access to such equipment, user training and advice under a new Tech Access Initiative.\nThe Ministers for Trade and Industry will elaborate at the COS.\nThe third way to strengthen our enterprises is to help them scale up globally. Many Singapore-based firms already have a presence in other markets, often with support from IE Singapore. In 2016, IE Singapore supported companies in over 37,000 cases, a 9% increase from 2015.\nTo further support our firms to grow, we will continue to develop a smart financing ecosystem. We will commit up to $600 million in Government capital for a new International Partnership Fund. The Fund will co-invest with Singapore-based firms to help them scale-up and internationalise.\nEnhancement of Internationalisation Finance Scheme. \nAn important opportunity for our companies is the growing market for infrastructure development in emerging economies, especially in Asia. However, there remain gaps in financial markets for project finance in the region. The Government will enhance its schemes to bridge these gaps, by catalysing private finance and sharing risks with financial institutions. \nWe set up Clifford Capital in 2012 to finance overseas projects by Singapore companies. To date, over $2.4 billion have been committed.\nWe will enhance IE Singapore’s Internationalisation Finance Scheme to further support growth in this sector. We will catalyse private cross-border project financing to smaller Singapore-based infrastructure developers, by co-sharing the default risk of lower quantum non-recourse loans. We will also catalyse financing for projects undertaken by larger firms in higher-risk developing markets, by providing a share of the needed sovereign risk insurance coverage. Overall, these enhancements will enable more companies to take on more overseas projects. The Ministers for Trade and Industry will provide more details at the COS.\nTo sum up, these are the measures to help enterprises build capabilities to go international, go digital, and to innovate.\nNext, I will speak on how we will help our people deepen their capabilities.\nOur people are valued for their skills and adaptability, and have enjoyed high employment rates and rising wages. We must build on these strengths. As the pace of change quickens, we will do more to help them stay ahead. I spoke earlier about how we will support those affected by economic restructuring, to re-skill to find new jobs. I will now touch on two areas: new skills to operate overseas; and deepening skillsets to remain relevant in jobs.\nWe will set up a Global Innovation Alliance for Singaporeans to gain overseas experience, build networks, and collaborate with their counterparts in other innovative cities.\nThe Global Innovation Alliance will have three programmes.\nFirst, the Innovators Academy will enable our tertiary students to build connections and capabilities overseas. We will build on the NUS Overseas College programme, which connects students to start-ups overseas. Many of these students have gone on to start companies or pursue interesting careers. The Innovators Academy will go further by making these opportunities available to students from other Singapore universities. We aim to grow the annual intake of students from 300 to 500 over the next five years.\nSecond, we will establish Innovation Launchpads in selected overseas markets. These create opportunities for our entrepreneurs and business owners to connect with mentors, investors and service providers.\nThird, through Welcome Centres, innovative foreign companies can also link up with Singapore partners to co-innovate, test new products in Singapore, and expand in the region.\nThe Global Innovation Alliance is a novel collaboration among our educational institutions, economic agencies and businesses. In the initial phase, we will launch the Alliance in Beijing, San Francisco and various ASEAN cities. The Ministers for Trade and Industry will share more details at the COS.\nFirms that want to expand overseas need capable leaders who have spent time in these markets, with insights and connections that can help their businesses scale up globally. The SkillsFuture Leadership Development Initiative will support companies to groom Singaporean leaders by expanding leadership development programmes. This includes sending promising individuals on specialised courses and overseas postings. For a start, the programme will target to develop 800 potential leaders over the next three years.\nI will set aside over $100 million to build capabilities under the Global Innovation Alliance and Leadership Development Initiative.\nAs our companies innovate and digitalise, we will also help our people acquire and use deep skills, taking the SkillsFuture movement further.\nTo enhance training and make it more accessible, we will offer more short, modular courses, and expand the use of e-learning. Our universities, polytechnics and ITE have started offering such modular courses.\nFunding support for Singaporeans to take approved courses will continue to be available through SkillsFuture. In addition, union members can get subsidies for selected courses through the NTUC-Education and Training Fund. We have set aside $150 million to match donations to the Fund.\nBesides learning new skills, our people must also apply and use these skills on their jobs. This requires employers, Trade Associations and Chambers, or TACs, unions and the Government to work together.\nFirst, we must make sure that skilled workers are matched to where they can best use their skills. We will make the National Jobs Bank more useful for jobseekers and employers, and work with private placement firms to deliver better job matching services for professionals.\nSecond, employers, TACs, and unions should play an active role in structuring training for workers. Some have been successful in this effort. For example, SHATEC was set up by the Singapore Hotel Association over 30 years ago to provide hands-on training and certified courses. It has since helped to build up a skilled hospitality workforce, with its alumni winning accolades worldwide.\nWe hope more employers and TACs can do likewise. Employers and TACs who develop training programmes for their workers and the industry can receive funding support from SkillsFuture Singapore.\nI have spoken about how we will support the development of our enterprises and our people. Beyond developing individual capabilities, we must also come together in partnerships - share expertise, tackle common challenges and reinforce our mutual efforts.\nTo systematically facilitate such partnerships, I announced in last year’s Budget a major initiative, the Industry Transformation Maps, or ITMs. The ITMs are integrative platforms, bringing together various stakeholders – TACs, unions, and Government – so as to align our efforts around a common plan to transform each sector. We will develop ITMs for 23 sectors, covering about 80% of our economy. Six have already been launched. We will keep this going at a good pace, and launch the remaining 17 within FY2017.\nThe ITMs help us to identify key enablers, which involve different stakeholders, to transform sectors. For example, the Centre of Innovation for Supply Chain Management at Republic Polytechnic works with companies to level up their capabilities and provides students with hands-on experience.\nAs I said last year, the ITMs are “live” plans that we will adjust along the way. Where we spot opportunities, including ones that do not fit any existing industry, we will adapt our ITMs to seize them. We must also maximise synergies between related ITMs, such as between the Food Services and Hotel industries.\nOur companies, TACs and unions can play a key role in the success of our ITMs.\nOne example is Singtel. Singtel not only trains its IT services employees to transition into cyber security roles, it also works with CSA and the IMDA on the Cyber Security Associates and Technologists programme to develop mid-career talent for the broader cyber security industry. Singtel has also launched its Cyber Security Institute to train technical professionals, management and boards to better handle cyber breaches. It also engages students through internship programmes.\nWith the emphasis on innovation, Government agencies need to play enabling roles, to help realise new ideas.\nOur regulatory agencies must balance managing risk and creating the space to test innovations.\nFor example, the Monetary Authority of Singapore (MAS) has just announced a simplified regulatory framework tailored to the needs of venture capital firms. This will give them greater flexibility, making Singapore more conducive to venture capital investment, thereby enhancing the supply of financing for start-ups.\nWe can also create more space for innovation through regulatory sandboxes. This involves setting boundaries within which some rules can be suspended, to allow greater experimentation.\nThe Land Transport Authority (LTA) has done this with self-driving vehicles, setting out specific zones where they can be tested on roads. Likewise, MAS has set up a regulatory sandbox for FinTech.\nRegulatory agencies will further explore how we can facilitate innovation. For instance, our regulators can make their risk assessments for new products and services more swift and effective. A good example is the Health Sciences Authority (HSA), which will be setting up a priority review scheme to evaluate new and innovative medical devices. This will accelerate the commercialisation process and make Singapore a preferred location to launch these devices.\nWe will also support our agencies to procure products and services in a way that builds capabilities in the economy and supports innovation.\nFor example, in the Construction sector, we will introduce the Public Sector Construction Productivity Fund, with about $150 million. It will allow Government agencies to procure innovative and productive construction solutions, which may have higher costs as these solutions may be nascent and lack scale. The fund will allow these solutions to enter and gain traction in the market.\nIn addition to the funding for the measures mentioned earlier, I will top up the National Research Fund by $500 million, to support innovation efforts, and the National Productivity Fund by another $1 billion, to support industry transformation.\nAll in, we are putting aside $2.4 billion over the next four years to implement the CFE strategies. This will be over and above the $4.5 billion set aside last year for the Industry Transformation Programme.\nMadam Speaker, may I have your permission to distribute materials to the Members in this House?\nEven as the voices against globalisation rise, we must strive to remain a vibrant and well-connected city that is highly liveable for our people and businesses. In this way, our people will be constantly in touch with and be energised by new ideas, concepts, people, services and products from all over the world. We will continue to make significant investments in critical economic infrastructure such as the new Changi Airport Terminal 5, the Kuala Lumpur-Singapore High Speed Rail and the Tuas Terminal to deepen Singapore’s connectivity to global markets.\nWe will also invest in shared infrastructure for economic clusters, so that industry players can network, pool resources and share knowledge. Last year, I spoke about how the Jurong Innovation District would be an exciting development to live, work and innovate in. The upcoming growth cluster in Punggol will co-locate cyber security and digital industries, with industry collaborating closely with the nearby Singapore Institute of Technology.\nA vibrant city must also have diverse social spaces where people can come together, create shared experiences, and forge stronger bonds. We are currently working with the community to design the new Jurong Lake Gardens as our new national gardens in the heartlands. We will also continue to invest in more sports and arts facilities around the island. Beyond physical infrastructure, we are also enhancing sports and arts programmes. I will share more on these efforts later.\nOur vibrant and connected city must also be sustainable and resilient. A high-quality living environment endears the city to its residents and visitors.\nAround the world, the effects of climate change, and air and water pollution are worsening public health and quality of life. These harmful effects transcend national boundaries. As an island, Singapore is vulnerable to rises in sea level due to climate change. Together with the international community, we have to play our part to protect our living environment. In doing so, we secure a better future not only for ourselves, but for generations to come.\nSingapore has joined more than 130 countries, including China, Japan and South Korea, in having ratified the Paris Agreement, re-affirming our commitment to address climate change and reduce emissions. It is in our own interest to support the international coordination required to deal with an issue that affects all countries, and in particular, small island states like ours.\nThere are different ways to reduce emissions. One is to ensure consumers understand the effects of their actions. So we have energy efficiency labels, like ticks on air-conditioners or refrigerators. Another is to regulate for higher standards. Singapore has good environmental protection standards and the Ministry of the Environment and Water Resources continues to ensure that our regulations are up-to-date. But the most economically efficient and fair way to reduce greenhouse gas emissions is to set a carbon tax, so that emitters will take the necessary actions.\nSingapore has studied this option for several years. We intend to implement a carbon tax on the emission of greenhouse gases. We will consult widely with stakeholders, and aim to implement the carbon tax from 2019. The tax will generally be applied upstream, for example, on power stations and other large direct emitters, rather than electricity users.\nWe are looking at a tax rate of between $10 and $20 per tonne of greenhouse gas emissions. This is in the range of what other jurisdictions have implemented. It will create a price signal to incentivise industries to reduce their emissions, complementing the regulatory measures which we are also introducing. It will help us to achieve our commitments to reduce emissions under the Paris Agreement, do so efficiently and at as low a cost to the economy as possible. This may also spur the creation of new opportunities in green growth industries such as clean energy.\nRevenue from the carbon tax will help to fund measures by industries to reduce emissions. The impact of the carbon tax on most businesses and households should be modest.\nThe Government has started industry consultations and will continue to reach out.\nPublic consultations will begin in March. The final carbon tax and exact implementation schedule will be decided after our consultations and further studies. We will take into consideration the lessons from other countries and prevailing economic conditions in Singapore in implementation. We will also provide appropriate measures to ease the transition.\nLet me turn now to another source of environmental pollution – diesel. Apart from carbon emissions, diesel emits highly pollutive particulate matter and nitrogen oxides which are associated with an increased risk of lung cancer and respiratory infection. The over-use of diesel vehicles has resulted in cities like London, Paris and Rome being enveloped in smog in recent years. Many of these cities have started taking action to reduce these harmful emissions. Athens, Madrid, Mexico City and Paris have announced plans to ban diesel vehicles from their city centres by 2025. Singapore must learn from these hard lessons.\nToday, motor fuels such as petrol and compressed natural gas are taxed based on how much is used. This approach incentivises users to reduce consumption and manufacturers to develop more energy-efficient vehicles.\nHowever, for diesel, we levy a lump sum Special Tax on diesel cars and taxis, regardless of the amount of diesel used.\nI will therefore restructure diesel taxes. I will introduce a volume-based duty at $0.10 per litre on automotive diesel, industrial diesel and the diesel component in biodiesel. Taxing diesel according to usage incentivises users to reduce diesel consumption. At the same time, I will permanently reduce the annual Special Tax on diesel cars and taxis by $100 and $850 respectively. In this way, we shift from an annual amount of tax to one which is related to usage. These changes will take effect today.\nThe Special Tax reduction will offset the impact of diesel duty for the majority of drivers. I strongly urge taxi companies to pass on the Special Tax reduction to taxi drivers.\nTo help businesses adjust, I will provide 100% road tax rebate for one year, and partial road tax rebate for another two years, for commercial diesel vehicles. There will be additional cash rebates for diesel buses ferrying school children. For the large majority of vehicles, the first year’s rebates will more than offset the diesel duty incurred in the same period. Details of the duty changes and offset measures are in the Annex.\nI will also adjust two vehicle incentive schemes to encourage the use of cleaner vehicles.\nThe current Carbon Emissions-based Vehicle Scheme, CEVS, was implemented in 2013 to encourage take-up of cars and taxis with low carbon emissions. We will replace this with a new Vehicular Emissions Scheme, which will consider four other pollutants on top of carbon dioxide, so as to account more holistically for the health and environmental impact of vehicular emissions. With this scheme, we hope to nudge car buyers towards cleaner and environmentally-friendly models.\nThe new Vehicular Emissions Scheme will run for two years, starting from 1 January 2018. We will review it before it expires. In the interim, we will extend the current CEVS until 31 December 2017.\nWe will also enhance and extend the Early Turnover Scheme for commercial diesel vehicles. This Scheme was first introduced in 2013 to encourage the early replacement of older and more pollutive commercial diesel vehicles. Since then, vehicle owners have switched 27,000 vehicles to cleaner models.\nAs the Scheme is due to expire on 31 July 2017, we will extend the scheme for vehicle owners who turn over their existing Euro II and III commercial diesel vehicles for Euro VI vehicles until 31 July 2019, and further enhance the Certificate of Entitlement (COE) bonus period for Light Goods Vehicles.\nThe Minister for Transport and the Minister for the Environment and Water Resources will share more details at the COS.\nLet me now speak about water. Water sufficiency is a matter of national survival. Imported water and local catchment water currently meet more than half of our water demand, but both sources depend heavily on weather conditions. To meet increasing water demand and strengthen the resilience of our water supply, we have invested in desalination and NEWater plants. These are costly but necessary investments which we must continue to make.\nWe have priced water to reflect the higher costs of desalination and NEWater production because every additional drop of water has to come from these two sources. The cost of water production and transmission has increased as we build more desalination and NEWater plants, and lay deeper pipes through an urbanised environment. Water prices were last revised in 2000, almost 20 years ago. We need to update our water prices to reflect the latest costs of water supply.\nWe will increase water prices by 30% in two phases, starting from 1 July 2017. As part of the exercise, we will be restructuring the Sanitary Appliance Fee and the Waterborne Fee into a single, volume-based fee. This is more reflective of the volume of used water discharged. The details are in the Annex.\nFor three-quarters of our businesses, the increase will be less than $25 per month, once the increase is fully phased in on 1 July 2018. For 75% of households, the increase in monthly water bills will be less than $18. We will have measures to help lower- and middle-income households manage this increase. I will give details later.\nToday, we impose a Water Conservation Tax on potable water, to promote conservation. To encourage the conservation of NEWater among industrial users, we will also impose a Water Conservation Tax on NEWater, which will be 10% of the NEWater tariff, starting from 1 July 2017.\nThe various emissions and water-related measures in Budget 2017 are necessary to protect our living environment. These will help keep our home in good shape for future generations. It is the right thing to do, even though some of these measures will lead to increases in household costs. The Government will help households, especially low income ones.\nBefore I speak on these household support measures, let me first speak on our measures to build a caring and inclusive society.\nWe are building an inclusive and caring society – one where Singaporeans of all backgrounds can improve their lives, where the vulnerable are uplifted, and where people of all ages can look to the future with optimism.\nThe Government will continue to support families, help the needy and catalyse community efforts. In the last decade, we have significantly increased our social spending and strengthened safety nets. Many Singaporeans are also stepping forward to help others in need.\nHowever, our needs are growing. Our population is ageing, families are smaller and our social needs are more complex. Even as the Government strengthens direct support, more people need to help out. In many areas, the human connection is critical – taking care of loved ones and neighbours, volunteering for worthy causes, organising community events or raising donations. We must strengthen the gotong-royong spirit where each one of us does our part to help one another.\nLet me first speak about what we will do to keep Singapore a Great Place for Families. In the past two years, we enhanced parental leave and grants for new-borns.\nIn this Budget, we will provide additional support for families in the areas of housing, pre-school and post-secondary education.\nMany couples start their lives together by applying for a BTO flat which is highly subsidised.\nFor others, a resale flat may better meet their needs. Some may wish to live near their parents. Some may prefer to move earlier into their own home. To help them, we will increase subsidies for those who buy their first HDB home from the resale market.\nWith immediate effect, we will increase the CPF Housing Grant from $30,000 to $50,000 for couples who purchase 4-room or smaller resale flats, and from $30,000 to $40,000 for couples who purchase 5-room or bigger resale flats. Together with the Additional CPF Housing Grant and Proximity Housing Grant, a couple can now receive a total of up to $110,000 in housing grants when buying a resale flat, depending on the flat location, flat type and their income. Other eligible first-timers will also benefit from some grant enhancement.\nThis measure will cost an additional $110 million per year. The Minister for National Development will announce other measures to help young families get their first home at the COS.\nWe will improve the accessibility of pre-school.\nOver the last five years, we increased childcare places by over 40%, to about 140,000. Now, there are enough places for more than half of all children between 18 months and six years of age. All receive Government subsidies.\nWe will provide more support for those with infants, that is, children under 18 months of age. At present, about 4,000, or 8% of all infants are enrolled in centre-based infant care. To meet growing demand, we will increase the capacity of centre-based infant care to over 8,000 places by 2020.\nThe Minister for Social and Family Development will give more details at the COS. The Senior Minister of State in charge of Population issues at the Prime Minister’s Office will also speak about our efforts to keep Singapore a Great Place for Families.\nWe will increase annual bursary amounts for those attending Post-Secondary Education Institutions, or PSEIs. The amount of increase will be up to $400 for undergraduate students, up to $350 for diploma students, and up to $200 for ITE students. For ITE students, existing bursaries already more than cover their course fees.\nNext, we will extend PSEI bursaries to more families, by revising the income eligibility criteria. About 12,000 more Singaporean students are expected to benefit, bringing the total number of beneficiaries to 71,000.\nIn total, PSEI bursaries will increase from about $100 million to $150 million per year.\nThe Minister for Education (Higher Education and Skills) will provide more details at the COS.\nLet me now speak of how we will assist households with their expenses.\nEarlier, I said that we would help households offset some of the increase in water prices. We will increase the GST Voucher – U-Save Rebate for eligible HDB households, by an amount ranging from $40 to $120, depending on flat type, as seen in the table. This increase will be permanent.\nFamilies living in 1- and 2-room HDB flats will receive $380 of U-Save rebates each year compared to $260, while families living in 3- and 4-room HDB flats will receive $340 and $300 per year respectively, compared to $240 and $220.\nTaking into account these higher U-Save rebates, 75% of all HDB households will see an average increase of less than $12 in their monthly water expenses. 1- and 2-room HDB households will on average have no increase in their water expenses.\nAbout 880,000 HDB households will benefit. This will cost an additional $71 million each year.\nThe U-Save rebate will soften the impact of the water price increase. Even as we provide this assistance, we should not lose sight of the scarcity of water, and thus should conserve it.\nTo help lower income households with expenses, we will provide a one-off GST Voucher – Cash Special Payment of up to $200 for eligible GST Voucher – Cash recipients.\nThis is in addition to the regular GST Voucher – Cash. In total, eligible Singaporeans can receive up to $500 in cash for 2017, as seen in the table.\nThe one-off Special Payment will cost about $280 million and benefit more than 1.3 million Singaporeans.\nWe will also extend the Service and Conservancy Charges (S&CC) rebate, and raise it by 0.5 months for FY2017. As seen in the table, this year, we will provide 1.5 to 3.5 months of S&CC rebate to eligible HDB households. 1- and 2-room HDB households will receive a total of 3.5 months of rebates, while 3- and 4-room households will receive 2.5 months of rebates.\nThis will cost the Government $120 million and benefit about 880,000 HDB households.\nFinally, I will give a Personal Income Tax Rebate of 20% of tax payable, capped at $500, for tax residents for YA2017 (i.e. for income earned in 2016). This will give households a reduction in their tax bills for this year. The rebate will cost the Government $385 million.\nIn total, we will provide additional support of over $850 million this year to help households with their expenses.\nTo support the increase in U-Save I announced earlier and other future GST Voucher payments, I will make a $1.5 billion top up to the GST Voucher Fund.\nSince the last top-up in FY2013, Medifund utilisation has increased by an average of 9% per year from FY2013 to FY2015. I will therefore also top up Medifund by $500 million, bringing the total fund size to $4.5 billion.\nIn addition, I will top up the ComCare Fund by $200 million. This fund supports families and individuals in need.\nI will now speak on measures to foster a caring and inclusive society.\nAll of us can play a part in our communities. All of us have something to offer, be it time, expertise or the extra attention, to care for each other. It takes all of us to build an inclusive society. Let me first touch on how we will increase support for Persons with Disabilities and people with mental health conditions.\nThe Enabling Masterplans are 5-year national plans to support persons with disabilities and their caregivers. Under the two previous Masterplans, we increased support in areas such as early intervention and education, employment, care services, assistive technologies and accessibility.\nWe will launch the Third Enabling Masterplan, which was put together by a committee of private and public sector representatives. The Masterplan calls for the Government and the community to better integrate Persons with Disabilities into the workforce, and to give more support to their caregivers.\nToday, higher-functioning graduands from Special Education schools who can work are matched to special training programmes, to prepare them for employment. We will make these training programmes available to not only graduands with mild intellectual disability and autism, but also those with moderate intellectual and multiple disabilities.\nTo support caregivers, we will set up a Disability Caregiver Support Centre to provide information, planned respite, training and peer support groups. The Centre will also work with VWOs to pilot programmes catering to caregivers of newly-diagnosed Persons with Disabilities.\nIncluding existing initiatives, we expect to spend around $400 million per year on initiatives supporting Persons with Disabilities. The Minister for Social and Family Development will elaborate at the COS.\nWe must also rally around those with mental health conditions, including dementia.\nMental health issues may not be easy to talk about, but we can make good progress when the community comes together.\nSince 2012, MOH has partnered VWOs to set up 36 teams that provide outreach and allied health support in the community. Last year, MOH also launched Dementia Friendly Communities in Hong Kah North, MacPherson and Yishun. These communities are networks of residents, businesses and services trained to look out for and help those with symptoms of dementia.\nWe will resource VWOs to set up more community-based teams to support those in need, as well as educate the public on mental health issues. MOH will provide mental health care services in polyclinics, as part of its broader effort to improve the delivery of care within the community.\nWe will also involve the wider community, and expand the number of Dementia Friendly Communities. The National Council for Social Services will also lead efforts to integrate persons with mental health issues at the workplace and in wider society.\nAs part of our Community Mental Health efforts, we will spend an additional $160 million in the next five years. The Minister for Health will elaborate during the COS.\nBeyond supporting families and those with greater needs, this Budget also aims to strengthen community bonds.\nSince 2002, the VWOs-Charities Capability Fund has helped VWOs and charities train their staff, expand their reach, and serve people better. So far, we have committed over $180 million of funding. This has helped around 400 VWOs and charities. Over the next five years, we will provide additional funding, up to $100 million in total, to further develop the capabilities of our VWOs and charities. The Minister for Social and Family Development will provide further details at the COS.\nOur Self-Help Groups have also been doing good work, working closely with the community to help needy families and children. To enable them to do more, I will provide an additional $6 million grant to the Self-Help Groups over the next two years.\nOne way we bring our society together is through sports – be it playing sports with friends and neighbours, or cheering on our sportsmen and women. Last year, we cheered our Olympic and Paralympics athletes who gave their very best, and did us proud. It was Team Singapore at its finest!\nWe will do more to make it easier for all Singaporeans to participate in sports. Over $50 million has been set aside to support community sports. We will expand the Sports-In-Precinct Programme, so that more Singaporeans can play sports near their homes. We will also expand the SportCares Programme, which encourages disadvantaged youth to discover their strengths through sports.\nTo help aspiring athletes reach their full potential, we will commit an additional $50 million in grants over five years. On top of that, we will provide up to $50 million to match sports donations dollar-for-dollar. This will build a wider base of support for Team Singapore. The Minister for Culture, Community and Youth will provide more details at the COS.\nLike sports, cultural activities build bonds among our people. Increasingly, many people not only visit museums and attend performances, but also participate actively as docents and donors. The Cultural Matching Fund currently provides dollar-for-dollar matching for donations to cultural institutions. Since the Fund was implemented in 2014, about $150 million has been committed, and donations to arts and heritage causes have more than doubled.\nTo sustain this momentum, I will top up the Cultural Matching Fund by $150 million.\nBudget 2017 is an investment in our economic transformation and social resilience. We will continue to invest in security measures to keep Singaporeans safe, and in infrastructure to support economic growth and improve quality of life.\nWe have been able to commit to these medium-term investments, because we have a sustainable fiscal system. Previous generations planned ahead and set aside savings when our economy was growing rapidly. We now benefit from the returns on our reserves.\nAt the same time, our pro-growth and progressive tax system has given us a steady revenue stream. Together, these have allowed us to fund new priorities without cutting back in essential areas.\nIn the coming years, we expect expenditure needs to rise rapidly, particularly in healthcare and infrastructure.\nOver the last five years, our annual healthcare spending has more than doubled to around $10 billion in FY2016 as we enhanced subsidies, and expanded healthcare services. Healthcare spending will continue to rise as our population ages.\nWe will also continue to enhance our public transport infrastructure, almost doubling the MRT network by 2030. This will put 8 in 10 households within a 10-minute walk of a rail station. This alone is expected to cost the Government more than $20 billion over the next five years.\nThe new Changi Terminal 5 is another critical national infrastructure that will cost tens of billions of dollars. Changi Airport is our gateway to the world. By enhancing our connectivity, we expand opportunities for our people.\nIn the longer term, we must also prepare to upgrade and renew existing infrastructure, to keep Singapore a good home. As these projects are costly, we have to plan ahead.\nWith our spending needs increasing, the Government must continue to spend judiciously, emphasise value-for-money and drive innovation in delivery. We can do better – and more – with less.\nWe will apply a permanent 2% downward adjustment to the budget caps of all Ministries and Organs of State from FY2017 onwards, to emphasise the need to stay prudent and effective. For four ministries that are serving security needs, or significantly expanding their services – namely Home Affairs, Defence, Health and Transport – the 2% adjustment will be phased in over FY2017 and FY2018.\nSome of the funds will be used for cross-agency projects that deliver value to citizens and businesses. Examples include initiatives by the Municipal Services Office.\nMadam Speaker, apart from managing our resources prudently, we must grow our revenues to finance our growing expenditures.\nGrowing our economy is the first and most important step to increasing our revenues sustainably. We need to achieve this growth by implementing the strategies set out in the CFE.\nNext, we need to strengthen our revenue base in a pro-growth and progressive manner.\nOver the past five years, we have revised our tax structure as well as the Net Investment Returns framework to ensure that we have sufficient revenue for our increased spending needs till 2020.\nLike all Finance Ministers before me, it is my duty to take the long view. Our domestic needs will grow over time, and the global environment will shift. We must study the implications and prepare our options early.\nOne international development affecting tax systems worldwide is the Base Erosion and Profit Shifting, or BEPS, project. The BEPS project seeks to ensure that companies are taxed where substantive economic activities are performed. Singapore supports this principle. We are, in consultation with businesses, refining our schemes and implementing the relevant standards.\nCountries, large and small, are also reviewing their corporate tax regimes to keep them competitive. With increasing digital transactions and cross-border trade, some countries have taken steps to adjust their GST system, to ensure a level playing field between their local businesses which are GST-registered, and foreign-based ones which are not. We are studying how we can do likewise.\nDomestically, we will also face rising expenditures over the longer term, as we invest more in healthcare and infrastructure. We will have to raise revenues through new taxes or raise tax rates. We are studying the options carefully. We must make these decisions in good time, to ensure that our future generations remain on a sustainable fiscal footing. As the Chinese idiom goes, 未雨綢繆, let us thatch the roof before it rains.\nFinally, let me move on to the remaining tax measures in this Budget.\nI will extend and strengthen tax incentives to enhance our business competitiveness, such as in the financial and global trading sectors. The details are in the Annex.\nI shall introduce refinements to vehicle taxes for motorcycles. Today, all motorcycles incur the same Additional Registration Fee, or ARF, at 15% of their Open Market Value (OMV). A small but rising number of buyers are buying expensive motorcycles – their motorcycles have OMVs similar to those of small cars. Just as we introduced tiers to the ARF for cars in 2013 to improve progressivity, I will introduce two more tiers for more expensive motorcycles. The ARF for motorcycles with OMV up to $5,000 will remain at the current 15%. The next $5,000 of motorcycle OMV will be subject to an ARF rate of 50%. The remaining motorcycle OMV beyond $10,000 will be subject to an ARF rate of 100%.\nBased on today’s registration trends, we expect that more than half of new motorcycle buyers will continue to pay the current ARF rate of 15%.\nThe tiered ARF will apply to motorcycles registered with COEs obtained from the second February COE bidding exercise onwards. Details are in the Annex.\nAs a complementary measure, the Ministry of Transport will cease the contribution of motorcycle COE quota to open category COE quota. This will help address the gradual decline in motorcycle population, as very few open category COEs have been used to register motorcycles.\nLet me now summarise our overall budget position. For FY2016, we expect a budget surplus of $5.2 billion, or 1.3% of GDP. This is higher than the surplus of $3.4 billion, or 0.8% of GDP, budgeted a year ago.\nNevertheless, when we exclude Government’s top-ups to funds and Net Investment Returns Contribution from past reserves, we expect a basic deficit of $5.6 billion, or 1.4% of GDP. FY2016 was hence an expansionary budget.\nIn FY2017, the Government’s budget remains expansionary. Ministries’ expenditures are expected to be $3.7 billion, or 5.2%, higher than in FY2016.\nOverall, a smaller budget surplus of $1.9 billion, or 0.4% of GDP, is expected in FY2017. As we expect expenditures to continue rising in the long term, this budget position is prudent, while supporting firms and households in the midst of continued economic restructuring.\nMadam Speaker, let me conclude. Budget 2017 outlines how we can thrive in an uncertain and rapidly changing world. It is a call for us to pull together – the Government, firms, unions, community organisations and individuals, with everyone doing his part. Our bonds will help us develop greater resilience in the face of unexpected shifts and improve our ability to adapt.\nOn the economic front, Budget 2017 sets out some support measures to address near-term concerns, particularly for sectors currently facing cyclical headwinds. It also sets out how we will implement our medium-term economic strategies to create new opportunities and jobs. We will build capabilities in our firms and workers – so they can operate well internationally, use digital technology and innovate. We will strengthen partnerships across the economy, with Government playing an enabling role and drawing different stakeholders together to align efforts\nBudget 2017 also reaffirms our commitment to keep Singapore a vibrant city with a quality living environment. We will study a carbon tax, change the tax structure for diesel, and adjust vehicle incentive schemes. We will also increase the price of water to reflect the latest costs of water supply. At the same time, we will provide support to lower income households.\nOn the social front, Budget 2017 supports families who wish to buy a home, enjoy the joys of parenthood and help their children through post-secondary education. It also takes further steps towards building an inclusive society, for Persons with Disabilities and those with mental health conditions. Just as importantly, this Budget will strengthen community bonds and partnerships among Government, community organisations and individuals, to build a better society together.\nI want to take a moment to applaud the good work done by the thousands of volunteers, working in partnership with Government agencies. Government schemes and funding can play a supporting role, but it is ultimately human relationships and community bonds that build a society. Only through concerted partnership between Government and people can we realise our vision of a caring and inclusive society.\nWe will do all this while maintaining fiscal discipline. This will lay a sustainable foundation for future generations to thrive.\nOur future is full of promise. Singaporeans are chasing their dreams in various fields and are excelling on the international stage.\nMathew Tham topped the Young Chef Olympiad.\nDing Eu-Wen won an international design award for his smart bicycle helmet.\nNathan Hartono did us proud at the Sing! China competition.\nNur Syahidah Alim won double gold medals in archery at the ASEAN Para Games.\nYip Pin Xiu won multiple medals at the Paralympics, and Joseph Schooling, was our first Olympic gold medallist.\nThis strong, can-do spirit will serve us well in the years ahead.\nLet us go forward together.\nMadam Speaker, I beg to move."}]
};

var current_duration_speech = '1hr 34min 05 sec';
var before_duration_speech = ' 1hr 55min 18 sec';


data.words = [];
data.categories = {};
data.twitter = [];
var year_default = 2016;
var option_t = '<option value="[VALUE]">[TEXT]</option>';
var expresion_search_n = /(?:\n)/g;
var a_all_data_desordenada = {};
var a_all_data_ordenada_etiq = {};
var a_all_data_category_ordenada = {};
var width_text_filter = 0;
var height_text_filter = 0;
var height_tag_scale = 0;
var height_text_scale;
var check_is_filtered = 0;
var just_one_cal = 0;
var last_position_minimap = 0;
var direction_mov = 0;
var action_scroll = 1;
var hash_word = window.location.hash;



var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
    FB.init({
        appId: fbAppId, // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the
        // server to access the session
        xfbml: true, // parse page for xfbml or html5
        // social plugins like login button below
        version: 'v2.0', // Specify an API version
    });

    // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// variables to animate button option
var pathDefs = {
    fill: ['M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809']
};
var animDefs = {
    fill: {
        speed: .8,
        easing: 'ease-in-out'
    }
};


var path_csv_categories = 'csv/categories.csv';
var path_csv_sgbudget = "csv/sgbudget.csv";
$(document).ready(function() {
    // $(".st_menu_mobile").on('click', function() {
    //     $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
    //     $(".st_content_shared_social").toggleClass('st_dialogIsOpen');

    //     eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
    // });

    $(".c-read-paragraphs").on('click', function() {
        var copy_head = $(this).parents('.head-data-minister');
        if (!copy_head.hasClass('speech_active')) {
            $('.head-data-minister').each(function() {
                if ($(this).hasClass('speech_active')) {
                    $(this).removeClass('speech_active');
                    $(this).addClass('speech_no_active');
                } else if ($(this).hasClass('speech_no_active')) {
                    $(this).removeClass('speech_no_active');
                    $(this).addClass('speech_active');
                }
            });
            $(".content-detail-minister").each(function() {
                if ($(this).hasClass('speech_active')) {
                    $(this).removeClass('speech_active');
                    $(this).addClass('speech_no_active');
                } else if ($(this).hasClass('speech_no_active')) {
                    $(this).removeClass('speech_no_active');
                    $(this).addClass('speech_active');
                }
            });
        }
    });

    d3.selectAll(".list-option-radio div").each(function() {
        d3.select(this).append('svg').attr('viewBox', '-7 -7 110 110');
    });

    document.getElementById("rad-speechs").checked = true;
    var svg_from_rad = d3.select(d3.select("#rad-speechs").node().parentNode).select('svg');
    draw_radio_button(svg_from_rad, animDefs);


    d3.selectAll(".rad-speech").on("change", function() {
        d3.selectAll('.list-option-radio path').remove();

        var svg_from_rad = d3.select(this.parentNode).select('svg');
        draw_radio_button(svg_from_rad, animDefs);
        if (document.getElementById("rad-speechs").checked) {
            $(".group_text_map,.div_head_twitter").removeClass('content_a');
            $(".content_tweet,.div_head_twitter").addClass('content_a');
            $(".refresh_display_filter").trigger('click');
        } else {
            $(".group_text_map,.div_head_twitter").addClass('content_a');
            $(".content_tweet,.div_head_twitter").removeClass('content_a');
        }
    });

    $(".btn_shared_face").on('click', function(e) {
        e.preventDefault();
        var image = 'http://graphics.straitstimes.com/STI/STIMEDIA/facebook_images/budget-2017-statement/statement.png';
        var name = "Singapore Budget 2017: Analysing the Finance Minister’s Budget statement";
        var description = " Finance Minister Heng Swee Keat delivered his Budget statement on Feb 20, 2017. Read the full speech below and compare it with his speech from last year.";

        share_face_book(image, name, description);
        return false;
    });

    $(".btn_shared_twitter").on('click', function(e) {
        e.preventDefault();
        var text = "Check out this word analysis tool for %23HengSweeKeat’s %23sgbudget2017 speech";
        var via = 'STcom';
        var url = 'http://str.sg/budget17statement';
        share_twitter(text, via, url);
        return false;
    });
    $(".go_to_st").on('click', function() {
        window.open("http://www.straitstimes.com/", "_blank");
    });

    data.add_word = function(name) {
        var obj_parraf_word = {};
        var o_words = {};
        o_words.words = name;
        o_words.category = 'Libre';
        $.each(data.speechs, function(year) {
            var a_all_word_by_year = [];
            a_all_word_by_year.push(data.map_word(o_words, year));
            obj_parraf_word[year] = a_all_word_by_year;
        });
        construct_all_text_div(obj_parraf_word, name);
    };


    data.map_word = function(o_word, year, a_category, category_name) {
        var count_total_words = 0;
        var count_total_word_category = 0
        var a_u_parraf_copy = [];
        var a_u_parraf = [];
        var a_u_parraf_category = [];
        data.speechs[year].forEach(function(speech_info) {
            var res_exec = {};
            var lastIindex = res_exec.index = 0;
            while (res_exec = expresion_search_n.exec(speech_info.speech)) {
                if (res_exec.index > lastIindex) {
                    var text_cut = speech_info.speech.substring(lastIindex, res_exec.index);
                    a_u_parraf.push(text_cut);
                }
                lastIindex = expresion_search_n.lastIndex;

                if (a_category !== undefined && category_name !== undefined) {
                    var obj_parraf_process = {};
                    obj_parraf_process.u_parraf = text_cut;
                    obj_parraf_process.quantity_word = 0;
                    a_category.forEach(function(word_by_category) {
                        var o_data_parraf_by_word = data.generate_parraf_by_text(obj_parraf_process.u_parraf, word_by_category);
                        obj_parraf_process.u_parraf = o_data_parraf_by_word.u_parraf;
                        obj_parraf_process[word_by_category] = o_data_parraf_by_word.quantity_word;
                        obj_parraf_process.quantity_word += o_data_parraf_by_word.quantity_word;
                    });
                    a_u_parraf_category.push(obj_parraf_process);
                    count_total_word_category += obj_parraf_process.quantity_word;
                } else {
                    var o_data_parraf_by_word = data.generate_parraf_by_text(text_cut, o_word.words);
                    a_u_parraf_copy.push(o_data_parraf_by_word);
                    count_total_words += o_data_parraf_by_word.quantity_word;
                }
            }

            var text_cut = speech_info.speech.substring(lastIindex, speech_info.speech.length);
            if (text_cut !== '') {
                if (a_category !== undefined && category_name !== undefined) {
                    var obj_parraf_process = {};
                    obj_parraf_process.u_parraf = text_cut;
                    obj_parraf_process.quantity_word = 0;
                    a_category.forEach(function(word_by_category) {
                        var o_data_parraf_by_word = data.generate_parraf_by_text(obj_parraf_process.u_parraf, word_by_category);
                        obj_parraf_process.u_parraf = o_data_parraf_by_word.u_parraf;
                        obj_parraf_process[word_by_category] = o_data_parraf_by_word.quantity_word;
                        obj_parraf_process.quantity_word += o_data_parraf_by_word.quantity_word;
                    });
                    a_u_parraf_category.push(obj_parraf_process);
                    count_total_word_category += obj_parraf_process.quantity_word;
                } else {
                    a_u_parraf.push(text_cut);
                    var o_data_parraf_by_word = data.generate_parraf_by_text(text_cut, o_word.words);
                    a_u_parraf_copy.push(o_data_parraf_by_word);
                    count_total_words += o_data_parraf_by_word.quantity_word;
                }
            }
        });
        if (a_category !== undefined && category_name !== undefined) {
            return {
                count_total_words: count_total_word_category,
                word: category_name,
                a_parraf_copy: a_u_parraf_category
            };
        } else {
            a_all_data_desordenada[year] = a_u_parraf;
            return {
                count_total_words: count_total_words,
                word: o_word.words.trim(),
                a_parraf_copy: a_u_parraf_copy,
                category: o_word.category.trim()
            };
        }
    };

    data.generate_parraf_by_text = function(u_parraf, text) {
        if (u_parraf != '') {
            var expresion_search_words = new RegExp("\\b(" + d3.requote(text.trim()) + ")\\b", "gi");
            var res_exec_p = {};
            var lastIndex_p = res_exec_p.index = 0;
            var count_word_by_parraf = 0;
            var cut_bucle = 0;
            while (res_exec_p = expresion_search_words.exec(u_parraf)) {
                count_word_by_parraf++;
                if (cut_bucle === 0) {
                    lastIndex_p = expresion_search_words.lastIndex;
                    var paragraphs_u_copy = u_parraf.replace(expresion_search_words, '<span class="word_select_extern">$1 <span class="cdr_total"></span></span>');
                    expresion_search_words.lastIndex = lastIndex_p;
                    cut_bucle++;
                }
            }
        }
        return {
            quantity_word: count_word_by_parraf,
            u_parraf: (count_word_by_parraf === 0) ? u_parraf : paragraphs_u_copy
        };
    };

    data.count_words_by_text = function(text_complete) {
        var regex = /\s+/gi;
        return text_complete.trim().replace(regex, ' ').split(' ').length;
    };


    d3.csv(path_csv_sgbudget, function(error, data_csv) {
        data.twitter = data_csv;
        if (hash_word) {
            var word_selected = hash_word.split("#")[1].trim();
            construct_all_tweet_div(data.twitter, word_selected);
        } else {
            construct_all_tweet_div(data.twitter);
        }
    });



    d3.csv(path_csv_categories, function(error, data_csv) {
        var data_csv_u = data_csv;
        $('.chosen-select-words').append(option_t.replace('[VALUE]', '').replace('[TEXT]', ''));
        $('.chosen-select-categories').append(option_t.replace('[VALUE]', '').replace('[TEXT]', ''));

        // $("#div_category").html("");

        var a_categories_compare = [];
        var option_group_before = "<optgroup label='" + (year_default - 1) + "'>";
        var option_group_current = "<optgroup label='" + year_default + "'>";
        var a_words_split,c_year;
        data_csv_u.forEach(function(o_word) {
            if (o_word.words.trim()) {
                a_words_split = o_word.words.split('-');
                c_year = parseInt(a_words_split[1].trim());

                o_word.words = a_words_split[0];
                var unit_word = "<div class='unit_word pull-left' data-word='" + o_word.words.trim() + "' >";
                unit_word += "<div>" + o_word.words.trim() + "</div>";
                unit_word += "</div>";



                if (c_year === year_default - 1) {
                    $("#div-before-year").append(unit_word);
                    option_group_before += option_t.replace('[VALUE]', o_word.words.trim()).replace('[TEXT]', o_word.words.trim());
                } else if (c_year === year_default) {
                    $("#div_words").append(unit_word);
                    $("#div-current-year").append(unit_word);
                    var opt = option_t.replace('[VALUE]', o_word.words.trim()).replace('[TEXT]', o_word.words.trim());
                    option_group_current += opt;
                    $("#chosen-select-words").append(opt);
                }

                data.words.push({
                    name: o_word.words.trim(),
                    number: -1
                });
            }

            if (o_word.category) {
                if ($.inArray(o_word.category, a_categories_compare) < 0) {
                    var unit_category = "<div class='unit_category pull-left' data-cat='" + o_word.category.trim() + "' >";
                    unit_category += "<div><img src='images/categories/" + o_word.category.trim() + ".svg' class='img-responsive center-block'  /></div>";
                    unit_category += "<div>" + o_word.category.trim() + "</div>";
                    unit_category += "</div>";
                    // $("#div_category").append(unit_category);

                    $('.chosen-select-categories').append(option_t.replace('[VALUE]', o_word.category.trim()).replace('[TEXT]', o_word.category.trim()));
                    a_categories_compare.push(o_word.category.trim());
                    data.categories[o_word.category.trim()] = {
                        name: o_word.category.trim(),
                        number: -1,
                        words: [o_word.cat_words.trim()],
                    }
                } else {
                    data.categories[o_word.category.trim()].words.push(o_word.cat_words.trim());
                }
            }
        });
        option_group_before += "</optgroup>";
        option_group_current += "</optgroup>";

        $('#chosen-select-words2').append(option_group_before);
        $('#chosen-select-words2').append(option_group_current);

        $.each(data.speechs, function(year, text_speech) {
            var a_all_word_by_year = [];
            var a_all_word_category_by_year = [];
            data_csv_u.forEach(function(o_word) {
                if (o_word.words) {
                    var obj_etiq = data.map_word(o_word, year);
                    a_all_word_by_year.push(obj_etiq);
                }
            });

            a_all_data_ordenada_etiq[year] = a_all_word_by_year;

            $.each(data.categories, function(head, bod) {
                var obj_etiq_category = data.map_word('', year, bod.words, head);
                a_all_word_category_by_year.push(obj_etiq_category);
            });
            a_all_data_category_ordenada[year] = a_all_word_category_by_year;
        });

        if (hash_word) {
            var word_selected = hash_word.split("#")[1].trim();
            data.add_word(word_selected);
        } else {
            construct_all_text_div(a_all_data_desordenada);
        }

        $(".unit_word").on('click', function() {
            if (!$(this).hasClass('wordActive')) {
                $(".unit_word").removeClass('wordActive');
                $(this).addClass('wordActive');
                var word_selected = $(this).data("word");
                if (word_selected) {
                    construct_all_text_div(a_all_data_ordenada_etiq, word_selected);
                    construct_all_tweet_div(data.twitter, word_selected);
                    $(".unit_category").removeClass('categoryActive');
                }
            }
        });

        $(".unit_category").on("click", function() {
            if (!$(this).hasClass('categoryActive')) {
                $(".unit_category").removeClass('categoryActive');
                $(this).addClass('categoryActive');
                var category_selected = $(this).data("cat");
                if (category_selected) {
                    construct_all_text_div(a_all_data_category_ordenada, category_selected.trim(), true);
                    construct_all_tweet_div(data.twitter, category_selected, true);
                    $(".unit_word").removeClass('wordActive');
                }
            }
        });
        $('#tab-mob a').click(function(e) {
            e.preventDefault();
            $(".refresh_display_filter").trigger('click');
        });
        $('#tab-desk a').click(function(e) {
            e.preventDefault();
            $(".refresh_display_filter").trigger('click');
            $(".unit_word").removeClass('wordActive');
            $(".unit_category").removeClass('categoryActive');
        });

        $('.chosen-select-words').chosen({
            no_results_text: "Oops, nothing found!",
            allow_single_deselect: true,
            disable_search: true
        }).on('change', function() {
            var word_selected = $(this).find('option:selected').val();
            if (word_selected) {
                construct_all_text_div(a_all_data_ordenada_etiq, word_selected);
                construct_all_tweet_div(data.twitter, word_selected);

                $(".word_searched_hide").data('typesearchcategory', 0);
                $(".word_searched_hide").val(word_selected.trim());

                $('.input_word_extern').val('');
                $('.chosen-select-categories').val('').trigger("chosen:updated");

                // $('.chosen-select-words').trigger('chosen:close');
                // $('.chosen-container .chosen-drop input').blur();
            } else {
                $(".refresh_display_filter").trigger('click');
            }
            return false;
        });

        $('.chosen-select-categories').chosen({
            no_results_text: "Oops, nothing found!",
            allow_single_deselect: true,
            disable_search: true
        }).on('change', function(e, d) {
            var word_selected = $(this).find('option:selected').val();
            if (word_selected.trim()) {
                $(".word_searched_hide").data('typesearchcategory', 1);
                $(".word_searched_hide").val(word_selected.trim());

                construct_all_text_div(a_all_data_category_ordenada, word_selected.trim(), true);
                construct_all_tweet_div(data.twitter, word_selected.trim(), true);
                $('.input_word_extern').val('');
                $('.chosen-select-words').val('').trigger("chosen:updated");
            } else {
                $(".refresh_display_filter").trigger('click');
            }
            return false;
        });

        $(".chosen-select-words,.chosen-select-categories").on('chosen:hiding_dropdown', function() {
            var var_this = this;
            hide_keyboorad(var_this);
        });
        $(".chosen-select-words,.chosen-select-categories").on('chosen:showing_dropdown', function() {
            var var_this = this;
            $(var_this).parent().find('input').focus();
        });

    });

    $(".input_word_extern").on('keyup', function(e) {
        if (e.keyCode === 13) {
            $(".add_word_filter").trigger('click');
        } else if ($(this).val() === '') {
            construct_all_text_div(a_all_data_desordenada);
            construct_all_tweet_div(data.twitter);
        }
        return false;
    });
    $(".add_word_filter").on('click', function() {
        if ($($(".input_word_extern")[0]).is(':visible')) {
            var word_enter = $($(".input_word_extern")[0]).val();
        } else if ($($(".input_word_extern")[1]).is(':visible')) {
            var word_enter = $($(".input_word_extern")[1]).val();
        }
        if (word_enter.trim()) {
            $(".word_searched_hide").data('typesearchcategory', 0);
            $(".word_searched_hide").val(word_enter.trim());

            data.add_word(word_enter.trim());
            construct_all_tweet_div(data.twitter, word_enter.trim());

        } else if (word_enter.trim() === '') {
            construct_all_text_div(a_all_data_desordenada);
            construct_all_tweet_div(data.twitter);
        }
        $('.chosen-select-categories').val('').trigger("chosen:updated");
        $('.chosen-select-words').val('').trigger("chosen:updated");
        return false;
    });

    $(".refresh_display_filter").on('click', function() {
        $('.input_word_extern').val('');
        $('.chosen-select-categories').val('').trigger("chosen:updated");
        $('.chosen-select-words').val('').trigger("chosen:updated");

        $(".word_searched_hide").data('typesearchcategory', 0);
        $(".word_searched_hide").val('');

        construct_all_text_div(a_all_data_desordenada);
        construct_all_tweet_div(data.twitter);
        return false;
    });

    $(".div-content-text-filter").scroll(function(e) {
        if (check_is_filtered === 0) {
            //var scroll_top_prop=($(this).scrollTop()*$(".div-content-text-filter").height())/$(".div-content-text-filter")[0].scrollHeight;

            var scroll_top_prop = ($(this).scrollTop() * height_tag_scale) / ($(".div-content-text-filter")[0].scrollHeight);
            if (action_scroll == 1) {
                last_position_minimap = scroll_top_prop;
                $('.div_content_text_' + year_default + ' .magnifier_text').css({
                    'top': scroll_top_prop
                });
            }
        } else {
            var height_mark_word = $(".div-content-text-filter .word_select_extern").height();
            $(".div-content-text-filter .word_select_extern").each(function() {
                if ($(this).position().top < height_text_scale - height_mark_word / 2 && $(this).position().top > -height_mark_word / 2) {
                    var pos_index = $(".div-content-text-filter .word_select_extern").index(this);
                    // var tag_word_select=$(".tag_scale_text:last .word_select_extern").get(pos_index);
                    var tag_word_select = $(".div_content_text_" + year_default + " .tag_scale_text .word_select_extern").get(pos_index);
                    $(tag_word_select).find('.cdr_total').css({
                        'background': '#5a9ddc',
                        'color': '#ffffff'
                    });

                } else {
                    var pos_index = $(".div-content-text-filter .word_select_extern").index(this);
                    var tag_word_select = $(".div_content_text_" + year_default + " .tag_scale_text .word_select_extern").get(pos_index);
                    $(tag_word_select).find('.cdr_total').css({
                        'background': '#333333',
                        'color': '#ffffff'
                    });
                }
            });
        }
    });

    $(".group_text_map").on('click', '.tag_scale_text_active .cdr_total', function() {
        var pos_tag = $('.tag_scale_text_active .word_select_extern').index($(this).parent('span'));
        var tag_filter = $(".div-content-text-filter .word_select_extern").get(pos_tag);
        var pos_tag_filter = $(tag_filter).position().top;
        var scroll_actual = $('.div-content-text-filter').scrollTop();
        var move_scroll = pos_tag_filter - (height_text_scale / 2) + scroll_actual;
        $('.div-content-text-filter').scrollTop(move_scroll);
    });

    $('.btn_div_content_year').on('click', function() {
        $(".btn_div_content_year").removeClass('active');
        $(this).addClass('active');

        var val_year_select = parseInt($(this).parent('div').data('value'));
        if (check_is_filtered === 0) {
            year_default = val_year_select;
            construct_all_text_div(a_all_data_desordenada);
        } else {
            var word_choose = $(".word_searched_hide").val();
            var type_search_category = $('.word_searched_hide').data('typesearchcategory');
            year_default = val_year_select;
            if (type_search_category === 0) {
                data.add_word(word_choose.trim());
            } else {
                construct_all_text_div(a_all_data_category_ordenada, word_choose.trim(), true);
            }
        }
        return false;
    });

    window.addEventListener('load', function() {
        document.body.addEventListener('touchstart', function(e) {
            if ($(e.target).parents("input").length === 0) {
                $('input').blur();
                $(".chosen-select-words").trigger("click");
                $(".chosen-select-categories").trigger("click");
            }
        }, false);
    }, false);

    $(window).on('resize', function() {
        var sttime = setTimeout(function() {
            if (!is_mobile()) {
                $(".refresh_display_filter").trigger('click');
                $(".unit_word").removeClass('wordActive');
                $(".unit_category").removeClass('categoryActive');
                // construct_all_text_div(a_all_data_desordenada);
                clearTimeout(sttime);
            }
        }, 300);
    });

    // Listen for orientation changes
    $(window).on("orientationchange", function() {
        var sttime = setTimeout(function() {
            if (is_mobile()) {
                $(".refresh_display_filter").trigger('click');
                // construct_all_text_div(a_all_data_desordenada);
                clearTimeout(sttime);
            }
        }, 300);
    });

    function construct_all_tweet_div(data_csv_u, word, category) {
        var div_all_tweet = "<div>";
        var number_tweets = 0;
        var text_head_tweet = "";
        $(".text_dinamic_show").html("");
        data_csv_u.forEach(function(d) {
            if (word !== undefined && category === undefined) {
                var word_filter = word;
                var obj_recover = data.generate_parraf_by_text(d.Message, word_filter);
                if (obj_recover.quantity_word > 0) {
                    var div_tweet = "<div class='unit_tweet'>";
                    div_tweet += "<div class='t_account'>" + d.Account + "</div>";
                    div_tweet += "<div class='t_date'>" + d.date + "</div>";
                    div_tweet += "<div class='t_message'>" + obj_recover.u_parraf.autoLink() + "</div>";
                    // div_tweet += "<div>" + d.url + "</div>";
                    div_tweet += "</div>";
                    div_all_tweet += div_tweet;
                    number_tweets++;
                    text_head_tweet = "<span class='bold_number'>" + number_tweets + "</span> tweet(s) containing the word " + word.toUpperCase();
                }
            } else if (word !== undefined && category) {
                if (data.categories[word] !== undefined) {
                    var obj_recover = {};
                    obj_recover.u_parraf = d.Message;
                    obj_recover.quantity_word = 0;
                    if (data.categories[word].words !== undefined && data.categories[word].words.length > 0) {
                        data.categories[word].words.forEach(function(wd) {
                            var obj_rec = data.generate_parraf_by_text(obj_recover.u_parraf, wd);
                            obj_recover.u_parraf = obj_rec.u_parraf;
                            obj_recover.quantity_word += obj_rec.quantity_word;
                        });
                        if (obj_recover.quantity_word > 0) {
                            var div_tweet = "<div class='unit_tweet'>";
                            div_tweet += "<div class='t_account'>" + d.Account + "</div>";
                            div_tweet += "<div class='t_date'>" + d.date + "</div>";
                            div_tweet += "<div class='t_message'>" + obj_recover.u_parraf.autoLink() + "</div>";
                            // div_tweet += "<div>" + d.url + "</div>";
                            div_tweet += "</div>";
                            div_all_tweet += div_tweet;
                            number_tweets++;
                            text_head_tweet = "<span class='bold_number'>" + number_tweets + "</span> tweet(s) containing words related with " + word.toUpperCase();
                        }
                    }
                }

            } else {
                var div_tweet = "<div class='unit_tweet'>";
                div_tweet += "<div class='t_account'>" + d.Account + "</div>";
                div_tweet += "<div class='t_date'>" + d.date + "</div>";
                div_tweet += "<div class='t_message'>" + d.Message.autoLink() + "</div>";
                // div_tweet += "<div>" + d.url + "</div>";
                div_tweet += "</div>";
                div_all_tweet += div_tweet;
                number_tweets++;
                text_head_tweet = "Total <span class='bold_number'>" + number_tweets + "</span> tweets";
            }
        });
        div_all_tweet += "</div>";
        $(".text_dinamic_show").html(text_head_tweet);

        $(".content_tweet").html(div_all_tweet);
        $(".content_tweet").outerHeight(height_text_scale - $(".div_head_twitter").outerHeight());
    }

    function construct_all_text_div(obj_data_speech, text_filter, by_category) {
        var wrong_width = 0;
        if (!$(".old_g").is(':visible')) {
            wrong_width = 1;
            $(".old_g").css({
                display: 'block',
                opacity: 0
            });
        }

        last_position_minimap = 0;
        width_text_filter = $('.div-content-text-filter').width();

        var div_content_search = ($(window).height() > 460) ? $('.div-content-search').height() + $(".list-option-radio").outerHeight() + 20 : 0;
        // height_text_scale = $(window).height() - div_content_search - $(".div_head_text_filter").outerHeight(true);
        height_text_scale = $(window).height() - div_content_search;

        $('.all-content-text').height(height_text_scale);

        $('.div-content-text-filter').css({
            'height': height_text_scale - $('.div_head_text_filter').outerHeight(true),
            'overflow-y': 'scroll',
            'overflow-x': 'hidden',
            '-webkit-overflow-scrolling': 'touch',
        });



        // blucle all speechs
        var scale_calc_x;
        var count_parraf_width_words = 0;
        var count_parraf_width_words2 = 0;
        var string_parraf_filter = '';
        var string_parraf_filter2 = '';
        var count_any_words = 0;
        var count_any_words2 = 0;

        $.each(obj_data_speech, function(year, a_data_speech_by_year) {
            $(".div_content_text_" + year).html('');

            var res_exec = {};

            var width_content_scale = $(".div_content_text_" + year).width();
            scale_calc_x = width_content_scale / width_text_filter;
            var tag_active_or_desactive = (parseInt(year) === year_default) ? "tag_scale_text_active" : "";
            var string_parraf = '<div class="tag_scale_text ' + tag_active_or_desactive + '">';

            a_data_speech_by_year.forEach(function(u_parraf) {
                if (u_parraf instanceof Object) {
                    if (u_parraf.word === text_filter) {
                        u_parraf.a_parraf_copy.forEach(function(o_parr) {
                            string_parraf += '<p><span>' + o_parr.u_parraf + '</span></p>';
                            if (parseInt(year) === year_default && parseInt(o_parr.quantity_word) > 0) {
                                count_any_words += parseInt(o_parr.quantity_word);

                                count_parraf_width_words++;
                                string_parraf_filter += '<p><span>' + o_parr.u_parraf + '</span></p>';
                            } else if (parseInt(year) === year_default - 1 && parseInt(o_parr.quantity_word) > 0) {
                                count_any_words2 += parseInt(o_parr.quantity_word);

                                count_parraf_width_words2++;
                                string_parraf_filter2 += '<p><span>' + o_parr.u_parraf + '</span></p>';
                            }
                        });
                    }
                } else {
                    string_parraf += '<p><span>' + u_parraf + '</span></p>';
                    if (parseInt(year) === year_default) {
                        count_any_words += parseInt(data.count_words_by_text(u_parraf));

                        string_parraf_filter += '<p><span>' + u_parraf + '</span></p>';
                    } else if (parseInt(year) === year_default - 1) {
                        count_any_words2 += parseInt(data.count_words_by_text(u_parraf));
                        string_parraf_filter2 += '<p><span>' + u_parraf + '</span></p>';
                    }
                }
            });

            $(".div_count_word_" + year).html("<span>Total words:</span> <span>" + format_number(count_any_words)) + "</span>";
            $(".div_content_year_" + year).data('value', year);
            // $(".div_content_year_"+year).html(year);

            string_parraf += '</div><div class="magnifier_text"></div>';
            $(".div_content_text_" + year).outerHeight(height_text_scale - $(".div_head_text_filter").outerHeight(true));
            $(".div_content_text_" + year).html(string_parraf);

            $(".div-content-text-filter").html(string_parraf_filter);
            $(".current_speech").html(string_parraf_filter);

            $(".div-content-text-filter2").html(string_parraf_filter2);
            $(".before_speech").html(string_parraf_filter2);



            if (text_filter === undefined) {
                if (parseInt(year) === year_default) {
                    var div = '<div class="content-total-words">Total Words: <span class="total-words">' + format_number(count_any_words) + '</span></div>';
                    div += '<div class="content-total-time">Speech Duration: <span>' + current_duration_speech + '</span></div>';

                    $(".minister-2016").html(div);
                } else if (parseInt(year) === (year_default - 1)) {
                    var div = '<div class="content-total-words">Total Words: <span class="total-words">' + format_number(count_any_words2) + '</span></div>';
                    div += '<div class="content-total-time">Speech Duration: <span>' + before_duration_speech + '</span></div>';


                    $(".minister-2015").html(div);
                }
            }
        });

        if (text_filter === undefined) {
            $(".div_head_text_filter").html('');
            $(".div_head_text_filter2").html('');
        } else {
            if (by_category === undefined) {
                $(".div_head_text_filter").html('<div><span>' + count_parraf_width_words + '</span> paragraph(s) containing the word <span>' + text_filter + '</span></div><hr>');
                $(".div_head_text_filter2").html('<div><span>' + count_parraf_width_words2 + '</span> paragraph(s) containing the word <span>' + text_filter + '</span></div><hr>');

                $(".minister-2015").html('<span class="number-words">' + format_number(count_any_words2) + '</span> Total word count for ' + '<span class="text-filter">' + text_filter + '</span>');
                $(".minister-2016").html('<span class="number-words">' + format_number(count_any_words) + '</span> Total word count for ' + '<span class="text-filter">' + text_filter + '</span>');
            } else {
                $(".div_head_text_filter").html('<div><span>' + count_parraf_width_words + '</span> paragraph(s) containing words related with <span>' + text_filter + '</span></div><hr>');
                $(".div_head_text_filter2").html('<div><span>' + count_parraf_width_words2 + '</span> paragraph(s) containing words related with <span>' + text_filter + '</span></div><hr>');

                $(".minister-2015").html('<span class="number-words">' + format_number(count_any_words2) + '</span> words related with <span class="text-filter">' + text_filter + '</span> <span class="paragraph-text" >' + count_parraf_width_words2 + ' paragraph(s)</span>');
                $(".minister-2016").html('<span class="number-words">' + format_number(count_any_words) + '</span> words related with <span class="text-filter">' + text_filter + '</span> <span class="paragraph-text">' + count_parraf_width_words + ' paragraph(s)</span>');
            }
        }



        $(".tag_scale_text").css({
            width: width_text_filter
        });

        // if (just_one_cal === 0) {
        //     $(".tag_scale_text").each(function() {
        //         if (height_text_filter < $(this).height()) {
        $(".tag_scale_text").css({
            "transform": "scaleX(" + 1 + ") scaleY(" + 1 + ")",
            "-ms-transform": "scaleX(" + 1 + ") scaleY(" + 1 + ")",
            "-webkit-transform": "scaleX(" + 1 + ") scaleY(" + 1 + ")"
        });
        height_text_filter = $('.tag_scale_text').height();
        //         }
        //     });
        // }

        var scale_calc_y = (height_text_scale - $(".div_head_text_filter").outerHeight(true)) / height_text_filter;

        $(".tag_scale_text").css({
            "transform": "scaleX(" + scale_calc_x + ") scaleY(" + scale_calc_y + ")",
            "-ms-transform": "scaleX(" + scale_calc_x + ") scaleY(" + scale_calc_y + ")",
            "-webkit-transform": "scaleX(" + scale_calc_x + ") scaleY(" + scale_calc_y + ")"
        });

        //if(text_filter===undefined)height_text_filter_personal=$(".div-content-text-filter")[0].scrollHeight;




        height_tag_scale = $('.div_content_text_' + year_default + ' .tag_scale_text').height() * scale_calc_y;
        $('.div_content_text_' + year_default + ' .magnifier_text').css({
            'height': $(".div-content-text-filter").height(),
            'transform': "scaleY(" + scale_calc_y + ")",
            '-ms-transform': "scaleY(" + scale_calc_y + ")",
            '-webkit-transform': "scaleY(" + scale_calc_y + ")"
        });

        if (text_filter !== undefined) {
            check_is_filtered = 1;
            $('.div_content_text_' + year_default + ' .magnifier_text').css({
                height: 0
            });
        } else {
            check_is_filtered = 0;
        }

        $(".div_content_year_" + year_default + " .btn_div_content_year").addClass('active');
        $(".div-content-text-filter").scrollTop(1);
        $(".div-content-text-filter").scrollTop(0);

        var myElement = $('.div_content_text_' + year_default + ' .magnifier_text')[0];
        var mc = new Hammer(myElement);

        // // let the pan gesture support all directions.
        // // this will block the vertical scrolling on a touch-device while on the element
        mc.get('pan').set({
            direction: Hammer.DIRECTION_ALL
        });

        // listen to events...

        var height_real_minimap = $('.div_content_text_' + year_default + ' .magnifier_text').height() * scale_calc_y;
        mc.on("panup pandown", function(ev) {
            $(".tag_scale_text").addClass("user_select");
            $(".div-content-text-filter").addClass("user_select");



            action_scroll = 0;
            direction_mov = ev.offsetDirection;
            var move_intern = (direction_mov != 8) ? -ev.distance : ev.distance;
            var new_top = ((last_position_minimap + move_intern) / scale_calc_y);
            if (last_position_minimap + move_intern >= 0 && last_position_minimap + move_intern + height_real_minimap <= height_tag_scale) {
                $('.div_content_text_' + year_default + ' .magnifier_text').css({
                    top: last_position_minimap + move_intern
                });
                $(".div-content-text-filter").scrollTop(new_top);
            } else {
                if (last_position_minimap + move_intern < 0) {
                    $('.div_content_text_' + year_default + ' .magnifier_text').css({
                        top: 0
                    });
                }
                if (last_position_minimap + move_intern + height_real_minimap > height_tag_scale) {
                    $('.div_content_text_' + year_default + ' .magnifier_text').css({
                        top: height_tag_scale - height_real_minimap
                    });
                }
                $(".div-content-text-filter").scrollTop(new_top);
            }
        });
        mc.on("panend", function(ev) {
            $(".tag_scale_text").removeClass("user_select");
            $(".div-content-text-filter").removeClass("user_select");
            action_scroll = 1;
            var move_intern = (direction_mov != 8) ? -ev.distance : ev.distance;
            if (last_position_minimap + move_intern >= 0 && last_position_minimap + move_intern + height_real_minimap <= height_tag_scale) {
                last_position_minimap += move_intern;
            } else {
                if (last_position_minimap + move_intern < 0) last_position_minimap = 0;
                if (last_position_minimap + move_intern + height_real_minimap > height_tag_scale) last_position_minimap = height_tag_scale - height_real_minimap;
            }
        });

        if (wrong_width === 1) {
            $(".old_g").css({
                display: 'none',
                opacity: 1
            });
        }

        var n_height_scale=height_text_scale - $('.content-head-minister').outerHeight(true);
        if(n_height_scale<=400){
        	n_height_scale=400;
        	$(".all-content-text").css('height','auto');
        }
        $('.div-content-text-filter2,.current_speech,.before_speech').css({
            'height': n_height_scale,
            'overflow-y': 'scroll',
            'overflow-x': 'hidden',
            '-webkit-overflow-scrolling': 'touch',
        });

        $(".content_tweet").height(height_text_scale - $(".div_head_twitter").outerHeight());
    }

    function is_mobile() {
        return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
    }

    function hide_keyboorad(var_this) {
        var time_close_c = setTimeout(function() {
            if ($(var_this).parent().find('input').is(':focus')) {
                $(var_this).parent().find('input').blur();
                clearTimeout(time_close_c);
            } else {
                clearTimeout(time_close_c);
                hide_keyboorad(var_this);
            }
        }, 100);
    }

    function format_number(number) {
        Math.floor(number);
        var val_return = 0;
        var format_number_int = d3.format(",d"),
            format_number_decimal = d3.format(",.2f");
        var prom = number % 1;
        if (prom === 0) {
            val_return = format_number_int(number);
        } else {
            if (format_number_decimal(prom) % 1 === 0) {
                val_return = format_number_int(Math.floor(number) + 1);
            } else {
                val_return = format_number_decimal(number);
            }
        }
        return val_return;
    }

    function share_face_book(image, name, description) {
        FB.ui({
            method: 'feed',
            link: window.location.href,
            caption: 'www.straitstimes.com',
            picture: image,
            name: name,
            description: description
        });
    }

    function share_twitter(text, via, url) {
        window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
    }

    function draw_radio_button(svg_from_rad, animDefs) {
        var path = svg_from_rad.append('path').attr('d', pathDefs.fill[0]);
        var length_path = path.node().getTotalLength();
        path.attr("stroke-dasharray", length_path + " " + length_path).attr("stroke-dashoffset", length_path)
            .transition()
            .duration(500)
            .ease(animDefs.fill.easing, animDefs.fill.speed)
            .attr("stroke-dashoffset", 0);
    }
});
