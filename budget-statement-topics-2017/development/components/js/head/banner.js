"use strict";
var React = require('react');

var Banner = React.createClass({
    getDefaultProps: function() {
        return {
            href: "http://www.straitstimes.com/budget-2017",
        }
    },
    handlemouseover: function() {
        // console.log("asdasdas");
    },
    render: function() {
        return ( <
                div className = "div-container-banner" >
                <
                Block_svg_image class = "desktop_bak"
                src = "images/budget-2017-logo2.svg"
                href = {
                    this.props.href
                }
                /> { /* < Block_svg_image class = "mobile_bak"
                src = "images/banner/banner_mobile.jpg"
                href = {
                    this.props.href
                }
                />*/
            } <
            /div>
    );
}
});


var Block_svg_image = React.createClass({
    render: function() {
        return ( <
            div className = {
                this.props.class
            } >
            <
            a href = {
                this.props.href
            }
            target = "_blank" >
            <
            figure itemProp = "associatedMedia image"
            itemScope itemType = "http://schema.org/ImageObject"
            data-component= "image" >
            <
            img src = {
                this.props.src
            }
            className = "center-block st-image-responsive" />
            <
            /figure> < /
            a > <
            /div>
        );
    }
});

module.exports = Banner;
