"use strict";
var React = require('react');

var Headline = React.createClass({
	getDefaultProps:function(){
		return {
			title:'Analysing the 2017 Budget Statement',
			text:"Finance Minister Heng Swee Keat delivered his Budget statement on Feb 20, 2017. Read the full speech below and compare it with his speech from last year. "
		};
	},
    render: function() {
    	var pStyle={
    		color: '#666',
    		fontSize: '11px'
    	};
        return (
        	<div className="content-headline">
        		<div className="text-center st-headline">
        			<h1>{this.props.title}</h1>
        		</div>
        		<div className="text-center st-strap">
        			<p>
        			{this.props.text}
        			</p>
        			<p style={pStyle}>
        				PUBLISHED: <time itemProp="datePublished" dateTime="2017-02-21T20:00:00-0000" data-timestamp="1487663093">FEB 21, 2017</time>
        			</p>
        		</div>
        	</div>
        );
    }
});

module.exports = Headline;
