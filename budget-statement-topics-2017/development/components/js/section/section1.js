"use strict";
var React = require('react');
var d3=require('d3');
var $=require('jquery');

var Section = React.createClass({
	getDefaultProps:function(){
		return {
			tabs:[
				{
					desktop: "SEARCH BY CATEGORY",
					mobile: "Category",
					active: 1,
					id_mobile: "tab_select_category",
					id_desktop: "div_category_select"
				},
				{
					desktop: "SEARCH BY MOST USED WORDS",
					mobile: "Most used",
					active: 0,
					id_mobile: "tab_select_words",
					id_desktop: "div_words_select"
				},
				{
					desktop: "FREE SEARCH",
					mobile: "Search",
					active: 0,
					id_mobile: "tab_add_words",
					id_desktop: "tab_add_word"
				}
			]
		}
	},
	handleChange:function(event){
		if(this.refs.chk_compare.checked){
			this.refs.list_options.setState({display: "none"});
			this.refs.text_speech.setState({display: "none"});
			this.refs.text_speech.setState({width: "100%"});

			this.refs.search.refs.group_words1.refs.group_words.setState({
				display_before:"block",
				display_now:"none"
			});

			this.refs.search.refs.group_word_mobile1.setState({
				display_before:"block",
				display_now:"none"
			});

			// console.log(this.refs.text_speech.refs.st_both_ministers);
			if(this.refs.list_options.refs.rad_reactions.checked){
				this.refs.text_speech.setState({show_text: "block"});
			}else{
				this.refs.text_speech.setState({show_text: ""});
			}

			setTimeout(function(){
				var ih=$(".all-content-text").height()-$(".content-head-minister").height();
				if(ih<=400){
					ih=400;
					$(".all-content-text").height('auto');
				}
				$(".before_speech,.current_speech").outerHeight(ih);
			},100);
		}else{
			this.refs.list_options.setState({display: "block"});
			this.refs.text_speech.setState({display: "block"});
			this.refs.text_speech.setState({width: ""});

			this.refs.search.refs.group_words1.refs.group_words.setState({
				display_before:"none",
				display_now:"block"
			});

			this.refs.search.refs.group_word_mobile1.setState({
				display_before:"none",
				display_now:"block"
			});

			if(this.refs.list_options.refs.rad_reactions.checked){
				this.refs.text_speech.setState({show_text: ""});
			}


			setTimeout(function(){
				var div_content_search = ($(window).height() > 460) ? $('.div-content-search').height() + $(".list-option-radio").outerHeight() + 20 : 0;
				var height_text_scale = $(window).height() - div_content_search;
				$('.all-content-text').height(height_text_scale);
			},100);
		}
	},
    render: function() {
        return (
        	<section className="chapter-division"  >
        		<div className="st_materialize">
        			<input type="checkbox" className="filled-in" id="filled-in-box" ref="chk_compare" onChange={this.handleChange} />
        			<label htmlFor="filled-in-box">Compare 2016</label>
        		</div>
        		<Search ref="search" text_tabs={this.props.tabs} />
        		<List_options ref="list_options" />
        		<Text_speech ref="text_speech" />
        	</section>
        );
    }
});

var Search=React.createClass({
	render:function(){
		var a_li_desktop=[];
		var a_li_mobile=[];
		var a_pane_desktop=[];
		var a_pane_mobile=[];
		this.props.text_tabs.forEach(function(d,i){
			var active =(d.active===1)?"active":"";
			var li_desktop = <li key={i} className={active} >
					<a href={"#"+d.id_desktop} role="tab" data-toggle="tab">
						{d.desktop}
					</a>
				</li>;
			var li_mobile = <li key={i} className={active} >
					<a href={"#"+d.id_mobile} role="tab" data-toggle="tab">
						{d.mobile}
					</a>
				</li>;
			var pane_desktop = <Panedesktop ref={"group_words"+i} key={i} keys={i} class={"tab-pane "+active} id={d.id_desktop} />;
			var pane_mobile = <Panemobile ref={"group_word_mobile"+i} key={i} keys={i} class={"tab-pane "+active} id={d.id_mobile} />;
			a_li_desktop.push(li_desktop);
			a_li_mobile.push(li_mobile);
			a_pane_desktop.push(pane_desktop);
			a_pane_mobile.push(pane_mobile);
		});
		return (
			<div className="div-content-search">
				<div className="div-tab-search div-tab-search-mobile">
					<ul className="nav nav-tabs" id="tab-mob" role="tablist">
						{a_li_mobile}
					</ul>
					<div className="tab-content" >
						{a_pane_mobile}
					</div>
				</div>
				<div className="div-tab-search div-tab-search-desktop">
					<ul className="nav nav-tabs" id="tab-desk" role="tablist">
						{a_li_desktop}
					</ul>
					<div className="tab-content">
						{a_pane_desktop}
					</div>
				</div>
			</div>
			);
	}
});


var Panemobile = React.createClass({
	getInitialState:function(){
		return {
			display_now:'block',
			display_before:'none'
		}
	},
	render:function(){
		var render_mobile="";
		if(this.props.keys===0){
			render_mobile = <select data-placeholder="By category" tabIndex="1" className="chosen-select-categories" style={{width:"100%"}}>
			</select>
		}else if(this.props.keys===1){
			render_mobile = <div>
								<div style={{display:this.state.display_now}}><select data-placeholder="Most used words" tabIndex="1" id="chosen-select-words" className="chosen-select-words" style={{width:"100%"}}></select></div>
								<div style={{display:this.state.display_before}}><select data-placeholder="Most used words" tabIndex="1" id="chosen-select-words2" className="chosen-select-words" style={{width:"100%"}}></select></div>
							</div>;
		}else if(this.props.keys===2){
			render_mobile = <div className="input-group">
				<input className="form-control input_word_extern" type="text" placeholder="Add word or phrase" style={{width:"100%"}}  />
				<span className="input-group-addon add_word_filter">
					<i className="glyphicon glyphicon-plus-sign"></i>
				</span>
				<span className="input-group-addon refresh_display_filter">
					<i className="glyphicon glyphicon-refresh">
					</i>
				</span>
			</div>;
		}
		return (
			<div className={this.props.class} id={this.props.id} >
				{render_mobile}
			</div>
			);
	}
});
var Panedesktop = React.createClass({
	render:function(){
		var render_desktop="";
		if(this.props.keys===0){
			render_desktop = <Group_category />
		}else if(this.props.keys===1){
			render_desktop = <Group_word ref="group_words" />;
		}else if(this.props.keys===2){
			render_desktop = <div className="input-group">
				<input className="form-control input_word_extern" type="text" placeholder="Add word or phrase" style={{width:"100%"}}  />
				<span className="input-group-addon add_word_filter">
					<i className="glyphicon glyphicon-plus-sign"></i>
				</span>
				<span className="input-group-addon refresh_display_filter">
					<i className="glyphicon glyphicon-refresh">
					</i>
				</span>
			</div>;
		}
		return (
			<div className={this.props.class} id={this.props.id} >
				{render_desktop}
			</div>
			);
	}
});

var Group_word = React.createClass({
	// getInitialState:function(){
	// 	return {
	// 		words:[]
	// 	}
	// },
	// componentWillMount:function(){
	// 	var path_csv_categories = 'csv/categories.csv';
	// 	var copy=this;
	// 	d3.csv(path_csv_categories, function(error, data_csv) {
	// 		var w=data_csv.filter(function(d){
	// 			return d.words!=="";
	// 		}).map(function(d){
	// 			return d.words;
	// 		});
	// 		console.log(w);
	// 		copy.setState(
	// 			{
	// 				words:w
	// 			}
	// 			);
	// 	});
	// },
	getInitialState:function(){
		return {
			display_now:'block',
			display_before:'none'
		}
	},
	render:function(){
		return (
			<div>
				<div id="div_words" style={{display:this.state.display_now}}>
				</div>
				<div id="current-div-words" style={{ display: this.state.display_before}}>
					<div id="div-current-year">
					</div>
					<div id="div-before-year">
					</div>
				</div>
			</div>
			);
	}
});
var Group_category = React.createClass({
	getDefaultProps:function(){
		return {
			categories:[
				{
					image: "images/categories/government.svg",
					name: "government"
				},
				{
					image: "images/categories/education.svg",
					name: "education"
				},
				{
					image: "images/categories/jobs.svg",
					name: "jobs"
				},
				{
					image: "images/categories/housing.svg",
					name: "housing"
				},
				{
					image: "images/categories/social.svg",
					name: "social"
				},
				{
					image: "images/categories/economy.svg",
					name: "economy"
				},
				{
					image: "images/categories/healthcare.svg",
					name: "healthcare"
				},
				{
					image: "images/categories/transport.svg",
					name: "transport"
				},
				{
					image: "images/categories/retirement.svg",
					name: "retirement"
				}

			]
		}
	},
	render:function(){
		return (
			<div id="div_category">
			{
				this.props.categories.map(function(d,i){
					return (
						<div key={i} className="unit_category" data-cat={d.name}>
							<div>
								<img src={d.image} className="st-image-responsive center-block" />
							</div>
							<div>
								{d.name}
							</div>
						</div>
						);
				})
			}
			</div>
			);
	}
});

var List_options = React.createClass({
	getInitialState:function(){
		return {
			display:"inherit"
		}
	},
	render:function(){
		return (
			<div className="list-option-radio " ref="div_list_option" style={{display:this.state.display}} >
				<div className="option-radio">
                	<div style={{"paddingLeft":"5px"}}>
                    	<input type="radio" name="rad-speech" className="rad-speech" id="rad-speechs" value="s" />
                    	<label htmlFor="rad-speechs">Statement</label>
                	</div>
            	</div>
            	<div className="option-radio">
                	<div style={{"paddingLeft":"5px"}}>
                		<input type="radio" name="rad-speech" className="rad-speech" id="rad-twitter" value="t" ref="rad_reactions" />
                		<label htmlFor="rad-twitter">Tweets</label>
                	</div>
            	</div>
			</div>
			);
	}
});


var Text_speech = React.createClass({
	getInitialState:function(){
		return {
			display:"inherit",
			show_text:"",
			width:""
		}
	},
	render:function(){
		var text="Reactions on Heng Swee Keat's speech as of Feb 21 (6am) - #SGBUDGET2017";
		return (
			<div className="all-content-text">
				<div className="st-both_ministers" style={{ display:(this.state.display==='none')?"table":"none" }} >
					<div className="content-head-minister">
						<Heade_detail_minister year="2017" charge="Heng Swee Keat" image="images/temp/minister-2016.png" class="minister-2016" speech_no_active="speech_active" />
						<Heade_detail_minister year="2016" charge="Tharman Shanmugaratnam" image="images/temp/minister-2015.png" class="minister-2015" speech_no_active="speech_no_active" />
					</div>
					<div>
						<Detail_minister ref="body_minister" speech="current_speech" speech_no_active="speech_active" />
						<Detail_minister ref="body_minister" speech="before_speech" speech_no_active="speech_no_active" />
					</div>
				</div>
				<div className="old_g" style={{display:this.state.display}}>
					<div className="col-st-8 ">
						<div className="group_text_map">
							<div className="col-st-8">
								<div className="both-profile">
									<div className="display-both1">
										<div className="div_head_text_filter">
				                        </div>
				                        <div className="div-content-text-filter"></div>
									</div>
			                        {/*<div className="display-both2" style={{ display:(this.state.display==='none')?"block":"none" }}>
					                	<div className="div_head_text_filter2">
						        		</div>
						        		<div className="div-content-text-filter2">
						        		</div>
					        		</div>*/}
				        		</div>
		                    </div>
		                    <div className="col-st-4">
		                        <div className="row">
		                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 none">
		                                <div className="div_content_year_2016">
		                                    <a className="hidden-xs btn btn-default btn_div_content_year" href="#" role="button">2016</a>
		                                </div>
		                            </div>
		                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                                <div className="div_count_word_2016"></div>
		                            </div>
		                        </div>
		                        <div className="div_content_text_2016"></div>
		                    </div>
		                </div>
		            </div>
		            <div className="col-st-4">
		                <div className="div_head_twitter content_a">
		                    <div className="clearfix">
		                        <div className="pull-left" style={{width:"36px",marginRight:"-36px"}}>
		                            <img className="img-responsive" src="images/temp/twitter-buttonsmall.svg" alt="" />
		                        </div>
		                        <div className="pull-left" style={{marginLeft:"36px", marginRight:"5px"}}>
		                            {text}
		                        </div>
		                    </div>
		                    <div className="text_dinamic_show">
		                    </div>
		                    <hr />
		                </div>
		                <div className="content_tweet content_a">
		                </div>
		            </div>
				</div>
	        </div>
			);
	}
});

var Heade_detail_minister= React.createClass({
	render:function(){
		return (
			<div className={"head-data-minister "+this.props.speech_no_active}>

				<div className="content-data-minister">
					<div className="year-speach">
						{this.props.year}
					</div>

					<div className={"data-speach " +this.props.class}>
						<span>60 </span>
						<span>paragraph(s) containing words related with</span>
						<span> JOBS</span>
					</div>
				</div>
				<div className="c-read-paragraphs" onClick={this.props.onClick}>
					{"VIEW "+this.props.year}
				</div>
			</div>
			);
	}
});

var Detail_minister = React.createClass({
	render:function(){
		return (
			<div className={"content-detail-minister " + this.props.speech_no_active}>
				<div className={this.props.speech}>
				</div>
			</div>
			);
	}
});

module.exports = Section;
