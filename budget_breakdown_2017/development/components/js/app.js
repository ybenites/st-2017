var $ = require('jquery');


// require("./magicsuggest-1.3.1");
require("jquery.scrollto");

// require('typeahead.js');


require('typeahead.js/dist/typeahead.jquery.js');
var Bloodhound = require('typeahead.js/dist/bloodhound.js');

var value_selected_expenditure;
var value_selected_revenue;


require('./waypoints');
require('select2');
var d3 = require('d3');




var d_small = 400;
var d_medium = 768;
var d_normal = 992;
var d_large = 1280;

var input_flag = null;
var input_flag2 = null;

var pathDefs = {
    fill: ['M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809']
};
var animDefs = {
    fill: {
        speed: .8,
        easing: 'ease-in-out'
    }
};

var url_expenditure_csv = "csv/expenditure.csv";
var url_sub_expenditure_csv = "csv/sub_expenditure.csv";
var url_revenue_csv = "csv/revenue.csv";

var array_year = [2014, 2015, 2016, 2017];
var year_default = 2017;
var array_branches = ['Expenditure', 'Sector'];
var form_show = 'millon';
var form_show_r = 'millon';
var transitions = 0;

var treemap = d3.layout.treemap().children(function(d, depth) {
        return depth ? null : d._children;
    }).sort(function(a, b) {
        return d3.ascending(b.value, a.value);
    })
    // .padding([1,0,0, 0])
    .mode("dice")
    .round(false);

var div_expenditure = d3.selectAll(".st-graphic-expenditure");
var div_revenue = d3.selectAll(".st-graphic-revenue");

var width_complete = div_expenditure.node().clientWidth;

var width_window = document.getElementsByTagName('body')[0].clientWidth;
width_window = width_window >= 992 ? width_window : width_window - 20;

width_window = width_window >= width_complete ? width_complete : width_window;

var margin = {
        top: 1,
        right: 1,
        bottom: 15,
        left: 1
    },
    width_g = 150,
    width_u = 30,
    height_g = 440 - margin.top - margin.bottom;

var f_rx = d3.scale.linear().domain([0, width_g]).range([0, width_g]);
var f_ry = d3.scale.linear().domain([0, height_g]).range([0, height_g]);
var max_value_every_year = 0;
var max_value_every_year_r = 0;

var position_axis = 40;
var tranlate_svg_expendeture = (width_window / 2) - width_g;
tranlate_svg_expendeture = tranlate_svg_expendeture <= position_axis ? position_axis : tranlate_svg_expendeture;
var tranlate_svg_revenue = (width_window / 2) - width_g;
tranlate_svg_revenue = tranlate_svg_revenue <= position_axis ? position_axis : tranlate_svg_revenue;


var svg = div_expenditure.append("svg")
    // .attr("width", width_complete - margin.left - margin.right)
    .attr("width", "100%")
    .attr("height", height_g + margin.top + margin.bottom)
    .style("margin-left", margin.left + "px")
    .style("margin-right", margin.right + "px")
    .style("display", "block")
    .style("background", '#fff').style('overflow', 'hidden');

var all_color_bar_expenditure = ["#E9ECEE", "#D4DADD", "#facbb5", "#f18b68", "#eb5c3b"];

var defs = svg.append('defs');
var pattern_before = defs.append('pattern').attr('id', 'pattern_before').attr("width", 8).attr('height', 8).attr('patternUnits', 'userSpaceOnUse');
pattern_before.append('rect').attr("x", 0).attr('y', 0).attr('fill', all_color_bar_expenditure[0]).attr("width", 8).attr('height', 8);
pattern_before.append('line').attr("fill", 'none').attr("stroke", "#474747").attr("stroke-width", 0.3)
    .attr('x1', 0).attr('y1', 0).attr('x2', 8).attr('y2', 8);

var pattern_default = defs.append('pattern').attr('id', 'pattern_default').attr("width", 8).attr('height', 8).attr('patternUnits', 'userSpaceOnUse');
pattern_default.append('rect').attr("x", 0).attr('y', 0).attr('fill', all_color_bar_expenditure[1]).attr("width", 8).attr('height', 8);
pattern_default.append('line').attr("fill", 'none').attr("stroke", "#474747").attr("stroke-width", 0.3)
    .attr('x1', 0).attr('y1', 0).attr('x2', 8).attr('y2', 8);

var pattern_first_child = defs.append('pattern').attr('id', 'pattern_first_child').attr("width", 8).attr('height', 8).attr('patternUnits', 'userSpaceOnUse');
pattern_first_child.append('rect').attr("x", 0).attr('y', 0).attr('fill', all_color_bar_expenditure[3]).attr("width", 8).attr('height', 8);
pattern_first_child.append('line').attr("fill", 'none').attr("stroke", "#474747").attr("stroke-width", 0.3)
    .attr('x1', 0).attr('y1', 0).attr('x2', 8).attr('y2', 8);

var g_second_child = defs.append('pattern').attr('id', 'g_second_child').attr("width", 8).attr('height', 8).attr('patternUnits', 'userSpaceOnUse');
g_second_child.append('rect').attr("x", 0).attr('y', 0).attr('fill', all_color_bar_expenditure[4]).attr("width", 8).attr('height', 8);
g_second_child.append('line').attr("fill", 'none').attr("stroke", "#474747").attr("stroke-width", 0.3)
    .attr('x1', 0).attr('y1', 0).attr('x2', 8).attr('y2', 8);




var svg_expendeture = svg.append('g').attr('class', 'svg_expendeture').attr('transform', 'translate(' + tranlate_svg_expendeture + ',0)');
var g_first_child = svg.append('g').attr('class', 'g_first_child');
g_first_child.append('g').attr('class', 'grandparent');

var g_second_child = svg.append('g').attr('class', 'g_second_child');
g_second_child.append('g').attr('class', 'grandparent');



var svg_r = div_revenue.append("svg")
    // .attr("width", width_complete - margin.left - margin.right)
    .attr("width", "100%")
    .attr("height", height_g + margin.top + margin.bottom)
    .style("margin-left", margin.left + "px")
    .style("margin-right", margin.right + "px")
    .style("display", "block")
    .style("background", '#fff').style('overflow', 'hidden');

var defs = svg_r.append('defs');
var pattern_before_r = defs.append('pattern').attr('id', 'pattern_before_r').attr("width", 8).attr('height', 8).attr('patternUnits', 'userSpaceOnUse');
pattern_before_r.append('rect').attr("x", 0).attr('y', 0).attr('fill', "#A1C7EB").attr("width", 8).attr('height', 8);
pattern_before_r.append('line').attr("fill", 'none').attr("stroke", "#085475").attr("stroke-width", 0.3)
    .attr('x1', 0).attr('y1', 0).attr('x2', 8).attr('y2', 8);

var pattern_default_r = defs.append('pattern').attr('id', 'pattern_default_r').attr("width", 8).attr('height', 8).attr('patternUnits', 'userSpaceOnUse');
pattern_default_r.append('rect').attr("x", 0).attr('y', 0).attr('fill', "#5A9DDC").attr("width", 8).attr('height', 8);
pattern_default_r.append('line').attr("fill", 'none').attr("stroke", "#085475").attr("stroke-width", 0.3)
    .attr('x1', 0).attr('y1', 0).attr('x2', 8).attr('y2', 8);


var svg_revenue = svg_r.append('g').attr('class', 'svg_revenue').attr('transform', 'translate(' + tranlate_svg_revenue + ',0)');

d3.selectAll(".list-option-radio div").each(function() {
    d3.select(this).append('svg').attr('viewBox', '-7 -7 110 110');
});

$(".content-stepper-expenditure .cancel").on('click', function() {
    $('.content-stepper-expenditure').animate({
        opacity: 0
    }, 500, function() {
        $(this).hide();
    });
});
var n_step = 1;
$(".content-stepper-expenditure .accept").on('click', function() {
    $(".content-stepper-expenditure .step").animate({
        opacity: 0
    }, 500, function() {
        $(this).hide();
    });

    n_step++;
    if ($(".content-stepper-expenditure .step" + n_step).length > 0) {

        $(".content-stepper-expenditure .step" + n_step).stop();
        $(".content-stepper-expenditure .step" + n_step).animate({
            opacity: 1
        }, 500, function() {
            $(this).fadeIn(300);
        });

    } else {
        $(".content-stepper-expenditure").animate({
            opacity: 0
        }, 500, function() {
            $(this).hide();
        });
    }
});

document.getElementById("rad-total-expenditure").checked = true;
var svg_from_rad = d3.select(d3.select("#rad-total-expenditure").node().parentNode).select('svg');
draw_radio_button(svg_from_rad, animDefs);

d3.selectAll(".rad-expenditure").on("change", function() {
    d3.selectAll('.content_control_expenditure .list-option-radio path').remove();

    var svg_from_rad = d3.select(this.parentNode).select('svg');
    draw_radio_button(svg_from_rad, animDefs);

    if (document.getElementById("rad-total-expenditure").checked) {
        svg.selectAll(".small_scale").transition().duration(500).attr("opacity", 1);
        svg_expendeture.selectAll(".depth").each(function() {
            form_show = "millon";
            var copy_this = this;
            var obj_datum = d3.select(this).datum();
            f_ry.domain([0, max_value_every_year]);
            initialize(obj_datum);
            f_ry.domain([0, height_g]);
            layout(obj_datum);

            d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", null);
            d3.select(this).datum(obj_datum);
            d3.select(this).selectAll('rect').transition().duration(750).call(rect).each('end', function() {
                d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", "crispEdges");
            });
        });

        var f_scale_Y = d3.scale.linear().domain([0, max_value_every_year]).range([height_g, 0]);
        var xAxis = d3.svg.axis().scale(f_scale_Y).orient("left").tickFormat(function(d) {
            return d / 1000000000 + "b"
        });
        svg_expendeture.select(".expenditure_axis").call(xAxis);
    } else {
        svg.selectAll(".small_scale").transition().duration(500).attr("opacity", 0);
        svg_expendeture.selectAll(".depth").each(function() {
            form_show = "percentaje";
            var copy_this = this;
            var obj_datum = d3.select(this).datum();
            f_ry.domain([0, obj_datum.value]);
            initialize(obj_datum);
            f_ry.domain([0, height_g]);
            layout(obj_datum);

            d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", null);
            d3.select(this).datum(obj_datum);
            d3.select(this).selectAll('rect').transition().duration(750).call(rect).each("end", function() {
                d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", "crispEdges");
            });
        });

        var f_scale_Y = d3.scale.linear().domain([0, 100]).range([height_g, 0]);
        var xAxis = d3.svg.axis().scale(f_scale_Y).orient("left").tickFormat(function(d) {
            return d + "%"
        });
        svg_expendeture.select(".expenditure_axis").call(xAxis).selectAll('.tick text').attr('y', function(d) {
            return d === 100 ? 5 : 0;
        });
    }
    compare_graphic_other_years('.content_control_expenditure', form_show);
});


document.getElementById("rad-total-revenue").checked = true;
var svg_from_rad = d3.select(d3.select("#rad-total-revenue").node().parentNode).select('svg');
draw_radio_button(svg_from_rad, animDefs);
d3.selectAll(".rad-revenue").on("change", function() {
    d3.selectAll('.content_control_revenue .list-option-radio path').remove();

    var svg_from_rad = d3.select(this.parentNode).select('svg');
    draw_radio_button(svg_from_rad, animDefs);

    if (document.getElementById("rad-total-revenue").checked) {
        svg_revenue.selectAll(".depth").each(function() {
            form_show_r = "millon";
            var copy_this = this;
            var obj_datum = d3.select(this).datum();
            f_ry.domain([0, max_value_every_year_r]);
            initialize(obj_datum);
            f_ry.domain([0, height_g]);
            layout(obj_datum);

            d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", null);
            d3.select(this).datum(obj_datum);
            d3.select(this).selectAll('rect').transition().duration(750).call(rect).each('end', function() {
                d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", "crispEdges");
            });
        });

        var f_scale_Y = d3.scale.linear().domain([0, max_value_every_year_r]).range([height_g, 0]);
        var xAxis = d3.svg.axis().scale(f_scale_Y).orient("left").tickFormat(function(d) {
            return d / 1000000000 + "b"
        });
        svg_revenue.select(".revenue_axis").call(xAxis);

    } else {
        svg_revenue.selectAll(".depth").each(function() {
            form_show_r = "percentaje";
            var copy_this = this;
            var obj_datum = d3.select(this).datum();
            f_ry.domain([0, obj_datum.value]);
            initialize(obj_datum);
            f_ry.domain([0, height_g]);
            layout(obj_datum);

            d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", null);
            d3.select(this).datum(obj_datum);
            d3.select(this).selectAll('rect').transition().duration(750).call(rect).each("end", function() {
                d3.select(copy_this.parentNode).selectAll(".depth").style("shape-rendering", "crispEdges");
            });
        });
        var f_scale_Y = d3.scale.linear().domain([0, 100]).range([height_g, 0]);
        var xAxis = d3.svg.axis().scale(f_scale_Y).orient("left").tickFormat(function(d) {
            return d + "%"
        });
        svg_revenue.select(".revenue_axis").call(xAxis).selectAll('.tick text').attr('y', function(d) {
            return d === 100 ? 5 : 0;
        });
    }
    compare_graphic_other_years('.content_control_revenue', form_show_r);
});


var obj_json = {},
    obj_array_revenue = {};
d3.csv(url_sub_expenditure_csv, function(error, programme) {
    var array_obj_enlace = build_enlace(programme, year_default);

    d3.csv(url_expenditure_csv, function(error, expend) {
        var array_key_select = build_array_key_select(expend, array_year);
        var response_array_all_elements = {}; // variable than store all the information from the expend in order to list

        var string_option_years = "";
        array_key_select.forEach(function(y_s) {
            if (y_s.year <= year_default - 1)
                string_option_years += "<option value='" + y_s.year + "' " + ((y_s.year === year_default - 1) ? "selected" : "") + ">" + y_s.year + "</option>";
            obj_json[y_s.year] = {};
            obj_json[y_s.year].name = y_s.year;
            obj_json[y_s.year].lastname = "Expenditure";
            obj_json[y_s.year].children = build_json(array_branches, y_s, expend, year_default, array_obj_enlace, response_array_all_elements);
        });


        //start to select year compare to

        $(".control-expediture .option_year_expenditure").html(string_option_years);

        $(".option_year_expenditure").select2({
            width: "100%",
            minimumResultsForSearch: Infinity
        }).on("change", function(event) {
            var selectedIndex = this.selectedIndex;
            var selected_year = array_year[selectedIndex];
            if (d3.select(".g_first_child .depth").empty()) {
                d3.select(".control-expediture .text_graphic_complete").datum([obj_json[year_default], obj_json[selected_year]]);
            } else {
                if (d3.select(".g_second_child .depth").empty()) {
                    d3.selectAll(".expenditure" + selected_year + " .children").each(function(d) {
                        if (d.name.trim() === d3.select(".g_first_child .depth").datum().name.trim()) {
                            d3.select(".control-expediture .text_graphic_complete").datum([d3.select(".g_first_child .depth").datum(), d]);
                        }
                    });
                } else {
                    d3.selectAll(".expenditure" + selected_year + " .children rect.child").each(function(d) {
                        if (d.name.trim() === d3.select(".g_second_child .depth").datum().name.trim()) {
                            d3.select(".control-expediture .text_graphic_complete").datum([d3.select(".g_second_child .depth").datum(), d]);
                        }
                    });
                }
            }
            compare_graphic_other_years('.content_control_expenditure', form_show);
        });

        // d3.select(".control-expediture .option_year_expenditure").on("change", function() {
        //     // d3.select(".control-expediture .option_year_expenditure").selectAll("option").data(array_key_select).enter().append('option').text(function(d){
        //     //     return d.year;
        //     // }).on("change", function() {
        //     var selectedIndex = this.selectedIndex;
        //     var selected_year = array_year[selectedIndex];
        //     if (d3.select(".g_first_child .depth").empty()) {
        //         d3.select(".control-expediture .text_graphic_complete").datum([obj_json[year_default], obj_json[selected_year]]);
        //     } else {
        //         if (d3.select(".g_second_child .depth").empty()) {
        //             d3.selectAll(".expenditure" + selected_year + " .children").each(function(d) {
        //                 if (d.name.trim() === d3.select(".g_first_child .depth").datum().name.trim()) {
        //                     d3.select(".control-expediture .text_graphic_complete").datum([d3.select(".g_first_child .depth").datum(), d]);
        //                 }
        //             });
        //         } else {
        //             d3.selectAll(".expenditure" + selected_year + " .children rect.child").each(function(d) {
        //                 if (d.name.trim() === d3.select(".g_second_child .depth").datum().name.trim()) {
        //                     d3.select(".control-expediture .text_graphic_complete").datum([d3.select(".g_second_child .depth").datum(), d]);
        //                 }
        //             });
        //         }
        //     }
        //     compare_graphic_other_years('.content_control_expenditure', form_show);
        // });
        //finish to year compare to

        // total_year = d3.set(total_year).values();
        d3.csv(url_revenue_csv, function(error, revenue) {
            var array_key_select = build_array_key_select(revenue, array_year);
            obj_array_revenue = build_json_revenue(revenue, array_key_select);

            var data_diference_total = initialize_all_year(array_key_select, obj_json, obj_array_revenue);

            //start control revenue right
            $(".control-revenue .option_year_revenue").html(string_option_years);
            /*end of array selects*/
            $(".option_year_revenue").select2({
                width: "100%",
                minimumResultsForSearch: Infinity
            }).on("change", function(event) {
                var selectedIndex = this.selectedIndex;
                var selected_year = array_year[selectedIndex];
                if (d3.select(".svg_revenue .g_first_child .depth").empty()) {
                    d3.select(".control-revenue .text_graphic_complete").datum([obj_array_revenue[year_default], obj_array_revenue[selected_year]]);
                } else {}
                compare_graphic_other_years('.content_control_revenue', form_show_r);
            });
            // d3.select(".control-revenue .option_year_revenue").on("change", function() {
            //     var selectedIndex = this.selectedIndex;
            //     var selected_year = array_year[selectedIndex];
            //     if (d3.select(".svg_revenue .g_first_child .depth").empty()) {
            //         d3.select(".control-revenue .text_graphic_complete").datum([obj_array_revenue[year_default], obj_array_revenue[selected_year]]);
            //     } else {}
            //     compare_graphic_other_years('.content_control_revenue', form_show_r);
            // });
            //finish control revenue right


            // obj_year_default = obj_array_revenue[year_default];
            // obj_year_ant = obj_array_revenue[2013];
            // root = obj_json[year_default];
            // root_2013 = obj_json[2013];

            //start asigment values general to content control
            d3.select(".content_control_expenditure .text_graphic_complete").datum([obj_json[year_default], obj_json[year_default - 1]]);
            d3.select(".content_control_revenue .text_graphic_complete").datum([obj_array_revenue[year_default], obj_array_revenue[year_default - 1]]);
            //finish asigment values general to content control

            //start compare graphics with other year
            compare_graphic_other_years('.content_control_expenditure', form_show);
            compare_graphic_other_years('.content_control_revenue', form_show_r);
            //finish compare graphics with other year

            //start to display first block budeget
            display_block_budget(obj_json, svg_expendeture, array_key_select);
            display_block_budget(obj_array_revenue, svg_revenue, array_key_select);
            //finish to display first block budeget


            //start create first breadcumb
            create_breadcumb(obj_json[year_default], ".st-breadcumb-expenditure");
            create_breadcumb(obj_array_revenue[year_default], ".st-breadcumb-revenue");
            d3.select(".st-breadcumb-expenditure").style("margin-left", (tranlate_svg_expendeture - 30) + "px");
            d3.select(".st-breadcumb-revenue").style("margin-left", (tranlate_svg_revenue - 30) + "px");
            //finish create first breadcumb


            var f_finish_animation_e = 0;
            $('.st-graphic-expenditure').waypoint(function(dir) {
                if (dir === "down" && f_finish_animation_e === 0) {
                    f_finish_animation_e = 1;
                    var g_hand_mov = d3.select(this).select("svg").append("g").attr("class", "g_hand_mov").attr("transform", "translate(" + (tranlate_svg_expendeture + width_g - 30) + ",0)");
                    g_hand_mov.append("polygon").attr("points", "17.3,25.5 17.2,15.2 16,14.1 14.8,14.1 13.6,15.2 13.6,32.9 12.2,32.9 8.5,29.3 6.3,29.4 5.6,30 5.7,32 12,38.3 16.3,42.4 16.4,45.8 27.5,44.9 27.5,41.4 29.6,39.3 29.8,26.6 28.7,25.5")
                        .attr('opacity', 0.85);
                    var h_circle = g_hand_mov.append("circle").attr('opacity', 0.85).attr('fill', 'none').attr('stroke', '#000000')
                        .attr('stroke-width', 2).attr('stroke-miterlimit', 10).attr('cx', 15).attr('cy', 14.5).attr('r', 7.4);

                    g_hand_mov.transition().duration(3000).attr("transform", "translate(" + (tranlate_svg_expendeture + width_g - 30) + "," + (height_g - 36) + ")").each("end", function() {
                        g_hand_mov.transition().duration(3000).attr("transform", "translate(" + (tranlate_svg_expendeture + width_g - 30) + ",0)").each("end", function() {
                            g_hand_mov.transition().attr("opacity", 1).duration(500).attr("opacity", 0).attr("display", "none");
                        });
                    });

                    h_circle.call(circle_flash1);
                }
            }, {
                offset: '50%'
            });

            var f_finish_animation_r = 0;
            $('.st-graphic-revenue').waypoint(function(dir) {
                if (dir === "down" && f_finish_animation_r === 0) {
                    f_finish_animation_r = 1;
                    var g_hand_mov = d3.select(this).select("svg").append("g").attr("class", "g_hand_mov").attr("transform", "translate(" + (tranlate_svg_revenue + width_g - 30) + ",0)");;
                    g_hand_mov.append("polygon").attr("points", "17.3,25.5 17.2,15.2 16,14.1 14.8,14.1 13.6,15.2 13.6,32.9 12.2,32.9 8.5,29.3 6.3,29.4 5.6,30 5.7,32 12,38.3 16.3,42.4 16.4,45.8 27.5,44.9 27.5,41.4 29.6,39.3 29.8,26.6 28.7,25.5")
                        .attr('opacity', 0.85);
                    var h_circle = g_hand_mov.append("circle").attr('opacity', 0.85).attr('fill', 'none').attr('stroke', '#000000')
                        .attr('stroke-width', 2).attr('stroke-miterlimit', 10).attr('cx', 15).attr('cy', 14.5).attr('r', 7.4);

                    g_hand_mov.transition().duration(3000).attr("transform", "translate(" + (tranlate_svg_revenue + width_g - 30) + "," + (height_g - 36) + ")").each("end", function() {
                        g_hand_mov.transition().duration(3000).attr("transform", "translate(" + (tranlate_svg_revenue + width_g - 30) + ",0)").each("end", function() {
                            g_hand_mov.attr("opacity", 1).transition().duration(500).attr("opacity", 0).attr("display", "none");
                        });
                    });

                    h_circle.call(circle_flash1);
                }
            }, {
                offset: '50%'
            });
            create_autocomplete($("#input_search_e"), obj_json[year_default]);
            create_autocomplete_r($("#input_search_r"), obj_array_revenue[year_default]);



            $(".content_control_expenditure .term_max_increase, .content_control_expenditure .term_min_increase").on("click", 'span', function() {
                var text_search = $(this).text();
                if (!d3.selectAll(".st-graphic-expenditure .g_first_child .depth").empty()) {
                    d3.selectAll(".st-graphic-expenditure .g_first_child rect.parent").each(function(nd) {
                        if (nd.name.trim() === text_search) {
                            mouseout_rect(nd);
                            mark_rect_over(nd);
                            d3.select('.tooltip_expenditure').style("display", "none");
                        }
                    })
                } else {
                    d3.selectAll(".expenditure" + year_default + " rect.parent").each(function(nd) {
                        if (nd.name.trim() === text_search) {
                            mouseout_rect(nd);
                            mark_rect_over(nd);
                            d3.select('.tooltip_expenditure').style("display", "none");
                        }
                    });
                }
            });

            $(".content_control_revenue .term_max_increase, .content_control_revenue .term_min_increase").on("click", 'span', function() {
                var text_search = $(this).text();
                d3.selectAll(".revenue" + year_default + " rect.parent").each(function(nd) {
                    if (nd.name.trim() === text_search) {
                        mouseout_rect(nd);
                        mark_rect_over(nd);
                        d3.select('.tooltip_revenue').style("display", "none");
                    }
                });
            });
        });

        $('body').on('touchstart', function(event) {
            if (mobileCheck()) {
                if ($(event.target).parents('.tooltip_expenditure').length === 0 && $(event.target).parents('.st-graphic-expenditure svg').length === 0) {
                    if ($(".tooltip_expenditure").is(':visible')) {
                        var nd = d3.selectAll(".tooltip_expenditure").datum();
                        mouseout_rect(nd);
                        d3.select('.tooltip_revenue').style("display", "none");
                    }
                }
            }
        });

        window.onload = function(event) {};
        window.onresize = function(event) {};

    });
});

function list_tree2(d, resp, order) {
    if (d._children !== undefined) {
        if (order === undefined) {
            order = 1;
        } else {
            order++;
        }
        d._children.forEach(function(ch, i) {
            var group_by_name = (get_seudoparent(ch).parent.lastname !== "Revenue") ? get_seudoparent(ch).name : "";
            var is_parent = (ch.enlace === undefined) ? 1 : 0;

            if (resp[group_by_name] === undefined) resp[group_by_name] = [];

            resp[group_by_name].push({
                id: i,
                name: ch.name,
                isparent: is_parent,
                gruop_by: group_by_name,
                order: order
            });
            list_tree2(ch, resp, order);
        });
    }
    return false;
}

function list_tree(d, a_resp) {
    if (d._children !== undefined) {
        d._children.forEach(function(ch, i) {
            var group_by_name = (get_seudoparent(ch).parent.lastname !== "Revenue") ? get_seudoparent(ch).name : "";
            var is_parent = (ch.enlace === undefined) ? 1 : 0;
            a_resp.push({
                id: i,
                name: ch.name,
                isparent: is_parent,
                gruop_by: group_by_name
            });
            list_tree(ch, a_resp);
        });
    }
    return false;
}


function searc_data_in_tree(d, word, result) {
    if (d.name.toString().trim() === word) {
        result.push(d);
    } else if (d._children !== undefined) {
        d._children.forEach(function(w_e) {
            searc_data_in_tree(w_e, word, result);
        });
    }
    return false;
}



function create_autocomplete_r(etiq, data) {
    var object_data_show = {};
    list_tree2(data, object_data_show);
    var a_bloodhound = [];
    $.each(object_data_show, function(h) {
        var data_show = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: this,
            identify: function(obj) {
                return obj.name;
            }
        });
        data_show.initialize();
        a_bloodhound.push({
            source: data_show,
            display: 'name',
            // name: "prefixmonth",
            items: 6,
            showHintOnFocus: true,
            templates: {
                header: '<h3 class="head_list">' + h + '</h3>'
            }

        });

    });

    etiq.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, a_bloodhound)
        .bind("typeahead:change", function(event, s_value) {
            var current = $(this).typeahead('val');
            if (current) {
                if (current === value_selected_revenue) {
                    value_selected_revenue = s_value;
                } else {
                    value_selected_revenue = "";
                    etiq.typeahead("val", "");
                    d3.selectAll(".svg_revenue .depth rect.child").attr('fill', function(d) {
                        if (d3.select(this).classed("isdeployment_last")) {
                            var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[4] : "#F5AA8C";
                        } else if (d3.select(this).classed('isdeployment')) {
                            var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[3] : all_color_bar_expenditure[2];
                        } else {
                            var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                        }
                        return fill;
                    });
                }
            } else {
                etiq.typeahead("val", "");
                d3.selectAll(".svg_revenue .depth rect.child").attr('fill', function(d) {
                    if (d3.select(this).classed("isdeployment_last")) {
                        var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[4] : "#F5AA8C";
                    } else if (d3.select(this).classed('isdeployment')) {
                        var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[3] : all_color_bar_expenditure[2];
                    } else {
                        var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                    }
                    return fill;
                });
            }
        })
        .bind("typeahead:select", function(event, o_selected) {
            if (o_selected) {
                value_selected_revenue = o_selected.name;
                var obj_e = [];
                searc_data_in_tree(data, value_selected_revenue, obj_e);
                if (obj_e.length > 0) {
                    mouseout_rect(obj_e[0]);
                    mark_rect_over(obj_e[0]);
                    d3.select('.tooltip_revenue').style("display", "none");
                }
            }
        }).bind("typeahead:autocomplete", function(event, o_selected) {
            if (o_selected) {
                value_selected_revenue = o_selected.name;
            }
        });
}

function create_autocomplete(etiq, data) {
    var object_data_show = {};

    list_tree2(data, object_data_show);

    var a_bloodhound = [];
    $.each(object_data_show, function(h) {
        var data_show = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: this,
            identify: function(obj) {
                return obj.name;
            }
        });
        data_show.initialize();
        a_bloodhound.push({
            source: data_show,
            display: 'name',
            // name: "prefixmonth",
            items: 6,
            showHintOnFocus: true,
            templates: {
                header: '<h3 class="head_list">' + h + '</h3>'
            }

        });

    });

    etiq.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, a_bloodhound)
        .bind("typeahead:change", function(event, s_value) {
            var current = $(this).typeahead('val');
            if (current) {
                if (current === value_selected_expenditure) {
                    value_selected_expenditure = s_value;
                } else {
                    value_selected_expenditure = "";
                    etiq.typeahead("val", "");
                    d3.select(".st-graphic-expenditure .g_first_child").selectAll("*").remove();
                    d3.select(".st-graphic-expenditure .g_second_child").selectAll("*").remove();
                    d3.selectAll(".st-breadcumb-expenditure").selectAll("*").remove();
                    create_breadcumb(obj_json[year_default], ".st-breadcumb-expenditure");


                    var selectedIndex = d3.select(".option_year_expenditure").node().selectedIndex;
                    var selected_year = array_year[selectedIndex];

                    d3.select(".control-expediture .text_graphic_complete").datum([obj_json[year_default], obj_json[selected_year]]);
                    compare_graphic_other_years('.content_control_expenditure', form_show);



                    d3.selectAll(".svg_expendeture .depth rect.child").attr('fill', function(d) {
                        d3.select(this).classed("isdeployment", false);
                        d3.select(this).classed("isdeployment_last", false);
                        var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                        return fill;
                    });
                }
            } else {
                etiq.typeahead("val", "");
                d3.select(".st-graphic-expenditure .g_first_child").selectAll("*").remove();
                d3.select(".st-graphic-expenditure .g_second_child").selectAll("*").remove();
                d3.selectAll(".st-breadcumb-expenditure").selectAll("*").remove();
                create_breadcumb(obj_json[year_default], ".st-breadcumb-expenditure");


                var selectedIndex = d3.select(".option_year_expenditure").node().selectedIndex;
                var selected_year = array_year[selectedIndex];

                d3.select(".control-expediture .text_graphic_complete").datum([obj_json[year_default], obj_json[selected_year]]);
                compare_graphic_other_years('.content_control_expenditure', form_show);



                d3.selectAll(".svg_expendeture .depth rect.child").attr('fill', function(d) {
                    d3.select(this).classed("isdeployment", false);
                    d3.select(this).classed("isdeployment_last", false);
                    var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                    return fill;
                });
            }
        })
        .bind("typeahead:select", function(event, o_selected) {
            if (o_selected) {
                value_selected_expenditure = o_selected.name;
                var obj_e = [];
                searc_data_in_tree(data, value_selected_expenditure, obj_e);
                if (obj_e.length > 0) {
                    if (obj_e[0].parent.parent === undefined) {
                        click(obj_e[0]);
                    } else if (obj_e[0].parent.parent.parent === undefined) {
                        click(obj_e[0].parent);
                        var xtime = setTimeout(function() {
                            click(obj_e[0]);
                            clearTimeout(xtime);
                        }, 3000);
                    } else if (obj_e[0].parent.parent.parent.parent === undefined) {
                        click(obj_e[0].parent.parent);
                        var xtime = setTimeout(function() {
                            click(obj_e[0].parent);
                            var ytime = setTimeout(function() {
                                mark_rect_over(obj_e[0]);
                                d3.select('.tooltip_expenditure').style("display", "none");
                                clearTimeout(ytime);
                            }, 3000);
                            clearTimeout(xtime);
                        }, 3000);
                    }
                }
            }
        }).bind("typeahead:autocomplete", function(event, o_selected) {
            if (o_selected) {
                value_selected_expenditure = o_selected.name;
            }
        });

}


function circle_flash1() {
    this.transition().attr("opacity", 0.01)
        .duration(3000)
        .attr("r", 15)
        .each("end", circle_flash2);
}

function circle_flash3() {
    d3.select(this).transition().attr("opacity", 0.01)
        .duration(3000)
        .attr("r", 15);
}

function circle_flash2() {
    d3.select(this)
        .transition()
        .delay(300)
        .duration(0.0001)
        .attr("r", 7.5)
        .attr("opacity", 0.8)
        .each("end", circle_flash3);
}

function create_breadcumb(obj, etiq, new_f) {
    var unit_breadcumb = d3.select(etiq).empty() ? d3.select(etiq).append("div").attr("class", "unit_breadcumb") : d3.select(etiq).insert("div", ".unit_breadcumb").attr("class", "unit_breadcumb");
    if (obj.parent === undefined) {
        var name_display = (obj.lastname === "Expenditure") ? "Total Expenditure" : "Total Revenue";
        unit_breadcumb.append("div").attr("class", "breadcumb_name").html(name_display);
        unit_breadcumb.append("div").attr("class", "breadcumb_total").html("S$" + format_number(obj.value / 1000000) + "m");
        unit_breadcumb.append('div').attr("class", "breadcumb_percentage").html("(" + getPercetage(obj.value, obj.value) + "% of " + (obj.lastname === "Expenditure" ? "Expenditure)" : "Revenue)"));
    } else {
        unit_breadcumb.append('div').attr('class', 'arrow_left').append("img").attr("src", "images/arrow_next.svg").attr('class', 'st-image-responsive');
        unit_breadcumb = unit_breadcumb.append('div').attr('class', 'next_to_arrow');
        unit_breadcumb.append("div").attr("class", "breadcumb_name").html(obj.name.trim());
        unit_breadcumb.append("div").attr("class", "breadcumb_total").html("S$" + format_number(obj.value / 1000000) + "m");
        unit_breadcumb.append('div').attr("class", "breadcumb_percentage").style({
            "display": (new_f === true ? "block" : "none"),
            "opacity": (new_f === true ? 1 : 0)
        }).html("(" + getPercetage(obj.value, get_seudoparent(obj).parent.value) + "% of Expenditure)");
        create_breadcumb(obj.parent, etiq, true);
    }
}

function compare_graphic_other_years(etiq_general, form_show) {
    d3.selectAll(etiq_general + " .st_division_text").style("opacity", 1);

    var present = d3.select(etiq_general + " .text_graphic_complete").datum()[0];
    var past = d3.select(etiq_general + " .text_graphic_complete").datum()[1];

    var compare_percentaje = past.value !== 0 ? d3.round((((present.value - past.value) / past.value) * 100), 2) : present.value * 100;
    compare_percentaje = (compare_percentaje >= 0 ? "+" + compare_percentaje : compare_percentaje) + "%";

    var compare_millon = (present.value - past.value) / 1000000;
    compare_millon = (compare_millon >= 0 ? "+S$" + format_number(compare_millon) : "S$" + format_number(compare_millon)) + "m";

    var text_g = etiq_general === ".content_control_expenditure" ? "Expenditure" : "Revenue";
    var etiq_text_select = etiq_general === ".content_control_expenditure" ? ".option_year_expenditure" : ".option_year_revenue";
    var name = (present.parent === undefined) ? text_g : present.name;

    d3.select(etiq_general + " .text_graphic_complete").html(name + " <span class='text_bold'>" + (form_show === "millon" ? compare_millon : compare_percentaje) + "</span>");

    //compared to previous year
    if (present._children !== undefined && past._children !== undefined) {
        if (present._children.length && past._children.length) {
            var a_save_difference_obj = [];
            present._children.forEach(function(pr) {
                past._children.forEach(function(ps) {
                    if (pr.name.trim() === ps.name.trim()) {
                        a_save_difference_obj.push({
                            name: pr.name.trim(),
                            pr: pr.value,
                            ps: ps.value,
                            diff_millon: (pr.value - ps.value) / 1000000,
                            diff_percentaje: ps.value !== 0 ? d3.round((((pr.value - ps.value) / ps.value) * 100), 2) : pr.value
                        });
                    }
                });
            });

            var max_value = d3.max(a_save_difference_obj, function(d) {
                return (form_show === "millon") ? d.diff_millon : d.diff_percentaje;
            });
            var min_value = d3.min(a_save_difference_obj, function(d) {
                return (form_show === "millon") ? d.diff_millon : d.diff_percentaje;
            });

            a_save_difference_obj.forEach(function(d) {
                var diff = form_show === "millon" ? d.diff_millon : d.diff_percentaje;
                if (diff === max_value) {
                    var name_max = d.name;

                    var max_value_millon = max_value;
                    max_value_millon = (max_value_millon >= 0 ? "+S$" + format_number(max_value_millon) : "S$" + format_number(max_value_millon)) + "m";

                    var max_value_percentaje = max_value;
                    max_value_percentaje = (max_value_percentaje >= 0 ? "+" + max_value_percentaje : max_value_percentaje) + "%";

                    d3.select(etiq_general + " .max_increse_compareto").html(form_show === "millon" ? max_value_millon : max_value_percentaje);
                    d3.select(etiq_general + " .term_max_increase").html('').append("span").on("click", function(dddd) {

                    }).html(name_max);
                    d3.select(etiq_general + " .text_increase_compareTo").html("Increased the most from " + array_year[d3.select(etiq_text_select).node().selectedIndex]);

                } else if (diff === min_value) {
                    var name_min = d.name;

                    var min_value_millon = min_value;
                    var min_description_millon = "";
                    if (min_value_millon >= 0) {
                        min_value_millon = "+S$" + format_number(min_value_millon) + "m";
                        min_description_millon = "Increased the least from " + array_year[d3.select(etiq_text_select).node().selectedIndex];
                    } else {
                        min_value_millon = "S$" + format_number(min_value_millon) + "m";
                        min_description_millon = "Decreased the most from " + array_year[d3.select(etiq_text_select).node().selectedIndex];
                    }

                    var min_value_percentaje = min_value;
                    var min_description_percentaje = "";
                    if (min_value_percentaje >= 0) {
                        min_value_percentaje = "+" + min_value_percentaje + "%";
                        min_description_percentaje = "Increased the least from " + array_year[d3.select(etiq_text_select).node().selectedIndex];
                    } else {
                        min_value_percentaje = min_value_percentaje + "%";
                        min_description_percentaje = "Decreased the most from " + array_year[d3.select(etiq_text_select).node().selectedIndex];
                    }

                    d3.select(etiq_general + " .min_decrese_compareto").html(form_show === "millon" ? min_value_millon : min_value_percentaje);
                    d3.select(etiq_general + " .term_min_increase").html('').append("span").html(name_min);
                    d3.select(etiq_general + " .text_decrease_compareTo").html(form_show === "millon" ? min_description_millon : min_description_percentaje);
                }
            });
        }
    } else {
        d3.selectAll(etiq_general + " .st_division_text").style("opacity", 0);
    }
}

function click(d) {
    if (mobileCheck() && this !== window) {
        mouseout_rect(d);
        d3.select(this).each(create_tooltip_expenditure);
    } else if (d._children !== undefined && get_seudoparent(d).parent.name === year_default) {
        graphic_rect_child(d);
    }
}

function mobileCheck() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
}

function create_tooltip_expenditure(d) {
    if (transitions === 0) {
        var pseudo_p = get_seudoparent(d);
        var size_rect = this.getBBox();
        var copy_this = this;

        if (pseudo_p.parent.name !== year_default) {
            var select_etiq = pseudo_p.parent.lastname === "Expenditure" ? ".svg_expendeture .expenditure" + year_default : ".svg_revenue .revenue" + year_default;
            d3.selectAll(select_etiq + " rect.parent").each(function(dd) {
                if (d.name.trim() === dd.name.trim()) {
                    d = dd;
                    copy_this = this;
                    size_rect = this.getBBox();
                }
            });
        }

        mark_rect_over(d);


        var select_tooltip = pseudo_p.parent.lastname === "Expenditure" ? d3.select('.tooltip_expenditure') : d3.select('.tooltip_revenue');
        var tooltip_g = select_tooltip.datum(d).style("display", "block");
        tooltip_g.selectAll("*").remove();

        var select_svg = pseudo_p.parent.lastname === "Expenditure" ? svg : svg_r;
        var calc_position_box = select_svg.attr("height") - size_rect.y - margin.bottom;

        var result_millon = "S$" + format_number(d.value / 1000000) + "m";

        var result_percentage = getPercetage(d.value, get_seudoparent(d).parent.value) + "% of " + pseudo_p.parent.lastname;


        var translate_g = d3.transform(d3.select(copy_this.parentNode.parentNode.parentNode).attr('transform')).translate[0];
        var width_g_rect = 30;

        var trans_def = pseudo_p.parent.lastname === "Expenditure" ? tranlate_svg_expendeture : tranlate_svg_revenue;

        var translate_g = (d.parent.parent === undefined) ? (translate_g + trans_def) : translate_g;

        var calc_t = d3.transform(d3.select(".st-graphic-expenditure_translate").style("transform")).translate[0] || d3.transform(d3.select(".st-graphic-expenditure_translate").style("-webkit-transform")).translate[0] || d3.transform(d3.select(".st-graphic-expenditure_translate").style("-moz-transform")).translate[0] || d3.transform(d3.select(".st-graphic-expenditure_translate").style("-ms-transform")).translate[0];
        if (pseudo_p.parent.lastname === "Expenditure") {
            var t_left = document.getElementsByTagName('body')[0].clientWidth > 768 ? (width_g_rect + translate_g + 2) : (width_g_rect + translate_g + 2 + calc_t);
        } else {
            var t_left = (width_g_rect + translate_g + 2)
        }

        tooltip_g.style("left", t_left + "px");


        tooltip_g.append('div').attr('class', 'head_box_compare').html(d.name);

        var a_form_show = pseudo_p.parent.lastname === "Expenditure" ? form_show : form_show_r;
        tooltip_g.append('div').attr('class', 'head_box_compare').html((a_form_show === 'millon') ? result_millon : result_percentage);

        tooltip_g.append('div').attr('class', 'percentaje_box_compare').html("(" + ((a_form_show === 'millon') ? result_percentage : result_millon) + ")");

        if (d.parent.parent === undefined || (d.parent.parent !== undefined && d.parent.parent.parent === undefined)) {
            tooltip_g.append('div').attr('class', 'svg_box_compare').append('svg').attr("width", document.getElementsByTagName('body')[0].clientWidth ? 180 : 93).attr("height", 180)
                .append('g').datum(d).call(compare_obj_other_years);
        }

        if (d._children !== undefined) {
            tooltip_g.append('div').attr('class', 'content_btn_break_down').append('button').datum(d).html("SEE BREAKDOWN").on("click", graphic_rect_child);
        }

        //position vertical tooltip
        var height_div = select_tooltip.node().clientHeight;

        if (calc_position_box < height_div) {
            tooltip_g.style({
                "bottom": "15px",
                "top": "auto"
            });
        } else {
            var height_breadcumb = pseudo_p.parent.lastname === "Expenditure" ? d3.select('.st-breadcumb-expenditure').node().clientHeight + 15 : d3.select('.st-breadcumb-revenue').node().clientHeight;
            tooltip_g.style({
                "bottom": "auto",
                "top": (size_rect.y + 1 + height_breadcumb) + "px",
            });
        }

        tooltip_g
            .on('mouseover', mark_rect_over)
            .on('mouseout', mouseout_rect);

        //finish position vertical tooltip
    }
}

function mouseout_rect(obj) {
    var names = get_seudoparent(obj).parent.lastname === "Expenditure" ? "expenditure" : "revenue";
    var names2 = get_seudoparent(obj).parent.lastname === "Expenditure" ? "expendeture" : "revenue";
    d3.select('.tooltip_' + names).style("display", "none");
    if (transitions === 0) {
        if (obj.parent.parent == undefined) {
            d3.selectAll(".svg_" + names2 + " .depth rect.child").attr('fill', function(d) {
                if (d3.select(this).classed("isdeployment_last")) {
                    var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[4] : "#F5AA8C";
                } else if (d3.select(this).classed('isdeployment')) {
                    var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[3] : all_color_bar_expenditure[2];
                } else {
                    var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                }
                return fill;
            });
        } else if (obj.parent.parent.parent === undefined) {
            d3.selectAll(".g_first_child .depth rect.child").attr('fill', function(d) {
                if (d3.select(this).classed('isdeployment')) {
                    var fill = all_color_bar_expenditure[4];
                } else {
                    var fill = all_color_bar_expenditure[3];
                }
                return fill;
            });
            d3.selectAll(".svg_expendeture .depth rect.isdeployment").attr('fill', function(d) {
                if (d3.select(this).classed("isdeployment_last")) {
                    var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[4] : "#F5AA8C";
                } else {
                    var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[3] : all_color_bar_expenditure[2];
                }
                return fill;
            });

        } else if (obj.parent.parent.parent.parent === undefined) {
            d3.selectAll(".g_second_child .depth rect.child").attr('fill', function(d) {
                var fill = all_color_bar_expenditure[4];
                return fill;
            });
            d3.selectAll(".g_first_child .depth rect.isdeployment").attr('fill', function(d) {
                return all_color_bar_expenditure[4];
            });
        }
    }
}

function mark_rect_over(obj) {
    var names = get_seudoparent(obj).parent.lastname === "Expenditure" ? "expenditure" : "revenue";
    var names2 = get_seudoparent(obj).parent.lastname === "Expenditure" ? "expendeture" : "revenue";
    var tooltip_expenditure = d3.select('.tooltip_' + names).style("display", "block");
    if (obj.parent.parent == undefined) {
        d3.selectAll(".svg_" + names2 + " .depth g").each(function(d) {
            if (obj.name.trim() === d.name.trim()) {
                d3.select(this).selectAll("rect.child").attr("fill", function(nc) {
                    var fill_pattern = (get_seudoparent(d).parent.name === year_default) ? 'pattern_default' : 'pattern_before';
                    if (d3.select(this).classed("isdeployment_last"))
                        fill_pattern = (get_seudoparent(nc).parent.name === year_default) ? 'pattern_first_child' : 'g_second_child';

                    d3.select("#" + fill_pattern + " rect").attr("fill", d3.select(this).attr("fill"));
                    return 'url(#' + (names === 'expenditure' ? fill_pattern : fill_pattern + '_r') + ')';
                });
            }
        });
    } else if (obj.parent.parent.parent === undefined) {
        d3.selectAll(".g_first_child .depth rect.child").each(function(d) {
            if (d3.select(this.parentNode).datum().name.trim() === obj.name.trim()) {
                var fill_pattern = "pattern_first_child";
                d3.select("#" + fill_pattern + " rect").attr("fill", d3.select(this).attr("fill"));
                d3.select(this).attr("fill", 'url(#' + fill_pattern + ')');
            }
        });
        d3.selectAll(".svg_expendeture .depth rect.isdeployment").each(function(d) {
            if (d.name.trim() === obj.name.trim()) {
                var fill_pattern = (get_seudoparent(d).parent.name === year_default) ? 'pattern_default' : 'pattern_before';
                d3.select("#" + fill_pattern + " rect").attr("fill", d3.select(this).attr("fill"));
                d3.select(this).attr("fill", 'url(#' + fill_pattern + ')');
            }
        });
    } else if (obj.parent.parent.parent.parent === undefined) {
        d3.selectAll(".g_second_child .depth rect.child").attr('fill', function(d) {
            var name_compare = (d3.select(this.parentNode).select("rect.parent").datum()._children === undefined) ? d.name.trim() : d.parent.name.trim();

            d3.select("#g_second_child rect").attr("fill", all_color_bar_expenditure[4]);
            var fill = (name_compare === obj.name.trim()) ? 'url(#g_second_child)' : all_color_bar_expenditure[4];
            return fill;
        });

        d3.selectAll(".g_first_child .depth rect.isdeployment").attr('fill', function(d) {
            var fill = (d.name.trim() === obj.name.trim()) ? 'url(#g_second_child)' : all_color_bar_expenditure[4];
            return fill;
        });
    }
}

function graphic_rect_child(d) {
    //start graphic breadcumb expenditure
    d3.select(".st-breadcumb-expenditure").html("");
    create_breadcumb(d, ".st-breadcumb-expenditure");
    //finish graphic breadcumb expenditure

    d3.select('.tooltip_expenditure').style("display", "none");
    tranlate_svg_expendeture = width_window < 768 ? tranlate_svg_expendeture : position_axis;
    if (d.parent.parent === undefined) {
        transitions = 1;

        g_first_child.select(".depth").remove();
        g_first_child.selectAll(".btn_go_back").remove();
        g_second_child.select(".depth").remove();
        g_second_child.selectAll(".btn_go_back2").remove();
        g_first_child.select(".small_scale").remove();
        g_second_child.select(".small_scale").remove();
        g_first_child.attr('transform', 'translate(' + (120 + tranlate_svg_expendeture) + ',0)');


        var value_domain = (form_show === "millon") ? max_value_every_year : d.parent.value;
        f_ry.domain([0, value_domain]);
        initialize(d.parent);
        f_ry.domain([0, height_g]);
        layout(d.parent);

        var obj_last_year = {};
        var obj_current_year = {};


        if (width_window > 768) d3.select(".st-breadcumb-expenditure").transition().duration(500).style("margin-left", "0px").each("end", function() {
            d3.selectAll(".st-breadcumb-expenditure .breadcumb_percentage").transition().duration(500).style({
                "display": "block",
                "opacity": 1
            });
        });
        svg_expendeture.transition().duration(1000).attr('transform', 'translate(' + tranlate_svg_expendeture + ',0)')
            .each('end', function() {
                display(d, g_first_child);

                var move_ini = (width_complete - width_window >= 120) ? 120 : width_complete - width_window;
                var move_bar = width_window >= 524 ? (width_window / 2) - (width_u / 2) : (width_window / 2) - (width_u / 2) + move_ini;

                // animateScroll("st-graphic-expenditure_content", move_ini,750);

                if (width_window <= 768) {
                    d3.select(".st-graphic-expenditure_translate").classed("move_126", true);
                    var new_b = g_first_child.transition().duration(1000).attr('transform', 'translate(' + (tranlate_svg_expendeture + width_g + 126) + ',0)');
                } else {
                    var new_b = g_first_child.transition().duration(1000).attr('transform', 'translate(' + move_bar + ',0)');
                }

                new_b.each('end', function() {
                    var copy_this = this;
                    f_ry.domain([0, d.value]);
                    initialize(d);
                    f_ry.domain([0, height_g]);
                    layout(d);

                    d3.select(copy_this).selectAll(".depth").style("shape-rendering", null);
                    d3.select(this).datum(d);
                    d3.select(this).selectAll('.depth rect').transition().duration(750).call(rect2).each('end', function() {
                        d3.select(copy_this).selectAll(".depth").style("shape-rendering", "crispEdges");
                        transitions = 0;
                    });
                });
                var btn_go_back = g_first_child.append("g").attr("class", "btn_go_back");
                btn_go_back.append("rect").attr("width", 27).attr('height', 27).attr('fill', '#E7EAED');
                btn_go_back.append('polyline').attr('points', '13,19 8.8,14.8 13,10.5').attr("stroke", "#666").attr('stroke-width', 1.5).attr('stroke-miterlimit', 10).attr("fill", "none");
                btn_go_back.append('polyline').attr('points', '8.8,14.8 19.9,14.8 19.9,7.4 17.3,7.4').attr("stroke", "#666").attr('stroke-width', 1.5).attr('stroke-miterlimit', 10).attr("fill", "none");
                btn_go_back.attr("transform", "translate(99,0)").attr("cursor", "pointer").on("click", function() {
                    d3.select(".st-graphic-expenditure_translate").classed("move_126", false);
                    d3.select(".st-graphic-expenditure_translate").classed("move_252", false);

                    g_first_child.select(".depth").remove();
                    g_first_child.selectAll(".btn_go_back").remove();
                    g_second_child.select(".depth").remove();
                    g_second_child.selectAll(".btn_go_back2").remove();
                    g_first_child.select(".small_scale").remove();
                    g_second_child.select(".small_scale").remove();
                    d3.select(d3.selectAll(".unit_breadcumb")[0][1]).remove();
                    d3.select(d3.selectAll(".unit_breadcumb")[0][2]).remove();

                    d3.selectAll(".svg_expendeture .depth rect.child").attr('fill', function(d) {
                        d3.select(this).classed("isdeployment", false);
                        d3.select(this).classed("isdeployment_last", false);
                        var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                        return fill;
                    });

                });
                if (!document.getElementById("rad-total-expenditure").checked) g_first_child.select(".small_scale").attr("opacity", 0);
            }).selectAll('g.depth .children').each(function(dd) {
                if (dd.name.trim() === d.name.trim()) {
                    if (get_seudoparent(dd).parent.name === array_year[d3.select(".option_year_expenditure").node().selectedIndex]) {
                        obj_last_year = dd;
                    }
                    if (get_seudoparent(dd).parent.name === year_default) {
                        obj_current_year = dd;
                    }
                }


                d3.select(this).selectAll(".child").classed("isdeployment_last", false);
                if (get_seudoparent(dd).parent.name === year_default) {
                    var fill_color = (dd.name.trim() === d.name.trim()) ? all_color_bar_expenditure[3] : all_color_bar_expenditure[1];
                    d3.select(this).selectAll(".child").attr('fill', fill_color).classed('isdeployment', dd.name.trim() === d.name.trim());
                } else {
                    var fill_color = (dd.name.trim() === d.name.trim()) ? all_color_bar_expenditure[2] : all_color_bar_expenditure[0];
                    d3.select(this).selectAll(".child").attr('fill', fill_color).classed('isdeployment', dd.name.trim() === d.name.trim());
                }
            });

        //create right graphic sumary
        d3.select(".text_graphic_complete").datum([obj_current_year, obj_last_year]);
        compare_graphic_other_years('.content_control_expenditure', form_show);

    } else if (d.parent.parent.parent === undefined) {
        g_first_child.select(".btn_go_back").style("display", "none");

        g_second_child.select(".depth").remove();
        g_second_child.select(".small_scale").remove();
        g_second_child.attr('transform', 'translate(' + 120 + ',0)');

        transitions = 1;
        var value_domain = (form_show === "millon") ? max_value_every_year : d.parent.value;

        //re-init graphic
        f_ry.domain([0, d.parent.value]);
        initialize(d.parent);
        f_ry.domain([0, height_g]);
        layout(d.parent);
        //finish re-init graphic


        var obj_last_year = {};
        var obj_current_year = {};



        var move_bar = 0;
        var move_bar2 = 0;
        move_bar2 = width_complete - width_u - 210;
        move_bar = (move_bar2 / 2) + (width_g / 2) - 10;


        // g_first_child.transition().duration(1000).attr('transform', "translate(" + move_bar + ")")
        g_first_child.transition().duration(1000).attr('transform', "translate(" + (tranlate_svg_expendeture + width_g + 126) + ",0)")
            .each("end", function() {
                var actual_translate = d3.transform(g_first_child.attr("transform")).translate;

                g_second_child.attr('transform', "translate(" + (tranlate_svg_expendeture + width_g + 126) + ",0)");
                if (width_window <= 768) {
                    d3.select(".st-graphic-expenditure_translate").classed("move_252", true);
                }
                display(d, g_second_child);

                g_second_child.transition().duration(1000).attr('transform', 'translate(' + (tranlate_svg_expendeture + width_g + 126 + 156) + ',0)')
                    .each('end', function() {
                        var copy_this = this;
                        f_ry.domain([0, d.value]);
                        initialize(d);
                        f_ry.domain([0, height_g]);
                        layout(d);

                        d3.select(copy_this).selectAll(".depth").style("shape-rendering", null);
                        d3.select(this).datum(d);
                        d3.select(this).selectAll('.depth rect').transition().duration(750).call(rect3).each('end', function() {
                            d3.select(copy_this).selectAll(".depth").style("shape-rendering", "crispEdges");
                            transitions = 0;
                        });
                    });

                var btn_go_back2 = g_second_child.append("g").attr("class", "btn_go_back2");
                btn_go_back2.append("rect").attr("width", 27).attr('height', 27).attr('fill', '#E7EAED');
                btn_go_back2.append('polyline').attr('points', '13,19 8.8,14.8 13,10.5').attr("stroke", "#666").attr('stroke-width', 1.5).attr('stroke-miterlimit', 10).attr("fill", "none");
                btn_go_back2.append('polyline').attr('points', '8.8,14.8 19.9,14.8 19.9,7.4 17.3,7.4').attr("stroke", "#666").attr('stroke-width', 1.5).attr('stroke-miterlimit', 10).attr("fill", "none");
                btn_go_back2.attr("transform", "translate(99,0)").attr("cursor", "pointer").on("click", function() {
                    d3.select(".st-graphic-expenditure_translate").classed("move_252", false);
                    g_first_child.select(".btn_go_back").style("display", "block");

                    g_second_child.select(".depth").remove();
                    g_second_child.selectAll(".btn_go_back2").remove();
                    g_second_child.select(".small_scale").remove();
                    d3.select(d3.selectAll(".unit_breadcumb")[0][2]).remove();

                    d3.selectAll(".svg_expendeture .depth rect.child").attr('fill', function(d) {
                        d3.select(this).classed("isdeployment_last", false);
                        if (d3.select(this).classed("isdeployment_last")) {
                            var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[4] : "#F5AA8C";
                        } else if (d3.select(this).classed('isdeployment')) {
                            var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[3] : all_color_bar_expenditure[2];
                        } else {
                            var fill = (get_seudoparent(d).parent.name === year_default) ? all_color_bar_expenditure[1] : all_color_bar_expenditure[0];
                        }


                        return fill;
                    });

                    d3.selectAll(".st-graphic-expenditure .g_first_child rect.child").attr('fill', function() {
                        d3.select(this).classed("isdeployment", false);
                        var fill = all_color_bar_expenditure[3];
                        return fill;
                    });

                });
                d3.selectAll(".st-breadcumb-expenditure .breadcumb_percentage").transition().duration(500).style({
                    "display": "block",
                    "opacity": 1
                });
                if (!document.getElementById("rad-total-expenditure").checked) g_second_child.select(".small_scale").attr("opacity", 0);
            }).selectAll('g.depth .children').each(function(dd) {
                var fill_color = (dd.name.trim() === d.name.trim()) ? all_color_bar_expenditure[4] : all_color_bar_expenditure[3];
                d3.select(this).selectAll(".child").attr('fill', fill_color).classed("isdeployment", dd.name.trim() === d.name.trim());
            });

        obj_current_year = d;
        d3.selectAll(".svg_expendeture .depth rect.isdeployment").each(function(dd) {
            if (get_seudoparent(dd).parent.name === year_default) {
                if (dd.name.trim() === d.name.trim()) {
                    d3.select(this).classed("isdeployment_last", true).attr('fill', all_color_bar_expenditure[4]);
                } else {
                    d3.select(this).classed("isdeployment_last", false).attr('fill', all_color_bar_expenditure[3]);
                }
            } else {
                if (dd.name.trim() === d.name.trim()) {
                    d3.select(this).classed("isdeployment_last", true).attr('fill', "#F5AA8C");
                } else {
                    d3.select(this).classed("isdeployment_last", false).attr('fill', all_color_bar_expenditure[2]);
                }
            }
        });

        d3.selectAll(".expenditure" + array_year[d3.select(".option_year_expenditure").node().selectedIndex] + " .depth rect.child").each(function(dc) {
            if (dc.name.trim() === d.name.trim()) {
                obj_last_year = dc;
            }
        });

        //create right graphic sumary
        d3.select(".text_graphic_complete").datum([obj_current_year, obj_last_year]);
        compare_graphic_other_years('.content_control_expenditure', form_show);
    }
}

function animateScroll(class_etiq, scrollTo, time) {
    var etiq = document.getElementsByClassName(class_etiq)[0];
    var start = etiq.scrollLeft;
    var finish = scrollTo;
    if (start !== finish) {
        var time_interval = time / Math.abs(finish - start);
        var fmove = start;
        var set_interval = setInterval(function() {
            (finish >= start) ? fmove++ : fmove--;
            etiq.scrollLeft = fmove;
            if (fmove === finish || fmove === start) {
                clearInterval(set_interval);
            }
        }, time_interval);
    }
}

function compare_obj_other_years() {
    var copy_this = this;
    var obj = copy_this.datum();

    var array_obj_comparation = [];
    var rect_width = document.getElementsByTagName('body')[0].clientWidth;
    if (rect_width > 370) {
        if (rect_width > 400) {
            rect_width = 180;
        } else {
            rect_width = 150;
        }
    } else {
        rect_width = 93;
    }

    var rect_height = 120;
    var width_text = 50;
    var width_text_r = document.getElementsByTagName('body')[0].clientWidth
    if (width_text_r > 370) {
        if (width_text_r > 400) {
            width_text_r = 100;
        } else {
            width_text_r = 90;
        }
    } else {
        width_text_r = 80;
    }

    var rect_scale = d3.scale.linear().range([0, rect_width - width_text_r]);


    var p_seudo = get_seudoparent(obj).parent.lastname;
    var select_all_rect = p_seudo === "Expenditure" ? d3.selectAll(".svg_expendeture g.depth rect") : d3.selectAll(".svg_revenue g.depth rect.parent");
    var o_form_show = p_seudo === "Expenditure" ? form_show : form_show_r;
    select_all_rect.each(function(o_rect) {
        if (o_rect.name.trim() === obj.name.trim()) {
            array_obj_comparation.push(o_rect);
        }
    });

    var value_max = d3.max(array_obj_comparation, function(d) {
        return (o_form_show === 'millon') ? obj.value : format_number((d.value / get_seudoparent(d).parent.value) * 100);
    });

    rect_scale.domain([0, value_max]);

    var a_obj_order = array_obj_comparation.sort(function(a, b) {
        return d3.descending(get_seudoparent(a).parent.name, get_seudoparent(b).parent.name);
    });

    var g_rect = copy_this.selectAll("g").data(a_obj_order).enter().append('g')
        .attr('transform', function(d, i) {
            return 'translate ( 0,' + (i * 45) + ')';
        });

    g_rect.append('text').attr('dominant-baseline', 'middle').attr('y', function(d, i) {
        return 10;
    }).text(function(d) {
        return get_seudoparent(d).parent.name;
    }).attr('fill', '#666');

    g_rect.append('rect').attr('fill', "#fff").attr('height', 16).attr('width', function(d) {
        return (o_form_show === 'millon') ? rect_scale(d.value) : rect_scale(format_number((d.value / get_seudoparent(d).parent.value) * 100));
    }).attr('y', 20);

    g_rect.append('text').attr('dominant-baseline', 'middle').attr('y', function(d, i) {
        return 30;
    }).text(function(d) {
        return (o_form_show === 'millon') ? 'S$' + format_number(d.value / 1000000) + 'm' : format_number((d.value / get_seudoparent(d).parent.value) * 100) + '%';
    }).attr('x', function(d) {
        var mov = (o_form_show === 'millon') ? rect_scale(d.value) : rect_scale(format_number((d.value / get_seudoparent(d).parent.value) * 100));
        return mov + (document.getElementsByTagName('body')[0].clientWidth > 400 ? 10 : 0);
    }).attr('fill', '#333');

    return;
}

function draw_radio_button(svg_from_rad, animDefs) {
    var path = svg_from_rad.append('path').attr('d', pathDefs.fill[0]);
    var length_path = path.node().getTotalLength();

    path.attr("stroke-dasharray", length_path + " " + length_path).attr("stroke-dashoffset", length_path)
        .transition()
        .duration(500)
        .ease(animDefs.fill.easing, animDefs.fill.speed)
        .attr("stroke-dashoffset", 0);
}

function display_block_budget(obj_json, svg, array_year) {
    var total_years = array_year.length;
    var margin_graphic = 10;
    var new_width_g = width_g - ((total_years - 1) * margin_graphic);
    var a_max_values = [];
    var class_first_bar = svg.attr('class') === "svg_expendeture" ? "expenditure" : "revenue";

    array_year.forEach(function(d, i) {
        var translate_x = (i * (new_width_g / total_years)) + (i * margin_graphic);
        var svg_g = svg.append("g")
            .attr("transform", "translate(" + translate_x + "," + margin.top + ")").attr('class', class_first_bar + d.year);
        svg_g.append("g").attr("class", "grandparent");
        a_max_values.push(obj_json[d.year].value);
        display(obj_json[d.year], svg_g);
    });

    var g_axis = svg.append("g").attr('class', class_first_bar + "_axis").attr("transform", "translate(-5,1)");
    class_first_bar === 'expenditure' ? max_value_every_year = d3.max(a_max_values) : max_value_every_year_r = d3.max(a_max_values);

    var f_scale_Y = d3.scale.linear().domain([0, class_first_bar === 'expenditure' ? max_value_every_year : max_value_every_year_r]).range([height_g, 0]);
    var xAxis = d3.svg.axis().scale(f_scale_Y).orient("left").tickFormat(function(d) {
        return d / 1000000000 + "b"
    });
    g_axis.call(xAxis);
}

function display(d, svg) {
    var color_bar = '#fff';
    if (svg.attr('class') === ("expenditure" + year_default)) {
        color_bar = all_color_bar_expenditure[1];
        svg.append('text').attr("y", height_g + margin.bottom).attr("x", 15).attr('class', 'text_ligther').attr("text-anchor", "middle").text(d.name);

        var f_scale_exp = d3.scale.linear().domain([0, d.value / 1000000000]).range([0, height_g]);
        var small_scale = svg.append("g").attr("class", "small_scale").attr('transform', 'translate(-3,0)');
        small_scale.append("line").attr('x1', 0).attr('y1', 0).attr('x2', 0).attr('y2', f_scale_exp(1));
        small_scale.append('text').attr('x', -20).attr('y', 10).text("1b");

    } else if (svg.attr('class') === ("revenue" + year_default)) {
        color_bar = all_color_bar_expenditure[0];
        svg.append('text').attr("y", height_g + margin.bottom).attr("x", 15).attr('class', 'text_ligther').attr("text-anchor", "middle").text(d.name);
    } else if (svg.attr('class') === "g_first_child") {
        color_bar = all_color_bar_expenditure[3];

        var max_val_sc = d.value / 1000000000;
        var f_scale_exp = d3.scale.linear().domain([0, max_val_sc]).range([0, height_g]);
        var small_scale = svg.append("g").attr("class", "small_scale").attr('transform', 'translate(-5,0)');
        var val_scale = (max_val_sc <= 1) ? d3.round(max_val_sc / 2, 1) : 1;

        small_scale.append("line").attr('x1', 0).attr('y1', 0).attr('x2', 0).attr('y2', f_scale_exp(val_scale));
        small_scale.append('text').attr('x', max_val_sc < 1 ? -30 : -20).attr('y', 10).text(val_scale + "b");

    } else if (svg.attr('class') === "g_second_child") {
        color_bar = all_color_bar_expenditure[4];
        var max_val_sc = d.value / 1000000000;
        var f_scale_exp = d3.scale.linear().domain([0, max_val_sc]).range([0, height_g]);
        var small_scale = svg.append("g").attr("class", "small_scale").attr('transform', 'translate(-5,0)');

        var val_scale = (max_val_sc <= 1) ? d3.round(max_val_sc / 2, 1) : 1;
        small_scale.append("line").attr('x1', 0).attr('y1', 0).attr('x2', 0).attr('y2', f_scale_exp(val_scale));
        small_scale.append('text').attr('x', max_val_sc < 1 ? -30 : -20).attr('y', 10).text(val_scale + "b");

    } else {
        color_bar = all_color_bar_expenditure[0];
        svg.append('text').attr("y", height_g + margin.bottom).attr("x", 15).attr('class', 'text_ligther').attr("text-anchor", "middle").text(d.name);
    }

    var g = '';
    var g1 = svg.insert("g", ".grandparent").datum(d)
        .attr("class", "depth");


    var g = g1.selectAll("g").data(d._children).enter().append("g");

    g.sort(function(m, n) {
        return d3.descending(m.value, n.value);
    }).classed("children", function(dd) {
        return dd._children !== undefined;
    });

    g.selectAll(".child").data(function(d) {
        return d._children || [d];
    }).enter().append("rect").attr("class", "child").call(rect).attr('fill', color_bar);

    g.append("rect").attr("class", "parent")
        .on("mouseover", create_tooltip_expenditure)
        .on("mouseout", mouseout_rect)
        .on("click", click)
        .call(rect)
        .append("title").text(function(d) {
            return "S$" + format_number(d.value / 1000000) + 'm';
        });
    return g;
}

function rect(rect) {
    rect.attr("x", function(d) {
            return f_rx(d.x);
        })
        .attr("y", function(d) {
            var new_y = f_ry(d.y);
            return new_y;
        })
        .attr("width", function(d) {
            return f_rx(d.x + d.dx) - f_rx(d.x);
        })
        .attr("height", function(d) {
            var new_height = f_ry(d.y + d.dy) - f_ry(d.y);

            return new_height;
        });
}

function rect2(rect) {
    rect.attr("x", function(d) {
            return f_rx(d.x);
        })
        .attr("y", function(d) {
            var new_y = f_ry(d.y);
            // new_y = (d.parent.parent.parent === undefined && d3.select(this).classed('child')) ? (new_y + 1) : new_y;
            return new_y;
        })
        .attr("width", function(d) {
            return f_rx(d.x + d.dx) - f_rx(d.x);
        })
        .attr("height", function(d) {
            var new_height = f_ry(d.y + d.dy) - f_ry(d.y);
            // new_height = (d.parent.parent.parent === undefined && d3.select(this).classed('child')) ? (new_height - 1) : new_height;
            return new_height;
        });
}

function rect3(rect) {
    rect.attr("x", function(d) {
            return f_rx(d.x);
        })
        .attr("y", function(d) {
            var new_y = f_ry(d.y);
            // new_y = (d.parent.parent.parent.parent === undefined && d3.select(this).classed('child')) ? (new_y + 1) : new_y;
            return new_y;
        })
        .attr("width", function(d) {
            return f_rx(d.x + d.dx) - f_rx(d.x);
        })
        .attr("height", function(d) {
            var new_height = f_ry(d.y + d.dy) - f_ry(d.y);
            // new_height = (d.parent.parent.parent.parent === undefined && d3.select(this).classed('child')) ? (new_height - 1) : new_height;
            return new_height;
        });
}

function build_enlace(programme, year) {
    var val_enlace = null;
    var array_obj = {};
    var keys = d3.keys(programme[0]);
    programme.forEach(function(d) {
        var name_obj = d[keys[0]].trim();
        if (val_enlace !== name_obj) {
            val_enlace = name_obj;
            array_obj[name_obj] = [];
        }
        var value_s = parseInt(d[keys[2]].trim().replace(/,/g, ''));

        array_obj[name_obj].push({
            enlace: name_obj,
            year: year,
            name: d[keys[1]].trim(),
            value: (isNaN(value_s) || value_s === 0) ? 0 : value_s
        });
    });
    return array_obj;
}

function build_array_key_select(expend, array_year) {
    var array_key_select = [];
    d3.keys(expend[0]).forEach(function(key_g) {
        var key_select = {};
        array_year.forEach(function(year) {
            if (key_g.search(year) !== -1) {
                key_select.year = year;
                key_select.key_g = key_g;
                array_key_select.push(key_select);
            }
        });
    });
    return array_key_select;
}

function build_json(array_branche, y_s, expend_g, year_default, array_obj_enlace, response_array_all_elements) {
    var element_brach_parent = array_branche[0];
    var element_branch_child = array_branche[1];

    var result = [];
    var obj_parent = {};

    response_array_all_elements[y_s.year] = [];

    var a_all_expenditure = [];
    expend_g.forEach(function(obj_csv, i) {
        var value_sector = parseInt(obj_csv[y_s.key_g].trim().replace(/,/g, ''));
        value_sector = (isNaN(value_sector) || value_sector === 0) ? 0 : value_sector;

        var name_expenditure = obj_csv[element_brach_parent].trim();
        var name_sector = obj_csv[element_branch_child].trim();

        if ($.inArray(name_expenditure, a_all_expenditure) >= 0) {
            if (y_s.year === year_default && array_obj_enlace[name_sector] !== undefined) {
                obj_parent[name_expenditure].children.push({
                    name: name_sector,
                    value: value_sector,
                    children: array_obj_enlace[name_sector]
                });
            } else {
                obj_parent[name_expenditure].children.push({
                    name: name_sector,
                    value: value_sector
                });
            }
        } else {
            a_all_expenditure.push(name_expenditure);
            obj_parent[name_expenditure] = {};
            obj_parent[name_expenditure].name = name_expenditure;
            obj_parent[name_expenditure].children = [];

            if (y_s.year === year_default && array_obj_enlace[name_sector] !== undefined) {
                obj_parent[name_expenditure].children.push({
                    name: name_sector,
                    value: value_sector,
                    children: array_obj_enlace[name_sector]
                });
            } else {
                obj_parent[name_expenditure].children.push({
                    name: name_sector,
                    value: value_sector
                });
            }
        }
    });

    a_all_expenditure.forEach(function(d_expenditure) {
        result.push(obj_parent[d_expenditure]);
    })

    return result;
}

function build_json_revenue(revenue, array_key_select) {
    var obj_array_work = {};
    array_key_select.forEach(function(y_s) {
        var year_currently = y_s.year;
        var array_by_year = [];

        var obj_st = {};
        var obj_net_ii = {};
        revenue.forEach(function(obj_csv) {
            var value_obj_csv = parseInt(obj_csv[y_s.key_g].trim().replace(/,/g, ''));


            if (obj_csv.Revenue === "Special Transfers" || obj_csv.Revenue === "Net Investment Income/Returns Contribution") {
                if (obj_csv.Revenue === "Special Transfers") {
                    obj_st.name = obj_csv.Revenue;
                    obj_st.value = (isNaN(value_obj_csv) || value_obj_csv === 0) ? 0 : value_obj_csv;
                    obj_st.year = y_s.year;
                    obj_st.head = y_s.key_g;
                }
                if (obj_csv.Revenue === "Net Investment Income/Returns Contribution") {
                    obj_net_ii.name = obj_csv.Revenue;
                    obj_net_ii.value = (isNaN(value_obj_csv) || value_obj_csv === 0) ? 0 : value_obj_csv;
                    obj_net_ii.year = y_s.year;
                    obj_net_ii.head = y_s.key_g;
                }
            } else {
                var new_obj = {};
                new_obj.name = obj_csv.Revenue;
                new_obj.value = (isNaN(value_obj_csv) || value_obj_csv === 0) ? 0 : value_obj_csv;
                new_obj.year = y_s.year;
                new_obj.head = y_s.key_g;
                array_by_year.push(new_obj);
            }
        });
        obj_array_work[year_currently] = {};
        obj_array_work[year_currently].special_transfer = obj_st;
        obj_array_work[year_currently].net_investment_income = obj_net_ii;
        obj_array_work[year_currently].children = array_by_year;
        obj_array_work[year_currently].name = year_currently;
        obj_array_work[year_currently].lastname = "Revenue";
        obj_array_work[year_currently].sum = d3.sum(array_by_year, function(obj) {
            return obj.value;
        });
    });
    return obj_array_work;
}

function initialize_all_year(array_key_select, obj_json, obj_array_work) {
    var data_diference_total = [];

    var a_totals_expediture = [];
    var a_totals_revenue = [];

    array_key_select.forEach(function(key_select) {
        accumulate(obj_json[key_select.year]);
        accumulate(obj_array_work[key_select.year]);


        a_totals_expediture.push(obj_json[key_select.year].value);
        a_totals_revenue.push(obj_array_work[key_select.year].value);
    });

    array_key_select.forEach(function(key_select) {
        var tree_currently_expend = obj_json[key_select.year];
        var tree_currently_revenue = obj_array_work[key_select.year];



        //initialize tree all year expended
        f_ry.domain([0, d3.max(a_totals_expediture)]);
        initialize(tree_currently_expend);
        f_ry.domain([0, height_g]);
        layout(tree_currently_expend);




        //initialize tree all year expended
        f_ry.domain([0, d3.max(a_totals_revenue)]);
        initialize(tree_currently_revenue);
        f_ry.domain([0, height_g]);
        layout(tree_currently_revenue);




        //build  diference expend and revenue by year and push in array
        var diference_partial = tree_currently_revenue.sum - tree_currently_expend.value;
        var special_transfer = tree_currently_revenue.special_transfer.value;
        var net_investment_income = tree_currently_revenue.net_investment_income.value;

        data_diference_total.push([key_select.year, diference_partial - special_transfer + net_investment_income]);
    });

    return data_diference_total;
}

function initialize(root) {
    var dy = f_ry(root.value);
    var y = height_g - dy;
    root.x = root.y = y;
    root.dx = (width_g - ((array_year.length - 1) * 10)) / array_year.length;
    root.dy = dy;
    root.depth = 0;
}

// Aggregate the values for internal nodes. This is normally done by the
// treemap layout, but not here because of our custom implementation.
// We also take a snapshot of the original children (_children) to avoid
// the children being overwritten when when layout is computed.
function accumulate(d) {
    return (d._children = d.children) ? d.value = d.children.reduce(function(p, v) {
        return p + accumulate(v);
    }, 0) : d.value;
}



function layout(d) {
    if (d._children) {
        treemap.size([d.dx, d.dy])
            .nodes({
                _children: d._children
            });

        d._children.forEach(function(c) {
            // c.x = d.x + c.x * d.dx;
            // c.y = d.y + c.y * d.dy;
            // c.dx *= d.dx;
            // c.dy *= d.dy;

            c.y = d.y + c.y;
            c.parent = d;
            layout(c);
        });

    }
}

function format_number(number) {
    Math.floor(number);
    var val_return = 0;
    var format_number_int = d3.format(",d"),
        format_number_decimal = d3.format(",.2f");
    var prom = number % 1;
    if (prom === 0) {
        val_return = format_number_int(number);
    } else {
        if (format_number_decimal(prom) % 1 === 0) {
            val_return = format_number_int(Math.floor(number) + 1);
        } else {
            val_return = format_number_decimal(number);
        }
    }
    return val_return;
}

function getPercetage(part, total) {
    var compare = part / total * 100;
    return format_number(compare);
}



function get_seudoparent(d) {
    return (d.enlace !== undefined) ? d.parent.parent : (d.parent.parent === undefined) ? d : d.parent;
}
