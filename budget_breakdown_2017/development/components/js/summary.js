var $=require("jquery");
var d3=require('d3');

var url_sumary_svg1 = "svg/sumary_svg1.svg";
var url_sumary_medium_svg1 = "svg/sumary_medium_svg1.svg";
var url_sumary_mobile_svg1 = "svg/sumary_mobile_svg1.svg";
d3.xml(url_sumary_svg1, "image/svg+xml", function(xml) {
    if (navigator.appName === 'Microsoft Internet Explorer') {
        var importedNode = cloneToDoc(xml.documentElement);
    } else {
        var importedNode = document.importNode(xml.documentElement, true);
    }
    d3.select(".graphics").node().appendChild(importedNode);


    $(".go_to_expenditure").on("click", function() {
        $(window).scrollTo($('.st-expenditure'), 1000, {
            offset: 50
        });
    });
    $(".go_to_revenue").on("click", function() {
        $(window).scrollTo($('.st-revenue'), 1000, {
            offset: 50
        });
    });

});
d3.xml(url_sumary_medium_svg1, "image/svg+xml", function(xml) {
    if (navigator.appName === 'Microsoft Internet Explorer') {
        var importedNode = cloneToDoc(xml.documentElement);
    } else {
        var importedNode = document.importNode(xml.documentElement, true);
    }
    d3.select(".graphics_medium").node().appendChild(importedNode);

    $(".go_to_expenditure").on("click", function() {
        $(window).scrollTo($('.st-expenditure'), 1000, {
            offset: 50
        });
    });
    $(".go_to_revenue").on("click", function() {
        $(window).scrollTo($('.st-revenue'), 1000, {
            offset: 50
        });
    });

});
d3.xml(url_sumary_mobile_svg1, "image/svg+xml", function(xml) {
    if (navigator.appName === 'Microsoft Internet Explorer') {
        var importedNode = cloneToDoc(xml.documentElement);
    } else {
        var importedNode = document.importNode(xml.documentElement, true);
    }
    d3.select(".graphics_mobile").node().appendChild(importedNode);

    $(".go_to_expenditure").on("click", function() {
        $(window).scrollTo($('.st-expenditure'), 1000, {
            offset: 50
        });
    });
    $(".go_to_revenue").on("click", function() {
        $(window).scrollTo($('.st-revenue'), 1000, {
            offset: 50
        });
    });

});


function cloneToDoc(node, doc) {
    if (!doc) doc = document;
    var clone = doc.createElementNS(node.namespaceURI, node.nodeName);
    for (var i = 0, len = node.attributes.length; i < len; ++i) {
        var a = node.attributes[i];
        if (/^xmlns\b/.test(a.nodeName)) continue; // IE can't create these
        clone.setAttributeNS(a.namespaceURI, a.nodeName, a.nodeValue);
    }
    for (var i = 0, len = node.childNodes.length; i < len; ++i) {
        var c = node.childNodes[i];
        clone.insertBefore(
            c.nodeType == 1 ? cloneToDoc(c, doc) : doc.createTextNode(c.nodeValue),
            null
        )
    }
    return clone;
}
