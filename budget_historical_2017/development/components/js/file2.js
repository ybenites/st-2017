import * as d3 from "./d3.slider";


d3.selectAll(".go_to_st").on('click', function() {
  window.open("http://www.straitstimes.com/", "_blank");
});


var url_expenditure_svg1 = "svg/graphic1-2016.svg",
  url_expenditure_svg1_medium = "svg/graphic1_medium-2016.svg",
  url_expenditure_svg1_mobile = "svg/graphic1_mobile-2016.svg",
  url_expenditure_svg2 = "svg/graphic2.svg",
  url_expenditure_svg2_medium = "svg/graphic2_medium.svg",
  url_expenditure_svg2_mobile = "svg/graphic2_mobile.svg",
  a_total_years = [],
  year_origin = 1965;



for (var count = 1965; count <= 2016; count++) a_total_years.push(count);








var year_axis = d3.slider().axis(d3.svg.axis().orient("bottom").ticks(5).tickFormat(d3.format("0"))).min(d3.min(a_total_years)).max(d3.max(a_total_years)).on("slide", function(a, b) {
  switch (b) {
    case 1965:
      d3.selectAll("#s2016,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1965").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1965");
      d3.selectAll("#yearspeech").transition().text("13 Dec 1965");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 46).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1966:
      d3.selectAll("#s2016,#s1965,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1966").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#min1").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1966");
      d3.selectAll("#yearspeech").transition().text("5 Dec 1966");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 54).transition().duration(500).attr("fill", "#5a9ddc");
      break;

    case 1967:
      d3.selectAll("#s2016,#s1965,#s1966,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1967").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1967");
      d3.selectAll("#yearspeech").transition().text("5 Dec 1967");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 58).transition().duration(500).attr("fill", "#5a9ddc");


      break;

    case 1968:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1968").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1968");
      d3.selectAll("#yearspeech").transition().text("3 Dec 1968");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 61).transition().duration(500).attr("fill", "#5a9ddc");


      break;

    case 1969:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1969").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1969");
      d3.selectAll("#yearspeech").transition().text("No speech");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 0).transition().duration(500).attr("fill", "#5a9ddc");


      break;

    case 1970:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1970").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1970");
      d3.selectAll("#yearspeech").transition().text("9 March 1970");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 62).transition().duration(500).attr("fill", "#5a9ddc");


      break;

    case 1971:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1971").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#min2").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1971");
      d3.selectAll("#yearspeech").transition().text("8 March 1971");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 57).transition().duration(500).attr("fill", "#5a9ddc");


      break;

    case 1972:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1972").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1972");
      d3.selectAll("#yearspeech").transition().text("7 March 1972");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 60).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1973:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
      d3.selectAll("#s1973").attr("display", "inline").style("opacity", 1);
      d3.selectAll("#hand").attr("display", "none");
      d3.selectAll("#yeartop").transition().text("1973");
      d3.selectAll("#yearspeech").transition().text("26 Feb 1973");
      d3.selectAll("#gdp").transition().duration(500).attr("r", 54).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1974:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3),
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1974").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1974"),
        d3.selectAll("#yearspeech").transition().text("4 March 1974"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 41).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1975:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3),
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1975").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1975"),
        d3.selectAll("#yearspeech").transition().text("3 March 1975"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 35).transition().duration(500).attr("fill", "#5a9ddc")

      break;

    case 1976:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1976").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1976"),
        d3.selectAll("#yearspeech").transition().text("1 March 1976"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 44).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1977:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1977").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1977"),
        d3.selectAll("#yearspeech").transition().text("28 Feb 1977"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 45).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1978:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1978").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1978"),
        d3.selectAll("#yearspeech").transition().text("27 Feb 1978"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 48).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1979:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1979").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1979"),
        d3.selectAll("#yearspeech").transition().text("5 March 1979"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 50).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1980:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1980").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min3").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1980"),
        d3.selectAll("#yearspeech").transition().text("5 March 1980"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 52).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1981:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1981").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1981"),
        d3.selectAll("#yearspeech").transition().text("6 March 1981"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 54).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1982:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1982").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1982"),
        d3.selectAll("#yearspeech").transition().text("5 March 1982"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 44).transition().duration(500).attr("fill", "#5a9ddc");




      break;

    case 1983:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1983").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1983"),
        d3.selectAll("#yearspeech").transition().text("4 March 1983"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 48).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1984:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1984").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1984"),
        d3.selectAll("#yearspeech").transition().text("2 March 1984"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 49).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1985:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1985").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1985"),
        d3.selectAll("#yearspeech").transition().text("8 March 1985"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 14).transition().duration(500).attr("fill", "#ec4123");

      break;

    case 1986:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1986").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1986"),
        d3.selectAll("#yearspeech").transition().text("7 March 1986"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 19).transition().duration(500).attr("fill", "#5a9ddc");
        d
      break;

    case 1987:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1987").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1987"),
        d3.selectAll("#yearspeech").transition().text("4 March 1987"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 53).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1988:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1988").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1988"),
        d3.selectAll("#yearspeech").transition().text("4 March 1988"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 54).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1989:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1989").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1989"),
        d3.selectAll("#yearspeech").transition().text("3 March 1989"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 55).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1990:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1990").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1990"),
        d3.selectAll("#yearspeech").transition().text("2 March 1990"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 52).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1991:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1991").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min4").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1991"),
        d3.selectAll("#yearspeech").transition().text("1 March 1991"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 42).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1992:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1992").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1992"),
        d3.selectAll("#yearspeech").transition().text("28 Feb 1992"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 44).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1993:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1993").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1993"),
        d3.selectAll("#yearspeech").transition().text("26 Feb 1993"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 55).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1994:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1994").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1994"),
        d3.selectAll("#yearspeech").transition().text("23 Feb 1994"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 54).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1995:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1995").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min5").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1995"),
        d3.selectAll("#yearspeech").transition().text("1 Mar 1995"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 43).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1996:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
        d3.selectAll("#s1996").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1996"),
        d3.selectAll("#yearspeech").transition().text("28 Feb 1996"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 45).transition().duration(500).attr("fill", "#5a9ddc");

      break;

    case 1997:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s1997").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1997"),
        d3.selectAll("#yearspeech").transition().text("11 July 1997"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 47).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 64),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 81);

      break;

    case 1998:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3),
        d3.selectAll("#s1998").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min6").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1998"),
        d3.selectAll("#yearspeech").transition().text("27 Feb 1998"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 25).transition().duration(500).attr("fill", "#ec4123"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 74),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 77);

      break;

    case 1999:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s1999").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1999"),
        d3.selectAll("#yearspeech").transition().text("26 Feb 1999"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 41).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 69),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 85);

      break;

    case 2000:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2000").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min7").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2000"),
        d3.selectAll("#yearspeech").transition().text("25 Feb 2000"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 49).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 77),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 87);

      break;

    case 2001:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2001").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2001"),
        d3.selectAll("#yearspeech").transition().text("23 Feb 2001"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 17).transition().duration(500).attr("fill", "#ec4123"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 75),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 79);

      break;

    case 2002:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2002").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2002"),
        d3.selectAll("#yearspeech").transition().text("3 May 2002"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 34).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 75),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 70),
        d3.selectAll("#block7").style("opacity", 0.25);

      break;

    case 2003:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2003").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min8").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2003"),
        d3.selectAll("#yearspeech").transition().text("28 Feb 2003"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 34).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 79),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 70);

      break;

    case 2004:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2004").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2004"),
        d3.selectAll("#yearspeech").transition().text("27 Feb 2004"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 50).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 80),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 76);

      break;

    case 2005:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2005").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2005"),
        d3.selectAll("#yearspeech").transition().text("18 Feb 2005"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 45).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 79),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 78);

      break;

    case 2006:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2006").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min9").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2006"),
        d3.selectAll("#yearspeech").transition().text("17 Feb 2006"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 49).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 83), d3.selectAll("#expbar").transition().duration(500).attr("width", 87);

      break;

    case 2007:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2007").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2007"),
        d3.selectAll("#yearspeech").transition().text("15 Feb 2007"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 50).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 91),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 112);

      break;

    case 2008:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2008").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2008"),
        d3.selectAll("#yearspeech").transition().text("15 Feb 2008"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 22).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 105),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 114);

      break;

    case 2009:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2009").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2009"),
        d3.selectAll("#yearspeech").transition().text("22 Jan 2009"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 13).transition().duration(500).attr("fill", "#ec4123"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 116),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 110);

      break;

    case 2010:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min11").style("opacity", 0.3);
        d3.selectAll("#s2010").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min10").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2010"),
        d3.selectAll("#yearspeech").transition().text("22 Feb 2010"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 64).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 126),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 128);

      break;

    case 2011:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2011").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2011"),
        d3.selectAll("#yearspeech").transition().text("18 Feb 2011"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 41).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 129),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 142);

      break;

    case 2012:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2012").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2012"),
        d3.selectAll("#yearspeech").transition().text("17 Feb 2012"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 26).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 136),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 155);

      break;

    case 2013:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2013").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2013"),
        d3.selectAll("#yearspeech").transition().text("25 Feb 2013"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 33).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 145),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 158);

      break;

    case 2014:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2014").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2014"),
        d3.selectAll("#yearspeech").transition().text("21 Feb 2014"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 28).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 157),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 165);

      break;

    case 2015:
      d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10").style("opacity", 0.3);
        d3.selectAll("#s2015").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#min11").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2015"),
        d3.selectAll("#yearspeech").transition().text("23 Feb 2015"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 20).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 178),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 175);

      break;

    case 2016:
      d3.selectAll("#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
      d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
        d3.selectAll("#s2016").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
        d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2016"),
        d3.selectAll("#yearspeech").transition().text("24 Mar 2016"),
        d3.selectAll("#gdp").transition().duration(500).attr("r", 0).transition().duration(500).attr("fill", "#5a9ddc"),
        d3.selectAll("#revbar").transition().duration(500).attr("width", 188),
        d3.selectAll("#expbar").transition().duration(500).attr("width", 178);


  }
}).value(year_origin);

d3.selectAll("#slider1").call(year_axis);


d3.selectAll(window).on("resize", function(a, b) {
  d3.selectAll("#slider1").html(""),
    d3.selectAll("#slider1").call(year_axis)
});

d3.xml(url_expenditure_svg1, "image/svg+xml", function(a) {
  if ("Microsoft Internet Explorer" === navigator.appName) var b = cloneToDoc(a.documentElement);
  else var b = document.importNode(a.documentElement, !0);


  d3.selectAll(".graphic1").node().appendChild(b);

  d3.xml(url_expenditure_svg1_medium, "image/svg+xml", function(a) {
    if ("Microsoft Internet Explorer" === navigator.appName) var b = cloneToDoc(a.documentElement);
    else var b = document.importNode(a.documentElement, !0);
    d3.selectAll(".graphic1_medium").node().appendChild(b);

    d3.xml(url_expenditure_svg1_mobile, "image/svg+xml", function(a) {
      if ("Microsoft Internet Explorer" === navigator.appName) var b = cloneToDoc(a.documentElement);
      else var b = document.importNode(a.documentElement, !0);
      d3.selectAll(".graphic1_mobile").node().appendChild(b);
      top_button();
    });
  });





});


d3.xml(url_expenditure_svg2, "image/svg+xml", function(a) {
  if ("Microsoft Internet Explorer" === navigator.appName) var b = cloneToDoc(a.documentElement);
  else var b = document.importNode(a.documentElement, !0);
  d3.selectAll(".graphic2").node().appendChild(b),

    d3.selectAll("#screen1").attr("display", "inline").transition().duration(1e3).style("opacity", 1),
    d3.selectAll("#screen2").attr("display", "none").style("opacity", 0), d3.selectAll("#screen1b").on("mousedown", function() {
      d3.selectAll("#screen1").attr("display", "inline").transition().delay(1e3).duration(1e3).style("opacity", 1),
        d3.selectAll("#screen2").attr("display", "none").style("opacity", 0), d3.selectAll("#bar1").transition().delay(50).duration(1e3).attr("height", 91.5).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar2").transition().delay(100).duration(1e3).attr("height", 14.5).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar3").transition().delay(150).duration(1e3).attr("height", 83).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar4").transition().delay(200).duration(1e3).attr("height", 52.6).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar5").transition().delay(250).duration(1e3).attr("height", 17.7).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
        d3.selectAll("#bar6").transition().delay(300).duration(1e3).attr("height", 25).attr("transform", "translate(0 , 0)").attr("fill", "#ec4123"),
        d3.selectAll("#bar7").transition().delay(350).duration(1e3).attr("height", 47.5).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar8").transition().delay(400).duration(1e3).attr("height", 22).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar9").transition().delay(450).duration(1e3).attr("height", 7).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
        d3.selectAll("#bar10").transition().delay(500).duration(1e3).attr("height", 20.6).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
        d3.selectAll("#bar11").transition().delay(550).duration(1e3).attr("height", 110.2).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar12").transition().delay(600).duration(1e3).attr("height", 44.7).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar13").transition().delay(650).duration(1e3).attr("height", 35).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar14").transition().delay(700).duration(1e3).attr("height", 10.7).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar15").transition().delay(750).duration(1e3).attr("height", 67).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar16").transition().delay(800).duration(1e3).attr("height", 101.5).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar17").transition().delay(850).duration(1e3).attr("height", 71.6).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar18").transition().delay(900).duration(1e3).attr("height", 62).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc");
      d3.selectAll("#bar19").transition().delay(900).duration(1e3).attr("height", 60).attr("transform", "translate(0 , 0)");
      d3.selectAll("#bar20").transition().delay(900).duration(1e3).attr("height", 73).attr("transform", "translate(0 , 0)").attr("fill", "#ec4123");
    }), d3.selectAll("#screen2b").on("mousedown", function() {
      d3.selectAll("#screen2").attr("display", "inline").transition().delay(1e3).duration(1e3).style("opacity", 1),
        d3.selectAll("#screen1").attr("display", "none").style("opacity", 0), d3.selectAll("#bar1").transition().delay(50).duration(1e3).attr("height", 78).attr("transform", "translate(0 , 13)"),
        d3.selectAll("#bar2").transition().delay(100).duration(1e3).attr("height", 14).attr("transform", "translate(0 , 0.5)"),
        d3.selectAll("#bar3").transition().delay(150).duration(1e3).attr("height", 73).attr("transform", "translate(0 , 10.4)"),
        d3.selectAll("#bar4").transition().delay(200).duration(1e3).attr("height", 59).attr("transform", "translate(0 , -7)"),
        d3.selectAll("#bar5").transition().delay(250).duration(1e3).attr("height", 40).attr("transform", "translate(0 , 17.7)").attr("fill", "#ec4123"),
        d3.selectAll("#bar6").transition().delay(300).duration(1e3).attr("height", 3).attr("transform", "translate(0 , -3)").attr("fill", "#5a9ddc"),
        d3.selectAll("#bar7").transition().delay(350).duration(1e3).attr("height", 28).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar8").transition().delay(400).duration(1e3).attr("height", 1.5).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar9").transition().delay(450).duration(1e3).attr("height", 22).attr("transform", "translate(0 , -22)").attr("fill", "#ec4123"),
        d3.selectAll("#bar10").transition().delay(500).duration(1e3).attr("height", 1).attr("transform", "translate(0 , 20.6)").attr("fill", "#ec4123"),
        d3.selectAll("#bar11").transition().delay(550).duration(1e3).attr("height", 114).attr("transform", "translate(0 , -4)"),
        d3.selectAll("#bar12").transition().delay(600).duration(1e3).attr("height", 3.5).attr("transform", "translate(0 , 41)"),
        d3.selectAll("#bar13").transition().delay(650).duration(1e3).attr("height", 12).attr("transform", "translate(0 , 0)"),
        d3.selectAll("#bar14").transition().delay(700).duration(1e3).attr("height", 14.5).attr("transform", "translate(0 , -4)"),
        d3.selectAll("#bar15").transition().delay(750).duration(1e3).attr("height", 59).attr("transform", "translate(0 , 7.6)"),
        d3.selectAll("#bar16").transition().delay(800).duration(1e3).attr("height", 86).attr("transform", "translate(0 , 14.7)"),
        d3.selectAll("#bar17").transition().delay(850).duration(1e3).attr("height", 58).attr("transform", "translate(0 , 12.7)"),
        d3.selectAll("#bar18").transition().delay(900).duration(1e3).attr("height", 3).attr("transform", "translate(0 , 62)").attr("fill", "#ec4123");
      d3.selectAll("#bar19").transition().delay(900).duration(1e3).attr("height", 69).attr("transform", "translate(0 , 0)");
      d3.selectAll("#bar20").transition().delay(900).duration(1e3).attr("height", 45).attr("transform", "translate(0 , -45)").attr("fill", "#5a9ddc");

    });
});

d3.xml(url_expenditure_svg2_medium, "image/svg+xml", function(a) {
    if ("Microsoft Internet Explorer" === navigator.appName) var b = cloneToDoc(a.documentElement);
    else var b = document.importNode(a.documentElement, !0);
    d3.selectAll(".graphic2_medium").node().appendChild(b);
    d3.selectAll("#screen1").attr("display", "inline").transition().duration(1e3).style("opacity", 1),
      d3.selectAll("#screen2").attr("display", "none").style("opacity", 0), d3.selectAll("#screen1b").on("mousedown", function() {
        d3.selectAll("#screen1").attr("display", "inline").transition().delay(1e3).duration(1e3).style("opacity", 1),
          d3.selectAll("#screen2").attr("display", "none").style("opacity", 0), d3.selectAll("#bar1").transition().delay(50).duration(1e3).attr("height", 91.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar2").transition().delay(100).duration(1e3).attr("height", 14.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar3").transition().delay(150).duration(1e3).attr("height", 83).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar4").transition().delay(200).duration(1e3).attr("height", 52.6).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar5").transition().delay(250).duration(1e3).attr("height", 17.7).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar6").transition().delay(300).duration(1e3).attr("height", 25).attr("transform", "translate(0 , 0)").attr("fill", "#ec4123"),
          d3.selectAll("#bar7").transition().delay(350).duration(1e3).attr("height", 47.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar8").transition().delay(400).duration(1e3).attr("height", 22).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar9").transition().delay(450).duration(1e3).attr("height", 7).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar10").transition().delay(500).duration(1e3).attr("height", 20.6).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar11").transition().delay(550).duration(1e3).attr("height", 110.2).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar12").transition().delay(600).duration(1e3).attr("height", 44.7).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar13").transition().delay(650).duration(1e3).attr("height", 35).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar14").transition().delay(700).duration(1e3).attr("height", 10.7).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar15").transition().delay(750).duration(1e3).attr("height", 67).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar16").transition().delay(800).duration(1e3).attr("height", 101.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar17").transition().delay(850).duration(1e3).attr("height", 71.6).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar18").transition().delay(900).duration(1e3).attr("height", 62).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc");
        d3.selectAll("#bar19").transition().delay(900).duration(1e3).attr("height", 60).attr("transform", "translate(0 , 0)");
        d3.selectAll("#bar20").transition().delay(900).duration(1e3).attr("height", 73).attr("transform", "translate(0 , 0)").attr("fill", "#ec4123");
      }), d3.selectAll("#screen2b").on("mousedown", function() {
        d3.selectAll("#screen2").attr("display", "inline").transition().delay(1e3).duration(1e3).style("opacity", 1),
          d3.selectAll("#screen1").attr("display", "none").style("opacity", 0), d3.selectAll("#bar1").transition().delay(50).duration(1e3).attr("height", 78).attr("transform", "translate(0 , 13)"),
          d3.selectAll("#bar2").transition().delay(100).duration(1e3).attr("height", 14).attr("transform", "translate(0 , 0.5)"),
          d3.selectAll("#bar3").transition().delay(150).duration(1e3).attr("height", 73).attr("transform", "translate(0 , 10.4)"),
          d3.selectAll("#bar4").transition().delay(200).duration(1e3).attr("height", 59).attr("transform", "translate(0 , -7)"),
          d3.selectAll("#bar5").transition().delay(250).duration(1e3).attr("height", 40).attr("transform", "translate(0 , 17.7)").attr("fill", "#ec4123"),
          d3.selectAll("#bar6").transition().delay(300).duration(1e3).attr("height", 3).attr("transform", "translate(0 , -3)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar7").transition().delay(350).duration(1e3).attr("height", 28).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar8").transition().delay(400).duration(1e3).attr("height", 1.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar9").transition().delay(450).duration(1e3).attr("height", 22).attr("transform", "translate(0 , -22)").attr("fill", "#ec4123"),
          d3.selectAll("#bar10").transition().delay(500).duration(1e3).attr("height", 1).attr("transform", "translate(0 , 20.6)").attr("fill", "#ec4123"),
          d3.selectAll("#bar11").transition().delay(550).duration(1e3).attr("height", 114).attr("transform", "translate(0 , -4)"),
          d3.selectAll("#bar12").transition().delay(600).duration(1e3).attr("height", 3.5).attr("transform", "translate(0 , 41)"),
          d3.selectAll("#bar13").transition().delay(650).duration(1e3).attr("height", 12).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar14").transition().delay(700).duration(1e3).attr("height", 14.5).attr("transform", "translate(0 , -4)"),
          d3.selectAll("#bar15").transition().delay(750).duration(1e3).attr("height", 59).attr("transform", "translate(0 , 7.6)"),
          d3.selectAll("#bar16").transition().delay(800).duration(1e3).attr("height", 86).attr("transform", "translate(0 , 14.7)"),
          d3.selectAll("#bar17").transition().delay(850).duration(1e3).attr("height", 58).attr("transform", "translate(0 , 12.7)"),
          d3.selectAll("#bar18").transition().delay(900).duration(1e3).attr("height", 3).attr("transform", "translate(0 , 62)").attr("fill", "#ec4123");
        d3.selectAll("#bar19").transition().delay(900).duration(1e3).attr("height", 69).attr("transform", "translate(0 , 0)");
        d3.selectAll("#bar20").transition().delay(900).duration(1e3).attr("height", 45).attr("transform", "translate(0 , -45)").attr("fill", "#5a9ddc");

      });
  }),

  d3.xml(url_expenditure_svg2_mobile, "image/svg+xml", function(a) {
    if ("Microsoft Internet Explorer" === navigator.appName) var b = cloneToDoc(a.documentElement);
    else var b = document.importNode(a.documentElement, !0);
    d3.selectAll(".graphic2_mobile").node().appendChild(b);
    d3.selectAll("#screen1").attr("display", "inline").transition().duration(1e3).style("opacity", 1),
      d3.selectAll("#screen2").attr("display", "none").style("opacity", 0), d3.selectAll("#screen1b").on("mousedown", function() {
        d3.selectAll("#screen1").attr("display", "inline").transition().delay(1e3).duration(1e3).style("opacity", 1),
          d3.selectAll("#screen2").attr("display", "none").style("opacity", 0), d3.selectAll("#bar1").transition().delay(50).duration(1e3).attr("height", 91.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar2").transition().delay(100).duration(1e3).attr("height", 14.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar3").transition().delay(150).duration(1e3).attr("height", 83).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar4").transition().delay(200).duration(1e3).attr("height", 52.6).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar5").transition().delay(250).duration(1e3).attr("height", 17.7).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar6").transition().delay(300).duration(1e3).attr("height", 25).attr("transform", "translate(0 , 0)").attr("fill", "#ec4123"),
          d3.selectAll("#bar7").transition().delay(350).duration(1e3).attr("height", 47.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar8").transition().delay(400).duration(1e3).attr("height", 22).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar9").transition().delay(450).duration(1e3).attr("height", 7).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar10").transition().delay(500).duration(1e3).attr("height", 20.6).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar11").transition().delay(550).duration(1e3).attr("height", 110.2).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar12").transition().delay(600).duration(1e3).attr("height", 44.7).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar13").transition().delay(650).duration(1e3).attr("height", 35).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar14").transition().delay(700).duration(1e3).attr("height", 10.7).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar15").transition().delay(750).duration(1e3).attr("height", 67).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar16").transition().delay(800).duration(1e3).attr("height", 101.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar17").transition().delay(850).duration(1e3).attr("height", 71.6).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar18").transition().delay(900).duration(1e3).attr("height", 62).attr("transform", "translate(0 , 0)").attr("fill", "#5a9ddc");
        d3.selectAll("#bar19").transition().delay(900).duration(1e3).attr("height", 60).attr("transform", "translate(0 , 0)");
        d3.selectAll("#bar20").transition().delay(900).duration(1e3).attr("height", 73).attr("transform", "translate(0 , 0)").attr("fill", "#ec4123");
      }), d3.selectAll("#screen2b").on("mousedown", function() {
        d3.selectAll("#screen2").attr("display", "inline").transition().delay(1e3).duration(1e3).style("opacity", 1),
          d3.selectAll("#screen1").attr("display", "none").style("opacity", 0), d3.selectAll("#bar1").transition().delay(50).duration(1e3).attr("height", 78).attr("transform", "translate(0 , 13)"),
          d3.selectAll("#bar2").transition().delay(100).duration(1e3).attr("height", 14).attr("transform", "translate(0 , 0.5)"),
          d3.selectAll("#bar3").transition().delay(150).duration(1e3).attr("height", 73).attr("transform", "translate(0 , 10.4)"),
          d3.selectAll("#bar4").transition().delay(200).duration(1e3).attr("height", 59).attr("transform", "translate(0 , -7)"),
          d3.selectAll("#bar5").transition().delay(250).duration(1e3).attr("height", 40).attr("transform", "translate(0 , 17.7)").attr("fill", "#ec4123"),
          d3.selectAll("#bar6").transition().delay(300).duration(1e3).attr("height", 3).attr("transform", "translate(0 , -3)").attr("fill", "#5a9ddc"),
          d3.selectAll("#bar7").transition().delay(350).duration(1e3).attr("height", 28).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar8").transition().delay(400).duration(1e3).attr("height", 1.5).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar9").transition().delay(450).duration(1e3).attr("height", 22).attr("transform", "translate(0 , -22)").attr("fill", "#ec4123"),
          d3.selectAll("#bar10").transition().delay(500).duration(1e3).attr("height", 1).attr("transform", "translate(0 , 20.6)").attr("fill", "#ec4123"),
          d3.selectAll("#bar11").transition().delay(550).duration(1e3).attr("height", 114).attr("transform", "translate(0 , -4)"),
          d3.selectAll("#bar12").transition().delay(600).duration(1e3).attr("height", 3.5).attr("transform", "translate(0 , 41)"),
          d3.selectAll("#bar13").transition().delay(650).duration(1e3).attr("height", 12).attr("transform", "translate(0 , 0)"),
          d3.selectAll("#bar14").transition().delay(700).duration(1e3).attr("height", 14.5).attr("transform", "translate(0 , -4)"),
          d3.selectAll("#bar15").transition().delay(750).duration(1e3).attr("height", 59).attr("transform", "translate(0 , 7.6)"),
          d3.selectAll("#bar16").transition().delay(800).duration(1e3).attr("height", 86).attr("transform", "translate(0 , 14.7)"),
          d3.selectAll("#bar17").transition().delay(850).duration(1e3).attr("height", 58).attr("transform", "translate(0 , 12.7)"),
          d3.selectAll("#bar18").transition().delay(900).duration(1e3).attr("height", 3).attr("transform", "translate(0 , 62)").attr("fill", "#ec4123");
        d3.selectAll("#bar19").transition().delay(900).duration(1e3).attr("height", 69).attr("transform", "translate(0 , 0)");
        d3.selectAll("#bar20").transition().delay(900).duration(1e3).attr("height", 45).attr("transform", "translate(0 , -45)").attr("fill", "#5a9ddc");

      });
  });








function top_button() {
  d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);

  d3.selectAll("#hand").attr("transform", "translate(0,0)").attr("display", "inline").transition().each(animatehand);

  function animatehand() {
    d3.selectAll("#hand").transition().duration(4000).attr("transform", "translate(760,0)").each("end", animatehand2);
  };

  function animatehand2() {
    d3.selectAll("#hand").transition().duration(4000).attr("transform", "translate(0,0)").each("end", animatehand);
  };

  d3.selectAll("#s2016,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
  d3.selectAll("#s1965").attr("display", "inline").style("opacity", 1)




  d3.selectAll("#min1").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
    d3.selectAll("#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
    d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
    d3.selectAll("#s1966").attr("display", "inline").style("opacity", 1);
    d3.selectAll("#min1").attr("display", "inline").style("opacity", 1);
    d3.selectAll("#hand").attr("display", "none");
    d3.selectAll("#yeartop").transition().text("1966");
    d3.selectAll("#yearspeech").transition().text("5 Dec 1966");
    d3.selectAll("#gdp").transition().duration(500).attr("r", 54).transition().duration(500).attr("fill", "#5a9ddc");
    d3.selectAll("a.d3-slider-handle").style("left", "1.96%");
    d3.selectAll("#hand").attr("display", "none");




  })
  d3.selectAll("#min2").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0);
    d3.selectAll("#min1,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
    d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0);
    d3.selectAll("#s1971").attr("display", "inline").style("opacity", 1);
    d3.selectAll("#min2").attr("display", "inline").style("opacity", 1);
    d3.selectAll("#hand").attr("display", "none");
    d3.selectAll("#yeartop").transition().text("1971");
    d3.selectAll("#yearspeech").transition().text("8 March 1971");
    d3.selectAll("#gdp").transition().duration(500).attr("r", 57).transition().duration(500).attr("fill", "#5a9ddc");
    d3.selectAll("a.d3-slider-handle").style("left", "11.76%");


  })


  d3.selectAll("#min3").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min4,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
      d3.selectAll("#s1980").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min3").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1980"),
      d3.selectAll("#yearspeech").transition().text("5 March 1980"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 52).transition().duration(500).attr("fill", "#5a9ddc");
    d3.selectAll("a.d3-slider-handle").style("left", "29.41%");





  })
  d3.selectAll("#min4").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min5,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
      d3.selectAll("#s1991").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min4").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1991"),
      d3.selectAll("#yearspeech").transition().text("1 March 1991"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 42).transition().duration(500).attr("fill", "#5a9ddc");
    d3.selectAll("a.d3-slider-handle").style("left", "50.98%");




  })

  d3.selectAll("#min5").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min6,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#spenrev").attr("display", "none").style("opacity", 0),
      d3.selectAll("#s1995").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min5").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1995"),
      d3.selectAll("#yearspeech").transition().text("1 Mar 1995"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 43).transition().duration(500).attr("fill", "#5a9ddc");
    d3.selectAll("a.d3-slider-handle").style("left", "58.82%");




  })


  d3.selectAll("#min6").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min7,#min8,#min9,#min10,#min11").style("opacity", 0.3),
      d3.selectAll("#s1998").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min6").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("1998"),
      d3.selectAll("#yearspeech").transition().text("27 Feb 1998"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 25).transition().duration(500).attr("fill", "#ec4123"),
      d3.selectAll("#revbar").transition().duration(500).attr("width", 74),
      d3.selectAll("#expbar").transition().duration(500).attr("width", 77);
    d3.selectAll("a.d3-slider-handle").style("left", "64.71%");



  })


  d3.selectAll("#min7").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min8,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#s2000").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min7").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2000"),
      d3.selectAll("#yearspeech").transition().text("25 Feb 2000"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 49).transition().duration(500).attr("fill", "#5a9ddc"),
      d3.selectAll("#revbar").transition().duration(500).attr("width", 77),
      d3.selectAll("#expbar").transition().duration(500).attr("width", 87);
    d3.selectAll("a.d3-slider-handle").style("left", "68.63%");




  })

  d3.selectAll("#min8").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min9,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#s2003").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min8").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2003"),
      d3.selectAll("#yearspeech").transition().text("28 Feb 2003"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 34).transition().duration(500).attr("fill", "#5a9ddc"),
      d3.selectAll("#revbar").transition().duration(500).attr("width", 79),
      d3.selectAll("#expbar").transition().duration(500).attr("width", 70);
    d3.selectAll("a.d3-slider-handle").style("left", " 74.51%");



  });

  d3.selectAll("#min9").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min10,#min11").style("opacity", 0.3);
      d3.selectAll("#s2006").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min9").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2006"),
      d3.selectAll("#yearspeech").transition().text("17 Feb 2006"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 49).transition().duration(500).attr("fill", "#5a9ddc"),
      d3.selectAll("#revbar").transition().duration(500).attr("width", 83), d3.selectAll("#expbar").transition().duration(500).attr("width", 87);
    d3.selectAll("a.d3-slider-handle").style("left", "80.39%");



  });

  d3.selectAll("#min10").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2011,#s2012,#s2013,#s2014,#s2015").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min11").style("opacity", 0.3);
      d3.selectAll("#s2010").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min10").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2010"),
      d3.selectAll("#yearspeech").transition().text("22 Feb 2010"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 64).transition().duration(500).attr("fill", "#5a9ddc"),
      d3.selectAll("#revbar").transition().duration(500).attr("width", 126),
      d3.selectAll("#expbar").transition().duration(500).attr("width", 128);
    d3.selectAll("a.d3-slider-handle").style("left", "88.24%");



  });

  d3.selectAll("#min11").on('click', function() {
    d3.selectAll("#s2016,#s1965,#s1966,#s1967,#s1968,#s1969,#s1970,#s1971,#s1972,#s1973,#s1974,#s1975,#s1976,#s1977,#s1978,#s1979,#s1980,#s1981,#s1982,#s1983,#s1984,#s1985,#s1986,#s1987,#s1988,#s1989,#s1990,#s1991,#s1992,#s1993,#s1994,#s1995,#s1996,#s1997,#s1998,#s1999,#s2000,#s2001,#s2002,#s2003,#s2004,#s2005,#s2006,#s2007,#s2008,#s2009,#s2010,#s2011,#s2012,#s2013,#s2014").attr("display", "none").style("opacity", 0),
    d3.selectAll("#min1,#min2,#min3,#min4,#min5,#min6,#min7,#min8,#min9,#min10").style("opacity", 0.3);
      d3.selectAll("#s2015").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#min11").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#spenrev").attr("display", "inline").style("opacity", 1),
      d3.selectAll("#hand").attr("display", "none"), d3.selectAll("#yeartop").transition().text("2015"),
      d3.selectAll("#yearspeech").transition().text("23 Feb 2015"),
      d3.selectAll("#gdp").transition().duration(500).attr("r", 20).transition().duration(500).attr("fill", "#5a9ddc"),
      d3.selectAll("#revbar").transition().duration(500).attr("width", 178),
      d3.selectAll("#expbar").transition().duration(500).attr("width", 175);
    d3.selectAll("a.d3-slider-handle").style("left", "98.04%");



  });
}
