var d3=require("d3");

var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
    FB.init({
        appId: fbAppId, // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the
        // server to access the session
        xfbml: true, // parse page for xfbml or html5
        // social plugins like login button below
        version: 'v2.0', // Specify an API version
    });

    // Put additional init code here
};


// Load the SDK Asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



if ($(".st-social-desktop").length > 0) {
    $(".st-social-desktop img:nth(0),.st-social-mobile img:nth(0)").on("click", function(e) {
        // code share facebook
        e.preventDefault();
        var image = 'http://graphics.straitstimes.com/STI/STIMEDIA/facebook_images/budget-2017-transformations/budget-2017-transformations.png';
        var name = "What the Budget tells us about Singapore's economic transformations";
        var description = "Over the years, Singapore’s Budgets have reflected the phase of economic development at home and external trends shaping the global economy. Here are some key policies that were rolled out over the decades, marking major turning points for the Singapore economy.";


        share_face_book(image, name, description);
        return false;
    });
    $(".st-social-desktop img:nth(1),.st-social-mobile img:nth(1)").on("click", function(e) {
        // code share twitter
        e.preventDefault();
        var text = "An interactive lookback at Singapore’s economic transformations over the years %23tbt ";
        var via = 'STcom';
        var url = 'http://str.sg/Budget17historical';

        share_twitter(text, via, url);
        return false;
    });
}

function share_face_book(image, name, description) {
    FB.ui({
        method: 'feed',
        link: window.location.href,
        caption: 'www.straitstimes.com',
        picture: image,
        name: name,
        description: description
    });
}

function share_twitter(text, via, url) {
    window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
}

$(document).ready(function() {
    var height_footer = $("footer.st-content-footer").outerHeight();
    $("body").css("margin", "0 0 " + height_footer + "px");

    $(".st-button-menu-mobile").on('click', function() {
        $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
        $(".st_content_shared_social").toggleClass('st_dialogIsOpen');

        eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
    });
});


$(window).on('resize', function() {
    var height_footer = $("footer.st-content-footer").outerHeight();
    $("body").css("margin", "0 0 " + height_footer + "px");
});

function eventCloseMenu(event) {
    if (event) {
        $(".st_menu_mobile").css('right', '10px');

        d3.select(".first_line").transition().duration(500).attr("x1", 12.8).attr("y1", 12.2).attr("x2", 23.5).attr("y2", 22.8);
        d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 0).attr("x2", 0);
        d3.select(".second_line").transition().duration(500).attr("x1", 12.9).attr("y1", 22.9).attr("x2", 23.4).attr("y2", 12.1);
        $('.st_content_menu_fixed').hide().slideDown('500').addClass('fixed_menu_mobile');
        $('body').css({
            'overflow': 'hidden',
            'position': 'relative'
        });
    } else {
        $(".st_menu_mobile").css('right', '0');
        d3.select(".first_line").transition().duration(500).attr("x1", 10.5).attr("y1", 13.2).attr("x2", 26.1).attr("y2", 13.2);
        d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 1).attr("x2", 26.1);
        d3.select(".second_line").transition().duration(500).attr("x1", 10.5).attr("y1", 21.9).attr("x2", 26.1).attr("y2", 21.9);
        $('.st_content_menu_fixed').slideUp('500', function() {
            $(this).show().removeClass('fixed_menu_mobile');
        });
        $('body').css('overflow', 'auto');
    }
}
