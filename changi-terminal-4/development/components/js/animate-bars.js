import $ from "jquery";
import * as d3 from "d3";
import lazy_load from './libraries/lazy-load';

import './libraries/modernizr-custom';

import "./libraries/sound";

import "./libraries/jquery.event.move";
import "./libraries/jquery.twentytwenty";
import "./libraries/iphone-inline-video";
import fix_svg from "./libraries/fix-svg-size";

$(function() {

  $("#container-before-after").twentytwenty();
    var instance;
    createjs.Sound.on("fileload", handleLoadComplete);
    createjs.Sound.alternateExtensions = ["mp3"];
    createjs.Sound.registerSound({src: "audios/waves16s.mp3", id: "sound"});
    function handleLoadComplete(event) {
        instance = createjs.Sound.play("sound", {loop: -1});
        instance.volume = 0.5;
        // $(".btn-with-sound").trigger("click");
    }

    var var_interval_sound;
    $(".content-buttons-sound").on("click", ".btn-with-sound", function() {
        if (instance !== undefined) {
            $(".btn-with-sound").animateCss_500("fadeOut", "out");
            d3.timeout(function(){
              $(".btn-without-sound").animateCss_500("fadeIn", "in");
              createjs.Sound.muted = true;
            },400);
            if(var_interval_sound!=undefined)clearInterval(var_interval_sound);

            var count=0;
            var_interval_sound=setInterval(function(){
              count++;
              instance.volume=(instance.volume*(87.5/100));
              if(count===8){
                clearInterval(var_interval_sound);
                instance.volume = 0.01;
              }
            },50);
        }
    });

    $(".content-buttons-sound").on("click", ".btn-without-sound", function() {
        if (instance !== undefined) {
            $(".btn-without-sound").animateCss_500("fadeOut", "out");
            d3.timeout(function(){
              $(".btn-with-sound").animateCss_500("fadeIn", "in");
              createjs.Sound.muted = false;
            },400);

            if(var_interval_sound!=undefined)clearInterval(var_interval_sound);
            var count=0;
            var_interval_sound=setInterval(function(){
              count++;
              instance.volume=(instance.volume*(112.5/100));
              if(count===8){
                clearInterval(var_interval_sound);
                instance.volume=0.5;
              }
            },50);
        }
    });


    var video_hero_1=$(".st-header-hero-video video").get(0);
    var video_hero_2=$(".st-header-hero-video video").get(1);
    enableInlineVideo(video_hero_1);
    enableInlineVideo(video_hero_2);

    video_hero_1.addEventListener("ended", function() {
      // $(video_hero_1).animateCss_500("fadeOut","out");
      $(video_hero_1).hide();
      $(video_hero_2).show();
      video_hero_2.play()
      // $(video_hero_2).animateCss_500("fadeIn","in");
      $(".st-title-general").animateCss_500("fadeIn", "in");
      var setx=setTimeout(function(){
        fix_svg({
          tag:".logo-suw"
        });
        clearTimeout(setx);
      },300);
    }, true);




});
