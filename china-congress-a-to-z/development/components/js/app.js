// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";

import * as d3 from "d3";

var Masonry = require('masonry-layout');
require('jquery-bridget');
$.bridget('masonry', Masonry);

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];
//
// a_test.forEach(d => {
//   console.log(d);
// });

let info = {};
let st_url_csv = "csv/a-to-z.csv";
let template_card = "<div class='item-card item-card-unit' data-name_deal=\"[ST-DATA-NAME-DEAL]\">";
template_card += "<h1>[ST-ALPHABET]</h1><span>[ST-IS-FOR]</span>";
template_card += "</div></div>";

info.t_content_item_card = "<div>";
info.t_content_item_card += "<div class='row-content'>";

info.t_content_item_card += "<div class='row-left'>";
info.t_content_item_card += "<img class='st-image-responsive' src='images/a-to-z/img/[ST-SUBTITLE-IMAGE].jpg' alt=''/>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-right'>";
info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-title'><strong>[ST-SUBTITLE]</strong></div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-mall'>[ST-CONTENT]</div>";
info.t_content_item_card += "</div>";
info.t_content_item_card += "</div>";
info.t_content_item_card += "</div>";

/*button close*/
info.t_content_item_card += "<div class='btn-close-content-card'><img src='images/close.svg' class='st-img-responsive' /></div>";
/*end button close*/
info.t_content_item_card += "</div>";


/*configuration for event click card*/
info.number_columns = 0;
info.last_click_card = 0;
info.count_clicks_card = 0;
info.cache_order = [];

d3.csv(st_url_csv, function(error, csv_data) {
  if (!error) {
    main(csv_data);
  }
});

$("#content-cards").on("click", '.btn-close-content-card', function() {
  info.f_clear_content_item_card_with_animation();
});

$(window).on('resize', function() {
  if (info.detect_device() === false) {
    info.last_click_card = 0;
    if ($("#content-cards .content-item-card").length > 0) {
      info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    }

    $(".item-card-unit").removeClass("item-active");
    info.number_columns = parseInt(($("#content-cards").outerWidth() + 10) / ($(".item-card").outerWidth() + 10));

    var min_height_card = $(".item-card").outerWidth();
    $(".item-card").css('min-height', min_height_card);

  }
});
window.addEventListener("orientationchange", function() {
  if (info.detect_device() === true) {
    info.last_click_card = 0;
    if ($("#content-cards .content-item-card").length > 0) {
      info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    }
    $(".item-card-unit").removeClass("item-active");
    info.number_columns = parseInt(($("#content-cards").outerWidth() + 10) / ($(".item-card").outerWidth() + 10));

    var min_height_card = $(".item-card").outerWidth();
    $(".item-card").css('min-height', min_height_card);
  }
}, false);

function main(csv_data) {

  info.mansonry = $("#content-cards").masonry({
    columnWidth: '.item-card',
    itemSelector: '.item-card',
    isFitWidth: false,
    gutter: 15,
    transitionDuration: "0.5s"
  });

  $(".select-category").html("<option value=''></option>");
  $(".select-region").html("<option value=''></option>");
  $(".select-type").html("<option value=''></option>");

  info.data_sort = csv_data;

  info.data_sort.forEach(function(d, order) {
    // var d_category = d.category.trim();
    // if ($.inArray(d_category, a_category) === -1) {
    //   $(".select-category").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_category), 'g').replace('[ST-NAME-DISPLAY]', d_category, 'g'));
    //   a_category.push(d_category);
    // }

    // var d_region = d.region.trim();
    // if ($.inArray(d_region, a_region) === -1) {
    //   $(".select-region").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_region), 'g').replace('[ST-NAME-DISPLAY]', d_region, 'g'));
    //   a_region.push(d_region);
    // }
    // var d_type = d.type.trim();
    // if ($.inArray(d_type, a_type) === -1) {
    //   $(".select-type").append(template_option.replace('[ST-NAME-VALUE]', d_type, 'g').replace('[ST-NAME-DISPLAY]', d_type, 'g'));
    //   a_type.push(d_type);
    // }
    var new_card = template_card
      // .replace('[ST-ORDER]', (order + 1), 'g')
      // .replace("[ST-CLASS-COLOR]", colours[d.area.trim()], "g")
      // .replace("[ST-CATEGORY]", info.change_space_underscore(d_category), 'g')
      // .replace("[ST-PRICE]", d.discounted_price.trim(), 'g')
      // .replace("[ST-RPRICE]", d.regular_retail_price.trim(), 'g')
      // .replace("[ST-DISC]", d.discount_per_cent.trim(), 'g')
      // .replace("[ST-REGION]", info.change_space_underscore(d_region), 'g')
      // .replace("[ST-TYPE]", d_type, 'g')
      // .replace("[ST-CLASS-CARD-BACK]", "item-card-back-" + (d.img.trim().match(/\d+/)[0]), 'g')
      .replace("[ST-DATA-NAME-DEAL]", d.subtitle.trim(), "g")
      // .replace("[ST-THUMBNAIL]", d.thumbnail.trim(), "g")
      // .replace("[ST-NAME]", d.item.trim(), "g")
      .replace("[ST-ALPHABET]", d.alphabet.trim(), "g")
      .replace("[ST-IS-FOR]", d.is_for.trim(), "g");

    $(".save-copy-card-1").append(new_card);
    $("#content-cards").append(new_card);
    info.cache_order.push(order + 1);
  });


  info.mansonry.masonry('reloadItems');
  info.mansonry.masonry("layout");

  info.mansonry.on("layoutComplete", function(event, item) {
    info.number_columns = parseInt(($("#content-cards").outerWidth() + 15) / ($(".item-card").outerWidth() + 10));
    if (info.flag_order_cards === 0) {
      info.flag_order_cards = 1;
      var set_time = setTimeout(function() {
        clearTimeout(set_time);
        info.mansonry.masonry("layout");
      }, 500);
    }

  });

  info.mansonry.on("removeComplete", function(event, item) {
    if (info.time_delete_contentcard === 0) {
      info.time_delete_contentcard = 500;
      info.mansonry.masonry('reloadItems');
      info.mansonry.masonry("layout");
    } else {
      var set_time = setTimeout(function() {
        clearTimeout(set_time);
        info.mansonry.masonry('reloadItems');
        info.mansonry.masonry("layout");
      }, 500);
    }
  });

  $("#content-cards").on("click", ".item-card-unit", function() {
    info.count_clicks_card++;

    $(".item-card-unit").find("h1").removeClass("fontThirty");
    $(".item-card-unit").find("span").removeClass("fontCuratorBold");
    $(this).find("h1").addClass("fontThirty");
    $(this).find("span").addClass("fontCuratorBold");

    var position_current_element = $("#content-cards .item-card-unit").index(this) + 1;
    var row_appear = Math.ceil(position_current_element / info.number_columns) * info.number_columns;
    var style_content_item_card = "style='position:absolute; top:" + parseInt($(this).css('top')) + "px;'";

    if (info.last_click_card !== row_appear) {
      if (row_appear > info.last_click_card) {
        var offset_top2 = $(window).scrollTop() - $(".content-item-card").outerHeight();
        $("body,html").scrollTop(offset_top2);
      }
      info.f_clear_content_item_card();

      var $div_item = $("#content-cards .item-card-unit").eq(row_appear - 1);
      var content_card = "<div class='item-card content-item-card' " + style_content_item_card + "></div>";
      if ($div_item.length > 0) {
        $(content_card).insertAfter($div_item);
      } else {
        $("#content-cards").append(content_card);
      }
    }

    var offset_top = $(this).offset().top;
    $("body,html").animate({
      scrollTop: offset_top - $(".padding-offset").outerHeight()
    }, 500);

    var st_name_deal = $(this).data('name_deal');
    info.data_sort.forEach(function(d) {
      if (d.subtitle.trim() === st_name_deal) {
        info.f_add_content_item_card(d);
      }
    });

    // console.log(position_current_element); //item #
    // console.log(row_appear); //item row #
    // console.log(style_content_item_card); //style
    // console.log(offset_top); //distance of card clicked from top
    // console.log(st_name_deal); //name of deal

    info.last_click_card = row_appear;

    $(".item-card-unit").removeClass("item-active");
    $(this).addClass("item-active");

  });

}

info.detect_device = function() {
  return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
};
// filling in info of item

info.f_add_content_item_card = function(obj_content_card) {
  // console.log(obj_content_card);
  var number_image = (parseInt(obj_content_card.img) < 10) ? (0 + obj_content_card.img) : obj_content_card.img;
  var content_item_card_html = info.t_content_item_card
    .replace("[ST-SUBTITLE]", obj_content_card.subtitle.trim(), "g")
    .replace("[ST-SUBTITLE-IMAGE]", obj_content_card.img.trim(), 'g')
    .replace("[ST-CONTENT]", obj_content_card.content.trim(), 'g');
  $(".content-item-card").html(content_item_card_html);
  $(".content-item-card").append("<div class='empty-space'></div>");
  $(".content-item-card").append("<div class='empty-space2'></div>");

  info.mansonry.masonry('reloadItems');
  info.mansonry.masonry("layout");
};

info.f_clear_content_item_card = function() {
  info.time_delete_contentcard = 0;
  if ($("#content-cards .content-item-card").length > 0) {
    info.last_click_card = 0;
    info.mansonry.masonry({
      transitionDuration: "0s"
    });
    info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    info.mansonry.masonry({
      transitionDuration: "0.5s"
    });
  }
  $(".item-card-unit").removeClass("item-active");
};

info.f_clear_content_item_card_with_animation = function() {
  if ($("#content-cards .content-item-card").length > 0) {
    info.last_click_card = 0;
    info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
  }
  $(".item-card-unit").removeClass("item-active");
  $(".item-card-unit").find("h1").removeClass("fontThirty");
  $(".item-card-unit").find("span").removeClass("fontCuratorBold");
};

info.f_clear_select2_icon = function(this_select) {
  var val_select = $(this_select).val();
  var parent_select = $(this_select).parent();
  if (val_select) {
    parent_select.find(".select2-selection").addClass("background_select");
    parent_select.find(".select2-selection__arrow").addClass("disappear_arrow");
  } else {
    parent_select.find(".select2-selection").removeClass("background_select");
    parent_select.find(".select2-selection__arrow").removeClass("disappear_arrow");
  }
};
