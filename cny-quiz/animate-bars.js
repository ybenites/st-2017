import $ from "jquery";

import * as d3 from "d3";

$(function() {

    //out here is D3 territory good luck
    //datas for the percentage of sugar daily
    var defdataset = [100, 100];
    var dataset = [67, 88];
    var dataset2 = [63, 84];
    var dataset3 = [48, 64];
    var dataset4 = [85, 100];
    var dataset5 = [60, 80];
    var labelset = [
        "././images/noun_164868_cc.png",
        "././images/noun_164871_cc.png"
    ];

    var setlabel = [
        "Men",
        "Women"
    ];
    //Width and height
    var w = 370;
    var h = 25;
    var onx = 70;
    var allbar = 100;
    var extlength = 4.2;

    //Create SVG element
    // var svg = d3.select(".ques_1_explanation")
    //   .insert("svg", ".replaybutton")
    //   .attr("class", "chartbar")
    //   .attr("viewBox", "0 0 650 110")
    //   .attr("preserveAspectRatio", "xMaxYMax meet")
    //   .classed("svg-content", true)
    //   .style("margin-top", "40px");

    var svg_c = d3.select(".ques_1_explanation div:nth-child(2)")
        .insert("div", ".replaybutton").classed("svg-content", true)
        .append("svg")
        .attr("class", "chartbar");
    svg_c.append("g").classed('g-barsall', true);
    svg_c.append("g").classed('g-bars', true);
    svg_c.append("g").classed("g-datashow", true);
    svg_c.append("g").classed("g-label", true);
    // .attr("viewBox", "0 0 650 110")
    // .attr("preserveAspectRatio", "xMaxYMax meet")
    // .style("margin-top", "40px");


    // var svg2 = d3.select(".ques_2_explanation div:nth-child(2)")
    //   .insert("svg", ".replaybutton")
    //   .attr("class", "chartbar")
    //   .attr("preserveAspectRatio", "xMaxYMax slice")
    //   .attr("viewBox", "0 0 650 110")
    //   .style("margin-top", "40px");

    var svg2 = d3.select(".ques_2_explanation div:nth-child(2)")
        .insert("div", ".replaybutton").classed("svg-content", true)
        .append("svg")
        .attr("class", "chartbar");
    svg2.append("g").classed('g-barsall', true);
    svg2.append("g").classed('g-bars', true);
    svg2.append("g").classed("g-datashow", true);
    svg2.append("g").classed("g-label", true);

    // var svg3 = d3.select(".ques_3_explanation div:nth-child(2)")
    //   .insert("svg", ".replaybutton")
    //   .attr("class", "chartbar")
    //   .attr("preserveAspectRatio", "xMaxYMax slice")
    //   .attr("viewBox", "0 0 650 110")
    //   .classed("svg-content", true)
    //   .style("margin-top", "40px");

    var svg3 = d3.select(".ques_3_explanation div:nth-child(2)")
        .insert("div", ".replaybutton").classed("svg-content", true)
        .append("svg")
        .attr("class", "chartbar");
    svg3.append("g").classed('g-barsall', true);
    svg3.append("g").classed('g-bars', true);
    svg3.append("g").classed("g-datashow", true);
    svg3.append("g").classed("g-label", true);

    // var svg4 = d3.select(".ques_4_explanation div:nth-child(2)")
    //   .insert("svg", ".replaybutton")
    //   .attr("class", "chartbar")
    //   .attr("preserveAspectRatio", "xMaxYMax slice")
    //   .attr("viewBox", "0 0 650 110")
    //   .classed("svg-content", true)
    //   .style("margin-top", "40px");

    var svg4 = d3.select(".ques_4_explanation div:nth-child(2)")
        .insert("div", ".replaybutton").classed("svg-content", true)
        .append("svg")
        .attr("class", "chartbar");
    svg4.append("g").classed('g-barsall', true);
    svg4.append("g").classed('g-bars', true);
    svg4.append("g").classed("g-datashow", true);
    svg4.append("g").classed("g-label", true);

    // var svg5 = d3.select(".ques_5_explanation div:nth-child(2)")
    //   .insert("svg", ".replaybutton")
    //   .attr("class", "chartbar")
    //   .attr("preserveAspectRatio", "xMaxYMax slice")
    //   .attr("viewBox", "0 0 650 110")
    //   .classed("svg-content", true)
    //   .style("margin-top", "40px");

    var svg5 = d3.select(".ques_5_explanation div:nth-child(2)")
        .insert("div", ".replaybutton").classed("svg-content", true)
        .append("svg")
        .attr("class", "chartbar");
    svg5.append("g").classed('g-barsall', true);
    svg5.append("g").classed('g-bars', true);
    svg5.append("g").classed("g-datashow", true);
    svg5.append("g").classed("g-label", true);

    var colorg = d3.scale.ordinal()
        .range(["#96aaf2", "#5a9ddc"]);
    var colora = d3.scale.ordinal()
        .range(["#f2f4f8", "#ddeaf2"]);


    var g_gobar = function(svg, g_defdataset, g_dataset) {
      var parentNode=$(svg.node().parentNode);
      if(!parentNode.is(':visible'))return;
      svg.attr('width',0);
      var width_bar = parentNode.width();

      // width_bar = $(".ques_body").width();

        svg.attr('height', ((g_defdataset.length * h) + ((g_defdataset.length - 1) * h / 2)))
            .attr('width', width_bar);

        var barsall = svg.select('.g-barsall').selectAll("rect");
        if (barsall.empty()) {
            barsall = barsall.data(g_defdataset)
                .enter().append("rect").attr('width', 0);
        }

        barsall.attr("fill", colora)
            .attr("y", function(d, i) {
                return (i * h) + (i * h / 2);
            })
            .attr("x", onx)
            .attr("height", h)
            .attr("width", function(d) {
                return width_bar - onx;
            });

        var bars = svg.select('.g-bars').selectAll("rect");
        if (bars.empty()) {
            bars = bars.data(g_dataset)
                .enter().append("rect").attr('width', 0);
        }

        bars.attr("fill", function(d, i) {
                return colorg(i);
            })
            .attr("y", function(d, i) {
                // return i * (h / dataset.length);
                return (i * h) + (i * h / 2);
            })
            .attr("x", onx)
            .attr("height", h)
            .transition()
            .duration(750)
            .delay(100)
            .attr("x", onx)
            .attr("width", function(d) {
                // return d * extlength;
                return (width_bar - onx) * d / 100;
            });

        var datashow = svg.select('.g-datashow').selectAll("text");
        if (datashow.empty()) {
            datashow = datashow.data(g_dataset)
                .enter().append("text").attr('x', 0);
        }

        datashow.attr("dx", onx)
            .attr("dy", h - 5)
            // .attr("dominant-baseline", "central")
            .attr("text-anchor", "middle")
            .attr("font-weight", "bold")
            .attr("font-family", "$font-Curator-Regular")
            .attr("fill", "white")
            .text(function(d) {
                return d + "% of daily limit";
            })
            .attr("y", function(d, i) {
                // return i * (h / dataset.length);
                return (i * h) + (i * h / 2);
            })
            .transition()
            .duration(750)
            .delay(100)
            .attr("x", function(d) {
                // return d * extlength;
                return ((width_bar - onx) * d / 100) / 2;
            });

        var label = svg.select('.g-label').selectAll("text");
        if (label.empty()) {
            label = label.data(setlabel)
                .enter().append("text").attr('x', 0);
        }

        label.attr("x", "65")
            .attr("text-anchor", "end")
            .attr("dominant-baseline", "central")
            .attr("y", function(d, i) {
                // return i * (h / labelset.length) + 12.5;
                return (i * h) + ((i + 1) * h / 2);
            })
            .attr("font-size", 15)
            .attr("font-family", "$font-Curator-Regular")
            .text(function(d) {
                return d;
            });

    };

    //Graph functions
    global.gobar1 = function() {

        g_gobar(svg_c, defdataset, dataset);
        // $answercall = true;
        return true;
    };

    global.gobar2 = function() {
        g_gobar(svg2, defdataset, dataset2);

        // var barsall2 = svg2.append("g")
        //   .selectAll("rect")
        //   .data(defdataset)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colora)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // var bars2 = svg2.append("g").selectAll("rect")
        //   .data(dataset2)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colorg)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", 40);
        // var datashow2 = svg2.selectAll("text")
        //   .data(dataset2)
        //   .enter()
        //   .append("text")
        //   .attr("x", 0)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("dx", onx - 15)
        //   .attr("dy", 20)
        //   .attr("text-anchor", "end")
        //   .attr("font-size", 20)
        //   .attr("font-weight", "bold")
        //   .attr("fill", "white")
        //   .text(function(d) {
        //     return d + "% of daily limit";
        //   });
        // var label = svg2.append("g").selectAll("text")
        //   .data(setlabel)
        //   .enter()
        //   .append("text")
        //   .attr("x", "65")
        //   .attr("text-anchor", "end")
        //   .attr("y", function(d, i) {
        //     return i * (h / labelset.length) + 15;
        //   })
        //   .attr("font-size", 11)
        //   .text(function(d) {
        //     return d;
        //   });
        // //animation
        // datashow2.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("x", function(d) {
        //     return d * extlength;
        //   });
        // bars2.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // // $answercall2 = true;

        return true;
    };

    global.gobar3 = function() {
        g_gobar(svg3, defdataset, dataset3);

        // var barsall3 = svg3.append("g")
        //   .selectAll("rect")
        //   .data(defdataset)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colora)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // var bars3 = svg3.append("g").selectAll("rect")
        //   .data(dataset3)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colorg)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", 40);
        // var datashow3 = svg3.selectAll("text")
        //   .data(dataset3)
        //   .enter()
        //   .append("text")
        //   .attr("x", 0)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("dx", onx - 15)
        //   .attr("dy", 20)
        //   .attr("text-anchor", "end")
        //   .attr("font-size", 20)
        //   .attr("font-weight", "bold")
        //   .attr("fill", "white")
        //   .text(function(d) {
        //     return d + "% of daily limit";
        //   });
        // var label = svg3.append("g").selectAll("text")
        //   .data(setlabel)
        //   .enter()
        //   .append("text")
        //   .attr("x", "65")
        //   .attr("text-anchor", "end")
        //   .attr("y", function(d, i) {
        //     return i * (h / labelset.length) + 15;
        //   })
        //   .attr("font-size", 11)
        //   .text(function(d) {
        //     return d;
        //   });
        // //animation
        // datashow3.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("x", function(d) {
        //     return d * extlength;
        //   });
        // bars3.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // // $answercall3 = true;

        return true;
    };

    global.gobar4 = function() {
        g_gobar(svg4, defdataset, dataset4);

        // var barsall4 = svg4.append("g")
        //   .selectAll("rect")
        //   .data(defdataset)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colora)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // var bars4 = svg4.append("g").selectAll("rect")
        //   .data(dataset4)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colorg)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", 40);
        // var datashow4 = svg4.selectAll("text")
        //   .data(dataset4)
        //   .enter()
        //   .append("text")
        //   .attr("x", 0)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("dx", onx - 15)
        //   .attr("dy", 20)
        //   .attr("text-anchor", "end")
        //   .attr("font-size", 20)
        //   .attr("font-weight", "bold")
        //   .attr("fill", "white")
        //   .text(function(d) {
        //     return d + "% of daily limit";
        //   });
        // var label = svg4.append("g").selectAll("text")
        //   .data(setlabel)
        //   .enter()
        //   .append("text")
        //   .attr("x", "65")
        //   .attr("text-anchor", "end")
        //   .attr("y", function(d, i) {
        //     return i * (h / labelset.length) + 15;
        //   })
        //   .attr("font-size", 11)
        //   .text(function(d) {
        //     return d;
        //   });
        // //animation
        // datashow4.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("x", function(d) {
        //     return d * extlength;
        //   });
        // bars4.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // // $answercall4 = true;


        return true;
    };

    global.gobar5 = function() {
        g_gobar(svg5, defdataset, dataset5);

        // var barsall5 = svg5.append("g")
        //   .selectAll("rect")
        //   .data(defdataset)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colora)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // var bars5 = svg5.append("g").selectAll("rect")
        //   .data(dataset5)
        //   .enter()
        //   .append("rect")
        //   .attr("fill", colorg)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("x", onx)
        //   .attr("height", 25)
        //   .attr("width", 40);
        // var datashow5 = svg5.selectAll("text")
        //   .data(dataset5)
        //   .enter()
        //   .append("text")
        //   .attr("x", 0)
        //   .attr("y", function(d, i) {
        //     return i * (h / dataset.length);
        //   })
        //   .attr("dx", onx - 15)
        //   .attr("dy", 20)
        //   .attr("text-anchor", "end")
        //   .attr("font-size", 20)
        //   .attr("font-weight", "bold")
        //   .attr("fill", "white")
        //   .text(function(d) {
        //     return d + "% of daily limit";
        //   });
        // var label = svg5.append("g").selectAll("text")
        //   .data(setlabel)
        //   .enter()
        //   .append("text")
        //   .attr("x", "65")
        //   .attr("text-anchor", "end")
        //   .attr("y", function(d, i) {
        //     return i * (h / labelset.length) + 15;
        //   })
        //   .attr("font-size", 11)
        //   .text(function(d) {
        //     return d;
        //   });
        // //animation
        // datashow5.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("x", function(d) {
        //     return d * extlength;
        //   });
        // bars5.transition()
        //   .duration(920)
        //   .delay(100)
        //   .attr("x", onx)
        //   .attr("width", function(d) {
        //     return d * extlength;
        //   });
        // // $answercall5 = true;

        return true;
    };
    $(window).on("resize", function() {
        gobar1();
        gobar2();
        gobar3();
        gobar4();
        gobar5();
    });
});
