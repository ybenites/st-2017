import $ from "jquery";

import * as d3 from "d3";

// import fix_svg from "./libraries/fix-svg-size";

// import get_x from "./libraries/get-svg";
// import Slider from "./libraries/slider.js";

import "./libraries/jquery-ui.min.js";

import "./libraries/jquery.ui.touch-punch.js"; //enable touch action in jqueryui

// import bootstrap from "./libraries/bootstrap.js";

import "./libraries/hammer.min.js";



$(function() {

    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
        /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

    //Sugar count for all 5 questions starts at 0
    var $sugarcont1 = 0;
    var $sugarcont2 = 0;
    var $sugarcont3 = 0;
    var $sugarcont4 = 0;
    var $sugarcont5 = 0;

    var $cokesugar = 34;
    var $cakesugar = 40;
    var $muffinsugar = 19;
    var $marshmallowsugar = 38;
    var $bubbleteasugar = 24;
    var $yogurtsugar = 25;
    var $sundaesugar = 50;
    var $milosugar = 46;

    var $answercall = false;
    var $answercall2 = false;
    var $answercall3 = false;
    var $answercall4 = false;
    var $answercall5 = false;


    //All draggable circles of foods for 5 questions
    $(".cokedrag1").draggable({
        snap: "#dragger_ques1-1",
        stack: ".cokedrag1",
        containment: ".ques_1_body",
        scroll: false,
        revert: "invalid",
        start: function() {
            $("#hand_space svg").css("display", "none");
        }
    });
    $(".chocolatecakedrag1").draggable({
        snap: "#dragger_ques1-2",
        stack: ".chocolatecakedrag1",
        containment: ".ques_1_body",
        scroll: false,
        revert: "invalid",
        start: function() {
            $("#hand_space svg").css("display", "none");
        }
    });
    $(".muffindrag1").draggable({
        snap: "#dragger_ques1-3",
        stack: ".muffindrag1",
        containment: ".ques_1_body",
        scroll: false,
        revert: "invalid",
        start: function() {
            $("#hand_space svg").css("display", "none");
        }
    });
    //qu
    $(".marshmallowdrag2").draggable({
        snap: "#dragger_ques2-1",
        stack: ".marshmallowdrag2",
        containment: ".ques_2_body",
        scroll: false,
        revert: "invalid"
    });
    $(".yogurtdrag2").draggable({
        snap: "#dragger_ques2-2",
        stack: ".yogurtdrag2",
        containment: ".ques_2_body",
        scroll: false,
        revert: "invalid"
    });
    $(".milodrag2").draggable({
        snap: "#dragger_ques2-3",
        stack: ".milodrag2",
        containment: ".ques_2_body",
        scroll: false,
        revert: "invalid"
    });
    //qu
    $(".teadrag3").draggable({
        snap: "#dragger_ques3-1",
        stack: ".teadrag3",
        containment: ".ques_3_body",
        scroll: false,
        revert: "invalid"
    });
    $(".cokedrag3").draggable({
        snap: "#dragger_ques3-2",
        stack: ".cokedrag3",
        containment: ".ques_3_body",
        scroll: false,
        revert: "invalid"
    });
    $(".muffindrag3").draggable({
        snap: "#dragger_ques3-3",
        stack: ".muffindrag3",
        containment: ".ques_3_body",
        scroll: false,
        revert: "invalid"
    });
    //qu
    $(".sundaedrag4").draggable({
        snap: "#dragger_ques4-1",
        stack: ".sundaedrag4",
        containment: ".ques_4_body",
        scroll: false,
        revert: "invalid"
    });
    $(".yogurtdrag4").draggable({
        snap: "#dragger_ques4-2",
        stack: ".yogurtdrag4",
        containment: ".ques_4_body",
        scroll: false,
        revert: "invalid"
    });
    $(".milodrag4").draggable({
        snap: "#dragger_ques4-3",
        stack: ".milodrag4",
        containment: ".ques_4_body",
        scroll: false,
        revert: "invalid"
    });
    //qu
    $(".cokedrag5").draggable({
        snap: "#dragger_ques5-1",
        stack: ".cokedrag5",
        containment: ".ques_5_body",
        scroll: false,
        revert: "invalid"
    });
    $(".muffindrag5").draggable({
        snap: "#dragger_ques5-2",
        stack: ".muffindrag5",
        containment: ".ques_5_body",
        scroll: false,
        revert: "invalid"
    });
    $(".chocolatecakedrag5").draggable({
        snap: "#dragger_ques5-3",
        stack: ".chocolatecakedrag5",
        containment: ".ques_5_body",
        scroll: false,
        revert: "invalid"
    });

    function dropIn($dragtwice) {
        $dragtwice.addClass("uidropped").removeClass("uidropped2");
    }

    function dropOut($dragagain) {
        $dragagain.addClass("uidropped2").removeClass("uidropped");
    }


    //question 1
    $("#answerarea1").droppable({
        greedy: true,
        accept: ".dragger1",
        drop: function(event, ui) {
            var suggar_before_calc = $sugarcont1;
            calSugar(ui.draggable, $sugarcont1);
            //ui.draggable.draggable("disable", 1);
            $(this).css("background-color", "#f2f4f8");
            //Number animation
            $('.countr').each(function() {
                $(this).prop('Counter', suggar_before_calc).animate({
                    Counter: $(this).text()
                }, {
                    duration: 899,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            //Number animation
            $("#ques_1_answer:hidden:first")
                .fadeIn("slow");


        }
    });

    function calSugar($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped")) {} else if ($event_counter.hasClass("cokedrag1")) {
            $sugarcont1 += $cokesugar;
            dropIn($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
            numFloat($cokesugar, ".cokedrag1");
            //if the answer has been reached
            if ($answercall !== true && $sugarcont1 >= 19) {
                answerOn();
            } else if ($answercall !== true) {
                $("#prompt_bub1")
                    .replaceWith("<div class='bub_1 bub_all'>Try adding more, or changing the food item</div>");
            }
        } else if ($event_counter.hasClass("chocolatecakedrag1")) {
            numFloat($cakesugar, ".chocolatecakedrag1");
            $sugarcont1 += $cakesugar;
            dropIn($event_counter);
            // ...then update the numbers
            $("#hand_space svg").css("display", "none");
            $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
            if ($answercall !== true && $sugarcont1 >= 19) {
                answerOn();
            } else if ($answercall !== true) {
                $("#prompt_bub1")
                    .replaceWith("<div class='bub_1 bub_all'>Try adding more, or changing the food item</div>");
            }
        } else if ($event_counter.hasClass("muffindrag1")) {
            $sugarcont1 += $muffinsugar;
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
            numFloat($muffinsugar, ".muffindrag1");
            if ($answercall !== true && $sugarcont1 >= 19) {
                answerOn();
            } else if ($answercall !== true) {
                $("#prompt_bub1")
                    .replaceWith("<div class='bub_1 bub_all'>Try adding more, or changing the food item</div>");
            }
        }
    }
    $("#dragger_ques1-1").droppable({
        accept: ".cokedrag1",
        drop: function(event, ui) {
            minSugar(ui.draggable, $sugarcont1);

            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });

        }
    });
    $("#dragger_ques1-2").droppable({
        accept: ".chocolatecakedrag1",
        drop: function(event, ui) {
            minSugar(ui.draggable, $sugarcont1);
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques1-3").droppable({
        accept: ".muffindrag1",
        drop: function(event, ui) {
            minSugar(ui.draggable, $sugarcont1);
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });

    function minSugar($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped2")) {} else if ($event_counter.hasClass("cokedrag1")) {
            $sugarcont1 -= $cokesugar;
            dropOut($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
            return $sugarcont1;
        } else if ($event_counter.hasClass("chocolatecakedrag1")) {
            $sugarcont1 -= $cakesugar;
            dropOut($event_counter);
            // ...then update the numbers

            $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
            return $sugarcont1;
        } else if ($event_counter.hasClass("muffindrag1")) {
            $sugarcont1 -= $muffinsugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
            return $sugarcont1;

        }
    }
    //question 1 done ***


    $("#answerarea2").droppable({
        accept: ".dragger2",
        drop: function(event, ui) {
            var suggar_before_calc2 = $sugarcont2;
            calSugar2(ui.draggable, $sugarcont2);
            //ui.draggable.draggable("disable", 1);
            $(this).css("background-color", "#f2f4f8");

            //Number animation
            $('.countr2').each(function() {
                $(this).prop('Counter', suggar_before_calc2).animate({
                    Counter: $(this).text()
                }, {
                    duration: 899,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            //Number animation
            $("#ques_2_answer:hidden:first").fadeIn("slow");

        }
    });

    function calSugar2($event_counter, new_count) {
        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped")) {} else if ($event_counter.hasClass("marshmallowdrag2")) {
            $sugarcont2 += $marshmallowsugar;
            dropIn($event_counter); //add a dropped tag
            numFloat($marshmallowsugar, ".marshmallowdrag2");
            // ...then update the numbers
            $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr2'>" + $sugarcont2 + "</span>g");
            //if the answer has been reached
            if ($answercall2 !== true && $sugarcont2 >= 38) {
                answerOn2();
            } else if ($answercall2 !== true) {
                $("#prompt_bub2")
                    .replaceWith("<div class='2bub bub_all'>Try adding more, or changing the food item</div>");
            }
        } else if ($event_counter.hasClass("yogurtdrag2")) {
            $sugarcont2 += $yogurtsugar;
            dropIn($event_counter);
            numFloat($yogurtsugar, ".yogurtdrag2");
            // ...then update the numbers
            $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr2'>" + $sugarcont2 + "</span>g");

            if ($answercall2 !== true && $sugarcont2 >= 38) {
                answerOn2();
            } else if ($answercall2 !== true) {
                $("#prompt_bub2")
                    .replaceWith("<div class='2bub bub_all'>Try adding more, or changing the food item</div>");
            }
        } else if ($event_counter.hasClass("milodrag2")) {
            $sugarcont2 += $milosugar;
            dropIn($event_counter);
            numFloat($milosugar, ".milodrag2");
            // ...then update the numbers
            $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr2'>" + $sugarcont2 + "</span>g");
            if ($answercall2 !== true && $sugarcont2 >= 38) {
                answerOn2();
            } else if ($answercall2 !== true) {
                $("#prompt_bub2")
                    .replaceWith("<div class='2bub bub_all'>Try adding more, or changing the food item</div>");
            }
        }
    }
    //draggable2
    $("#dragger_ques2-1").droppable({
        accept: ".marshmallowdrag2",
        drop: function(event, ui) {
            minSugar2(ui.draggable, $sugarcont2);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });

    $("#dragger_ques2-2").droppable({
        accept: ".yogurtdrag2",
        drop: function(event, ui) {
            minSugar2(ui.draggable, $sugarcont2);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques2-3").droppable({
        accept: ".milodrag2",
        drop: function(event, ui) {
            minSugar2(ui.draggable, $sugarcont2);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });

    function minSugar2($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped2")) {} else if ($event_counter.hasClass("marshmallowdrag2")) {
            $sugarcont2 -= $marshmallowsugar;
            dropOut($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr2'>" + $sugarcont2 + "</span>g");
            return $sugarcont2;
        } else if ($event_counter.hasClass("yogurtdrag2")) {
            $sugarcont2 -= $yogurtsugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr2'>" + $sugarcont2 + "</span>g");
            return $sugarcont2;
        } else if ($event_counter.hasClass("milodrag2")) {
            $sugarcont2 -= $milosugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr2'>" + $sugarcont2 + "</span>g");
            return $sugarcont2;
        }
    }



    $("#answerarea3").droppable({
        accept: ".dragger3",
        drop: function(event, ui) {
            var suggar_before_calc3 = $sugarcont3;
            calSugar3(ui.draggable, $sugarcont3);
            //ui.draggable.draggable("disable", 1);

            $(this).css("background-color", "#f2f4f8");
            //Number animation
            $('.countr3').each(function() {
                $(this).prop('Counter', suggar_before_calc3).animate({
                    Counter: $(this).text()
                }, {
                    duration: 899,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            //Number animation
            $("#ques_3_answer:hidden:first").fadeIn("slow");

        }
    });

    function calSugar3($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped")) {} else if ($event_counter.hasClass("teadrag3")) {
            $sugarcont3 += $bubbleteasugar;
            numFloat($bubbleteasugar, ".teadrag3");
            dropIn($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr3'>" + $sugarcont3 + "</span>g");
            //if the answer has been reached
            if ($answercall3 !== true && $sugarcont3 >= 29) {
                answerOn3();
            } else if ($answercall3 !== true) {
                $("#prompt_bub3")
                    .replaceWith("<div class='3bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont3;
        } else if ($event_counter.hasClass("cokedrag3")) {
            $sugarcont3 += $cokesugar;
            numFloat($cokesugar, ".cokedrag3");
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr3'>" + $sugarcont3 + "</span>g");

            if ($answercall3 !== true && $sugarcont3 >= 29) {
                answerOn3();
            } else if ($answercall3 !== true) {
                $("#prompt_bub3")
                    .replaceWith("<div class='3bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont3;
        } else if ($event_counter.hasClass("muffindrag3")) {
            $sugarcont3 += $muffinsugar;
            numFloat($muffinsugar, ".muffindrag3");
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr3'>" + $sugarcont3 + "</span>g");

            if ($answercall3 !== true && $sugarcont3 >= 29) {
                answerOn3();
            } else if ($answercall3 !== true) {
                $("#prompt_bub3")
                    .replaceWith("<div class='3bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont3;
        }
    }

    //draggable3
    $("#dragger_ques3-1").droppable({
        accept: ".teadrag3",
        drop: function(event, ui) {
            minSugar3(ui.draggable, $sugarcont3);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques3-2").droppable({
        accept: ".cokedrag3",
        drop: function(event, ui) {
            minSugar3(ui.draggable, $sugarcont3);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques3-3").droppable({
        accept: ".muffindrag3",
        drop: function(event, ui) {
            minSugar3(ui.draggable, $sugarcont3);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });

    function minSugar3($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped2")) {} else if ($event_counter.hasClass("teadrag3")) {
            $sugarcont3 -= $bubbleteasugar;
            dropOut($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr3'>" + $sugarcont3 + "</span>g");
            return $sugarcont3;
        } else if ($event_counter.hasClass("cokedrag3")) {
            $sugarcont3 -= $cokesugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr3'>" + $sugarcont3 + "</span>g");
            return $sugarcont3;
        } else if ($event_counter.hasClass("muffindrag3")) {
            $sugarcont3 -= $muffinsugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr3'>" + $sugarcont3 + "</span>g");
            return $sugarcont3;
        }
    }
    //question 3 done


    $("#answerarea4").droppable({
        accept: ".dragger4",
        drop: function(event, ui) {
            var suggar_before_calc4 = $sugarcont4;
            calSugar4(ui.draggable, $sugarcont4);
            //ui.draggable.draggable("disable", 1);

            $(this).css("background-color", "#f2f4f8");
            //Number animation
            $('.countr4').each(function() {
                $(this).prop('Counter', suggar_before_calc4).animate({
                    Counter: $(this).text()
                }, {
                    duration: 899,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            //Number animation
            $("#ques_4_answer:hidden:first").fadeIn("slow");

        }
    });

    function calSugar4($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped")) {} else if ($event_counter.hasClass("sundaedrag4")) {
            $sugarcont4 += $sundaesugar;
            numFloat($sundaesugar, ".sundaedrag4");
            dropIn($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_4_answer").html("<strong>Sugar</strong> <span class='countr4'>" + $sugarcont4 + "</span>g");
            //if the answer has been reached
            if ($answercall4 !== true && $sugarcont4 >= 51) {
                answerOn4();
            } else if ($answercall4 !== true) {
                $("#prompt_bub4")
                    .replaceWith("<div class='4bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont4;
        } else if ($event_counter.hasClass("yogurtdrag4")) {
            $sugarcont4 += $yogurtsugar;
            numFloat($yogurtsugar, ".yogurtdrag4");
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_4_answer").html("<strong>Sugar</strong> <span class='countr4'>" + $sugarcont4 + "</span>g");

            if ($answercall4 !== true && $sugarcont4 >= 51) {
                answerOn4();
            } else if ($answercall4 !== true) {
                $("#prompt_bub4")
                    .replaceWith("<div class='4bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont4;
        } else if ($event_counter.hasClass("milodrag4")) {
            $sugarcont4 += $milosugar;
            numFloat($milosugar, ".milodrag4");
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_4_answer").html("<strong>Sugar</strong> <span class='countr4'>" + $sugarcont4 + "</span>g");

            if ($answercall4 !== true && $sugarcont4 >= 51) {
                answerOn4();
            } else if ($answercall4 !== true) {
                $("#prompt_bub4")
                    .replaceWith("<div class='4bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont4;
        }
    }

    //draggable4
    $("#dragger_ques4-1").droppable({
        accept: ".sundaedrag4",
        drop: function(event, ui) {
            minSugar4(ui.draggable, $sugarcont4);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques4-2").droppable({
        accept: ".yogurtdrag4",
        drop: function(event, ui) {
            minSugar4(ui.draggable, $sugarcont4);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques4-3").droppable({
        accept: ".milodrag4",
        drop: function(event, ui) {
            minSugar4(ui.draggable, $sugarcont4);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });

    function minSugar4($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped2")) {} else if ($event_counter.hasClass("sundaedrag4")) {
            $sugarcont4 -= $sundaesugar;
            dropOut($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_4_answer").html("Sugar<span class='countr4'>" + $sugarcont4 + "</span>g");
            return $sugarcont4;
        } else if ($event_counter.hasClass("yogurtdrag4")) {
            $sugarcont4 -= $yogurtsugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_4_answer").html("Sugar<span class='countr4'>" + $sugarcont4 + "</span>g");
            return $sugarcont4;
        } else if ($event_counter.hasClass("milodrag4")) {
            $sugarcont4 -= $milosugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_4_answer").html("Sugar<span class='countr4'>" + $sugarcont4 + "</span>g");
            return $sugarcont4;
        }
    }

    //question 4 done! ***
    $("#answerarea5").droppable({
        accept: ".dragger5",
        drop: function(event, ui) {
            var suggar_before_calc5 = $sugarcont5;
            calSugar5(ui.draggable, $sugarcont5);
            //ui.draggable.draggable("disable", 1);
            $(this).css("background-color", "#f2f4f8");
            //Number animation
            $('.countr5').each(function() {
                $(this).prop('Counter', suggar_before_calc5).animate({
                    Counter: $(this).text()
                }, {
                    duration: 899,
                    easing: 'swing',
                    step: function(now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
            //Number animation
            $("#ques_5_answer:hidden:first").fadeIn("slow");

        }
    });

    function calSugar5($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped")) {} else if ($event_counter.hasClass("cokedrag5")) {
            $sugarcont5 += $cokesugar;
            numFloat($cokesugar, ".cokedrag5");
            dropIn($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr5'>" + $sugarcont5 + "</span>g");
            //if the answer has been reached
            if ($answercall5 !== true && $sugarcont5 >= 36) {
                answerOn5();
            } else if ($answercall5 !== true) {
                $("#prompt_bub5")
                    .replaceWith("<div class='5bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont4;
        } else if ($event_counter.hasClass("muffindrag5")) {
            $sugarcont5 += $muffinsugar;
            numFloat($muffinsugar, ".muffindrag5");
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr5'>" + $sugarcont5 + "</span>g");
            if ($answercall5 !== true && $sugarcont5 >= 36) {
                answerOn5();
            } else if ($answercall5 !== true) {
                $("#prompt_bub5")
                    .replaceWith("<div class='5bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont5;
        } else if ($event_counter.hasClass("chocolatecakedrag5")) {
            $sugarcont5 += $cakesugar;
            numFloat($cakesugar, ".chocolatecakedrag5");
            dropIn($event_counter);
            // ...then update the numbers
            $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr5'>" + $sugarcont5 + "</span>g");

            if ($answercall5 !== true && $sugarcont5 >= 36) {
                answerOn5();
            } else if ($answercall5 !== true) {
                $("#prompt_bub5")
                    .replaceWith("<div class='5bub bub_all'>Try adding more, or changing the food item</div>");
            }
            return $sugarcont5;
        }
    }

    //draggable5
    $("#dragger_ques5-1").droppable({
        accept: ".cokedrag5",
        drop: function(event, ui) {
            minSugar5(ui.draggable, $sugarcont5);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques5-2").droppable({
        accept: ".muffindrag5",
        drop: function(event, ui) {
            minSugar5(ui.draggable, $sugarcont5);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });
    $("#dragger_ques5-3").droppable({
        accept: ".chocolatecakedrag5",
        drop: function(event, ui) {
            minSugar5(ui.draggable, $sugarcont5);
            //ui.draggable.draggable("disable", 1);
            //Number animation
            //Number animation
            var $this = $(this);
            ui.draggable.position({
                my: "center",
                at: "center",
                of: $this,
                using: function(pos) {
                    $(this).animate(pos, 700, "swing");
                }
            });
        }
    });

    function minSugar5($event_counter, new_count) {

        //If it is already dropped nothing would happen
        if ($event_counter.hasClass("uidropped2")) {} else if ($event_counter.hasClass("cokedrag5")) {
            $sugarcont5 -= $cokesugar;
            dropOut($event_counter); //add a dropped tag
            // ...then update the numbers
            $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr5'>" + $sugarcont5 + "</span>g");
            return $sugarcont5;
        } else if ($event_counter.hasClass("muffindrag5")) {
            $sugarcont5 -= $muffinsugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr5'>" + $sugarcont5 + "</span>g");
            return $sugarcont5;
        } else if ($event_counter.hasClass("chocolatecakedrag5")) {
            $sugarcont5 -= $cakesugar;
            dropOut($event_counter);
            // ...then update the numbers
            $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr5'>" + $sugarcont5 + "</span>g");
            return $sugarcont5;
        }
    }


    //Question 5 done!
    //The following functions are displaying the info when the sugar count is achieved
    function answerOn() {

        $(".ques_1_explanation:hidden:first")
            .fadeIn("slow")
            .css("display", "inline-block");

        $answercall = gobar1();


        if (isMobile) {
            $('html, body').animate({
                scrollTop: $(".ques_1_explanation").offset().top
            }, 1500);
        } else {
            $('html, body').animate({
                scrollTop: $("#autoScroll_1").offset().top
            }, 1500);

        }
    }

    function answerOn2() {

        $(".ques_2_explanation:hidden:first")
            .fadeIn("slow")
            .css("display", "inline-block");

        $answercall2 = gobar2();

        if (isMobile) {
            $('html, body').animate({
                scrollTop: $(".ques_2_explanation").offset().top
            }, 1500);
        } else {
            $('html, body').animate({
                scrollTop: $("#autoScroll_2").offset().top
            }, 1500);

        }
    }

    function answerOn3() {

        $(".ques_3_explanation:hidden:first")
            .fadeIn("slow")
            .css("display", "inline-block");
        $answercall3 = gobar3();


        if (isMobile) {
            $('html, body').animate({
                scrollTop: $(".ques_3_explanation").offset().top
            }, 1500);
        } else {
            $('html, body').animate({
                scrollTop: $("#autoScroll_3").offset().top
            }, 1500);

        }
    }

    function answerOn4() {

        $(".ques_4_explanation:hidden:first")
            .fadeIn("slow")
            .css("display", "inline-block");
        $answercall4 = gobar4();


        if (isMobile) {
            $('html, body').animate({
                scrollTop: $(".ques_4_explanation").offset().top
            }, 1500);
        } else {
            $('html, body').animate({
                scrollTop: $("#autoScroll_4").offset().top
            }, 1500);

        }
    }


    function answerOn5() {

        $(".ques_5_explanation:hidden:first")
            .fadeIn("slow")
            .css("display", "inline-block");
        $answercall5 = gobar5();

        if (isMobile) {
            $('html, body').animate({
                scrollTop: $(".ques_5_explanation").offset().top
            }, 1500);
        } else {
            $('html, body').animate({
                scrollTop: $("#autoScroll_5").offset().top
            }, 1500);

        }

    }

    function numFloat($num, classpart) {
        $(classpart).prepend("<div class='numberBlock'>" + $num + "g</div>");
        $(".numberBlock").hide("drop", {
            direction: "up"
        }, "slow", function() {
            $(".numberBlock").remove();
        });
        return false;
    }

    //reset buttons

    function resetdrag(dragger) {
        if ($(dragger).hasClass("uidropped")) {
            $(dragger).addClass("uidropped2").removeClass("uidropped").animate({
                'top': 0,
                'left': 0
            });
        }
    }

    $(".replay1").click(function() {
        resetdrag('.chocolatecakedrag1');
        resetdrag('.cokedrag1');
        resetdrag('.muffindrag1');
        $(".ques_1_explanation")
            .fadeOut("slow");
        $(".bub_1")
            .replaceWith("<p id='prompt_bub1'>Drag the comparison here</p>");
        $sugarcont1 = 0;
        $("#ques_1_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont1 + "</span>g");
        $answercall = false;
        $("#answerarea1").css("background-color", "transparent");
        $('html, body').animate({
            scrollTop: $("#Q1").offset().top
        }, 1500);
    });

    $(".replay2").click(function() {
        resetdrag('.marshmallowdrag2');
        resetdrag('.yogurtdrag2');
        resetdrag('.milodrag2');
        $(".ques_2_explanation")
            .fadeOut("slow");
        $(".2bub")
            .replaceWith("<p id='prompt_bub2'>Drag the comparison here</p>");
        $sugarcont2 = 0;
        $("#ques_2_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont2 + "</span>g");
        $answercall2 = false;
        $("#answerarea2").css("background-color", "transparent");
        $('html, body').animate({
            scrollTop: $("#Q2").offset().top
        }, 1500);
    });

    $(".replay3").click(function() {
        resetdrag('.teadrag3');
        resetdrag('.cokedrag3');
        resetdrag('.muffindrag3');
        $(".ques_3_explanation")
            .fadeOut("slow");

        $(".3bub")
            .replaceWith("<p id='prompt_bub3'>Drag the comparison here</p>");
        $sugarcont3 = 0;
        $("#ques_3_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont3 + "</span>g");
        $answercall3 = false;
        $("#answerarea3").css("background-color", "transparent");
        $('html, body').animate({
            scrollTop: $("#Q3").offset().top
        }, 1500);
    });

    $(".replay4").click(function() {
        resetdrag('.sundaedrag4');
        resetdrag('.yogurtdrag4');
        resetdrag('.milodrag4');
        $(".ques_4_explanation")
            .fadeOut("slow");

        $(".4bub")
            .replaceWith("<p id='prompt_bub4'>Drag the comparison here</p>");
        $sugarcont4 = 0;
        $("#ques_4_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont4 + "</span>g");
        $answercall4 = false;
        $("#answerarea4").css("background-color", "transparent");
        $('html, body').animate({
            scrollTop: $("#Q4").offset().top
        }, 1500);
    });

    $(".replay5").click(function() {
        resetdrag('.cokedrag5');
        resetdrag('.muffindrag5');
        resetdrag('.chocolatecakedrag5');
        $(".ques_5_explanation")
            .fadeOut("slow");
        $(".5bub")
            .replaceWith("<p id='prompt_bub5'>Drag the comparison here</p>");
        $sugarcont5 = 0;
        $("#ques_5_answer").html("<strong>Sugar</strong> <span class='countr'>" + $sugarcont5 + "</span>g");
        $answercall5 = false;
        $("#answerarea5").css("background-color", "transparent");
        $('html, body').animate({
            scrollTop: $("#Q5").offset().top
        }, 1500);
    });







});
