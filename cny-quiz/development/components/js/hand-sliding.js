import $ from "jquery";
import * as d3 from "d3";

// hand sliding parameters
var width_hand, svg_hand, g_hand_mov;

var margin_hand = {
    right: 10,
    left: 10
};

// hand sliding
$(function() {
    width_hand = ($("#hand_space").width() - margin_hand.left - margin_hand.right) * 0.7;
    svg_hand = d3.select("#hand_space").append("svg")
        .attr("width", width_hand + margin_hand.left + margin_hand.right)
        .attr("height", 50)
        .attr("fill", "#707070");


    g_hand_mov = svg_hand.append("g")
        .attr("class", "g_hand_mov")
        .attr("transform", "translate(0,0)"); // where hand starts sliding

    g_hand_mov.append("polygon")
        .attr("points", "17.3,25.5 17.2,15.2 16,14.1 14.8,14.1 13.6,15.2 13.6,32.9 12.2,32.9 8.5,29.3 6.3,29.4 5.6,30 5.7,32 12,38.3 16.3,42.4 16.4,45.8 27.5,44.9 27.5,41.4 29.6,39.3 29.8,26.6 28.7,25.5")
        .attr('transform', 'scale(0.9)')
        .attr('opacity', 0.85);

    var h_circle = g_hand_mov.append("circle")
        .attr('opacity', 0.85)
        .attr('fill', 'none')
        .attr('stroke', '#707070')
        .attr('stroke-width', 2)
        .attr('stroke-miterlimit', 10)
        .attr('cx', 15).attr('cy', 14.5).attr('r', 7.4);

    h_circle.each(circle_flash1);
    g_hand_mov.each(translate_hand1);

});

function circle_flash1() {
    d3.select(this)
        .transition()
        .attr("opacity", 0.01)
        .duration(800)
        .attr("r", 15)
        .each("end", circle_flash2);
}

function circle_flash2() {
    d3.select(this)
        .transition()
        .duration(0.001)
        .attr("r", 7.5)
        .attr("opacity", 0.8)
        .each("end", circle_flash1);
}

function translate_hand1() {
    d3.select(this)
        .transition()
        .duration(3000)
        .attr("transform", "translate(" + width_hand * 0.97 + ",0)")
        .each("end", translate_hand2);

}

function translate_hand2() {
    // alert("translate_hand2");
    d3.select(this)
        .transition()
        .duration(3000)
        .attr("transform", "translate(0,0)")
        .each("end", translate_hand1);

}
