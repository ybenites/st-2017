import $ from "jquery";
import * as d3 from "d3";
import * as Highcharts from "./libraries/highcharts-custom";

import t_options_line from "./line_setup";
import t_options_chart from "./chart_setup";

// var list_data = "http://localhost/graphics/api/list/list-data-sgcarmart";
// var list_data = "http://52.74.240.184/graphics/api/list/list-data-sgcarmart";
var list_data = "http://graphics.straitstimes.com/api/list/list-data-sgcarmart";

var name_date = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var name_date_long = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var order_coe = ['1st', '2nd'];
var ptos_show_display = 0;
var obj_categories = {
  a: {
    txt_top_cat: "CAT A",
    text_cat: "CARS UP TO 1600CC AND 130BHP",
    img: "cat-a.svg",
    scroll: "scroll-A"
  },

  b: {
    txt_top_cat: "CAT B",
    text_cat: "CARS ABOVE 1600CC OR 130BHP",
    img: "cat-b.svg",
    scroll: "scroll-B"
  },
  c: {
    txt_top_cat: "CAT C",
    text_cat: "GOODS VEHICLE & BUS",
    img: "cat-c.svg",
    scroll: "scroll-C"
  },
  d: {
    txt_top_cat: "CAT D",
    text_cat: "MOTORCYCLE",
    img: "cat-d.svg",
    scroll: "scroll-D"
  },
  e: {
    txt_top_cat: "CAT E",
    text_cat: "OPEN",
    img: "cat-e-wide.svg",
    scroll: "scroll-E"
  }
};

var template_card = `<div class="content-category">
  <div class="head-category">
    <div class="txt-top-cat">[TXT-TOP-CAT]<img class = "[SCROLL]" src="images/arrow-down.svg"></div>

    <div class="txt-cat">[TXT-CAT]</div>
    <div><img src="[IMG-CAT]" class="st-image-responsive" /></div>
  </div>
  <div class="body-category">
    <div class="cat-quota-premium">QUOTA PREMIUM</div>
    <div class="a-quota-premium">[A-QUOTA-PREMIUM]</div>
    <div class="content-symbol-txt-i-d">
      <span>[TEXT-I-D]</span>
    </div>
    <div class="a-increase-decrease">[A-I-D]</div>
  </div>
  <div class="footer-category">
    <div class="l-footer">
      <div class="l-quota">QUOTA</div>
      <div class="a-quota">[A-QUOTA]</div>
    </div>
    <div class="r-footer">
      <div class="l-bids-received">BIDS RECEIVED</div>
      <div class="a-bids-received">[A-BIDS-RECEIVED]</div>
    </div>
  </div>
</div>`;
var txt_increase = "INCREASED";
var txt_decrease = "DECREASED";
var txt_nochange = "NO CHANGE";

$(document).on("click", '.scroll-A', function() {
  $('html,body').animate({
    scrollTop: $(".section-2").offset().top
  }, 1000);
});

$(document).on("click", '.scroll-B', function() {
  $('html,body').animate({
    scrollTop: $(".section-3").offset().top
  }, 1000);
});

$(document).on("click", '.scroll-C', function() {
  $('html,body').animate({
    scrollTop: $(".section-4").offset().top
  }, 1000);
});

$(document).on("click", '.scroll-D', function() {
  $('html,body').animate({
    scrollTop: $(".section-5").offset().top
  }, 1000);
});

$(document).on("click", '.scroll-E', function() {
  $('html,body').animate({
    scrollTop: $(".section-6").offset().top
  }, 1000);
});

$(function() {
  loopAnimate();
  d3.selectAll(".content-graphic-2").style("opacity", 0).style("pointer-events", "none");
  $(".sky-tabs label").on("click", function() {
    var class_div = $(this).attr("for");
    var div_hide1 = $(this).parents(".st-row-graphic").find(".content-graphics .content-graphic-1");
    var div_hide2 = $(this).parents(".st-row-graphic").find(".content-graphics .content-graphic-2");

    d3.select(div_hide1[0])
      .style('opacity', 1)

      .transition().duration(500)
      .style('opacity', 0)
      .style("pointer-events", "none");

    d3.select(div_hide2[0])
      .style('opacity', 1)
      .transition().duration(500)
      .style('opacity', 0)
      .style("pointer-events", "none");


    var div_show = $(this).parents(".st-row-graphic").find(".content-graphics ." + class_div);
    d3.select(div_show[0]).style('opacity', 0)
      .transition().duration(500)
      .style('opacity', 1)
      .style("pointer-events", "all");
  });


  $.ajax({
    url: list_data,
    method: 'GET',
    dataType: 'json',
    headers: {
      // "Access-Control-Allow-Origin": "http://localhost"
    },
    success: function(data) {
      d3.select('.loading-page').classed('none', true);

      for (let i = 1; i <= 5; i++) {
        var data_cat = data[i - 1];
        var obj_cat = obj_categories[data_cat.category_coe];
        var replace_var = template_card.replace("[TXT-TOP-CAT]", obj_cat.txt_top_cat, "gi");
        replace_var = replace_var.replace("[TXT-CAT]", obj_cat.text_cat, "gi");
        replace_var = replace_var.replace("[IMG-CAT]", `images/images-coe/${obj_cat.img}`, "gi");
        replace_var = replace_var.replace("[SCROLL]", obj_cat.scroll, "gi");

        replace_var = replace_var.replace("[A-QUOTA-PREMIUM]", `$${format_number(data_cat.quota_premium)}`, "gi");
        var sign_positive = (data_cat.change > 0) ? "+" : "";
        replace_var = replace_var.replace("[A-I-D]", sign_positive + "$" + format_number(data_cat.change), "gi");
        replace_var = replace_var.replace("[A-QUOTA]", format_number(data_cat.quota), "gi");
        replace_var = replace_var.replace("[A-BIDS-RECEIVED]", format_number(data_cat.bids), "gi");

        if (parseInt(data_cat.change) > 0) {
          replace_var = replace_var.replace("[TEXT-I-D]", txt_increase, "gi");
          replace_var = $(replace_var).addClass('bg-increase');
        } else if (parseInt(data_cat.change) === 0) {
          replace_var = replace_var.replace("[TEXT-I-D]", txt_nochange, "gi");
          replace_var = $(replace_var).addClass('bg-nochange');
        } else {
          replace_var = replace_var.replace("[TEXT-I-D]", txt_decrease, "gi");
          replace_var = $(replace_var).addClass('bg-decrease');
        }

        $(".content-categories").append(replace_var);

        if (data_cat.date_next_tender) {
          var date_next_tender = data_cat.date_next_tender.trim().split(" ")[0].split("-");
          var string_next_tender = name_date[parseInt(date_next_tender[1]) - 1] + " " + date_next_tender[2] + ", " + date_next_tender[0];

          $(".date-next-tender").html(string_next_tender);
        }
      }
      var copy_months = [];
      var date_current_tender = "";
      for (let i = 1; i <= 10; i++) {
        var data_cat = data[i - 1];
        if (data_cat.date_tender) {
          var date_tender = data_cat.date_tender.trim().split(" ")[0].split("-");
          var exist_month = false;
          copy_months.forEach(function(d) {

            if (d === date_tender[1]) {
              exist_month = true;
            }
          });
          if (!exist_month) copy_months.push(date_tender[1]);
        }
        if (i <= 5) {
          date_current_tender = date_tender;
        }
      }
      var string_tender = (copy_months.length === 1) ? "2ND TENDER" : "1ST TENDER";
      var string_current_tender = name_date_long[parseInt(date_current_tender[1]) - 1] + " " + date_current_tender[0] + ", " + string_tender;

      $(".st-byline time").html(string_current_tender);


      var st_date_format = d3.timeParse("%Y-%m-%d");
      var a_date_cat_a = [],
        a_date_cat_b = [],
        a_date_cat_c = [],
        a_date_cat_d = [],
        a_date_cat_e = [];
      var a_price_cat_a = [],
        a_price_cat_b = [],
        a_price_cat_c = [],
        a_price_cat_d = [],
        a_price_cat_e = [];

      var a_quote_cat_a = [],
        a_quote_cat_b = [],
        a_quote_cat_c = [],
        a_quote_cat_d = [],
        a_quote_cat_e = [];
      var a_bid_cat_a = [],
        a_bid_cat_b = [],
        a_bid_cat_c = [],
        a_bid_cat_d = [],
        a_bid_cat_e = [];

      // var obj_months={};

      data.reverse().forEach((d, num) => {
        var a_date_split = d.date_tender.split("00:00:00")[0].trim();
        a_date_split = a_date_split.split("-");


        var name_month_s = name_date[parseInt(a_date_split[1]) - 1];
        var date_f = a_date_split[2] + ' ' + name_month_s + ' ' + a_date_split[0];

        if (d.category_coe === 'a') {
          a_price_cat_a.push(parseInt(d.quota_premium));

          a_date_cat_a.push(date_f + " " + (a_date_cat_a.length));
          a_quote_cat_a.push(parseInt(d.quota));
          a_bid_cat_a.push(parseInt(d.bids));
        } else if (d.category_coe === 'b') {
          a_price_cat_b.push(parseInt(d.quota_premium));
          a_date_cat_b.push(date_f + " " + (a_date_cat_b.length));
          a_quote_cat_b.push(parseInt(d.quota));
          a_bid_cat_b.push(parseInt(d.bids));
        } else if (d.category_coe === 'c') {
          a_price_cat_c.push(parseInt(d.quota_premium));
          a_date_cat_c.push(date_f + " " + (a_date_cat_c.length));
          a_quote_cat_c.push(parseInt(d.quota));
          a_bid_cat_c.push(parseInt(d.bids));
        } else if (d.category_coe === 'd') {
          a_price_cat_d.push(parseInt(d.quota_premium));
          a_date_cat_d.push(date_f + " " + (a_date_cat_d.length));
          a_quote_cat_d.push(parseInt(d.quota));
          a_bid_cat_d.push(parseInt(d.bids));
        } else if (d.category_coe === 'e') {
          a_price_cat_e.push(parseInt(d.quota_premium));
          a_date_cat_e.push(date_f + " " + (a_date_cat_e.length));
          a_quote_cat_e.push(parseInt(d.quota));
          a_bid_cat_e.push(parseInt(d.bids));
        }
      });

      var options_line_a = Object.assign({}, t_options_line());
      var options_line_b = Object.assign({}, t_options_line());
      var options_line_c = Object.assign({}, t_options_line());
      var options_line_d = Object.assign({}, t_options_line());
      var options_line_e = Object.assign({}, t_options_line());



      var options_chart_a = Object.assign({}, t_options_chart());
      var options_chart_b = Object.assign({}, t_options_chart());
      var options_chart_c = Object.assign({}, t_options_chart());
      var options_chart_d = Object.assign({}, t_options_chart());
      var options_chart_e = Object.assign({}, t_options_chart());


      /*setup options line for Category A*/
      options_line_a.series.push({
        color: "#5a9ddc",
        data: a_price_cat_a,
      });

      options_line_a.xAxis.categories = a_date_cat_a;
      Highcharts.chart('graphics-price-cata', options_line_a);
      /*end setup category A*/

      /*setup options chart for Category A*/
      options_chart_a.series.push({
        name: "BIDS RECEIVED",
        color: "#9cc4ea",
        visible: false,
        data: a_bid_cat_a
      });

      options_chart_a.series.push({
        name: "QUOTA",
        color: "#5a9ddc",
        visible: true,
        data: a_quote_cat_a
      });

      options_chart_a.xAxis.categories = a_date_cat_a;
      Highcharts.chart('graphics-qbids-cata', options_chart_a);
      /*end setup category A*/

      /*setup options for Category B*/
      options_line_b.series.push({
        name: "test",
        color: "#5a9ddc",
        data: a_price_cat_b
      });

      options_line_b.xAxis.categories = a_date_cat_b;
      Highcharts.chart('graphics-price-catb', options_line_b);
      /*end setup category B*/
      /*setup options chart for Category B*/

      options_chart_b.series.push({
        name: "BIDS RECEIVED",
        color: "#9cc4ea",
        visible: false,
        data: a_bid_cat_b
      });

      options_chart_b.series.push({
        name: "QUOTA",
        color: "#5a9ddc",
        visible: true,
        data: a_quote_cat_b
      });


      options_chart_b.xAxis.categories = a_date_cat_b;
      Highcharts.chart('graphics-qbids-catb', options_chart_b);
      /*end setup chart category B*/

      /*setup options for Category C*/
      options_line_c.series.push({
        name: "test",
        color: "#5a9ddc",
        data: a_price_cat_c
      });

      options_line_c.xAxis.categories = a_date_cat_c;
      Highcharts.chart('graphics-price-catc', options_line_c);
      /*end setup category C*/
      /*setup options chart for Category C*/
      options_chart_c.series.push({
        name: "BIDS RECEIVED",
        color: "#9cc4ea",
        visible: false,
        data: a_bid_cat_c
      });

      options_chart_c.series.push({
        name: "QUOTA",
        color: "#5a9ddc",
        visible: true,
        data: a_quote_cat_c
      });


      options_chart_c.xAxis.categories = a_date_cat_c;
      Highcharts.chart('graphics-qbids-catc', options_chart_c);
      /*end setup chart category C*/

      /*setup options for Category D*/
      options_line_d.series.push({
        name: "test",
        color: "#5a9ddc",
        data: a_price_cat_d
      });

      options_line_d.xAxis.categories = a_date_cat_d;
      Highcharts.chart('graphics-price-catd', options_line_d);
      /*end setup category D*/
      /*setup options chart for Category d*/
      options_chart_d.series.push({
        name: "BIDS RECEIVED",
        color: "#9cc4ea",
        visible: false,
        data: a_bid_cat_d
      });

      options_chart_d.series.push({
        name: "QUOTA",
        color: "#5a9ddc",
        visible: true,
        data: a_quote_cat_d
      });


      options_chart_d.xAxis.categories = a_date_cat_d;
      Highcharts.chart('graphics-qbids-catd', options_chart_d);
      /*end setup chart category D*/

      /*setup options for Category E*/
      options_line_e.series.push({
        name: "test",
        color: "#5a9ddc",
        data: a_price_cat_e
      });

      options_line_e.xAxis.categories = a_date_cat_e;
      Highcharts.chart('graphics-price-cate', options_line_e);
      /*end setup category E*/
      /*setup options chart for Category E*/
      options_chart_e.series.push({
        name: "BIDS RECEIVED",
        color: "#9cc4ea",
        visible: false,
        data: a_bid_cat_e
      });

      options_chart_e.series.push({
        name: "QUOTA",
        color: "#5a9ddc",
        visible: true,
        data: a_quote_cat_e
      });


      options_chart_e.xAxis.categories = a_date_cat_e;
      Highcharts.chart('graphics-qbids-cate', options_chart_e);
      /*end setup chart category e*/

    }
  });
});

function f_format_date(pos, m, y) {
  // var pos_f = (pos === '') ? '' : pos + '<br>';
  var pos_f = (pos === '') ? '' : pos + ' ';
  return pos_f + m + '<br>' + y;
}

function calc_step(total_ptos, ptos_show) {
  var res;
  var tax = Math.round(total_ptos / ptos_show);
  if (tax <= 1) {
    res = 0;
  } else {
    res = tax;
  }
  return res;
}

function format_number(number) {
  Math.floor(number);
  var val_return = 0;
  var format_number_int = d3.format(",d"),
    format_number_decimal = d3.format(",.2f");
  var prom = number % 1;
  if (prom === 0) {
    val_return = format_number_int(number);
  } else {
    if (format_number_decimal(prom) % 1 === 0) {
      val_return = format_number_int(Math.floor(number) + 1);
    } else {
      val_return = format_number_decimal(number);
    }
  }
  return val_return;
}


function loopAnimate() {
  var sreenwidth = $(window).width();
  // console.log(sreenwidth * -1.3);
  var tmpmargin = sreenwidth * -1.3;
  $(".car-movement").css("margin-left", tmpmargin + "px");

  $(".car-movement").animate({
    marginLeft: "0"
  }, sreenwidth * 20, "linear", loopAnimate);

}
// $(".scroll").click(function() {
//   alert("hi");
//     // $('html,body').animate({
//     //     scrollTop: $(".section-2").offset().top},
//     //     'slow');
// });
//
//
// $(document).ready(function(){
//     $(".scroll").click(function(){
//         alert("The paragraph was clicked.");
//     });
// });
