var options = function() {
  function format_number(number) {
    Math.floor(number);
    var val_return = 0;
    var format_number_int = d3.format(",d"),
      format_number_decimal = d3.format(",.2f");
    var prom = number % 1;
    if (prom === 0) {
      val_return = format_number_int(number);
    } else {
      if (format_number_decimal(prom) % 1 === 0) {
        val_return = format_number_int(Math.floor(number) + 1);
      } else {
        val_return = format_number_decimal(number);
      }
    }
    return val_return;
  }
  return {
    chart: {
      zoomType: 'x',
      type: 'column',
      spacingRight: 30,
      events: {
        selection: function(event) {
          if (typeof event.xAxis !== "undefined" && typeof event.xAxis !== 'undefined') {
            //       var min = event.xAxis[0].min;
            //       var max = event.xAxis[0].max;
            //       ptos_show_display=($(window).width()-120)/75;
            // step = calc_step(max - min, ptos_show_display);
            //       // img_drag.fadeOut();
            //     } else {
            //       ptos_show_display=($(window).width()-120)/75;
            //       step = calc_step(24, ptos_show_display);
          }
        }
      }
    },
    credits: {
      enabled: false
    },
    title: {
      text: '',
      x: -20 //center
    },

    xAxis: {
      lineColor: '#e0e0e0',
      lineWidth: 1,
      tickWidth: 1,
      tickColor: '#e0e0e0',
      // tickAmount: 4,

      // startOnTick: true,
      // endOnTick: true,
      categories: [],
      // tickInterval: 10,
      labels: {
        autoRotation: false,
        staggerLines: 1,
        align: "center",
        // step: 5,
        y: 25,
        format: "{value}",
        formatter: function() {
          var r_value = "";
          var a_value = this.value.trim().split(" ");
          a_value = a_value.filter(function(d) {
            return d !== "";
          });

          r_value += a_value[1] + "&nbsp" + a_value[0] + "<br>";
          r_value += a_value[a_value.length - 2];

          var pos = a_value[a_value.length - 1];
          var gap = parseInt(($(window).width() - 200));
          var num_lot = parseInt((gap + 20) / 70);
          var even_num = Math.ceil((this.axis.tickPositions.length / (num_lot + 1)));

          var real_pos;
          this.axis.tickPositions.forEach(function(d, n) {
            if (parseInt(pos) === parseInt(d)) real_pos = (n);
          });
          // console.log(real_pos)
          // console.log(this.axis.tickPositions.length/2);
          even_num = (((this.axis.tickPositions.length / even_num) % 2) === 0) ? (even_num + 1) : even_num;
          return (this.isLast || real_pos % even_num === 0) ? r_value : "";
        },
        useHTML: true,
        rotation: 0,
        style: {
          fontSize: '14px',
          fontFamily: `'CuratorRegular', 'Helvetica Neue', Helvetica, Arial, sans-serif`,
          textOverflow: "none",
          "textAlign": "center"
        }
      },
      events: {
        setExtremes: function(event) {
          // this.options.labels.step = 6;
        }
      },
    },
    yAxis: {
      min: 0,

      // lineColor: '#e0e0e0',
      // lineWidth: 1,
      // tickWidth: 1,
      // tickColor: '#e0e0e0',
      gridLineDashStyle: 'dot',
      tickWidth: 0,
      // tickColor: '#e0e0e0',
      title: {
        text: '',
        align: 'high',
        offset: 10,
        rotation: 0,
        y: -20,
        style: {
          color: '#333'
        }
      },
      labels: {
        format: '{value}',
        formatter: function() {
          var number = this.value;
          Math.floor(number);
          var val_return = 0;
          var format_number_int = d3.format(",d"),
            format_number_decimal = d3.format(",.2f");
          var prom = number % 1;
          if (prom === 0) {
            val_return = format_number_int(number);
          } else {
            if (format_number_decimal(prom) % 1 === 0) {
              val_return = format_number_int(Math.floor(number) + 1);
            } else {
              val_return = format_number_decimal(number);
            }
          }
          return val_return;
        },
        style: {
          fontSize: '14px',
          fontFamily: `'CuratorRegular', 'Helvetica Neue', Helvetica, Arial, sans-serif`,
          textOverflow: "none",
          "textAlign": "center"
        }
      }
    },
    legend: {
      layout: 'horizontal',
      align: 'right',
      verticalAlign: 'top',
      borderWidth: 0,
      symbolRadius: 0,
      itemStyle: {
        "fontWeight": "normal"
      }
    },
    tooltip: {
      shared: true,
      shadow: false,
      borderRadius: 0,
      backgroundColor: '#EFF3F4',
      borderColor: '#EFF3F4',
      useHTML: true,
      formatter: function() {
        var elemt = this;
        var a_date = elemt.x.split(" ");
        var date = a_date[1] + " " + a_date[0] + ", " + a_date[2];
        var bidsReceived;
        if (elemt.points[0] && elemt.points[0].series.name == "BIDS RECEIVED") bidsReceived = format_number(elemt.points[0].y);
        if (elemt.points[0] && elemt.points[0].series.name == "QUOTA") quota = format_number(elemt.points[0].y);
        var quota;
        if (elemt.points[1]) quota = format_number(elemt.points[1].y);

        var classBidsReceived = "";
        if (bidsReceived === undefined) classBidsReceived = "none";

        var classQuota = "";
        if (quota === undefined) classQuota = "none";
        return `<div class="tooltip-st-dev"><div class="tooltip-row">${date}</div><div class="tooltip-row ${classBidsReceived}"><label>BIDS RECEIVED: </label><strong>${bidsReceived}</strong></div><div class="tooltip-row ${classQuota}"><label>QUOTA: </label><strong>${quota}</strong></div></div>`;
      }
    },
    plotOptions: {
      column: {
        //stacking: 'normal',
        grouping: false,
        shadow: false,
        dataLabels: {
          enabled: false,
          //color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
        }
      }
    },
    series: []
  };
}

module.exports = options;
