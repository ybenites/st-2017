import $ from "jquery";

$(function() {
  loopAnimate();


});


function loopAnimate() {
  var sreenwidth = $(window).width();
  // console.log(sreenwidth * -1.3);
  var tmpmargin = sreenwidth * -1.3;
  $(".car-movement").css("margin-left", tmpmargin + "px");

  $(".car-movement").animate({
    marginLeft: "0"
  }, sreenwidth * 20, "linear", loopAnimate);

}
