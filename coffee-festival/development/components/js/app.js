import $ from "jquery";
import * as d3 from "d3";
var screenWidth = $(window).width();
var totalParticipant;
var img_base64 = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA9JREFUeNpi+P//P0CAAQAF/gL+Lc6J7gAAAABJRU5ErkJggg==`;
var url_save_image = "/functions/graphics/save_images";
// var url_save_image = "/graphics/functions/graphics/save_images";
var global_coofee;
var questionClicked = [false, false, false, false, false, false, false];
var currentQn = 0;
$(document).ready(function() {
  // $('.title').show().animateCss('bounceInDown');
  // $('.deck').show().delay(500).animateCss('bounceInDown');
  // $('.by-line').show().animateCss('bounceInDown');
  $(".main-headline").fadeIn();
  setTimeout(function() {
    $(".play").animate({
      opacity: 1
    }, 1000);
  }, 1800);
  $(".play").click(function() {
    //$(".st-header-page").fadeOut();
    //$(".st-header-page").animateCss('flipOutY');
    $(".st-header-page").fadeOut();
    $(".play").hide();
    $(".quiz").delay(500).fadeIn(500);

    setTimeout(function() {
      $('body').css("background-color", "#EFF3F4");
    }, 500);

    if (screenWidth > 767) {
      $(".banner").delay(500).fadeIn(500);
    }
  });
});

$(function() {
  d3.csv("data/coffee-quiz.csv", function(data) {

    data.forEach(function(d, i) {
      d.question_id = d.question_id;
      d.class = d.class;
      d.image = "images/_img/" + d.image;
      d.image_class = d.image_class;
      d.question = d.question;
      d.img_bean = "images/_img/" + d.img_bean;
      d.opt_4 = d.opt_4;
      d.opt_3 = d.opt_3;
      d.opt_2 = d.opt_2;
      d.opt_1 = d.opt_1;

      var quiz = '<div class="' + d.class + '" id="' + d.question_id + '"><div class="question"><div class="' + d.image_class + '"><img src="' + d.image + '"></div><div class="selectAns"><h1>' +
        d.question + '</h1><label id="Lbl1" for="question1"> <img src="' + d.img_bean + '"><span>' + d.opt_4 +
        '</span><input type="radio" name="question1" value="4"></label><br /><label id="Lbl2" for="question1"><img src="' + d.img_bean + '"><span>' + d.opt_3 +
        '</span><input type="radio" name="question1" value="3"></label><br /><label id="Lbl3" for="question1"><img src="' + d.img_bean + '"><span>' + d.opt_2 +
        '</span><input type="radio" name="question1" value="2"></label><br /><label id="Lbl4" for="question1"><img src="' + d.img_bean + '"><span>' + d.opt_1 +
        '</span><input type="radio" name="question1" value="1"></label><br /> </div></div></div>';

      $('.quiz').append(quiz);
    });
  });
});

$(function() {
  d3.csv("data/text.csv", function(data) {

    data.forEach(function(d, i) {
      d.id = d.id;
      d.name = d.name;
      d.text1 = d.text1;
      d.information = d.information;
      d.funfact = d.funfact;
      d.img = "images/_img/" + d.img;

      var content = "<div class='coffee none' id='" + d.id + "'><img src='" + d.img + "'><h1>Your coffee personality is...</h1> <h2>" +
        d.name + "</h2><h3>" + d.text1 + "</h3><p>" + d.information + "</p><h1>DID YOU KNOW?</h1><span>" + d.funfact + "</span>" +
        "<div class='sharebtn'><p>SHARE</p></div>" +
        "<div class='comparebtn'><p>COMPARE YOUR RESULTS</p></div></div>";
      $(".results").append(content);
    });


    let totalcount = 0;
    let gif = 1;
    var coffee;

    // $(document).on("click", '.question span', function() {
    $(document).on("click", 'label', function() {
      $("span", this).next().prop("checked", "checked");
      $("span", this).prev().attr('src', "images/_img/ans-bean-select.png");
      if (!questionClicked[currentQn]) {
        questionClicked[currentQn] = true;
        totalcount += parseInt($("span", this).next().val());
        gif += 1;

        console.log(totalcount);
      }
      // console.log(gif);
      //$(this).closest(".qnContainer").delay(500).fadeOut(500).next().delay(1000).fadeIn(500);
      // $(this).closest(".question").delay(500).slideUp();
      // $(this).closest(".qnContainer").next().delay(1000).slideDown();
      // $(this).closest(".question").fadeOut(function(){
      $("span", this).closest(".qnContainer").fadeOut(function() {
        currentQn++;
        $("span", this).closest(".qnContainer").next().fadeIn();
      });
      $(".drink img").attr('src', "images/_img/qn" + gif + "_animate.gif");

      if (gif == 9) {
        //$(".quiz").animateCss('flipOutY');
        $(".quiz").delay(500).fadeOut(500);
        $(".drink img").delay(500).fadeOut(500);
        $(".resultpage").delay(1000).fadeIn(500);
        // $(".sharebtn").delay(1000).fadeIn(500);
        // $(".infocontainer").delay(1000).fadeIn(500);

        if (totalcount < 11) {
          $("#affogato").fadeIn();
          coffee = "affogato";
        }
        if (11 < totalcount && totalcount < 16) {
          $("#kopiluwak").fadeIn(500);
          coffee = "kopiluwak";
        }
        if (15 < totalcount && totalcount < 19) {
          $("#latte").fadeIn(500);
          coffee = "latte";
        }
        if (18 < totalcount && totalcount < 23) {
          $("#yuanyang").fadeIn(500);
          coffee = "yuanyang";
        }
        if (22 < totalcount && totalcount < 26) {
          $("#coldbrew").fadeIn(500);
          coffee = "coldbrew";
        }
        if (25 < totalcount && totalcount < 29) {
          $("#americano").fadeIn(500);
          coffee = "americano";
        }
        if (28 < totalcount && totalcount < 33) {
          $("#espresso").fadeIn(500);
          coffee = "espresso";
        }
        var compare = '<h1>Welcome to The Coffeeshop<br /> Here you can see just how other coffee lovers fared in the quiz!</h1><img class="none" src="images/_img/' +
          coffee + '_s.jpg"><p><span class="percentage_personal"></span><br>of other quiz users got the same coffee type as you</p> <h2>See how popular the other coffee types are:</h2>';
        $(".compare").append(compare);


        var serverData_array = [coffee];
        var img_data = {
          'img_base64': img_base64,
          'img_name': "test",
          "img_ext": "png",
          'data_record': serverData_array
        };

        $.ajax({
          url: url_save_image,
          type: "POST",
          data: img_data,
          dataType: "jsonp",
          success: function(response) {
            if (response.code === "ok") {
              // call your function with your parameter
              var obj_coffee = [];
              var new_total = 0;
              var parse_record = JSON.parse(response.record);
              parse_record.forEach(d => {
                if (d.selection_name !== "Total") {
                  new_total += parseInt(d.count);
                }
              });
              parse_record.forEach(d => {
                if (d.selection_name !== "Total") {
                  var percentage_personal = parseInt((parseInt(d.count) / new_total) * 10000) / 100;
                  obj_coffee.push(percentage_personal);
                  if (d.selection_name === coffee) {
                    $(".compare").find(".percentage_personal").html(percentage_personal + " %");
                  }
                }
              });
              global_coofee = obj_coffee;
              // createCircles(global_coofee);


              // update_server_data(serverData_array, response.record);
            }
          }
        });


        $(".comparebtn").click(function() {
          $(".resultpage").delay(500).fadeOut(500);
          $(".comparepage").delay(900).fadeIn(500);
          $(".st-content-footer").delay(900).fadeIn(500);
          $(".popupbox").delay(500).fadeOut(500);
          $('html, body').animate({
            scrollTop: '+=50px'
          }, 800);
          $(".totalcircle").each(function() {
            var compareID = $(this).attr("id");
            compareID = "coffee-" + coffee;
            $("#" + compareID).css('font-family', '"CuratorBold", "Helvetica Neue", Helvetica, Arial, sans-serif');
          });

          // $.getJSON("coffee.json", function(obj) {
          //
          //   // var data = {
          //   //   "count": value.count,
          //   //   "coffee": value.selection_name
          //   // };
          //   // var myArray = [];
          //   // myArray.push(value.selection_name, value.count);
          //   // console.log(myArray);
          //   var arr = $.map(obj, function(el) {
          //     return el.count;
          //   });
          //   console.log(arr);
          //   //console.log(arr[0]);
          //   totalParticipant = arr[0];
          //   // console.log(totalParticipant);
          //   $.each(obj, function(key, value) {
          //     var tmpCount = (Math.round(value.count / totalParticipant * 100));
          //     console.log(tmpCount);
          //   });
          //   localStorage.setItem("tmpCount", tmpCount);
          //
          // });
        });
      }
    });
  });
});

var size;
if ($(window).width() < 321) {
  size = 60;
}
if ($(window).width() > 321) {
  size = 75;
}

// (function() {
var canvas = document.getElementById('graph'),
  circlesCreated = false;

function onScroll() {
  if (!circlesCreated && $('.comparepage').is(':visible')) {
    circlesCreated = true;
    createCircles(global_coofee);
  }
}

function createCircles(a_value) {
  var colors = [
    ['#EFF3F4', '#D30000'],
    ['#EFF3F4', '#F45100'],
    ['#EFF3F4', '#FF8C36'],
    ['#EFF3F4', '#FFE739'],
    ['#EFF3F4', '#62BF00'],
    ['#EFF3F4', '#0094BC'],
    ['#EFF3F4', '#4B253A']
  ];

  var coffeegraph = [
      ['Affogato'],
      ['Americano'],
      ['Latte'],
      ['Coldbrew'],
      ['Espresso'],
      ['Kopi Luwak'],
      ['Yuan Yang']
    ],

    circles = [];

  for (var i = 1; i <= 7; i++) {
    var child = document.getElementById('circles-' + i),
      value = a_value[i - 1];
    circles.push(Circles.create({
      id: child.id,
      value: value,
      radius: size,
      width: 10,
      colors: colors[i - 1],
      maxValue: 100,
      text: coffeegraph[i - 1] + ' ' + value + '%',
      duration: 2000,
      wrpClass: 'circles-wrp',
      textClass: 'circles-text',
      valueStrokeClass: 'circles-valueStroke',
      maxValueStrokeClass: 'circles-maxValueStroke',
      styleWrapper: true,
      styleText: true
    }));
  }
}
window.onscroll = onScroll;
// })();

$(function() {
  $(document).on("click", '.sharebtn', function() {
    $(".popupbox").fadeIn();
  });

  $(".closeimg").click(function() {
    $(".popupbox").fadeOut();

  });

  $(".gotitbtn").click(function() {
    $(this).fadeOut(function() {
      $(".shareIcons").fadeIn();
    });
  });

});
