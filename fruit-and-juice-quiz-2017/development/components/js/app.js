// require("../precss/app.css");

import $ from "jquery";
import hand_sliding_effect from "./libraries/hand-sliding";
// import xx from "./library/fix-svg-size";



import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];
//
// a_test.forEach(d => {
//   console.log(d);
// });

let ansArray, check_quiz_if_ans = [],
  top_bar_progress = 0,
  qu_id_show = 0;
$(function() {
  d3.csv("data/fruits-data.csv", function(data) {

    data.forEach(function(d) {
      top_bar_progress = parseInt(d.questionID);
      d.justID = d.questionID;
      d.questionID = 'QUESTION ' + d.questionID;
      d.questionText = d.questionText;
      d.optionA = d.optionA;
      d.optionB = d.optionB;
      d.AnsText = d.AnsText;
      d.optionA_img = "images/fruits/" + d.optionA_img + ".jpg";
      d.optionB_img = "images/fruits/" + d.optionB_img + ".jpg";
      if (d.optionAns == 'a') ansArray = ["correct_img", "wrong_img"];
      else ansArray = ["wrong_img", "correct_img"];

      var htmlcontent = '<div class="qns none"><div class="qns_top">' +
        '<div class="qns_num">' + d.questionID + '</div>' +
        '<div class="qns_title">' + d.questionText + '</div></div>' +
        '<div class="compareBox">' +
        '<div class="pic_text">' +
        '<div class="foodOptionPic">' +
        '<div id="fn_' + d.justID + '_1" class="foodName"><div class="text_vertical_center">' + d.optionA + '</div></div>' +
        '<img src="' + d.optionA_img + '" class="st-image-responsive">' +
        '<div class="ans_img ' + ansArray[0] + ' none"></div>' +
        '</div></div>' +
        '<span class="or">OR</span>' +
        '<div class="pic_text">' +
        '<div class="foodOptionPic">' +
        '<div id="fn_' + d.justID + '_2" class="foodName"><div class="text_vertical_center">' + d.optionB + '</div></div>' +
        '<img src="' + d.optionB_img + '" class="st-image-responsive">' +
        '<div class="ans_img ' + ansArray[1] + ' none"></div>' +
        '</div></div></div>' +
        '<div class="answer none">' + d.AnsText + '</div></div>';

      $('.qnsSection').append(htmlcontent);

      check_quiz_if_ans[top_bar_progress - 1] = "false";
      // make_same_height("#fn_" + d.justID + "_1", "#fn_" + d.justID + "_2");
    });

  });

});
$(function() {
  $(document).on("click", '.startbtn', function() {

    // show first question
    $(".main-headline").fadeOut(function() {
      $(".qnsSection .qns").eq(0).fadeIn();
      make_same_height("#fn_1_1", "#fn_1_2");
    });


    // $('html, body').animate({
    //   scrollTop: $('.st-deck').offset().top
    // }, 'slow');

    // show hand sliding in first question
    $(".qnsSection .qns").eq(0).attr("id", "hand_space");
    hand_sliding_effect("#hand_space");

    $(this).fadeOut();
  });

  $(document).on("click", '.compareBox', function() {

    $("#hand_space svg").remove();

    // show answer text
    $(this).siblings(".answer").fadeIn();
    $(this).find(".ans_img").fadeIn();
    // $('html, body').animate({
    //   scrollTop: $(this).siblings(".qns_top").offset().top - 20
    // }, 'slow');

    // show direction btn
    var qu_index = $(this).parent(".qns").index();
    if (qu_index > 1 && qu_index < top_bar_progress + 2) $("#rightbtn").fadeIn();
    if (qu_index > 2 && qu_index < top_bar_progress + 2) $("#leftbtn").fadeIn();

    check_quiz_if_ans[qu_index - 2] = "true";

    // console.log(qu_index - 2);
    // console.log(check_quiz_if_ans[qu_index - 2]);
  });

  $("#rightbtn").click(function() {
    // console.log(top_bar_progress);
    $(".qnsSection .qns").eq(qu_id_show).fadeOut();
    qu_id_show++;
    $('.bar-long').css('width', qu_id_show / top_bar_progress * 100 + "%");

    // show next question
    var this_qu = qu_id_show;
    this_qu++;
    $(".qnsSection .qns").eq(qu_id_show).fadeIn(function() {
      for_answered_quiz(qu_id_show);
    });
    make_same_height("#fn_" + this_qu + "_1", "#fn_" + this_qu + "_2");
    $(this).css("display", "none");
    $("#leftbtn").css("display", "none");

    // for answered quiz, show right and left btn
    // for_answered_quiz(qu_id_show);

  });
  $("#leftbtn").click(function() {
    $(".qnsSection .qns").eq(qu_id_show).fadeOut();
    qu_id_show--;
    $('.bar-long').css('width', qu_id_show / top_bar_progress * 100 + "%");

    // show previous question
    $(".qnsSection .qns").eq(qu_id_show).fadeIn(function() {
      for_answered_quiz(qu_id_show);
    });
    $(this).css("display", "none");
    $("#rightbtn").css("display", "none");

    // for answered quiz, show right and left btn

  });
});

function make_same_height(div1, div2) {
  var hheight = Math.max($(div1).height(), $(div2).height());
  $(div1).height(hheight);
  $(div2).height(hheight);
}

function for_answered_quiz(index) {
  if (check_quiz_if_ans[index] == "true") {
    if (index > 0) $("#leftbtn").css("display", "block");
    $("#rightbtn").css("display", "block");
  }
}
