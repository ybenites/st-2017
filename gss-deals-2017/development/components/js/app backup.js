// require("../precss/app.css");
// import xx from "./library/fix-svg-size";

var $ = require('jquery');
var jQuery = $;
global.jQuery=jQuery;

var Masonry=require('masonry-layout');
require('jquery-bridget');
$.bridget( 'masonry', Masonry);

// var Typeahead = require('typeahead');

require('select2');


import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);


// file.js

var url_marker_svg = "images/icon-marker.svg";
var url_marker_svg_selected = "images/icon-marker-selected.svg";
var marker_flag_selected;

var info = {};
// var st_url_csv = "http://localhost/graphics/st-get-file-csv/1LWJVov1y1pIxABdB1-ngh9XxzP4tD51T_Y9fU7wt7H8";
// var st_url_csv = "http://52.74.240.184/graphics/st-get-file-csv/1LWJVov1y1pIxABdB1-ngh9XxzP4tD51T_Y9fU7wt7H8";
// var st_url_csv = "http://graphics.straitstimes.com/st-get-file-csv/1LWJVov1y1pIxABdB1-ngh9XxzP4tD51T_Y9fU7wt7H8";
var st_url_csv = "csv/BT-wine.csv";


var template_card = "<div class='item-card item-card-unit [ST-CLASS-COLOR]' data-order='[ST-ORDER]' data-name_wine=\"[ST-DATA-NAME-WINE]\" data-award='[ST-AWARD]' data-price='[ST-PRICE]' data-country='[ST-COUNTRY]' data-grape='[ST-GRAPE]' data-vintage='[ST-VINTAGE]'  >";
template_card += "<div class='[ST-CLASS-CARD-BACK]' >";
template_card += "<p>[ST-NAME-WINE]</p>";
template_card += "</div></div>";

var template_option = "<option value='[ST-NAME-VALUE]'>[ST-NAME-DISPLAY]</option>";


info.t_content_item_card = "<div>";
info.t_content_item_card += "<div class='row-content' >";

info.t_content_item_card += "<div class='row-left'>";
info.t_content_item_card += "<img class='st-image-responsive' src='images/wines/[ST-NAME-IMAGE]' alt='' />";
info.t_content_item_card += "</div>";



info.t_content_item_card += "<div class='row-right' >";
info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-title'>[ST-WINE-NAME]</div>";
info.t_content_item_card += "</div>";


info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>DESCRIPTION</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-DESCRIPTION]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>AWARD</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-AWARD]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>PRICE</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-PRICE]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>COUNTRY</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-COUNTRY]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>GRAPE</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-GRAPE]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>VINTAGE</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-VINTAGE]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-label'>DISTRIBUTOR</div>";
info.t_content_item_card += "<div class='row-description'>[ST-ROW-DISTRIBUTOR]</div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "</div>";


info.t_content_item_card += "</div>";
    /*button close*/
info.t_content_item_card += "<div class='btn-close-content-card'><img src='images/close.svg' class='st-img-responsive' /></div>";
    /*end button close*/
info.t_content_item_card += "</div>";


/*configuration for event click card*/
info.number_columns = 0;
info.last_click_card = 0;
info.count_clicks_card = 0;
info.cache_order = [];




/*array of selects*/
var a_award = [];
var a_country = [];
var a_grape = [];
var a_vintage = [];

/*end of array selects*/
$(".select-award").select2({
    placeholder: "AWARD",
    allowClear: true,
    width: "100%",
    minimumResultsForSearch: Infinity
}).on("change", function(event) {
    // $(".list_candidate_select").val("");
    info.f_filter_elements();
    info.f_clear_select2_icon(this);
});

$(".select-price").select2({
    placeholder: "PRICE",
    allowClear: true,
    width: "100%",
    minimumResultsForSearch: Infinity
}).on("change", function(event) {
    // $(".list_candidate_select").val("");
    info.f_filter_elements();
    info.f_clear_select2_icon(this);
});
$(".select-country").select2({
    placeholder: "COUNTRY",
    allowClear: true,
    width: "100%",
    minimumResultsForSearch: Infinity,
    sorter: function(data) {
        return info.order_ascendent_by_id(data);
    }
}).on("change", function(event) {
    // $(".list_candidate_select").val("");
    info.f_filter_elements();
    info.f_clear_select2_icon(this);
});
$(".select-grape").select2({
    placeholder: "GRAPE",
    allowClear: true,
    width: "100%",
    minimumResultsForSearch: Infinity,
    sorter: function(data) {
        return info.order_ascendent_by_id(data);
    }
}).on("change", function(event) {
    // $(".list_candidate_select").val("");
    info.f_filter_elements();
    info.f_clear_select2_icon(this);
});
$(".select-vintage").select2({
    placeholder: "VINTAGE",
    allowClear: true,
    width: "100%",
    minimumResultsForSearch: Infinity,
    sorter: function(data) {
        return info.order_descendent_by_id(data);
    }
}).on("change", function(event) {
    // $(".list_candidate_select").val("");
    info.f_filter_elements();
    info.f_clear_select2_icon(this);
});

// var ta = Typeahead($(".typehead-search-wine"), {
//     source: []
// });



d3.csv(st_url_csv, function(error, csv_data) {
    if (!error) {
        main(csv_data);
    }
});

var colours = {
    Red: "red-wine",
    White: "white-wine"
};

$("#tabs-graphic a").on('click', function(e) {
    e.preventDefault();
    $(this).tab('show');
    var id_href = $(this).attr("href");
    $(".active-tab").removeClass("active-tab");
    $(id_href).addClass('active-tab');
});

$("#content-cards").on("click", '.btn-close-content-card', function() {
    info.f_clear_content_item_card_with_animation();
});

$(window).on('resize', function() {
    if (info.detect_device() === false) {
        info.last_click_card = 0;
        if ($("#content-cards .content-item-card").length > 0) {
            info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
        }

        $(".item-card-unit").removeClass("item-active");
        info.number_columns = parseInt(($("#content-cards").outerWidth() + 10) / ($(".item-card").outerWidth() + 10));

        var min_height_card = $(".item-card").outerWidth();
        $(".item-card").css('min-height', min_height_card);
    }
});
window.addEventListener("orientationchange", function() {
    if (info.detect_device() === true) {
        info.last_click_card = 0;
        if ($("#content-cards .content-item-card").length > 0) {
            info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
        }
        $(".item-card-unit").removeClass("item-active");
        info.number_columns = parseInt(($("#content-cards").outerWidth() + 10) / ($(".item-card").outerWidth() + 10));

        var min_height_card = $(".item-card").outerWidth();
        $(".item-card").css('min-height', min_height_card);
    }
}, false);

function main(csv_data) {

    info.mansonry = $("#content-cards").masonry({
        columnWidth: '.item-card',
        itemSelector: '.item-card',
        isFitWidth: false,
        gutter: 15,
        transitionDuration: "0.5s"
    });

    $(".select-award").html("<option value=''></option>");
    $(".select-country").html("<option value=''></option>");
    $(".select-grape").html("<option value=''></option>");
    $(".select-vintage").html("<option value=''></option>");

    info.data_sort = csv_data;

    info.data_sort.forEach(function(d, order) {
        var d_award = d.Award.trim();
        if ($.inArray(d_award, a_award) === -1) {
            $(".select-award").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_award), 'g').replace('[ST-NAME-DISPLAY]', d_award, 'g'));
            a_award.push(d_award);
        }

        var d_country = d.Country.trim();
        if ($.inArray(d_country, a_country) === -1) {
            $(".select-country").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_country), 'g').replace('[ST-NAME-DISPLAY]', d_country, 'g'));
            a_country.push(d_country);
        }
        var d_grape = d.Grape.trim();
        if ($.inArray(d_grape, a_grape) === -1) {
            $(".select-grape").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_grape), 'g').replace('[ST-NAME-DISPLAY]', d_grape, 'g'));
            a_grape.push(d_grape);
        }
        var d_vintage = d.Vintage.trim();
        if ($.inArray(d_vintage, a_vintage) === -1) {
            $(".select-vintage").append(template_option.replace('[ST-NAME-VALUE]', d_vintage, 'g').replace('[ST-NAME-DISPLAY]', d_vintage, 'g'));
            a_vintage.push(d_vintage);
        }

        var new_card = template_card
            .replace('[ST-ORDER]', (order + 1), 'g')
            .replace("[ST-CLASS-COLOR]", colours[d.Colour.trim()], "g")
            .replace("[ST-AWARD]", info.change_space_underscore(d_award), 'g')
            .replace("[ST-PRICE]", d.Price.trim(), 'g')
            .replace("[ST-COUNTRY]", info.change_space_underscore(d_country), 'g')
            .replace("[ST-GRAPE]", info.change_space_underscore(d_grape), 'g')
            .replace("[ST-VINTAGE]", d_vintage, 'g')
            .replace("[ST-CLASS-CARD-BACK]", "item-card-back-" + d.img.trim(), 'g')
            .replace("[ST-DATA-NAME-WINE]", d.Name.trim(), "g")
            .replace("[ST-NAME-WINE]", d.Name.trim(), "g");

        $(".save-copy-card-1").append(new_card);
        $("#content-cards").append(new_card);
        info.cache_order.push(order + 1);
    });


    info.mansonry.masonry('reloadItems');
    info.mansonry.masonry("layout");

    info.mansonry.on("layoutComplete", function(event, item) {
        info.number_columns = parseInt(($("#content-cards").outerWidth() + 15) / ($(".item-card").outerWidth() + 10));
        if (info.flag_order_cards === 0) {
            info.flag_order_cards = 1;
            var set_time = setTimeout(function() {
                clearTimeout(set_time);
                info.mansonry.masonry("layout");
            }, 500);
        }

    });

    info.mansonry.on("removeComplete", function(event, item) {
        if (info.time_delete_contentcard === 0) {
            info.time_delete_contentcard = 500;
            info.mansonry.masonry('reloadItems');
            info.mansonry.masonry("layout");
        } else {
            var set_time = setTimeout(function() {
                clearTimeout(set_time);
                info.mansonry.masonry('reloadItems');
                info.mansonry.masonry("layout");
            }, 500);
        }
    });

    $("#content-cards").on("click", ".item-card-unit", function() {
        info.count_clicks_card++;

        var position_current_element = $("#content-cards .item-card-unit").index(this) + 1;
        var row_appear = Math.ceil(position_current_element / info.number_columns) * info.number_columns;

        var style_content_item_card = "style='position:absolute;top:" + $(this).css('top') + "'";

        if (info.last_click_card !== row_appear) {
            if (row_appear > info.last_click_card) {
                var offset_top2 = $(window).scrollTop() - $(".content-item-card").outerHeight();
                $("body,html").scrollTop(offset_top2);
            }
            info.f_clear_content_item_card();



            var $div_item = $("#content-cards .item-card-unit").eq(row_appear - 1);
            var content_card = "<div class='item-card content-item-card' " + style_content_item_card + "></div>";
            if ($div_item.length > 0) {
                $(content_card).insertAfter($div_item);
            } else {
                $("#content-cards").append(content_card);
            }
        }

        var offset_top = $(this).offset().top;
        $("body,html").animate({
            scrollTop: offset_top - $(".padding-offset").outerHeight()
        }, 500);

        var st_name_wine = $(this).data('name_wine');
        info.data_sort.forEach(function(d) {
            if (d.Name.trim() === st_name_wine) {
                info.f_add_content_item_card(d);
            }
        });


        info.last_click_card = row_appear;

        $(".item-card-unit").removeClass("item-active");
        $(this).addClass("item-active");
    });
}

info.order_ascendent_by_id = function(a_o_data) {
    return a_o_data.sort(function(a, b) {
        return d3.ascending(a.id, b.id);
    });
};

info.order_descendent_by_id = function(a_o_data) {
    return a_o_data.sort(function(a, b) {
        return d3.descending(a.id, b.id);
    });
};

info.f_add_content_item_card = function(obj_content_card) {
    console.log(obj_content_card);
    var number_image = (parseInt(obj_content_card.img) < 10) ? (0 + obj_content_card.img) : obj_content_card.img;
    var content_item_card_html = info.t_content_item_card
        .replace("[ST-WINE-NAME]", obj_content_card.Name.trim(), "g")
        .replace("[ST-NAME-IMAGE]", "img" + number_image + ".png", 'g')
        .replace("[ST-ROW-DESCRIPTION]", obj_content_card.Description.trim(), 'g')
        .replace("[ST-ROW-AWARD]", obj_content_card.Award.trim(), 'g')
        .replace("[ST-ROW-PRICE]", obj_content_card.Price.trim(), 'g')
        .replace("[ST-ROW-COUNTRY]", obj_content_card.Country.trim(), 'g')
        .replace("[ST-ROW-GRAPE]", obj_content_card.Grape.trim(), 'g')
        .replace("[ST-ROW-VINTAGE]", obj_content_card.Vintage.trim(), 'g')
        .replace("[ST-ROW-COLOUR]", obj_content_card.Colour.trim(), 'g')
        .replace("[ST-ROW-DISTRIBUTOR]", obj_content_card.Distributor.trim(), 'g');

    $(".content-item-card").html(content_item_card_html);
    info.mansonry.masonry('reloadItems');
    info.mansonry.masonry("layout");
};

info.f_clear_content_item_card = function() {
    info.time_delete_contentcard = 0;
    if ($("#content-cards .content-item-card").length > 0) {
        info.last_click_card = 0;
        info.mansonry.masonry({
            transitionDuration: "0s"
        });
        info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
        info.mansonry.masonry({
            transitionDuration: "0.5s"
        });
    }
    $(".item-card-unit").removeClass("item-active");
};

info.f_clear_content_item_card_with_animation = function() {
    if ($("#content-cards .content-item-card").length > 0) {
        info.last_click_card = 0;
        info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    }
    $(".item-card-unit").removeClass("item-active");
};

info.f_clear_select2_icon = function(this_select) {
    var val_select = $(this_select).val();
    var parent_select = $(this_select).parent();
    if (val_select) {
        parent_select.find(".select2-selection").addClass("background_select");
        parent_select.find(".select2-selection__arrow").addClass("disappear_arrow");
    } else {
        parent_select.find(".select2-selection").removeClass("background_select");
        parent_select.find(".select2-selection__arrow").removeClass("disappear_arrow");
    }
};


info.f_filter_elements = function() {
    info.last_click_card = 0;
    info.flag_order_cards = 0;
    info.copy_elements = $(".save-copy-card-1").clone();


    $(".item-card-unit").removeClass("item-active");
    var elements_to_display = info.copy_elements.find(".item-card");


    var select_award = $(".select-award").select2('val');
    if (select_award) {
        elements_to_display = elements_to_display.filter("[data-award='" + select_award + "']");
    }

    var select_price = parseInt($(".select-price").select2('val'));
    if (select_price) {
        elements_to_display = elements_to_display.filter(function(index) {
            var r_ = false;
            if (select_price === 1 && parseFloat($(this).data('price')) < 80) {
                r_ = true;
            } else if (select_price === 2 && parseFloat($(this).data('price')) >= 80 && parseFloat($(this).data('price')) < 100) {
                r_ = true;
            } else if (select_price === 3 && parseFloat($(this).data('price')) >= 100) {
                r_ = true;
            }
            return r_;
        });
    }


    var select_country = $(".select-country").select2('val');
    if (select_country) {
        elements_to_display = elements_to_display.filter("[data-country='" + select_country + "']");
    }
    var select_grape = $(".select-grape").select2('val');
    if (select_grape) {
        elements_to_display = elements_to_display.filter("[data-grape='" + select_grape + "']");
    }

    var select_vintage = $(".select-vintage").select2('val');
    if (select_vintage) {
        elements_to_display = elements_to_display.filter("[data-vintage='" + select_vintage + "']");
    }






    $("#content-cards .item-card").addClass("target-delete-items");


    info.go_over_cards(elements_to_display);


    if ($(".target-delete-items").length > 0) info.mansonry.masonry("remove", $(".target-delete-items"));

    info.mansonry.masonry('reloadItems');
    info.mansonry.masonry("layout");
};

info.go_over_cards = function(elements_to_display) {
    var a_new_cache_order = [];
    elements_to_display.each(function(i) {
        a_new_cache_order.push($(this).data("order"));
        var new_order = $(this).data("order");

        if ($.inArray(new_order, info.cache_order) == -1) {
            var div_to_add = this;
            // add new card
            var last_min = 0;
            var capture_last_min;
            if ($("#content-cards .item-card").length > 0) {
                $("#content-cards .item-card").each(function() {
                    old_order = $(this).data("order");
                    if (new_order > old_order) {
                        capture_last_min = this;
                    }
                });
                if (capture_last_min) {
                    $(div_to_add).insertAfter(capture_last_min);
                } else {
                    $("#content-cards").prepend(div_to_add);
                }
            } else {
                $("#content-cards").append(div_to_add);
            }
        } else {
            $("#content-cards .item-card").filter("[data-order=" + new_order + "]").removeClass("target-delete-items");
        }
    });
    info.cache_order = a_new_cache_order;
};

info.change_space_underscore = function(element) {
    return element.split(" ").join("_");
};
info.detect_device = function() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
};
if (info.detect_device()) {
    $(".st-header-hero").hide();
}
