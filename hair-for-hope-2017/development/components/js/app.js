// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";

import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];
//
// a_test.forEach(d => {
//   console.log(d);
// });

import gsap from 'gsap';
import setTween from "./libraries/plugins/animation.gsap";
import ScrollMagic from "scrollmagic";

function getNo() {

  var numof = $("div[id^='scrollfade']").length;

  for (var i = 1; i <= numof; i++) {
    scrollfade(i);
  }

}

function scrollfade(i) {

var scrollControl = new ScrollMagic.Controller();

var hideAni = TweenMax.to('.second'+i, 1, {autoAlpha: 0});

// scene used to pin the container
var pinScene = new ScrollMagic.Scene({
  triggerHook: 0.01,
  triggerElement: '#scrollfade'+i,
  duration: 800
  })
.setPin('#scrollfade'+i)
.addTo(scrollControl);

// scene used to fade the images
var aniScene = new ScrollMagic.Scene({
  triggerHook: 0.01,
  triggerElement: '#scrollfade'+i,
  offset: 0,
  duration: 800
  })
.setTween(hideAni)
.addTo(scrollControl);

}

  getNo();
