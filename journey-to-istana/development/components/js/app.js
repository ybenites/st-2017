// require("../precss/app.css");

import $ from "jquery";
// import xx from "./library/fix-svg-size";
import * as d3 from "d3";
// import fullpage from "fullpage.js";
import "./libraries/jquery.fullPage";

import makeVideoPlayableInline from "iphone-inline-video";
import swipe from "jquery-touchswipe";
// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];

// a_test.forEach(d => {
//   console.log(d);
// });
let isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
  /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

let htmlcontent, screenWidth = $(window).width();
let f_click = 0,
  a_videos = [];

let ua = navigator.userAgent.toLowerCase();
let isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");

let totalSection = 0;
$(function() {
  d3.csv("data/journey-to-istana.csv", function(data) {

    data.forEach(function(d, i) {
      i++;
      totalSection = i;
      d.content = d.content;

      if (d.video == '') {
        d.img_desktop = "images/journey-to-istana/img/" + d.img_desktop + ".jpg";
        d.img_mobile = "images/journey-to-istana/img/" + d.img_mobile + ".jpg";
        htmlcontent = '<div class="section" id="section' + i + '"><div class="text-bg ' + d.text_position + '">' + d.content + '</div></div>';

      } else {
        if (isAndroid) d.video = '<video data-autoplay playsinline loop muted class="myVideo" id="v' + i + '" poster=""><source src="videos/' + d.video + '.mp4" type="video/mp4"></video>';
        else d.video = '<video data-autoplay playsinline loop class="myVideo" id="v' + i + '" poster=""><source src="videos/' + d.video + '.mp4" type="video/mp4"></video>';
        htmlcontent = '<div class="section" id="section' + i + '">' + d.video + '<div class="text-bg ' + d.text_position + '">' + d.content + '</div></div>';

      }
      $('#fullpage').append(htmlcontent);

      if (screenWidth <= 769) $("#section" + i).css("background-image", "url(" + d.img_mobile + ")");
      else $("#section" + i).css("background-image", "url(" + d.img_desktop + ")");


    });
    totalSection++;
    fullpage_main(totalSection);
    unmute();
  });
});

function unmute() {
  $("#section1").swipe({
    //Generic swipe handler for all directions
    swipeUp: function(event, direction, distance, duration, fingerCount) {
      if (isAndroid) {
        $('.section video').each(function() {
          if (this.muted === true) this.muted = 0;
        });
      }
    }
  });
}

function fullpage_main(totalSection) {
  if (isMobile) {
    $(window).on("touchstart click", function(e) {
      if (f_click === 0 && a_videos.length > 0) {
        f_click = 1;
        a_videos.forEach(function(v) {
          v.play();
          v.pause();
        });
      }
    });
  }
  $('#fullpage').fullpage({
    afterRender: function() {
      $('video').each(function() {
        makeVideoPlayableInline(this);
        a_videos.push(this);
      });
    },
    afterLoad: function(anchorLink, index) {
      let f_video = $(this).find('video');
      if (f_video.length > 0) {
        f_video[0].play();
      }
      $(this).find('.text-bg').animate({
        bottom: 0
      }, 500);
    },
    onLeave: function(index, nextIndex, direction) {
      // processing bar
      if (nextIndex > 1 && nextIndex < totalSection + 2 && direction == 'down') {
        $('.bar-long').css('background-color', '#5A9DDC');
        $('.bar-long').css('width', (100 / totalSection) * (nextIndex - 0) + "%");
      } else if (nextIndex > 0 && nextIndex < totalSection + 1 && direction == 'up') {
        $('.bar-long').css('background-color', '#5A9DDC');
        $('.bar-long').css('width', (100 / totalSection) * (nextIndex - 1) + "%");
      } else if (nextIndex == totalSection + 1 && direction == 'up') {
        $('.bar-long').css('background-color', '#5A9DDC');
        $('.bar-long').css('width', "100%");
      }
    }
  });
}
