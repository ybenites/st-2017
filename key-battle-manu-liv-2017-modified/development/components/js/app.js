// require("../precss/app.css");

import $ from "jquery";

// import svg from "./library/fix-svg-size";
import svg from "./libraries/fix-svg-size";
import animate_svg from "./libraries/lazy-load";

import * as d3 from "d3";


$(".st-header-hero-video").height($(window).height() - $(".st-content-menu").height());
$(function(){
  $(".st-header-hero-video").height($(window).height() - $(".st-content-menu").height());

  svg({
    tag:".content-svg-rd-1"
  });
  svg({
    tag:".content-svg-rd-2"
  });
  svg({
    tag:".content-svg-rd-3"
  });

  animate_svg({
    tag:".animate-1",
    todo:function(el){
      console.log(el);
      d3.select(el).attr('opacity', 0).attr('transform', 'translate(-300,0)').transition().duration(1500).attr('opacity', 1).attr('transform', 'translate(0,0)');
    }
  })

  animate_svg({
    tag:".animate-2",
    todo:function(el){
      console.log(el);
      d3.select(el).attr('opacity', 0).attr('transform', 'translate(300,0)').transition().duration(1500).attr('opacity', 1).attr('transform', 'translate(0,0)');
    }
  })

  animate_svg({
    tag:".animate-3",
    todo:function(el){
      console.log(el);
      d3.select(el).style('opacity', 0).transition().delay(500).duration(1500).style('opacity', 1);
    }
  })

  animate_svg({
    tag:".animate-4",
    todo:function(el){
      console.log(el);
      d3.select(el).style('opacity', 0).transition().delay(500).duration(1500).style('opacity', 1);
    }
  })

  animate_svg({
    tag:".animate-5",
    todo:function(el){
      console.log(el);
      d3.select(el).style('opacity', 0).transition().delay(500).duration(1500).style('opacity', 1);
    }
  })




});

$(window).on("resize",function(){
  $(".st-header-hero-video").height($(window).height() - $(".st-content-menu").height());
});
