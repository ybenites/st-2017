import $ from "jquery";
import * as d3 from "d3";
import lazy_load from './libraries/lazy-load';

import './libraries/modernizr-custom';


$(function() {

  d3.selectAll(".button-1").on("mousedown", function () {

    d3.selectAll(".nr-2,.nr-3,.none-class").transition().duration(500).style("opacity", 0.3);
    d3.selectAll(".nr-1").transition().duration(500).style("opacity", 1);
    d3.selectAll(".button-1").transition().duration(500).style("opacity", 1);
    d3.selectAll(".button-2").transition().duration(500).style("opacity", 0.3);
    d3.selectAll(".button-3").transition().duration(500).style("opacity", 0.3);
      d3.selectAll(".hand").attr("display", 'none')

});

d3.selectAll(".button-2").on("mousedown", function () {

                d3.selectAll(".nr-1,.nr-3,.none-class").transition().duration(500).style("opacity", 0.3);
                d3.selectAll(".nr-2").transition().duration(500).style("opacity", 1);
                d3.selectAll(".button-2").transition().duration(500).style("opacity", 1);
                d3.selectAll(".button-1").transition().duration(500).style("opacity", 0.3);
                d3.selectAll(".button-3").transition().duration(500).style("opacity", 0.3);
                  d3.selectAll(".hand").attr("display", 'none')

  });

  d3.selectAll(".button-3").on("mousedown", function () {

                  d3.selectAll(".nr-1,.nr-2,.none-class").transition().duration(500).style("opacity", 0.3);
                  d3.selectAll(".nr-3").transition().duration(500).style("opacity", 1);
                  d3.selectAll(".button-3").transition().duration(500).style("opacity", 1);
                  d3.selectAll(".button-1").transition().duration(500).style("opacity", 0.3);
                  d3.selectAll(".button-2").transition().duration(500).style("opacity", 0.3);
                    d3.selectAll(".hand").attr("display", 'none')

    });




    d3.selectAll(".hand").transition().each(animateFirstStep3);

      function animateFirstStep3(){
          d3.selectAll(".hand").transition().duration(2000).attr("transform", "translate(400,0)").on("end", animateSecondStep3);
    };

    function animateSecondStep3(){
            d3.selectAll(".hand").transition().duration(2000).attr("transform", "translate(0,0)").on("end", animateFirstStep3);

    };


  //
  // lazy_load({
  //   tag: ' .maplabels',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });
  //
  // lazy_load({
  //   tag: ' .red-animation-1',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#EF4123");
  //   }
  // });
  // lazy_load({
  //   tag: ' .labels-red-1',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });
  // lazy_load({
  //   tag: ' .red-animation-2',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#EF4123");
  //   }
  // });
  // lazy_load({
  //   tag: ' .label-red-2',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });
  // lazy_load({
  //   tag: ' .yellow-animation',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#8799D9");
  //   }
  // });

  if (Modernizr.cssanimations) {
    lazy_load({
      tag: '.time-animated,.readmore',
      todo: function(el) {
        d3.select(el)
          .attr('opacity', 0)
          .transition().duration(1000)
          .delay(1000).attr('opacity', 1);
      }
    });
  }else{
    d3.selectAll(".time-animated,.readmore")
      .attr('opacity', 0)
      .transition().duration(1000)
      .delay(1000).attr('opacity', 1);
  }
  if (Modernizr.cssanimations) {
    lazy_load({
      tag: '.text-fade-in',
      todo: function(el) {
        d3.select(el)
          .attr('opacity', 0)
          .transition().duration(1000)
          .delay(2000).attr('opacity', 1);
      }
    });
  }else{
    d3.selectAll(".text-fade-in")
      .attr('opacity', 0)
      .transition().duration(1000)
      .delay(2000).attr('opacity', 1);
  }

if (Modernizr.cssanimations) {
  lazy_load({
    tag: ".fadeout",
    todo: function(el) {
      d3.select(el)
        .style('opacity', 1)
        .transition().duration(1500)
        .style('opacity', 0.1);
    }
  });
}else{
  $(".fadeout")
    // .style('opacity', 1)
    // .transition().duration(1500)
    .css('opacity', 0.1);
}

  lazy_load({
    tag: ".fadein",
    todo: function(el) {
      d3.select(el)
        .style('opacity', 0)
        .transition().duration(1500)
        .style('opacity', 1);
    }
  });

  lazy_load({
    tag: ".boxes-animated",
    todo: function(el) {
      d3.select(el)
        .attr('opacity', 0)
        .attr('transform', 'translate(0,300)')
        .transition().duration(1500)
        .attr('opacity', 1)
        .attr('transform', 'translate(0,0)');
    }
  });

  if (Modernizr.cssanimations) {
    d3.selectAll(".line-animation path").each(function() {
      var copy_this = this;
      var total_length = copy_this.getTotalLength();
      d3.select(copy_this)
        .attr('stroke-dasharray', total_length)
        .attr('stroke-dashoffset', total_length);
    });

    lazy_load({
      tag: ".flowchart-mobile",
      todo: function(el) {
        d3.selectAll(".flowchart-mobile .line-animation path").transition().duration(3000).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: ".flowchart-desktop",
      todo: function(el) {
        d3.selectAll(".flowchart-desktop .line-animation path").transition().duration(3000).attr('stroke-dashoffset', 0);
      }
    });
  }


});
