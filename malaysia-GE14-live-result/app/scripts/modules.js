export default {
  "artefacs":{
    subtitle:require("artefacts/subtitle.vue"),
    paragraph:require("artefacts/paragraph.vue"),
    bullets:require("artefacts/bullets.vue"),
    "svg-folder": require("artefacts/st-get-svg.vue")
  },
  block:{
    "st-credits":require("../template/main/article/st-credits.vue"),
    "st-next-chapter":require("../template/main/article/next-chapter.vue")
  }
};
