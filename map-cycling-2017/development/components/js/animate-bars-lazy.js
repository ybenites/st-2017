import $ from "jquery";

var d3 = Object.assign({}, require("d3-timer"), require("d3-selection"), require("d3-transition"), require("d3-geo"), require("d3-queue"), require("d3-collection"), require("d3-dispatch"), require("d3-dsv"), require("d3-request"), require("d3-color"), require("d3-ease"), require("d3-interpolate"));

import lazy_load from './libraries/lazy-load';
import './libraries/modernizr-custom';


$(function() {


  lazy_load({
    tag: '.fade-in-1',
    todo: function(el) {
      d3.select(el).transition().duration(2000).attr('opacity', 1);
    }
  });


  lazy_load({
    tag: '.item-1',
    todo: function(el) {
      d3.select(el).style('opacity', 0).transition().duration(1000).style('opacity', 1);
    }
  });

  lazy_load({
    tag: '.item-2',
    todo: function(el) {
      d3.select(el).style('opacity', 0).transition().delay(500).duration(1000).style('opacity', 1);
    }
  });

  lazy_load({
    tag: '.item-3',
    todo: function(el) {
      d3.select(el).style('opacity', 0).transition().delay(1000).duration(1000).style('opacity', 1);
    }
  });

  d3.selectAll(".lines-animation path").each(function() {
      var copy_this = this;
      var total_length = copy_this.getTotalLength();
      d3.select(copy_this)
        .attr('stroke-dasharray', total_length)
        .attr('stroke-dashoffset', total_length);
    });

    d3.selectAll(".lines-2 path").each(function() {
        var copy_this_2 = this;
        var total_length_2 = copy_this_2.getTotalLength();
        d3.select(copy_this_2)
          .attr('stroke-dasharray', total_length_2)
          .attr('stroke-dashoffset', total_length_2);
      });

      d3.selectAll(".lines-22 path").each(function() {
          var copy_this_22 = this;
          var total_length_22 = copy_this_22.getTotalLength();
          d3.select(copy_this_22)
            .attr('stroke-dasharray', total_length_22)
            .attr('stroke-dashoffset', total_length_22);
        });



    lazy_load({
      tag: ".red-line-1",
      todo: function(el) {
        d3.selectAll(".red-line-1").transition().duration(3500).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-1',
      todo: function(el) {
        d3.select(el).transition().delay(1000).duration(100).attr('opacity', 1);
      }
    });

    lazy_load({
      tag: ".blue-line-1",
      todo: function(el) {
        d3.selectAll(".blue-line-1").transition().duration(1400).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-2',
      todo: function(el) {
        d3.select(el).transition().delay(3000).duration(100).attr('opacity', 1);
      }
    });

    lazy_load({
      tag: ".green-line-1",
      todo: function(el) {
        d3.selectAll(".green-line-1").transition().duration(5900).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-3',
      todo: function(el) {
        d3.select(el).transition().delay(5200).duration(100).attr('opacity', 1);
      }
    });



    lazy_load({
      tag: ".red-line-11",
      todo: function(el) {
        d3.selectAll(".red-line-11").transition().duration(4100).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-11',
      todo: function(el) {
        d3.select(el).transition().delay(1500).duration(100).attr('opacity', 1);
      }
    });

    lazy_load({
      tag: ".blue-line-11",
      todo: function(el) {
        d3.selectAll(".blue-line-11").transition().duration(3000).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-21',
      todo: function(el) {
        d3.select(el).transition().delay(2500).duration(100).attr('opacity', 1);
      }
    });

    lazy_load({
      tag: ".green-line-11",
      todo: function(el) {
        d3.selectAll(".green-line-11").transition().duration(3500).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-31',
      todo: function(el) {
        d3.select(el).transition().delay(2000).duration(100).attr('opacity', 1);
      }
    });








    lazy_load({
      tag: ".red-line-2",
      todo: function(el) {
        d3.selectAll(".red-line-2").transition().duration(4800).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-1-1',
      todo: function(el) {
        d3.select(el).transition().delay(2000).duration(100).attr('opacity', 1);
      }
    });

    lazy_load({
      tag: ".blue-line-2",
      todo: function(el) {
        d3.selectAll(".blue-line-2").transition().duration(2400).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-2-1',
      todo: function(el) {
        d3.select(el).transition().delay(4000).duration(100).attr('opacity', 1);
      }
    });

    lazy_load({
      tag: ".green-line-2",
      todo: function(el) {
        d3.selectAll(".green-line-2").transition().duration(5800).attr('stroke-dashoffset', 0);
      }
    });
    lazy_load({
      tag: '.fadein-3-1',
      todo: function(el) {
        d3.select(el).transition().delay(5000).duration(100).attr('opacity', 1);
      }
    });




});
