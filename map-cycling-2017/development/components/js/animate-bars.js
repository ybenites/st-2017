import $ from "jquery";

var d3 = Object.assign({}, require("d3-timer"), require("d3-selection"), require("d3-transition"), require("d3-geo"), require("d3-queue"), require("d3-collection"), require("d3-dispatch"), require("d3-dsv"), require("d3-request"), require("d3-color"), require("d3-ease"), require("d3-interpolate"), require("d3-time"), require("d3-time-format"));

require("./libraries/pulse");
// import * as d3_queue from "";
// import * as topojson from "topojson";


if(!detectIE())document.getElementsByClassName("box-info-ie")[0].style.display="none";
import slider from './libraries/slider';

var myPlayerRoute;
var timeParseT = d3.timeParse("%Y-%m-%dT%H:%M:%SZ");
var points_marker = [];
var time_aceleration = 500;
var dansonRenderer,
    hongRenderer,
    rebeccaRenderer;
var map_leaflet;
var move_blue = 2340;
var move_green = 3000;
var move_red = 3768;
var track_red = 0,
    track_green = 0,
    track_blue = 0;
var time_control_track = 0;
var f_execute_slider;

var a_obj_info_track_rebecca,
    a_obj_info_track_hong,
    a_obj_info_track_danson;

var pulsingIconRed = L.icon.pulse({
    iconSize: [
        9, 9
    ],
    color: '#ef4123'
});
var pulsingIconGreen = L.icon.pulse({
    iconSize: [
        9, 9
    ],
    color: '#f6bd00'
});
var pulsingIconBlue = L.icon.pulse({
    iconSize: [
        9, 9
    ],
    color: '#5a9ddc'
});

$(function() {
    var url_tiles = "https://api.mapbox.com/styles/v1/{user_id}/cixzsyyzb00892sqdda8kdsay/tiles/256/{z}/{x}/{y}?access_token={token}";
    var definition_layer1 = new L.TileLayer(url_tiles, {
        continuousWorld: true,
        // subdomains: "1234",
        token: "pk.eyJ1IjoiY2hhY2hvcGF6b3MiLCJhIjoib2w2VmRlOCJ9.0oe-BndrfdhOBtI-4mzMeQ",
        user_id: "chachopazos",
        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'
    });

    definition_layer1.on("load",function(){
      $(".loading-page").animateCss("fadeOut","out");
    });

    map_leaflet = new L.Map("map-f", {
        center: [
            1.350270, 103.851959
        ],
        zoom: 12,
        renderer: L.svg(),
        maxZoom: 18,
        maxZoom: 14,
    });
    map_leaflet.scrollWheelZoom.disable();
    map_leaflet.addLayer(definition_layer1);
    L.Icon.Default.imagePath = "images/markers/";

    var patternLayer = L.Class.extend({
        initialize: function(map, xml) {
            this._map = map;
            var svgRender = map.getRenderer(map);
            svgRender.options.padding = 100;
            var svg;
            if (navigator.appName === 'Microsoft Internet Explorer') {
                svg = cloneToDoc(xml.documentElement);
            } else {
                svg = document.importNode(xml.documentElement, true);
            }

            var def_e = d3.select(svgRender._container).insert('defs', ':first-child');
            this._el = def_e.append('pattern').attr('id', 'fill_map').attr('patternUnits', 'userSpaceOnUse').attr('viewBox', d3.select(svg).attr('viewBox')).attr('x', 0).attr('y', 0).attr('width', 0).attr('height', 0);

            if (svg.innerHTML !== undefined) {
                this._el.node().innerHTML = svg.innerHTML;
            } else {
                var element = svg.childNodes;
                for (var i = 0; i < element.length; i++) {
                    if (element[i].nodeName != '#text') {
                        this._el.node().appendChild(element[i]);
                    }
                }
            }
        },
        onAdd: function(layer) {
            this._layer = layer;
            this._path = layer.getLayers()[0]._path;
            this._setValuesPattern();
            d3.select(this._path).attr('fill', "url(#fill_map)");
            // add a viewreset event listener for updating layer's position, do the latter
            this._map.on('viewreset', this._reset, this);
            this._map.on('moveend', this._reset, this);
            this._map.on('movestart', this._reset, this);
            // this._map.on('move', this._reset,this);
            this._map.on('zoomlevelschange', this._reset, this);
            this._map.on('zoomend', this._reset, this);
            this._map.on('resize', this._reset, this);
            this._map.on('load',this._reset, this);
            this._reset();
        },
        addTo: function(node_path) {
            this.onAdd(node_path);
        },
        _setValuesPattern: function() {
            var tl = this._layer.getBounds().getNorthWest();
            var tr = this._layer.getBounds().getNorthEast();
            var bl = this._layer.getBounds().getSouthWest();
            var br = this._layer.getBounds().getSouthEast();
            var box_values = {};
            var ptl = this._map.latLngToLayerPoint(tl);
            var ptr = this._map.latLngToLayerPoint(tr);
            var pbl = this._map.latLngToLayerPoint(bl);
            var pbr = this._map.latLngToLayerPoint(br);
            box_values.x = ptl.x;
            box_values.y = ptl.y;
            box_values.width = ptr.x - ptl.x;
            box_values.height = pbl.y - ptl.y;

            // var box_values = this._path.getBBox();
            this._el.attr('x', box_values.x).attr('y', box_values.y).attr('width', box_values.width).attr('height', box_values.height);
        },
        _reset: function() {
            this._setValuesPattern();

            var danson_line_path = d3.select(".danson_line");
            var rebecca_line_path = d3.select(".rebecca_line");
            var hong_line_path = d3.select(".hong_line");
            var time_stroke = setTimeout(function() {
                clearTimeout(time_stroke);
                if (danson_line_path.size() > 0)
                    complete_dashoffset_stroke(danson_line_path, track_red, 0);
                if (rebecca_line_path.size() > 0)
                    complete_dashoffset_stroke(rebecca_line_path, track_green, 0);
                if (hong_line_path.size() > 0)
                    complete_dashoffset_stroke(hong_line_path, track_blue, 0);

                if (points_marker.length > 0) {
                    points_marker.forEach(d => {
                        var length_percentage = d.datum;
                        var point_moment = length_percentage * d.path.node().getTotalLength();
                        var pointPath = d.path.node().getPointAtLength(point_moment);
                        var latlngMap = map_leaflet.layerPointToLatLng(new L.Point(pointPath.x, pointPath.y));
                        d.marker.setLatLng(latlngMap);
                    });
                }

            }, 200);

            // update layer's position
            // var pos = this._map.latLngToLayerPoint(this._latlng);
            // L.DomUtil.setPosition(this._el, pos);
        }
    });

    var q = d3.queue();
    q.defer(d3.json, "geojson/map.geojson");
    q.defer(d3.xml, "geojson/map-base-all.svg");
    q.defer(d3.json, "geojson/danson.geojson");
    q.defer(d3.json, "geojson/rebecca.geojson");
    q.defer(d3.json, "geojson/wei-hong.geojson");
    q.defer(d3.csv, "csv/timing-notes.csv");
    q.awaitAll(function(error, results) {
        if (error)
            throw error;

        // var map_sg = results[0];
        var land = results[0];
        var xml_value = results[1];
        var danson = results[2];
        var rebecca = results[3];
        var hong = results[4];

        var pattern = new patternLayer(map_leaflet, xml_value);

        // var layer_map_sg = L.geoJSON(map_sg, {
        //   style: function(feature) {
        //     return {};
        //   },
        //   className: 'map_sg'
        // }).addTo(map_leaflet);

        var layer_land = L.geoJSON(land, {
            style: function(feature) {
                return {};
            },
            className: 'land'
        }).addTo(map_leaflet);
        pattern.addTo(layer_land);

        var a_points_danson;
        dansonRenderer = L.svg({padding: 100});
        var layer_danson = L.geoJSON(danson);

        var getOT_AP = get_objTrack_and_aPoints(layer_danson);
        a_points_danson = getOT_AP.points;
        a_obj_info_track_danson = getOT_AP.track;

        L.polyline(a_points_danson, {
            noClip: true,
            color: "#ef4123",
            weight: 3,
            opacity: 1,
            fillColor: "#fff",
            fillOpacity: 1,
            radius: 2,
            renderer: dansonRenderer,
            className: 'danson_line'
        }).addTo(map_leaflet);
        var danson_line_path = d3.select(".danson_line");
        complete_dashoffset_stroke(danson_line_path, track_red, 0);

        var a_points_rebecca;
        rebeccaRenderer = L.svg({padding: 100});
        var layer_rebecca = L.geoJSON(rebecca);
        var getOT_AP_r = get_objTrack_and_aPoints(layer_rebecca);
        a_points_rebecca = getOT_AP_r.points;
        a_obj_info_track_rebecca = getOT_AP_r.track;

        L.polyline(a_points_rebecca, {
            noClip: true,
            color: "#f6bd00",
            weight: 3,
            opacity: 1,
            fillColor: "#fff",
            fillOpacity: 1,
            radius: 2,
            renderer: rebeccaRenderer,
            className: 'rebecca_line'
        }).addTo(map_leaflet);
        var rebecca_line_path = d3.select(".rebecca_line");
        complete_dashoffset_stroke(rebecca_line_path, track_green, 0);

        var a_points_hong;
        hongRenderer = L.svg({padding: 100});
        var layer_hong = L.geoJSON(hong);
        var getOT_AP_h = get_objTrack_and_aPoints(layer_hong);
        a_points_hong = getOT_AP_h.points;
        a_obj_info_track_hong = getOT_AP_h.track;

        var ply_blue=L.polyline(a_points_hong, {
            noClip: true,
            color: "#5a9ddc",
            weight: 3,
            opacity: 1,
            fillColor: "#fff",
            fillOpacity: 1,
            radius: 2,
            renderer: hongRenderer,
            className: 'hong_line'
        }).addTo(map_leaflet);
        var hong_line_path = d3.select(".hong_line");
        complete_dashoffset_stroke(hong_line_path, track_blue, 0);

        map_leaflet.setMaxBounds(layer_land.getBounds());
        map_leaflet.fitBounds(ply_blue.getBounds());

        videojs($(".video-route video").get(0)).ready(function() {
          myPlayerRoute = this;
          myPlayerRoute.currentTime(370);
          myPlayerRoute.play();
          d3.timeout(function(){
            myPlayerRoute.pause();
            $(".st-header-hero-video video").get(0).play();
          },5000);


          // var video=$(".st-header-hero-video video").get(0);
          // enableInlineVideo(video);

          // myPlayerRoute.on("timeupdate", function() {
          //   var current_time = Math.floor(this.currentTime());
          //   time_control_track=current_time;
          //   console.log(current_time);
          //   f_execute_slider(time_control_track);
          // });

        });

        var a_text_by_seconds = d3.map(results[5], function(d) {
            drawPointMap(d);
            return d.total_seconds;
        });

        var a_time = [];
        for (var i = 0; i <= move_red; i++) {
            a_time.push(i);
        }

        slider({
            tag: '.st-slider',
            domain: a_time,
            end: function(number) {
                // time_control_track = number;
                update_time_track(time_control_track);

                getPointPath(number);

                // a_obj_info_track_danson
                // update_lines(number, 1000 / time_aceleration);
                f_update_box_text(a_text_by_seconds.get(number));

                if(number<540){
                  $($(".train-fare .content-text-fare div").get(0)).html("$0.00");
                  $($(".train-fare .content-text-fare div").get(1)).html("00:00m");
                }
                if(number>=540 && number<2050){
                  $($(".train-fare .content-text-fare div").get(0)).html("$0.04");
                  $($(".train-fare .content-text-fare div").get(1)).html(getTimeByMinutes(540));
                }
                if(number>=2050){
                  $($(".train-fare .content-text-fare div").get(0)).html("$1.41");
                  $($(".train-fare .content-text-fare div").get(1)).html(getTimeByMinutes(2050));
                }

                if(number<840){
                  $($(".taxi-fare .content-text-fare div").get(0)).html("$0.00");
                  $($(".taxi-fare .content-text-fare div").get(1)).html("00:00m");
                }
                if(number>=840 && number<965){
                  $($(".taxi-fare .content-text-fare div").get(0)).html("$3.30");
                  $($(".taxi-fare .content-text-fare div").get(1)).html(getTimeByMinutes(840));
                }
                if(number>=965 && number<1380){
                  $($(".taxi-fare .content-text-fare div").get(0)).html("$4.30");
                  $($(".taxi-fare .content-text-fare div").get(1)).html(getTimeByMinutes(965));
                }
                if(number>=1380 && number<1680){
                  $($(".taxi-fare .content-text-fare div").get(0)).html("$5.30");
                  $($(".taxi-fare .content-text-fare div").get(1)).html(getTimeByMinutes(1380));
                }
                if(number>=1680 && number<2345){
                  $($(".taxi-fare .content-text-fare div").get(0)).html("$7.80");
                  $($(".taxi-fare .content-text-fare div").get(1)).html(getTimeByMinutes(1680));
                }
                if(number>=2345){
                  $($(".taxi-fare .content-text-fare div").get(0)).html("$25.15");
                  $($(".taxi-fare .content-text-fare div").get(1)).html(getTimeByMinutes(2345));
                }


                var red_time_f=a_obj_info_track_danson[a_obj_info_track_danson.length-1].Tseconds;
                var green_time_f=a_obj_info_track_rebecca[a_obj_info_track_rebecca.length-1].Tseconds;
                var blue_time_f=a_obj_info_track_hong[a_obj_info_track_hong.length-1].Tseconds;
                if(number>=red_time_f){
                  $($(".bike-fare .content-text-fare div").get(0)).html("$0.00");
                  $($(".bike-fare .content-text-fare div").get(1)).html(getTimeByMinutes(red_time_f));
                  if(!$($(".bike-fare .content-text-fare div").get(1)).is(":visible"))$($(".bike-fare .content-text-fare div").get(1)).animateCss("fadeIn","in");
                }else if(number<red_time_f){
                  $($(".bike-fare .content-text-fare div").get(0)).html("$0.00");
                  if($($(".bike-fare .content-text-fare div").get(1)).is(":visible"))$($(".bike-fare .content-text-fare div").get(1)).animateCss("fadeOut","out");
                  $($(".bike-fare .content-text-fare div").get(1)).html("00:00m");
                }

                if(number>=green_time_f){
                  // $($(".train-fare .content-text-fare div").get(0)).html("$0.00");
                  $($(".train-fare .content-text-fare div").get(1)).html(getTimeByMinutes(green_time_f));
                  if(!$($(".train-fare .content-text-fare div").get(1)).is(":visible"))$($(".train-fare .content-text-fare div").get(1)).animateCss("fadeIn","in");
                }else if(number<green_time_f){
                  // $($(".train-fare .content-text-fare div").get(0)).html("$0.00");
                  if($($(".train-fare .content-text-fare div").get(1)).is(":visible"))$($(".train-fare .content-text-fare div").get(1)).animateCss("fadeOut","out");
                  $($(".train-fare .content-text-fare div").get(1)).html("00:00m");
                }

                if(number>=blue_time_f){
                  // $($(".taxi-fare .content-text-fare div").get(0)).html("$0.00");
                  $($(".taxi-fare .content-text-fare div").get(1)).html(getTimeByMinutes(blue_time_f));
                  if(!$($(".taxi-fare .content-text-fare div").get(1)).is(":visible"))$($(".taxi-fare .content-text-fare div").get(1)).animateCss("fadeIn","in");
                }else if(number<blue_time_f){
                  // $($(".taxi-fare .content-text-fare div").get(0)).html("$0.00");
                  if($($(".taxi-fare .content-text-fare div").get(1)).is(":visible"))$($(".taxi-fare .content-text-fare div").get(1)).animateCss("fadeOut","out");
                  $($(".taxi-fare .content-text-fare div").get(1)).html("00:00m");
                }
            },
            moves: function(number) {

                time_control_track = number;
                update_time_track(time_control_track);
                getPointPath(number);
                // update_lines(number, 1000 / time_aceleration);
            },
            exec_f: function(f) {
                f_execute_slider = f;
            }
        });

    });

});

var timeout;
$(function() {
    $(".st-slider").on("click", ".play_track,.icon_track", function(e) {
        e.preventDefault();
        $(".content-big-play").animateCss("fadeOut",'out');
        if (!d3.select(".play_track").classed('playing')) {
            d3.select(".play_track").classed('playing', true);
            change_state_button(0);
        } else {
            d3.select(".play_track").classed('playing', false);
            change_state_button(1);
        }
    });

    $(".st-slider").on("click", ".restart_track", function(e) {
        e.preventDefault();
        // if(!d3.select(".play_track").classed('playing')){
        track_red = 0;
        track_green = 0;
        track_blue = 0;
        time_control_track = 0;
        d3.select(".play_track").classed('playing', true);
        change_state_button(0);
        // }
    });
    $(".content-big-play").on("click",function(){
      $(".play_track").trigger("click");
    });
});

function get_objTrack_and_aPoints(glayer) {
    var total_seconds = 0;
    var total_distance = 0;
    var copy_time,
        copy_layer;

    var a_obj_info_trackR = [];
    var a_points = [];
    glayer.eachLayer(function(layer) {
        var timeFormatedCurrent = timeParseT(layer.feature.properties.Time);
        var current_seconds = 0;
        var current_distance = 0;
        if (copy_time !== undefined) {
            current_seconds = d3.timeSecond.count(copy_time, timeFormatedCurrent);
            current_distance = copy_layer.getLatLng().distanceTo(layer.getLatLng());
        }
        total_seconds += current_seconds;
        total_distance += current_distance;
        a_obj_info_trackR.push({distance: current_distance, Tdistance: total_distance, Tseconds: total_seconds, seconds: current_seconds, latLng: layer.getLatLng()});

        copy_time = timeFormatedCurrent;
        copy_layer = layer;
        a_points.push(layer.getLatLng());
    });
    return {track: a_obj_info_trackR, points: a_points};
}
function getTimeByMinutes(seconds){
  var o_time=getTimeBySeconds(seconds);
  return o_time.hour+":"+o_time.min+"m";
}
function getCurrentTrack(a_obj_info_track, number) {
    if (number === 0)
        return 0;
    var obj_before,
        obj_after;

    var Tdistance = a_obj_info_track[a_obj_info_track.length - 1].Tdistance;
    var Ttime = a_obj_info_track[a_obj_info_track.length - 1].Tseconds;
    if (number >= Ttime)
        return 1;
    a_obj_info_track.forEach((d, i) => {
        if (number > d.Tseconds) {
            obj_before = a_obj_info_track[i];
            obj_after = a_obj_info_track[i + 1];
        }
    });

    var small_left_time = number - obj_before.Tseconds;
    var small_left_distance = (small_left_time / obj_after.seconds) * obj_after.distance;
    var right_distance = small_left_distance + obj_before.Tdistance;
    return right_distance / Tdistance;
}

function getPointPath(number) {

    track_red = getCurrentTrack(a_obj_info_track_danson, number);
    // complete_dashoffset_stroke(d3.select(".danson_line"), track_red, 1000 / time_aceleration);
    complete_dashoffset_stroke(d3.select(".danson_line"), track_red, 0);

    track_green = getCurrentTrack(a_obj_info_track_rebecca, number);
    // complete_dashoffset_stroke(d3.select(".rebecca_line"), track_green, 1000 / time_aceleration);
    complete_dashoffset_stroke(d3.select(".rebecca_line"), track_green, 0);

    track_blue = getCurrentTrack(a_obj_info_track_hong, number);
    // complete_dashoffset_stroke(d3.select(".hong_line"), track_blue, 1000 / time_aceleration);
    complete_dashoffset_stroke(d3.select(".hong_line"), track_blue, 0);

    /*zoom and center map by event*/
    // zoom_center_map(track_red,track_green,track_blue);
}

function zoom_center_map(track_red,track_green,track_blue){
  var node_red=d3.select(".danson_line").node();
  var node_green=d3.select(".rebecca_line").node();
  var node_blue=d3.select(".hong_line").node();

  var red=node_red.getTotalLength()*track_red;
  var green=node_green.getTotalLength()*track_green;
  var blue=node_blue.getTotalLength()*track_blue;

  var point_red=map_leaflet.layerPointToLatLng(node_red.getPointAtLength(red));
  var point_green=map_leaflet.layerPointToLatLng(node_green.getPointAtLength(green));
  var point_blue=map_leaflet.layerPointToLatLng(node_blue.getPointAtLength(blue));

  var bounds = L.latLngBounds(point_red,point_blue);
  map_leaflet.flyToBounds(bounds,{
    padding:[15,15]
  });
  // console.log(map_leaflet.getZoom())
  // map_leaflet.fitBounds(bounds);

}

function drawPointMap(obj_text) {
    var point_moment,
        pointPath,
        latlngMap;
    var total_seconds_at_moment = obj_text.total_seconds;
    var circleMarker_dot,
        length_percentage,
        path;

    if (obj_text.type_motor === "cycle" || obj_text.type_motor === "video" ) {
        var danson_line = d3.select(".danson_line");
        path = danson_line;
        // length_percentage = total_seconds_at_moment / move_red;
        length_percentage = getCurrentTrack(a_obj_info_track_danson, total_seconds_at_moment);
        point_moment = length_percentage * danson_line.node().getTotalLength();
        pointPath = danson_line.node().getPointAtLength(point_moment);
        latlngMap = map_leaflet.layerPointToLatLng(new L.Point(pointPath.x, pointPath.y));

        circleMarker_dot = L.marker(latlngMap, {icon: pulsingIconRed}).addTo(map_leaflet);
        $(circleMarker_dot._icon).addClass("none points_marker points_marker_" + total_seconds_at_moment);

        // circleMarker_dot = L.circleMarker(latlngMap, {
        //   color: "#ef4123",
        //   weight: 1,
        //   opacity: 0,
        //   fillColor: "#ef4123",
        //   fillOpacity: 1,
        //   radius: 3,
        //   className: "points_marker points_marker_" + total_seconds_at_moment,
        //   renderer: dansonRenderer
        // }).addTo(map_leaflet);

    } else if (obj_text.type_motor === "publictransport") {
        var rebecca_line = d3.select(".rebecca_line");
        path = rebecca_line;
        // length_percentage = total_seconds_at_moment / move_green;
        length_percentage = getCurrentTrack(a_obj_info_track_rebecca, total_seconds_at_moment);
        point_moment = length_percentage * rebecca_line.node().getTotalLength();
        pointPath = rebecca_line.node().getPointAtLength(point_moment);
        latlngMap = map_leaflet.layerPointToLatLng(new L.Point(pointPath.x, pointPath.y));

        circleMarker_dot = L.marker(latlngMap, {icon: pulsingIconGreen}).addTo(map_leaflet);
        $(circleMarker_dot._icon).addClass("none points_marker points_marker_" + total_seconds_at_moment);

        // circleMarker_dot = L.circleMarker(latlngMap, {
        //   color: "#f6bd00",
        //   weight: 1,
        //   opacity: 0,
        //   fillColor: "#f6bd00",
        //   fillOpacity: 1,
        //   radius: 2,
        //   className: "points_marker points_marker_" + total_seconds_at_moment,
        //   renderer: rebeccaRenderer
        // }).addTo(map_leaflet);

    } else if (obj_text.type_motor === "taxi") {
        var hong_line = d3.select(".hong_line");
        path = hong_line;
        // length_percentage = total_seconds_at_moment / move_blue;
        length_percentage = getCurrentTrack(a_obj_info_track_hong, total_seconds_at_moment);
        point_moment = length_percentage * hong_line.node().getTotalLength();
        pointPath = hong_line.node().getPointAtLength(point_moment);
        latlngMap = map_leaflet.layerPointToLatLng(new L.Point(pointPath.x, pointPath.y));

        circleMarker_dot = L.marker(latlngMap, {icon: pulsingIconBlue}).addTo(map_leaflet);
        $(circleMarker_dot._icon).addClass("none points_marker points_marker_" + total_seconds_at_moment);

        // circleMarker_dot = L.circleMarker(latlngMap, {
        //   color: "#5a9ddc",
        //   weight: 1,
        //   opacity: 0,
        //   fillColor: "#5a9ddc",
        //   fillOpacity: 1,
        //   radius: 2,
        //   className: "points_marker points_marker_" + total_seconds_at_moment,
        //   renderer: hongRenderer
        // }).addTo(map_leaflet);

    }
    if (circleMarker_dot !== undefined) {
        points_marker.push({datum: length_percentage, path: path, marker: circleMarker_dot, time: total_seconds_at_moment});
    }

    // if (circleMarker_dot_shadow !== undefined) points_marker.push({
    //   datum: length_percentage,
    //   path: path,
    //   marker: circleMarker_dot_shadow
    // });
}

var template_toast = `<div class="text-info">
<div class="content-info-icon">[T-ICON]</div><div class="content-info-text">[T-TEXT]</div>
</div>`;

var time_temp_stop = 0;
function f_update_box_text(obj_text) {
    if (obj_text !== undefined) {
        var point_moment;
        var total_seconds_at_moment = obj_text.total_seconds;

        var points_hide = $(".leaflet-marker-pane .points_marker").filter(function() {
            return !d3.select(this).classed("points_marker_" + total_seconds_at_moment);
        });

        $(".leaflet-marker-pane .points_marker_" + total_seconds_at_moment).removeClass('none');
        points_hide.addClass('none');

        var a_marker_at_moment = points_marker.filter(d => {
            d.marker.unbindTooltip();
            return parseInt(d.time) === parseInt(total_seconds_at_moment);
        });

        if (obj_text.type_motor !== "video") {
            time_temp_stop = 1;

            var new_str,
                m_tooltip;
            if (obj_text.type_motor === "cycle") {
                new_str = template_toast.replace('[T-ICON]', '<img class="st-image-responsive" src="images/cycling/bike-icon.svg" />', 'g').replace('[T-TEXT]', obj_text.type_event_emoji, 'g');
                // new_str = $(new_str).addClass('points_marker points_marker_' + total_seconds_at_moment);

                // $('.content-text-info').append(new_str);

                  m_tooltip = a_marker_at_moment[0].marker.bindTooltip($(new_str)[0]);
                  m_tooltip._tooltip.options.permanent = true;
                  m_tooltip._tooltip.options.offset=L.point(8, 0);
                  m_tooltip._tooltip.options.className = "type_cycle";

            } else if (obj_text.type_motor === "publictransport") {
                // if (d3.selectAll('.text-info').size() >= 3) $(".text-info").get(0).remove();
                new_str = template_toast.replace('[T-ICON]', '<img class="st-image-responsive" src="images/cycling/public-icon.svg" />', 'g').replace('[T-TEXT]', obj_text.type_event_emoji, 'g');
                // new_str = $(new_str).addClass('type_publictransport points_marker_' + total_seconds_at_moment);

                // $('.content-text-info').append(new_str);
                m_tooltip = a_marker_at_moment[0].marker.bindTooltip($(new_str)[0]);
                m_tooltip._tooltip.options.permanent = true;
                m_tooltip._tooltip.options.offset=L.point(8, 0);
                m_tooltip._tooltip.options.className = "type_publictransport";
            } else if (obj_text.type_motor === "taxi") {
                // if (d3.selectAll('.text-info').size() >= 3) $(".text-info").get(0).remove();
                new_str = template_toast.replace('[T-ICON]', '<img class="st-image-responsive" src="images/cycling/car-icon.svg" />', 'g').replace('[T-TEXT]', obj_text.type_event_emoji, 'g');
                // new_str = $(new_str).addClass('type_taxi points_marker_' + total_seconds_at_moment);

                // $('.content-text-info').append(new_str);
                m_tooltip = a_marker_at_moment[0].marker.bindTooltip($(new_str)[0]);
                m_tooltip._tooltip.options.permanent = true;
                m_tooltip._tooltip.options.offset=L.point(8,0);
                m_tooltip._tooltip.options.className = "type_taxi";
            }

            d3.timeout(() => {
                if(m_tooltip!==undefined)m_tooltip.openTooltip();
            }, 250);

            d3.timeout(() => {
                time_temp_stop = 0;
                a_marker_at_moment[0].marker.unbindTooltip();
                $(".leaflet-marker-pane .points_marker_" + total_seconds_at_moment).addClass('none');
            }, 3000);
        }
        if(obj_text.type_motor === "video") {
            time_temp_stop = 1;
            if(myPlayerRoute!==undefined){
              myPlayerRoute.currentTime(total_seconds_at_moment - 4);
              myPlayerRoute.play();
            }

            $(".text-bike-video").html(obj_text.type_event_emoji);
            $(".content-video-route").removeClass('none').animateCss('fadeIn');
            d3.timeout(() => {
                time_temp_stop = 0;
                if(myPlayerRoute!==undefined)myPlayerRoute.pause();
                $(".content-video-route").animateCss('fadeOut','out');
                $(".leaflet-marker-pane .points_marker_" + total_seconds_at_moment).addClass('none');
                d3.timeout(() => {
                    $(".text-bike-video").html("");
                }, 1000);
            }, 8000);
        }


    }
}

function f_update_box_text2(obj_text) {
    if (obj_text !== undefined) {
        var point_moment;
        var total_seconds_at_moment = obj_text.total_seconds;

        if (obj_text.type_motor !== "video") {
            if ($(".content-text-info").hasClass('none'))
                $(".content-text-info").removeClass('none').animateCss('fadeIn');
            time_temp_stop = 1;
            var stop_few_seconds = setTimeout(function() {
                clearTimeout(stop_few_seconds);
                time_temp_stop = 0;
            }, 2000);

            if (d3.selectAll('.text-info').size() >= 3) {
                let elem_remove = $(".text-info").get(0);
                let old_classes = d3.select(elem_remove).attr('class');
                let class_elem = d3.select(elem_remove).classed('type_taxi', false).classed('type_publictransport', false).classed('type_cycle', false).classed('points_marker', false).classed('text-info', false).attr('class');
                d3.select(elem_remove).attr('class', old_classes);

                $(elem_remove).animateCss('slideOutUp');
                $(".leaflet-marker-pane ." + class_elem).animateCss('fadeOut');
                var time_fade = setTimeout(function() {
                    clearTimeout(time_fade);
                    $(elem_remove).remove();
                    $(".leaflet-marker-pane ." + class_elem).addClass('none');
                }, 750);
            }
            var new_str;
            if (obj_text.type_motor === "cycle") {
                new_str = template_toast.replace('[T-ICON]', '<img class="st-image-responsive" src="images/cycling/bike-icon.svg" />', 'g').replace('[T-TEXT]', obj_text.type_event, 'g');
                new_str = $(new_str).addClass('none type_cycle points_marker points_marker_' + total_seconds_at_moment);

                $('.content-text-info').append(new_str);

            } else if (obj_text.type_motor === "publictransport") {
                // if (d3.selectAll('.text-info').size() >= 3) $(".text-info").get(0).remove();
                new_str = template_toast.replace('[T-ICON]', '<img class="st-image-responsive" src="images/cycling/public-icon.svg" />', 'g').replace('[T-TEXT]', obj_text.type_event, 'g');
                new_str = $(new_str).addClass('none type_publictransport points_marker_' + total_seconds_at_moment);
                $('.content-text-info').append(new_str);

            } else if (obj_text.type_motor === "taxi") {
                // if (d3.selectAll('.text-info').size() >= 3) $(".text-info").get(0).remove();
                new_str = template_toast.replace('[T-ICON]', '<img class="st-image-responsive" src="images/cycling/car-icon.svg" />', 'g').replace('[T-TEXT]', obj_text.type_event, 'g');
                new_str = $(new_str).addClass('none type_taxi points_marker_' + total_seconds_at_moment);
                $('.content-text-info').append(new_str);
            }

            var time_show = 0;
            if (d3.selectAll('.text-info').size() >= 3)
                time_show = 1000;
            var exe = setTimeout(function() {
                clearTimeout(exe);
                new_str.removeClass('none').animateCss('slideInUp');
                $(".leaflet-marker-pane .points_marker_" + total_seconds_at_moment).removeClass('none').animateCss('fadeIn');
            }, time_show);
        } else {}

    }

}

function change_state_button(state_track) {
    var i_play = "M11,10 L17,10 17,26 11,26 M20,10 L26,10 26,26 20,26";
    var i_pause = "M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28";
    if (state_track === 0) {
        d3.select(".icon_track").transition().duration(300).attr('d', i_play);
        // timeout=setInterval(time_exec,1);

        timeout=d3.timer(time_exec);

    } else if (state_track === 1) {
        d3.select(".icon_track").transition().duration(300).attr('d', i_pause);

        timeout.stop();
        // clearTimeout(timeout)
        // clearInterval(timeout);
    }
}

function complete_dashoffset_stroke(tag, percentage_track, time) {
    var length_line = tag.node().getTotalLength();
    var t = d3.transition().duration((time !== undefined)
        ? time
        : 0).ease(d3.easeLinear);

    tag
    // .transition(t)
    .attr("stroke-dasharray", length_line).attr('stroke-dashoffset', length_line - (length_line * percentage_track));
}

function time_exec() {
    if (time_control_track >= move_red) {
        d3.select(".play_track").classed('playing', false);
        change_state_button(1);
        // $(".play_track").trigger('click');
        return false;
    }
    if (timeout !== undefined){
      // timeout.stop();
      // clearTimeout(timeout);
      // clearInterval(timeout);
    }

    if (time_temp_stop === 0) {
        time_control_track++;
        f_execute_slider(time_control_track);
    }
    // timeout = d3.timeout(time_exec, (1000 - Date.now() % 1000) / time_aceleration);
    // timeout=setTimeout(function(){
    //   clearTimeout(timeout)
    //   time_exec();
    // },10);

}

function update_time_track(time_track) {
    d3.select(".time_track").datum(time_track).text(function(total_sec) {
        let obj_time = getTimeBySeconds(total_sec);
        return `${obj_time.hour}:${obj_time.min}:${obj_time.sec}`;
    });
}

function getTimeBySeconds(total_sec) {
    let sec,
        min,
        hour = 0;
    sec = (total_sec >= 60)
        ? total_sec % 60
        : total_sec;
    sec = sec < 10
        ? `0${sec}`
        : sec;

    let total_min = Math.floor(total_sec / 60);
    min = (total_min >= 60)
        ? total_min % 60
        : total_min;
    min = min < 10
        ? `0${min}`
        : min;

    let total_hours = Math.floor(total_sec / 3600);
    hour = total_hours < 10
        ? `0${total_hours}`
        : total_hours;
    return {sec: sec, min: min, hour: hour};
}

function cloneToDoc(node, doc) {
    if (!doc)
        doc = document;
    var clone = doc.createElementNS(node.namespaceURI, node.nodeName);
    for (var i = 0, len = node.attributes.length; i < len; ++i) {
        var a = node.attributes[i];
        if (/^xmlns\b/.test(a.nodeName))
            continue; // IE can't create these
        clone.setAttributeNS(a.namespaceURI, a.nodeName, a.nodeValue);
    }
    for (var ii = 0, len2 = node.childNodes.length; ii < len2; ++ii) {
        var c = node.childNodes[ii];
        clone.insertBefore(c.nodeType == 1
            ? cloneToDoc(c, doc)
            : doc.createTextNode(c.nodeValue), null);
    }
    return clone;
}
function detectIE() {
    var ua = window.navigator.userAgent;
    var ie = ua.search(/(MSIE|Trident|Edge)/);

    return ie > -1;
}
$.fn.extend({
    animateCss: function(animationName,type) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        var copy_this = this;
        if(type!==undefined){
          if(type==='in'){
            d3.timeout(function () {
              copy_this.removeClass('none');
            }, 100);
          }
        }
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            copy_this.off();
            if(type!==undefined){
              if(type==='out')copy_this.addClass("none");
            }
            copy_this.removeClass('animated ' + animationName);
        });
    }
});
