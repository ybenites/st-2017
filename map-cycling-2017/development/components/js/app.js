import $ from "jquery";

var d3 = Object.assign({}, require("d3-selection"), require("d3-transition"));

import * as L from "leaflet";
import fix_svg from "./libraries/fix-svg-size";
import './libraries/iphone-inline-video';

$(function() {
  fix_svg({tag: '.ct-svg-time-spent'});
    fix_svg({tag: '.ct-svg-time-spent-2'});

    fix_svg({tag: '.ct-svg-clementi'});
    fix_svg({tag: '.ct-svg-bukit-batok'});
});

$(function(){
  var video=$(".st-header-hero-video video").get(0);
  enableInlineVideo(video);


});

// $(function() {
//     window.onload = function() {
//         cartodb.createVis('map-carto', 'https://straitstimes.cartodb.com/api/v2/viz/b6b97b5e-ded5-11e6-a070-0e98b61680bf/viz.json', {tiles_loader: true});
//         // .done(function(vis,layers){
//         //   console.log(layers);
//         // });
//     }
// });
