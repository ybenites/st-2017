import $ from 'jquery';

import * as d3 from "d3";

var create_slider = function(options) {
  var x_interval;
  var width2;
  var values_domain;
  var last_value;
  var handle, brush, g_hand_mov, track_inset_left;
  var c_options;
  var path_color;
  var text_mark;
  var flag_click_svg = 0;
  var d3_drag, slider;
  if (options) {
    if (options.tag !== undefined) {
      c_options = options;
      var width_g = $(options.tag).width();

      var margin2 = {
        top: 43,
        right: 10,
        bottom: 35,
        left: 10
      };
      width2 = width_g - margin2.left - margin2.right;
      var height = 80 - margin2.bottom - margin2.top;


      var domain = [0, 100];
      if (options.domain) domain = options.domain;
      values_domain = domain;

      x_interval = d3.scalePoint()
        .domain(domain)
        .range([0, width2]);

      d3_drag = d3.drag()
        .on("start.interrupt", function() {
          slider.interrupt();
        })
        .on("start drag", brushed)
        .on('end', brushended);

      options.exec_f(brushended);

      var svg = d3.select(options.tag).append("svg")
        .attr("width", width2 + margin2.left + margin2.right)
        .attr("height", height + margin2.top + margin2.bottom);
      slider = svg.append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

      var track = slider.append("line")
        .attr("class", "track")
        .attr("x1", x_interval.range()[0])
        .attr("x2", x_interval.range()[1]);

      var track_inset = track.select(function() {
          return this.parentNode.appendChild(this.cloneNode(true));
        })
        .attr("class", "track-inset");

      track_inset_left = slider.append("line")
        .attr("class", "track-inset-left")
        .attr("x1", x_interval.range()[0])
        .attr("x2", x_interval.range()[0]);
      // .attr("x2", x_interval.range()[1]);

      var track_overlay = track_inset.select(function() {
          return this.parentNode.appendChild(this.cloneNode(true));
        })
        .attr("class", "track-overlay")
        .attr("x1", x_interval.range()[0] - 10)
        .attr("x2", x_interval.range()[1] + 10)
        .call(d3_drag);


      var axis = d3.axisBottom(x_interval);
      // .tickValues(x_interval.domain().filter(function(d, i) {
      //   return !(i % 2);
      // }));

      var format_axis_m = d3.timeFormat("%b %d");
      var format_axis_y = d3.timeFormat("%Y");

      var eje_x_s = slider.insert("g", ".track-overlay")
        .attr("class", "ticks")
        .attr("transform", "translate(0," + height / 2 + ")");

      var step = Math.ceil(44 / x_interval.step());
      // last_value = domain[domain.length - 1];
      last_value = domain[0];

      eje_x_s.append('text').attr('class', 'time_track').attr('y', -27).datum(0).text("00:00:00");
      eje_x_s.append('text').attr('class', 'play_track').attr('y', 35).text("PLAY");
      eje_x_s.append('g').attr('transform', 'translate(35,10)')
        .append('path').attr('class', 'icon_track').attr('d', 'M11,10 L18,13.74 18,22.28 11,26 M18,13.74 L26,18 26,18 18,22.28');
      var restart_text = eje_x_s.append('text').attr('class', 'restart_track')
        .attr('text-anchor', 'end').attr('x', x_interval(domain[domain.length - 1])).attr('y', 35)
        .text("RESTART");

      // eje_x_s.call(axis).selectAll('text')
      //   .attr('opacity', function(d, i) {
      //     var r;
      //     if(step>1) r = ((i  % step) === 0) ? 1 : 0;
      //     else r=1;
      //
      //     return r;
      //   })
      //   .html(function(d) {
      //     return `<tspan x="0" dy="0.71em">${format_axis_m(d)}</tspan><tspan x="0" dy="1.2em">${format_axis_y(d)}</tspan>`;
      //   });

      handle = slider.insert("circle", ".track-overlay")
        .attr("class", "handle")
        .attr("r", 10);

      handle
        .transition()
        .duration(10)
        // .attr("cx", x_interval(last_value));
        .attr("cx", x_interval(domain[0]));

      var resizeTimer, resizeTimer2;
      $(window).on('resize', function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
          if (!is_mobile()) {
            exec_resize();
          }
        }, 100);
      });
      $(window).on("orientationchange", function() {
        clearTimeout(resizeTimer2);
        resizeTimer2 = setTimeout(function() {
          if (is_mobile()) {
            exec_resize();
          }
        }, 100);
      });

      var exec_resize = function() {
        width_g = $(options.tag).width();
        width2 = width_g - margin2.left - margin2.right;
        d3.select(options.tag).select("svg").attr("width", width2 + margin2.left + margin2.right);

        x_interval.range([0, width2]);
        var axis = d3.axisBottom(x_interval);
        var step = Math.ceil(44 / x_interval.step());
        // .tickValues(x_interval.domain().filter(function(d, i) {
        //   return !(i % 2);
        // }));

        track
          .transition()
          .duration(500)
          .attr("x1", x_interval.range()[0])
          .attr("x2", x_interval.range()[1]);
        track_inset
          .transition()
          .duration(500)
          .attr("x1", x_interval.range()[0])
          .attr("x2", x_interval.range()[1]);

        track_overlay
          .transition()
          .duration(500)
          .attr("x1", x_interval.range()[0] - 10)
          .attr("x2", x_interval.range()[1] + 10);

        // eje_x_s
        //   .call(axis).selectAll('text')
        //   .attr('opacity', function(d, i) {
        //     var r;
        //     if(step>1) r = ((i  % step) === 0) ? 1 : 0;
        //     else r=1;
        //
        //     return r;
        //   })
        //   .html(function(d) {
        //     return `<tspan x="0" dy="0.71em">${format_axis_m(d)}</tspan><tspan x="0" dy="1.2em">${format_axis_y(d)}</tspan>`;
        //   });
        restart_text.attr('x', x_interval(domain[domain.length - 1])).attr('y', 30);


        handle
          .transition()
          .duration(500)
          .attr("cx", x_interval(last_value));

        track_inset_left.transition()
          .duration(500)
          .attr("x2", x_interval(last_value));

      };
    }
  }

  function is_mobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }

  function circle_flash1() {

    d3.select(this).transition().attr("opacity", 0.01)
      .duration(3000)
      .attr("r", 15)
      .each("end", circle_flash2);
  }

  function circle_flash2() {
    d3.select(this)
      .transition()
      .delay(300)
      .duration(0.0001)
      .attr("r", 7.5)
      .attr("opacity", 0.8)
      .each("end", circle_flash1);
  }

  function translate_hand1() {

    d3.select(this).transition().duration(5000)
      .attr("transform", "translate(" + x_interval(domain[domain.length - 1]) + ",-5)")
      .each('end', translate_hand2);
  }

  function translate_hand2() {
    d3.select(this).transition().duration(5000)
      .attr("transform", "translate(" + x_interval(domain[0]) + ",-5)")
      .each('end', translate_hand1);
  }

  function click_svg_content() {
    if (flag_click_svg === 0 && c_options.pointer) {
      flag_click_svg++;
      g_hand_mov.remove();
    }
  }



  function brushed() {
    var value;
    // var mouse_value=x_interval.invert(d3.event.x);

    var mouse_value = d3.event.x;
    value = mouse_value;
    if (mouse_value <= 0) value = 0;
    if (mouse_value >= width2) value = width2;

    handle.attr("cx", value);
    track_inset_left.attr("x2", value);

    var res = calc_paramters(value);
    options.moves(res.fix);

  }

  function calc_paramters(value) {
    var value_return = {};
    var value1, value2;
    var before_value = 0;

    values_domain.forEach(function(d, count) {
      value1 = x_interval(values_domain[count]);
      value2 = x_interval(values_domain[count + 1]);
      if (count !== values_domain.length - 1) {
        if (value >= before_value && value <= (value1 / 2 + value2 / 2)) {
          // handle
          //   .transition()
          //   .duration((param)?0:100)
          //   .attr("cx", value1);
          // track_inset_left.transition()
          //   .duration((param)?0:100).attr("x2", value1);

          last_value = values_domain[count];
          value_return.orig = value1;
          value_return.fix = last_value;
          // c_options.end(values_domain[count]);
        }
      } else {
        if (value >= before_value) {
          // handle
          //   .transition()
          //   .duration((param)?0:100)
          //   .attr("cx", value1);
          // track_inset_left.transition()
          //   .duration((param)?0:100).attr("x2", value1);

          last_value = values_domain[count];
          value_return.orig = value1;
          value_return.fix = last_value;
          // c_options.end(values_domain[count]);
        }
      }
      before_value = (value1 / 2) + (value2 / 2);
    });
    return value_return;
  }

  function brushended(param) {
    var value = (param !== undefined) ? x_interval(param) : d3.event.x;
    if (value <= 0) value = 0;
    if (value >= width2) value = width2;
    if (value === undefined) value = last_value;

    var value1 = calc_paramters(value).orig;
    handle
      // .transition()
      // .duration((param) ? 0 : 100)
      // .duration(1)
      .attr("cx", value1);

    track_inset_left
    // .transition()
    // .duration((param) ? 0 : 100)
      // .duration(1)
      .attr("x2", value1);

    c_options.end(last_value);

    // values_domain.forEach(function(d, count) {
    //   value1 = x_interval(values_domain[count]);
    //   value2 = x_interval(values_domain[count + 1]);
    //   if (count !== values_domain.length - 1) {
    //     if (value >= before_value && value <= (value1 / 2 + value2 / 2)) {
    //       handle
    //         .transition()
    //         .duration((param)?0:100)
    //         .attr("cx", value1);
    //       track_inset_left.transition()
    //         .duration((param)?0:100).attr("x2", value1);
    //
    //       last_value = values_domain[count];
    //       c_options.end(values_domain[count]);
    //     }
    //   } else {
    //     if (value >= before_value) {
    //       handle
    //         .transition()
    //         .duration((param)?0:100)
    //         .attr("cx", value1);
    //       track_inset_left.transition()
    //         .duration((param)?0:100).attr("x2", value1);
    //
    //       last_value = values_domain[count];
    //       c_options.end(values_domain[count]);
    //     }
    //   }
    //   before_value = (value1 / 2) + (value2 / 2);
    // });

  }

};

module.exports = create_slider;
