// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";
import * as d3 from "d3";

$(document).ready(function() {
  $('#avatar img').click(function() {
    $("#start p").delay(300).fadeIn(500);
    $(this).next().prop("checked", "checked");
    $(this).css("opacity", 1);
    $("#avatar img").not($(this)).css("opacity", 0.5);
  });
});

$(document).ready(function() {
  $("#start").click(function() {
    $("#start").fadeOut(500);
    $(".main-headline").fadeOut(500);
    $(".start").fadeOut(500);
    $(".st-content-footer").fadeOut(500);
    $(".content").delay(500).fadeIn(500);
    $("#form").show();
    $("#form").css("opacity", "0");
    $(".half").fadeIn(500);
    $(".online").fadeIn(500);
    $("#status").fadeIn(500);
    if ($(window).width() < 768) {
      $("#info").hide();
    }
  });
});

$(document).ready(function() {
  $("#start").click(function() {
    $("#background img").css("height", $(".conversational-form-inner").height() + "vh");
    var avatarValue = $('input[name=avatar]:checked').val();

    var conversationalForm = window.cf.ConversationalForm.startTheConversation({
      formEl: document.getElementById("form"),
      robotImage: "./images/merlion.png",
      userImage: "./images/Avatar/" + avatarValue + ".png",
      //context: document.getElementById("cf-context"),
      submitCallback: function() {
        // be aware that this prevents default form submit.
        var formData = conversationalForm.getFormData();
        var formDataSerialized = conversationalForm.getFormData(true);
        console.log("Formdata:", formData);
        console.log("Formdata, serialized:", formDataSerialized);
        var myarray = [];

        $('#form').find(':input:checked').each(function() {
          $(':input:checked.location').each(function() { //check location
            var location = $(this).val();
            $("#" + location).show();
          });

          $(':input:checked.effects').each(function() { //check effects
            var effects = $(this).val();

            if ($(window).width() > 768) {

              $("#" + effects).show();
              $("#" + effects).stop().delay(500).animate({
                right: "1%"
              }, 2000);

            } else {
              $("#" + effects).show().addClass("right");
            }
          });

          $(':input:checked.character').each(function() { //check first character
            var character = $(this).val();
            if ($(window).width() > 768) {
              $("#" + character).show();
              $("#" + character).stop().delay(500).animate({
                right: "48%"
              }, 2000);
            } else {
              $("#" + character).show().addClass("left");
            }
          });

          $(':input:checked.character2').each(function() { //check second character
            var character2 = $(this).val();
            if ($(window).width() > 768) {
              $("#" + character2).show();
              $("#" + character2).stop().delay(1000).animate({
                right: "1%"
              }, 2000);
            } else {
              $("#" + character2).show().addClass("right");
            }
          });

          myarray.push($(this).val());
          var tmpvalue = $(this).val();
          $("#info img").each(function() {

            if ($(this).attr("id") == tmpvalue + "_shadow" || $(this).attr("id") == tmpvalue + "_shadow2") {
              myarray.push($(this).attr("id"));
              $("#" + tmpvalue + "_shadow").show();
              $("#" + tmpvalue + "_shadow").animate({
                right: "48%"
              }, 2000);
              $("#" + tmpvalue + "_shadow2").show();
              $("#" + tmpvalue + "_shadow2").animate({
                right: "1%"
              }, 2000);
            }
          });
          console.log(myarray);

        });

        conversationalForm.addRobotChatResponse("Congratulations, you have reached the end of the form!");

        if ($(window).width() < 768) {
          $(".notification").show();
          $("#drpdwn").animate({
            "right": "0"
          }, 1500);
        } else {
          $("#deskdrpdwn").fadeIn();
        }
        // setTimeout(function() {
        //   var interval = setInterval(function() {
        //     seconds--;
        //     if (seconds <= 0) {
        //              start the conversation
        // window.ConversationalForm.remove();
        // $("#form").fadeOut();
        // $(".online").fadeOut();
        // //$("#text").hide();
        // //$("#end").fadeIn(500);
        // //$(".half").fadeIn(500);
        // $(".half").hide();
        // $("#text").fadeIn(500);
        // $("#download").fadeIn(500);
        //
        // clearInterval(interval);

        var image_background = myarray[0];
        var image_name_left = myarray[1];
        var shadow = myarray[2];
        var image_name_right = myarray[3];
        var shadow2 = myarray[4];
        var image_name_top = myarray[5];

        $(document).ready(function() {
          var d_canvas = document.getElementById('canvas');
          var context = d_canvas.getContext('2d');
          var background = document.getElementById(image_background);
          var top = document.getElementById(image_name_top);
          var right = document.getElementById(image_name_right);
          var left = document.getElementById(image_name_left);
          var shadowleft = document.getElementById(shadow);
          var shadowright = document.getElementById(shadow2);

          context.drawImage(background, 0, 0, 800, 1080);
          context.drawImage(top, 400, 0);
          context.drawImage(shadowright, 400, 520, 400, 500);
          context.drawImage(right, 400, 520, 400, 500);
          context.drawImage(shadowleft, 0, 520, 400, 500);
          context.drawImage(left, 0, 520, 400, 500);

          $('#download').click(function() {

            var img = context.getImageData(0, 0, 150, 150);
            var base64 = d_canvas.toDataURL("image/gif");
            var a = document.createElement('a');

            a.href = base64;
            a.target = '_blank';
            a.download = 'myImage.png';

            document.body.appendChild(a);
            a.click();

          });
        });
      }
      //}, 1000);
      //}, 500);
      //});
    });
  });
});

$('.continue').click(function() {
  $(".half").fadeOut(500);
  $(".continue").fadeOut(500);
  $(".online").fadeOut(500);
  window.ConversationalForm.remove();
  $("#text").delay(500).fadeIn(500);
  $(".details").delay(500).fadeIn(500);

});

$(".newparade").click(function() {
  $("#canvas").delay(500).fadeOut(500);
  $(".details").delay(500).fadeOut(500);
  $(".main-headline").fadeIn(500);
  $(".st-content-footer").fadeIn(500);
});
