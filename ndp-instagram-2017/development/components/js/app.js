var $ = require('jquery');
var jQuery = $;

require("./libraries/waypoint");
var lazy_load = require("./libraries/lazy-load");
var Masonry = require("masonry-layout");

require("./libraries/nano");

var tag_search = "ndp2017";
var kp = "st_ndp2017";

var para_url = "?kp=" + kp + "&tag=" + tag_search;
var url_instagrams = "http://graphics.straitstimes.com/api/instagram/list-instagrams-2016" + para_url;
// var url_instagrams = "http://st-visuals.com/graphics/api/instagram/list-instagrams-2016" + para_url;
// var url_instagrams = "http://localhost/graphics/api/instagram/list-instagrams-2016"+para_url;

var st_read_instagram = "http://graphics.straitstimes.com/st-read-from-instagram";
// var st_read_instagram = "http://st-visuals.com/graphics/graphics/st-read-from-instagram";
// var st_read_instagram = "http://localhost/graphics/st-read-from-instagram";

var parameter = 0;
var msnry;
var info = {};
var pagination = 0;

var setTime,
  setTime2;
var f_data_loaded = 0;
var f_data_loaded_f;
var a_temp = [];
var save_last_id = 0;
var save_lower_id = 0;

var template_instagram = '<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="4" style="background:#FFF;border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style = "padding:8px;" ><div style="background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;" ><div style="background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;" > </div> </div> <p style=" margin:8px 0 0 0; padding:0 4px;" > <a href="[ST-LINK]" style="color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target = "_top" > [ST-CAPTION] </a></p></div> </blockquote>';

(function() {
  lazy_load({tag: '.st-header-title h1,.st-header-strap ', animation: 'ld_paragraph'});

  $.ajax({
    url: url_instagrams,
    dataType: "jsonp",
    success: function(data) {
      if (data.length > 0) {

        $.each(data, function(i, d) {
          if ($.inArray(tag_search, d.tags) !== -1) {
            var link_i = d.link;
            var url_media = $(d.media.trim()).find(".content").html();
            // var caption = d.text;
            // var new_template = template_instagram.replace("[ST-CAPTION]", caption, 'gi').replace("[ST-LINK]", link_i, 'gi');

            var a_url = url_media.split(".");
            var total_a_url = a_url.length;
            var format_media = a_url[total_a_url - 1];

            var new_item;
            if (format_media.trim() === "mp4") {
              new_item = $("#container-elements-social").append('<div class="item item' + i + '" data-link="' + link_i + '"  data-id="' + d.id + '"><video class="st-image-responsive"><source src="' + url_media + '" type="video/mp4"></video></div>');
            } else {
              new_item = $("#container-elements-social").append('<div class="item item' + i + '" data-link="' + link_i + '" data-id="' + d.id + '"><img src="' + url_media + '" alt="img" class="st-image-responsive"></div>');
            }

            // $("#container-elements-social .item" + i).data("string", new_template);
          }

        });

        save_last_id = $("#container-elements-social .item:nth(0)").data("id");
        save_lower_id = $("#container-elements-social .item:last-child").data("id");

        if (typeof(Masonry) == "function") {
          var container = document.querySelector('#container-elements-social');
          msnry = new Masonry(container, {
            columnWidth: '.item',
            itemSelector: '.item',
            isFitWidth: true,
            gutter: 20
          });

          msnry.on('layoutComplete', function() {
            var loaded = setTimeout(function() {
              clearTimeout(loaded);
              if (Waypoint)
                Waypoint.refreshAll();
              f_data_loaded = 1;
            }, 500);
          });
        }

        lazy_load({tag: "#container-elements-social img", animation: "ld_image"});

        $("#container-elements-social img").load(function() {
          msnry.layout();
          if (Waypoint)
            Waypoint.refreshAll();
            // $.waypoints('refresh');
          }
        );

        $("#container-elements-social video").on("loadeddata", function() {
          msnry.layout();
          if (Waypoint)
            Waypoint.refreshAll();
            // $.waypoints('refresh');
          }
        );

        var life = setTimeout(function() {
          clearTimeout(life);
          msnry.layout();
          if (Waypoint)
            Waypoint.refreshAll();

          // $.waypoints('refresh');
          parameter = 1;
        }, 500);

        setTime = setTimeout(info.get_new_instagram, 1000);
        setTime2 = setTimeout(info.execute_event_new_instagram, 5000);
        $(".st-loading-page").fadeOut();
      }
    }
  });

  $(window).on("load", function() {
    if (parameter === 1) {
      msnry.layout();
      // $.waypoints('refresh');
      if (Waypoint)
        Waypoint.refreshAll();
      }
    });

  var interval = setInterval(function() {
    if (parameter === 1) {
      clearInterval(interval);
      msnry.layout();
      // $.waypoints('refresh');
      if (Waypoint)
        Waypoint.refreshAll();
      }
    }, 300);

  $("#container-elements-social").on("click", '.item', function() {
    var url_profile = $(this).data('link').trim();
    if (url_profile) {
      // template_instagram
      var t_inst = template_instagram.replace('[ST-LINK]', url_profile, 'g').replace('[ST-CAPTION]', 'straitstimes loading', 'g');
      info.create_modal(t_inst);
    }
  });
  $(".btn_modal_close").on('click', function() {
    $('body').removeClass('hidden-important');
    $(".modal_picture_8").removeClass('modal_picture_8_open');

    var waite_add_index = setTimeout(function() {
      $(".modal_picture_8").addClass('z-index-1');
      $(".modal_picture_8").hide();
      $(".column_content_modal").html("");
      clearTimeout(waite_add_index);
    }, 500);
  });

  $(window).on('click', function(e) {
    if ($(e.target).hasClass("modal_picture_8")) {
      $(".btn_modal_close").trigger("click");
    }
  });
  $(document).keyup(function(evt) {
    if (evt.keyCode == 27) {
      if ($(".modal_picture_8").hasClass("modal_picture_8_open")) {
        $(".btn_modal_close").trigger("click");
      }
    }
  });

  f_data_loaded_f = 1;
  var nl_item = new Waypoint({
    element: $("html")[0],
    offset: 'bottom-in-view',
    handler: function(direction) {
      if (direction === 'down' && f_data_loaded === 1 && f_data_loaded_f === 1) {
        f_data_loaded = 0;
        f_data_loaded_f = 0;
        $(".st-load-more").fadeIn();
        pagination++;
        info.get_more_items(pagination);
      }
    }
  });

})();

info.get_more_items = function(page) {
  var para_url = "?kp=" + kp + "&tag=" + tag_search + "&page=" + page;
  var url_instagrams = "http://graphics.straitstimes.com/api/instagram/list-instagrams-2016" + para_url;

  $.ajax({
    url: url_instagrams,
    dataType: "jsonp",
    success: function(data) {
      if (data.length > 0) {
        var t_new_item = [];
        data.forEach(function(d, i) {
          if (info.is_smaller_than(d, save_lower_id) === 1 && $.inArray(tag_search, d.tags) !== -1) {
            t_new_item.push(d);
          }
        });
        $(".st-load-more").fadeOut();
        if (t_new_item.length >= 5) {
          var init_total = $("#container-elements-social .item").length;
          t_new_item.forEach(function(add_element, i) {
            init_total += i;

            var link_i = add_element.link;
            // var caption = add_element.caption.text;
            // var new_template = template.replace("[ST-CAPTION]", caption, 'gi').replace("[ST-LINK]", link_i, 'gi');
            //
            var url_media = $(add_element.media.trim()).find(".content").html();
            // var caption = d.text;
            // var new_template = template_instagram.replace("[ST-CAPTION]", caption, 'gi').replace("[ST-LINK]", link_i, 'gi');

            var a_url = url_media.split(".");
            var total_a_url = a_url.length;
            var format_media = a_url[total_a_url - 1];

            if (format_media.trim() === "mp4") {
              $("#container-elements-social").append('<div class="item item' + init_total + '" data-link="' + link_i + '"  data-id="' + add_element.id + '"><video class="st-image-responsive"><source src="' + url_media + '" type="video/mp4"></video></div>');
            } else {
              $("#container-elements-social").append('<div class="item item' + init_total + '" data-link="' + link_i + '" data-id="' + add_element.id + '"><img src="' + url_media + '" alt="img" class="st-image-responsive"></div>');
            }

          });
          save_lower_id = $("#container-elements-social .item:last-child").data("id");

          lazy_load({tag: "#container-elements-social img", animation: "ld_image"});

          msnry.reloadItems();

          $("#container-elements-social .item:nth(0) img").load(function() {
            msnry.layout();
            // $.waypoints('refresh');
            if (Waypoint)
              Waypoint.refreshAll();
            }
          );
          $("#container-elements-social .item:nth(0) video").on("loadeddata", function() {
            msnry.layout();
            // $.waypoints('refresh');
            if (Waypoint)
              Waypoint.refreshAll();
            }
          );

          var reset = setTimeout(function() {
            clearTimeout(reset);
            msnry.layout();
            if (Waypoint)
              Waypoint.refreshAll();
            f_data_loaded_f = 1;
          }, 200);

        } else {
          pagination++;
          info.get_more_items(pagination);
        }
      } else {
        $(".st-load-more").html(`<div style="padding:20px;text-align:center;font-family:'Curator Bold', 'Helvetica Neue', Helvetica, Arial, sans-serif">no more items</div>`);
      }
    }
  });

};

info.is_smaller_than = function(new_o, first_id) {
  var r_value = 0;
  if (first_id !== 0) {
    var id_save = first_id.split("_")[0];
    var new_id = new_o.id.split("_")[0];
    if (parseInt(new_id) < parseInt(first_id)) {
      save_lower_id = new_o.id;
      r_value = 1;
    }
  }
  return r_value;
};

info.get_new_instagram = function() {
  clearTimeout(setTime);
  $.ajax({
    url: st_read_instagram, dataType: "json",
    // jsonp: "callback",
    cache: false,
    success: function(ndata) {
      if (ndata.length > 0) {
        $.each(ndata.reverse(), function(inf, n_d) {
          if (info.find_id_in_array(n_d, a_temp) === 0 && $.inArray(tag_search, n_d.tags) !== -1) {
            a_temp.push(n_d);
          }
        });
      }

      setTime = setTimeout(info.get_new_instagram, 40000);
      // location.reload();
    }
  });

};

info.find_id_in_array = function(element, array) {
  var r_value = 0;
  $.each(array, function(i, d) {
    if (d.id === element.id) {
      r_value = 1;
    }
  });
  return r_value;
};

info.execute_event_new_instagram = function() {
  clearTimeout(setTime2);
  if (a_temp.length > 0) {
    var add_element = a_temp[0];
    a_temp.splice(0, 1);
    if (info.is_greater_than(add_element, save_last_id) === 1) {
      var link_i = add_element.link;
      // var caption = add_element.caption.text;
      // var new_template = template.replace("[ST-CAPTION]", caption, 'gi').replace("[ST-LINK]", link_i, 'gi');
      var url_media;
      if (add_element.type === "mp4") {
        url_media = add_element.videos.standard_resolution.url;
        $("#container-elements-social").prepend('<div class="item" data-link="' + link_i + '" data-id="' + add_element.id + '"><video class="st-image-responsive"><source src="' + url_media + '" type="video/mp4"></video></div>');

        msnry.prepended($('<div class="item" data-link="' + link_i + '" data-id="' + add_element.id + '"><video class="st-image-responsive"><source src="' + url_media + '" type="video/mp4"></video></div>'));
      } else {
        url_media = add_element.images.standard_resolution.url;
        $("#container-elements-social").prepend('<div class="item" data-link="' + link_i + '" data-id="' + add_element.id + '"><img src="' + url_media + '" alt="img" class="st-image-responsive"></div>');

        msnry.prepended($('<div class="item" data-link="' + link_i + '" data-id="' + add_element.id + '"><img src="' + url_media + '" alt="img" class="st-image-responsive"></div>'));
      }

      // $("#container-elements-social .item:nth(0)").data("string", new_template);

      lazy_load({tag: "#container-elements-social img", animation: "ld_image"});

      msnry.reloadItems();
      $("#container-elements-social div:first-child").hide().fadeTo("slow", 1);

      $("#container-elements-social .item:nth(0) img").load(function() {
        msnry.layout();
        // $.waypoints('refresh');
        if (Waypoint)
          Waypoint.refreshAll();
        }
      );
      $("#container-elements-social .item:nth(0) video").on("loadeddata", function() {
        msnry.layout();
        // $.waypoints('refresh');
        if (Waypoint)
          Waypoint.refreshAll();
        }
      );

      var reset = setTimeout(function() {
        clearTimeout(reset);
        msnry.layout();
        if (Waypoint)
          Waypoint.refreshAll();
        }
      , 200);

      // $("#container-elements-social div:last-child").addClass("opacity_d_element");
      // $("#container-elements-social div:last-child").remove();
    }
  }
  setTime2 = setTimeout(info.execute_event_new_instagram, 10000);
};

info.is_greater_than = function(new_o, first_id) {
  var r_value = 0;
  if (first_id !== 0) {
    var id_save = first_id.split("_")[0];
    var new_id = new_o.id.split("_")[0];
    if (parseInt(new_id) > parseInt(first_id)) {
      save_last_id = new_o.id;
      r_value = 1;
    }
  }
  return r_value;
};

info.create_modal = function(content_modal) {
  if (typeof instgrm !== 'undefined') {
    $('body').addClass('hidden-important');
    $(".modal_picture_8").show();
    var waite_add_index = setTimeout(function() {
      $(".modal_picture_8").addClass('modal_picture_8_open');
      $(".modal_picture_8").removeClass('z-index-1');
      clearTimeout(waite_add_index);
    }, 100);

    $(".column_content_modal").html(content_modal);
    instgrm.Embeds.process();

    var scroll_height = $('.scroll_content_quote').outerHeight();
    var content_quote_height = $('.content_quote').outerHeight();
    if (content_quote_height > scroll_height) {
      $('.content_quote').addClass('no_center_div');
      $('.scroll_content_quote').css({'overflow-y': 'scroll'});
    } else {
      $('.content_quote').removeClass('no_center_div');
      $('.scroll_content_quote').css({'overflow-y': 'hidden'});
    }
    $(".nano").nanoScroller({scroll: 'top', alwaysVisible: true});
    $(window).trigger("resize");
  }
};
