// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";
// console.log(xx);
// console.log(d3);
var serverDate;
var curContestExpiry;
$(function(){
  $.ajax({
        url: 'php/time.php',
        type: 'post',
        data: {'action': 'follow'},
        success: function(data, status) {
          serverDate = new Date(data);
          $('#timenow').html(serverDate);
            createForm(serverDate);
        },
        error: function(xhr, desc, err) {
          //server down, please try again later
          console.log(xhr);
          console.log("Details: " + desc + "\nError:" + err);
        }
      }); // end ajax call
});

$("#VoteButton").on("click",function(){
  // console.log("submit clicked");
  Validate();
});


function getMonthStr(index){
  var monthStrs = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
return monthStrs[index];
}

function getPlaceStr(index,toCap){
  var places = ["first",'second','third','fourth','fifth','sixth','seventh'];
  var selection = places[index];
  // var tempWord =
  return toCap?selection[0].toUpperCase()+selection.substring(1) : selection;
}

function getCurrentTime(){
  $.ajax({
        url: 'php/time.php',
        type: 'post',
        data: {'action': 'follow'},
        success: function(data, status) {
             serverDate = new Date(data);
        },
        error: function(xhr, desc, err) {
          console.log(xhr);
          console.log("Details: " + desc + "\nError:" + err);
          return false;
        }
      }); // end ajax call

      return serverDate;
}

function createForm(dateNow){
  d3.csv("data/ndp_photo_contest.csv",function(data){
    var startDate = new Date(data[0].date_begin);
    var endDate = new Date(data[data.length-1].date_end);
    if (dateNow < startDate){
      //contest not yet start
      $(".loading-form").hide();
      $(".notStart").show();
      $(".content-fb-comments").hide();
    }else if (dateNow > endDate){
      //contest over
      $(".loading-form").hide();
      $(".ended").show();
      $(".prev-contest").show();
      var prevIntroHTML ='<div class="prev-separation"></div>'+
                         '<h2>Previous locations</h2>'+
                         '<p>Here are the other landmarks ST’s photojournalists selected for previous questions.</p>';
        $(".prev-contest").append(prevIntroHTML);
      for (var k =0; k<data.length;k++){
        var fromDate = new Date(data[k].date_begin);
        var toDate = new Date(data[k].date_end);
        var fromDate_r = new Date(data[k].date_begin_r);
        var toDate_r = new Date(data[k].date_end_r);
        var prevContestHTML = '<div class="prev-contest-container">'+
                              '<div class="prev-img-container">'+
                               '<img src="images/ndp-photo-contest/'+data[k].image+'"/>'+
                               '<p>'+getPlaceStr(k,true)+' location ('+
                               getMonthStr(fromDate_r.getMonth())+'&nbsp;'+fromDate_r.getDate()+' - '+
                               getMonthStr(toDate_r.getMonth())+'&nbsp;'+toDate_r.getDate()+')</p>'+
                               '<span>'+data[k].location+'</span></div>'+
                               '<div class="prev-feeback">'+
                               '<p class="head-author">Shot by '+data[k].photographer+'.</p>'+
                               '<p>&ldquo;'+data[k].reflection+'&rdquo;</p></div></div>';

          $(".prev-contest").append(prevContestHTML);
      }//end of previous contest for loop
    }else{
      // console.log("contest in progress");
      for (var i=data.length-1;i>=0;i--){
           var curfromDate = new Date(data[i].date_begin);
           var curToDate = new Date(data[i].date_end);
       if (dateNow >curfromDate && dateNow <curToDate){ //within certain contest range
          curContestExpiry = curToDate;
          $(".contestNo").attr("value","NDPPC"+(i+1)+"-2017");
          // $(".contestNo").attr("value","ndpt"+(i+1)+"-2017");
          var curfromDate_r = new Date(data[i].date_begin_r);
          var curToDate_r = new Date(data[i].date_end_r);
         $(".img-guess-container img").attr('src',"images/ndp-photo-contest/"+data[i].image);
         $("#place_order").text(getPlaceStr(i,false));
         $("#expire_date").text(getMonthStr(curToDate_r.getMonth())+' '+curToDate_r.getDate());

         if (i < data.length-1){ //not the last one
           var nextFromDate = new Date(data[i+1].date_begin);
           var nextFromDate_r = new Date(data[i+1].date_begin_r);

           $("#next_place_order").text(getPlaceStr(i+1,false));
           $("#next_date").text(getMonthStr(nextFromDate_r.getMonth())+' '+nextFromDate_r.getDate());
           $("#next_contest_remark").removeClass("none");
         }

         if (i >0){ //not 1st contest, has previous images to be shown
           $(".prev-contest").show();
           var prevIntroHTML ='<div class="prev-separation"></div>'+
                              '<h2>Previous locations</h2>'+
                              '<p>Here are the other landmarks ST’s photojournalists selected for previous questions.</p>';
             $(".prev-contest").append(prevIntroHTML);
           for (var j =0; j<i;j++){
             var fromDate = new Date(data[j].date_begin);
             var toDate = new Date(data[j].date_end);
             var fromDate_r = new Date(data[j].date_begin_r);
             var toDate_r = new Date(data[j].date_end_r);
             var prevContestHTML = '<div class="prev-contest-container">'+
                                   '<div class="prev-img-container">'+
                                    '<img src="images/ndp-photo-contest/'+data[j].image+'"/>'+
                                    '<p>'+getPlaceStr(j,true)+' location ('+
                                    getMonthStr(fromDate_r.getMonth())+'&nbsp;'+fromDate_r.getDate()+' - '+
                                    getMonthStr(toDate_r.getMonth())+'&nbsp;'+toDate_r.getDate()+')</p>'+
                                    '<span>'+data[j].location+'</span></div>'+
                                    '<div class="prev-feeback">'+
                                    '<p class="head-author">Shot by '+data[j].photographer+'.</p>'+
                                    '<p>&ldquo;'+data[j].reflection+'&rdquo;</p></div></div>';

               $(".prev-contest").append(prevContestHTML);
           }//end of previous contest for loop
         }
       } // end of within contest date comparison

     }//end of all contest for loop
     $(".loading-form").fadeOut(function(){
       $(".contest-form").fadeIn();
     });

   }//end of contest has started condition

  });
}

function Validate() {
  var realTime = getCurrentTime();
  // console.log("current time: "+realTime);
  // console.log("current contest expired time: "+curContestExpiry);
  // console.log("expired?:"+(realTime>curContestExpiry));
    if (realTime>curContestExpiry){
      alert("This contest had expired. Refresh the page to view the next available contest");
      location.reload(true);
      return;
    }else{
      // console.log("contest not expired, continue validate");

  var fname = document.getElementById("fname");
  var eml = document.getElementById("email");
  var phno = document.getElementById("mobiletel");

  var reason = document.getElementById("reasonID");
  var ans_place = document.getElementById("placeID");
  var nric = document.getElementById("nric");
  var agreement = document.getElementById("ans1");
  var err = "";

  if ((fname.value).trim() === "") {
    err = err + "Please enter your full name\n";
  } else if (!validateName(fname.value)) {
    err = err + "Please use only letters and/or spaces for first name\n";
  }


  if ((phno.value).trim() === "") {
    err = err + "Please enter your contact number\n";

  } else if (!validatePhone(phno.value)) {
    err = err + "Please enter a valid Singapore number\n";
  }

  if ((eml.value).trim() === "") {
    err = err + "Please enter your e-mail\n";
  } else if (!validateEmail(eml.value.trim())) {
    err = err + "Please enter a valid e-mail\n";
  }

  if ((nric.value).trim() === "") {
    err = err + "Please enter your NRIC / Passport number\n";
  }
  if ((nric.value).length != 5) {
    err = err + "Please enter last 4 digits of NRIC / Passport number + letter\n";
  }
  if (!nric.value.match(/[a-z]/i)) {
    err = err + "There is no letter in the NRIC / Passport number, please check again.\n";
  }
  if (!nric.value.slice(-1).match(/[a-z]/i)) {
    err = err +
      "The letter in the NRIC / Passport number is in the incorrect position, please check again.\n";
  }

  if ((ans_place.value).trim() ===""){
    err = err + "Please answer the question.\n";
  }

  if ((reason.value).trim() === "") {
    err = err + "Please provide your explanation.\n";
    document.getElementById("exceed_span").style.opacity = 0;
  }else if (checkTextAreaWordCount(reason.value) > 52){
    document.getElementById("exceed_span").style.opacity = 1;
    err = err + "Please explain in not more than 52 words\n"
  }else{
    document.getElementById("exceed_span").style.opacity = 0;
  }

  if (!agreement.checked) err = err+"Please select the checkbox to agree to our Terms & Conditions\n";

  if (err !== "") {
    alert(err);
  } else {
    document.forms['stform'].submit();
  }
}
  return false;
}

function validateName(str) {
  var re = /^[a-zA-Z ]+$/;
  return re.test(str);
}

function validatePhone(str) {
  var re = /^[1-9][0-9]{7}$/;
  return re.test(str);
}

function validateEmail(email) {
  var re =
    /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(email);
}

function validateRadioButtonList(radioButtonListId) {
  var listItemArray = document.getElementsByName(radioButtonListId);
  var isItemChecked = false;
  for (var i = 0; i < listItemArray.length; i++) {
    var listItem = listItemArray[i];
    if (listItem.checked) {
      isItemChecked = true;
    }
  }
  return isItemChecked;
}
