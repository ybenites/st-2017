// require("../precss/app.css");
import $ from "jquery";

// import xx from "./library/fix-svg-size";

import OpenSeadragon from "openseadragon";
import Swiper from "swiper";

// var url_csv_text_main = "http://st-visuals.com/graphics/st-get-file-csv/12J63j4erH4TZQVf0uT8sIQh3lHHAnXFl8KDcmRuiC0c";
var url_csv_text_main = "csv/csv-ndp2017.csv";

var url_list_submission = "/api/list/global_submission_json?project_code=ndp%20submission%20photo"
// var url_save_submission = "/functions/graphics/general_submission";

var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
var width_photo_gigant = 155610;
var height_photo_gigant = 39030;
var a_data_photo;

var tag_people = false;
var viewer;
var imagePoint;
var internal_viewer;
var viewportPoint;

var hash = window.location.hash;
var f_x, f_y, f_zoom;

$(".content-ndp-photos").height(window.innerHeight);
if (hash !== "") {
  $("#intropage").hide();
  $(".main-headline").hide();
  $(".content-ndp-photos").css("z-index", 1);
  $(".container-position-gallery").css("opacity", 1);
  $(".content-ndp-photos").css("filter", "blur(0px)");
  $(".content-ndp-photos").css("-webkit-filter", "blur(0px)");
  $(".content-ndp-photos").css("transform", "scale(1.0)");
  $(".content-ndp-photos").css("-webkit-transform", "scale(1.0)");
  $(".tagbtnImg-container").show();


  var cut_hash = hash.substring(1);
  var a_hash = cut_hash.split("-");
  f_x = a_hash[0];
  f_y = a_hash[1];
  f_zoom = a_hash[2];
}


var spacebetweenpx = 10;
var Sw = Swiper('.container-gallery-photos', {
  // Optional parameters
  direction: 'horizontal',
  // loop: true,
  // If we need pagination
  // pagination: '.swiper-pagination',
  // Navigation arrows

  nextButton: '.swiper-button-next',
  prevButton: '.swiper-button-prev',
  // pagination: '.swiper-pagination',
  // paginationType: 'progress',
  // And if we need scrollbar
  // scrollbar: '.swiper-scrollbar',
  // height:400,
  // slidesPerView: 7,
  // centeredSlides: true,
  slidesPerView: "auto",
  spaceBetween: spacebetweenpx,
  // breakpoints: {
  //   1024: {
  //     slidesPerView: 4,
  //     spaceBetween: spacebetweenpx
  //   },
  //   768: {
  //     slidesPerView: 3,
  //     spaceBetween: spacebetweenpx
  //   },
  //   640: {
  //     slidesPerView: 2,
  //     spaceBetween: spacebetweenpx
  //   },
  //   320: {
  //     slidesPerView: 1,
  //     spaceBetween: spacebetweenpx
  //   }
  // },
  onSlideChangeEnd: function() {}
});


d3.csv(url_csv_text_main, function(error, data) {
  if (error)
    return error;
  a_data_photo = data;
  data.forEach((d, i) => {

    Sw.appendSlide(`<div class="swiper-slide"><div class="st-slide key_${ (i + 1)}" data-key="${ (i + 1)}"><div class="sw-title"><img src="images/target-people/${d.image}" class="st-image-responsive" /></div><div class="sw-description">${d.name}</div></div></div>`);
  });

});

$("#toggle").click(function() {
  $("#intropage").fadeOut();
  $(".main-headline").fadeOut();
  $(".content-ndp-photos").css("z-index", 1);
  $(".container-position-gallery").animate({
    opacity: 1
  });
  $(".content-ndp-photos").css("filter", "blur(0px)");
  $(".content-ndp-photos").css("-webkit-filter", "blur(0px)");
  $(".content-ndp-photos").css("transform", "scale(1.0)");
  $(".content-ndp-photos").css("-webkit-transform", "scale(1.0)");
  $(".tagbtnImg-container").fadeIn();
  $(".info-removetag-box").fadeIn();
});

viewer = OpenSeadragon({
  id: "content-ndp-photos",
  prefixUrl: "images/buttons_zoom/",

  tileSources: [{
    type: "zoomifytileservice",
    width: width_photo_gigant,
    height: height_photo_gigant,
    tilesUrl: "images/ndp_photos/"
  }],
  showNavigator: true,
  showNavigationControl: true,
  autoHideControls:false,
  showFullPageControl:false,
  gestureSettingsMouse: {
    clickToZoom: false,
    dblClickToZoom: true
  },
  gestureSettingsPen: {
    clickToZoom: false,
    dblClickToZoom: true
  },
  // minZoomLevel: getMinZoomLevel(window.innerWidth / window.innerHeight),
  constrainDuringPan: true,
  visibilityRatio: 1.0,
  preserveImageSizeOnResize: true,
  autoResize: true,
  defaultZoomLevel:1,
  // defaultZoomLevel: getMinZoomLevel(window.innerWidth / window.innerHeight),
  // immediateRender:true,
  //debugMode: true
});

if (hash) {
  gotoHighlight(f_x, f_y, f_zoom);
}else{
  zoom_screen_fit();
}
$.ajax({
  url: url_list_submission,
  dataType: "jsonp",
  success: function(response) {
    if (response.length > 0) {
      draw_pins(response);
    }
  }
});

function draw_pins(data){
  if(viewer.isOpen()){
    data.forEach(d => {
      var o_pin = {
        id: d.id,
        message1: d.message1,
        name1: d.name1
      };
      var new_parse = JSON.parse(d.json1[0]);
      var obj_coordinates = {};
      obj_coordinates.x = parseFloat(new_parse.x);
      obj_coordinates.y = parseFloat(new_parse.y);
      addRealTimePin(obj_coordinates, o_pin, viewer);
    });
  }else{
    d3.timeout(d=>{
      draw_pins(data);
    },100);
  }
}

$(function() {
  $(".content-ndp-photos").height(window.innerHeight);
  // var duomo = {
  //   Image: {
  //     xmlns: "http://schemas.microsoft.com/deepzoom/2008",
  //     Url: "//openseadragon.github.io/example-images/duomo/duomo_files/",
  //     Format: "jpg",
  //     Overlap: "2",
  //     TileSize: "256",
  //     Size: {
  //       Width: "13920",
  //       Height: "10200"
  //     }
  //   }
  // };

  // function newpushPin(id) {
  //     var img = document.createElement("img");
  //     img.id = id;
  //     img.class = "preLoadPin";
  //     img.src = "images/ndp-2017-tag/pointer.png";
  //     console.log("img id?: "+img.id);
  //     return img.id;
  // }


  // var Tagbutton = new OpenSeadragon.Button({
  //   tooltip: "Tag people",
  //   srcRest: "images/buttons_zoom/tag_rest.png",
  //   srcGroup: "images/buttons_zoom/tag_grouphover.png",
  //   srcHover: "images/buttons_zoom/tag_hover.png",
  //   srcDown: "images/buttons_zoom/tag_pressed.png",
  //   onClick: function() {
  //     tag_people = !tag_people;
  //     if (tag_people) {
  //       $("canvas").css("cursor", "crosshair");
  //     } else {
  //       $("canvas").css("cursor", "default");
  //     }
  //     $(".tagbtnImg-container").fadeOut();
  //   }
  // });
  // viewer.buttons.buttons.push(Tagbutton);
  // $(Tagbutton.element).addClass("tagbtn");
  // $(viewer.buttons.element).append(Tagbutton.element);
  $(viewer.buttons.element).find("img").addClass('cursorFinger');

  // getOverlayObject(viewer, overlay);
  // var viewer = OpenSeadragon({
  //   id: "openseadragon1",
  //   prefixUrl: "/openseadragon/images/",
  //   tileSources: [{
  //     type: "zoomifytileservice",
  //     width: 7026,
  //     height: 9221,
  //     tilesUrl: "/test/data/zoomify/"
  //   }]
  // });

  viewer.addHandler('canvas-click', function(event) {
    // new OpenSeadragon.MouseTracker({
    //   element: 'UID001',
    //   clickHandler: function(e) {
    //     console.log("mouse tracker check: " + event.originalEvent.target.id);
    //   }
    // });

    internal_viewer = event.eventSource;
    viewportPoint = internal_viewer.viewport.pointFromPixel(event.position, true);
    imagePoint = internal_viewer.viewport.viewportToImageCoordinates(viewportPoint.x, viewportPoint.y);

    console.log("X :" + imagePoint.x);
    console.log("Y :" + imagePoint.y);
    console.log("Zoom: " + internal_viewer.viewport.getZoom());

    if (tag_people) {
      $(".tagshare").fadeIn(500);
      $(".namefield").focus();
      var string_hash = imagePoint.x + "-" + imagePoint.y + "-" + internal_viewer.viewport.getZoom();
      $(".share").attr("href", window.location.href + "#" + string_hash);
      // $(".tagshare").css("cursor", "default");
    }
  });


  // viewer.addHandler("open", function(e) {
  //
  //
  // });

  // $("#content-ndp-photos").on("touch", ".realtimePin", function(e) {
  //   e.preventDefault();
  //
  // });

  $(".container-position-gallery").on("click", ".sw-title img", function() {
    var slide = $(this).parents(".st-slide");
    var obj = a_data_photo[parseInt(slide.data("key")) - 1];

    $(".st-slide").children().css("opacity", 0.5);
    $(slide).children().css("opacity", 1);
    gotoHighlight(obj.position_x, obj.position_y, obj.zoom);
  });

  $(".share").on("click", function(e) {
    // code share facebook
    e.preventDefault();
    $('.error').hide();
    var name = $(".namefield").val();
    if (name == "") {
      $("label#name_error").fadeIn(500);
      $(".namefield").focus();
      return false;
    }

    var url = $(this).attr("href");
    var name = $(".namefield").val();
    var description = "I was at the NDP on Aug 9, and just tagged myself. Were you there? Tag yourself too!";

    share_face_book(url, name, description);
    return false;
  });



  $(".close").click(function() {
    tag_people = false;
    $(".tagshare").fadeOut(500);
    $("canvas").css("cursor", "default");

    $('.error').hide();
    $(".namefield").val("");
    $(".messagefield").val("");

  });

  $(".done").click(function(e) {
    e.preventDefault();
    $('.error').hide();
    var name = $(".namefield").val();
    if (name === "") {
      $(".namefield").focus();
      $("label#name_error").fadeIn(500);
      return false;
    }
    // fn_save_submission(internal_viewer);
  });

  $(".info-box").on("click", ".close-info-box-button img", function() {
    $(this).parents(".info-box").fadeOut();
    $(".txt-name-person").val();
    $(".txt-comment-person").val();
  });

$(".info-removetag-box").on("click",".close-removetag-button",function(){
  $(this).parents(".info-removetag-box").fadeOut();
});
  var minHeight = "0px";
  $(".container-position-gallery").on("click", ".sw-button-hide-gallery p", function() {
    console.log("height: " + $(".swiper-container").css("height"));
    var isNotMinHeight = $(".swiper-container").css("height") !== minHeight;
    console.log("isnot min height?:" + isNotMinHeight);
    $(".swiper-button-prev,.swiper-button-next").css("opacity", isNotMinHeight ?
      0 :
      1);
    $(".swiper-container").animate({
      height: isNotMinHeight ?
        "0px" : "150px"
    }, function() {
      $(".sw-button-hide-gallery p").text(isNotMinHeight ?
        "SHOW" :
        "HIDE");
    });

  });

  $(window).on("load", function() {
    height_screen();
    // addRealTimePin({
    //   x: 50670.70609436904,
    //   y: 16037.84236382074
    // }, "UID001");
    zoom_screen();

  });
  $(window).on("resize", function() {
    height_screen();
    zoom_screen();
    // console.log("document width: "+window.innerWidth);
  });
  $(window).on("orientationchange", function() {
    height_screen();
  });

});

function gotoHighlight(x, y, zoom) {
  if(viewer.isOpen()){
    var viewport = viewer.viewport;
    var vc = viewport.imageToViewportCoordinates(parseFloat(x), parseFloat(y) + 80);

    // var new_rectangule = new OpenSeadragon.Rect(parseFloat(x), parseFloat(y), width_photo_gigant / parseFloat(zoom), height_photo_gigant / parseFloat(zoom));
    // var bounds = viewport.imageToViewportRectangle(new_rectangule);

    viewport.panTo(vc).zoomTo(zoom);

  } else {
    d3.timeout(d => {
      gotoHighlight(x, y, zoom);
    }, 100);
  }

}

function height_screen() {
  $(".content-ndp-photos").height(window.innerHeight - $(".st-content-menu").height());
}

function getMinZoomLevel(aspectRatio) {
  if (aspectRatio > 1.2) {
    return 3;
  } else if (aspectRatio <= 0.4) {
    return 9;
  } else if (aspectRatio <= 0.6) {
    return 8;
  } else if (aspectRatio <= 1.2) {
    return 5;
  }
}

$(document).one('focus.autoExpand', 'textarea.autoExpand', function() {
  var savedValue = this.value;
  this.value = '';
  this.baseScrollHeight = this.scrollHeight;
  this.value = savedValue;
}).on('input.autoExpand', 'textarea.autoExpand', function() {
  var minRows = this.getAttribute('data-min-rows') | 0,
    rows;
  this.rows = minRows;
  rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
  this.rows = minRows + rows;
});

var send_submission = true;

// function fn_save_submission(internal_viewer) {
//   if (!send_submission)
//     return false;
//   send_submission = false;
//   var name1 = $(".namefield").val();
//   var message1 = $(".messagefield").val();
//   var data = {
//     code: "ndp submission photo",
//     name1: name1,
//     message1: message1,
//     json1: [{
//       x: imagePoint.x,
//       y: imagePoint.y,
//       zoom: internal_viewer.viewport.getZoom()
//     }]
//   };
//   $.ajax({
//     url: url_save_submission,
//     type: "POST",
//     data: data,
//     dataType: "jsonp",
//     success: function(response) {
//       if (response.code === "ok") {
//         var o_pin = {
//           id: response.id,
//           name1: name1,
//           message1: message1
//         };
//         addRealTimePin(imagePoint, o_pin, internal_viewer);
//         $(".close").trigger("click");
//
//         send_submission = true;
//       }
//     }
//   });
// }

function share_face_book(url, name, description) {
  FB.ui({
    method: 'feed',
    link: url,
    hashtag: "#ndp2017",
    quote: description
  }, function(response) {
    if (response && !response.error_message) {
      // fn_save_submission(internal_viewer);
    } else {}
  });
  //
  // FB.ui({
  //   method: 'feed',
  //   link: window.location.href,
  //   caption: 'www.straitstimes.com',
  //   picture: image,
  //   name: name,
  //   description: description
  // });
}

function addRealTimePin(coord, obj_pin, viewer) {
  var elem = document.createElement("img");
  elem.id = obj_pin.id;
  elem.src = "images/ndp-2017-tag/pointer.png";
  elem.className = "realtimePin";

  var newPin = viewer.addOverlay({
    element: elem,
    px: coord.x,
    py: coord.y,
    checkResize: false,
    placement: OpenSeadragon.Placement.CENTER
  });
  d3.select(elem).data([obj_pin]);
  new OpenSeadragon.MouseTracker({
      element: obj_pin.id,
      clickHandler: function(e) {
        var data_card = d3.select(this.element).data();
        // console.log("pin with id: " + this.id + " is clicked");
        $(".txt-name-person").text(data_card[0].name1);
        $(".txt-comment-person").text(data_card[0].message1);
        $(".info-box").fadeIn();
      }
  });


}
function zoom_screen_fit(){
  if(viewer.isOpen()){
    zoom_screen();
    viewer.viewport.fitVertically();
  }else{
    d3.timeout(d=>{
      zoom_screen_fit();
    },100);
  }
}
function zoom_screen() {
  if(viewer.isOpen()){
    viewer.viewport.minZoomLevel = getMinZoomLevel(window.innerWidth / window.innerHeight);
    viewer.viewport.defaultZoomLevel = getMinZoomLevel(window.innerWidth / window.innerHeight);
  }else{
  d3.timeout(d=>{
    zoom_screen();
  });
  }
}
