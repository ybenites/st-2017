// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";

import 'select2';

import "./libraries/easing";


var manualChange = false;


$(function() {
  if ('scrollRestoration' in history) {
  history.scrollRestoration = 'manual';
}

  $("#tl-date-dropdown").select2({
    width:'100%',
    allowClear:true,
    placeholder:"select date",
    minimumResultsForSearch:Infinity
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() > $(".st-header-page").outerHeight() + $(".content-sub-menu").outerHeight()) {
        $(".content-sub-menu-empty").show();
      $(".content-sub-menu").addClass("fix-menu");
    } else {
      $(".content-sub-menu-empty").hide();
      $(".content-sub-menu").removeClass("fix-menu");
    }

    if (!manualChange) {
      var passHighestDate = '';
      $('.tl-node').each(function() {
        var currScroll = $(window).scrollTop();
        var passDate = $(this).find('p').text();

        if (currScroll > ($(this).offset().top - $(".content-sub-menu").outerHeight())) {
          passHighestDate = passDate;
        } else if (currScroll < ($('.tl-node:first').offset().top - $(".content-sub-menu").outerHeight())) {
          passHighestDate = $('.tl-node:first').find('p').text();
        }
        $("#tl-date-dropdown").val(passHighestDate).trigger('change.select2');
        // $(".select2-selection__rendered").text(passHighestDate);
      });
    }

    $('.tl-block').each(function(){
      var top_of_object = $(this).offset().top - 10;
      var bottom_of_window = $(window).scrollTop() + $(window).height();

      if (bottom_of_window>top_of_object) {
        if (!$(this).hasClass("shown")){
        $(this).animate({
          opacity:1,
           paddingTop:0
          // marginTop:0
        },1000,'easeOutCubic',function(){
          $(this).addClass("shown");
        });
      }
      }
    });
  });

  var align = 'r';
  var current_year = '';
  d3.csv("data/ndp-history-events1.csv", function(data) {
    data.forEach(function(d, i) {
        $("#tl-date-dropdown").append('<option>'+d.year+'</option>');
      var htmlcontent = '';

      ///////////////////////////////////////////
      // if (i<=1){
      //   align = (align==='l')?'r':'l';
      // }else{
      //   var prevBlock = $('.tl-block').eq(i-1);
      //   var prevBlock2 = $('.tl-block').eq(i-2);
      //   var prevBlockPos = prevBlock.position().top+prevBlock.offset().top+prevBlock.outerHeight(true);
      //   var prevBlock2Pos = prevBlock2.position().top+prevBlock2.offset().top+prevBlock2.outerHeight(true);
      //   console.log("tl-block"+i+" pos defined");
      //   console.log("blk"+(i-1)+"pos: "+prevBlockPos);
      //   console.log("blk"+(i-2)+"pos: "+prevBlock2Pos);
      //   if (prevBlockPos>prevBlock2Pos){
      //     console.log("tl-block must float to prev2");
      //     console.log("prev2 float r?:"+$('.tl-block').eq(i-2).hasClass("tl-r"));
      //     align = $('.tl-block').eq(i-1).hasClass("tl-r")?"l":"r";
      //   }else if (prevBlockPos<prevBlock2Pos){
      //     console.log("tl-block must float to prev");
      //     console.log("prev float r?:"+$('.tl-block').eq(i-1).hasClass("tl-r"));
      //     align = $('.tl-block').eq(i-2).hasClass("tl-r")?"l":"r";
      //   }else{
      //       align = (align==='l')?'r':'l';
      //       console.log("same height");
      //   }
      // }
      ///////////////////////////////////////////

      align = (align==='l')?'r':'l';
      var contentArr = d.description.split(/(\[ILLUSTRATION-[0-9]\]|\[IMAGE-[0-9]\])/i);
      contentArr = contentArr.filter(function(elem){
        return elem !=="";
      })
      // console.log(contentArr);

      htmlcontent +='<div class="tl-block tl-'+align+'">'+
                     '<div id="opt-'+d.year+'" class="tl-node node-'+align+'"><p>' + d.year + '</p></div>'+
                     '<div class="tl-content node-'+align+'">';
      contentArr.forEach(function(content){
         if (content.match(/(\[ILLUSTRATION-[0-9])/i)){
           var illIndex = content.split(/-|\]/g)[1];
          //  console.log("illst matched");
           htmlcontent +='<figure class="img-segment">'+
                        //  '<img src="images/ndp-timeline/illustrations/GIF/i-'+d.year+'-'+illIndex+'.gif"></figure>';
                         '<img src="images/ndp-timeline/illustrations/'+d.illustration+'"></figure>';
         }else if (content.match(/(\[IMAGE-[0-9])/i)){
          //  console.log("img matched");
           var imgIndex = content.split(/-|\]/g)[1];
           htmlcontent +='<figure class="img-segment">'+
                        //  '<img src="images/ndp-timeline/ndp-images/'+d.year+'-'+imgIndex+'.jpg"></figure>';
                         '<img src="images/ndp-timeline/ndp-images/'+d.images.split(',')[imgIndex-1]+'"></figure>';
         }else{
           htmlcontent +='<p>'+content+'</p>';
         }
      });
      $(".timeline-container").append(htmlcontent);
    });
  });

  // $(".sub-menu-box").on('click',function(){
  //   var clickedBtn = this.id;
  //   var _this = this;
  //   if ($(this).hasClass("clickedBtn")){
  //     $(this).removeClass("clickedBtn");
  //     $(".tl-content").each(function(){
  //       if (!$(this).hasClass(_this.id)){
  //           $(this).find(".filter-layer").css("opacity",0);
  //       }
  //     });
  //     $(".sub-menu-box").css("opacity",1);
  //   }else{
  //     $(this).addClass("clickedBtn");
  //     $(this).siblings(".sub-menu-box").removeClass("clickedBtn");
  //     $(".tl-content").each(function(){
  //       if ($(this).hasClass(_this.id)){
  //           $(this).find(".filter-layer").css("opacity",0);
  //       }else{
  //           $(this).find(".filter-layer").css("opacity",0.7);
  //       }
  //     });
  //     $(_this).siblings(".sub-menu-box").css("opacity",0.7);
  //     $(_this).css("opacity",1);
  //   }
  //
  //
  //
  // });

  $("#tl-date-dropdown").on("change",function(){
    manualChange = true;
    var target="opt-"+d3.select(this).property("value");
    var offset = $(".content-sub-menu").outerHeight();

    $('html,body').animate({
       scrollTop: $("#"+target).offset().top - offset
    },2500,function(){
      var currScroll = $(window).scrollTop();

      if (currScroll !== ($("#"+target).offset().top - offset)){
        $('html,body').animate({
           scrollTop: $("#"+target).offset().top - offset
        },function(){
          manualChange = false;
        });
      }else{
        manualChange = false;
      }
    });
  });
});
