// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";
import * as d3 from "d3";

window.cf = require("./conversational-form");




var screenHeight = $(window).height();
var screenWidth = $(window).width();
var serverData_array = [];
var get_data_array = [];
var totalParticipant = 0;
var info = {};
var url_save_image = "/functions/graphics/save_images";
// var url_save_image="/graphics/functions/graphics/save_images";

var isMobile = (function(a) {
  return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
})(navigator.userAgent || navigator.vendor || window.opera);

function detectIE() {
  var ua = window.navigator.userAgent;
  var ie = ua.search(/(MSIE|Trident)/);

  return ie > -1;
}
$(document).ready(function() {
  if (!detectIE()) document.getElementsByClassName("box-info-ie")[0].style.display = "none";
});
// Animate loader off screen
$(window).load(function() {
  $(".loading-icon ").fadeOut("slow", function() {
    // if (!isMobile)
    document.getElementById("bg_sound").play();
    // else $(".sounddiv").hide();
    $(".headline").delay(200).fadeIn(500);

  });
});

$(function() {
  var introimgheight = 534;
  if (screenWidth > 768) {
    var heightleft = screenHeight - introimgheight;
    var giveheightspace = heightleft / 2;
    $(".page-bg").css("top", giveheightspace);
    $(".headline").css("top", giveheightspace * 0.7);
  }

  $(".sounddiv img").click(function() {
    // $(".sounddiv img").toggle();
    toggleAudio("bg_sound");
  });
});

function chinook_intro_transition() {
  $(".chinook_intro").stop(true, true).animate({
      right: "99%"
    }, transition_speed, "swing",
    function() {
      $(this).css("right", '0');
    });
}

$(document).ready(function() {
  $('#avatar img').click(function() {
    $("#start").delay(300).fadeIn(500);
    $(this).next().prop("checked", "checked");
    $(this).css("opacity", 1);
    if (screenWidth > 768)
      $("#avatar img").not($(this)).css("opacity", 0.5);
  });
});

$(document).ready(function() {
  $("#start").click(function() {
    $("#start").fadeOut(500);
    $(".headline").fadeOut(500);
    $(".content").delay(500).fadeIn(500);
    $("#instructions").stop(500).delay(500).toggle("slide");
    $("#form").show();
    $("#form").css("opacity", "0");
    $(".half").fadeIn(500);
    $(".online").fadeIn(500);
    $("#status").fadeIn(500);
    if (screenWidth < 769) {
      // $(".halfscreen").hide();
      $(".online").hide();

    }
    // if (isMobile) {
    //   document.getElementById("bg_sound").play();
    //   $(".sounddiv").show();
    // }
  });
});

$(document).ready(function() {
  $("#gotit").click(function() {
    $("#gotit").fadeOut();
    $("#instructions").fadeOut();
    $(".online").delay(500).fadeIn(500);
  });
});

$(document).ready(function() {
  $("#start").click(function() {
    $("#background img").css("height", $(".conversational-form-inner").height() + "vh");
    $('.animate-in textarea').focus(function() {
      this.blur();
    });
    var avatarValue = $('input[name=avatar]:checked').val();

    var conversationalForm = window.cf.ConversationalForm.startTheConversation({
      formEl: document.getElementById("form"),
      context: document.getElementById("cf-context"),
      robotImage: "http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/07/create-your-own-ndp/images/merlion.png",
      // userImage: "images/Avatar/" + avatarValue + ".png",
      // userImage: "http://localhost/2017/ndp2017/development/images/Avatar/" + avatarValue + ".png",
      userImage: "http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/07/create-your-own-ndp/images/Avatar/" + avatarValue + ".png",

      submitCallback: function() {
        // be aware that this prevents default form submit.
        var formData = conversationalForm.getFormData();
        var formDataSerialized = conversationalForm.getFormData(true);

        $('#form').find(':input:checked').each(function() {
          $(':input:checked.location').each(function() { //check location
            var location = $(this).val();
          });

          $(':input:checked.location2').each(function() { //check location
            var location = $(this).val();
          });

          $(':input:checked.effects').each(function() { //check effects
            var effects = $(this).val();

            if (screenWidth > 768) {

              $("#" + effects).show();
              $("#" + effects).stop().animate({
                right: 0
              }, 2000);

            } else {
              $("#" + effects).show().addClass("right");
            }
          });

          $(':input:checked.character').each(function() { //check first character
            var character = $(this).val();
            if (screenWidth > 768) {
              $("#" + character).show();
              $("#" + character).stop().animate({
                right: "46%"
              }, 2000);
            } else {
              $("#" + character).show().addClass("left");
            }
          });

          $(':input:checked.character2').each(function() { //check second character
            var character2 = $(this).val();
            if (screenWidth > 768) {
              $("#" + character2).show();
              $("#" + character2).stop().animate({
                right: 0
              }, 2000);
            } else {
              $("#" + character2).show().addClass("right");
            }
          });

          get_data_array.push($(this).val());
          var tmpvalue = $(this).val();

          $("#info img").each(function() {
            if ($(this).attr("id") == tmpvalue + "_shadow" || $(this).attr("id") == tmpvalue + "_shadow2" || $(this).attr("id") == tmpvalue + "_shadow3") {
              get_data_array.push($(this).attr("id"));
              $("#" + tmpvalue + "_shadow3").show();
              $("#" + tmpvalue + "_shadow3").stop().animate({
                right: 0
              }, 2000);
              $("#" + tmpvalue + "_shadow").delay(2000).fadeIn();
              $("#" + tmpvalue + "_shadow2").delay(2000).fadeIn();
            }
          });

          // if (get_data_array[7] == "Fireworks" || get_data_array[3] == "Neon" || get_data_array[3] == "Stiltwalker" || get_data_array[5] == "Neon2" || get_data_array[5] == "Stiltwalker2") {
          //   $("#" + get_data_array[2] + "_night").fadeIn();
          //   get_data_array[2] = get_data_array[2] + "_night";
          //   get_data_array.push(get_data_array[2] + "_night");
          // }
          // if (get_data_array[7] == "Blackknight" || get_data_array[7] == "RedLions" || get_data_array[7] == "Drone" || get_data_array[7] == "Chinook") {
          //   $("#" + get_data_array[2]).fadeIn();
          //   get_data_array.push(get_data_array[2]);
          // }
          // console.log(get_data_array);
        });
        if (get_data_array[7] == "Fireworks" || get_data_array[3] == "Neon" || get_data_array[3] == "Stiltwalker" || get_data_array[5] == "Neon2" || get_data_array[5] == "Stiltwalker2") {
          get_data_array[2] = get_data_array[2] + "_night";
          $("#" + get_data_array[2]).fadeIn();
        } else {
          $("#" + get_data_array[2]).fadeIn();
        }

        // $.each(get_data_array, function(i, value) {
        //   console.log(i);
        //   console.log(value);
        // });
        // location, effect, character 1, charater 2
        serverData_array = [get_data_array[2].replace('_night', ''), get_data_array[3], get_data_array[5].slice(0, -1), get_data_array[7]];

        window.ConversationalForm.remove();
        $(".online").fadeOut(500);
        $(".data").fadeIn(500);
        $(".finalWhiteArea").fadeIn(500);
        // $(".howToGetAward").fadeIn(500);
        $(".mask").show();
        $(".endtext").fadeIn(500);
        $("#form").hide();
        // update_server_data(serverData_array);
        $(".halfscreen").addClass("white");
        $(".instructions").fadeOut();
        $(".container").fadeIn();

        info.convertTocanvas();

      }
    });
  });
});



$("#close").click(function() {
  $(".mask").hide();
  $(".endtext").hide();
});

function toggleAudio(AudioID) {
  var myAudio = document.getElementById(AudioID);
  if (myAudio.paused){
    myAudio.play();
    $(".sounddiv #soundIcon").show();
    $(".sounddiv #muteIcon").hide();
  }else {
    myAudio.pause();
    $(".sounddiv #soundIcon").hide();
    $(".sounddiv #muteIcon").show();
  }
  // return myAudio.paused ? myAudio.play() : myAudio.pause();
}

function update_server_data(Array, data) {
  var tmpCount = 0;
  // pass selection data to php to json data
  // then grab updated json data to display
  // $.post("update_json.php", {
  //   dbArray: Array
  // }, function(data) {
  // console.log(data);
  // get the new data to display
  $.each($.parseJSON(data), function(i, field) {
    // console.log(field.selection_name);
    // console.log(field.count);
    if (i === 0) totalParticipant = field.count;
    else {
      tmpCount = Math.round(field.count / totalParticipant * 100);
      // $("#myBar" + i).animate({
      //   width: tmpCount + "%"
      // });
      $("#myBar" + i).css("width", tmpCount + "%");

      $("#countPerc" + i).html(tmpCount + "%");

      // statistics text
      if (Array[0] == field.selection_name) {
        $("#text" + i).show();
        $("#text" + i).prev().children("p").addClass("makeBold");
      }
      if (Array[1] == field.selection_name) {
        $("#text" + i).show();
        $("#text" + i).prev().children("p").addClass("makeBold");
      }
      if (Array[2] == field.selection_name) {
        $("#text" + i).show();
        $("#text" + i).prev().children("p").addClass("makeBold");
      }
      if (Array[3] == field.selection_name) {
        $("#text" + i).show();
        $("#text" + i).prev().children("p").addClass("makeBold");
      }
    }
  });
  // });
}

$(document).ready(function() {
  $(".images").click(function() {
    var number = $(this).attr("id");
    $("#text" + number).toggle();
  });

  $(".new").click(function() {
    download_image();
  });
});

info.convertTocanvas = function() {

  var image_background = get_data_array[2];
  var image_name_left = get_data_array[3];
  var shadow = get_data_array[4];
  var image_name_right = get_data_array[5];
  var shadow2 = get_data_array[6];
  var image_name_top = get_data_array[7];
  var shadow_effect = get_data_array[8];

  var d_canvas = document.getElementById('canvas');
  var context = d_canvas.getContext('2d');
  var background = document.getElementById(image_background);
  var top = document.getElementById(image_name_top);
  // var shadowtop = document.getElementById(shadow_effect);
  var right = document.getElementById(image_name_right);
  var left = document.getElementById(image_name_left);
  var shadowleft = document.getElementById(shadow);
  var shadowright = document.getElementById(shadow2);

  context.drawImage(background, 0, 0, 800, 1080);
  context.drawImage(top, 280, -50, 500, 450);
  // context.drawImage(shadowtop, 280, -50, 500, 450);
  context.drawImage(shadowright, 400, 550, 400, 500);
  context.drawImage(right, 400, 550, 400, 500);
  context.drawImage(shadowleft, 0, 550, 400, 500);
  context.drawImage(left, 0, 550, 400, 500);

  var imgname = Math.round(new Date().getTime() + (Math.random() * 100000));
  imgname = imgname.toString();
  $(".hiddenfield").html(imgname);
  // yime code
  var imgnameArray = [];
  $(".halfscreen img").each(function(i, d) {
    if ($(d).is(":visible")) {
      imgnameArray += $(d).attr("id");
    }
  });

  var img_data = {
    'img_base64': d_canvas.toDataURL('image/png', 1.0),
    'img_name': imgname,
    "img_ext": "png",
    'data_record': serverData_array
  };
  // $.post("save_image.php", img_data, function() {
  // });

  $.ajax({
    url: url_save_image,
    type: "POST",
    data: img_data,
    dataType: "jsonp",
    success: function(response) {
      if (response.code === "ok") {
        $(".hiddenfield").html(response.image);
        update_server_data(serverData_array, response.record);
      }
    }
  });
};

function download_image() {
  var base64 = document.getElementById('canvas').toDataURL("image/png");
  var a = document.createElement('a');

  a.href = base64;
  a.target = '_blank';
  a.download = 'ndp_parade.png';

  document.body.appendChild(a);
  a.click();
}
