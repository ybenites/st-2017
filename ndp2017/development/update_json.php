<?php
  //Load the file
  $contents = file_get_contents('ndp_data.json');

  //Decode the JSON data into a PHP array.
  $contentsDecoded = json_decode($contents, true);

  // get data from js
  $data = $_REQUEST['dbArray'];
  // $data = ["Float","Chinook","Merdragon","Stiltwalker"];

  // increment number of user
  $contentsDecoded[0]['count']++;

  // increment number of selected option
  foreach ($contentsDecoded as $key => $value) {
    if ($value["selection_name"]==$data[0]) $contentsDecoded[$key]['count']++;
    if ($value["selection_name"]==$data[1]) $contentsDecoded[$key]['count']++;
    if ($value["selection_name"]==$data[2]) $contentsDecoded[$key]['count']++;
    if ($value["selection_name"]==$data[3]) $contentsDecoded[$key]['count']++;

  }

  //Encode the array back into a JSON string.
  $json = json_encode($contentsDecoded);
  //Save the file.
  file_put_contents('ndp_data.json', $json);
  echo $json;
?>
