import $ from "jquery";

import * as d3 from "d3";

var total_questions = 7;
var total_result = 0;
var xmlns = "http://www.w3.org/2000/svg";

var path_chk = "M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809";

$(".content-answer input[type=radio]").change(function(e) {
  e.preventDefault();
  var content_answer = $(this).parents(".content-answer").removeClass("flag_check");
  content_answer.find("input[type=radio]").prop("disabled", true);

  var svgElem = document.createElementNS(xmlns, "svg");
  svgElem.setAttributeNS(null, "viewBox", "0 0 100 100");
  svgElem.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

  var path = document.createElementNS(xmlns, "path");
  svgElem.appendChild(path);
  path.setAttributeNS(null, 'd', path_chk);
  var length = path.getTotalLength();
  path.style.strokeDasharray = length + ' ' + length;
  path.style.strokeDashoffset = Math.floor(length) - 1;

  $(this).parent().append(svgElem);

  total_questions--;
  d3.select(path).transition().duration(500).style("stroke-dashoffset", 0).on("end", show_card_semiresult);

});

$(".btn-play-again").on("click", function() {
  total_questions = 7;
  total_result = 0;
  d3.selectAll(".st-question svg").transition().duration(500).attr("opacity", 0).remove();
  d3.selectAll(".content-answer").classed("flag_check", true);
  $(".content-answer input[type=radio]").prop("disabled", false);

  $(".content-partial-result").animateCss("fadeOut", "out");
  $(".st-question:not(.question1)").animateCss("fadeOut", "out");
  $(".credits-section").animateCss("fadeOut", "out");
  $(".content-fb-comments").animateCss("fadeOut", "out");

  // $('html,body').animate({
  //   scrollTop: $(".question1").offset().top
  // },500);
});

$(".content-partial-result img").on("click", function() {
  var current_card = $(this).parents(".st-question").index(".st-question");
  var next_card = $(".st-question").get(current_card + 1);
  $('html,body').animate({
    scrollTop: $(next_card).offset().top
  }, 500);
});

$(".pmlee-share-tw").on("click", function(e) {
  e.preventDefault();
  var description=getDescription(total_result);
  var text = description+" %23ndrsg";
  var via = 'STcom';
  var url = 'http://str.sg/pmleequiz';
  share_twitter(text, via, url)
});

$(".pmlee-share-fb").on("click", function(e) {
  e.preventDefault();
  var description=getDescription(total_result);

  share_face_book(description);
});

function getDescription(total_result){
  var description;
  if(total_result<0){
    description = "I scored worse than PM Lee. What about you? Take the healthy lifestyle quiz.";
  }
  if(total_result===0){
    description = "I scored the same as PM Lee. What about you? Take the healthy lifestyle quiz.";
  }
  if(total_result>0){
    description ="I scored better than PM Lee. What about you? Take the healthy lifestyle quiz.";
  }
  return description
}
function share_twitter(text, via, url) {
  window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
}
function share_face_book(description) {
  FB.ui({
    method: 'feed',
    link: window.location.href,
    hashtag: "#ndrsg",
    quote: description
  }, function(response) {
    if (response && !response.error_message) {
      $(".btn-play-again").trigger("click");
    } else {}
  });
}

function show_card_semiresult(d) {
  var current_value = parseInt($(this).parents(".answer").find("input").val());
  total_result = total_result + current_value;
  var current_question = $(this).parents(".st-question");
  $('html,body').animate({
    scrollTop: $(current_question).find(".text-ask").offset().top
  }, 500);

  current_question.find(".content-partial-result").animateCss("fadeIn", "in");

  var next_card = $(".st-question").get(current_question.index(".st-question") + 1);

  if (total_questions === 0) {
    show_result(total_result);
    $(".credits-section").animateCss("fadeIn", "in");
    $(".content-fb-comments").animateCss("fadeIn", "in");
  }
  $(next_card).animateCss("fadeIn", "in");
}
function show_result(total) {
  $(".as-result").addClass("none");
  if (total < 0) {
    $(".txt-score-result-lee").html("worse");
    $(".img-score-result").attr("src", require("../images/pm-lee-quiz/emoji-worst.jpg"));

    $(".img-score-result-lee").attr("src", require("../images/pm-lee-quiz/worse-than-pm.gif"));
    $(".txt-after-result").html("than");
  }
  if (total === 0) {
    $(".as-result").removeClass("none");
    $(".txt-score-result-lee").html("same");
    $(".img-score-result").attr("src", require("../images/pm-lee-quiz/emoji-equal.jpg"));

    $(".img-score-result-lee").attr("src", require("../images/pm-lee-quiz/same-as-pm.gif"));
    $(".txt-after-result").html("as");
  }
  if (total > 0) {
    $(".txt-score-result-lee").html("better");
    $(".img-score-result").attr("src", require("../images/pm-lee-quiz/emoji-better.jpg"));

    $(".img-score-result-lee").attr("src", require("../images/pm-lee-quiz/pm-lee-better.gif"));
    $(".txt-after-result").html("than");
  }
}
