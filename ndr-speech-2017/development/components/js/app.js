var Hammer = require("./hammer.min");

var d3 = require('./d3.min');

var $ = require('./jquery-1.11.2.min');
window.jQuery = $;


require('./bootstrap.min');
require('./chosen.jquery.min');
require('./autolink-min');


var data = {};
data.speechs = {};

data.speechs = require('./datajson');

data.words = [];
data.categories = {};
data.twitter = [];
var year_default = 2017;
var option_t = '<option value="[VALUE]">[TEXT]</option>';
var expresion_search_n = /(?:\n)/g;
var a_all_data_desordenada = {};
var a_all_data_ordenada_etiq = {};
var a_all_data_category_ordenada = {};
var width_text_filter = 0;
var height_text_filter = 0;
var height_tag_scale = 0;
var height_text_scale;
var check_is_filtered = 0;
var just_one_cal = 0;
var last_position_minimap = 0;
var direction_mov = 0;
var action_scroll = 1;
var hash_word = window.location.hash;



var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
  FB.init({
    appId: fbAppId, // App ID
    status: true, // check login status
    cookie: true, // enable cookies to allow the
    // server to access the session
    xfbml: true, // parse page for xfbml or html5
    // social plugins like login button below
    version: 'v2.0', // Specify an API version
  });

  // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// variables to animate button option
var pathDefs = {
  fill: ['M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809']
};
var animDefs = {
  fill: {
    speed: 0.8,
    easing: 'ease-in-out'
  }
};


var path_csv_categories = 'csv/categories.csv';
var path_csv_sgbudget = "csv/sgbudget.csv";
$(document).ready(function() {




  // $(".st_menu_mobile").on('click', function() {
  //   $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
  //   $(".st_content_shared_social").toggleClass('st_dialogIsOpen');
  //
  //   eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
  // });
  d3.selectAll(".list-option-radio div").each(function() {
    d3.select(this).append('svg').attr('viewBox', '-7 -7 110 110');
  });

  document.getElementById("rad-speechs").checked = true;
  var svg_from_rad = d3.select(d3.select("#rad-speechs").node().parentNode).select('svg');
  draw_radio_button(svg_from_rad, animDefs);


  d3.selectAll(".rad-speech").on("change", function() {
    d3.selectAll('.list-option-radio path').remove();

    var svg_from_rad = d3.select(this.parentNode).select('svg');
    draw_radio_button(svg_from_rad, animDefs);
    if (document.getElementById("rad-speechs").checked) {
      $(".group_text_map,.div_head_twitter").removeClass('content_a');
      $(".content_tweet,.div_head_twitter").addClass('content_a');
      $(".refresh_display_filter").trigger('click');
    } else {
      $(".group_text_map,.div_head_twitter").addClass('content_a');
      $(".content_tweet,.div_head_twitter").removeClass('content_a');
    }
  });

  $(".btn_shared_face").on('click', function(e) {
    e.preventDefault();
    var image = 'http://graphics.straitstimes.com/STI/STIMEDIA/facebook_images/ndr2015/ndr2015speech.png';
    var name = "Explore PM Lee’s SG50 NDRSG speech";
    var description = "The entire #NDRSG transcript of PM Lee’s jubilee speech is inside this tool so you can quickly find the most discussed themes, words or search for your own. Great for those who didn’t watch it!";

    share_face_book(image, name, description);
    return false;
  });

  $(".btn_shared_twitter").on('click', function(e) {
    e.preventDefault();
    var text = "Explore PM Lee’s SG50 %23NDRSG speech with this transcript analyser and search tool ";
    var via = 'STcom';
    var url = 'http://str.sg/Z7Pj';
    share_twitter(text, via, url);
    return false;
  });
  $(".go_to_st").on('click', function() {
    window.open("http://www.straitstimes.com/", "_blank");
  });

  data.add_word = function(name) {
    var obj_parraf_word = {};
    var o_words = {};
    o_words.words = name;
    o_words.category = 'Libre';
    $.each(data.speechs, function(year) {
      var a_all_word_by_year = [];
      a_all_word_by_year.push(data.map_word(o_words, year));
      obj_parraf_word[year] = a_all_word_by_year;
    });
    construct_all_text_div(obj_parraf_word, name);
  };

  data.paragraph_process = function(a_category, text_cut) {
    var obj_parraf_process = {};
    obj_parraf_process.u_parraf = text_cut;
    obj_parraf_process.quantity_word = 0;
    var o_data_parraf_by_word;
    a_category.forEach(word_by_category => {
      o_data_parraf_by_word = data.generate_parraf_by_text(obj_parraf_process.u_parraf, word_by_category);
      obj_parraf_process.u_parraf = o_data_parraf_by_word.u_parraf;
      obj_parraf_process[word_by_category] = o_data_parraf_by_word.quantity_word;
      obj_parraf_process.quantity_word += o_data_parraf_by_word.quantity_word;
    });
    return obj_parraf_process;
  };

  data.map_word = function(o_word, year, a_category, category_name) {
    var count_total_words = 0;
    var count_total_word_category = 0;
    var a_u_parraf_copy = [];
    var a_u_parraf = [];
    var a_u_parraf_category = [];
    data.speechs[year].forEach(function(speech_info) {
      var res_exec = {};
      var lastIindex = res_exec.index = 0;
      while ((res_exec = expresion_search_n.exec(speech_info.speech))) {
        var text_cut;
        if (res_exec.index > lastIindex) {
          text_cut = speech_info.speech.substring(lastIindex, res_exec.index);
          a_u_parraf.push(text_cut);
        }
        lastIindex = expresion_search_n.lastIndex;

        var o_data_parraf_by_word;
        if (a_category !== undefined && category_name !== undefined) {
          var obj_parraf_process = {};
          // obj_parraf_process.u_parraf = text_cut;
          // obj_parraf_process.quantity_word = 0;
          obj_parraf_process = data.paragraph_process(a_category, text_cut);
          // a_category.forEach(function(word_by_category) {
          //   o_data_parraf_by_word = data.generate_parraf_by_text(obj_parraf_process.u_parraf, word_by_category);
          //   obj_parraf_process.u_parraf = o_data_parraf_by_word.u_parraf;
          //   obj_parraf_process[word_by_category] = o_data_parraf_by_word.quantity_word;
          //   obj_parraf_process.quantity_word += o_data_parraf_by_word.quantity_word;
          // });

          a_u_parraf_category.push(obj_parraf_process);
          count_total_word_category += obj_parraf_process.quantity_word;
        } else {
          o_data_parraf_by_word = data.generate_parraf_by_text(text_cut, o_word.words);
          a_u_parraf_copy.push(o_data_parraf_by_word);
          count_total_words += o_data_parraf_by_word.quantity_word;
        }
      }

      var text_cut2 = speech_info.speech.substring(lastIindex, speech_info.speech.length);
      if (text_cut2 !== '') {
        var o_data_parraf_by_word2;
        if (a_category !== undefined && category_name !== undefined) {
          var obj_parraf_process2 = {};
          obj_parraf_process2.u_parraf = text_cut2;
          obj_parraf_process2.quantity_word = 0;
          a_category.forEach(function(word_by_category) {
            o_data_parraf_by_word2 = data.generate_parraf_by_text(obj_parraf_process2.u_parraf, word_by_category);
            obj_parraf_process2.u_parraf = o_data_parraf_by_word2.u_parraf;
            obj_parraf_process2[word_by_category] = o_data_parraf_by_word2.quantity_word;
            obj_parraf_process2.quantity_word += o_data_parraf_by_word2.quantity_word;
          });
          a_u_parraf_category.push(obj_parraf_process2);
          count_total_word_category += obj_parraf_process2.quantity_word;
        } else {
          a_u_parraf.push(text_cut2);
          o_data_parraf_by_word2 = data.generate_parraf_by_text(text_cut2, o_word.words);
          a_u_parraf_copy.push(o_data_parraf_by_word2);
          count_total_words += o_data_parraf_by_word2.quantity_word;
        }
      }
    });
    if (a_category !== undefined && category_name !== undefined) {
      return {
        count_total_words: count_total_word_category,
        word: category_name,
        a_parraf_copy: a_u_parraf_category
      };
    } else {
      a_all_data_desordenada[year] = a_u_parraf;
      return {
        count_total_words: count_total_words,
        word: o_word.words.trim(),
        a_parraf_copy: a_u_parraf_copy,
        category: o_word.category.trim()
      };
    }
  };

  data.generate_parraf_by_text = function(u_parraf, text) {
    var count_word_by_parraf = 0;
    var paragraphs_u_copy;
    if (u_parraf !== '') {
      var expresion_search_words = new RegExp("\\b(" + d3.requote(text.trim()) + ")\\b", "gi");
      var res_exec_p = {};
      var lastIndex_p = res_exec_p.index = 0;

      var cut_bucle = 0;
      while ((res_exec_p = expresion_search_words.exec(u_parraf))) {
        count_word_by_parraf++;
        if (cut_bucle === 0) {
          lastIndex_p = expresion_search_words.lastIndex;
          paragraphs_u_copy = u_parraf.replace(expresion_search_words, '<span class="word_select_extern">$1 <span class="cdr_total"></span></span>');
          expresion_search_words.lastIndex = lastIndex_p;
          cut_bucle++;
        }
      }
    }
    return {
      quantity_word: count_word_by_parraf,
      u_parraf: (count_word_by_parraf === 0) ? u_parraf : paragraphs_u_copy
    };
  };

  data.count_words_by_text = function(text_complete) {
    var regex = /\s+/gi;
    return text_complete.trim().replace(regex, ' ').split(' ').length;
  };


  d3.csv(path_csv_sgbudget, function(error, data_csv) {
    data.twitter = data_csv;
    if (hash_word) {
      var word_selected = hash_word.split("#")[1].trim();
      construct_all_tweet_div(data.twitter, word_selected);
    } else {
      construct_all_tweet_div(data.twitter);
    }
  });



  d3.csv(path_csv_categories, function(error, data_csv) {
    var data_csv_u = data_csv;
    $('.chosen-select-words').append(option_t.replace('[VALUE]', '').replace('[TEXT]', ''));
    $('.chosen-select-categories').append(option_t.replace('[VALUE]', '').replace('[TEXT]', ''));

    $("#div_category").html("");

    var a_categories_compare = [];
    data_csv_u.forEach(function(o_word) {
      if (o_word.words.trim()) {
        var unit_word = "<div class='unit_word pull-left' data-word='" + o_word.words.trim() + "' >";
        unit_word += "<div>" + o_word.words.trim() + "</div>";
        unit_word += "</div>";
        $("#div_words").append(unit_word);


        $('.chosen-select-words').append(option_t.replace('[VALUE]', o_word.words.trim()).replace('[TEXT]', o_word.words.trim()));
        data.words.push({
          name: o_word.words.trim(),
          number: -1
        });
      }

      if (o_word.category) {
        if ($.inArray(o_word.category, a_categories_compare) < 0) {
          var unit_category = "<div class='unit_category pull-left' data-cat='" + o_word.category.trim() + "' >";
          unit_category += "<div><img src='images/NDR/categories/" + o_word.category.trim() + ".svg' class='img-responsive center-block'  /></div>";
          unit_category += "<div>" + o_word.category.trim() + "</div>";
          unit_category += "</div>";
          $("#div_category").append(unit_category);

          $('.chosen-select-categories').append(option_t.replace('[VALUE]', o_word.category.trim()).replace('[TEXT]', o_word.category.trim()));
          a_categories_compare.push(o_word.category.trim());
          data.categories[o_word.category.trim()] = {
            name: o_word.category.trim(),
            number: -1,
            words: [o_word.cat_words.trim()],
          };
        } else {
          data.categories[o_word.category.trim()].words.push(o_word.cat_words.trim());
        }
      }
    });

    $.each(data.speechs, function(year, text_speech) {
      var a_all_word_by_year = [];
      var a_all_word_category_by_year = [];
      data_csv_u.forEach(function(o_word) {
        if (o_word.words) {
          var obj_etiq = data.map_word(o_word, year);
          a_all_word_by_year.push(obj_etiq);
        }
      });
      a_all_data_ordenada_etiq[year] = a_all_word_by_year;

      $.each(data.categories, function(head, bod) {
        var obj_etiq_category = data.map_word('', year, bod.words, head);
        a_all_word_category_by_year.push(obj_etiq_category);
      });
      a_all_data_category_ordenada[year] = a_all_word_category_by_year;
    });

    if (hash_word) {
      var word_selected = hash_word.split("#")[1].trim();
      data.add_word(word_selected);
    } else {
      construct_all_text_div(a_all_data_desordenada);
    }



    $(".unit_word").on("click", function() {
      if (!$(this).hasClass('wordActive')) {
        $(".unit_word").removeClass('wordActive');
        $(this).addClass('wordActive');
        var word_selected = $(this).data("word");
        if (word_selected) {
          construct_all_text_div(a_all_data_ordenada_etiq, word_selected);
          construct_all_tweet_div(data.twitter, word_selected);
          $(".unit_category").removeClass('categoryActive');
        }
      }
    });
    $(".unit_category").on("click", function() {
      if (!$(this).hasClass('categoryActive')) {
        $(".unit_category").removeClass('categoryActive');
        $(this).addClass('categoryActive');
        var category_selected = $(this).data("cat");
        if (category_selected) {
          construct_all_text_div(a_all_data_category_ordenada, category_selected.trim(), true);
          construct_all_tweet_div(data.twitter, category_selected, true);
          $(".unit_word").removeClass('wordActive');
        }
      }
    });
    $('#tab_mob a').click(function(e) {
      e.preventDefault();
      $(".refresh_display_filter").trigger('click');
    });
    $('#tab_desk a').click(function(e) {
      e.preventDefault();
      $(".refresh_display_filter").trigger('click');
      $(".unit_word").removeClass('wordActive');
      $(".unit_category").removeClass('categoryActive');
    });

    $('.chosen-select-words').chosen({
      no_results_text: "Oops, nothing found!",
      allow_single_deselect: true,
      disable_search: true
    }).on('change', function() {
      var word_selected = $(this).find('option:selected').val();
      if (word_selected) {
        construct_all_text_div(a_all_data_ordenada_etiq, word_selected);
        construct_all_tweet_div(data.twitter, word_selected);

        $(".word_searched_hide").data('typesearchcategory', 0);
        $(".word_searched_hide").val(word_selected.trim());

        $('.input_word_extern').val('');
        $('.chosen-select-categories').val('').trigger("chosen:updated");

        // $('.chosen-select-words').trigger('chosen:close');
        // $('.chosen-container .chosen-drop input').blur();
      } else {
        $(".refresh_display_filter").trigger('click');
      }
      return false;
    });

    $('.chosen-select-categories').chosen({
      no_results_text: "Oops, nothing found!",
      allow_single_deselect: true,
      disable_search: true
    }).on('change', function(e, d) {
      var word_selected = $(this).find('option:selected').val();
      if (word_selected.trim()) {
        $(".word_searched_hide").data('typesearchcategory', 1);
        $(".word_searched_hide").val(word_selected.trim());

        construct_all_text_div(a_all_data_category_ordenada, word_selected.trim(), true);
        construct_all_tweet_div(data.twitter, word_selected.trim(), true);
        $('.input_word_extern').val('');
        $('.chosen-select-words').val('').trigger("chosen:updated");
      } else {
        $(".refresh_display_filter").trigger('click');
      }
      return false;
    });

    $(".chosen-select-words,.chosen-select-categories").on('chosen:hiding_dropdown', function() {
      var var_this = this;
      hide_keyboorad(var_this);
    });
    $(".chosen-select-words,.chosen-select-categories").on('chosen:showing_dropdown', function() {
      var var_this = this;
      $(var_this).parent().find('input').focus();
    });

  });

  $(".input_word_extern").on('keyup', function(e) {
    if (e.keyCode === 13) {
      $(".add_word_filter").trigger('click');
    } else if ($(this).val() === '') {
      construct_all_text_div(a_all_data_desordenada);
      construct_all_tweet_div(data.twitter);
    }
    return false;
  });
  $(".add_word_filter").on('click', function() {
    var word_enter;
    if ($($(".input_word_extern")[0]).is(':visible')) {
      word_enter = $($(".input_word_extern")[0]).val();
    } else if ($($(".input_word_extern")[1]).is(':visible')) {
      word_enter = $($(".input_word_extern")[1]).val();
    }
    if (word_enter.trim()) {
      $(".word_searched_hide").data('typesearchcategory', 0);
      $(".word_searched_hide").val(word_enter.trim());

      data.add_word(word_enter.trim());
      construct_all_tweet_div(data.twitter, word_enter.trim());

    } else if (word_enter.trim() === '') {
      construct_all_text_div(a_all_data_desordenada);
      construct_all_tweet_div(data.twitter);
    }
    $('.chosen-select-categories').val('').trigger("chosen:updated");
    $('.chosen-select-words').val('').trigger("chosen:updated");
    return false;
  });

  $(".refresh_display_filter").on('click', function() {
    $('.input_word_extern').val('');
    $('.chosen-select-categories').val('').trigger("chosen:updated");
    $('.chosen-select-words').val('').trigger("chosen:updated");

    $(".word_searched_hide").data('typesearchcategory', 0);
    $(".word_searched_hide").val('');

    construct_all_text_div(a_all_data_desordenada);
    construct_all_tweet_div(data.twitter);
    return false;
  });

  $(".div_content_text_filter").scroll(function(e) {
    if (check_is_filtered === 0) {
      //var scroll_top_prop=($(this).scrollTop()*$(".div_content_text_filter").height())/$(".div_content_text_filter")[0].scrollHeight;

      var scroll_top_prop = ($(this).scrollTop() * height_tag_scale) / ($(".div_content_text_filter")[0].scrollHeight);
      if (action_scroll == 1) {
        last_position_minimap = scroll_top_prop;
        $('.div_content_text_' + year_default + ' .magnifier_text').css({
          'top': scroll_top_prop
        });
      }
    } else {
      var height_mark_word = $(".div_content_text_filter .word_select_extern").height();
      $(".div_content_text_filter .word_select_extern").each(function() {
        var pos_index, tag_word_select;
        if ($(this).position().top < height_text_scale - height_mark_word / 2 && $(this).position().top > -height_mark_word / 2) {
          pos_index = $(".div_content_text_filter .word_select_extern").index(this);
          // var tag_word_select=$(".tag_scale_text:last .word_select_extern").get(pos_index);
          tag_word_select = $(".div_content_text_" + year_default + " .tag_scale_text .word_select_extern").get(pos_index);
          $(tag_word_select).find('.cdr_total').css({
            'background': '#0880af',
            'color': '#ffffff'
          });

        } else {
          pos_index = $(".div_content_text_filter .word_select_extern").index(this);
          tag_word_select = $(".div_content_text_" + year_default + " .tag_scale_text .word_select_extern").get(pos_index);
          $(tag_word_select).find('.cdr_total').css({
            'background': '#333333',
            'color': '#ffffff'
          });
        }
      });
    }
  });

  $(".group_text_map").on('click', '.tag_scale_text_active .cdr_total', function() {
    var pos_tag = $('.tag_scale_text_active .word_select_extern').index($(this).parent('span'));
    var tag_filter = $(".div_content_text_filter .word_select_extern").get(pos_tag);
    var pos_tag_filter = $(tag_filter).position().top;
    var scroll_actual = $('.div_content_text_filter').scrollTop();
    var move_scroll = pos_tag_filter - (height_text_scale / 2) + scroll_actual;
    $('.div_content_text_filter').scrollTop(move_scroll);
  });

  $('.btn_div_content_year').on('click', function() {
    $(".btn_div_content_year").removeClass('active');
    $(this).addClass('active');

    var val_year_select = parseInt($(this).parent('div').data('value'));
    if (check_is_filtered === 0) {
      year_default = val_year_select;
      construct_all_text_div(a_all_data_desordenada);
    } else {
      var word_choose = $(".word_searched_hide").val();
      var type_search_category = $('.word_searched_hide').data('typesearchcategory');
      year_default = val_year_select;
      if (type_search_category === 0) {
        data.add_word(word_choose.trim());
      } else {
        construct_all_text_div(a_all_data_category_ordenada, word_choose.trim(), true);
      }
    }
    return false;
  });

  window.addEventListener('load', function() {
    document.body.addEventListener('touchstart', function(e) {
      if ($(e.target).parents("input").length === 0) {
        $('input').blur();
        $(".chosen-select-words").trigger("click");
        $(".chosen-select-categories").trigger("click");
      }
    }, false);
  }, false);

  $(window).on('resize', function() {
    var sttime = setTimeout(function() {
      construct_all_text_div(a_all_data_desordenada);
      clearTimeout(sttime);
    }, 300);
  });


  function construct_all_tweet_div(data_csv_u, word, category) {
    var div_all_tweet = "<div>";
    var number_tweets = 0;
    var text_head_tweet = "";
    $(".text_dinamic_show").html("");
    data_csv_u.forEach(function(d) {
      var obj_recover;
      var div_tweet;
      if (word !== undefined && category === undefined) {
        var word_filter = word;
        obj_recover = data.generate_parraf_by_text(d.Message, word_filter);
        if (obj_recover.quantity_word > 0) {
          div_tweet = "<div class='unit_tweet'>";
          div_tweet += "<div class='t_account'>" + d.Account + "</div>";
          div_tweet += "<div class='t_date'>" + d.date + "</div>";
          div_tweet += "<div class='t_message'>" + obj_recover.u_parraf.autoLink() + "</div>";
          // div_tweet += "<div>" + d.url + "</div>";
          div_tweet += "</div>";
          div_all_tweet += div_tweet;
          number_tweets++;
          text_head_tweet = "<span class='bold_number'>" + number_tweets + "</span> tweet(s) containing the word " + word.toUpperCase();
        }
      } else if (word !== undefined && category) {
        if (data.categories[word] !== undefined) {
          obj_recover = {};
          obj_recover.u_parraf = d.Message;
          obj_recover.quantity_word = 0;
          if (data.categories[word].words !== undefined && data.categories[word].words.length > 0) {
            data.categories[word].words.forEach(function(wd) {
              var obj_rec = data.generate_parraf_by_text(obj_recover.u_parraf, wd);
              obj_recover.u_parraf = obj_rec.u_parraf;
              obj_recover.quantity_word += obj_rec.quantity_word;
            });
            if (obj_recover.quantity_word > 0) {
              div_tweet = "<div class='unit_tweet'>";
              div_tweet += "<div class='t_account'>" + d.Account + "</div>";
              div_tweet += "<div class='t_date'>" + d.date + "</div>";
              div_tweet += "<div class='t_message'>" + obj_recover.u_parraf.autoLink() + "</div>";
              // div_tweet += "<div>" + d.url + "</div>";
              div_tweet += "</div>";
              div_all_tweet += div_tweet;
              number_tweets++;
              text_head_tweet = "<span class='bold_number'>" + number_tweets + "</span> tweet(s) containing words related to " + word.toUpperCase();
            }
          }
        }

      } else {
        div_tweet = "<div class='unit_tweet'>";
        div_tweet += "<div class='t_account'>" + d.Account + "</div>";
        div_tweet += "<div class='t_date'>" + d.date + "</div>";
        div_tweet += "<div class='t_message'>" + d.Message.autoLink() + "</div>";
        // div_tweet += "<div>" + d.url + "</div>";
        div_tweet += "</div>";
        div_all_tweet += div_tweet;
        number_tweets++;
        text_head_tweet = "Total <span class='bold_number'>" + number_tweets + "</span> tweets";
      }
    });
    div_all_tweet += "</div>";
    $(".text_dinamic_show").html(text_head_tweet);
    $(".content_tweet").html(div_all_tweet);
  }

  function construct_all_text_div(obj_data_speech, text_filter, by_category) {
    $(".tag_scale_text").css({
      "transform": "scaleX(1) scaleY(1)",
      "-ms-transform": "scaleX(1) scaleY(1)",
      "-webkit-transform": "scaleX(1) scaleY(1)"
    });
    height_text_filter=0;

    last_position_minimap = 0;
    width_text_filter = $('.div_content_text_filter').width();
    var div_content_search = ($(window).height() > 460) ? $('.div_content_search').outerHeight(true) + 50 : 0;
    height_text_scale = $(window).height() - div_content_search - $(".div_head_text_filter").outerHeight(true);

    $(".content_tweet").height(height_text_scale);

    $('.div_content_text_filter').css({
      'height': height_text_scale,
      'overflow-y': 'scroll',
      'overflow-x': 'hidden',
      '-webkit-overflow-scrolling': 'touch',
    });

    // blucle all speechs
    var scale_calc_x;
    var count_parraf_width_words = 0;
    var string_parraf_filter = '';
    $.each(obj_data_speech, function(year, a_data_speech_by_year) {
      var count_any_words = 0;
      $(".div_content_text_" + year).html('');

      var res_exec = {};

      var width_content_scale = $(".div_content_text_" + year).width();
      scale_calc_x = width_content_scale / width_text_filter;
      var tag_active_or_desactive = (parseInt(year) === year_default) ? "tag_scale_text_active" : "";
      var string_parraf = '<div class="tag_scale_text ' + tag_active_or_desactive + '">';
      a_data_speech_by_year.forEach(function(u_parraf) {
        if (u_parraf instanceof Object) {
          if (u_parraf.word === text_filter) {
            u_parraf.a_parraf_copy.forEach(function(o_parr) {
              count_any_words += parseInt(o_parr.quantity_word);
              string_parraf += '<p><span>' + o_parr.u_parraf + '</span></p>';
              if (parseInt(year) === year_default && parseInt(o_parr.quantity_word) > 0) {
                count_parraf_width_words++;
                string_parraf_filter += '<p><span>' + o_parr.u_parraf + '</span></p>';
              }
            });
          }
        } else {
          count_any_words += parseInt(data.count_words_by_text(u_parraf));

          string_parraf += '<p><span>' + u_parraf + '</span></p>';
          if (parseInt(year) === year_default) {
            string_parraf_filter += '<p><span>' + u_parraf + '</span></p>';
          }
        }
      });

      string_parraf += '</div><div class="magnifier_text"></div>';
      $(".div_content_text_" + year).height(height_text_scale);
      $(".div_content_text_" + year).html(string_parraf);
      $(".div_content_text_filter").html(string_parraf_filter);

      $(".div_count_word_" + year).html("<span>Total words:</span> <span>" + format_number(count_any_words) + "</span>");
      $(".div_content_year_" + year).data('value', year);
      // $(".div_content_year_"+year).html(year);
    });

    if (text_filter === undefined) {
      $(".div_head_text_filter").html('');
    } else {
      if (by_category === undefined) {
        $(".div_head_text_filter").html('<div><span>' + count_parraf_width_words + '</span> paragraph(s) containing the word <span>' + text_filter + '</span></div><hr>');

      } else {
        $(".div_head_text_filter").html('<div><span>' + count_parraf_width_words + '</span> paragraph(s) containing words related to <span>' + text_filter + '</span></div><hr>');
      }
    }



    $(".tag_scale_text").css({
      width: width_text_filter
    });

    if (just_one_cal === 0) {
      $(".tag_scale_text").each(function() {
        if (height_text_filter < $(this).height()) {
          height_text_filter = $(this).height();
        }
      });
    }


    var scale_calc_y = height_text_scale / height_text_filter;

    $(".tag_scale_text").css({
      "transform": "scaleX(" + scale_calc_x + ") scaleY(" + scale_calc_y + ")",
      "-ms-transform": "scaleX(" + scale_calc_x + ") scaleY(" + scale_calc_y + ")",
      "-webkit-transform": "scaleX(" + scale_calc_x + ") scaleY(" + scale_calc_y + ")"
    });

    //if(text_filter===undefined)height_text_filter_personal=$(".div_content_text_filter")[0].scrollHeight;

    height_tag_scale = $('.div_content_text_' + year_default + ' .tag_scale_text').height() * scale_calc_y;
    $('.div_content_text_' + year_default + ' .magnifier_text').css({
      'height': $(".div_content_text_filter").height(),
      'transform': "scaleY(" + scale_calc_y + ")",
      '-ms-transform': "scaleY(" + scale_calc_y + ")",
      '-webkit-transform': "scaleY(" + scale_calc_y + ")"
    });

    if (text_filter !== undefined) {
      check_is_filtered = 1;
      $('.div_content_text_' + year_default + ' .magnifier_text').css({
        height: 0
      });
    } else {
      check_is_filtered = 0;
    }

    $(".div_content_year_" + year_default + " .btn_div_content_year").addClass('active');
    $(".div_content_text_filter").scrollTop(1);
    $(".div_content_text_filter").scrollTop(0);

    var myElement = $('.div_content_text_' + year_default + ' .magnifier_text')[0];
    
    var mc = new Hammer(myElement);

    // let the pan gesture support all directions.
    // this will block the vertical scrolling on a touch-device while on the element
    mc.get('pan').set({
      direction: Hammer.DIRECTION_ALL
    });

    // listen to events...

    var height_real_minimap = $('.div_content_text_' + year_default + ' .magnifier_text').height() * scale_calc_y;
    mc.on("panup pandown", function(ev) {
      $(".tag_scale_text").addClass("user_select");
      $(".div_content_text_filter").addClass("user_select");

      action_scroll = 0;
      direction_mov = ev.offsetDirection;
      var move_intern = (direction_mov != 8) ? -ev.distance : ev.distance;
      var new_top = ((last_position_minimap + move_intern) / scale_calc_y);
      if (last_position_minimap + move_intern >= 0 && last_position_minimap + move_intern + height_real_minimap <= height_tag_scale) {
        $('.div_content_text_' + year_default + ' .magnifier_text').css({
          top: last_position_minimap + move_intern
        });
        $(".div_content_text_filter").scrollTop(new_top);
      } else {
        if (last_position_minimap + move_intern < 0) {
          $('.div_content_text_' + year_default + ' .magnifier_text').css({
            top: 0
          });
        }
        if (last_position_minimap + move_intern + height_real_minimap > height_tag_scale) {
          $('.div_content_text_' + year_default + ' .magnifier_text').css({
            top: height_tag_scale - height_real_minimap
          });
        }
        $(".div_content_text_filter").scrollTop(new_top);
      }
    });
    mc.on("panend", function(ev) {
      $(".tag_scale_text").removeClass("user_select");
      $(".div_content_text_filter").removeClass("user_select");
      action_scroll = 1;
      var move_intern = (direction_mov != 8) ? -ev.distance : ev.distance;
      if (last_position_minimap + move_intern >= 0 && last_position_minimap + move_intern + height_real_minimap <= height_tag_scale) {
        last_position_minimap += move_intern;
      } else {
        if (last_position_minimap + move_intern < 0) last_position_minimap = 0;
        if (last_position_minimap + move_intern + height_real_minimap > height_tag_scale) last_position_minimap = height_tag_scale - height_real_minimap;
      }
    });
  }

  function hide_keyboorad(var_this) {
    var time_close_c = setTimeout(function() {
      if ($(var_this).parent().find('input').is(':focus')) {
        $(var_this).parent().find('input').blur();
        clearTimeout(time_close_c);
      } else {
        clearTimeout(time_close_c);
        hide_keyboorad(var_this);
      }
    }, 100);
  }

  function format_number(number) {
    Math.floor(number);
    var val_return = 0;
    var format_number_int = d3.format(",d"),
      format_number_decimal = d3.format(",.2f");
    var prom = number % 1;
    if (prom === 0) {
      val_return = format_number_int(number);
    } else {
      if (format_number_decimal(prom) % 1 === 0) {
        val_return = format_number_int(Math.floor(number) + 1);
      } else {
        val_return = format_number_decimal(number);
      }
    }
    return val_return;
  }

  function share_face_book(image, name, description) {
    FB.ui({
      method: 'feed',
      link: window.location.href,
      caption: 'www.straitstimes.com',
      picture: image,
      name: name,
      description: description
    });
  }

  function share_twitter(text, via, url) {
    window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
  }

  function draw_radio_button(svg_from_rad, animDefs) {
    var path = svg_from_rad.append('path').attr('d', pathDefs.fill[0]);
    var length_path = path.node().getTotalLength();

    path.attr("stroke-dasharray", length_path + " " + length_path).attr("stroke-dashoffset", length_path)
      .transition()
      .duration(500)
      .ease(animDefs.fill.easing, animDefs.fill.speed)
      .attr("stroke-dashoffset", 0);
  }

  // function eventCloseMenu(event) {
  //   if (event) {
  //     $(".st_menu_mobile").css('right', '10px');
  //
  //     d3.select(".first_line").transition().duration(500).attr("x1", 12.8).attr("y1", 12.2).attr("x2", 23.5).attr("y2", 22.8);
  //     d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 0).attr("x2", 0);
  //     d3.select(".second_line").transition().duration(500).attr("x1", 12.9).attr("y1", 22.9).attr("x2", 23.4).attr("y2", 12.1);
  //     $('.st_content_menu_fixed').hide().slideDown('500').addClass('fixed_menu_mobile');
  //     $('body').css({
  //       'overflow': 'hidden',
  //       'position': 'relative'
  //     });
  //   } else {
  //     $(".st_menu_mobile").css('right', '0');
  //     d3.select(".first_line").transition().duration(500).attr("x1", 10.5).attr("y1", 13.2).attr("x2", 26.1).attr("y2", 13.2);
  //     d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 1).attr("x2", 26.1);
  //     d3.select(".second_line").transition().duration(500).attr("x1", 10.5).attr("y1", 21.9).attr("x2", 26.1).attr("y2", 21.9);
  //     $('.st_content_menu_fixed').slideUp('500', function() {
  //       $(this).show().removeClass('fixed_menu_mobile');
  //     });
  //     $('body').css('overflow', 'auto');
  //   }
  // }
});
