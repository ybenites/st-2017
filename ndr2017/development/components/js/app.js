// require("../precss/app.css");
import select2 from "select2";
import $ from "jquery";

// import xx from "./library/fix-svg-size";

import * as d3 from "d3";
// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);
import Swiper from "swiper";
// import Hammer from 'hammerjs';

var isMobile = (function(a) {
  return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
})(navigator.userAgent || navigator.vendor || window.opera);

$(document).bind('mobileinit', function() {
  $.mobile.selectmenu.prototype.options.nativeMenu = false;
});

$(document).on("mobileinit", function() {
  $.mobile.ignoreContentEnabled = true;
});

// document.addEventListener('touchmove', function(event) {
//    event = event.originalEvent || event;
//    if(event.scale > 1) {
//      event.preventDefault();
//    }
//  }, false);

var swiper;
var swipeGuide = true;
//Smooth scroll when clicking "PLAY"
$(document).ready(function() {
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      // $('html, body').animate({
      //   scrollTop: $(hash).offset().top
      // }, 800, function() {

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      // });
    } // End if
  });


    height_screen() ;

  $(".playagainbtn").click(function() {
    // window.location.href = "http://graphics.straitstimes.com/STI/STIMEDIA/Interactives/2017/08/ndr-2017-pm-lee-national-day-rally-quiz/index.html";
    $(".banner").fadeOut();
    $(".swiper-container").fadeOut(function(){
      swiper.destroy();
      $(".st-header-page").fadeIn(function(){
        $("body").css("background-color","#5A9DDC");
        $(".swiper-paginat").css("pointer-events","none");
        $(".swiper-paginat").css("opacity",0);
        $(".select-qn").each(function(i) {
          var index = i+1;
          $(this).val('').trigger('change.select2');
          $(this).select2("destroy");
          $(".swiper-slide:nth-child(" + index + ")").removeClass("slide-answered");

          $(this).parent(".qntext").fadeIn(function() {
            $(".swiper-slide:nth-child(" + index + ")").find(".lhl").fadeIn();
            $(".swiper-slide:nth-child(" + index + ")").find(".answer").fadeOut();
            $("#info" + index).fadeOut();
            // if ($(".select-qn").index(this)==0 && isMobile){
            //   $(".swiper-guide").fadeIn(500);
            // }
          });

        });
      });
    });

  });

  $(".playbtn").click(function() {
    $(".st-header-page").delay(500).fadeOut(function(){
      $(".swiper-paginat").animate({
        opacity:1
      });
      $(".swiper-paginat").css("pointer-events","all");
      initSwiper();
      initSelect2();
      // swiper.slideTo(0);
        $("body").css("background-color","white");
    });
    $(".banner").delay(1000).fadeIn(500);
    $(".swiper-container").delay(1000).queue(function() {
      $(this).css("position", "relative").dequeue();
    }).fadeTo(1000, 1);
  });
});

$(window).on("resize", function() {
  height_screen();
});

function height_screen() {
  $(".st-header-page").height(window.innerHeight);
  // $(".main-headline").height(window.innerHeight - $(".st-content-menu").height());
// $('.main-headline').css({top:'50%',left:'50%',margin:'-'+($('.main-headline').height() / 2)+'px 0 0 -'+($('.main-headline').width() / 2)+'px'});
}

function initSwiper(){
    swiper = new Swiper('.swiper-container', {
    initialSlide: 0,
    pagination: '.swiper-paginat',
    paginationClickable: true,
    prevButton: '.swiper-button-prev',
    nextButton: '.swiper-button-next',
    spaceBetween: 30,

    noSwiping:true,
    noSwipingClass:"select2",
    onSliderMove: function(swiper) {
      $(".select-qn").select2("close");
    },
    onSlideChangeEnd: function(swiper){
      if (swiper.activeIndex == 1 && swipeGuide){
        $(".swiper-guide").fadeOut();
        swipeGuide = false;
      }
    }
  });
  swiper.update(true);
}

function initSelect2(){
  // var correctAns = ["politician", "Rotimatic", "hope", "ronaldo", "Facebook", "casino", "brown"];
  var correctAns = ["Rotimatic", "hope", "ronaldo", "Facebook", "casino", "brown"];

  $(".select-qn").each(function(i) {
    var index = i + 1;
    $(this).select2({
      placeholder: "",
      allowClear: true,
      width: "100%",
      disabled:false,
      minimumResultsForSearch: Infinity
    }).on("change", function(event) {
      var selectedValue = $(this).find(':selected').val();
      $(this).select2("enable", false);
      $(".swiper-slide:nth-child(" + index + ")").addClass("slide-answered");

      if (jQuery.inArray(selectedValue, correctAns) != '-1') {
        $("#qn" + index).find(".selection .select2-selection--single").css("background-color", "rgba(86,200,206,0.2)");
      } else {
        $("#qn" + index).find(".selection .select2-selection--single").css("background-color", "rgba(239,65,35,0.2)");
      }
      var _this = this;
      $(this).parent(".qntext").delay(1000).fadeOut(function() {
        $(".swiper-slide:nth-child(" + index + ")").find(".lhl").css("display","none");
        // $(".swiper-slide:nth-child(" + index + ")").find(".lhl").addClass("hideLHL");
        $(".swiper-slide:nth-child(" + index + ")").find(".answer").fadeIn(500);
        $("#info" + index).fadeIn(500);
        if (swipeGuide && $(".select-qn").index(_this)==0 && isMobile){
          $(".swiper-guide").fadeIn(500);
        }
      });
    });
  });
}
