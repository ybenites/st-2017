// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";
import hand_sliding_effect from "./libraries/hand-sliding";
// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

//var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];
//
//a_test.forEach(d => {
//  console.log(d);
//});

$(function() {
  hand_sliding_effect("#hand_space");
  $(".qn1").click(function() {
    $(this).find(".wrong_img").show();
    $("#q1_false").toggle();
    $("#hand_space svg").remove();
  });
  $(".qn1a").click(function() {
    $(this).find(".correct_img").show();
    $("#q1_true").toggle();
    $("#hand_space svg").remove();
  });

  $(".qn2").click(function() {
    $(this).find(".wrong_img").show();
    $("#q2_false").toggle();
  });
  $(".qn2a").click(function() {
    $(this).find(".correct_img").show();
    $("#q2_true").toggle();
  });

  $(".qn3").click(function() {
    $(this).find(".wrong_img").show();
    $("#q3_false").toggle();
  });
  $(".qn3a").click(function() {
    $(this).find(".correct_img").show();
    $("#q3_true").toggle();
  });

  $(".qn4a").click(function() {
    $(this).find(".wrong_img").show();
    $("#q4a").toggle();
  });
  $(".qn4b").click(function() {
    $(this).find(".correct_img").show();
    $("#q4b").toggle();
  });
  $(".qn4c").click(function() {
    $(this).find(".wrong_img").show();
    $("#q4c").toggle();
  });

  $(".qn5").click(function() {
    $(this).find(".wrong_img").show();
  });
  $(".qn5a").click(function() {
    $(this).find(".correct_img").show();
    $("#q5_true").toggle();
  });


  $(".qn6").click(function() {
    $(this).find(".wrong_img").show();
  });
  $(".qn6a").click(function() {
    $(this).find(".correct_img").show();
    $("#q6_true").toggle();
  });

});
