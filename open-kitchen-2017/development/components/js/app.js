// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";

import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

$('.infopoint').click(function() {
  $('#info').toggleClass('tabout');
});

$('.infoarrow').click(function() {
  $('#info').toggleClass('tabout');
});










// 1 //

$(".nextarrow1").click(function() {
    var current = $('.active1');
    var next = current.next();
    if( !next.length )
        next = current.parent().find('img:first');

    next.addClass('active1');
    current.removeClass('active1');
    updateText1();
});

$(".prevarrow1").click(function() {
    var current = $('.active1');
    var prev = current.prev();
    if( !prev.length )
        prev = current.parent().find('img:last');

    prev.addClass('active1');
    current.removeClass('active1');
    updateText1();
});

$( "#path-mint-kabaya1" ).hover(function() {
    $("#mint1").addClass('active1').siblings().removeClass('active1');
    updateText1();
  });


$( "#path-red-blouse1" )
  .mouseenter(function() {
    $("#red1").addClass('active1').siblings().removeClass('active1');
    updateText1();
  });


$("#path-black-blouse1")
  .mouseenter(function() {
    $("#black1").addClass('active1').siblings().removeClass('active1');
    updateText1();
  });

function updateText1() {

    if ( $( "img" ).hasClass( "indiv1 active1" ) ) {
      $(".cook1").text("THE COOK:");
      $(".name1").text("Asha Adnan, 27, founder of Asha & Co");

    //   if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    //     $(".bio1").html("<p>Asha Adnan runs Asha & Co, a community where adults meet, experience and grow together through interactive cooking and art sessions. Her passion for culinary and art as a form of therapy is based on her personal journey in living with mental illness. Once a month, she hosts a community gathering for people to gather, have meaningful conversations over various topics and bond over food.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
    //     readmore();
    //  } else {
        $(".bio1").text("Asha Adnan runs Asha & Co, a community where adults meet, experience and grow together through interactive cooking and art sessions. Her passion for culinary and art as a form of therapy is based on her personal journey in living with mental illness. Once a month, she hosts a community gathering for people to gather, have meaningful conversations over various topics and bond over food.");
    // }

      $(".where1").text("Bedok North Road");
      $(".when1").text("Jul 30, 10.30am");
      $(".menu1").text("Nasi Kerabu, Kuih Keria and Blue Pea Flower Tea");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform1").attr("action", "images/openkitchen/pdf/Asha-Adnan-mobile.pdf");
     } else {
       $(".dlform1").attr("action", "images/openkitchen/pdf/Asha-Adnan-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv2 active1" ) ) {
      $(".cook1").text("THE COOK:");
      $(".name1").text("Ng Swee Hiah, 74, retiree");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio1").html("<p>The eldest of six girls and six boys, Madam Soh was a career primary school teacher. She is now the matriarch of One Kind House, a social enterprise where everyone can gather for cooking classes and gardening sessions as well as to collaborate on ideas and startups.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio1").text("The eldest of six girls and six boys, she was a career primary school teacher. She is now the matriarch of One Kind House, a social enterprise where everyone can gather for cooking classes and gardening sessions as well as to collaborate on ideas and startups.");
      // }

      $(".where1").text("Lorong J Telok Kurau, Joo Chiat");
      $(".when1").text("Jul 20, 6.30pm");
      $(".menu1").text("Bluepea Flower Beehoon, Chicken Curry and Fried Fish with Kedongdong Pesto");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform1").attr("action", "images/openkitchen/pdf/Mummy-Soh-mobile.pdf");
     } else {
       $(".dlform1").attr("action", "images/openkitchen/pdf/Mummy-Soh-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv3 active1" ) ) {
      $(".cook1").text("THE COOK:");
      $(".name1").text("Lkhvinder Dhillon, 55, health advisor");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio1").html("<p>Cooking has been a passion for this health advisor since she was 21. She developed a love for mixing flavours and feeding people when she was helping her mother in the kitchen. She adds that her family bonds over meals, whether home-made or at restaurants.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio1").text("Cooking has been a passion for this health advisor since she was 21. She developed a love for mixing flavours and feeding people when she was helping her mother in the kitchen. She adds that her family bonds over meals, whether home-made or at restaurants.");
      // }

      $(".where1").text("Joo Seng Road, Aljunied");
      $(".when1").text("July 15, 10.30am");
      $(".menu1").text("Chapati, Chicken Masala with salad on the side, and Masala chai with chai cupcakes");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform1").attr("action", "images/openkitchen/pdf/Lkhvinder-Dhillon-mobile.pdf");
     } else {
       $(".dlform1").attr("action", "images/openkitchen/pdf/Lkhvinder-Dhillon-desktop.pdf");
     }

    }

    // readmore();

}

updateText1();










// 2 //

$(".nextarrow2").click(function() {
    var current = $('.active2');
    var next = current.next();
    if( !next.length )
        next = current.parent().find('img:first');

    next.addClass('active2');
    current.removeClass('active2');
    updateText2();
});

$(".prevarrow2").click(function() {
    var current = $('.active2');
    var prev = current.prev();
    if( !prev.length )
        prev = current.parent().find('img:last');

    prev.addClass('active2');
    current.removeClass('active2');
    updateText2();
});

$("#path-white-shirt2")
  .mouseenter(function() {
    $("#white2").addClass('active2').siblings().removeClass('active2');
    updateText2();
  });


$("#path-couple2")
  .mouseenter(function() {
    $("#couple2").addClass('active2').siblings().removeClass('active2');
    updateText2();
  });

function updateText2() {

    if ( $( "img" ).hasClass( "indiv1 active2" ) ) {
      $(".cook2").text("THE COOKS:");
      $(".name2").text("Oniatta Effendi, 43, and Nizam Ismail, 50");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio2").html("<p>Oniatta Effendi is a drama educator and an applied drama practitioner. Formerly a stage actress and TV presenter, she recently founded Baju by Oniatta, a clothing line dedicated to batik. Nizam Ismail is a regulatory lawyer and a partner in a law firm. He is also co-founder of a compliance consultancy.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio2").text("Oniatta Effendi is a drama educator and an applied drama practitioner. Formerly a stage actress and TV presenter, she recently founded Baju by Oniatta, a clothing line dedicated to batik. Nizam Ismail is a regulatory lawyer and a partner in a law firm. He is also co-founder of a compliance consultancy.");
      // }
      $(".where2").text("Lorong 24A Geylang Road");
      $(".when2").text("Jul 23, 5pm");
      $(".menu2").text("Soto Ayam Harmoni (Harmonious Chicken Soto), a traditional Indonesian dish, and Jus Alpukat Manja (Manja Avocado Shake)");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform2").attr("action", "images/openkitchen/pdf/Oniatta-Effendi-mobile.pdf");
     } else {
       $(".dlform2").attr("action", "images/openkitchen/pdf/Oniatta-Effendi-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv2 active2" ) ) {
      $(".cook2").text("THE COOK:");
      $(".name2").text("Restu Kusumaningrum, 52, founder and creative director of the Bali Purnati Center for the Arts");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio2").html("<p>Restu Kusumaningrum was a professional dancer and she holds a degree from the Landscape and Environmental Technology School in Trisakti University, Jakarta. The veteran arts producer and practitioner is the creative director of the Bali Purnati Center for the Arts, a non-profit cultural foundation with a performance amphitheatre and housing for artists in residence.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio2").text("Restu Kusumaningrum was a professional dancer and she holds a degree from the Landscape and Environmental Technology School in Trisakti University, Jakarta. The veteran arts producer and practitioner is the creative director of the Bali Purnati Center for the Arts, a non-profit cultural foundation with a performance amphitheatre and housing for artists in residence.");
      // }

      $(".where2").text("Mount Pleasant, Thomson");
      $(".when2").text("Jul 11, 6.30pm");
      $(".menu2").text("Telur Pedas (Boiled Eggs in Chilli) with Sayur Tumis Paku (Fried Fern), Nasi Madura (Squid Ink Rice with Onions and Squid), Ayam Suwir (Fried Chicken in Chilli)and Soup Dingin Kelapa Muda (Young Coconut Soup)");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform2").attr("action", "images/openkitchen/pdf/Restu-Kusumaningrum-mobile.pdf");
     } else {
       $(".dlform2").attr("action", "images/openkitchen/pdf/Restu-Kusumaningrum-desktop.pdf");
     }

    }

    // readmore();

}

updateText2();










// 3

$(".nextarrow3").click(function() {
    var current = $('.active3');
    var next = current.next();
    if( !next.length )
        next = current.parent().find('img:first');

    next.addClass('active3');
    current.removeClass('active3');
    updateText3();
});

$(".prevarrow3").click(function() {
    var current = $('.active3');
    var prev = current.prev();
    if( !prev.length )
        prev = current.parent().find('img:last');

    prev.addClass('active3');
    current.removeClass('active3');
    updateText3();
});

$( "#path-blue-top3")
  .mouseenter(function() {
    $("#blue3").addClass('active3').siblings().removeClass('active3');
    updateText3();
  });

$( "#path-red-top3" )
  .mouseenter(function() {
    $("#red3").addClass('active3').siblings().removeClass('active3');
    updateText3();
  });


$("#path-flower-dress3")
  .mouseenter(function() {
    $("#flower3").addClass('active3').siblings().removeClass('active3');
    updateText3();
  });





function updateText3() {

    if ( $( "img" ).hasClass( "indiv1 active3" ) ) {
      $(".cook3").text("THE COOKS:");
      $(".name3").text("Nurhasana Kamaruzaman, 30, hawker, and Rusnah Sajee, 58 ");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio3").html("<p>Madam Nurhasana, a mother of three primary school children, is re-entering the workforce as a part-timer. She joined Bakers Beyond, a project under Beyond Social Services (www.beyond.org.sg) which provides opportunities to community mothers who work on an ad hoc basis for festive bakes.  She is now running a western food stall at a primary school in Bishan.  Madam Rusnah is a mother of four and grandmother of five.  She works as a Looms artisan under <a href='https://www.theloomsworkshops.com/' target='_blank' >The Looms Workshop</a>.  She has a great love for sewing/craft work, cooking and baking.  She is also the team leader of Bakers Beyond.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio3").html("Madam Nurhasana, a mother of three primary school children, is re-entering the workforce as a part-timer. She joined Bakers Beyond, a project under Beyond Social Services (www.beyond.org.sg) which provides opportunities to community mothers who work on an ad hoc basis for festive bakes.  She is now running a western food stall at a primary school in Bishan.<br />Madam Rusnah is a mother of four and grandmother of five.  She works as a Looms artisan under <a href='https://www.theloomsworkshops.com/' target='_blank' >The Looms Workshop</a>.  She has a great love for sewing/craft work, cooking and baking.  She is also the team leader of Bakers Beyond.");
      // }

      $(".where3").text("Ang Mo Kio Avenue 6");
      $(".when3").text("Jul 28, 6.30pm");
      $(".menu3").text("Beef Rendang, Sambal Goreng, Begedil and Agar-agar");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform3").attr("action", "images/openkitchen/pdf/Mdm-Nurhasana-mobile.pdf");
     } else {
       $(".dlform3").attr("action", "images/openkitchen/pdf/Mdm-Nurhasana-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv2 active3" ) ) {
      $(".cook3").text("THE COOK:");
      $(".name3").text("Amy Tashiana, 51, Youtube personality and home chef");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio3").html("<p>A former model, fashion stylist and now home chef, Amy Tashiana has a <a href='https://www.youtube.com/user/loveViviane' target='_blank' > YouTube channel</a> which features her updating of Malay recipes for contemporary cooks as well as her explorations of Chinese, Vietnamese and Thai cuisines. She thinks home cooking is a good way for Muslim families to enjoy these latter cuisines.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio3").html("A former model, fashion stylist and now home chef, Amy Tashiana has a <a href='https://www.youtube.com/user/loveViviane' target='_blank' > YouTube channel</a> which features her updating of Malay recipes for contemporary cooks as well as her explorations of Chinese, Vietnamese and Thai cuisines. She thinks home cooking is a good way for Muslim families to enjoy these latter cuisines.");
      // }
      $(".where3").text("Lorong 24A, Geylang Road");
      $(".when3").text("July 16, 5pm");
      $(".menu3").text("Lemak Daun Turi (Turi Leaves in Coconut Milk), Rendang Ayam Melayu (Malay Chicken Rendang) and Udang Masak Cabai (Chilli Prawns) ");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform3").attr("action", "images/openkitchen/pdf/Amy-Tashiana-mobile.pdf");
     } else {
       $(".dlform3").attr("action", "images/openkitchen/pdf/Amy-Tashiana-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv3 active3" ) ) {
      $(".cook3").text("THE COOK:");
      $(".name3").text("Nabila Talib, 26, executive, arts, at Yale-NUS College");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio3").html("<p>Apart from her love of the arts, Nabila is an avid lover of all things cats and scones. She is collaborating with her father Abu Talib for the O.P.E.N. Kitchens sessions. He is going to prepare his special Bamia, a Middle Eastern lamb stew which he learnt from his mother, back during the kampung days at Siglap.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio3").text("Apart from her love of the arts, Nabila is an avid lover of all things cats and scones. She is collaborating with her father Abu Talib for the O.P.E.N. Kitchens sessions. He is going to prepare his special Bamia, a Middle Eastern lamb stew which he learnt from his mother, back during the kampung days at Siglap.");
      // }
      $(".where3").text("Lorong 24A, Geylang Road");
      $(".when3").text("Jul 14, 6.30pm");
      $(".menu3").text("Bamia and date scones");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform3").attr("action", "images/openkitchen/pdf/Nabila-Talib-mobile.pdf");
     } else {
       $(".dlform3").attr("action", "images/openkitchen/pdf/Nabila-Talib-desktop.pdf");
     }

    }

    // readmore();

}

updateText3();










//4
// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
//   $(".bio4").html("<p>Rose Sivam is the chief executive officer of at Pantheon Communications. She has experience in television production and now runs a media consultancy developing content for all media, lectures part-time and consults on academic and policy-related developments. Chris Choo is a dotcom entrepreneur who has turned his attention to cooking. He co-founded Relish.Sg, a private dining experience that combines food with art and entertainment, with his wife Rose Sivam</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
//   readmore();
// }

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
   $(".dlform4").attr("action", "images/openkitchen/pdf/Rose-Sivam-mobile.pdf");
  } else {
   $(".dlform4").attr("action", "images/openkitchen/pdf/Rose-Sivam-desktop.pdf");
  }








// 5

$(".nextarrow5").click(function() {
    var current = $('.active5');
    var next = current.next();
    if( !next.length )
        next = current.parent().find('img:first');

    next.addClass('active5');
    current.removeClass('active5');
    updateText5();
});

$(".prevarrow5").click(function() {
    var current = $('.active5');
    var prev = current.prev();
    if( !prev.length )
        prev = current.parent().find('img:last');

    prev.addClass('active5');
    current.removeClass('active5');
    updateText5();
});

$("#path-black5")
  .mouseenter(function() {
    $("#black5").addClass('active5').siblings().removeClass('active5');
    updateText5();
  });


$( "#path-yellow5" )
  .mouseenter(function() {
    $("#yellow5").addClass('active5').siblings().removeClass('active5');
    updateText5();
  });


$( "#path-checkered5" )
  .mouseenter(function() {
    $("#checkered5").addClass('active5').siblings().removeClass('active5');
    updateText5();
  });


$( "#path-grey5" )
  .mouseenter(function() {
    $("#grey5").addClass('active5').siblings().removeClass('active5');
    updateText5();
  });

function updateText5() {

    if ( $( "img" ).hasClass( "indiv1 active5" ) ) {
      $(".cook5").text("THE COOK:");
      $(".name5").text("Joshua Tang, 17, student");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio5").html("<p>Currently in his second year of studies for Tourism diploma at Singapore Polytechnic, Joshua Tang started learning how to cook from YouTube videos when he was about 13. He was motivated to learn cooking because he was not good at art or music, but found a way to express himself with cooking. His guilty pleasure is chicken nuggets.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio5").text("Currently in his second year of studies for a tourism diploma at Singapore Polytechnic, Joshua Tang started learning how to cook from YouTube videos when he was about 13. He was motivated to learn cooking because he was not good at art or music, but found a way to express himself with cooking. His guilty pleasure is chicken nuggets.");
      // }
      $(".where5").text("Lorong 24A Geylang Road");
      $(".when5").text("Jul 2, 6.30pm");
      $(".menu5").text("Pork Loin, Crackling, Truffled Cabbage and Apple Sauce; Mushroom 'Risotto' with a Parmesan Crisp; and a surprise third dish");
      $(".dlbutton5").attr("style", "display: inline-block;");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform5").attr("action", "images/openkitchen/pdf/Joshua-Tang-mobile.pdf");
     } else {
       $(".dlform5").attr("action", "images/openkitchen/pdf/Joshua-Tang-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv2 active5" ) ) {
      $(".cook5").text("THE COOK:");
      $(".name5").text("Sylvia Tan, 70, food writer");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio5").html("<p>Sylvia Tan writes about Singapore’s culinary heritage and her book, Singapore Heritage Food, is a classic. It describes the multicultural nature of Singapore food, and documents the dishes particular to Singapore and the various culinary influences at play. She also lectures on Singapore’s food heritage including Peranakan food and gives demonstrations on these dishes. Beyond food heritage, she also writes on healthy Asian cooking.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio5").text("Sylvia Tan writes about Singapore’s culinary heritage and her book, Singapore Heritage Food, is a classic. It describes the multicultural nature of Singapore food, and documents the dishes particular to Singapore and the various culinary influences at play. She also lectures on Singapore’s food heritage including Peranakan food and gives demonstrations on these dishes. Beyond food heritage, she also writes on healthy Asian cooking.");
      // }
      $(".where5").text("Lorong 24A Geylang Road");
      $(".when5").text("Jul 13, 6.30pm");
      $(".menu5").text("Nasi Ulam with Five Herbs and Sambal Belacan");
      $(".dlbutton5").attr("style", "display: inline-block;");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform5").attr("action", "images/openkitchen/pdf/Sylvia-Tan-mobile.pdf");
     } else {
       $(".dlform5").attr("action", "images/openkitchen/pdf/Sylvia-Tan-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv3 active5" ) ) {
      $(".cook5").text("THE COOK:");
      $(".name5").text("Lok Meng Chue, 61, actress");
      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio5").html("<p>This veteran actress is a familiar face who has appeared in many theatre and television productions in the last 30 years.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio5").text("This veteran actress is a familiar face who has appeared in many theatre and television productions in the last 30 years.");
      // }
      $(".where5").text("Lorong 24A Geylang Road");
      $(".when5").text("Jul 27, 6.30pm");
      $(".menu5").text("Steamed Chicken with Shiitake Mushrooms, Cloud Fungus, Red Dates, Lily Buds, Chinese Pork and Liver Sausages; Big Aunty Marrying Off Her Daughter (Hairy Gourd stirfry with Dried Shrimps, Black Fungus and Bean Vermicelli); soup of Ikan Bilis (dried anchovies) stock with Malabar Spinach and Salted Egg");
      $(".dlbutton5").attr("style", "display: inline-block;");

      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
       $(".dlform5").attr("action", "images/openkitchen/pdf/Lok-Meng-Chue-mobile.pdf");
     } else {
       $(".dlform5").attr("action", "images/openkitchen/pdf/Lok-Meng-Chue-desktop.pdf");
     }

    }

    if ( $( "img" ).hasClass( "indiv4 active5" ) ) {
      $(".cook5").text("THE COOK:");
      $(".name5").text("Anita Kapoor, 46");

      // if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      //   $(".bio5").html("<p>Born in Mumbai, India, Anita Kapoor has been living in Singapore since 1978. The television presenter declares she is 'an immigrant, third culture kid and a proud Singaporean all rolled into one'. She cooks in the same way her mother and grandmother did, borrowing cuisines and integrating them into her diet.</p><p class='read-more'><a href='#' class='button'>Read More</a></p>");
      //   readmore();
      // } else {
        $(".bio5").text("Born in Mumbai, India, Anita Kapoor has been living in Singapore since 1978. The television presenter declares she is 'an immigrant, third culture kid and a proud Singaporean all rolled into one'. She cooks in the same way her mother and grandmother did, borrowing cuisines and integrating them into her diet.");
      // }
      $(".where5").text("Lorong 24A ");
      $(".when5").text("July 22, 6.30pm");
      $(".menu5").text("Ohn no khao swè (Burmese dish consisting of wheat noodles in a curried chicken and coconut milk broth), Patrani Machchi (A popular Parsi preparation of fish cooked with green chutney wrapped in banana leaves) and Falooda (Dessert drink of rose syrup, milk, vermicelli and basil seeds)");
      $(".dlbutton5").attr("style", "display: none;");
    }

}

updateText5();













// function readmore() {
//   var $el, $p, $ps, $up, totalHeight;
//
//   $(".bio .read-more .button").click(function() {
//
//     totalHeight = 0;
//
//     $el = $(this);
//     $p  = $el.parent();
//     $up = $p.parent();
//     $ps = $up.find("p:not('.read-more')");
//
//     // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
//     $ps.each(function() {
//       totalHeight += $(this).outerHeight();
//     });
//
//     $up
//       .css({
//         // Set height to prevent instant jumpdown when max height is removed
//         "height": $up.height(),
//         "max-height": 9999
//       })
//       .animate({
//         "height": totalHeight
//       });
//
//     // fade out read-more
//     $p.fadeOut();
//
//     // prevent jump-down
//     return false;
//
//   });
//
//   $(".bio").attr("style", "max-height: 55px; position: relative; overflow: hidden;");
//   $(".read-more").attr("style", "position: absolute; bottom: -3px; width: 100%; margin: 0; padding-top: 20px; background-image: linear-gradient(to bottom, transparent, white 20px);");
//
// }
