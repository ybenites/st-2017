// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";

import 'select2';
// var isMobile = (function(a) {
//   return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
// })(navigator.userAgent || navigator.vendor || window.opera);


var manualChange = false;
$(function() {
  if ('scrollRestoration' in history) {
  history.scrollRestoration = 'manual';
}

  $("#tl-date-dropdown").select2({
    width:'100%',
    allowClear:true,
    placeholder:"select date",
    minimumResultsForSearch:Infinity
  });

  $(window).scroll(function() {
    // var offset = 2;
    // var linePerc = $(window).scrollTop() / ($(document).height() - $(window).height()) * 100 - offset;
    // if (linePerc <80){
    //   $(".tl-line").css("height",linePerc+"%");
    // }else{
    //   $(".tl-line").css("height","100%");
    // }
    // console.log("window scroll top: "+$(window).scrollTop());
    // console.log("timeline-container height outer"+$(".st-header-page").outerHeight());
    if ($(window).scrollTop() > $(".st-header-page").outerHeight() + $(".content-sub-menu").outerHeight()) {
        $(".content-sub-menu-empty").show();
      $(".content-sub-menu").addClass("fix-menu");
    } else {
      $(".content-sub-menu-empty").hide();
      $(".content-sub-menu").removeClass("fix-menu");
    }

    if (!manualChange) {
      var passHighestDate = '';
      $('.tl-date').each(function() {
        var currScroll = $(window).scrollTop();
        var passDate = $(this).find('p').text();
        // console.log("date: "+passDate);
        // console.log("currScroll: "+currScroll);
        // console.log("target: "+$(this).offset().top);
        // console.log("offset menu: "+$(".content-sub-menu").outerHeight());
        // console.log("window height: "+$(window).height());

        if (currScroll > ($(this).offset().top - $(".content-sub-menu").outerHeight())) {
          passHighestDate = passDate;
        } else if (currScroll < ($('.tl-date:first').offset().top - $(".content-sub-menu").outerHeight())) {
          passHighestDate = $('.tl-date:first').find('p').text();
        }
        // $(".select2-selection__rendered").text(passHighestDate);
        $("#tl-date-dropdown").val(passHighestDate).trigger('change.select2');
      });
    }

    $('.tl-block').each(function(){
      var bottom_of_object = $(this).offset().top + $(this).outerHeight() *0.1;
      var bottom_of_window = $(window).scrollTop() + $(window).height();

      if (bottom_of_window>bottom_of_object) {
        if (!$(this).hasClass("shown")){
        $(this).animate({
          opacity:1,
          paddingTop:0
        },function(){
          $(this).addClass("shown");
        });
      }
      }
    });
  });

  var align = 'left';
  var current_date = '';
  var select_date = d3.select("#tl-date-dropdown");
  d3.csv("data/oxley_event.csv", function(data) {
    data.forEach(function(d, i) {
      var htmlcontent = '';
      var avatarImg = '';
      var segmentImg = '';
      var char1, char2 = '';
      var charClass = '';
      var videoStr='';
      if (current_date !== d.date) {
        current_date = d.date;
        var opt_date = current_date;
        opt_date = opt_date.replace(/ /g,"_");
          htmlcontent += '<div class ="tl-date" id="'+opt_date+'"><p>' + d.date + '</p></div>';
        $("#tl-date-dropdown").append('<option id="opt-'+opt_date+'">'+d.date+'</option>');

      }
      switch (d.main_character_1) {
        case 'lhl':
        case 'lhy':
        case 'lwl':
          char1 = d.main_character_1;
          avatarImg += '<img id="avatar_' + char1 + '"src="images/lee-family/avatar_' + char1 + '.png" class="img-avatar">';
          break;
      }
      switch (d.main_character_2) {
        case 'lhl':
        case 'lhy':
        case 'lwl':
          char2 = d.main_character_2;
          avatarImg += '<img id="avatar_' + char2 + '"src="images/lee-family/avatar_' + char2 + '.png" class="img-avatar">';
          break;
      }
      charClass = char1 +' '+char2;
      if (d.images !== '-') {
        var imagesArr = d.images.split(',');
        var imageCapArr = d.image_caption.split('--');
        imagesArr.forEach(function(imgStr,i){
          segmentImg += '<figure class="img-segment">' +
            '<img src="images/lee-family/' + imgStr + '">' +
            '<figcaption>' + imageCapArr[i] + '</figcaption></figure>';
        });
      }

      if (d.video !=='-'){
        var videoArr = d.video.split(',');
        videoArr.forEach(function(vid){
          videoStr +='<div class="content-video"><iframe src="'+vid+'" allowfullscreen frameborder="0"></iframe></div>';
        });
      }

      align = d.position;
      htmlcontent += '<div class="tl-block">' +
        '<div class="tl-node"><p>' + d.time + '</p></div>' +
        '<div class="tl-content tl-' + align +' '+charClass+ '">' +
        '<div class="filter-layer"></div>'+
        '<div class="tl-image">' + avatarImg + '</div>' +
        '<p>' + d.description + '</p>' + segmentImg+videoStr+
        '<a target="_blank" href="'+d.related+'"><div class="read-more">READ MORE</div></a></div></div>';

      $(".timeline-container").append(htmlcontent);
    });
  });

  $(".sub-menu-box").on('click',function(){
    var clickedBtn = this.id;
    var _this = this;
    if ($(this).hasClass("clickedBtn")){
      $(this).removeClass("clickedBtn");
      $(".tl-content").each(function(){
        if (!$(this).hasClass(_this.id)){
            $(this).find(".filter-layer").css("opacity",0);
        }
      });
      $(".sub-menu-box").css("opacity",1);
    }else{
      $(this).addClass("clickedBtn");
      $(this).siblings(".sub-menu-box").removeClass("clickedBtn");
      $(".tl-content").each(function(){
        if ($(this).hasClass(_this.id)){
            $(this).find(".filter-layer").css("opacity",0);
        }else{
            $(this).find(".filter-layer").css("opacity",0.7);
        }
      });
      $(_this).siblings(".sub-menu-box").css("opacity",0.7);
      $(_this).css("opacity",1);
    }



  });

  $("#tl-date-dropdown").on("change",function(){
    manualChange = true;
    var target = d3.select(this).property("value").replace(/ /g,"_");
    var offset = $(".content-sub-menu").outerHeight();

    $('html,body').animate({
       scrollTop: $("#"+target).offset().top - offset
    },2500,function(){
      var currScroll = $(window).scrollTop();

      if (currScroll !== ($("#"+target).offset().top - offset)){
        $('html,body').animate({
           scrollTop: $("#"+target).offset().top - offset
        },function(){
          manualChange = false;
        });
      }else{
        manualChange = false;
      }
    });
  });
});
