var $=require('jquery');
var jQuery=$;


var d3 = Object.assign({}, require("d3-selection"), require("d3-transition"), require("d3-collection"), require("d3-dispatch"), require("d3-ease"), require("d3-interpolate"));


var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
    FB.init({
        appId: fbAppId, // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the
        // server to access the session
        xfbml: true, // parse page for xfbml or html5
        // social plugins like login button below
        version: 'v2.0', // Specify an API version
    });
    // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


/*start set values to footer*/
var height_footer = $("footer.st-content-footer").outerHeight();
$("body").css("margin", "0 0 " + height_footer + "px");
/*end set values to footer*/




if ($(".st-button-menu-mobile").length > 0) {
    $(".st-button-menu-mobile").on("click", function() {
        $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
        $(".st-content-menu-fixed").toggleClass('st_dialogIsOpen');
        eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
    });
}

if ($(".modal-menu-mobile").length > 0) {
    var elem = document.querySelector('.modal-menu-mobile');
    elem.addEventListener('touchstart', function(event) {
        startY = event.touches[0].pageY;
        startTopScroll = elem.scrollTop;
        if (startTopScroll <= 0)
            elem.scrollTop = 1;
        if (startTopScroll + elem.offsetHeight >= elem.scrollHeight)
            elem.scrollTop = elem.scrollHeight - elem.offsetHeight - 1;
    }, false);
}

if ($(".st-social-desktop").length > 0) {
    $(".st-social-desktop img:nth(0),.st-social-mobile img:nth(0)").on("click", function(e) {
        // code share facebook
        e.preventDefault();
        var image = 'http://graphics.straitstimes.com/STI/STIMEDIA/facebook_images/oxley-dispute-live-blog/oxley-dispute-live-blog.png';
        var name = "PM Lee’s ministerial statement on Oxley: Live blog";
        var description = "Get live updates as PM Lee Hsien Loong delivers his ministerial statement on the dispute over Mr Lee Kuan Yew’s home at 38, Oxley Road.";
        var url = "http://str.sg/oxleyblog";

        share_face_book(image, name, description, url);
        return false;
    });
    $(".st-social-desktop img:nth(1),.st-social-mobile img:nth(1)").on("click", function(e) {
        // code share twitter
        e.preventDefault();
        var text = "Get live updates as PM Lee delivers his ministerial statement on Oxley dispute";
        var via = 'STcom';
        var url = 'http://str.sg/oxleyblog';

        share_twitter(text, via, url);
        return false;
    });
}




// Listen for orientation changes
window.addEventListener("orientationchange", function() {
    update_parameters();
}, false);


var settime;
$(window).on("resize", function() {
    if (!is_mobile()) {
        if (settime) clearTimeout(settime);
        settime = setTimeout(function() {
            update_parameters();
            clearTimeout(settime);
        }, 100);
    }
});

function share_face_book(image, name, description, url) {
    FB.ui({
        method: 'feed',
        link: url === undefined ? window.location.href : url,
        caption: 'www.straitstimes.com',
        picture: image,
        name: name,
        description: description
    });
}

function share_twitter(text, via, url) {
    window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
}

function eventCloseMenu(event) {
    if (event) {
        $(".st_menu_mobile").css('right', '10px');

        d3.select(".first_line").transition().duration(500).attr("x1", 12.8).attr("y1", 12.2).attr("x2", 23.5).attr("y2", 22.8);
        d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 0).attr("x2", 0);
        d3.select(".second_line").transition().duration(500).attr("x1", 12.9).attr("y1", 22.9).attr("x2", 23.4).attr("y2", 12.1);
        $('.st_content_menu_fixed').hide().slideDown('500').addClass('fixed_menu_mobile');
        $('body').css({
            'overflow': 'hidden',
            'position': 'relative'
        });
    } else {
        $(".st-menu-mobile").css('right', '0');
        d3.select(".first_line").transition().duration(500).attr("x1", 10.5).attr("y1", 13.2).attr("x2", 26.1).attr("y2", 13.2);
        d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 1).attr("x2", 26.1);
        d3.select(".second_line").transition().duration(500).attr("x1", 10.5).attr("y1", 21.9).attr("x2", 26.1).attr("y2", 21.9);
        $('.st_content_menu_fixed').slideUp('500', function() {
            $(this).show().removeClass('fixed_menu_mobile');
        });
        $('body').removeAttr('style');
        $("body").css("margin", "0 0 " + height_footer + "px");
    }
}


function update_parameters() {
    var height_footer = $("footer.st-content-footer").outerHeight();
    $("body").css("margin", "0 0 " + height_footer + "px");
}

function is_mobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
}
