// import fix_svg from "./libraries/fix-svg-size";
import get_svg from "./libraries/get-svg";
import animateJs from "./libraries/animateJs";

new animateJs({
  tag:".main-headline",
  // todo:function(){}
});


//Uncomment line 13 if have inline svg to make it responsive. Also uncomment line 1. As tag add the class of the parent of the svg

// new fix_svg({
//   tag:['']
// });




//automatic will animate all clases with .animate-up / .animate-down / .animate-left / animate-right / animate-fadeIn / animate-fadeOut / animate-line

new get_svg({
  //load svgs first class need to match with first folder. It will load 3 files (desktop-image.svg w800px - tablet-image.svg w550px - mobile-image.svg w350px)
  tags: [".svg-1"],
  folders: ["svg1"],
  todo: function(d) {

    //here to add some custom JS. Example set up element to 0 opacity

    delete_line_path();

    // d3.selectAll(".animate-line-x").select("path").each(function() {
    //   var copy_this = this;
    //   var total_length = copy_this.getTotalLength();
    //   d3.select(copy_this)
    //     .attr('stroke-dasharray', total_length)
    //     .attr('stroke-dashoffset', total_length);
    // });

    //custom animation 1 start
    // new animateJs({
    //   tag:".animate-line-x",
    //   todo:function(el){
    //     d3.select(el).select("path").transition().duration(3000).attr("stroke-dashoffset",0);
    //   }
    // });
    //custom animation 1 finish

    //custom animation 2 start
    new animateJs({
      tag:".button-1",
      todo:function(el){
        d3.select(el).transition().duration(3000).style("opacity",0);
      }
    });
    //custom animation 2 finish

    animate_bridge();
  }
});

function animate_bridge(){
  $(".svg-1").on("click",".animation-1 .next-1",function(){
    $(".animation-1").animateCss("fadeOut",'out');
    $(".animation-2").animateCss("fadeIn",'in');
    d3.timeout(d=>{
      $(".animation-2").find(".animate-fadeIn2").each(function(i){
        if(i%2===0){
          d3.select(this).attr("opacity",0).classed("none",false)
          .transition().duration(1000).attr("opacity",1);
        }else{
          d3.select(this).attr("opacity",0).classed("none",false)
          .transition().duration(1000).delay(1000).attr("opacity",1)
          .on("end",d=>{
            $(".next-2").animateCss("fadeIn","in");
            $(".back-1").animateCss("fadeIn","in");
          });
        }
      });
    },1000);
  });
  $(".svg-1").on("click",".animation-2 .back-1",function(){
    $(".animation-1").animateCss("fadeIn",'in');
    $(".animation-2").animateCss("fadeOut",'out');
    $(".animation-2").find(".animate-fadeIn2").addClass("none");
    $(".next-2,.back-1").addClass("none");
  });
  $(".svg-1").on("click",".animation-2 .next-2",function(){
    $(".animation-2").animateCss("fadeOut",'out');
    $(".animation-3").animateCss("fadeIn",'in');
    d3.timeout(d=>{
      $(".animation-3").find(".animate-fadeIn2").each(function(i){
        if(i%2===0){
          d3.select(this).attr("opacity",0).classed("none",false)
          .transition().duration(1000).attr("opacity",1);
        }else{
          d3.select(this).attr("opacity",0).classed("none",false)
          .transition().duration(1000).delay(1000).attr("opacity",1)
          .on("end",d=>{
            $(".back-2").animateCss("fadeIn","in");
            d3.selectAll(".animation-3 .animate-line_2_ path").transition().duration(3000).attr("stroke-dashoffset",0);
          });
        }
      });
    },1000);
  });
  $(".svg-1").on("click",".animation-3 .back-2",function(){
    $(".animation-2").animateCss("fadeIn",'in');
    $(".animation-3").animateCss("fadeOut",'out');
    $(".animation-3").find(".animate-fadeIn2").addClass("none");
    $(".back-2").addClass("none");
    delete_line_path();
  });

  d3.selectAll(".r1").transition().each(animateFirstStep1);
}





function animateFirstStep1(){
      d3.selectAll(".r1").transition().attr("opacity", 0.01).duration(3000).attr("r", 30).on("end", animateSecondStep1);
};

function animateSecondStep1(){
      d3.selectAll(".r1").transition().delay(300).duration(0.0001).attr("r", 15).attr("opacity", 1).on("end", animateFirstStep1);
};
function delete_line_path(){
  d3.selectAll(".animate-line_2_").selectAll("path").each(function(){
    var copy_this=this;
    var total_length = copy_this.getTotalLength();
    d3.select(copy_this)
        .attr('stroke-dasharray', total_length)
        .attr('stroke-dashoffset', total_length);
  });
}
