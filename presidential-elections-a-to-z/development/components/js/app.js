// require("../precss/app.css");
// import xx from "./library/fix-svg-size";

var $ = require('jquery');
var jQuery = $;
global.jQuery = jQuery;

var Masonry = require('masonry-layout');
require('jquery-bridget');
$.bridget('masonry', Masonry);

// var Typeahead = require('typeahead');

require('select2');


import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);



// file.js

var url_marker_svg = "images/icon-marker.svg";
var url_marker_svg_selected = "images/icon-marker-selected.svg";
var marker_flag_selected;

var info = {};
// var st_url_csv = "http://localhost/graphics/st-get-file-csv/1LWJVov1y1pIxABdB1-ngh9XxzP4tD51T_Y9fU7wt7H8";
// var st_url_csv = "http://52.74.240.184/graphics/st-get-file-csv/1LWJVov1y1pIxABdB1-ngh9XxzP4tD51T_Y9fU7wt7H8";
// var st_url_csv = "http://graphics.straitstimes.com/st-get-file-csv/1LWJVov1y1pIxABdB1-ngh9XxzP4tD51T_Y9fU7wt7H8";
var st_url_csv = "csv/a-to-z.csv";


// var template_card = "<div class='item-card item-card-unit [ST-CLASS-COLOR]' data-order='[ST-ORDER]' data-name_deal=\"[ST-DATA-NAME-DEAL]\" data-category='[ST-CATEGORY]' data-price='[ST-PRICE]' data-rprice='[ST-RPRICE]' data-disc='[ST-DISC]' data-region='[ST-REGION]' data-type='[ST-TYPE]'  >";
var template_card = "<div class='item-card item-card-unit' data-name_deal=\"[ST-DATA-NAME-DEAL]\">";
// template_card += "<div class='[ST-CLASS-CARD-BACK]' >";
// template_card += "<p>[ST-NAME]</p><div class='aftercard' style=' background-image: url(./images/gss-deals/img/thumbnails/[ST-THUMBNAIL].png)' ></div> ";
template_card += "<h1>[ST-ALPHABET]</h1><span>[ST-IS-FOR]</span>";
template_card += "</div></div>";

// var template_option = "<option value='[ST-NAME-VALUE]'>[ST-NAME-DISPLAY]</option>";


info.t_content_item_card = "<div>";
info.t_content_item_card += "<div class='row-content'>";

info.t_content_item_card += "<div class='row-left'>";
info.t_content_item_card += "<img class='st-image-responsive' src='images/a-to-z/img/[ST-SUBTITLE-IMAGE].jpg' alt=''/>";
info.t_content_item_card += "</div>";


info.t_content_item_card += "<div class='row-right'>";
info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-title'><strong>[ST-SUBTITLE]</strong></div>";
info.t_content_item_card += "</div>";

info.t_content_item_card += "<div class='row-content-right'>";
info.t_content_item_card += "<div class='row-mall'>[ST-CONTENT]</div>";
info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>DISCOUNTED PRICE</div>";
// info.t_content_item_card += "<div class='row-mall'>$[ST-ROW-PRICE]</div>";
// info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>RETAIL PRICE</div>";
// info.t_content_item_card += "<div class='row-mall'>$[ST-ROW-RPRICE]</div>";
// info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>STORE</div>";
// info.t_content_item_card += "<div class='row-mall'>[ST-ROW-STORE]</div>";
// info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>MALL</div>";
// info.t_content_item_card += "<div class='row-mall'>[ST-ROW-MALL]</div>";
// info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>ADDRESS</div>";
// info.t_content_item_card += "<a href='http://maps.google.com/?q=[ST-MAP]' target='_blank'><div class='row-mall'>[ST-ROW-ADDRESS]</div></a>";
// info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>OPENING HOURS</div>";
// info.t_content_item_card += "<div class='row-mall'>[ST-ROW-HOURS]</div>";
// info.t_content_item_card += "</div>";

// info.t_content_item_card += "<div class='row-content-right'>";
// info.t_content_item_card += "<div class='row-label'>CONTACT</div>";
// info.t_content_item_card += "<a href='tel:[ST-CONTACT]'><div class='row-mall'>[ST-ROW-CONTACT]</div></a>";
// info.t_content_item_card += "</div>";

info.t_content_item_card += "</div>";

// info.t_content_item_card += "<iframe class='addressmap' src='https://www.google.com/maps/embed/v1/place?key=AIzaSyDC-cZMn7hA6cPGSijKRRgE9g7l_ZVI1R8&q=[ST-ROW-MAP]' allowfullscreen > </iframe>";


info.t_content_item_card += "</div>";
// info.t_content_item_card += "<div class='empty-space'></div>";

/*button close*/
info.t_content_item_card += "<div class='btn-close-content-card'><img src='images/close.svg' class='st-img-responsive' /></div>";
/*end button close*/
info.t_content_item_card += "</div>";


/*configuration for event click card*/
info.number_columns = 0;
info.last_click_card = 0;
info.count_clicks_card = 0;
info.cache_order = [];




/*array of selects*/
var a_category = [];
var a_region = [];
var a_type = [];

/*end of array selects*/
// $(".select-category").select2({
//   placeholder: "CATEGORY",
//   allowClear: true,
//   width: "100%",
//   minimumResultsForSearch: Infinity
// }).on("change", function(event) {
//   // $(".list_candidate_select").val("");
//   info.f_filter_elements();
//   info.f_clear_select2_icon(this);
// });

// $(".select-price").select2({
//   placeholder: "PRICE",
//   allowClear: true,
//   width: "100%",
//   minimumResultsForSearch: Infinity
// }).on("change", function(event) {
//   // $(".list_candidate_select").val("");
//   info.f_filter_elements();
//   info.f_clear_select2_icon(this);
// });

// $(".select-disc").select2({
//   placeholder: "DISCOUNT",
//   allowClear: true,
//   width: "100%",
//   minimumResultsForSearch: Infinity
// }).on("change", function(event) {
//   // $(".list_candidate_select").val("");
//   info.f_filter_elements();
//   info.f_clear_select2_icon(this);
// });

// $(".select-region").select2({
//   placeholder: "REGION",
//   allowClear: true,
//   width: "100%",
//   minimumResultsForSearch: Infinity
// }).on("change", function(event) {
//   // $(".list_candidate_select").val("");
//   info.f_filter_elements();
//   info.f_clear_select2_icon(this);
// });

// $(".select-type").select2({
//   placeholder: "TYPE",
//   allowClear: true,
//   width: "100%",
//   minimumResultsForSearch: Infinity
// }).on("change", function(event) {
//   // $(".list_candidate_select").val("");
//   info.f_filter_elements();
//   info.f_clear_select2_icon(this);
// });

// var ta = Typeahead($(".typehead-search-wine"), {
//     source: []
// });



d3.csv(st_url_csv, function(error, csv_data) {
  if (!error) {
    main(csv_data);
  }
});

// var colours = {
//   // North: "red-line",
//   // South: "purple-line",
//   // East: "green-line",
//   // West: "green-line",
//   // Central: "blue-line",
//   'Bedok': "green-line",
//   'Bugis': "green-line",
//   'Changi': "green-line",
//   'Pasir Ris': "green-line",
//   'Queenstown': "green-line",
//   'Tampines': "green-line",
//   'Marina Bay': "yellow-line",
//   'Paya Lebar': "yellow-line",
//   'Telok Blangah': "yellow-line",
//   'City Hall': "red-line",
//   'Orchard': "red-line",
//   'Tanglin': "red-line",
//   'Jurong': "red-line",
//   'Jurong East': "red-line",
//   'Choa Chu Kang': "red-line",
//   'Yishun': "red-line",
//   'Woodlands': "red-line",
//   'Sembawang': "red-line",
//   'Ang Mo Kio': "red-line",
//   'Harbourfront': "purple-line"
// };

// $("#tabs-graphic a").on('click', function(e) {
//   e.preventDefault();
//   //$(this).tab('show');
//   var id_href = $(this).attr("href");
//   $(".active-tab").removeClass("active-tab");
//   $(id_href).addClass('active-tab');
// });
// $("#tabs-graphic li").click(function() {
//   $(this).addClass('active').siblings().removeClass('active');
// });

$("#content-cards").on("click", '.btn-close-content-card', function() {
  info.f_clear_content_item_card_with_animation();
});

$(window).on('resize', function() {
  if (info.detect_device() === false) {
    info.last_click_card = 0;
    if ($("#content-cards .content-item-card").length > 0) {
      info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    }

    $(".item-card-unit").removeClass("item-active");
    info.number_columns = parseInt(($("#content-cards").outerWidth() + 10) / ($(".item-card").outerWidth() + 10));

    var min_height_card = $(".item-card").outerWidth();
    $(".item-card").css('min-height', min_height_card);

  }
});
window.addEventListener("orientationchange", function() {
  if (info.detect_device() === true) {
    info.last_click_card = 0;
    if ($("#content-cards .content-item-card").length > 0) {
      info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    }
    $(".item-card-unit").removeClass("item-active");
    info.number_columns = parseInt(($("#content-cards").outerWidth() + 10) / ($(".item-card").outerWidth() + 10));

    var min_height_card = $(".item-card").outerWidth();
    $(".item-card").css('min-height', min_height_card);
  }
}, false);

function main(csv_data) {

  info.mansonry = $("#content-cards").masonry({
    columnWidth: '.item-card',
    itemSelector: '.item-card',
    isFitWidth: false,
    gutter: 15,
    transitionDuration: "0.5s"
  });

  $(".select-category").html("<option value=''></option>");
  $(".select-region").html("<option value=''></option>");
  $(".select-type").html("<option value=''></option>");

  info.data_sort = csv_data;

  info.data_sort.forEach(function(d, order) {
    // var d_category = d.category.trim();
    // if ($.inArray(d_category, a_category) === -1) {
    //   $(".select-category").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_category), 'g').replace('[ST-NAME-DISPLAY]', d_category, 'g'));
    //   a_category.push(d_category);
    // }

    // var d_region = d.region.trim();
    // if ($.inArray(d_region, a_region) === -1) {
    //   $(".select-region").append(template_option.replace('[ST-NAME-VALUE]', info.change_space_underscore(d_region), 'g').replace('[ST-NAME-DISPLAY]', d_region, 'g'));
    //   a_region.push(d_region);
    // }
    // var d_type = d.type.trim();
    // if ($.inArray(d_type, a_type) === -1) {
    //   $(".select-type").append(template_option.replace('[ST-NAME-VALUE]', d_type, 'g').replace('[ST-NAME-DISPLAY]', d_type, 'g'));
    //   a_type.push(d_type);
    // }

    var new_card = template_card
      // .replace('[ST-ORDER]', (order + 1), 'g')
      // .replace("[ST-CLASS-COLOR]", colours[d.area.trim()], "g")
      // .replace("[ST-CATEGORY]", info.change_space_underscore(d_category), 'g')
      // .replace("[ST-PRICE]", d.discounted_price.trim(), 'g')
      // .replace("[ST-RPRICE]", d.regular_retail_price.trim(), 'g')
      // .replace("[ST-DISC]", d.discount_per_cent.trim(), 'g')
      // .replace("[ST-REGION]", info.change_space_underscore(d_region), 'g')
      // .replace("[ST-TYPE]", d_type, 'g')
      // .replace("[ST-CLASS-CARD-BACK]", "item-card-back-" + (d.img.trim().match(/\d+/)[0]), 'g')
      .replace("[ST-DATA-NAME-DEAL]", d.subtitle.trim(), "g")
      // .replace("[ST-THUMBNAIL]", d.thumbnail.trim(), "g")
      // .replace("[ST-NAME]", d.item.trim(), "g")
      .replace("[ST-ALPHABET]", d.alphabet.trim(), "g")
      .replace("[ST-IS-FOR]", d.is_for.trim(), "g");

    $(".save-copy-card-1").append(new_card);
    $("#content-cards").append(new_card);
    info.cache_order.push(order + 1);
  });


  info.mansonry.masonry('reloadItems');
  info.mansonry.masonry("layout");

  info.mansonry.on("layoutComplete", function(event, item) {
    info.number_columns = parseInt(($("#content-cards").outerWidth() + 15) / ($(".item-card").outerWidth() + 10));
    if (info.flag_order_cards === 0) {
      info.flag_order_cards = 1;
      var set_time = setTimeout(function() {
        clearTimeout(set_time);
        info.mansonry.masonry("layout");
      }, 500);
    }

  });

  info.mansonry.on("removeComplete", function(event, item) {
    if (info.time_delete_contentcard === 0) {
      info.time_delete_contentcard = 500;
      info.mansonry.masonry('reloadItems');
      info.mansonry.masonry("layout");
    } else {
      var set_time = setTimeout(function() {
        clearTimeout(set_time);
        info.mansonry.masonry('reloadItems');
        info.mansonry.masonry("layout");
      }, 500);
    }
  });

  $("#content-cards").on("click", ".item-card-unit", function() {
    info.count_clicks_card++;

    $(".item-card-unit").find("h1").removeClass("fontThirty");
    $(".item-card-unit").find("span").removeClass("fontCuratorBold");
    $(this).find("h1").addClass("fontThirty");
    $(this).find("span").addClass("fontCuratorBold");

    var position_current_element = $("#content-cards .item-card-unit").index(this) + 1;
    var row_appear = Math.ceil(position_current_element / info.number_columns) * info.number_columns;
    var style_content_item_card = "style='position:absolute; top:" + parseInt($(this).css('top')) + "px;'";

    if (info.last_click_card !== row_appear) {
      if (row_appear > info.last_click_card) {
        var offset_top2 = $(window).scrollTop() - $(".content-item-card").outerHeight();
        $("body,html").scrollTop(offset_top2);
      }
      info.f_clear_content_item_card();

      var $div_item = $("#content-cards .item-card-unit").eq(row_appear - 1);
      var content_card = "<div class='item-card content-item-card' " + style_content_item_card + "></div>";
      if ($div_item.length > 0) {
        $(content_card).insertAfter($div_item);
      } else {
        $("#content-cards").append(content_card);
      }
    }

    var offset_top = $(this).offset().top;
    $("body,html").animate({
      scrollTop: offset_top - $(".padding-offset").outerHeight()
    }, 500);

    var st_name_deal = $(this).data('name_deal');
    info.data_sort.forEach(function(d) {
      if (d.subtitle.trim() === st_name_deal) {
        info.f_add_content_item_card(d);
      }
    });

    // console.log(position_current_element); //item #
    // console.log(row_appear); //item row #
    // console.log(style_content_item_card); //style
    // console.log(offset_top); //distance of card clicked from top
    // console.log(st_name_deal); //name of deal

    info.last_click_card = row_appear;

    $(".item-card-unit").removeClass("item-active");
    $(this).addClass("item-active");

  });

}

info.order_ascendent_by_id = function(a_o_data) {
  return a_o_data.sort(function(a, b) {
    return d3.ascending(a.id, b.id);
  });
};

info.order_descendent_by_id = function(a_o_data) {
  return a_o_data.sort(function(a, b) {
    return d3.descending(a.id, b.id);
  });
};






// filling in info of item

info.f_add_content_item_card = function(obj_content_card) {
  // console.log(obj_content_card);
  var number_image = (parseInt(obj_content_card.img) < 10) ? (0 + obj_content_card.img) : obj_content_card.img;
  // info.change_address_map = function(element) {
  //   return element.split(" ").join("+"), element.split("#").join("%2F");
  // };
  var content_item_card_html = info.t_content_item_card
    .replace("[ST-SUBTITLE]", obj_content_card.subtitle.trim(), "g")
    .replace("[ST-SUBTITLE-IMAGE]", obj_content_card.img.trim(), 'g')
    .replace("[ST-CONTENT]", obj_content_card.content.trim(), 'g');
  // .replace("[ST-ROW-PRICE]", obj_content_card.discounted_price.trim(), 'g')
  // .replace("[ST-ROW-RPRICE]", obj_content_card.regular_retail_price.trim(), 'g')
  // .replace("[ST-ROW-STORE]", obj_content_card.store.trim(), 'g')
  // .replace("[ST-ROW-MALL]", obj_content_card.mall.trim(), 'g')
  // .replace("[ST-ROW-ADDRESS]", obj_content_card.address.trim(), 'g')
  // .replace("[ST-MAP]", info.change_address_map(obj_content_card.address.trim()), 'g')
  // .replace("[ST-ROW-HOURS]", obj_content_card.hours.trim(), 'g')
  // .replace("[ST-ROW-CONTACT]", obj_content_card.contact.trim(), 'g')
  // .replace("[ST-CONTACT]", obj_content_card.contact.trim(), 'g')
  // .replace("[ST-ROW-MAP]", info.change_address_map(obj_content_card.address.trim()), 'g');

  $(".content-item-card").html(content_item_card_html);
  $(".content-item-card").append("<div class='empty-space'></div>");
  $(".content-item-card").append("<div class='empty-space2'></div>");

  info.mansonry.masonry('reloadItems');
  info.mansonry.masonry("layout");
};

info.f_clear_content_item_card = function() {
  info.time_delete_contentcard = 0;
  if ($("#content-cards .content-item-card").length > 0) {
    info.last_click_card = 0;
    info.mansonry.masonry({
      transitionDuration: "0s"
    });
    info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
    info.mansonry.masonry({
      transitionDuration: "0.5s"
    });
  }
  $(".item-card-unit").removeClass("item-active");
};

info.f_clear_content_item_card_with_animation = function() {
  if ($("#content-cards .content-item-card").length > 0) {
    info.last_click_card = 0;
    info.mansonry.masonry("remove", $("#content-cards .content-item-card"));
  }
  $(".item-card-unit").removeClass("item-active");
  $(".item-card-unit").find("h1").removeClass("fontThirty");
  $(".item-card-unit").find("span").removeClass("fontCuratorBold");
};

info.f_clear_select2_icon = function(this_select) {
  var val_select = $(this_select).val();
  var parent_select = $(this_select).parent();
  if (val_select) {
    parent_select.find(".select2-selection").addClass("background_select");
    parent_select.find(".select2-selection__arrow").addClass("disappear_arrow");
  } else {
    parent_select.find(".select2-selection").removeClass("background_select");
    parent_select.find(".select2-selection__arrow").removeClass("disappear_arrow");
  }
};



// filter select options

info.f_filter_elements = function() {
  info.last_click_card = 0;
  info.flag_order_cards = 0;
  info.copy_elements = $(".save-copy-card-1").clone();

  $(".item-card-unit").removeClass("item-active");
  var elements_to_display = info.copy_elements.find(".item-card");

  var select_category = $(".select-category").select2('val');
  if (select_category) {
    elements_to_display = elements_to_display.filter("[data-category='" + select_category + "']");
  }

  var select_price = parseInt($(".select-price").select2('val'));
  if (select_price) {
    elements_to_display = elements_to_display.filter(function(index) {
      var r_ = false;
      if (select_price === 1 && parseFloat($(this).data('price')) < 50) {
        r_ = true;
      } else if (select_price === 2 && parseFloat($(this).data('price')) >= 50 && parseFloat($(this).data('price')) < 100) {
        r_ = true;
      } else if (select_price === 3 && parseFloat($(this).data('price')) >= 100) {
        r_ = true;
      }
      return r_;
    });
  }

  var select_disc = parseInt($(".select-disc").select2('val'));
  if (select_disc) {
    elements_to_display = elements_to_display.filter(function(index) {
      var r_ = false;
      if (select_disc === 1 && parseFloat($(this).data('disc')) < 30) {
        r_ = true;
      } else if (select_disc === 2 && parseFloat($(this).data('disc')) >= 30 && parseFloat($(this).data('disc')) < 40) {
        r_ = true;
      } else if (select_disc === 3 && parseFloat($(this).data('disc')) >= 40 && parseFloat($(this).data('disc')) < 50) {
        r_ = true;
      } else if (select_disc === 4 && parseFloat($(this).data('disc')) >= 50) {
        r_ = true;
      }
      return r_;
    });
  }

  var select_region = $(".select-region").select2('val');
  if (select_region) {
    elements_to_display = elements_to_display.filter("[data-region='" + select_region + "']");
  }

  var select_type = $(".select-type").select2('val');
  if (select_type) {
    elements_to_display = elements_to_display.filter("[data-type='" + select_type + "']");
  }


  $("#content-cards .item-card").addClass("target-delete-items");


  info.go_over_cards(elements_to_display);


  if ($(".target-delete-items").length > 0) info.mansonry.masonry("remove", $(".target-delete-items"));

  info.mansonry.masonry('reloadItems');
  info.mansonry.masonry("layout");
};

info.go_over_cards = function(elements_to_display) {
  var a_new_cache_order = [];
  elements_to_display.each(function(i) {
    a_new_cache_order.push($(this).data("order"));
    var new_order = $(this).data("order");

    if ($.inArray(new_order, info.cache_order) == -1) {
      var div_to_add = this;
      // add new card
      var last_min = 0;
      var capture_last_min;
      if ($("#content-cards .item-card").length > 0) {
        $("#content-cards .item-card").each(function() {
          let old_order = $(this).data("order");
          if (new_order > old_order) {
            capture_last_min = this;
          }
        });
        if (capture_last_min) {
          $(div_to_add).insertAfter(capture_last_min);
        } else {
          $("#content-cards").prepend(div_to_add);
        }
      } else {
        $("#content-cards").append(div_to_add);
      }
    } else {
      $("#content-cards .item-card").filter("[data-order=" + new_order + "]").removeClass("target-delete-items");
    }
  });
  info.cache_order = a_new_cache_order;
};

// info.change_space_underscore = function(element) {
//   return element.split(" ").join("_");
// };
info.detect_device = function() {
  return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
};



$("#content-select-category").mousedown(function sortoptions() {
  var list, i, switching, b, shouldSwitch;
  list = document.getElementById("select2-select-category-results");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    //Loop through all list items:
    for (i = 0; i < (b.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*check if the next item should
      switch place with the current item:*/
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        /*if next item is alphabetically lower than current item,
        mark as a switch and break the loop:*/
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark the switch as done:*/
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
});

$("#content-select-region").mousedown(function sortoptions() {
  var list, i, switching, b, shouldSwitch;
  list = document.getElementById("select2-select-region-results");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    //Loop through all list items:
    for (i = 0; i < (b.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*check if the next item should
      switch place with the current item:*/
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        /*if next item is alphabetically lower than current item,
        mark as a switch and break the loop:*/
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark the switch as done:*/
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
});

$("#content-select-type").mousedown(function sortoptions() {
  var list, i, switching, b, shouldSwitch;
  list = document.getElementById("select2-select-type-results");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    //Loop through all list items:
    for (i = 0; i < (b.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*check if the next item should
      switch place with the current item:*/
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        /*if next item is alphabetically lower than current item,
        mark as a switch and break the loop:*/
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark the switch as done:*/
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
});

$(function($) {
  $('#bookmark-this').click(function(e) {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && addToHomescreen.isCompatible) {
      // Mobile browsers
      addToHomescreen({
        autostart: false,
        startDelay: 0
      }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox <=22
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox 23+ and Opera <=14
      $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorites
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit & Blink - Safari, Chrome, Opera 15+)
      alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
  });
});

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  $(".banner").css("display", "none");
  $(".bannermobile").css("display", "block");
} else {
  $(".banner").css("display", "block");
  $(".bannermobile").css("display", "none");
}



function detectIE() {
  var ua = window.navigator.userAgent;
  var ie = ua.search(/(MSIE|Trident)/);

  // if Safari
  if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
    ie = 1;
  }

  return ie > -1;
}

$(document).ready(function() {
  if (!detectIE()) {
    d3.selectAll("path, polygon, polyline, ellipse, line, text, circle").each(function() {
      // var total_length = this.getTotalLength();
      d3.select(this)
        .attr('stroke-dasharray', 300)
        .attr('stroke-dashoffset', 300);
    });

    d3.selectAll("path, polygon, polyline, ellipse, line, text, circle").transition().delay(4800).duration(2500).attr('stroke-dashoffset', 0);


    d3.selectAll(".linefront").each(function() {
      // var total_length = this.getTotalLength();
      d3.select(this)
        .attr('stroke-dasharray', 3835.17)
        .attr('stroke-dashoffset', 3835.17);
    });

    d3.selectAll(".linefront").transition().duration(6200).attr('stroke-dashoffset', 0);



    d3.selectAll(".lineback").each(function() {
      // var total_length = this.getTotalLength();
      d3.select(this)
        .attr('stroke-dasharray', 3774.22)
        .attr('stroke-dashoffset', -3774.22);
    });

    d3.selectAll(".lineback").transition().duration(5500).attr('stroke-dashoffset', 0);



    d3.selectAll("#object-1, #object-8, #station-1, #station-5")
      .style('fill-opacity', 0)
      .transition().delay(1800).duration(1000)
      .style('fill-opacity', 1);

    d3.selectAll("#object-3, #object-7, #station-4")
      .style('fill-opacity', 0)
      .transition().delay(2800).duration(1000)
      .style('fill-opacity', 1);

    d3.selectAll("#object-4, #object-5, #object-6, #station-3, #station-2")
      .style('fill-opacity', 0)
      .transition().delay(4800).duration(1000)
      .style('fill-opacity', 1);
  }
});
