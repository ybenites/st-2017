// import fix_svg from "./libraries/fix-svg-size";
import get_svg from "./libraries/get-svg";
import animateJs from "./libraries/animateJs";

new animateJs({
  tag:".main-headline",
  // todo:function(){}
});


//Uncomment line 13 if have inline svg to make it responsive. Also uncomment line 1. As tag add the class of the parent of the svg

// new fix_svg({
//   tag:['']
// });




//automatic will animate all clases with .animate-up / .animate-down / .animate-left / animate-right / animate-fadeIn / animate-fadeOut / animate-line

new get_svg({
  //load svgs first class need to match with first folder. It will load 3 files (desktop-image.svg w800px - tablet-image.svg w550px - mobile-image.svg w350px)
  tags: [".svg-1", ".svg-2", ".svg-3", ".svg-4", ".svg-5", ".svg-6", ".svg-7"],
  folders: ["svg", "svg-2", "svg-3", "svg-4", "svg-5", "svg-6", "svg-7"],
  todo: function(d) {

    //here to add some custom JS. Example set up element to 0 opacity

    d3.selectAll(".animate-line-x").select("path").each(function() {
      var copy_this = this;
      var total_length = copy_this.getTotalLength();
      d3.select(copy_this)
        .attr('stroke-dasharray', total_length)
        .attr('stroke-dashoffset', total_length);
    });

    //custom animation 1 start
    new animateJs({
      tag:".animate-line-x",
      todo:function(el){
        d3.select(el).select("path").transition().duration(3000).attr("stroke-dashoffset",0);
      }
    });
    //custom animation 1 finish

    //custom animation 2 start
    new animateJs({
      tag:".button-1",
      todo:function(el){
        d3.select(el).transition().duration(3000).style("opacity",0);
      }
    });
    //custom animation 2 finish


  }
});
