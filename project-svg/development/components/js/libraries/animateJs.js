require('./waypoint');

export default class Animate {
  constructor(options) {
    if (options) {
      this.options = options;
      var tag = options.tag;

      this.o_type_animations = {
        "animate-up": "slideUp",
        "animate-down": "slideDown",
        "animate-left": "slideLeft",
        "animate-right": "slideRight",
        "animate-fadeIn": "fadeIn",
        "animate-fadeOut": "fadeOut",
        "animate-line": "animatePath"
      };

      this.entries_animations = d3.entries(this.o_type_animations);

      this.checkIsAnimate(tag);

    }
  }
  checkIsAnimate(tag) {
    var animate = false;
    var newTag = (typeof tag === "object")
      ? d3.select(tag)
      : d3.selectAll(tag);

    if (newTag.size() === 0)
      return;
    this.entries_animations.forEach(d => {
      if (newTag.classed(d.key)) {
        animate = true;
        this.animateTag(newTag, d.value, "in");
      }
    });

    if (!animate) {

      this.entries_animations.forEach(d => {

        if (newTag.selectAll("." + d.key).size() > 0) {
          animate = true;
          this.animateTag(newTag.selectAll("." + d.key), d.value, "in");
        }
      });
    }
    if (!animate) {
      this.animateTag(newTag);
    }
    // return animate;
  }
  animateTag(tags, animateName, type) {
    var _this = this;
    tags.each(function() {
      if (animateName === "animatePath") {
        var total_length = $(this).find("path").get(0).getTotalLength();
        d3.select(this).select("path").attr('stroke-dasharray', total_length).attr('stroke-dashoffset', total_length);
      }

      var waypoint_top = new Waypoint({
        element: this,
        handler: function(direction) {
          var element_animated = this.element;
          if (direction === "down") {

            if (typeof _this.options.todo === "function")
              _this.options.todo(element_animated);
            if (animateName !== "animatePath") {
              if (animateName !== undefined)
                $(element_animated).animateCss(animateName, type);
              }
            else {
              d3.select(element_animated).select("path").transition().duration(3000).attr('stroke-dashoffset', 0);
            }
          } else if (direction === "up") {
            if (typeof _this.options.before === "function")
              _this.options.before(element_animated);

            if (animateName !== "animatePath") {} else {
              var total_length = d3.select(element_animated).select("path").node().getTotalLength();
              d3.select(element_animated).select("path").transition().duration(0).attr('stroke-dasharray', total_length).attr('stroke-dashoffset', total_length);
            }
          }

        },
        offset: "100%"
      });
    });
  }

  animateCss() {}
  animateHand() {}
}
