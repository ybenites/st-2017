import Backbone from "backbone";

import './babel-helpers';
import _ from "underscore";

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var textures = require("textures");



var forest1 = "#C4D6C4";
var forest2 = "#A5C3A8";
var forest3 = "#73A97F";
var forest4 = "#008C4E";
var forest5 = "#007741";
var forest6 = "#005027";
var green1 = "#CBE1C8";
var green2 = "#AED3AB";
var green3 = "#7DBE80";
var green4 = "#0AA74B";
var green5 = "#008C3E";
var green6 = "#005E25";
var olive1 = "#E0EDCB";
var olive2 = "#D0E4AF";
var olive3 = "#B4D682";
var olive4 = "#8FC641";
var olive5 = "#74A535";
var olive6 = "#476E1E";
var lime1 = "#EFF4CC";
var lime2 = "#E8EEAF";
var lime3 = "#DBE580";
var lime4 = "#CADB2E";
var lime5 = "#A6B626";
var lime6 = "#6A7812";
var yellow1 = "#FCF8CD";
var yellow2 = "#FBF5B0";
var yellow3 = "#F9F17E";
var yellow4 = "#F6EB0E";
var yellow5 = "#CAC313";
var yellow6 = "#838103";
var tangerine1 = "#FFECC6";
var tangerine2 = "#FFE3A7";
var tangerine3 = "#FFD576";
var tangerine4 = "#FDC218";
var tangerine5 = "#D0A115";
var tangerine6 = "#886A00";
var orange1 = "#FDD5BB";
var orange2 = "#FBBE99";
var orange3 = "#F79967";
var orange4 = "#F26725";
var orange5 = "#C8551D";
var orange6 = "#843401";
var red1 = "#FBC9BA";
var red2 = "#F8AB98";
// var red3 = "#F37B68";
var red3 = "#fd6e5f";
var red4 = "#EC2033";
// var red5 = "#C31729";
var red5 = "#ef4123";
var red6 = "#82000D";
var rose1 = "#E9C3C8";
var rose2 = "#DFA4AE";
var rose3 = "#D17589";
var rose4 = "#C02460";
var rose5 = "#A11950";
var rose6 = "#6B0031";
var violet1 = "#DABFD1";
var violet2 = "#C99FBB";
var violet3 = "#B1709B";
var violet4 = "#952978";
var violet5 = "#7E1E65";
var violet6 = "#530041";
var purple1 = "#CABDDC";
var purple2 = "#B19CC9";
var purple3 = "#8D6EAE";
var purple4 = "#653290";
var purple5 = "#552479";
var purple6 = "#360451";
var navy1 = "#BCC2E0";
var navy2 = "#9BA4CF";
var navy3 = "#697CB8";
var navy4 = "#0F519F";
var navy5 = "#0A4286";
var navy6 = "#002459";
var blue1 = "#C8DAF0";
var blue2 = "#ABC8E8";
// var blue3 = "#7AADDC";
var blue3 = "#29abe2";
var blue4 = "#1F8FCE";
// var blue5 = "#1B78AC";
var blue5 = "#12589e";
var blue6 = "#044F74";
var cyan1 = "#CFE8F9";
var cyan2 = "#B5DDF6";
var cyan3 = "#86CCF1";
var cyan4 = "#2AB8EB";
var cyan5 = "#259AC5";
var cyan6 = "#0C6785";
var gray1 = "#DCDDDE";
// var gray2 = "#BCBEC0";
var gray2 = "#666666";
var gray3 = "#939598";
var gray4 = "#6D6E71";
var gray5 = "#4D4D4F";
var gray6 = "#414042";

var grey1 = "#AFBABF";
var grey2 = "#D4D8DA";
var black = "#231F20";
var white = "#FFFFFF";

var Reuters = window["Reuters"] = window["Reuters"] || {};

Reuters.Locales = {

    en: {
        decimal: ".",
        thousands: ",",
        grouping: [3],
        currency: ["$", ""],
        dateTime: "%a %b %e %X %Y",
        date: "%m/%d/%Y",
        time: "%H:%M:%S",
        periods: ["AM", "PM"],
        days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        "shortMonths": ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
    },

    es: {
        "decimal": ",",
        "thousands": ".",
        "grouping": [3],
        "currency": ["$", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%m/%d/%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        "shortDays": ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        "months": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        "shortMonths": ["Ene", "Feb", "Mar", "Abr", "Mayo", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
    },

    fr: {
        "decimal": ",",
        "thousands": ".",
        "grouping": [3],
        "currency": ["$", ""],
        "dateTime": "%A, le %e %B %Y, %X",
        "date": "%d/%m/%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
        "shortDays": ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
        "months": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
        "shortMonths": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."]
    },

    ch: {
        decimal: ".",
        thousands: ",",
        grouping: [3],
        currency: ["¥", ""],
        dateTime: "%a %b %e %X %Y",
        date: "%d/%m/%Y",
        time: "%H:%M:%S",
        periods: ["AM", "PM"],
        days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
        shortDays: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
        months: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
        "shortMonths": ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"]
    },

    pt: {
        "decimal": ",",
        "thousands": ".",
        "grouping": [3],
        "currency": ["$", ""],
        "dateTime": "%a %b %e %X %Y",
        "date": "%m/%d/%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        "shortDays": ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
        "months": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        "shortMonths": ["Jan", "Fev", "Mar", "Abr", "Maio", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
    }

}


Reuters.LANGUAGE = 'en';
Reuters.CurrentLocale = d3.locale(Reuters.Locales[Reuters.LANGUAGE]);

(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["candidateContent"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }

        t.election.candidates.forEach(function(d, i) {;
            __p += '\n   <div class="row">\n        <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n            <img src="images/candidates/' + ((__t = d.imageid) == null ? '' : __t) + '.png" class="candidate-image mt-2 mx-auto d-block" width="100%">\n\n        </div>\n    </div>\n\n    <div class="row">\n        <div class="col-md-6 offset-md-3 ">\n            <h3 class="mt-1 text-xs-center">' + ((__t = d.candidate) == null ? '' : __t) + ' <span class="font-weight-normal">' + ((__t = d.age) == null ? '' : __t) + '</span></h3>  \n            <h4 class="font-weight-normal candidate-content-age text-xs-center"></h4>               \n            <p class="candidate-content-party text-uppercase text-xs-center">' + ((__t = d.party) == null ? '' : __t) + '</p>               \n            <p class="bottomline-text mb-2 text-sm-center">' + ((__t = d.bottomline) == null ? '' : __t) + '</p>               \n       </div>\n    </div>\n    \n    <div class="row">\n        ';
            t.election.issues.forEach(function(issue) {;
                __p += '\n            <div class="col-md-3 ">\n                <p class="text-uppercase mb-0 font-weight-bold">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</p>\n                <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>\n            </div>  \n        ';
            });
            __p += '        \n    </div>\n\n        ';
            if (i != t.election.candidates.length - 1) {;
                __p += '\n            <hr>\n         ';
            };
            __p += ' \n\n';
        });
        __p += '\n\n';
        return __p;
    };
})();
(function() {

    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["featureLayout"] = function(t) {
        var __t,
            __p = '';
        __p += '<!-- Header -->\n<div class="feature-header mt-0 container" id="package-container">\n        <div id="headerbanner" class="row header-img-holder">\n            <img id="bannerImg" class="img-fluid feature-header-img mb-1 hidden-sm-down" src="images/banners/French-Header-Banner-Desktop.png">\n            <img class="img-fluid feature-header-img mb-1 hidden-md-up" src="images/banners/French-Header-Banner-Mobile.jpg">\n  <div id="bannerText">FRENCH PRESIDENTIAL ELECTION 2017</div>      </div>\n\n        <div class="row header-text-holder">\n           <h1 class="display-1 div-col text-xs-center header-text">' + ((__t = 'The poll tracker') == null ? '' : __t) + '</h1>\n        </div>\n\n        <div id="deck" class="row">\n    <p id="decktText" class="div-col text-xs-center">' + ((__t = 'France elects a new president in a two-round contest on April 23 and May 7. It is the most closely-watched election in years. Who is leading the national polls?') == null ? '' : __t) + '</p>\n     <div class="time">    <p class="div-col text-xs-center text-md-center">' + ((__t = 'PUBLISHED: APRIL 20, 2017') == null ? '' : __t) + '</p>\n      </div>\n    </div>\n        \n        <div class="col-sm-12 text-sm-center masthead-nav text-uppercase">\n            <div class="btn-group nav-options horizontal" data-toggle="buttons" id="section-buttons">\n                <label class="btn btn-link" data-id="candidates">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off" checked> \n                    ' + ((__t = 'Candidates') == null ? '' : __t) + '\n                </label>\n                <label class="btn btn-link active" data-id="polling">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off">\n                    ' + ((__t = 'Polls') == null ? '' : __t) + '\n                </label>\n<!--                 <label class="btn btn-link" data-id="feature-nav-option-3">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off">\n                    ' + ((__t = 'Results') == null ? '' : __t) + '\n                </label> -->\n            </div>\n        </div>        \n</div>\n\n<div class="container graphic-section-container">\n\n    <!-- Polls -->\n    <section class="graphic-section polls-container selected" id="polling">\n        <div class="row subheads" style="display:none;">\n            <h2 class="mobile-section-headers text-xs-center hidden-sm-up col-md-6 offset-md-3 text-uppercase">' + ((__t = 'Polls') == null ? '' : __t) + '</h2>\n                   </div>\n        <div id="polls" class="polls"></div>\n    </section>\n\n\n	<!-- Candidates -->\n	<section class="graphic-section candidate-container" id="candidates">\n	\n	    <div class="row subheads">\n            <h2 class="mobile-section-headers text-xs-center hidden-sm-up col-md-6 offset-md-3 text-uppercase">' + ((__t = 'Candidates') == null ? '' : __t) + '</h2>\n	           <p class="graphic-subhead text-xs-center col-md-6 offset-md-3">' + ((__t = 'France elects a new president in a two-round contest that takes place on April 23 and May 7. Here are the main competitors and their programmes:') == null ? '' : __t) + '</p>\n	    </div>\n		<div class="candidate-buttons navContainer text-center">\n	        <div class="btn-group nav-options horizontal" data-toggle="buttons">\n	                <label dataid="candidate-sort" class="btn btn-primary active smaller">\n	                    <input type="radio" name="nav-options" autocomplete="off"> \n	                    ' + ((__t = 'KEY PLAYERS') == null ? '' : __t) + '           \n	                </label>\n	                <label dataid="issue-sort" class="btn btn-primary smaller">\n	                    <input type="radio" name="nav-options" autocomplete="off"> \n	                    ' + ((__t = 'ISSUES') == null ? '' : __t) + '     \n	                </label>\n	        </div>    		    		\n		</div>    	\n	    <div id="candidate-sort" class="candidate-table selected"></div>\n	    <div id="issue-sort" class="candidate-table"></div>\n	</section>\n\n</div>\n\n\n<!-- footer -->\n<div class="container graphic-section-container" style="display:none">\n     <div class="row col-md-8 offset-md-2 header-img-holder" >\n           \n    </div>  \n     <div class="row col-md-8 offset-md-2 header-img-holder" >\n    </div>    \n    <div class="row">   \n        <div class="col-xs-12 mt-1 text-xs-center">\n            <p class="graphic-source">' + ((__t = 'Sources: Candidate programmes and declarations; the pollsters') == null ? '' : __t) + '<br />' + ((__t = 'Graphics by Michael Ovaska, Matthew Weber and Brian Love. Illustrations by Weiyi Cai | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>   \n</div>';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["firstRoundPollsLegendTemplate"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }
        __p += '<div class=\'legendContainer\'>\n	<div class="legend-ital">' + ((__t = 'Hide/show') == null ? '' : __t) + '</div>\n	\n	<div class="legend-items-holder">\n		';
        t.data.forEach(function(d) {;
            __p += '\n			<div class="legendItems">\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
            print(t.self.colorScale(d.name));
            __p += ';\'></div>\n				<div class="legendInline">\n					<div class="nameTip">	' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n				</div>\n			</div>\n		';
        });
        __p += '\n	</div>\n\n</div>\n\n';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["issueContent"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }

        t.election.issues.forEach(function(issue, i) {;
            __p += '\n    <div class="row">\n              <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n                    <img src="images/icons/' + ((__t = issue) == null ? '' : __t) + '-icon.png" class="candidate-image mx-auto d-block" width="100%">\n              </div>\n    </div>\n\n    <div class="row">\n              <div class="col-sm-6 offset-sm-3">\n                    <h3 class="text-center mb-1 mt-1">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</h3>\n              </div>\n                      \n    </div>\n\n  ';
            t.election.candidates.forEach(function(d) {;
                __p += '\n      <div class="row">\n\n                <div class="col-lg-1">            \n                </div> \n\n                <div class="col-md-4 col-lg-2 text-xs-center text-md-left issue-content-header">\n                    <p><span class="text-uppercase font-weight-bold">' + ((__t = d.candidate) == null ? '' : __t) + '</span><br>\n                    <span class="">' + ((__t = d.party) == null ? '' : __t) + '</span></p>\n                </div>\n\n\n                <div class="col-md-8  text-xs-left">\n                    <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>               \n                </div>\n\n                <div class="col-lg-1">            \n                </div> \n\n      </div>\n\n   ';
            });
            __p += '\n         \n        ';
            if (i != t.election.issues.length - 1) {;
                __p += '\n            <hr>\n         ';
            };
            __p += ' \n\n';
        });
        __p += '        \n\n\n';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["pollSecondTooltip"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }
        __p += '\n<!-- <div class=\'dateTip text-uppercase\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div> -->\n';
        t.data.forEach(function(d, i) {;
            __p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
            print(t.self.colorScale(d.name));
            __p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
            if (t.self.chartLayout == "stackPercent") {;
                __p += '\n					';
                print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
                __p += '				\n				';
            } else {;
                __p += '\n					';
                print(t.self.tipNumbFormat(d[t.self.dataType]));
                __p += '				\n				';
            };
            __p += '\n			</div>\n	\n		</div>\n';
        });
        __p += '	\n';
        if (t.self.timelineData) {
            var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
            print(t.self.timelineTemplate({
                data: timelineData,
                self: t.self
            }));
        };
        __p += '	\n\n	<div class=\' tooltip-methodology mt-1\'>' + ((__t = t.data[0].pollster) == null ? '' : __t) + ' survey of ' + ((__t = t.data[0].samplesize) == null ? '' : __t) + ' respondents conducted ' + ((__t = t.data[0].category) == null ? '' : __t) + '. Margin of error: +/- ' + ((__t = t.data[0].marginoferror) == null ? '' : __t) + ' pct. pts.</div>\n';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["pollTooltip"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }
        __p += '	';
        if (t.data[0].displayDate) {;
            __p += '\n<div class=\'dateTip text-uppercase\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
        } else {;
            __p += '\n<div class=\'dateTip text-uppercase\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
        };
        __p += '\n';
        t.data.forEach(function(d, i) {;
            __p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
            print(t.self.colorScale(d.name));
            __p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
            if (t.self.chartLayout == "stackPercent") {;
                __p += '\n					';
                print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
                __p += '				\n				';
            } else {;
                __p += '\n					';
                print(t.self.tipNumbFormat(d[t.self.dataType]));
                __p += '				\n				';
            };
            __p += '\n			</div>\n	\n		</div>\n';
        });
        __p += '	\n';
        if (t.self.timelineData) {
            var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
            print(t.self.timelineTemplate({
                data: timelineData,
                self: t.self
            }));
        };
        __p += '	\n\n';
        if (t.data[0].pollster) {;
            __p += '\n	<div class=\' tooltip-methodology\'>' + ((__t = t.data[0].pollster) == null ? '' : __t) + ' surevey of ' + ((__t = t.data[0].samplesize) == null ? '' : __t) + ' respondents. Margin of error: ' + ((__t = t.data[0].marginoferror) == null ? '' : __t) + ' percentage points</div>\n';
        };

        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["pollsfirst"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }
        __p += '\n<div class="navContainer poll-buttons text-center">\n    <div class="btn-group nav-options horizontal" data-toggle="buttons">\n            <label dataid="first-round" class="btn btn-primary active smaller">\n \n                ' + ((__t = 'FIRST ROUND') == null ? '' : __t) + '           \n            </label>\n            <label dataid="second-round" class="btn btn-primary smaller">\n \n                ' + ((__t = 'SECOND ROUND') == null ? '' : __t) + '     \n            </label>\n    </div>    		    		\n</div>\n\n<div class="poll-section selected" id="first-round">\n	<p class="graphic-chart-label text-xs-center mt-1">' + ((__t = 'Historical Polls') == null ? '' : __t) + '</p>\n	<p class="bottomline-text text-xs-center">Daily voting intentions for the first round of voting on April 23 with margins of error shown</p>\n	\n	<div class="row">\n		<div class="col-sm-6">\n	        <p class="pollster"><strong>' + ((__t = 'OpinionWay') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="reutersGraphic-first-Opinionway" class="reuters-chart"></div>		\n		</div>\n	\n		<div class="col-sm-6">\n	        <p class="pollster"><strong>' + ((__t = 'Ifop-Fiducial') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="reutersGraphic-first-Ifop" class="reuters-chart"></div>\n		</div>\n	</div>\n	\n	\n	\n	<div class="row hidden-xs-down">\n		';
        t.self.candidatesArray.forEach(function(d, i) {;
            __p += '\n			<div class="col-sm-2 ';
            if (i == 0) {;
                __p += 'offset-sm-1';
            };
            __p += '">\n				<img class="img-fluid" src="images/candidates/' + ((__t = d) == null ? '' : __t) + '-single-color.jpg">\n				<div class="candidate-name text-sm-center">\n					' + ((__t = t.self.candidateLookup[d]) == null ? '' : __t) + '\n				</div>\n				</div>\n		\n			\n		';
            //<div class="candidate-underline ' + ((__t = d) == null ? '' : __t) + '"></div>		\n	\n
        });
        __p += '\n	</div>\n	\n	<div class="row mt-1 mb-1">\n		<div class="col-sm-12">\n			<p class="graphic-chart-label mt-2 text-xs-center">' + ((__t = 'Latest Polls') == null ? '' : __t) + '</p>\n			<p class="bottomline-text text-xs-center">Voting intentions for the first round of voting on April 23 with margins of error shown</p>\n		</div>\n	</div>\n	\n	';
        t.pollsters.forEach(function(key) {
            var value = t.firstPollsData[key];
            // console.log(value);;
            __p += '\n		<div class="row">\n			<div class="col-sm-2 col-md-2 poll-text">\n				<p class="pollster">' + ((__t = key) == null ? '' : __t) + '</p>\n				<p class="moe">Survey of ' + ((__t = value[0].samplesize) == null ? '' : __t) + ' respondents conducted ' + ((__t = value[0].daterange) == null ? '' : __t) + '</p>\n			</div>\n			<div class="col-sm-10 col-md-10">\n		        <div id="reutersGraphic-latest-poll-' + ((__t = key.split(' ')[0].split('/')[0]) == null ? '' : __t) + '" class=""></div>\n			</div>\n	\n			\n		</div>\n	';
        });
        __p += '\n</div>\n<div class="poll-section" id="second-round">	\n\n\n\n</div>	';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["pollssecond"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }
        __p += '<div class="row ">\n	<div class="col-sm-12">\n		<p class="graphic-chart-label mt-2 text-xs-center mt-1">' + ((__t = 'Latest Numbers') == null ? '' : __t) + '</p>\n		<p class="bottomline-text text-xs-center">Voting intentions for second-round two-candidate scenarios on May 7</p>\n	</div>\n</div>\n	\n';
        t.matchups.forEach(function(d, i) {;
            __p += '\n	<div class="row ';
            if (i == t.matchups.length - 1) {;
                __p += 'mb-3';
            };
            __p += '">\n		<div class="col-xs-6 col-sm-6 col-md-2">\n			<img class="img-fluid " src="images/candidates/' + ((__t = d.first) == null ? '' : __t) + '-single-color.jpg">\n			<p class="candidate-name text-xs-center">' + ((__t = d.firstFull) == null ? '' : __t) + ' </p>			\n		</div>\n	     \n		<div class="col-xs-6 hidden-md-up">\n		<img class="img-fluid " src="images/candidates/' + ((__t = d.second) == null ? '' : __t) + '-single-color.jpg">\n			<p class="candidate-name text-md-center text-xs-center">' + ((__t = d.secondFull) == null ? '' : __t) + ' </p>			\n		</div>\n\n	     <div class="col-sm-12 col-md-8">\n		     <div id="reutersGraphic-first-' + ((__t = d.first + d.second) == null ? '' : __t) + '" class="reuters-chart"></div>				     \n	     </div>\n\n		<div class="col-xs-6 col-sm-6 col-md-2 hidden-md-down ">\n			<img class="img-fluid " src="images/candidates/' + ((__t = d.second) == null ? '' : __t) + '-single-color.jpg">\n			<p class="candidate-name text-xs-center">' + ((__t = d.secondFull) == null ? '' : __t) + ' </p>			\n		</div>\n\n	</div>\n	\n\n';
        });
        __p += '\n\n\n';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["sample"] = function(t) {
        var __t,
            __p = '';
        __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
        return __p;
    };
})();


(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
    window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

    window["Reuters"]["Graphics"]["basicCharter"]["Template"]["tooltip"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }

        if (t.data[0].quarters) {;
            __p += '\n	<div class=\'dateTip\'> ' + ((__t = t.data[0].quarters) == null ? '' : __t) + ' ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
        } else if (t.data[0].displayDate) {;
            __p += '\n	<div class=\'dateTip\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
        } else {;
            __p += '\n	<div class=\'dateTip\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
        };
        __p += '\n\n';
        t.data.forEach(function(d, i) {;
            __p += '\n		<div class="tipHolder">\n			';
            if (t.data.length > 1) {;
                __p += '\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
                print(t.self.colorScale(d.name));
                __p += ';\'></div>\n				<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			';
            };
            __p += '\n			<div class=\'valueTip\'>\n				';
            if (t.self.chartLayout == "stackPercent") {;
                __p += '\n					';
                print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
                __p += '				\n				';
            } else {;
                __p += '\n					';
                print(t.self.tipNumbFormat(d[t.self.dataType]));
                __p += '				\n				';
            };
            __p += '\n			</div>\n	\n		</div>\n';
        });
        __p += '	\n';
        if (t.self.timelineData) {
            var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
            print(t.self.timelineTemplate({
                data: timelineData,
                self: t.self
            }));
        };
        __p += '	';
        return __p;
    };
})();

(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
    window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

    window["Reuters"]["Graphics"]["basicCharter"]["Template"]["chartTemplate"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }

        if (t.self.multiDataColumns) {;
            __p += '\n	<div class="chart-nav">\n		<div class="navContainer">\n            <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                ';
            t.self.multiDataColumns.forEach(function(d, i) {;
                __p += '                \n                    <label dataid="' + ((__t = d) == null ? '' : __t) + '" class="btn btn-primary ';
                if (i == t.self.multiDataLabels.length - 1) {;
                    __p += 'active';
                };
                __p += ' smaller">\n                        <input type="radio" name="nav-options" autocomplete="off"> \n                        ' + ((__t = t.self.multiDataLabels[i]) == null ? '' : __t) + '\n                    </label>\n                ';
            });
            __p += '\n            </div>    		    		\n		</div>    	\n	</div>\n';
        };
        __p += '\n';
        if (t.self.navSpacer) {;
            __p += '\n	<div class="chart-nav">\n		<div class="navContainer spacer"></div>\n	</div>\n';
        };
        __p += '\n\n';
        if (t.self.chartLayoutLables) {;
            __p += '\n	<div class="chart-layout"></div>\n';
        };
        __p += '\n<div class="chart-holder \n	';
        if (!t.self.hasLegend) {
            print('no-legend');
        };
        __p += '\n	">\n	<div class="legend nested-legend"></div>\n	<div class="chart nested-chart"></div>\n</div>';
        return __p;
    };
})();

(function() {

    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["ChartBase"] = window["Reuters"]["Graphics"]["ChartBase"] || {};

    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["BarChart"] = window["Reuters"]["Graphics"]["BarChart"] || {};


    //the view that constructs a linechart
    Reuters.Graphics.ChartBase = Backbone.View.extend({
        groupSort: "descending",
        categorySort: "ascending",
        margin: {
            top: 40,
            right: 20,
            bottom: 30,
            left: 40
        },
        dateFormat: d3.time.format("%b %Y"),
        tipTemplate: Reuters.Graphics.basicCharter.Template.tooltip,
        chartTemplate: Reuters.Graphics.basicCharter.Template.chartTemplate,
        legendTemplate: Reuters.Graphics.basicCharter.Template.legendTemplate,
        tipNumbFormat: function tipNumbFormat(d) {
            var self = this;
            if (isNaN(d) === true) {
                return "N/A";
            } else {
                return self.dataLabels[0] + self.numbFormat(d) + self.dataLabels[1];
            }
        },
        colors: [blue3, purple3, orange3, red3, yellow3],
        dataType: 'value',
        xScaleTicks: 5,
        yScaleTicks: 5,
        YTickLabel: [
            ["", ""]
        ],
        numbFormat: d3.format(",.0f"),
        lineType: "linear",
        chartBreakPoint: 300,
        hasLegend: true,
        xOrY: "x",
        yOrX: "y",
        leftOrTop: "left",
        heightOrWidth: "height",
        widthOrHeight: "width",
        topOrLeft: "top",
        recessionDateParse: d3.time.format("%m/%d/%Y").parse,
        updateCount: 0,
        divisor: 1,
        timelineDate: d3.time.format("%m/%d/%Y"),
        timelineDateDisplay: d3.time.format("%b %e, %Y"),
        timelineTemplate: Reuters.Graphics.basicCharter.Template.tooltipTimeline,
        xTickFormat: Reuters.CurrentLocale.timeFormat.multi([
            ["%H:%M", function(d) {
                return d.getMinutes();
            }],
            ["%H:%M", function(d) {
                return d.getHours();
            }],
            ["%a %d", function(d) {
                return d.getDay() && d.getDate() != 1;
            }],
            ["%b %d", function(d) {
                return d.getDate() != 1;
            }],
            ["%B", function(d) {
                return d.getMonth();
            }],
            ["%Y", function() {
                return true;
            }]
        ]),
        initialize: function initialize(opts) {
            var self = this;
            this.options = opts;

            // if we are passing in options, use them instead of the defualts.
            _.each(opts, function(item, key) {
                self[key] = item;
            });

            if (self.timelineData) {
                self.loadTimelineData();
            } else {
                self.loadData();
            }
        },
        loadData: function loadData() {
            var self = this;
            //Test which way data is presented and load appropriate way
            if (this.dataURL.indexOf("csv") == -1 && !_.isObject(this.dataURL)) {
                d3.json(self.dataURL, function(data) {
                    self.parseData(data);
                });
            }
            if (this.dataURL.indexOf("csv") > -1) {
                d3.csv(self.dataURL, function(data) {
                    self.parseData(data);
                });
            }
            if (_.isObject(this.dataURL)) {
                setTimeout(function() {
                    self.parseData(self.dataURL);
                }, 100);
            }
        },
        loadTimelineData: function loadTimelineData() {
            var self = this;
            //Test which way data is presented and load appropriate way
            if (this.timelineData.indexOf("csv") == -1 && babelHelpers.typeof(this.timelineData) != "object") {
                d3.json(self.timelineData, function(data) {
                    self.timelineData = data;
                    self.loadData();
                    return;
                });
            }
            if (this.timelineData.indexOf("csv") > -1) {
                d3.csv(self.timelineData, function(data) {
                    self.timelineData = data;
                    // console.log(self.timelineData);
                    self.loadData();
                    return;
                });
            }
            if (babelHelpers.typeof(this.timelineData) == "object") {
                self.loadData();
                return;
            }
        },
        formatDataStream: function formatDataStream(response) {
            var newArray = [];

            response.Dates.forEach(function(d, i) {
                var obj = {};

                var newDate = d.replace(/\//g, '').replace('Date(', '').replace(')', '').replace('+0000', '');
                var date = new Date(+newDate);
                var betterDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
                var formatDate = betterDate.getMonth() + 1 + "/" + betterDate.getDate() + "/" + betterDate.getFullYear();

                obj.date = formatDate;

                response.DataTypeValues[0].SymbolValues.forEach(function(item, index) {
                    var name = response.DataTypeValues[0].SymbolValues[index].Symbol;
                    obj[name] = response.DataTypeValues[0].SymbolValues[index].Value[i];
                });

                newArray.push(obj);
            });
            return newArray;
        },
        parseData: function parseData(data) {
            var self = this;

            if (self.dataStream) {
                var response = data.DataResponse || data.DataResponses[0];
                data = self.formatDataStream(response);
            }

            //figuring out if there is a timescale, is this necessary?
            if (data[0].date) {
                self.hasTimeScale = true;
            }
            // if parser undefined, figure out if it's a 4 year or 2 year date and set parser to match
            if (self.hasTimeScale && !self.parseDate) {
                if (data[0].date.split('/')[2].length == 2) {
                    self.parseDate = d3.time.format("%m/%d/%y").parse;
                }
                if (data[0].date.split('/')[2].length == 4) {
                    self.parseDate = d3.time.format("%m/%d/%Y").parse;
                }
            }
            //figure out current chart label, if it's switching between them.
            if (self.chartLayoutLables) {
                self.chartLayout = self.chartLayoutLables[self.chartLayoutLables.length - 1];
            }

            //set values if horizontal or not
            if (self.horizontal) {
                self.xOrY = "y";
                self.yOrX = "x";
                self.leftOrTop = "top";
                self.heightOrWidth = "width";
                self.widthOrHeight = "height";
                self.topOrLeft = "left";
            }

            //find values to map, if not defined
            if (!self.columnNames) {
                self.columnNames = _.keys(data[0]).filter(function(d) {
                    return d != "date" && d != "category" && d !== "type" && d !== "rawDate" && d !== "displayDate";
                });
                self.columnNamesDisplay = self.columnNames;
            }
            if (_.isObject(self.columnNames) && !_.isArray(self.columnNames)) {
                self.columnNamesDisplay = _.values(self.columnNames);
                self.columnNames = _.keys(self.columnNames);
            }
            if (_.isArray(self.columnNames) && !self.columnNamesDisplay) {
                self.columnNamesDisplay = self.columnNames;
            }

            self.colorScale = d3.scale.ordinal();
            //figure out the color scale
            if (_.isObject(self.colors) && !_.isArray(self.colors)) {
                self.colorScale.domain(_.keys(self.colors));
                self.colorScale.range(_.values(self.colors));
            }
            if (_.isArray(self.colors)) {
                self.colorScale.domain(self.columnNames);
                self.colorScale.range(self.colors);
            }

            //handle multidata
            if (data[0].type) {
                if (!self.multiDataColumns) {
                    self.multiDataColumns = _.uniq(_.pluck(data, 'type'));
                }
                var groupedData = _.groupBy(data, "type");
                self.multiDataColumns.forEach(function(d) {
                    self.modelData(groupedData[d], d);
                });
            } else {
                if (self.multiDataColumns) {
                    self.dataType = self.multiDataColumns[self.multiDataColumns.length - 1];
                }
                self.modelData(data, "dataholder");
            }

            //make labels if none
            if (!self.multiDataLabels && self.multiDataColumns) {
                self.multiDataLabels = self.multiDataColumns;
            }

            self.flattenData(self.chartData);
            self.baseRender();
            self.renderChart();
        },

        //function to make all the data.
        modelData: function modelData(data, name) {
            var self = this;

            if (!self.groupedData) {
                self.groupedData = {};
            }

            self.groupedData[name] = new Reuters.Graphics.DataPointCollection([], {
                parseDate: self.parseDate,
                dateFormat: self.dateFormat
            });
            self.groupedData[name].reset(data, {
                parse: true
            });

            self[name] = new Reuters.Graphics.DateSeriesCollection([], {
                parseDate: self.parseDate,
                groupSort: self.groupSort,
                divisor: self.divisor,
                categorySort: self.categorySort,
                dataType: self.dataType,
                multiDataColumns: self.multiDataColumns,
                dateFormat: self.dateFormat
            });

            self[name].reset(self.columnNames.map(function(d, i) {
                return {
                    name: d,
                    displayName: self.columnNamesDisplay[i],
                    values: self.groupedData[name]
                };
            }), {
                parse: true
            });
            self.chartData = self[name];
        },

        flattenData: function flattenData(data) {
            var self = this;

            var filtered = data.filter(function(d) {
                return d.get("visible");
            });

            self.jsonData = _.invoke(filtered, 'toJSON');
            self.jsonData.forEach(function(d) {
                var name = d.name;
                d.values = d.values.toJSON();
                d.values.forEach(function(point) {
                    _.extend(point, point[name]);
                    point.name = name;
                });
            });

            if (self.timelineData) {
                self.showTip = true;
                var closestData = null;
                self.timelineData.forEach(function(timelineItem) {
                    timelineItem.parsedDate = self.parseDate(timelineItem.date);

                    self.chartData.first().get("values").each(function(d, i) {
                        if (closestData === null || Math.abs(d.get("date") - timelineItem.parsedDate) < Math.abs(closestData - timelineItem.parsedDate)) {
                            closestData = d.get("date");
                        }
                        timelineItem.closestDate = closestData;
                        timelineItem.formatedDate = self.timelineDate(timelineItem.closestDate);
                    });
                });
                self.timelineDataGrouped = _.groupBy(self.timelineData, "formatedDate");
            }
        },
        barCalculations: function barCalculations() {
            var self = this;
            // some aspects of the data useful for figuring out bar placement
            self.dataLength = 0;

            self.jsonData.forEach(function(d) {
                if (d.values.length > self.dataLength) {
                    self.dataLength = d.values.length;
                }
            });
            self.numberOfObjects = function() {
                if (self.chartLayout == "onTopOf") {
                    return 1;
                } else {
                    return self.jsonData.length;
                }
            };

            self.widthOfBar = function() {
                if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent") {
                    return self[self.widthOrHeight] / self.dataLength - self[self.widthOrHeight] / self.dataLength * 0.2;
                } else {
                    return self[self.widthOrHeight] / (self.dataLength * self.numberOfObjects()) - self[self.widthOrHeight] / (self.dataLength * self.numberOfObjects()) * 0.2;
                }
            };
        },

        baseRender: function baseRender() {
            var self = this;
            self.trigger("baseRender:start");

            //make basic template and set names of stuff
            self.$el.html(self.chartTemplate({
                self: self
            }));
            if (self.$el.width() < self.chartBreakPoint) {
                self.$el.find('.chart-holder').addClass("smaller");
            }

            //make a label based on the div's ID to use as unique identifiers
            self.targetDiv = self.$el.attr("id");
            self.chartDiv = self.targetDiv + " .chart";
            self.legendDiv = self.targetDiv + " .legend";
            self.$chartEl = $("#" + self.chartDiv);
            self.$legendEl = $("#" + self.legendDiv);

            //set the width and the height to be the width and height of the div the chart is rendered in
            self.width = self.$chartEl.width() - self.margin.left - self.margin.right;

            //if no height set, square, otherwise use the set height, if lower than 10, it is a ratio to width
            if (!self.options.height) {
                self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
            }
            if (self.options.height < 10) {
                if ($(window).width() < 400) {
                    self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
                } else {
                    self.height = self.$chartEl.width() * self.options.height - self.margin.top - self.margin.bottom;
                }
            }

            self.barCalculations();

            //create an SVG
            self.svg = d3.select("#" + self.chartDiv).append("svg").attr({
                width: self.width + self.margin.left + self.margin.right,
                height: self.height + self.margin.top + self.margin.bottom
            }).append("g").attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");


            //make a rectangle so there is something to click on
            self.svg.append("svg:rect").attr({
                width: self.width,
                height: self.height,
                class: "plot"
            });

            //make a clip path for the graph
            self.clip = self.svg.append("svg:clipPath").attr("id", "clip" + self.targetDiv).append("svg:rect").attr({
                x: -self.margin.left,
                y: -4,
                width: self.width + self.margin.left + 8,
                height: self.height + 8
            });

            //go get the scales from the chart type view
            this.scales = {
                x: this.getXScale(),
                y: this.getYScale()
            };

            //render all the incidentals.
            self.recessionMaker();
            self.scaleMaker();
            self.toolTipMaker();
            self.legendMaker();
            self.multiDataMaker();
            self.chartLayoutMaker();
            self.baseUpdate(1);

            $(window).on("resize", _.debounce(function(event) {
                var width = self.$el.width();
                if (width < self.chartBreakPoint) {
                    self.$el.find('.chart-holder').addClass("smaller");
                } else {
                    self.$el.find('.chart-holder').removeClass("smaller");
                }
                self.update();
            }, 100));

            self.trigger("baseRender:end");
            //end of chart render
            return this;
        },

        setWidthAndMargins: function setWidthAndMargins() {
            var self = this;

            //length of largest tick
            var maxWidth = -1;
            $("#" + self.targetDiv + " .y.axis").find("text").not(".topTick").each(function() {
                maxWidth = maxWidth > $(this).width() ? maxWidth : $(this).width();
            });
            if (maxWidth === 0) {
                $("#" + self.targetDiv + " .y.axis").find("text").not(".topTick").each(function() {
                    maxWidth = maxWidth > $(this)[0].getBoundingClientRect().width ? maxWidth : $(this)[0].getBoundingClientRect().width;
                });
            }

            if (!self.options.margin) {
                self.margin = {
                    top: 15,
                    right: 20,
                    bottom: 30,
                    left: 9 + maxWidth
                };
                if (self.orient == "right") {
                    self.margin = {
                        top: 15,
                        left: 5,
                        bottom: 30,
                        right: 20 + maxWidth
                    };
                }
            }

            self.width = this.$chartEl.width() - self.margin.left - self.margin.right;
            if (!self.options.height) {
                self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
            }
            if (self.options.height < 10) {
                if ($(window).width() < 400) {
                    self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
                } else {
                    self.height = self.$chartEl.width() * self.options.height - self.margin.top - self.margin.bottom;
                }
            }
        },

        recessionMaker: function recessionMaker() {
            var self = this;
            //put in the recessions, if there are any.
            if (!self.hasRecessions) {
                return;
            }

            var recessionData = [{
                "recess": [{
                    "start": "5/1/1937",
                    "end": "6/1/1938"
                }, {
                    "start": "2/1/1945",
                    "end": "10/1/1945"
                }, {
                    "start": "11/1/1948",
                    "end": "10/1/1949"
                }, {
                    "start": "7/1/1953",
                    "end": "5/1/1954"
                }, {
                    "start": "8/1/1957",
                    "end": "4/1/1958"
                }, {
                    "start": "4/1/1960",
                    "end": "2/1/1961"
                }, {
                    "start": "12/1/1969",
                    "end": "11/1/1970"
                }, {
                    "start": "11/1/1973",
                    "end": "3/1/1975"
                }, {
                    "start": "1/1/1980",
                    "end": "7/1/1980"
                }, {
                    "start": "7/1/1981",
                    "end": "11/1/1982"
                }, {
                    "start": "7/1/1990",
                    "end": "3/1/1991"
                }, {
                    "start": "3/1/2001",
                    "end": "11/1/2001"
                }, {
                    "start": "12/1/2007",
                    "end": "6/1/2009"
                }]
            }];
            self.recessions = self.svg.selectAll('.recession').data(recessionData);

            self.recessionsEnter = self.recessions.enter().append('g').attr({
                "clip-path": "url(#clip" + self.targetDiv + ")",
                class: "recession"
            });

            self.recessionsEnter.selectAll("rect").data(function(d) {
                return d.recess;
            }).enter().append("rect").attr({
                class: "recessionBox",
                x: function x(d) {
                    return self.scales.x(self.recessionDateParse(d.start));
                },
                y: 0,
                width: function width(d) {
                    return self.scales.x(self.recessionDateParse(d.end)) - self.scales.x(self.recessionDateParse(d.start));
                },
                height: self.height
            });
        },

        scaleMaker: function scaleMaker() {
            var self = this;

            //create and draw the x axis
            self.xAxis = d3.svg.axis().scale(self.scales[self.xOrY]).orient("bottom").ticks(self[self.xOrY + "ScaleTicks"]).tickPadding(8);

            //create and draw the y axis
            self.yAxis = d3.svg.axis().scale(self.scales[self.yOrX]).orient("left").ticks(self[self.yOrX + "ScaleTicks"]).tickPadding(8);

            if (self.orient == "right") {
                self.yAxis.orient("right").tickPadding(20);
            }

            //change the tic size if it's sideways
            if (self.horizontal) {
                self.xAxis.tickSize(0 - self.height).tickPadding(12);
            }
            //forces a tick for every value on the x scale
            if (self.tickAll) {
                self.fullDateDomain = [];
                self.smallDateDomain = [];
                self.chartData.first().get("values").each(function(d, i) {
                    self.fullDateDomain.push(d.get("date"));
                    if (i === 0 || i == self.dataLength - 1) {
                        self.smallDateDomain.push(d.get("date"));
                    }
                });
            }

            if (self.hasTimeScale || self.options.xTickFormat) {
                self[self.xOrY + "Axis"].tickFormat(self.xTickFormat);
            }

            //FIX the tier thing needs rethought
            if (self.chartLayout == "tier") {
                self[self.yOrX + "Axis"].ticks(2);
            }

            //define the tick format if it's specified, change tick length if it's horizontal
            if (self.yTickFormat) {
                self[self.yOrX + "Axis"].tickFormat(self.yTickFormat);
            }
            if (!self.horizontal) {
                self.yAxis.tickSize(0 - self.width);
            } else {
                self.yAxis.tickSize(0);
            }

            //if autoScale ing then let it use the default auto scale.  hasZoom and multiData automatically get auto-scaling
            if (self.yScaleVals && !self.hasZoom) {
                self[self.yOrX + "Axis"].tickValues(self.yScaleVals);
            }
            //draw all the axis
            self.svg.append("svg:g").attr("class", "x axis");
            self.svg.select(".x.axis").attr("transform", "translate(0," + self.height + ")").call(self.xAxis);
            self.svg.append("svg:g").attr("class", "y axis");
            self.svg.select(".y.axis").attr("transform", function(d) {
                if (self.orient == "right") return "translate(" + self.width + ",0)";
            }).call(self.yAxis);

            self.adjustXTicks();

            if (!self.horizontal) {
                self.svg.selectAll(".y.axis line").attr("x1", -self.margin.left);
            }

            //get the latest labels
            self.dataLabels = self.YTickLabel[self.YTickLabel.length - 1];
            self.topTick(self.dataLabels);
        },

        makeZeroLine: function makeZeroLine() {
            var self = this;
            self.zeroLine = self.svg.append("line").attr("class", "zeroAxis").attr("clip-path", "url(#clip" + self.targetDiv + ")").attr(self.xOrY + "1", function() {
                if (self.horizontal) {
                    return 0;
                }
                return -self.margin[self.leftOrTop];
            }).attr(self.xOrY + "2", self[self.widthOrHeight]).attr(self.yOrX + "1", self.scales.y(0)).attr(self.yOrX + "2", self.scales.y(0));
        },

        topTick: function topTick(tickLabels) {
            var self = this;

            d3.selectAll("#" + self.targetDiv + " .topTick").remove();

            var topTick = $("#" + self.targetDiv + " ." + self.yOrX + ".axis .tick:last-of-type").find("text");
            var topTickHeight = topTick.height();
            if (topTickHeight === 0) {
                topTickHeight = topTick[0].getBoundingClientRect().height;
            }

            d3.selectAll("#" + self.targetDiv + " ." + self.yOrX + ".axis .tick text").attr("transform", "translate(0,-" + topTickHeight / 2 + ")");

            var topTickHTML = topTick.text();
            var backLabel = "";
            if (self.horizontal) {
                backLabel = tickLabels[1];
            }
            topTick.text(tickLabels[0] + topTickHTML + backLabel);
            if (!self.horizontal) {
                topTick.clone().appendTo(topTick.parent()).text(tickLabels[1]).css('text-anchor', "start").attr("class", "topTick");
            }
        },

        toolTipMaker: function toolTipMaker() {
            var self = this;

            self.baseElement = document.getElementById(self.targetDiv).querySelector('svg');
            self.svgFind = self.baseElement;
            self.pt = self.svgFind.createSVGPoint();

            //start the cursor off to the left
            self.xPointCursor = 0 - self.margin[self.leftOrTop] - 500;
            //add a line
            self.cursorLine = self.svg.append('svg:line').attr('class', 'cursorline').attr("clip-path", "url(#clip" + self.targetDiv + ")").attr(self.xOrY + '1', self.xPointCursor).attr(self.xOrY + '2', self.xPointCursor).attr(self.yOrX + '1', 0).attr(self.yOrX + '2', self[self.heightOrWidth]);

            // tooltip divs, default turns em off.  They get turned on if you have no legend, or if you specifically turn them on.
            self.tooltip = d3.select("#" + self.chartDiv).append("div").attr("class", "reuters-tooltip").style({
                opacity: 0,
                display: function display() {
                    if (self.showTip || !self.hasLegend) {
                        return "block";
                    }
                    return "none";
                }
            });

            self.svgmove = self.svgFind.addEventListener('mousemove', function(evt) {
                return self.tooltipMover(evt);
            }, false);
            self.svgtouch = self.svgFind.addEventListener('touchmove', function(evt) {
                return self.tooltipMover(evt);
            }, false);
            self.svgout = self.svgFind.addEventListener('mouseout', function(evt) {
                return self.tooltipEnd(evt);
            }, false);
            self.svgtouchout = self.svgFind.addEventListener('touchend', function(evt) {
                return self.tooltipEnd(evt);
            }, false);
        },

        tooltipMover: function tooltipMover(evt, self) {
            var self = this;
            self.loc = self.cursorPoint(evt);
            self.xPointCursor = self.loc[self.xOrY];
            self.yPointCursor = self.loc[self.yOrX];

            //little maths to figure out in the sideBySide layout what data point you are on.
            var widthsOver = 0;
            if (self.chartLayout == "sideBySide") {
                var eachChartWidth = self[self.widthOrHeight] / self.numberOfObjects();
                for (i = 0; i < self.numberOfObjects; i++) {
                    if (self.xPointCursor - self.margin[self.leftOrTop] > eachChartWidth) {
                        self.xPointCursor = self.xPointCursor - eachChartWidth;
                        widthsOver = widthsOver + eachChartWidth;
                    }
                }
            }
            if (self.chartLayout == "tier") {
                widthsOver = self.widthOfBar() * self.numberOfObjects() / 2 - self.widthOfBar() / 2;
            }

            var toolTipModifier = 0;
            //use a reverse scale to figure out where you are at if there is a time scale.
            //if it's an ordinal scale it's a little trickier.
            self.closestData = null;
            var indexLocation = self.xPointCursor - parseFloat(self.margin[self.leftOrTop]);

            if (self.hasTimeScale) {
                self.locationDate = self.scales.x.invert(indexLocation);
                self.chartData.first().get("values").each(function(d, i) {
                    if (self.closestData === null || Math.abs(d.get("date") - self.locationDate) < Math.abs(self.closestData - self.locationDate)) {
                        self.closestData = d.get("date");
                    }
                });
                if (self.timelineData) {
                    self.closestData = null;
                    self.timelineData.forEach(function(d, i) {
                        if (self.closestData === null || Math.abs(d.closestDate - self.locationDate) < Math.abs(self.closestData - self.locationDate)) {
                            self.closestData = d.closestDate;
                        }
                    });
                }
            } else {
                var closestRange = null;
                self.scales.x.range().forEach(function(d) {
                    if (closestRange === null || Math.abs(d - indexLocation) < Math.abs(closestRange - indexLocation)) {
                        closestRange = d;
                    }
                });
                var closestIndex = self.scales.x.range().indexOf(closestRange);
                self.closestData = self.scales.x.domain()[closestIndex];
                toolTipModifier = self.widthOfBar() / 2;
            }

            //Got the value, now let's move the line.
            self.cursorLine.attr(self.xOrY + '1', self.scales.x(self.closestData) + toolTipModifier + widthsOver).attr(self.xOrY + '2', self.scales.x(self.closestData) + toolTipModifier + widthsOver);

            //highlight current bar
            self.highlightCurrent();

            //and now we can update the tooltip
            self.tooltip.html(function(d) {
                return self.tipTemplate({
                    self: self,
                    data: self.makeTipData()
                });
            }).style(self.leftOrTop, function(d) {
                var tipWidth = $("#" + self.targetDiv + " .reuters-tooltip").outerWidth();

                if (self.horizontal) {
                    tipWidth = $("#" + self.targetDiv + " .reuters-tooltip").outerHeight();
                }
                if (self.xPointCursor < (self.margin.left + self.width + self.margin.right) / 2) {
                    return self.margin[self.leftOrTop] + self.scales.x(self.closestData) + toolTipModifier + 15 + "px";
                } else {
                    return self.scales.x(self.closestData) + toolTipModifier - tipWidth + 15 + "px";
                }
            }).style(self.topOrLeft, function(d) {
                var toolTipHeight = $("#" + self.targetDiv + " .reuters-tooltip").height();
                if (self.horizontal) {
                    toolTipHeight = $("#" + self.targetDiv + " .reuters-tooltip").outerWidth();
                }
                if (self.yPointCursor > (self.margin.top + self.height + self.margin.bottom) * 2 / 3) {
                    return self.yPointCursor - toolTipHeight - 20 + "px";
                }
                if (self.yPointCursor < (self.margin.top + self.height + self.margin.bottom) / 3) {
                    return self.yPointCursor + "px";
                }
                return self.yPointCursor - toolTipHeight / 2 + "px";
            });

            //and now we can update the legend.
            if (self.hasLegend) {
                var legendData = self.makeTipData();

                d3.select("#" + self.legendDiv).selectAll(".valueTip").data(legendData, function(d) {
                    return d.name;
                }).html(function(d, i) {
                    if (self.chartLayout == "stackPercent") {
                        return self.tipNumbFormat(d.y1Percent - d.y0Percent);
                    }
                    return self.tipNumbFormat(d[self.dataType]);
                });

                self.legendDate.html(function() {
                    if (legendData[0].category) {
                        return legendData[0].category;
                    }
                    if (legendData[0].quarters) {
                        return legendData[0].quarters + legendData[0].displayDate;
                    }
                    return legendData[0].displayDate;
                });

                self.setLegendPositions();
            }

            self.tooltip.style({
                opacity: 1
            });
        },

        highlightCurrent: function highlightCurrent() {
            var self = this;
            if (self.chartType == "bar") {
                self.barChart.selectAll(".bar").classed("lighter", function(d) {
                    if (d.date == self.closestData || d.category == self.closestData) {
                        return false;
                    }
                    return true;
                });
            }
            if (self.chartType == "line") {
                self.lineChart.selectAll(".tipCircle").classed("highlight", function(d) {
                    if (d.date == self.closestData || d.category == self.closestData) {
                        return true;
                    }
                    return false;
                });
            }
        },

        makeTipData: function makeTipData() {
            var self = this;
            var xDataType = "category";
            if (self.hasTimeScale) {
                xDataType = "date";
            }
            var filtered = self.chartData.filter(function(d) {
                return d.get("visible");
            });
            var tipData = [];
            filtered.forEach(function(d) {
                var name = d.get("name");
                var displayName = d.get("displayName");
                var timeFormatter = d3.time.format("%m/%d/%Y");
                var matchingValues = d.get("values").filter(function(d) {
                    if (self.hasTimeScale) {
                        return timeFormatter(d.get(xDataType)) == timeFormatter(self.closestData);
                    }
                    return d.get(xDataType) == self.closestData;
                });
                matchingValues.forEach(function(d) {
                    var matchObj = d.toJSON();
                    _.extend(matchObj, matchObj[name]);
                    matchObj.name = name;
                    matchObj.displayName = displayName;
                    tipData.push(matchObj);
                });
            });
            return tipData;
        },

        cursorPoint: function cursorPoint(evt) {
            var self = this;
            if (evt.clientX && evt.clientY) {
                self.pt.x = evt.clientX;
                self.pt.y = evt.clientY;
            } else if (evt.targetTouches) {
                self.pt.x = evt.targetTouches[0].clientX;
                self.pt.y = evt.targetTouches[0].clientY;
                self.pt.deltaX = Math.abs(self.pt.x - self.pt.lastX);
                self.pt.deltaY = Math.abs(self.pt.y - self.pt.lastY);
                if (self.pt.deltaX > self.pt.deltaY) {
                    evt.preventDefault();
                }
                self.pt.lastY = self.pt.y;
                self.pt.lastX = self.pt.x;
            }
            return self.pt.matrixTransform(self.svgFind.getScreenCTM().inverse());
        },

        tooltipEnd: function tooltipEnd(evt) {
            var self = this;
            self.cursorLine.attr(self.xOrY + '1', 0 - self.margin[self.leftOrTop] - 10).attr(self.xOrY + '2', 0 - self.margin[self.leftOrTop] - 10);

            self.tooltip.style("opacity", 0);

            if (self.hasLegend) {
                self.legendItems.selectAll(".valueTip").html("<br>");

                self.legendDate.html("<br>");
                self.setLegendPositions();
            }
            if (self.chartType == "bar") {
                self.barChart.selectAll(".bar").classed("lighter", false);
            }
            if (self.chartType == "line") {
                self.lineChart.selectAll(".tipCircle").classed("highlight", false);
            }
        },

        multiDataMaker: function multiDataMaker() {
            var self = this;
            if (!self.multiDataColumns) {
                return;
            }
            self.$(".chart-nav .btn").on("click", function(evt) {

                var thisID = $(this).attr("dataid");
                var i = self.$(".chart-nav .btn").index(this);

                if (self.YTickLabel[i]) {
                    self.dataLabels = self.YTickLabel[i];
                } else {
                    self.dataLabels = self.YTickLabel[0];
                }

                //this has to act differently if you're doing a data transform, or picking up totally different data.
                if (!self[thisID]) {
                    self.dataType = thisID;
                } else {
                    self.chartData = self[thisID];
                    self.flattenData(self.chartData);
                }

                self.update();
            });
            return;
        },

        chartLayoutMaker: function chartLayoutMaker() {
            var self = this;
            if (!self.chartLayoutLables) {
                return;
            }

            self.makeNavLayout = d3.select("#" + self.targetDiv + " .chart-layout").append("div").attr("class", "layoutNavContainer");

            self.makeNavLayoutButtons = self.makeNavLayout.selectAll('.layoutNavButtons').data(self.chartLayoutLables).enter().append("div").attr("class", "layoutNavButtons").attr("dataid", function(d) {
                return d;
            }).style("background-position", function(d) {
                //makes the sprite work.  i should change this to a class instead.
                var positionArray = ["basicbar", "stackTotalbar", "stackPercentbar", "sideBySidebar", "tierbar", "onTopOfbar", "basicline", "stackTotalline", "stackPercentline", "fillLinesline"];
                var positionNumber = positionArray.indexOf(d + self.chartType);
                return "0px " + -(positionNumber * 40) + "px";
            }).classed("selected", function(d, i) {
                if (i == self.chartLayoutLables.length - 1) {
                    return true;
                } else {
                    return false;
                }
            }).on("click", function(d, i) {
                if ($(this).hasClass("selected")) {
                    return;
                }
                var thisID = $(this).attr("dataid");
                $(this).addClass("selected").siblings().removeClass("selected");

                self.chartLayout = d;

                //get the right data labels
                if (self.YTickLabel[i]) {
                    self.dataLabels = self.YTickLabel[i];
                } else {
                    self.dataLabels = self.YTickLabel[0];
                } //and override them if it's stack percent
                if (self.chartLayout == "stackPercent") {
                    self.dataLabels = ["", "%"];
                }

                //run the updater
                self.update();
            });
        },

        legendMaker: function legendMaker() {
            var self = this;
            //if no legend, get out of here.
            if (!self.hasLegend) {
                return;
            }
            self.$legendEl.html(self.legendTemplate({
                data: self.jsonData,
                self: self
            }));

            self.legendItems = d3.selectAll("#" + self.legendDiv + ' .legendItems').data(self.jsonData).on("click", function(d) {
                var that = this;
                self.chartData.where({
                    name: d.name
                }).forEach(function(d) {
                    if ($(that).hasClass("clicked")) {
                        d.set({
                            visible: true
                        });
                    } else {
                        d.set({
                            visible: false
                        });
                    }
                });
                if (self.multiDataColumns && self[self.multiDataColumns[0]]) {
                    self.multiDataColumns.forEach(function(data) {
                        self[data].where({
                            name: d.name
                        }).forEach(function(d) {
                            if ($(that).hasClass("clicked")) {
                                d.set({
                                    visible: true
                                });
                            } else {
                                d.set({
                                    visible: false
                                });
                            }
                        });
                    });
                }

                $(this).toggleClass("clicked");
                self.flattenData(self.chartData);
                self.update();
            });

            self.legendValues = d3.select("#" + self.legendDiv).selectAll(".valueTip").data(self.jsonData);

            self.legendDate = d3.selectAll("#" + self.legendDiv + ' .dateTip');

            self.setLegendPositions();
        },

        setLegendPositions: function setLegendPositions() {
            var self = this;

            if (!self.hasLegend) {
                return;
            }
            var depth = 0;

            self.legendItems.data(self.jsonData, function(d) {
                return d.name;
            }).style("top", function(d, i) {
                var returnDepth = depth;
                depth += $(this).height() + 5;
                return returnDepth + "px";
            });
            self.legendItems.data(self.jsonData, function(d) {
                return d.name;
            }).exit().style("top", function(d, i) {
                var returnDepth = depth;
                depth += $(this).height() + 5;
                return returnDepth + "px";
            });
        },

        adjustXTicks: function adjustXTicks() {
            var self = this;

            var ticksWidth = 0;
            $("#" + self.targetDiv + " .x.axis .tick").find("text").each(function(d) {
                ticksWidth += $(this).width() + 5;
            });
            if (self.tickAll) {
                self[self.xOrY + "Axis"].tickValues(self.fullDateDomain);
            }

            if (ticksWidth > self.width) {
                self[self.xOrY + "Axis"].ticks(2);

                if (self.tickAll) {
                    self[self.xOrY + "Axis"].tickValues(self.smallDateDomain);
                }

                self.svg.select(".x.axis").transition().attr("transform", "translate(0," + self.height + ")").call(self.xAxis);
            }
        },

        baseUpdate: function baseUpdate(duration) {
            var self = this;
            self.trigger("baseUpdate:start");

            if (!duration) {
                duration = 1000;
            }
            self.setWidthAndMargins();
            self.setLegendPositions();
            self.barCalculations();

            self.svg.selectAll('.cursorline').attr(self.yOrX + '1', 0).attr(self.yOrX + '2', self[self.heightOrWidth]);

            d3.select("#" + self.targetDiv + " svg").transition().duration(duration).attr({
                width: self.width + self.margin.left + self.margin.right,
                height: self.height + self.margin.top + self.margin.bottom
            });

            // if (isMobile.any()) self.svg.transition().duration(duration).attr("transform", "translate(" + self.margin.left * 3 + "," + self.margin.top + ")");
            // else self.svg.transition().duration(duration).attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");
            self.svg.transition().duration(duration).attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");


            self.svg.select(".plot").transition().duration(duration).attr({
                width: self.width,
                height: self.height
            });

            self.clip.transition().duration(duration).attr({
                x: -self.margin.left,
                y: -4,
                width: self.width + self.margin.left + 8,
                height: self.height + 8
            });

            this.scales = {
                x: this.getXScale(),
                y: this.getYScale()
            };

            self.xAxis.scale(self.scales[self.xOrY]);

            self.yAxis.scale(self.scales[self.yOrX]);

            self[self.yOrX + "Axis"].tickSize(0 - self[self.widthOrHeight]);

            if (self.chartLayout == "tier") {
                self[self.yOrX + "Axis"].ticks(2);
            } else {
                self.yAxis.ticks(self[self.yOrX + "ScaleTicks"]);
                self.xAxis.ticks(self[self.xOrY + "ScaleTicks"]);
            }

            if (self.updateCount == 1) {
                self.adjustXTicks();
            }

            // update the axes,
            self.svg.select(".x.axis").transition().duration(duration).attr("transform", "translate(0," + self.height + ")").call(self.xAxis);

            self.svg.select(".y.axis").transition().duration(duration).attr("transform", function(d) {
                if (self.orient == "right") return "translate(" + self.width + ",0)";
            }).call(self.yAxis).each("end", function(d) {
                if (self.updateCount === 0) {
                    self.updateCount++;
                    setTimeout(function() {
                        self.update();
                    }, 10);
                } else {
                    self.updateCount = 0;
                }
            });

            if (!self.horizontal) {
                self.svg.selectAll(".y.axis line").attr("x1", "-" + self.margin.left);
            }

            //FIX - should be better x axis on the side by side
            if (self.chartLayout == "sideBySide") {
                self.svg.select("." + self.xOrY + ".axis").style("display", "none");
            } else {
                self.svg.select("." + self.xOrY + ".axis").style("display", "block");
            }

            //fix, tier is all screwy, should just rethink
            if (self.chartLayout == "tier") {

                self.barChart.selectAll("." + self.yOrX + ".axis").remove();

                self.barChart.append("svg:g").attr("class", self.yOrX + " axis").attr("transform", function(d) {
                    if (self.horizontal) {
                        return "translate(0," + self.height + ")";
                    } else {
                        return "translate(0,0)";
                    }
                });

                self.barChart.data(self.chartData, function(d) {
                    return d.name;
                }).selectAll("." + self.yOrX + ".axis").call(self[self.yOrX + "Axis"]);
                self.barChart.each(function(d) {
                    var thisId = $(this).attr("id");
                    var barAxis = $("#" + thisId + " .axis").detach();
                    barAxis.prependTo($(this));
                });
            } else {}
            //FIX what's up here?



            //update the top tick label
            self.topTick(self.dataLabels);

            //zero line
            if (self.zeroLine) {
                self.zeroLine.transition().duration(duration).attr(self.xOrY + "1", function() {
                    if (self.horizontal) {
                        return 0;
                    }
                    return "-" + self.margin[self.leftOrTop];
                }).attr(self.xOrY + "2", self[self.widthOrHeight]).attr(self.yOrX + "1", self.scales.y(0)).attr(self.yOrX + "2", self.scales.y(0));
            }

            //recessions
            self.svg.selectAll(".recessionBox").transition().duration(duration).attr({
                x: function x(d) {
                    return self.scales.x(self.recessionDateParse(d.start));
                },
                width: function width(d) {
                    return self.scales.x(self.recessionDateParse(d.end)) - self.scales.x(self.recessionDateParse(d.start));
                },
                height: self.height
            });

            if (self.zoom) {
                self.zoom.x(self.scales.x).y(self.scales.y);
            }
            self.trigger("baseUpdate:end");
            //end of base update
        },

        zoomChart: function zoomChart() {
                var self = this;
                //if there is a zoom, then setup the zoom
                //define the zoom
                self.zoom = d3.behavior.zoom().x(self.scales.x).y(self.scales.y).scaleExtent([1, 8]).on("zoom", zoomed);

                //call the zoom on the SVG
                self.svg.call(self.zoom);

                //define the zoom function
                function zoomed() {
                    self.svg.select(".x.axis").call(self.xAxis);
                    self.svg.select(".y.axis").call(self.yAxis);

                    if (!self.horizontal) {
                        self.svg.selectAll(".y.axis line").attr("x1", -self.margin.left);
                    }

                    //get the latest labels
                    self.dataLabels = self.YTickLabel[self.YTickLabel.length - 1];
                    self.topTick(self.dataLabels);

                    if (self.chartType == "line") {
                        self.lineChart.data(self.jsonData, function(d) {
                            return d.name;
                        }).selectAll(".tipCircle").data(function(d) {
                            return d.values;
                        }).attr("cx", function(d, i) {
                            return self.scales.x(d.date);
                        }).attr("cy", function(d, i) {
                            return self.scales.y(d[self.dataType]);
                        });

                        self.lineChart.selectAll(".line").data(self.jsonData, function(d) {
                            return d.name;
                        }).attr("d", function(d) {
                            return self.line(d.values);
                        });

                        self.lineChart.selectAll(".area").data(self.jsonData, function(d) {
                            return d.name;
                        }).attr("d", function(d, i) {
                            return self.area(d.values);
                        });
                    }
                    if (self.chartType == "bar") {

                        self.svg.selectAll(".barChart").data(self.jsonData, function(d) {
                            return d.name;
                        }).selectAll(".bar").data(function(d) {
                            return d.values;
                        }).attr(self.yOrX, function(d) {
                            return self.yBarPosition(d);
                        }).attr(self.heightOrWidth, function(d) {
                            return self.barHeight(d);
                        }).attr(self.widthOrHeight, function(d, i, j) {
                            return self.barWidth(d, i, j);
                        }).attr(self.xOrY, function(d, i, j) {
                            return self.xBarPosition(d, i, j);
                        });
                    }

                    self.zeroLine.attr(self.yOrX + "1", self.scales.y(0)).attr(self.yOrX + "2", self.scales.y(0));

                    self.svg.selectAll(".recessionBox").attr("x", function(d) {
                        return self.scales.x(self.recessionDateParse(d.start));
                    }).attr("width", function(d) {
                        return self.scales.x(self.recessionDateParse(d.end)) - self.scales.x(self.recessionDateParse(d.start));
                    });
                }
                //end of zoom
            }
            //end of view
    });
    //# sourceMappingURL=chartBase.js.map

    //the view that constructs a barChart  data, get, models
    Reuters.Graphics.BarChart = Reuters.Graphics.ChartBase.extend({
        defaults: _.defaults({
            someNewDefault: "yes"
        }, Reuters.Graphics.ChartBase.prototype.defaults),
        //setup the scales
        chartType: "bar",

        xScaleMin: function xScaleMin() {
            return d3.min(this.jsonData, function(c) {
                return d3.min(c.values, function(v) {
                    return v.date;
                });
            });
        },

        xScaleMax: function xScaleMax() {
            return d3.max(this.jsonData, function(c) {
                return d3.max(c.values, function(v) {
                    return v.date;
                });
            });
        },

        xScaleRange: function xScaleRange() {
            var objectNumber = this.numberOfObjects();
            if (this.chartLayout == "stackPercent" || this.chartLayout == "stackTotal") {
                objectNumber = 1;
            }
            var range = [this.widthOfBar() * objectNumber / 2, this[this.widthOrHeight] - this.widthOfBar() * objectNumber];
            if (this.chartLayout == "sideBySide") {
                range = [0, this[this.widthOrHeight] / (this.jsonData.length * (1 + 2 / this.jsonData[0].values.length))];
            }
            return range;
        },

        getXScale: function getXScale() {
            if (this.hasTimeScale) {
                return d3.time.scale().domain([this.xScaleMin(), this.xScaleMax()]).range(this.xScaleRange());
            } else {
                return d3.scale.ordinal().domain(this.jsonData[0].values.map(function(d) {
                    return d.category;
                })).rangeRoundBands([0, this[this.widthOrHeight]], 0);
            }
        },

        yScaleMin: function yScaleMin() {
            var theValues = this.dataType;
            if (this.chartLayout == "stackTotal") {
                theValues = "stackMin";
            }
            var min = d3.min(this.jsonData, function(c) {
                return d3.min(c.values, function(v) {
                    return v[theValues];
                });
            });
            if (this.chartLayout == "stackPercent") {
                min = 0;
            }
            if (min > 0) {
                min = 0;
            }
            return min;
        },

        yScaleMax: function yScaleMax() {
            var theValues = this.dataType;
            if (this.chartLayout == "stackTotal") {
                theValues = "stackTotal";
            }
            var max = d3.max(this.jsonData, function(c) {
                return d3.max(c.values, function(v) {
                    return v[theValues];
                });
            });
            if (this.chartLayout == "stackPercent") {
                max = 100;
            }
            if (max < 0) {
                max = 0;
            }
            return max;
        },

        yScaleRange: function yScaleRange() {
            var fullHeight = this[this.heightOrWidth];
            if (this.chartLayout == "tier") {
                fullHeight = this[this.heightOrWidth] / (this.jsonData.length * (1 + 2 / this.jsonData[0].values.length));
            }
            var rangeLow = fullHeight;
            var rangeHigh = 0;
            if (this.horizontal) {
                rangeLow = 0;
                rangeHigh = fullHeight;
            }
            return [rangeLow, rangeHigh];
        },

        getYScale: function getYScale() {
            if (!this.yScaleVals || this.hasZoom) {
                return d3.scale.linear().domain([this.yScaleMin(), this.yScaleMax()]).nice(this.yScaleTicks).range(this.yScaleRange());
            } else {
                return d3.scale.linear().domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]]).range(this.yScaleRange());
            }
        },

        xBarPosition: function xBarPosition(d, i, j) {
            var self = this;
            var theScale = 'category';
            var modifier = 0;
            if (self.hasTimeScale) {
                theScale = 'date';
                modifier = self.widthOfBar() * self.numberOfObjects() / 2;
            }
            if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "sideBySide" || self.chartLayout == "tier") {
                if (self.hasTimeScale) {
                    modifier = self.widthOfBar() / 2;
                    if (self.chartLayout == "sideBySide") {
                        modifier = 0;
                    }
                }
                return self.scales.x(d[theScale]) - modifier;
            } else {
                if (self.chartLayout == "onTopOf") {
                    return self.scales.x(d[theScale]) - modifier + self.widthOfBar() / (j + 1) * j / 2;
                } else {
                    return self.scales.x(d[theScale]) - j * self.widthOfBar() + self.widthOfBar() * (self.numberOfObjects() - 1) - modifier;
                }
            }
        },

        yBarPosition: function yBarPosition(d) {
            var self = this;
            if (isNaN(d[self.dataType])) {
                return 0;
            }
            var positioner = "y1";
            if (self.horizontal || d.y1Total < 0) {
                positioner = "y0";
            }
            if (self.horizontal && d.y1Total < 0) {
                positioner = "y1";
            }
            if (self.chartLayout == "stackTotal") {
                return self.scales.y(d[positioner + "Total"]);
            } else {
                if (self.chartLayout == "stackPercent") {
                    return self.scales.y(d[positioner + "Percent"]);
                } else {
                    var minOrMax = "max";
                    if (self.horizontal) {
                        minOrMax = "min";
                    }
                    return self.scales.y(Math[minOrMax](0, d[self.dataType]));
                }
            }
        },

        barHeight: function barHeight(d) {
            var self = this;
            if (isNaN(d[self.dataType])) {
                return 0;
            }
            if (self.chartLayout == "stackTotal") {
                return Math.abs(self.scales.y(d.y0Total) - self.scales.y(d.y1Total));
            } else {
                if (self.chartLayout == "stackPercent") {
                    return Math.abs(self.scales.y(d.y0Percent) - self.scales.y(d.y1Percent));
                } else {
                    return Math.abs(self.scales.y(d[self.dataType]) - self.scales.y(0));
                }
            }
        },

        barFill: function barFill(d) {
            var self = this;
            if (self.colorUpDown) {
                if (d[self.dataType] > 0) {
                    return self.colorScale.range()[0];
                } else {
                    return self.colorScale.range()[1];
                }
            } else {
                return self.colorScale(d.name);
            }
        },

        barWidth: function barWidth(d, i, j) {
            var self = this;
            if (self.chartLayout == "tier") {
                return self.widthOfBar() * self.numberOfObjects();
            }
            if (self.chartLayout == "onTopOf") {
                return self.widthOfBar() / (j + 1);
            } else {
                return self.widthOfBar();
            }
        },

        renderChart: function renderChart() {
            var self = this;
            self.trigger("renderChart:start");

            if (self.hasZoom) {
                self.zoomChart();
            }
            //enter g tags for each set of data, then populate them with bars.  some attribute added on end, for updating reasons
            self.barChart = self.svg.selectAll(".barChart").data(self.jsonData, function(d) {
                return d.name;
            }).enter().append("g").attr("clip-path", "url(#clip" + self.targetDiv + ")").attr("class", "barChart").attr('id', function(d) {
                return self.targetDiv + d.displayName.replace(/\s/g, '') + "-bar";
            });

            if (self.chartLayout == "sideBySide") {
                self.barChart.attr("transform", function(d, i) {
                    if (!self.horizontal) {
                        return "translate(" + i * (self[self.widthOrHeight] / self.numberOfObjects()) + ",0)";
                    } else {
                        return "translate(0," + i * (self[self.widthOrHeight] / self.numberOfObjects()) + ")";
                    }
                });
            }
            if (self.chartLayout == "tier") {
                self.barChart.attr("transform", function(d, i) {
                    if (!self.horizontal) {
                        return "translate(0," + i * (self[self.heightOrWidth] / self.numberOfObjects()) + ")";
                    } else {
                        return "translate(" + i * (self[self.heightOrWidth] / self.numberOfObjects()) + ",0)";
                    }
                });
            }

            self.barChart.selectAll(".bar").data(function(d) {
                return d.values;
            }).enter().append("rect").attr("class", "bar").style("fill", function(d) {
                return self.barFill(d);
            }).attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0)).attr(self.widthOrHeight, function(d, i, j) {
                return self.barWidth(d, i, j);
            }).attr(self.xOrY, function(d, i, j) {
                return self.xBarPosition(d, i, j);
            }).transition().duration(1000).attr(self.yOrX, function(d) {
                return self.yBarPosition(d);
            }).attr(self.heightOrWidth, function(d) {
                return self.barHeight(d);
            });

            if (self.chartLayout == "sideBySide") {
                self.svg.select("." + self.xOrY + ".axis").style("display", "none");
            } else {
                self.svg.select("." + self.xOrY + ".axis").style("display", "block");
            }

            if (self.chartLayout == "tier") {
                self.barChart.append("svg:g").attr("class", self.yOrX + " axis").attr("transform", function(d) {
                    if (!self.horizontal) {
                        return "translate(0," + self.height + ")";
                    } else {
                        return "translate(0,0)";
                    }
                });

                self.barChart.selectAll("." + self.yOrX + ".axis").call(self[self.yOrX + "Axis"]);
                self.barChart.each(function(d) {
                    var thisId = $(this).attr("id");
                    var barAxis = $("#" + thisId + " .axis").detach();
                    barAxis.prependTo($(this));
                });
            } else {
                self.barChart.selectAll("." + self.yOrX + ".axis").remove();
            }

            //add teh zero line on top.
            self.makeZeroLine();
            self.trigger("renderChart:end");
            self.trigger("chart:loaded");

            //end of render
        },
        update: function update() {
            var self = this;
            self.baseUpdate();

            self.trigger("update:start");

            self.barChart.data(self.jsonData, function(d) {
                return d.name;
            }).order().transition().duration(1000).attr("transform", function(d, i) {
                var xPosit = 0;
                var yPosit = 0;
                if (self.chartLayout == "sideBySide") {
                    if (!self.horizontal) {
                        xPosit = i * (self[self.widthOrHeight] / self.numberOfObjects());
                    } else {
                        yPosit = i * (self[self.widthOrHeight] / self.numberOfObjects());
                    }
                }
                if (self.chartLayout == "tier") {
                    if (!self.horizonta) {
                        yPosit = i * (self[self.heightOrWidth] / self.numberOfObjects());
                    } else {
                        xPosit = i * (self[self.heightOrWidth] / self.numberOfObjects());
                    }
                }
                return "translate(" + xPosit + "," + yPosit + ")";
            });

            self.barChart.data(self.jsonData, function(d) {
                return d.name;
            }).exit().selectAll(".bar").transition().attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0));

            self.svg.selectAll(".barChart").data(self.jsonData, function(d) {
                return d.name;
            }).selectAll(".bar").data(function(d) {
                return d.values;
            }).transition().duration(1000).style("fill", function(d) {
                return self.barFill(d);
            }).attr(self.yOrX, function(d) {
                return self.yBarPosition(d);
            }).attr(self.heightOrWidth, function(d) {
                return self.barHeight(d);
            }).attr(self.widthOrHeight, function(d, i, j) {
                return self.barWidth(d, i, j);
            }).attr(self.xOrY, function(d, i, j) {
                return self.xBarPosition(d, i, j);
            });

            self.svg.selectAll(".barChart").data(self.jsonData, function(d) {
                return d.name;
            }).selectAll(".bar").data(function(d) {
                return d.values;
            }).exit().transition().attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0)).each("end", function(d) {
                d3.select(this).remove();
            });

            self.barChart.selectAll(".bar").data(function(d) {
                return d.values;
            }).enter().append("rect").attr("class", "bar").style("fill", function(d) {
                return self.barFill(d);
            }).attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0)).attr(self.widthOrHeight, function(d, i, j) {
                return self.barWidth(d, i, j);
            }).attr(self.xOrY, function(d, i, j) {
                return self.xBarPosition(d, i, j);
            }).transition().duration(1000).attr(self.yOrX, function(d) {
                return self.yBarPosition(d);
            }).attr(self.heightOrWidth, function(d) {
                return self.barHeight(d);
            });

            self.trigger("update:end");
            //end of update
        }

        //end of mdoel
    });
    //# sourceMappingURL=barChart.js.map

    Reuters.Graphics.LineChart = Reuters.Graphics.ChartBase.extend({
        defaults: _.defaults({
            someNewDefault: "yes"
        }, Reuters.Graphics.ChartBase.prototype.defaults),
        //setup the scales.  You have to do this in the specific view, it will be called in the Reuters.Graphics.ChartBase.
        chartType: "line",
        xScaleMin: function xScaleMin() {
            return d3.min(this.jsonData, function(c) {
                return d3.min(c.values, function(v) {
                    return v.date;
                });
            });
        },
        xScaleMax: function xScaleMax() {
            return d3.max(this.jsonData, function(c) {
                return d3.max(c.values, function(v) {
                    return v.date;
                });
            });
        },
        getXScale: function getXScale() {
            return d3.time.scale().domain([this.xScaleMin(), this.xScaleMax()]).range([0, this.width]);
        },
        yScaleMin: function yScaleMin() {
            var theValues = this.dataType;
            if (this.chartLayout == "stackTotal") {
                theValues = "stackTotal";
            }
            var min = d3.min(this.jsonData, function(c) {
                return d3.min(c.values, function(v) {
                    return v[theValues];
                });
            });
            if (this.chartlayout == "fillLines") {
                if (min > 0) {
                    min = 0;
                }
            }
            if (this.chartLayout == "stackTotal" || this.chartLayout == "stackPercent") {
                min = 0;
            }
            return min;
        },
        yScaleMax: function yScaleMax() {
            var theValues = this.dataType;
            if (this.chartLayout == "stackTotal") {
                theValues = "stackTotal";
            }
            var max = d3.max(this.jsonData, function(c) {
                return d3.max(c.values, function(v) {
                    return v[theValues];
                });
            });
            if (this.chartLayout == "stackPercent") {
                max = 100;
            }
            return max;
        },
        getYScale: function getYScale() {
            var self = this;
            if (!self.yScaleVals || this.hasZoom) {
                return d3.scale.linear().domain([this.yScaleMin(), this.yScaleMax()]).nice(this.yScaleTicks).range([this.height, 0]);
            } else {
                return d3.scale.linear().domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]]).nice(this.yScaleTicks).range([this.height, 0]);
            }
        },
        renderChart: function renderChart() {
            // create a variable called "self" to hold a reference to "this"
            var self = this;
            self.trigger("renderChart:start");

            if (self.hasZoom) {
                self.zoomChart();
            }

            //will draw the line
            self.line = d3.svg.line().x(function(d) {
                return self.scales.x(d.date);
            }).y(function(d) {
                if (self.chartLayout == "stackTotal") {
                    return self.scales.y(d.y1Total);
                } else {
                    if (self.chartLayout == "stackPercent") {
                        return self.scales.y(d.y1Percent);
                    } else {
                        return self.scales.y(d[self.dataType]);
                    }
                }
            }).interpolate(self.lineType).defined(function(d) {
                return !isNaN(d[self.dataType]);
            });

            self.area = d3.svg.area().x(function(d) {
                return self.scales.x(d.date);
            }).y0(function(d) {
                if (self.chartLayout == "stackTotal") {
                    return self.scales.y(d.y0Total);
                } else {
                    if (self.chartLayout == "stackPercent") {
                        return self.scales.y(d.y0Percent);
                    } else {
                        return self.scales.y(0);
                    }
                }
            }).y1(function(d) {
                if (self.chartLayout == "stackTotal") {
                    return self.scales.y(d.y1Total);
                } else {
                    if (self.chartLayout == "stackPercent") {
                        return self.scales.y(d.y1Percent);
                    } else {
                        return self.scales.y(d[self.dataType]);
                    }
                }
            }).interpolate(self.lineType).defined(function(d) {
                return !isNaN(d[self.dataType]);
            });

            //bind the data and put in some G elements with their specific mouseover behaviors.
            self.lineChart = self.svg.selectAll(".lineChart").data(self.jsonData, function(d) {
                return d.name;
            }).enter().append("g").attr({
                "clip-path": "url(#clip" + self.targetDiv + ")",
                class: "lineChart",
                id: function id(d) {
                    return self.targetDiv + d.displayName.replace(/\s/g, '') + "-line";
                }
            }).on("mouseover", function(d) {
                //put the line we've hovered on on top=
                self.lineChart.sort(function(a, b) {
                    if (a.name == d.name) {
                        return 1;
                    } else {
                        return -1;
                    }
                }).order();

                //class all other lines to be lighter
                d3.selectAll("#" + self.targetDiv + " .lineChart").classed('notSelected', true);
                d3.select(this).classed("notSelected", false);
            }).on("mouseout", function(d) {
                d3.selectAll(".lineChart").classed('notSelected', false);
            });

            self.lineChart.append("path").attr("class", "line").style("stroke", function(d) {
                return self.colorScale(d.name);
            }).attr("d", function(d) {
                return self.line(d.values[0]);
            }).transition().duration(1500).delay(function(d, i) {
                return i * 100;
            }).attrTween('d', function(d) {
                var interpolate = d3.scale.quantile().domain([0, 1]).range(d3.range(1, d.values.length + 1));
                return function(t) {
                    return self.line(d.values.slice(0, interpolate(t)));
                };
            });

            self.lineChart.append("path").attr("class", "area").style("fill", function(d) {
                return self.colorScale(d.name);
            }).attr("d", function(d) {
                return self.area(d.values[0]);
            }).style("display", function(d) {
                if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "fillLines") {
                    return "block";
                } else {
                    return "none";
                }
            }).transition().duration(1500).delay(function(d, i) {
                return i * 100;
            }).attrTween('d', function(d) {
                var interpolate = d3.scale.quantile().domain([0, 1]).range(d3.range(1, d.values.length + 1));
                return function(t) {
                    return self.area(d.values.slice(0, interpolate(t)));
                };
            });

            self.lineChart.selectAll(".tipCircle").data(function(d) {
                    return d.values;
                }).enter().append("circle").attr("class", "tipCircle").attr("cx", function(d, i) {
                    return self.scales.x(d.date);
                }).attr("cy", function(d, i) {
                    if (self.chartLayout == "stackTotal") {
                        return self.scales.y(d.y1Total);
                    } else {
                        if (self.chartLayout == "stackPercent") {
                            return self.scales.y(d.y1Percent);
                        } else {
                            return self.scales.y(d[self.dataType]);
                        }
                    }
                }).attr("r", function(d, i) {
                    if (isNaN(d[self.dataType])) {
                        return 0;
                    }
                    return 5;
                }).style('opacity', function(d) {
                    if (self.markDataPoints) {
                        return 1;
                    }
                    return 0;
                }).style("fill", function(d) {
                    return self.colorScale(d.name);
                }) //1e-6
                .classed("timeline", function(d) {
                    if (self.timelineDataGrouped) {
                        if (self.timelineDataGrouped[self.timelineDate(d.date)]) {
                            return true;
                        }
                    }
                    return false;
                });

            //add teh zero line on top.
            self.makeZeroLine();

            self.trigger("renderChart:end");
            self.trigger("chart:loaded");
            self.trigger("chart:loaded");

            //end chart render
        },
        update: function update() {
                var self = this;

                self.baseUpdate();
                self.trigger("update:start");

                self.exitLine = d3.svg.line().x(function(d) {
                    return self.scales.x(d.date);
                }).y(function(d) {
                    return self.margin.bottom + self.height + self.margin.top + 10;
                }).interpolate(self.lineType);

                self.exitArea = d3.svg.area().x(function(d) {
                    return self.scales.x(d.date);
                }).y0(function(d) {
                    return self.margin.bottom + self.height + self.margin.top + 10;
                }).y1(function(d) {
                    return self.margin.bottom + self.height + self.margin.top + 10;
                }).interpolate(self.lineType);

                //exiting lines
                self.lineChart.data(self.jsonData, function(d) {
                    return d.name;
                }).exit().selectAll(".line").transition().attr("d", function(d, i) {
                    return self.exitLine(d.values);
                });

                //exiting area
                self.lineChart.data(self.jsonData, function(d) {
                    return d.name;
                }).exit().selectAll(".area").transition().attr("d", function(d, i) {
                    return self.exitArea(d.values);
                });

                self.lineChart.data(self.jsonData, function(d) {
                    return d.name;
                }).exit().selectAll(".tipCircle").transition().attr("r", 0);

                //update the line
                self.lineChart.selectAll(".line").data(self.jsonData, function(d) {
                    return d.name;
                }).transition().duration(1000).attr("d", function(d, i) {
                    return self.line(d.values);
                });

                //update the area
                self.lineChart.selectAll(".area").data(self.jsonData, function(d) {
                    return d.name;
                }).style("display", function(d) {
                    if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "fillLines") {
                        return "block";
                    } else {
                        return "none";
                    }
                }).transition().duration(1000).attr("d", function(d, i) {
                    return self.area(d.values);
                });

                //the circles
                self.lineChart.data(self.jsonData, function(d) {
                    return d.name;
                }).selectAll(".tipCircle").data(function(d) {
                    return d.values;
                }).transition().duration(1000).attr("cy", function(d, i) {
                    if (self.chartLayout == "stackTotal") {
                        return self.scales.y(d.y1Total);
                    } else {
                        if (self.chartLayout == "stackPercent") {
                            return self.scales.y(d.y1Percent);
                        } else {
                            return self.scales.y(d[self.dataType]);
                        }
                    }
                }).attr("cx", function(d, i) {
                    return self.scales.x(d.date);
                }).attr("r", function(d, i) {
                    if (isNaN(d[self.dataType])) {
                        return 0;
                    }
                    return 5;
                });

                self.lineChart.data(self.jsonData, function(d) {
                    return d.name;
                }).selectAll(".tipCircle").data(function(d) {
                    return d.values;
                }).exit().transition().duration(1000).attr("r", 0).each("end", function(d) {
                    d3.select(this).remove();
                });

                self.lineChart.data(self.jsonData, function(d) {
                        return d.name;
                    }).selectAll(".tipCircle").data(function(d) {
                        return d.values;
                    }).enter().append("circle").attr("class", "tipCircle").attr("cx", function(d, i) {
                        return self.scales.x(d.date);
                    }).attr("cy", function(d, i) {
                        if (self.chartLayout == "stackTotal") {
                            return self.scales.y(d.y1Total);
                        } else {
                            if (self.chartLayout == "stackPercent") {
                                return self.scales.y(d.y1Percent);
                            } else {
                                return self.scales.y(d[self.dataType]);
                            }
                        }
                    }).style('opacity', function(d) {
                        if (self.markDataPoints) {
                            return 1;
                        }
                        return 0;
                    }).style("fill", function(d) {
                        return self.colorScale(d.name);
                    }) //1e-6
                    .attr("r", 0).transition().duration(1000).attr("r", function(d, i) {
                        if (isNaN(d[self.dataType])) {
                            return 0;
                        }
                        return 5;
                    });

                self.trigger("update:end");

                //end of update
            }
            //end model
    });
    //# sourceMappingURL=lineChart.js.map
    //individual points
    Reuters.Graphics.DataPointModel = Backbone.Model.extend({
        initialize: function initialize(attributes, options) {
            return;
        },

        parse: function parse(point, options) {
            if (point.date) {
                point.rawDate = point.date;
                point.date = options.collection.parseDate(point.date);
                point.displayDate = options.collection.dateFormat(point.date);
            }
            return point;
        }
    });

    //the collection of datapoint which will sort by date.
    Reuters.Graphics.DataPointCollection = Backbone.Collection.extend({
        initialize: function initialize(models, options) {
            var self = this;
            _.each(options, function(item, key) {
                self[key] = item;
            });
        },

        comparator: function comparator(item) {
            var self = this;
            return item.get("date");
        },

        model: Reuters.Graphics.DataPointModel,

        parse: function parse(data) {
            var self = this;
            return data;
        }
    });

    //a model for each collection of data
    Reuters.Graphics.DataSeriesModel = Backbone.Model.extend({
        defaults: {
            name: undefined,
            values: undefined,
            visible: true
        },

        initialize: function initialize(attributes, options) {
            var self = this;
            var totalChange = 0;
            var cumulate = 0;
            var name = self.get("name");
            var firstItem = self.get("values").at(0);
            var firstValue = parseFloat(firstItem.get(name));

            self.get("values").each(function(currentItemInLoop) {
                var previousItem = self.get("values").at(self.get("values").indexOf(currentItemInLoop) - 1);
                var currentValue = parseFloat(currentItemInLoop.get(name));
                var change, percent;
                if (previousItem) {
                    var previousValue = currentValue;
                    if (previousItem.get(name)) {
                        previousValue = parseFloat(previousItem.get(name).value);
                    }
                    change = currentValue - previousValue;
                    totalChange += change;
                    cumulate += currentValue;
                    percent = (currentValue / firstValue - 1) * 100;
                    currentItemInLoop.set(name, {
                        changePreMonth: change,
                        cumulate: cumulate,
                        CumulativeChange: totalChange,
                        percentChange: percent,
                        value: currentValue / options.collection.divisor
                    });
                } else {
                    currentItemInLoop.set(name, {
                        changePreMonth: 0,
                        cumulate: currentValue,
                        CumulativeChange: 0,
                        percentChange: 0,
                        value: currentValue / options.collection.divisor
                    });
                }
            });
        },

        parse: function parse(point, options) {
            return point;
        }
    });

    //a collection of those collectioins
    Reuters.Graphics.DateSeriesCollection = Backbone.Collection.extend({
        initialize: function initialize(models, options) {
            var self = this;
            _.each(options, function(item, key) {
                self[key] = item;
            });
        },

        comparator: function comparator(item) {
            var self = this;
            var name = item.get("name");
            var lastItem = item.get("values").last();
            // for time series, is going to be last value

            if (self.groupSort == "none") {
                return;
            }
            for (var index = item.get("values").length - 1; index > -1; index--) {
                if (!isNaN(parseFloat(item.get("values").at(index).get(name)[self.dataType]))) {
                    lastItem = item.get("values").at(index);
                    break;
                }
            }
            var valueForSort = lastItem.get(name)[self.dataType];

            //if categories, find greatest value for each
            if (lastItem.get("category")) {
                valueForSort = item.get("values").max(function(d) {
                    return d.get(name)[self.dataType];
                }).get(name)[self.dataType];
            }

            var plusMinus = 1;
            if (Array.isArray(self.groupSort)) {
                return self.groupSort.indexOf(name);
            }
            if (self.groupSort == "descending") {
                plusMinus = -1;
            }
            return plusMinus * valueForSort;
        },

        model: Reuters.Graphics.DataSeriesModel,

        parse: function parse(data) {
            var self = this;

            self.on('reset', function(d) {
                if (d.first().get("values").first().get("category")) {
                    self.categorySorter(d);
                }
                self.makeStacks(d);
            });
            self.on("change", function(d) {
                self.makeStacks(d);
            });

            return data;
        },

        categorySorter: function categorySorter(item) {
            var self = this;
            var name = item.last().get("name");
            var plusMinus = 1;
            if (self.categorySort == "descending") {
                plusMinus = -1;
            }
            if (self.categorySort == "none") {
                return;
            }

            item.last().get("values").models.sort(function(a, b) {
                if (a.get(name)[self.dataType] > b.get(name)[self.dataType]) {
                    return 1 * plusMinus;
                }
                if (a.get(name)[self.dataType] < b.get(name)[self.dataType]) {
                    return -1 * plusMinus;
                }
                return 0;
            });

            if (!Array.isArray(self.categorySort)) {
                self.categorySort = [];
                item.last().get("values").each(function(d) {
                    self.categorySort.push(d.get("category"));
                });
            }

            item.each(function(d) {
                d.get("values").models.sort(function(a, b) {
                    if (self.categorySort.indexOf(a.get("category")) > self.categorySort.indexOf(b.get("category"))) {
                        return 1;
                    }
                    if (self.categorySort.indexOf(a.get("category")) < self.categorySort.indexOf(b.get("category"))) {
                        return -1;
                    }
                    return 0;
                });
            });
        },

        makeStacks: function makeStacks(item) {
            var self = this;
            var filtered = self.filter(function(d) {
                return d.get("visible");
            });

            filtered.forEach(function(eachGroup, indexofKey) {
                var name = eachGroup.get("name");
                //back to here
                eachGroup.get("values").each(function(d, i) {
                    var masterPositive = 0;
                    var masterNegative = 0;
                    var stackTotal = 0;
                    var masterPercent = 0;
                    var stackMin = 0;
                    var thisValue = d.get(name)[self.dataType];
                    var counter;

                    filtered.forEach(function(collection, counter) {
                        var loopName = collection.get("name");
                        var currentValue = collection.get("values").at(i).get(loopName)[self.dataType];
                        if (currentValue >= 0) {
                            stackTotal = stackTotal + currentValue;
                        } else {
                            stackMin = stackMin + currentValue;
                        }
                    });

                    for (counter = indexofKey; counter < filtered.length; counter++) {
                        var loopName = filtered[counter].get("name");
                        var currentValue = filtered[counter].get("values").at(i).get(loopName)[self.dataType];
                        if (currentValue > 0) {
                            masterPositive = masterPositive + currentValue;
                            masterPercent = masterPercent + currentValue / stackTotal * 100;
                        } else {
                            masterNegative = masterNegative + currentValue;
                        }
                    }
                    var y0Total = masterPositive - thisValue;
                    var y1Total = masterPositive;
                    if (thisValue < 0) {
                        y0Total = masterNegative - thisValue;
                        y1Total = masterNegative;
                    }
                    d.set(name, _.extend(d.get(name), {
                        y0Total: y0Total,
                        y1Total: y1Total,
                        stackTotal: stackTotal,
                        stackMin: stackMin,
                        y0Percent: masterPercent - thisValue / stackTotal * 100,
                        y1Percent: masterPercent
                    }));
                });
            });
        }

    });
    //# sourceMappingURL=dataModels.js.map
})();


//for translations.
window.gettext = function(text) {
    return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function(hashBased) {
    var query = void 0;
    if (hashBased) {
        var pos = location.href.indexOf('?');
        if (pos == -1) return [];
        query = location.href.substr(pos + 1);
    } else {
        query = location.search.substr(1);
    }
    var result = {};
    query.split('&').forEach(function(part) {
        if (!part) return;
        part = part.split('+').join(' '); // replace every + with space, regexp-free version
        var eq = part.indexOf('=');
        var key = eq > -1 ? part.substr(0, eq) : part;
        var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

        //convert true / false to booleans.
        if (val == 'false') {
            val = false;
        } else if (val == 'true') {
            val = true;
        }

        var f = key.indexOf('[');
        if (f == -1) {
            result[decodeURIComponent(key)] = val;
        } else {
            var to = key.indexOf(']');
            var index = decodeURIComponent(key.substring(f + 1, to));
            key = decodeURIComponent(key.substring(0, f));
            if (!result[key]) {
                result[key] = [];
            }
            if (!index) {
                result[key].push(val);
            } else {
                result[key][index] = val;
            }
        }
    });
    return result;
};

Reuters.trackEvent = function(category, type, id) {
    category = category || 'Page click';
    //console.log(category, type, id);
    var typeString = type;
    if (id) {
        typeString += ': ' + id;
    }
    var gaOpts = {
        'nonInteraction': false,
        'page': PAGE_TO_TRACK
    };

    ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function() {
    $('[data-slider]').each(function() {
        var $el = $(this);
        var getPropArray = function getPropArray(value) {
            if (!value) {
                return [0];
            }
            var out = [];
            var values = value.split(',');
            values.forEach(function(value) {
                out.push(parseFloat(value));
            });
            return out;
        };
        var pips = undefined;
        var start = getPropArray($el.attr('data-start'));
        var min = getPropArray($el.attr('data-min'));
        var max = getPropArray($el.attr('data-max'));
        var orientation = $el.attr('data-orientation') || 'horizontal';
        var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
        var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
        var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
        var snap = $el.attr('data-snap') === 'true' ? true : false;
        var pipMode = $el.attr('data-pip-mode');
        var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
        var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
        var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
        if (pipMode === 'count') {
            pipValues = pipValues[0];
        }

        if (pipMode) {
            pips = {
                mode: pipMode,
                values: pipValues,
                stepped: pipStepped,
                density: pipDensity
            };
        }

        if (connect) {
            (function() {
                var cs = [];
                connect.split(',').forEach(function(c) {
                    c = c === 'true' ? true : false;
                    cs.push(c);
                });
                connect = cs;
            })();
        }

        noUiSlider.create(this, {
            start: start,
            range: {
                min: min,
                max: max
            },
            snap: snap,
            orientation: orientation,
            step: step,
            tooltips: tooltips,
            connect: connect,
            pips: pips
        });
        //This probably doesn't belong here, but will fix the most common use-case.
        $(this).find('div.noUi-marker-large:last').addClass('last');
        $(this).find('div.noUi-marker-large:first').addClass('first');
    });
};

Reuters.hasPym = false;
try {
    Reuters.pymChild = new pym.Child({
        polling: 500
    });
    if (Reuters.pymChild.id) {
        Reuters.hasPym = true;
        $("body").addClass("pym");
    }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
    $("html").addClass("media");
}
if (Reuters.Graphics.Parameters.eikon) {
    $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
    $("html").addClass("remove-header");
}
if (Reuters.Graphics.Parameters.image == "no") {
    $("html").addClass("remove-img");
}
if (Reuters.Graphics.Parameters.onlycandidates == "yes") {
    $("html").addClass("only-candidate");
}
if (Reuters.Graphics.Parameters.onlypolls == "yes") {
    $("html").addClass("only-polls");
}
if (Reuters.Graphics.Parameters.headline == "no") {
    $("html").addClass("remove-headline");
}
//# sourceMappingURL=utils.js.map

Reuters.Graphics.FeaturePage = function(_Backbone$View) {
    babelHelpers.inherits(FeaturePage, _Backbone$View);

    function FeaturePage() {
        babelHelpers.classCallCheck(this, FeaturePage);
        return babelHelpers.possibleConstructorReturn(this, (FeaturePage.__proto__ || Object.getPrototypeOf(FeaturePage)).apply(this, arguments));
    }

    babelHelpers.createClass(FeaturePage, [{
        key: 'preinitialize',
        value: function preinitialize() {
            this.events = {
                'change .nav-options .btn': 'onSectionChange'
            };
            this.router = new Reuters.Graphics.FeaturePageRouter();
        }
    }, {
        key: 'initialize',
        value: function initialize(options) {

            var self = this;

            this.$el.html(Reuters.Graphics.Template.featureLayout());

            d3.queue().defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/candidates.json").defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/pollone.json").defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/polltwo.json").await(render);

            /*
   			.defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-test-fr-polls-first")
   			.defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-test-fr-polls-second")
   */

            function render(error, candidates, firstPolls, secondPolls) {
                // console.log(candidates, firstPolls, secondPolls);
                self.render(error, candidates, firstPolls, secondPolls);
            }

            this.listenTo(this.router, 'route:section', this.changeSection);
        }
    }, {
        key: 'render',
        value: function render(error, candidates, firstPolls, secondPolls) {
            this.parseDate = d3.time.format("%d/%m/%Y").parse;
            this.formatDate = d3.time.format("%b %e");

            Backbone.history.start();

            this.candidatesArray = ["melenchon", "hamon", "macron", "fillon", "lepen"];
            this.candidateLookup = {
                melenchon: "Jean-Luc Melenchon",
                hamon: "Benoit Hamon",
                macron: "Emmanuel Macron",
                fillon: "Francois Fillon",
                lepen: "Marine Le Pen"
            };

            this.candidates(candidates);
            this.firstPolls(firstPolls, secondPolls);

            return this;
        }
    }, {
        key: 'candidates',
        value: function candidates(data) {
            var election = {
                issues: ["economy", "securityandimmigration", "europeandworld", "societyandgovernance"],
                candidates: data,
                issuelookup: {
                    economy: "Economy",
                    securityandimmigration: "Security and immigration",
                    europeandworld: "Europe and the world",
                    societyandgovernance: "Society and governance"
                }
            };

            $("#candidate-sort").html(Reuters.Graphics.Template.candidateContent({
                election: election
            }));
            $("#issue-sort").html(Reuters.Graphics.Template.issueContent({
                election: election
            }));

            $(".navContainer.candidate-buttons .btn").on("click", function(evt) {
                var thisID = $(this).attr("dataid");
                $(".candidate-table").removeClass("selected");
                $("#" + thisID).addClass("selected");
            });
        }
    }, {
        key: 'firstPolls',
        value: function firstPolls(_firstPolls, secondPolls) {
            var self = this;

            var groupedFirstPolls = _.groupBy(_firstPolls, "pollster");

            var firstPollsData = {};
            var largestFirstPollValue = 0;
            var firstPollsHistorical = {};

            _.each(groupedFirstPolls, function(array, key) {

                if (key == "Opinionway" || key == "Ifop-Fiducial") {
                    firstPollsHistorical[key] = array;
                }

                var lastItem = array[array.length - 1];

                firstPollsData[key] = self.candidatesArray.map(function(candidate) {
                    largestFirstPollValue = parseFloat(lastItem[candidate]) > largestFirstPollValue ? parseFloat(lastItem[candidate]) : largestFirstPollValue;
                    return {
                        category: candidate,
                        value: lastItem[candidate],
                        pollster: lastItem.pollster,
                        marginoferror: lastItem.marginoferror,
                        samplesize: lastItem.samplesize,
                        polldate: lastItem.date,
                        daterange: lastItem.daterange
                    };
                });
            });

            largestFirstPollValue = Math.ceil(largestFirstPollValue / 10) * 10;

            var pollsters = _.keys(firstPollsData).sort(function(a, b) {
                var aValue = self.parseDate(firstPollsData[a][0].polldate);
                var bValue = self.parseDate(firstPollsData[b][0].polldate);
                if (aValue > bValue) {
                    return -1;
                }
                if (aValue < bValue) {
                    return 1;
                }
                return 0;
            });

            $("#polls").html(Reuters.Graphics.Template.pollsfirst({
                pollsters: pollsters,
                firstPollsData: firstPollsData,
                self: this
            }));

            pollsters.forEach(function(key, i) {
                var value = firstPollsData[key];

                var id = key.split(' ')[0].split('/')[0];

                value.forEach(function(d) {
                    d.category = self.candidateLookup[d.category];
                });

                var sortOrder = self.candidatesArray.map(function(d) {
                    return self.candidateLookup[d];
                });

                Reuters.Graphics[id] = new Reuters.Graphics.BarChart({
                    el: "#reutersGraphic-latest-poll-" + id,
                    dataURL: value,
                    height: 100, //if < 10 - ratio , if over 10 - hard height.  undefined - square
                    columnNames: {
                        value: "Value"
                    }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
                    color: [red5, red3, gray2, blue3, blue5],
                    orient: "right",
                    YTickLabel: [
                        [gettext(""), "%"]
                    ], //  \u00A0  - use that code for a space.
                    hasLegend: false,
                    horizontal: true,
                    yScaleMax: function yScaleMax() {
                        return largestFirstPollValue;
                    },
                    categorySort: sortOrder,
                    barFill: function barFill(d) {
                        var self = this;
                        return self.color[self.categorySort.indexOf(d.category)];
                    },
                    parseDate: d3.time.format("%d/%m/%Y").parse
                });

                Reuters.Graphics[id].on("renderChart:end", function(evt) {
                    self.addMoe(this);
                });

                Reuters.Graphics[id].on("update:start", function(evt) {
                    self.updateMoe(this);
                });
                if (i < pollsters.length - 1) {
                    $("#reutersGraphic-latest-poll-" + id).addClass("hidden-x-axis");
                }
            });

            _.each(firstPollsHistorical, function(array, key) {
                var id = key.split("-")[0];

                Reuters.Graphics["historical" + id] = new Reuters.Graphics.LineChart({
                    el: "#reutersGraphic-first-" + id,
                    dataURL: array,
                    height: 220, //if < 10 - ratio , if over 10 - hard height.  undefined - square
                    columnNames: self.candidateLookup,
                    colors: {
                        melenchon: red5,
                        hamon: red3,
                        macron: gray2,
                        fillon: blue3,
                        lepen: blue5
                    },
                    yScaleTicks: 3,
                    dateFormat: d3.time.format("%b %d"),
                    YTickLabel: [
                        [gettext(""), "%"]
                    ], //  \u00A0  - use that code for a space.
                    hasLegend: false,
                    yScaleMax: function yScaleMax() {
                        return largestFirstPollValue;
                    },
                    chartLayout: "fillLines",
                    xTickFormat: function xTickFormat(d) {
                        var formatDate = d3.time.format("%b %e");
                        return formatDate(d);
                    },
                    parseDate: d3.time.format("%d/%m/%Y").parse
                });

                Reuters.Graphics["historical" + id].on("renderChart:end", function(evt) {
                    var self = this;

                    self.area.y0(function(d) {
                        var moe = parseFloat(d.marginoferror) / 2;
                        return self.scales.y(d[self.dataType] - moe);
                    }).y1(function(d) {
                        var moe = parseFloat(d.marginoferror) / 2;
                        return self.scales.y(d[self.dataType] + moe);
                    });
                });

                Reuters.Graphics["historical" + id].on("update:start", function(evt) {});
            });

            this.secondPolls(secondPolls);
        }
    }, {
        key: 'secondPolls',
        value: function secondPolls(_secondPolls) {
            var self = this;

            _secondPolls.forEach(function(d) {
                d.parsedDate = self.parseDate(d.date);
            });
            var groupedMatchup = _.groupBy(_secondPolls, "matchup");
            var matchupData = {};
            _.each(groupedMatchup, function(values, key) {
                matchupData[key] = [];
                var groupedPolls = _.groupBy(values, "pollster");
                _.each(groupedPolls, function(array, pollster) {
                    array.sort(function(a, b) {
                        if (a.parsedDate > b.parsedDate) {
                            return 1;
                        }
                        if (a.parsedDate < b.parsedDate) {
                            return -1;
                        }
                        return 0;
                    });
                    var lastItem = array[array.length - 1];
                    var obj = {
                        polldate: lastItem.date,
                        category: lastItem.daterange,
                        samplesize: lastItem.samplesize,
                        marginoferror: lastItem.marginoferror,
                        pollster: lastItem.pollster
                    };
                    self.candidatesArray.forEach(function(d) {
                        if (lastItem[d]) {
                            obj[d] = lastItem[d];
                        }
                    });
                    matchupData[key].push(obj);
                });
            });

            var matchups = _.keys(matchupData).map(function(d) {
                return {
                    matchup: d,
                    first: d.split(" / ")[0],
                    second: d.split(" / ")[1],
                    firstFull: self.candidateLookup[d.split(" / ")[0]],
                    secondFull: self.candidateLookup[d.split(" / ")[1]]
                };
            });

            $("#second-round").html(Reuters.Graphics.Template.pollssecond({
                matchups: matchups,
                data: matchupData,
                self: this
            }));

            matchups.forEach(function(d) {
                var columnName = {};
                var colors = {};
                var colorarray = [red5, red3, gray2, blue3, blue5];
                // console.log(d.first, d.second, matchupData);
                var keys = _.keys(matchupData[d.first + " / " + d.second][0]).filter(function(d) {
                    return d != "category" && d != "pollster" && d != "marginoferror" && d != "polldate" && d != "samplesize";
                });

                keys.forEach(function(d) {
                    columnName[d] = self.candidateLookup[d];
                    colors[d] = colorarray[self.candidatesArray.indexOf(d)];
                });

                matchupData[d.first + " / " + d.second].sort(function(a, b) {
                    var aDate = self.parseDate(a.polldate);
                    var bDate = self.parseDate(b.polldate);
                    if (aDate < bDate) {
                        return 1;
                    }
                    if (aDate > bDate) {
                        return -1;
                    }
                    return 0;
                });

                Reuters.Graphics[d.first + d.second] = new Reuters.Graphics.BarChart({
                    el: "#reutersGraphic-first-" + d.first + d.second,
                    dataURL: matchupData[d.first + " / " + d.second],
                    height: 100, //if < 10 - ratio , if over 10 - hard height.  undefined - square
                    columnNames: columnName, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
                    colors: colors,
                    YTickLabel: [
                        [gettext(""), "%"]
                    ], //  \u00A0  - use that code for a space.
                    hasLegend: false,
                    horizontal: true,
                    yScaleVals: [0, 50, 100],
                    chartLayout: "stackTotal",
                    groupSort: [d.second, d.first],
                    categorySort: "none",
                    tipTemplate: Reuters.Graphics.Template.pollSecondTooltip,
                    parseDate: d3.time.format("%d/%m/%Y").parse
                });
            });

            $(".navContainer.poll-buttons .btn").on("click", function(evt) {

                var thisID = $(this).attr("dataid");

                // console.log($(this).siblings());
                $(this).siblings().removeClass("active");
                $(this).addClass("active");
                $(".poll-section").removeClass("selected");
                $("#" + thisID).addClass("selected");
            });
        }
    }, {
        key: 'addMoe',
        value: function addMoe(self) {

            self.t = textures.lines().size(5).orientation("2/8").stroke("#C3C4C6");

            self.svg.call(self.t);

            self.addMoe = self.barChart.selectAll(".moebar").data(function(d) {
                return d.values;
            }).enter().append("rect").attr("class", ".moebar").style("fill", function(d) {
                return self.t.url();
            }).attr("height", function(d, i, j) {
                return self.barWidth(d, i, j);
            }).attr("y", function(d, i, j) {
                return self.xBarPosition(d, i, j);
            }).attr("x", function(d) {
                return self.scales.y(d.value) - self.scales.y(d.marginoferror) / 2;
            }).attr("width", function(d) {
                return self.scales.y(d.marginoferror);
            });
        }
    }, {
        key: 'updateMoe',
        value: function updateMoe(self) {

            self.addMoe.transition().duration(1000).attr("height", function(d, i, j) {
                return self.barWidth(d, i, j);
            }).attr("y", function(d, i, j) {
                return self.xBarPosition(d, i, j);
            }).attr("x", function(d) {
                return self.scales.y(d.value) - self.scales.y(d.marginoferror) / 2;
            }).attr("width", function(d) {
                return self.scales.y(d.marginoferror);
            });
        }
    }, {
        key: 'onSectionChange',
        value: function onSectionChange(event) {
            var $el = $(event.currentTarget);
            var id = $el.attr('data-id');
            this.changeSection(id, $el);
        }
    }, {
        key: 'changeSection',
        value: function changeSection(id, $button) {
            // console.log('changing section fired', id, $button);
            var $el = this.$('#' + id);
            if ($el.hasClass('selected')) {
                return;
            }
            if (!$button) {
                $button = this.$('.nav-options .btn[data-id="' + id + '"]');
                $button.addClass('active').siblings().removeClass('active');
                $button.find('input').addClass('active').attr('checked', 'checked');
            }
            $el.addClass('selected').siblings().removeClass('selected');
            this.router.navigate('section/' + id, {
                trigger: false
            });
            // 		_.invoke(this.charts, 'update');
        }
    }]);
    return FeaturePage;
}(Backbone.View);

Reuters.Graphics.FeaturePageRouter = function(_Backbone$Router) {
    babelHelpers.inherits(FeaturePageRouter, _Backbone$Router);

    function FeaturePageRouter() {
        babelHelpers.classCallCheck(this, FeaturePageRouter);
        return babelHelpers.possibleConstructorReturn(this, (FeaturePageRouter.__proto__ || Object.getPrototypeOf(FeaturePageRouter)).apply(this, arguments));
    }

    babelHelpers.createClass(FeaturePageRouter, [{
        key: 'preinitialize',
        value: function preinitialize() {
            this.routes = {
                'section/:id': 'section'
            };
        }
    }]);
    return FeaturePageRouter;
}(Backbone.Router);

$(document).ready(function() {
    if (Reuters.Graphics.Parameters.eikon) {
        black = white;
    }

    window.featurePageExample = new Reuters.Graphics.FeaturePage({
        el: '.main'
    });
});
//# sourceMappingURL=main.js.map
