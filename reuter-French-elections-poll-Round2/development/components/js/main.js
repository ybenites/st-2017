(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["candidateContent"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '<section class="visible-md-up hidden-sm-down">\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left"></p>\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              \n              <div class="col-md-5">\n                  <img src="images/candidates/' + ((__t = d.imageid) == null ? '' : __t) + '.jpg" class="mt-2 float-left" width="50%"> \n              </div>\n        ';
    });
    __p += '\n    </div>\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left"></p>\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              \n        <div class="col-md-5">\n                  <h3 class="mt-1 text-sm-left">' + ((__t = d.candidate) == null ? '' : __t) + '<span class="font-weight-normal">, ' + ((__t = d.age) == null ? '' : __t) + '</span></h3>  \n              </div>\n        ';
    });
    __p += '\n    </div>\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left"></p>\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <p class="candidate-content-party text-uppercase text-sm-left">' + ((__t = d.party) == null ? '' : __t) + '</p>\n\n              </div>\n        ';
    });
    __p += '        \n    </div>\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left"></p>\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <p class="bottomline-text mb-2 font-italic text-sm-left">' + ((__t = d.bottomline) == null ? '' : __t) + '</p>\n\n              </div>\n        ';
    });
    __p += '        \n    </div>\n    <hr class="mb-2">\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left text-uppercase mb-0 font-weight-bold">First-round vote</p>\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <h5 class="font-weight-bold mb-2 text-sm-left">' + ((__t = d.firstroundvote) == null ? '' : __t) + '%</h5>\n              </div>\n        ';
    });
    __p += '        \n    </div>\n    <hr class="mb-2">\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left text-uppercase mb-0 font-weight-bold">Economy</p>\n                  <img src="images/icons/economy-icon.png" class="mb-0 mt-2 mx-auto d-block" width="100%">\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <p class="bottomline-text mb-2 text-sm-left">' + ((__t = d.economy) == null ? '' : __t) + '</p>\n              </div>\n        ';
    });
    __p += '        \n    </div>\n    <hr class="mb-2">\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left text-uppercase mb-0 font-weight-bold">Security and immigration</p>\n                  <img src="images/icons/securityandimmigration-icon.png" class="mb-0 mx-auto d-block" width="100%">\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <p class="bottomline-text mb-2 text-sm-left">' + ((__t = d.securityandimmigration) == null ? '' : __t) + '</p>\n\n              </div>\n        ';
    });
    __p += '        \n    </div>\n    <hr class="mb-2">\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left text-uppercase mb-0 font-weight-bold">Europe and the world</p>\n                  <img src="images/icons/europeandworld-icon.png" class="mb-2 mt-2 mx-auto d-block" width="100%">\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <p class="bottomline-text mb-2 text-sm-left">' + ((__t = d.europeandworld) == null ? '' : __t) + '</p>\n\n              </div>\n        ';
    });
    __p += '        \n    </div>\n    <hr class="mb-2">\n    <div class="row">\n              <div class="col-md-2">\n                  <p class="text-sm-left text-uppercase mb-0 font-weight-bold">Society and governance</p>\n                  <img src="images/icons/societyandgovernance-icon.png" class="mb-0 mt-2 mx-auto d-block" width="100%">\n\n              </div>\n        ';
    t.election.candidates.forEach(function(d) {;
      __p += '\n              <div class="col-md-5">\n                  <p class="bottomline-text mb-2 text-sm-left">' + ((__t = d.societyandgovernance) == null ? '' : __t) + '</p>\n\n              </div>\n        ';
    });
    __p += '        \n    </div>\n</section>\n\n<section class="hidden-md-up visible-sm-down">\n';
    t.election.candidates.forEach(function(d, i) {;
      __p += '\n   <div class="row">\n        <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n            <img src="images/candidates/' + ((__t = d.imageid) == null ? '' : __t) + '.jpg" class="candidate-image mt-2 mx-auto d-block" width="100%">\n\n        </div>\n    </div>\n\n    <div class="row">\n        <div class="col-md-6 offset-md-3 ">\n            <h3 class="mt-1 text-xs-center">' + ((__t = d.candidate) == null ? '' : __t) + ' <span class="font-weight-normal">' + ((__t = d.age) == null ? '' : __t) + '</span></h3>  \n            <h4 class="font-weight-normal candidate-content-age text-xs-center"></h4>               \n            <p class="candidate-content-party text-uppercase text-xs-center">' + ((__t = d.party) == null ? '' : __t) + '</p>               \n            <p class="bottomline-text mb-2 font-italic text-sm-center">' + ((__t = d.bottomline) == null ? '' : __t) + '</p>               \n       </div>\n    </div>\n    \n    <div class="row">\n        ';
      t.election.issues.forEach(function(issue) {;
        __p += '\n            <div class="col-md-3 ">\n                <p class="text-uppercase mb-0 font-weight-bold">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</p>\n                <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>\n            </div>  \n        ';
      });
      __p += '        \n    </div>\n\n        ';
      if (i != t.election.candidates.length - 1) {;
        __p += '\n            <hr>\n         ';
      };
      __p += ' \n\n';
    });
    __p += '\n</section>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["featureLayout"] = function(t) {
    var __t,
      __p = '';
    __p += '<!-- Header -->\n<div class="feature-header mt-0 container" id="package-container">\n        <div id="headerbanner" class="row header-img-holder">\n            <img class="img-fluid feature-header-img mb-1 hidden-sm-down" src="images/banners/French-Header-Banner-Desktop.png">\n            <img class="img-fluid feature-header-img mb-1 hidden-md-up" src="images/banners/French-Header-Banner-Mobile.jpg"">\n   <div id="bannerTextMobile">FRENCH PRESIDENTIAL ELECTION 2017</div>     </div>\n\n        <div class="row header-text-holder">\n           <h1 class="display-1 div-col text-xs-center header-text" style="margin-bottom:0;">' + ((__t = 'Latest polls') == null ? '' : __t) + '</h1>\n           <h2 class="mobile-section-headers text-xs-center mb-2" style="display:none"><a class="first-round-link font-weight-light" href="http://fingfx.thomsonreuters.com/gfx/rngs/FRANCE-ELECTION/010031D933E/index.html">&nbsp; &nbsp; &nbsp;&nbsp;First round&nbsp;</a><span class="second-round-link">' + ((__t = '|&nbsp;Second round') == null ? '' : __t) + '</span></h2>\n    \n           <p class="mobile-section-headers col-md-8 offset-md-2 text-xs-center mb-3" style="display:none">' + ((__t = "On May 7, France elects a president for the next five years&#8212;either Emmanuel Macron, a 39-year-old centrist or Marine Le Pen, 48-year-old leader of the anti-immigrant National Front. Some 47 million people can vote for Macron, an internationalist liberal who is pro-EU, and Le Pen, a protectionist, nativist who is anti-EU. <br>Just 977,833 votes separated first-placed Macron from second-placed Le Pen when the two and nine others competed in the opening ballot on April 23.") == null ? '' : __t) + '</p>\n        </div>\n\n<!--         <div class="row">\n\n            <p class="graphic-timestamp div-col text-xs-center text-md-center col-md-6 offset-md-3">' + ((__t = 'Updated February 22, 2017') == null ? '' : __t) + '</p>\n        </div> -->\n\n        <div class="col-sm-12 text-sm-center masthead-nav text-uppercase" style="display:none">\n            <div class="btn-group nav-options horizontal" data-toggle="buttons" id="section-buttons">\n                <label class="btn btn-link" data-id="candidates">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off" checked> \n                    ' + ((__t = 'Candidates') == null ? '' : __t) + '\n                </label>\n                <label class="btn btn-link active" data-id="polling">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off">\n                    ' + ((__t = 'Polls') == null ? '' : __t) + '\n                </label>\n                <label class="btn btn-link" data-id="predictions">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off">\n                    ' + ((__t = 'Predictions') == null ? '' : __t) + '\n                </label>\n                <label class="btn btn-link" data-id="results">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off">\n                    ' + ((__t = 'Results') == null ? '' : __t) + '\n                </label>\n            </div>\n        </div>        \n</div>\n\n<div class="container graphic-section-container">\n\n    <!-- Polls -->\n    <section class="graphic-section polls-container selected" id="polling">\n        <div id="deck" class="row">\n            <h2 class="mobile-section-headers text-xs-center hidden-sm-up col-md-6 offset-md-3 text-uppercase">' + ((__t = '') == null ? '' : __t) + '</h2>\n            <p id="decktText" class="graphic-subhead text-xs-center offset-md-3">' + ((__t = 'Opinion pollsters have been measuring voting scenarios for the May 7 playoff between Emmanuel Macron and Marine Le Pen. Here are the trends they show:') == null ? '' : __t) + '</p>\n   <div class="time">      <p class="div-col text-xs-center text-md-center">PUBLISHED: MAY 5, 2017</p></div>     </div>\n        <div id="polls" class="polls"></div>\n    </section>\n\n\n	<!-- Candidates -->\n	<section class="graphic-section candidate-container hideSection" id="candidates">\n	\n	    <div class="row">\n            <h2 class="mobile-section-headers text-xs-center hidden-sm-up col-md-6 offset-md-3 text-uppercase">' + ((__t = 'Candidates') == null ? '' : __t) + '</h2>\n	           <p class="graphic-subhead text-xs-center offset-md-3">' + ((__t = 'The candidates and their programmes:') == null ? '' : __t) + '</p>\n	     </div>\n\n	    <div id="candidate-sort" class="candidate-table selected"></div>\n<!-- 	    <div id="issue-sort" class="candidate-table"></div> -->\n	</section>\n\n    <section class="graphic-section predictions-container hideSection" id="predictions">\n    \n        <div class="row subheads">\n            <h2 class="mobile-section-headers text-xs-center hidden-sm-up col-md-6 offset-md-3 text-uppercase">' + ((__t = 'Predictions') == null ? '' : __t) + '</h2>\n               <p class="graphic-subhead text-xs-center col-md-6 offset-md-3">' + ((__t = "Hypermind, with eight out of ten forecasters on this election from France, gives Le Pen a much lower chance of winning than U.S. prediction market PredictIt.") == null ? '' : __t) + '</p>\n        </div>\n   \n        <div id="predictions-charts" class="prediction-chart"></div>\n\n    </section>\n\n<!--     results -->\n    <section class="graphic-section results-container hideSection" id="results">\n    \n        <div class="row">\n            <h2 class="mobile-section-headers text-xs-center hidden-sm-up col-md-6 offset-md-3 text-uppercase">' + ((__t = 'Results') == null ? '' : __t) + '</h2>\n               <p class="graphic-subhead text-xs-center">' + ((__t = "French voters cast their ballots on April 23 in the opening round of a presidential election that is considered the most unpredictable in memory.") == null ? '' : __t) + '</p>\n\n       </div>\n   \n        <div id="results-charts" class="result-chart"></div>\n\n\n</div>\n\n\n<!-- footer -->\n<div class="container graphic-section-container">\n      </div>    \n    ';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["firstRoundPollsLegendTemplate"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '<div class=\'legendContainer\'>\n	<div class="legend-ital">' + ((__t = 'Hide/show') == null ? '' : __t) + '</div>\n	\n	<div class="legend-items-holder">\n		';
    t.data.forEach(function(d) {;
      __p += '\n			<div class="legendItems">\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n				<div class="legendInline">\n					<div class="nameTip">	' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n				</div>\n			</div>\n		';
    });
    __p += '\n	</div>\n\n</div>\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["issueContent"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    t.election.issues.forEach(function(issue, i) {;
      __p += '\n    <div class="row">\n              <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n                    <img src="images/icons/' + ((__t = issue) == null ? '' : __t) + '-icon.png" class="candidate-image mx-auto d-block" width="100%">\n              </div>\n    </div>\n\n    <div class="row">\n              <div class="col-sm-6 offset-sm-3">\n                    <h3 class="text-center mb-1 mt-1">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</h3>\n              </div>\n                      \n    </div>\n\n  ';
      t.election.candidates.forEach(function(d) {;
        __p += '\n      <div class="row">\n\n                <div class="col-lg-1">            \n                </div> \n\n                <div class="col-md-4 col-lg-2 text-xs-center text-md-left issue-content-header">\n                    <p><span class="text-uppercase font-weight-bold">' + ((__t = d.candidate) == null ? '' : __t) + '</span><br>\n                    <span class="">' + ((__t = d.party) == null ? '' : __t) + '</span></p>\n                </div>\n\n\n                <div class="col-md-8  text-xs-left">\n                    <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>               \n                </div>\n\n                <div class="col-lg-1">            \n                </div> \n\n      </div>\n\n   ';
      });
      __p += '\n         \n        ';
      if (i != t.election.issues.length - 1) {;
        __p += '\n            <hr>\n         ';
      };
      __p += ' \n\n';
    });
    __p += '        \n\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["pollSecondTooltip"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '\n<!-- <div class=\'dateTip text-uppercase\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div> -->\n';
    t.data.forEach(function(d, i) {;
      __p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
      if (t.self.chartLayout == "stackPercent") {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
        __p += '				\n				';
      } else {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d[t.self.dataType]));
        __p += '				\n				';
      };
      __p += '\n			</div>\n	\n		</div>\n';
    });
    __p += '	\n';
    if (t.self.timelineData) {
      var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
      print(t.self.timelineTemplate({
        data: timelineData,
        self: t.self
      }));
    };
    __p += '	\n\n	';

    var samplesizeFormat = d3.format(",.0f");
    var marginFormat = d3.format(",.1f");

    ;
    __p += '\n\n	<div class=\'font-italic tooltip-methodology mt-1\'>' + ((__t = t.data[0].pollster) == null ? '' : __t) + '' + ((__t = ' survey of ') == null ? '' : __t) + '' + ((__t = samplesizeFormat(t.data[0].samplesize)) == null ? '' : __t) + '' + ((__t = ' respondents conducted ') == null ? '' : __t) + '' + ((__t = t.data[0].category) == null ? '' : __t) + '' + ((__t = '. Margin of error: +/- ') == null ? '' : __t) + '' + ((__t = marginFormat(t.data[0].marginoferror)) == null ? '' : __t) + '' + ((__t = ' pct. pts.') == null ? '' : __t) + '</div>\n\n	\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["pollTooltip"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '	';
    if (t.data[0].displayDate) {;
      __p += '\n<div class=\'dateTip text-uppercase\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
    } else {;
      __p += '\n<div class=\'dateTip text-uppercase\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
    };
    __p += '\n';
    t.data.forEach(function(d, i) {;
      __p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
      if (t.self.chartLayout == "stackPercent") {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
        __p += '				\n				';
      } else {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d[t.self.dataType]));
        __p += '				\n				';
      };
      __p += '\n			</div>\n	\n		</div>\n';
    });
    __p += '	\n';
    if (t.self.timelineData) {
      var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
      print(t.self.timelineTemplate({
        data: timelineData,
        self: t.self
      }));
    };
    __p += '	\n\n';
    if (t.data[0].pollster) {;
      __p += '\n	<div class=\'font-italic tooltip-methodology\'>' + ((__t = t.data[0].pollster) == null ? '' : __t) + ' surevey of ' + ((__t = t.data[0].samplesize) == null ? '' : __t) + ' respondents. Margin of error: ' + ((__t = t.data[0].marginoferror) == null ? '' : __t) + ' percentage points</div>\n';
    };

    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["pollsfirst"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '\n<div class="poll-section selected" id="first-round">\n	<p class="graphic-chart-label text-xs-center mt-1">' + ((__t = 'Historical Polls') == null ? '' : __t) + '</p>\n	<p class="bottomline-text text-xs-center">' + ((__t = 'Daily voting intentions for the second round of voting on May 7 with margins of error shown') == null ? '' : __t) + '</p>\n	\n	<div class="row">\n		<div class="col-sm-6">\n	        <p class="pollster"><strong>' + ((__t = 'OpinionWay') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="reutersGraphic-first-Opinionway" class="reuters-chart"></div>		\n		</div>\n	\n		<div class="col-sm-6">\n	        <p class="pollster"><strong>' + ((__t = 'Ifop-Fiducial') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="reutersGraphic-first-Ifop" class="reuters-chart"></div>\n		</div>\n	</div>\n	\n	\n	\n	<div class="row hidden-xs-down">\n		';
    var canarray = ["macron", "lepen"];
    var canlook = {

      macron: "Emmanuel Macron",

      lepen: "Marine Le Pen"
    };
    canarray.forEach(function(d, i) {;
      __p += '\n			<div class="col-sm-2 ';
      if (i == 0) {;
        __p += 'offset-sm-4';
      };
      __p += '">\n				<img class="img-fluid" src="images/candidates/' + ((__t = d) == null ? '' : __t) + '.jpg">\n				<div class="candidate-name text-sm-center">\n					' + ((__t = canlook[d]) == null ? '' : __t) + '\n				</div>\n						\n	\n			</div>	\n		';
    });
    __p += '\n	</div>\n	\n	<div class="row mt-1 mb-1">\n		<div class="col-sm-12">\n			<p class="graphic-chart-label mt-2 text-xs-center">' + ((__t = 'Latest Polls') == null ? '' : __t) + '</p>\n			<p class="bottomline-text text-xs-center">' + ((__t = 'Voting intentions for the second round of voting on May 7 with margins of error shown') == null ? '' : __t) + '</p>\n		</div>\n	</div>\n	\n	';
    t.pollsters.forEach(function(key) {
      var value = t.firstPollsData[key];
      var samplesizeFormat = d3.format(",.0f");
      var marginFormat = d3.format(",.1f");;
      __p += '\n		<div class="row">\n			<div class="col-sm-2 col-md-2 poll-text">\n				<p class="pollster">' + ((__t = key) == null ? '' : __t) + '</p>\n				<p class="moe">' + ((__t = 'Survey of ') == null ? '' : __t) + '' + ((__t = t.self.samplesizeFormat(value[0].samplesize)) == null ? '' : __t) + ' ' + ((__t = 'respondents conducted ') == null ? '' : __t) + '' + ((__t = value[0].daterange) == null ? '' : __t) + '' + ((__t = '; Margin of error: +/- ') == null ? '' : __t) + '' + ((__t = t.self.marginFormat(value[0].marginoferror)) == null ? '' : __t) + '' + ((__t = ' pct. pts.') == null ? '' : __t) + '</p>\n			</div>\n			<div class="col-sm-10 col-md-10">\n		        <div id="reutersGraphic-latest-poll-' + ((__t = key.split(' ')[0].split('/')[0]) == null ? '' : __t) + '" class=""></div>\n			</div>\n	\n			\n		</div>\n	';
    });
    __p += '\n</div>\n<div class="poll-section" id="second-round">	\n\n\n\n</div>	';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["pollssecond"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '<div class="row ">\n	<div class="col-sm-12">\n		<p class="graphic-chart-label mt-2 text-xs-center mt-1">' + ((__t = 'Latest Numbers') == null ? '' : __t) + '</p>\n		<p class="bottomline-text font-italic text-xs-center">' + ((__t = 'Voting intentions for second-round two-candidate scenarios on May 7') == null ? '' : __t) + '</p>\n	</div>\n</div>\n	\n';
    t.matchups.forEach(function(d, i) {;
      __p += '\n	<div class="row ';
      if (i == t.matchups.length - 1) {;
        __p += 'mb-3';
      };
      __p += '">\n		<div class="col-xs-6 col-sm-6 col-md-2">\n			<img class="img-fluid " src="images/candidates/' + ((__t = d.first) == null ? '' : __t) + '.jpg">\n			<p class="candidate-name text-xs-center">' + ((__t = d.firstFull) == null ? '' : __t) + ' </p>			\n		</div>\n	     \n		<div class="col-xs-6 hidden-md-up">\n		<img class="img-fluid " src="images/candidates/' + ((__t = d.second) == null ? '' : __t) + '.jpg">\n			<p class="candidate-name text-md-center text-xs-center">' + ((__t = d.secondFull) == null ? '' : __t) + ' </p>			\n		</div>\n\n	     <div class="col-sm-12 col-md-8">\n		     <div id="reutersGraphic-first-' + ((__t = d.first + d.second) == null ? '' : __t) + '" class="reuters-chart"></div>				     \n	     </div>\n\n		<div class="col-xs-6 col-sm-6 col-md-2 hidden-md-down ">\n			<img class="img-fluid " src="images/candidates/' + ((__t = d.second) == null ? '' : __t) + '.jpg">\n			<p class="candidate-name text-xs-center">' + ((__t = d.secondFull) == null ? '' : __t) + ' </p>			\n		</div>\n\n	</div>\n	\n\n';
    });
    __p += '\n\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["predictions"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '\n<div class="" id="">\n	<p class="graphic-chart-label text-xs-center mt-1">' + ((__t = 'Predictions markets') == null ? '' : __t) + '</p>\n	<p class="bottomline-text font-italic text-xs-center col-md-6 offset-md-3">' + ((__t = "Percentage probabilities on winning the French election, where punters trade the likely outcome of a political event as if it were a stock price.") == null ? '' : __t) + '</p>\n	\n	<div class="row">\n		<div class="col-sm-6">\n	        <p class="pollster"><strong>' + ((__t = 'Hypermind') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="reutersGraphic-predictions-hypermind" class="reuters-chart"></div>		\n		</div>\n	\n		<div class="col-sm-6">\n	        <p class="pollster"><strong>' + ((__t = 'PredictIt') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="reutersGraphic-predictions-predictit" class="reuters-chart"></div>\n		</div>\n	</div>\n	\n	<div class="row hidden-xs-down">\n		';
    var canarray = ["macron", "lepen"];
    var canlook = {

      macron: "Emmanuel Macron",

      lepen: "Marine Le Pen"
    };
    canarray.forEach(function(d, i) {;
      __p += '\n			<div class="col-sm-2 ';
      if (i == 0) {;
        __p += 'offset-sm-4';
      };
      __p += '">\n				<img class="img-fluid" src="images/candidates/' + ((__t = d) == null ? '' : __t) + '.jpg">\n				<div class="candidate-name text-sm-center">\n					' + ((__t = canlook[d]) == null ? '' : __t) + '\n				</div>\n					\n	\n			</div>	\n		';
    });
    __p += '\n	</div>\n	\n</div>	';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["results"] = function(t) {
    var __t,
      __p = '';
    __p += '\n	<div class="row">\n		<div class="col-sm-8 offset-sm-2">\n			<p class="graphic-chart-label text-xs-center">' + ((__t = '2017 results') == null ? '' : __t) + '</p>\n			<p class="bottomline-text text-xs-center col-md-6 offset-md-3">' + ((__t = "How France voted in the first round") == null ? '' : __t) + '</p>\n			\n\n		</div>\n\n			<div class="col-sm-12">\n		        <p class="pollster text-sm-center"><strong>' + ((__t = 'Candidates proceeding to the second-round runoff') == null ? '' : __t) + '</strong></p>\n		        <br class="hidden-md-up">\n			    <div id="reutersGraphic-chart-results-winners" class="reuters-chart"></div>\n			</div>\n			\n			<div class="col-sm-12"> \n			    <p class="pollster text-sm-center"><strong>' + ((__t = 'Unsuccessful candidates') == null ? '' : __t) + '</strong></p>\n		        <br class="hidden-md-up">\n		        <div id="reutersGraphic-chart-results-losers" class="reuters-chart"></div>	\n		    </div>\n		      \n\n		<div class="col-md-8 offset-md-2 mt-2 mb-2">\n	        <p class="pollster text-sm-center"><strong>' + ((__t = 'Leading candidates in the 2017 first-round vote, by departments') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n				<div class="legend">\n					<div class="legend-box macron"></div> EMMANUEL MACRON, En Marche, an unaffiliated movement\n				</div>\n				<div class="legend">\n					<div class="legend-box lepen"></div> MARINE LE PEN, National Front\n				</div>\n				<div class="legend">\n					<div class="legend-box fillon"></div> FRANCOIS FILLON, the Republicans\n				</div>\n				<div class="legend">\n					<div class="legend-box melenchon"></div> JEAN-LUC MELENCHON, France Unbowed\n				</div>\n	        <img class="mt-2" src="images/results/2017leadingFirstRound.jpg" width="100%">\n		</div>\n\n		<div class="col-md-8 offset-md-2 mt-2 mb-2">\n	        <p class="pollster text-sm-center"><strong>' + ((__t = 'Voter turnout for French presidential elections') == null ? '' : __t) + '</strong></p>\n	        <br class="hidden-md-up">\n	        <div id="results-turnout-scatter" class="result-chart"></div>\n		</div>\n\n		\n	</div>\n	\n\n		';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["sample"] = function(t) {
    var __t,
      __p = '';
    __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["scatterSetupTemplate"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.self.dataType) {;
      __p += '\n	<div class="chart-nav">\n			';
      if (!t.self.multiDataSlider) {;
        __p += '		\n            		<div class="navContainer">\n                        <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                            ';
        t.self.multiDataLabels.forEach(function(d, i) {;
          __p += '\n                                <label dataid="' + ((__t = t.self.multiDataColumns[i]) == null ? '' : __t) + '" class="btn btn-primary ';
          if (i == t.self.multiDataLabels.length - 1) {;
            __p += 'active';
          };
          __p += ' smaller">\n                                    <input type="radio" name="nav-options" autocomplete="off"> \n                                    ' + ((__t = d) == null ? '' : __t) + '\n                                </label>\n                            ';
        });
        __p += '\n                        </div>    		    		\n            		</div>    	\n			';
      } else {;
        __p += '\n				<div class="slider-container">\n                    <div class="slider-holder">\n        				<div class="slider" data-slider="true"></div>\n                    </div>\n\n                    <div class="slider-controls">\n                        <div class="btn-group animation-control" data-toggle="buttons">\n                            <label class="btn btn-primary smaller animation-play">\n                                <input type="radio" name="animation-control-group" id="animation-play" autocomplete="off" > \n                                <i class="fa fa-play" aria-hidden="true"></i>\n                            </label>\n                            <label class="btn btn-primary smaller active animation-pause">\n                                <input type="radio" name="animation-control-group" id="animation-pause" autocomplete="off" checked>\n                                <i class="fa fa-pause" aria-hidden="true"></i>\n                            </label>\n                        </div>\n                    </div>\n				</div>\n			';
      };
      __p += '\n	</div>\n';
    };
    __p += '\n\n\n<div class="chart-holder">\n    ';
    if (t.self.colorDomain && t.self.colorDomain.length > 1) {;
      __p += '\n    	<div class="scatter-nested-legend">\n            ';
      t.self.colorDomain.forEach(function(d, i) {;
        __p += '\n                <div class ="scatter-legend-item" data-id="' + ((__t = d) == null ? '' : __t) + '">\n                	<div class = "scatter-legend-circle circle" style="background-color:' + ((__t = t.self.colors[i]) == null ? '' : __t) + ';"></div>\n                	<p class = "scatter-legend-text">' + ((__t = d) == null ? '' : __t) + '</p>\n                </div>\n            ';
      });
      __p += '\n        	';
      if (t.self.rvalue) {;
        __p += '\n                <br>\n                <div class ="scatter-legend-size">\n                    <div class = "scatter-legend-circle scatter-size circle order-legend"></div>\n                    <p class = "scatter-legend-text">' + ((__t = 'Size indicates Orders') == null ? '' : __t) + '</p>\n                 </div>\n        	';
      };
      __p += ' \n            ';
      if (t.self.dropdown) {;
        __p += '\n                <div class="mt-2 hidden-sm-down">\n                    <select class="custom-select scatter-select">\n                        <option selected>Show All ...   </option>\n                    </select>\n                    <small class="text-muted text-uppercase d-block">Choose to highlight</small>\n                </div>\n            ';
      };
      __p += '        	        \n    	</div>\n        <div class="scatter-nested-chart" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + '-chart"></div>\n    ';
    } else {;
      __p += '\n        ';
      if (t.self.dropdown) {;
        __p += '\n            <div class="mt-2 hidden-sm-down">\n                <select class="custom-select scatter-select">\n                    <option selected>Show All ...   </option>\n                </select>\n                <small class="text-muted text-uppercase d-block">Choose to highlight</small>\n            </div>\n        ';
      };
      __p += '         \n        <div class="" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + '-chart"></div>\n    ';
    };
    __p += '\n</div>\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["scatterToolTip"] = function(t) {
    var __t,
      __p = '';
    __p += '<p class="tooltip-text text-uppercase font-weight-bold"> ' + ((__t = t.data.round + " ROUND " + t.data.category) == null ? '' : __t) + '</p>\n<p class="tooltip-text">' + ((__t = t.self.oneDecimal(t.data[t.self.xvalue]) + "%") == null ? '' : __t) + '</p>\n\n';
    return __p;
  };
})();
//for translations.
window.gettext = function(text) {
  return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function(hashBased) {
  var query = void 0;
  if (hashBased) {
    var pos = location.href.indexOf('?');
    if (pos == -1) return [];
    query = location.href.substr(pos + 1);
  } else {
    query = location.search.substr(1);
  }
  var result = {};
  query.split('&').forEach(function(part) {
    if (!part) return;
    part = part.split('+').join(' '); // replace every + with space, regexp-free version
    var eq = part.indexOf('=');
    var key = eq > -1 ? part.substr(0, eq) : part;
    var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

    //convert true / false to booleans.
    if (val == 'false') {
      val = false;
    } else if (val == 'true') {
      val = true;
    }

    var f = key.indexOf('[');
    if (f == -1) {
      result[decodeURIComponent(key)] = val;
    } else {
      var to = key.indexOf(']');
      var index = decodeURIComponent(key.substring(f + 1, to));
      key = decodeURIComponent(key.substring(0, f));
      if (!result[key]) {
        result[key] = [];
      }
      if (!index) {
        result[key].push(val);
      } else {
        result[key][index] = val;
      }
    }
  });
  return result;
};

Reuters.trackEvent = function(category, type, id) {
  category = category || 'Page click';
  //console.log(category, type, id);
  var typeString = type;
  if (id) {
    typeString += ': ' + id;
  }
  var gaOpts = {
    'nonInteraction': false,
    'page': PAGE_TO_TRACK
  };

  ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function() {
  $('[data-slider]').each(function() {
    var $el = $(this);
    var getPropArray = function getPropArray(value) {
      if (!value) {
        return [0];
      }
      var out = [];
      var values = value.split(',');
      values.forEach(function(value) {
        out.push(parseFloat(value));
      });
      return out;
    };
    var pips = undefined;
    var start = getPropArray($el.attr('data-start'));
    var min = getPropArray($el.attr('data-min'));
    var max = getPropArray($el.attr('data-max'));
    var orientation = $el.attr('data-orientation') || 'horizontal';
    var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
    var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
    var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
    var snap = $el.attr('data-snap') === 'true' ? true : false;
    var pipMode = $el.attr('data-pip-mode');
    var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
    var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
    var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
    if (pipMode === 'count') {
      pipValues = pipValues[0];
    }

    if (pipMode) {
      pips = {
        mode: pipMode,
        values: pipValues,
        stepped: pipStepped,
        density: pipDensity
      };
    }

    if (connect) {
      (function() {
        var cs = [];
        connect.split(',').forEach(function(c) {
          c = c === 'true' ? true : false;
          cs.push(c);
        });
        connect = cs;
      })();
    }

    noUiSlider.create(this, {
      start: start,
      range: {
        min: min,
        max: max
      },
      snap: snap,
      orientation: orientation,
      step: step,
      tooltips: tooltips,
      connect: connect,
      pips: pips
    });
    //This probably doesn't belong here, but will fix the most common use-case.
    $(this).find('div.noUi-marker-large:last').addClass('last');
    $(this).find('div.noUi-marker-large:first').addClass('first');
  });
};

Reuters.hasPym = false;
try {
  Reuters.pymChild = new pym.Child({
    polling: 500
  });
  if (Reuters.pymChild.id) {
    Reuters.hasPym = true;
    $("body").addClass("pym");
  }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
  $("html").addClass("media");
}
if (Reuters.Graphics.Parameters.eikon) {
  $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
  $("html").addClass("remove-header");
}
if (Reuters.Graphics.Parameters.image == "no") {
  $("html").addClass("remove-img");
}
if (Reuters.Graphics.Parameters.onlycandidates == "yes") {
  $("html").addClass("only-candidate");
}
if (Reuters.Graphics.Parameters.onlypolls == "yes") {
  $("html").addClass("only-polls");
}
if (Reuters.Graphics.Parameters.headline == "no") {
  $("html").addClass("remove-headline");
}
//# sourceMappingURL=utils.js.map

Reuters.Graphics.FeaturePage = function(_Backbone$View) {
  babelHelpers.inherits(FeaturePage, _Backbone$View);

  function FeaturePage() {
    babelHelpers.classCallCheck(this, FeaturePage);
    return babelHelpers.possibleConstructorReturn(this, (FeaturePage.__proto__ || Object.getPrototypeOf(FeaturePage)).apply(this, arguments));
  }

  babelHelpers.createClass(FeaturePage, [{
    key: 'preinitialize',
    value: function preinitialize() {
      this.events = {
        'change .nav-options .btn': 'onSectionChange'
      };
      this.router = new Reuters.Graphics.FeaturePageRouter();
    }
  }, {
    key: 'initialize',
    value: function initialize(options) {
      var self = this;
      this.$el.html(Reuters.Graphics.Template.featureLayout());
      d3.queue()
        //english language version
        .defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-election-second-round-candidates-en").defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/pollone.json").defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/polltwo.json").await(render);
      // //for french langauge version
      // .defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-election-first-round-candidates-fr")
      // .defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/pollone.json")
      // .defer(d3.json, "//graphics.thomsonreuters.com/frenchelex/polltwo.json")
      // .await(render);

      /*
   			.defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-test-fr-polls-first")
   			.defer(d3.json, "//d3sl9l9bcxfb5q.cloudfront.net/json/mo-test-fr-polls-second")
   */

      function render(error, candidates, firstPolls, secondPolls) {
        // console.log(candidates, firstPolls, secondPolls)

        firstPolls.forEach(function(d) {
          d.daterange = d.daterange.replace("Jan.", "Jan.").replace("Feb.", "Feb.").replace("March", "March").replace("April", "April").replace("Dec.", "Dec.");
        });
        secondPolls.forEach(function(d) {
          d.daterange = d.daterange.replace("Jan.", "Jan.").replace("Feb.", "Feb.").replace("March", "March").replace("April", "April").replace("Dec.", "Dec.");
        });

        self.render(error, candidates, firstPolls, secondPolls);
      }

      this.listenTo(this.router, 'route:section', this.changeSection);
    }
  }, {
    key: 'render',
    value: function render(error, candidates, firstPolls, secondPolls) {
      this.parseDate = d3.time.format("%d/%m/%Y").parse;
      this.formatDate = d3.time.format("%b %e");

      Backbone.history.start();
      this.candidatesArray = ["macron", "lepen"];
      this.candidateLookup = {
        macron: "Emmanuel Macron",
        lepen: "Marine Le Pen"
      };

      this.candidates(candidates);
      this.firstPolls(firstPolls, secondPolls);
      this.predictions();
      this.results();

      return this;
    }
  }, {
    key: 'candidates',
    value: function candidates(data) {
      var election = {
        issues: ["economy", "securityandimmigration", "europeandworld", "societyandgovernance"],
        candidates: data,
        issuelookup: {
          economy: "Economy",
          securityandimmigration: "Security and immigration",
          europeandworld: "Europe and the world",
          societyandgovernance: "Society and governance"
        }
      };

      $("#candidate-sort").html(Reuters.Graphics.Template.candidateContent({
        election: election
      }));
      $("#issue-sort").html(Reuters.Graphics.Template.issueContent({
        election: election
      }));

      $(".navContainer.candidate-buttons .btn").on("click", function(evt) {
        var thisID = $(this).attr("dataid");
        $(".candidate-table").removeClass("selected");
        $("#" + thisID).addClass("selected");
      });
    }
  }, {
    key: 'results',
    value: function results() {
      var self = this;
      $("#results-charts").html(Reuters.Graphics.Template.results({
        self: self
      }));

      d3.json("//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-first-round-results", function(data) {

        data.sort(function(a, b) {
          return parseFloat(b.results) - parseFloat(a.results);
        });

        var winnersData = JSON.parse(JSON.stringify([data[0], data[1]]));
        var losersData = JSON.parse(JSON.stringify([data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10]]));

        // var losersData = JSON.parse(JSON.stringify( [,data[2],data[3],data[4],data[5],data[6],data[7],data[8],data[9],data[10]]));

        Reuters.Graphics.sharePrice = new Reuters.Graphics.BarChart({
          el: "#reutersGraphic-chart-results-winners",
          // dataURL: '//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-first-round-results',
          dataURL: winnersData,
          height: 90, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          barFill: function barFill(d) {
            var self = this;
            if (d.category === "JEAN-LUC MELENCHON") {
              return red5;
            } else if (d.category === "EMMANUEL MACRON") {
              return gray3;
            } else if (d.category === "MARINE LE PEN") {
              return blue5;
            } else if (d.category === "BENOIT HAMON") {
              return red3;
            } else if (d.category === "FRANCOIS FILLON") {
              return blue3;
            }
            return gray1;
          },
          YTickLabel: [
            [gettext(""), "%"]
          ], //  \u00A0  - use that code for a space.
          numbFormat: d3.format(",.2f"),
          hasLegend: false,
          showTip: true,
          margin: {
            top: 10,
            right: 30,
            bottom: 30,
            left: 165
          },
          yScaleVals: [0, 5, 10, 15, 20, 25],
          horizontal: true,
          categorySort: "descending"
        });
        Reuters.Graphics.sharePrice = new Reuters.Graphics.BarChart({
          el: "#reutersGraphic-chart-results-losers",
          // dataURL: '//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-first-round-results',
          dataURL: losersData,
          height: 350, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          barFill: function barFill(d) {
            var self = this;
            return gray1;
          },
          YTickLabel: [
            [gettext(""), "%"]
          ], //  \u00A0  - use that code for a space.
          numbFormat: d3.format(",.2f"),
          hasLegend: false,
          showTip: true,
          yScaleVals: [0, 5, 10, 15, 20, 25],
          margin: {
            top: 10,
            right: 30,
            bottom: 30,
            left: 165
          },
          horizontal: true,
          categorySort: "descending"
        });
      });

      Reuters.Graphics.scattergraphic = new Reuters.Graphics.ScatterPlot({
        el: "#results-turnout-scatter",
        dataURL: '//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-presidential-voter-turnout',
        xvalue: "turnout",
        yvalue: "category",
        colorvalue: "round",
        colors: [gray2, gray6],
        colorDomain: ["First round", "Second round"], //can define colors explicitly
        radiusModifier: 5, // a multiplier for sized radius's
        hardRadius: 8,
        yticks: 5,
        xticks: 5,
        height: 450, //if a number smaller then 10, that will be it's aspect to width, if over 10 will be hard height.  if undefined will be same as width
        xvalues: [60, 70, 80, 90, 100],
        xLabelText: "Percentage of registered voters who cast a ballot",

        // scatterSetupTemplate: Reuters.Graphics.Template.scatterSetupTemplate,
        tooltipTemplate: Reuters.Graphics.Template.scatterToolTip

      });

      // 	Reuters.Graphics.scattergraphic = new Reuters.Graphics.ScatterPlot({
      // 	el: "#results-unemploy-scatter",
      // 	dataURL: '//d3sl9l9bcxfb5q.cloudfront.net/json/mo-french-2017firstroundresults-demographics',
      // 	xvalue:"unemploy",
      // 	yvalue:"category",
      // 	colors: [tangerine3],
      // 	radiusModifier:5, // a multiplier for sized radius's
      // 	hardRadius:8,
      // 	yticks:5,
      // 	xticks:5,
      // 	height:450, //if a number smaller then 10, that will be it's aspect to width, if over 10 will be hard height.  if undefined will be same as width
      // 	xLabelText:"Unemloyment rate by leading candidates in each department ",

      // 	scatterSetupTemplate: Reuters.Graphics.Template.scatterSetupTemplate,
      // 	tooltipTemplate: Reuters.Graphics.Template.scatterToolTip,

      // });
    }
  }, {
    key: 'predictions',
    value: function predictions() {
      var self = this;
      $("#predictions-charts").html(Reuters.Graphics.Template.predictions({
        self: self
      }));

      d3.json("//graphics.thomsonreuters.com/data/frenchPredict.json", function(data) {

        data.forEach(function(object) {
          var total = 0;
          //the underscore .each looks at each key in the object.  So it finds d.Hamon and d.Macron, et cetera
          _.each(object, function(value, key) {
            //we dont' want to add the date to the total, so ignore this, if it is date
            if (key == "date") {
              return;
            }
            if (key != "Mélenchon" && key != "Hamon" && key != "Fillon" && key != "Le Pen" && key != "Macron") {
              return;
            }
            //otherwise add it to the total. Now we ahve each candidate added together.
            if (!value) {
              value = 0;
            }
            total += parseFloat(value);
          });
          //but let's do the loop again, and this time divide each candidate by the total
          _.each(object, function(value, key) {
            if (key == "date") {
              return;
            }
            if (key != "Mélenchon" && key != "Hamon" && key != "Fillon" && key != "Le Pen" && key != "Macron") {
              return;
            }
            if (!value) {
              value = 0;
            }
            //d[key} is going to be each of the candidates in order d.hamon, d.macron, et cetera
            object[key] = parseFloat(value) / total;
          });
        });

        Reuters.Graphics.predictionspredictit = new Reuters.Graphics.LineChart({
          el: "#reutersGraphic-predictions-predictit",
          // dataURL: '//graphics.thomsonreuters.com/data/frenchPredict.json',
          dataURL: data,
          height: 220, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {

            Macron: "Macron",

            "Le Pen": "Le Pen"
          },
          colors: {

            Macron: gray2,

            "Le Pen": blue5
          },
          YTickLabel: [
            [gettext(""), "%"]
          ], //  \u00A0  - use that code for a space.
          dateFormat: d3.time.format("%b %d"),
          numbFormat: d3.format(",.1f"),
          divisor: .01,
          hasLegend: false,
          showTip: true,
          xScaleMin: function xScaleMin() {
            var parseDate = d3.time.format("%m/%d/%Y").parse;
            var min = parseDate("2/13/2017");
            return min;
          }

        });

        Reuters.Graphics.predictionspredictit.on("renderChart:start", function(evt) {
          var self = this;
          var parseDate = d3.time.format("%d/%m/%y").parse;

          self.addLine = self.svg.append("line").attr({
            x1: self.scales.x(parseDate("23/4/17")),
            x2: self.scales.x(parseDate("23/4/17")),
            y1: 0,
            y2: self.height,
            class: "leder-line"
          });
        });

        Reuters.Graphics.predictionspredictit.on("update:end", function(evt) {

          var self = this;
          var parseDate = d3.time.format("%d/%m/%y").parse;

          self.addLine.transition().duration(1000).attr({
            x1: self.scales.x(parseDate("23/4/17")),
            x2: self.scales.x(parseDate("23/4/17")),
            y1: 0,
            y2: self.height,
            class: "leder-line-2"
          });
        });
      });

      Reuters.Graphics.predictionsHypermind = new Reuters.Graphics.LineChart({
        el: "#reutersGraphic-predictions-hypermind",
        dataURL: '//graphics.thomsonreuters.com/data/frenchHypermind.json',
        height: 220, //if < 10 - ratio , if over 10 - hard height.  undefined - square
        columnNames: {

          em: "Macron",

          mlp: "Le Pen"
        },
        colors: {

          em: gray2,

          mlp: blue5
        },
        YTickLabel: [
          [gettext(""), "%"]
        ], //  \u00A0  - use that code for a space.
        dateFormat: d3.time.format("%b %d"),
        numbFormat: d3.format(",.1f"),
        hasLegend: false,
        showTip: true,
        xScaleMin: function xScaleMin() {
          var parseDate = d3.time.format("%m/%d/%Y").parse;
          var min = parseDate("2/13/2017");
          return min;
        }
      });

      Reuters.Graphics.predictionsHypermind.on("renderChart:start", function(evt) {
        var self = this;
        var parseDate = d3.time.format("%d/%m/%y").parse;

        self.addLine = self.svg.append("line").attr({
          x1: self.scales.x(parseDate("23/4/17")),
          x2: self.scales.x(parseDate("23/4/17")),
          y1: 0,
          y2: self.height,
          class: "leder-line"
        });
      });

      Reuters.Graphics.predictionsHypermind.on("update:end", function(evt) {

        var self = this;
        var parseDate = d3.time.format("%d/%m/%y").parse;

        self.addLine.transition().duration(1000).attr({
          x1: self.scales.x(parseDate("23/4/17")),
          x2: self.scales.x(parseDate("23/4/17")),
          y1: 0,
          y2: self.height,
          class: "leder-line-2"
        });
      });
    }
  }, {
    key: 'firstPolls',
    value: function firstPolls(_firstPolls, secondPolls) {
      var self = this;
      var secondData = _.groupBy(secondPolls, "matchup")["macron / lepen"];
      console.log(secondData);

      var groupedFirstPolls = _.groupBy(secondData, "pollster");

      var firstPollsData = {};
      var largestFirstPollValue = 0;
      var firstPollsHistorical = {};

      _.each(groupedFirstPolls, function(array, key) {

        if (key == "Opinionway" || key == "Ifop-Fiducial") {
          firstPollsHistorical[key] = array;
        }

        var lastItem = array[array.length - 1];

        firstPollsData[key] = self.candidatesArray.map(function(candidate) {
          largestFirstPollValue = parseFloat(lastItem[candidate]) > largestFirstPollValue ? parseFloat(lastItem[candidate]) : largestFirstPollValue;
          return {
            category: candidate,
            value: lastItem[candidate],
            pollster: lastItem.pollster,
            marginoferror: lastItem.marginoferror,
            samplesize: lastItem.samplesize,
            polldate: lastItem.date,
            daterange: lastItem.daterange
          };
        });
      });

      largestFirstPollValue = Math.ceil(largestFirstPollValue / 10) * 10;

      var pollsters = _.keys(firstPollsData).sort(function(a, b) {
        var aValue = self.parseDate(firstPollsData[a][0].polldate);
        var bValue = self.parseDate(firstPollsData[b][0].polldate);
        if (aValue > bValue) {
          return -1;
        }
        if (aValue < bValue) {
          return 1;
        }
        return 0;
      });
      self.samplesizeFormat = d3.format(",.0f");
      self.marginFormat = d3.format(",.1f");

      $("#polls").html(Reuters.Graphics.Template.pollsfirst({
        pollsters: pollsters,
        firstPollsData: firstPollsData,
        self: this
      }));
      pollsters.forEach(function(key, i) {
        var value = firstPollsData[key];
        var id = key.split(' ')[0].split('/')[0];
        value.forEach(function(d) {
          d.category = self.candidateLookup[d.category];
        });

        var sortOrder = self.candidatesArray.map(function(d) {
          return self.candidateLookup[d];
        });

        Reuters.Graphics[id] = new Reuters.Graphics.BarChart({
          el: "#reutersGraphic-latest-poll-" + id,
          dataURL: value,
          height: 75, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: {
            value: "Value"
          }, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          color: [gray2, blue5],
          orient: "right",
          YTickLabel: [
            [gettext(""), "%"]
          ], //  \u00A0  - use that code for a space.
          hasLegend: false,
          numbFormat: d3.format(",.1f"),
          horizontal: true,
          yScaleMax: function yScaleMax() {
            return largestFirstPollValue;
          },
          categorySort: sortOrder,
          barFill: function barFill(d) {
            var self = this;
            return self.color[self.categorySort.indexOf(d.category)];
          },
          parseDate: d3.time.format("%d/%m/%Y").parse
        });

        Reuters.Graphics[id].on("renderChart:end", function(evt) {
          self.addMoe(this);
        });

        Reuters.Graphics[id].on("update:start", function(evt) {
          self.updateMoe(this);
        });
        if (i < pollsters.length - 1) {
          $("#reutersGraphic-latest-poll-" + id).addClass("hidden-x-axis");
        }
      });
      _.each(firstPollsHistorical, function(array, key) {
        var id = key.split("-")[0];
        Reuters.Graphics["historical" + id] = new Reuters.Graphics.LineChart({
          el: "#reutersGraphic-first-" + id,
          dataURL: array,
          height: 220, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: self.candidateLookup,
          colors: {
            melenchon: red5,
            hamon: red3,
            macron: gray2,
            fillon: blue3,
            lepen: blue5
          },
          yScaleTicks: 3,
          numbFormat: d3.format(",.1f"),
          dateFormat: d3.time.format("%b %d"),
          YTickLabel: [
            [gettext(""), "%"]
          ], //  \u00A0  - use that code for a space.
          hasLegend: false,
          yScaleMax: function yScaleMax() {
            return largestFirstPollValue;
          },
          chartLayout: "fillLines",
          xTickFormat: function xTickFormat(d) {
            var formatDate = d3.time.format("%b %e");
            return formatDate(d);
          },
          parseDate: d3.time.format("%d/%m/%Y").parse
        });

        Reuters.Graphics["historical" + id].on("renderChart:end", function(evt) {
          var self = this;
          self.area.y0(function(d) {
            var moe = parseFloat(d.marginoferror) / 2;
            return self.scales.y(d[self.dataType] - moe);
          }).y1(function(d) {
            var moe = parseFloat(d.marginoferror) / 2;
            return self.scales.y(d[self.dataType] + moe);
          });
        });

        Reuters.Graphics["historical" + id].on("update:start", function(evt) {});
      });
      //this.secondPolls(secondPolls)
    }
  }, {
    key: 'secondPolls',
    value: function secondPolls(_secondPolls) {
      var self = this;
      _secondPolls.forEach(function(d) {
        d.parsedDate = self.parseDate(d.date);
      });
      var groupedMatchup = _.groupBy(_secondPolls, "matchup");
      var matchupData = {};
      _.each(groupedMatchup, function(values, key) {
        matchupData[key] = [];
        var groupedPolls = _.groupBy(values, "pollster");
        _.each(groupedPolls, function(array, pollster) {
          array.sort(function(a, b) {
            if (a.parsedDate > b.parsedDate) {
              return 1;
            }
            if (a.parsedDate < b.parsedDate) {
              return -1;
            }
            return 0;
          });
          var lastItem = array[array.length - 1];
          var obj = {
            polldate: lastItem.date,
            category: lastItem.daterange,
            samplesize: lastItem.samplesize,
            marginoferror: lastItem.marginoferror,
            pollster: lastItem.pollster
          };
          self.candidatesArray.forEach(function(d) {
            if (lastItem[d]) {
              obj[d] = lastItem[d];
            }
          });
          matchupData[key].push(obj);
        });
      });

      var matchups = _.keys(matchupData).map(function(d) {
        return {
          matchup: d,
          first: d.split(" / ")[0],
          second: d.split(" / ")[1],
          firstFull: self.candidateLookup[d.split(" / ")[0]],
          secondFull: self.candidateLookup[d.split(" / ")[1]]
        };
      });

      $("#second-round").html(Reuters.Graphics.Template.pollssecond({
        matchups: matchups,
        data: matchupData,
        self: this
      }));

      matchups.forEach(function(d) {
        var columnName = {};
        var colors = {};
        var colorarray = [red5, red3, gray2, blue3, blue5];
        // console.log(d.first, d.second, matchupData)
        var keys = _.keys(matchupData[d.first + " / " + d.second][0]).filter(function(d) {
          return d != "category" && d != "pollster" && d != "marginoferror" && d != "polldate" && d != "samplesize";
        });
        keys.forEach(function(d) {
          columnName[d] = self.candidateLookup[d];
          colors[d] = colorarray[self.candidatesArray.indexOf(d)];
        });

        matchupData[d.first + " / " + d.second].sort(function(a, b) {
          var aDate = self.parseDate(a.polldate);
          var bDate = self.parseDate(b.polldate);
          if (aDate < bDate) {
            return 1;
          }
          if (aDate > bDate) {
            return -1;
          }
          return 0;
        });

        Reuters.Graphics[d.first + d.second] = new Reuters.Graphics.BarChart({
          el: "#reutersGraphic-first-" + d.first + d.second,
          dataURL: matchupData[d.first + " / " + d.second],
          height: 125, //if < 10 - ratio , if over 10 - hard height.  undefined - square
          columnNames: columnName, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
          colors: colors,
          YTickLabel: [
            [gettext(""), "%"]
          ], //  \u00A0  - use that code for a space.
          numbFormat: d3.format(",.1f"),
          hasLegend: false,
          horizontal: true,
          yScaleVals: [0, 50, 100],
          chartLayout: "stackTotal",
          groupSort: [d.second, d.first],
          categorySort: "none",
          tipTemplate: Reuters.Graphics.Template.pollSecondTooltip,
          parseDate: d3.time.format("%d/%m/%Y").parse
        });
      });

      $(".navContainer.poll-buttons .btn").on("click", function(evt) {
        var thisID = $(this).attr("dataid");
        $(".poll-section").removeClass("selected");
        $("#" + thisID).addClass("selected");
      });
    }
  }, {
    key: 'addMoe',
    value: function addMoe(self) {

      self.t = textures.lines().size(5).orientation("2/8").stroke("#C3C4C6");

      self.svg.call(self.t);

      self.addMoe = self.barChart.selectAll(".moebar").data(function(d) {
        return d.values;
      }).enter().append("rect").attr("class", ".moebar").style("fill", function(d) {
        return self.t.url();
      }).attr("height", function(d, i, j) {
        return self.barWidth(d, i, j);
      }).attr("y", function(d, i, j) {
        return self.xBarPosition(d, i, j);
      }).attr("x", function(d) {
        return self.scales.y(d.value) - self.scales.y(d.marginoferror) / 2;
      }).attr("width", function(d) {
        return self.scales.y(d.marginoferror);
      });
    }
  }, {
    key: 'updateMoe',
    value: function updateMoe(self) {

      self.addMoe.transition().duration(1000).attr("height", function(d, i, j) {
        return self.barWidth(d, i, j);
      }).attr("y", function(d, i, j) {
        return self.xBarPosition(d, i, j);
      }).attr("x", function(d) {
        return self.scales.y(d.value) - self.scales.y(d.marginoferror) / 2;
      }).attr("width", function(d) {
        return self.scales.y(d.marginoferror);
      });
    }
  }, {
    key: 'onSectionChange',
    value: function onSectionChange(event) {
      var $el = $(event.currentTarget);
      var id = $el.attr('data-id');
      this.changeSection(id, $el);
    }
  }, {
    key: 'changeSection',
    value: function changeSection(id, $button) {
      // console.log('changing section fired', id, $button);
      var $el = this.$('#' + id);
      if ($el.hasClass('selected')) {
        return;
      }
      if (!$button) {
        $button = this.$('.nav-options .btn[data-id="' + id + '"]');
        $button.addClass('active').siblings().removeClass('active');
        $button.find('input').addClass('active').attr('checked', 'checked');
      }
      $el.addClass('selected').siblings().removeClass('selected');
      this.router.navigate('section/' + id, {
        trigger: false
      });
      // 		_.invoke(this.charts, 'update');
    }
  }]);
  return FeaturePage;
}(Backbone.View);
Reuters.Graphics.FeaturePageRouter = function(_Backbone$Router) {
  babelHelpers.inherits(FeaturePageRouter, _Backbone$Router);

  function FeaturePageRouter() {
    babelHelpers.classCallCheck(this, FeaturePageRouter);
    return babelHelpers.possibleConstructorReturn(this, (FeaturePageRouter.__proto__ || Object.getPrototypeOf(FeaturePageRouter)).apply(this, arguments));
  }

  babelHelpers.createClass(FeaturePageRouter, [{
    key: 'preinitialize',
    value: function preinitialize() {
      this.routes = {
        'section/:id': 'section'
      };
    }
  }]);
  return FeaturePageRouter;
}(Backbone.Router);
$(document).ready(function() {
  if (Reuters.Graphics.Parameters.eikon) {
    black = white;
  }
  window.featurePageExample = new Reuters.Graphics.FeaturePage({
    el: '.main'
  });
});
//# sourceMappingURL=main.js.map
