(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["candidateContent"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }

        t.election.candidates.forEach(function(d, i) {;
            __p += '\n   <div class="row">\n        <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n            <img src="images/candidates/' + ((__t = d.imageid) == null ? '' : __t) + '-single-color.jpg" class="candidate-image mx-auto d-block" width="100%">\n\n        </div>\n    </div>\n\n    <div class="row">\n        <div class="col-md-6 offset-md-3 ">\n            <h2 class="candidate-text text-xs-center">' + ((__t = d.candidate) == null ? '' : __t) + '</h2>  \n            <h4 class="font-weight-normal candidate-content-age text-xs-center">' + ((__t = d.age) == null ? '' : __t) + '</h4>               \n            <p class="candidate-content-party text-uppercase text-xs-center">' + ((__t = d.party) == null ? '' : __t) + '</p>               \n            <p class="bottomline-text text-sm-center">' + ((__t = d.bottomline) == null ? '' : __t) + '</p>               \n       </div>\n    </div>\n    \n    <div class="row">\n        ';
            t.election.issues.forEach(function(issue) {;
                __p += '\n            <div class="col-md-3 ">\n                <p class="text-uppercase issue-header font-weight-bold">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</p>\n                <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>\n            </div>  \n        ';
            });
            __p += '        \n    </div>\n\n        ';
            if (i != t.election.candidates.length - 1) {;
                __p += '\n            <hr>\n         ';
            };
            __p += ' \n\n';
        });
        __p += '\n\n';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["featureLayout"] = function(t) {
        var __t,
            __p = '';
        __p += '<div class="feature-header container">\n        <div id="headerbanner" class="row">\n            <img id="bannerImg" class="feature-header-img hidden-sm-down" src="images/banners/French-Header-Banner-Desktop.png">\n     <img class="img-fluid feature-header-img hidden-md-up" src="images/banners/French-Header-Banner-Mobile.jpg">\n    <div id="bannerText">FRENCH PRESIDENTIAL ELECTION 2017</div>     </div>\n\n       <div class="row" id="h1row">\n           <h1 class="display-3 div-col text-xs-center header-text">' + ((__t = 'The candidates') == null ? '' : __t) + '</h1>\n        </div>\n        <div id="deck" class="row">\n            <p id="decktText" class="div-col text-xs-center ">' + ((__t = "France elects a new president in a two-round contest on April 23 and May 7. It is one of the most unpredictable elections in modern French history and one that is being closely watched, after Britain's vote to exit the European Union and Donald Trump's election as US president.<br> A look at the frontrunners and key issues.") == null ? '' : __t) + '</p>\n      <div class="time">      <p class="div-col text-xs-center text-md-center">' + ((__t = 'PUBLISHED: APRIL 21, 2017') == null ? '' : __t) + '</p>\n        </div>\n\n</div>\n\n</div>\n\n<div class="container graphic-section-container">\n\n	<div class="navContainer text-center" style="display:none;">\n        <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                <label dataid="candidate-sort" class="btn btn-primary active smaller">\n                    <input type="radio" name="nav-options" autocomplete="off"> \n                    ' + ((__t = 'KEY PLAYERS') == null ? '' : __t) + '\n                    \n                </label>\n                <label dataid="issue-sort" class="btn btn-primary smaller">\n                    <input type="radio" name="nav-options" autocomplete="off"> \n                    ' + ((__t = 'ISSUES') == null ? '' : __t) + '\n                    \n                </label>\n\n        </div>    		    		\n	</div>    	\n    <div id="candidate-sort" class="candidate-table selected"></div>\n    <div id="issue-sort" class="candidate-table"></div>\n\n';
        return __p;
        //<div class="row col-md-8 offset-md-2" >\n            <img class="img-fluid bottom-banner feature-header-img mb-1 mx-auto hidden-sm-down" src="images/banners/flipped-banner.png">\n    </div>  \n     <div class="row col-md-8 offset-md-2" >\n            <img class="img-fluid bottom-banner feature-header-img mb-1 mx-auto hidden-md-up" src="images/banners/ribbon-mobile.png">\n    </div>    \n    <div class="row">   \n        <div class="col-xs-12 mt-1 text-xs-center">\n            <p class="graphic-source">' + ((__t = 'Source: Candidate programmes and declarations') == null ? '' : __t) + '<br />' + ((__t = 'Graphic by Michael Ovaska and Brian Love. Illustrations by Weiyi Cai | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>   \n\n\n</div>
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["issueContent"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }

        t.election.issues.forEach(function(issue, i) {;
            __p += '\n    <div class="row">\n              <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n                    <img src="images/candidates/' + ((__t = issue) == null ? '' : __t) + '-icon.png" class="candidate-image mx-auto d-block" width="100%">\n              </div>\n    </div>\n\n    <div class="row">\n              <div class="col-sm-6 offset-sm-3">\n                    <h2 class="text-center issue-header">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</h2>\n              </div>\n                      \n    </div>\n\n  ';
            t.election.candidates.forEach(function(d) {;
                __p += '\n      <div class="row">\n\n                <div class="col-lg-1">            \n                </div> \n\n                <div class="col-md-4 col-lg-2 text-xs-center text-md-left issue-content-header">\n                    <p><span class="text-uppercase font-weight-bold">' + ((__t = d.candidate) == null ? '' : __t) + '</span><br>\n                    <span class="">' + ((__t = d.party) == null ? '' : __t) + '</span></p>\n                </div>\n\n\n                <div class="col-md-8  text-xs-left">\n                    <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>               \n                </div>\n\n                <div class="col-lg-1">            \n                </div> \n\n      </div>\n\n   ';
            });
            __p += '\n         \n        ';
            if (i != t.election.issues.length - 1) {;
                __p += '\n            <hr>\n         ';
            };
            __p += ' \n\n';
        });
        __p += '        \n\n\n';
        return __p;
    };
})();
(function() {
    window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["sample"] = function(t) {
        var __t,
            __p = '';
        __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
        return __p;
    };
})();
//for translations.
window.gettext = function(text) {
    return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function(hashBased) {
    var query = void 0;
    if (hashBased) {
        var pos = location.href.indexOf('?');
        if (pos == -1) return [];
        query = location.href.substr(pos + 1);
    } else {
        query = location.search.substr(1);
    }
    var result = {};
    query.split('&').forEach(function(part) {
        if (!part) return;
        part = part.split('+').join(' '); // replace every + with space, regexp-free version
        var eq = part.indexOf('=');
        var key = eq > -1 ? part.substr(0, eq) : part;
        var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

        //convert true / false to booleans.
        if (val == 'false') {
            val = false;
        } else if (val == 'true') {
            val = true;
        }

        var f = key.indexOf('[');
        if (f == -1) {
            result[decodeURIComponent(key)] = val;
        } else {
            var to = key.indexOf(']');
            var index = decodeURIComponent(key.substring(f + 1, to));
            key = decodeURIComponent(key.substring(0, f));
            if (!result[key]) {
                result[key] = [];
            }
            if (!index) {
                result[key].push(val);
            } else {
                result[key][index] = val;
            }
        }
    });
    return result;
};

Reuters.trackEvent = function(category, type, id) {
    category = category || 'Page click';
    //console.log(category, type, id);
    var typeString = type;
    if (id) {
        typeString += ': ' + id;
    }
    var gaOpts = {
        'nonInteraction': false,
        'page': PAGE_TO_TRACK
    };

    ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function() {
    $('[data-slider]').each(function() {
        var $el = $(this);
        var getPropArray = function getPropArray(value) {
            if (!value) {
                return [0];
            }
            var out = [];
            var values = value.split(',');
            values.forEach(function(value) {
                out.push(parseFloat(value));
            });
            return out;
        };
        var pips = undefined;
        var start = getPropArray($el.attr('data-start'));
        var min = getPropArray($el.attr('data-min'));
        var max = getPropArray($el.attr('data-max'));
        var orientation = $el.attr('data-orientation') || 'horizontal';
        var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
        var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
        var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
        var snap = $el.attr('data-snap') === 'true' ? true : false;
        var pipMode = $el.attr('data-pip-mode');
        var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
        var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
        var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
        if (pipMode === 'count') {
            pipValues = pipValues[0];
        }

        if (pipMode) {
            pips = {
                mode: pipMode,
                values: pipValues,
                stepped: pipStepped,
                density: pipDensity
            };
        }

        if (connect) {
            (function() {
                var cs = [];
                connect.split(',').forEach(function(c) {
                    c = c === 'true' ? true : false;
                    cs.push(c);
                });
                connect = cs;
            })();
        }

        noUiSlider.create(this, {
            start: start,
            range: {
                min: min,
                max: max
            },
            snap: snap,
            orientation: orientation,
            step: step,
            tooltips: tooltips,
            connect: connect,
            pips: pips
        });
        //This probably doesn't belong here, but will fix the most common use-case.
        $(this).find('div.noUi-marker-large:last').addClass('last');
        $(this).find('div.noUi-marker-large:first').addClass('first');
    });
};

Reuters.hasPym = false;
try {
    Reuters.pymChild = new pym.Child({
        polling: 500
    });
    if (Reuters.pymChild.id) {
        Reuters.hasPym = true;
        $("body").addClass("pym");
    }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
    $("html").addClass("media");
}
if (Reuters.Graphics.Parameters.eikon) {
    $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
    $("html").addClass("remove-header");
}
//# sourceMappingURL=utils.js.map

$(".main").html(Reuters.Graphics.Template.featureLayout());

d3.csv("data/candidates-data.csv", function(data) {
    var election = {
        issues: ["economy", "securityandimmigration", "europeandworld", "societyandgovernance"],
        candidates: data,
        issuelookup: {
            economy: "Economy",
            securityandimmigration: "Security and immigration",
            europeandworld: "Europe and the world",
            societyandgovernance: "Society and governance"
        }
    };

    $("#candidate-sort").html(Reuters.Graphics.Template.candidateContent({
        election: election
    }));
    $("#issue-sort").html(Reuters.Graphics.Template.issueContent({
        election: election
    }));

    $(".navContainer .btn").on("click", function(evt) {
        var thisID = $(this).attr("dataid");
        $(".candidate-table").removeClass("selected");
        $("#" + thisID).addClass("selected");
    });
});
//# sourceMappingURL=main.js.map
