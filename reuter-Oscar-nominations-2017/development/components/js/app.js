var blue2 = "#ABC8E8";
var blue3 = "#7AADDC";
var blue4 = "#1F8FCE";
var blue5 = "#1B78AC";
var gray2 = "#BCBEC0";
var tangerine1 = "#FFECC6";
var tangerine2 = "#FFE3A7";
var tangerine3 = "#FFD576";
var tangerine4 = "#FDC218";
var tangerine5 = "#D0A115";
var tangerine6 = "#886A00";
(function() {
    Reuters = window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["SortingSquareLegend"] = function(t) {
        var __t,
            __p = '';
        __p += '<div class="chart-holder">\n        <div class="" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + 'Chart"></div>\n</div>\n\n\n\n';
        return __p;
    };
})();
(function() {
    Reuters = window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["SortingSquaresTooltip"] = function(t) {
        var __t,
            __p = '',
            __j = Array.prototype.join;

        function print() {
            __p += __j.call(arguments, '');
        }
        __p += '<div class="row">\n	<div class="col-sm-6">\n		<img class="img-fluid" src="' + ((__t = t.d.Poster) == null ? '' : __t) + '">\n	</div>\n	<div class="col-sm-6">\n		<p class="tooltip-title">' + ((__t = t.d.Title) == null ? '' : __t) + ' - ' + ((__t = t.d.Year) == null ? '' : __t) + '</p>\n		<p class="tooltip-text">' + ((__t = t.d.Plot) == null ? '' : __t) + '</p>			\n		<p class="tooltip-subhead"> ';

        // if (t.d.Title != "La La Land") {
        if (t.d.wins == 1) {;
            __p += '  ' + ((__t = t.d.wins) == null ? '' : __t) + ' win ';
        } else {;
            __p += ' ' + ((__t = t.d.wins) == null ? '' : __t) + ' wins';
        };
        __p += ' 				\n			';
        // };
        __p += '\n		</p>\n	</div>\n</div>\n';
        return __p;
    };
})();
(function() {
    Reuters = window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["basicChartLayout"] = function(t) {
        var __t,
            __p = '';
        __p += '<div class="container">\n    \n    <div class="row">\n        <div class="col-md-5 col-xl-3 col-lg-4 hidden-sm-down" style="display:none">\n			<img src="images/oscars.jpg" class="img-fluid">\n        </div>\n\n        <div class="col-md-7 col-xl-9 col-lg-8">\n            <h1 class="graphic-title" style="display:none">' + ((__t = 'Golden Opportunity') == null ? '' : __t) + '</h1>\n            <p class="graphic-subhead" style="display:none">' + ((__t = 'La La Land has tied Titanic and All About Eve as the most nominated films in Academy Awards history.') == null ? '' : __t) + '</p>\n				<div class="legend-div mb-1">\n		        	<p class="graphic-chart-subhead">NUMBER OF WINS</p>\n		            <div class="key-wrapper ">\n			            <div class="key-box" style="background-color:' + ((__t = gray2) == null ? '' : __t) + '"></div>\n			            <div class="key-box" style="background-color:' + ((__t = tangerine1) == null ? '' : __t) + '"></div>\n			            <div class="key-box" style="background-color:' + ((__t = tangerine2) == null ? '' : __t) + '"></div>\n			            <div class="key-box" style="background-color:' + ((__t = tangerine3) == null ? '' : __t) + '"></div>\n			            <div class="key-box" style="background-color:' + ((__t = tangerine4) == null ? '' : __t) + '"></div>\n			            <div class="key-box" style="background-color:' + ((__t = tangerine5) == null ? '' : __t) + '"></div>\n			            <div class="key-box" style="background-color:' + ((__t = tangerine6) == null ? '' : __t) + '"></div>\n		            </div>\n		            <div class="key-wrapper ">\n			            <div class="key-text" >0</div>\n			            <div class="key-text" >1 - 2</div>\n			            <div class="key-text" >2 - 4</div>\n			            <div class="key-text" >4 - 6</div>\n			            <div class="key-text" >6 - 8</div>\n			            <div class="key-text" >8 - 10</div>\n			            <div class="key-text" > > 10</div>\n		            </div>\n\n				</div>\n				\n            <div id="reutersGraphic-chart1" class="reuters-chart"></div>\n        </div>\n    </div>\n        \n    <div class="row" style="display:none">            \n        <div class="col-xs-12 mt-1">\n            <p class="graphic-source">' + ((__t = 'Sources: Academy Awards Database; OMBD; Reuters (photo)') == null ? '' : __t) + '<br />' + ((__t = 'By Matthew Weber | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>\n</div>\n';
        return __p;
    };
})();
(function() {
    Reuters = window["Reuters"] = window["Reuters"] || {};
    window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
    window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

    window["Reuters"]["Graphics"]["Template"]["sample"] = function(t) {
        var __t,
            __p = '';
        __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
        return __p;
    };
})();
//for translations.
window.gettext = function(text) {
    return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function(hashBased) {
    var query = void 0;
    if (hashBased) {
        var pos = location.href.indexOf('?');
        if (pos == -1) return [];
        query = location.href.substr(pos + 1);
    } else {
        query = location.search.substr(1);
    }
    var result = {};
    query.split('&').forEach(function(part) {
        if (!part) return;
        part = part.split('+').join(' '); // replace every + with space, regexp-free version
        var eq = part.indexOf('=');
        var key = eq > -1 ? part.substr(0, eq) : part;
        var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

        //convert true / false to booleans.
        if (val == 'false') {
            val = false;
        } else if (val == 'true') {
            val = true;
        }

        var f = key.indexOf('[');
        if (f == -1) {
            result[decodeURIComponent(key)] = val;
        } else {
            var to = key.indexOf(']');
            var index = decodeURIComponent(key.substring(f + 1, to));
            key = decodeURIComponent(key.substring(0, f));
            if (!result[key]) {
                result[key] = [];
            }
            if (!index) {
                result[key].push(val);
            } else {
                result[key][index] = val;
            }
        }
    });
    return result;
};

Reuters.trackEvent = function(category, type, id) {
    category = category || 'Page click';
    //console.log(category, type, id);
    var typeString = type;
    if (id) {
        typeString += ': ' + id;
    }
    var gaOpts = {
        'nonInteraction': false,
        'page': PAGE_TO_TRACK
    };

    ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function() {
    $('[data-slider]').each(function() {
        var $el = $(this);
        var getPropArray = function getPropArray(value) {
            if (!value) {
                return [0];
            }
            var out = [];
            var values = value.split(',');
            values.forEach(function(value) {
                out.push(parseFloat(value));
            });
            return out;
        };
        var pips = undefined;
        var start = getPropArray($el.attr('data-start'));
        var min = getPropArray($el.attr('data-min'));
        var max = getPropArray($el.attr('data-max'));
        var orientation = $el.attr('data-orientation') || 'horizontal';
        var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
        var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
        var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
        var snap = $el.attr('data-snap') === 'true' ? true : false;
        var pipMode = $el.attr('data-pip-mode');
        var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
        var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
        var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
        if (pipMode === 'count') {
            pipValues = pipValues[0];
        }

        if (pipMode) {
            pips = {
                mode: pipMode,
                values: pipValues,
                stepped: pipStepped,
                density: pipDensity
            };
        }

        if (connect) {
            (function() {
                var cs = [];
                connect.split(',').forEach(function(c) {
                    c = c === 'true' ? true : false;
                    cs.push(c);
                });
                connect = cs;
            })();
        }

        noUiSlider.create(this, {
            start: start,
            range: {
                min: min,
                max: max
            },
            snap: snap,
            orientation: orientation,
            step: step,
            tooltips: tooltips,
            connect: connect,
            pips: pips
        });
        //This probably doesn't belong here, but will fix the most common use-case.
        $(this).find('div.noUi-marker-large:last').addClass('last');
        $(this).find('div.noUi-marker-large:first').addClass('first');
    });
};

Reuters.hasPym = false;
try {
    Reuters.pymChild = new pym.Child({
        polling: 500
    });
    if (Reuters.pymChild.id) {
        Reuters.hasPym = true;
        $("body").addClass("pym");
    }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
    $("html").addClass("media-flat");
}
if (Reuters.Graphics.Parameters.eikon) {
    $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
    $("html").addClass("remove-header");
}
//# sourceMappingURL=utils.js.map

Reuters = Reuters || {};
Reuters.Graphics = Reuters.Graphics || {};
//back to here
Reuters.Graphics.SortingSquares = Backbone.View.extend({
    data: undefined,
    dataURL: undefined,
    legendtemplate: Reuters.Graphics.Template.SortingSquareLegend,
    tooltipTemplate: Reuters.Graphics.Template.SortingSquaresTooltip,
    idField: "id",
    colorRange: [blue2, blue3, blue4, blue5],
    maxAcross: 3,
    boxSize: 25,
    margin: {
        left: 1,
        top: 35,
        right: 1,
        bottom: 0
    },
    totalHeightObj: {},
    initialize: function initialize(opts) {
        var self = this;
        this.options = opts;

        // if we are passing in options, use them instead of the defualts.
        _.each(opts, function(item, key) {
            self[key] = item;
        });

        if (!self.options.maxAcross) {
            self.options.maxAcross = self.maxAcross;
        }

        //Test which way data is presented and load appropriate way
        if (this.dataURL.indexOf("csv") == -1 && !_.isObject(this.dataURL)) {
            d3.json(self.dataURL, function(data) {
                self.parseData(data);
            });
        }
        if (this.dataURL.indexOf("csv") > -1) {
            d3.csv(self.dataURL, function(data) {
                self.parseData(data);
            });
        }
        if (_.isObject(this.dataURL)) {
            setTimeout(function() {
                self.parseData(self.dataURL);
            }, 100);
        }
        //end of initialize
    },
    parseData: function parseData(data) {
        var self = this;

        self.data = data;

        if (!self.options.idField) {
            self.data.forEach(function(d, i) {
                d.id = i;
            });
        }
        self.targetDiv = self.$el.attr("id");
        self.chartId = "#" + self.targetDiv + "Chart";

        self.parseDate = d3.time.format("%Y-%m-%d").parse;
        self.parseYear = d3.time.format("%m/%d/%Y").parse;
        self.formatDate = d3.time.format("%B %e, %Y");
        self.formatYear = d3.time.format("%Y");

        if (!self.buttonText) {
            self.buttonText = self.buttonColumns;
        }

        if (!self.initialSort) {
            self.initialSort = self.buttonColumns[0];
        }
        self.category = self.initialSort;

        if (!self.colorDomain) {
            self.colorDomain = _.uniq(_.pluck(data, self.colorField));
        }

        self.color = d3.scale.threshold().domain([1, 2, 4, 6, 8, 10]).range([gray2, tangerine1, tangerine2, tangerine3, tangerine4, tangerine5, tangerine6]);

        self.$el.html(self.legendtemplate({
            self: self
        }));
        self.width = $(self.chartId).width() - self.margin.left - self.margin.right;
        self.render();
    },
    setPositions: function setPositions(category) {
        var self = this;
        self.trigger("setPositions:start");
        self.data.sort(function(a, b) {
            var counts = _.countBy(self.data, self.category);
            if (parseFloat(a.noms) > parseFloat(b.noms)) {
                return 1;
            }
            if (parseFloat(a.noms) < parseFloat(b.noms)) {
                return -1;
            }
            if (a.noms == b.noms) {
                if (parseFloat(a.wins) < parseFloat(b.wins)) {
                    return -1;
                }
                if (parseFloat(a.wins) > parseFloat(b.wins)) {
                    return 1;
                }
            }
            return 0;
        });

        if ($(window).width() < 600) {
            self.maxAcross = 1;
        } else {
            self.maxAcross = self.options.maxAcross;
        }

        var nest = d3.nest().key(function(d) {
            return d[category];
        }).map(self.data);

        var keys = d3.keys(nest);

        self.categoryLength = keys.length;

        self.sectionLength = self.width / self.categoryLength;
        self.boxesPerRow = Math.floor(self.sectionLength / self.boxSize) - 1;
        self.labels = [];

        if (self.categoryLength > self.maxAcross) {
            self.sectionLength = self.width / self.maxAcross;
            self.boxesPerRow = Math.floor(self.sectionLength / self.boxSize) - 1;
            self.rows = Math.ceil(self.categoryLength / self.maxAcross);

            var longest = 0;

            _.each(nest, function(d, key) {
                if (d.length > longest) {
                    longest = d.length;
                }
            });

            var depth = Math.ceil(longest / self.boxesPerRow);
            var sectionHeight = depth * self.boxSize + self.boxSize * 2;
            self.height = sectionHeight * self.rows;

            keys.forEach(function(item, index) {
                var columnadjuster = Math.floor(index % self.maxAcross);
                var rowadjuster = Math.floor(index / self.maxAcross);
                var obj = {
                    name: item + " Nominations"
                };
                nest[item].forEach(function(d, i) {
                    d.smallpositionX = Math.floor(i % self.boxesPerRow) * self.boxSize;
                    d.smallpositionY = Math.floor(i / self.boxesPerRow) * self.boxSize;
                    d.masterpositionX = self.width * columnadjuster / self.maxAcross;
                    d.masterpositionY = self.height * rowadjuster / self.rows;
                    d.x = Math.floor(i % self.boxesPerRow) * self.boxSize + self.width * columnadjuster / self.maxAcross;
                    d.y = Math.floor(i / self.boxesPerRow) * self.boxSize + self.height * rowadjuster / self.rows;
                    obj.x = self.width * columnadjuster / self.maxAcross;
                    obj.y = self.height * rowadjuster / self.rows - self.boxSize / 2;
                });
                self.labels.push(obj);
            });

            var highest = 0;
            self.data.forEach(function(d) {
                if (d.y > highest) {
                    highest = d.y;
                }
            });
            self.height = highest + self.boxSize;
            self.totalHeightObj[category] = self.height;
        } else {

            keys.forEach(function(item, index) {
                var obj = {
                    name: item + " (" + nest[item].length + ")"
                };

                nest[item].forEach(function(d, i) {
                    d.smallpositionX = Math.floor(i % self.boxesPerRow) * self.boxSize;
                    d.smallpositionY = Math.floor(i / self.boxesPerRow) * self.boxSize;
                    d.masterpositionX = self.width * index / self.categoryLength;
                    d.masterpositionY = 0;
                    d.x = Math.floor(i % self.boxesPerRow) * self.boxSize + self.width * index / self.categoryLength;
                    d.y = Math.floor(i / self.boxesPerRow) * self.boxSize;
                    obj.x = self.width * index / self.categoryLength;
                    obj.y = 0 - self.boxSize / 2;
                });
                self.labels.push(obj);
            });

            var highest = 0;
            self.data.forEach(function(d) {
                if (d.y > highest) {
                    highest = d.y;
                }
            });
            self.height = highest + self.boxSize;
            self.totalHeightObj[category] = self.height;
        }
        self.trigger("setPositions:end");
    },
    render: function render() {
        var self = this;
        self.trigger("renderChart:start");

        self.buttonColumns.forEach(function(d) {
            self.setPositions(d);
        });

        self.setPositions(self.category);

        self.masterHeight = 0;
        _.each(self.totalHeightObj, function(value, key) {
            if (value > self.masterHeight) {
                self.masterHeight = value;
            }
        });

        self.svg = d3.select(self.chartId).append("svg").attr("width", self.width + self.margin.left + self.margin.right).attr("height", self.masterHeight + self.margin.top + self.margin.top).append("g").attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

        self.labels = self.svg.selectAll(".back-labels").data(self.labels, function(d) {
            return d.name;
        }).enter().append("text").attr("x", function(d) {
            return d.x;
        }).attr("y", function(d) {
            return d.y;
        }).text(function(d) {
            return d.name;
        }).attr("class", "back-labels");

        self.addRects = self.svg.selectAll("rect").data(self.data, function(d) {
            return d[self.idField];
        }).enter().append("rect").attr("width", self.boxSize).attr("height", self.boxSize).attr("x", function(d) {
            return d.x;
        }).attr("y", function(d) {
            return d.y;
        }).attr("fill", function(d) {
            return self.color(d[self.colorField]);
        }).attr("id", function(d) {
            return d.title.replace(/ /g, '');
        }).attr('class', "sortingSquares").attr("title", function(d) {
            return self.tooltipTemplate({
                d: d
            });
        });

        self.addRects.each(function(d) {
            if (d.title == "La La Land") {
                self.lalaX = d.x;
                self.lalaY = d.y;
            }
        });

        self.addLaLine = self.svg.append("line").attr({
            x1: self.lalaX + 12.5,
            x2: self.lalaX + 12.5,
            y1: self.lalaY + 25,
            y2: self.lalaY + 32
        }).attr("class", "la-la-line");

        self.addLaText = self.svg.append("text").attr({
            x: self.lalaX,
            y: self.lalaY + 45
        }).text("La La Land").attr("class", "la-la-text");

        self.$('.sortingSquares').tooltip({
            html: true,
            trigger: "hover",
            placement: function placement(tooltip, element) {
                var cx = $(element).attr("x");
                var svgCenter = self.width / 2;
                if (cx < svgCenter) {
                    return 'right';
                }
                return 'left';
            }
        });

        $(window).on("resize", _.debounce(function(event) {
            self.newWidth = self.$(self.chartId).width() - self.margin.left - self.margin.right;
            if (self.newWidth == self.width || self.newWidth <= 0) {
                return;
            }

            self.width = self.newWidth;

            self.update();
        }, 100));

        self.$(".chart-nav .btn").on("click", function() {
            self.category = $(this).attr("dataid");
            self.update();
        });
        self.trigger("renderChart:end");
    },
    update: function update() {
        var self = this;
        self.trigger("update:start");

        self.buttonColumns.forEach(function(d) {
            self.setPositions(d);
        });
        self.masterHeight = 0;
        _.each(self.totalHeightObj, function(value, key) {
            if (value > self.masterHeight) {
                self.masterHeight = value;
            }
        });

        self.setPositions(self.category);

        d3.select(self.chartId).selectAll("svg").transition().duration(1000).attr("width", self.width + self.margin.left + self.margin.right).attr("height", self.masterHeight + self.margin.top + self.margin.top);

        self.addRects.each(function(d) {
            if (d.title == "La La Land") {
                self.lalaX = d.x;
                self.lalaY = d.y;
            }
        });

        self.addRects.transition().duration(1000).attr("x", function(d) {
            return d.x;
        }).attr("y", function(d) {
            return d.y;
        });

        self.svg.selectAll(".back-labels").data(self.labels, function(d) {
            return d.name;
        }).enter().append('text').attr("x", function(d) {
            return d.x;
        }).attr("y", function(d) {
            return d.y;
        }).text(function(d) {
            return d.name;
        }).attr("class", "back-labels").style("opacity", 0);

        self.svg.selectAll(".back-labels").transition().duration(1000).attr("x", function(d) {
            return d.x;
        }).attr("y", function(d) {
            return d.y;
        }).text(function(d) {
            return d.name;
        }).style("opacity", 1);

        self.svg.selectAll(".back-labels").data(self.labels, function(d) {
            return d.name;
        }).exit().transition().duration(1000).style("opacity", 0).remove();

        self.addLaLine.transition().duration(1000).attr({
            x1: self.lalaX + 12.5,
            x2: self.lalaX + 12.5,
            y1: self.lalaY + 25,
            y2: self.lalaY + 32
        });

        self.addLaText.transition().duration(1000).attr({
            x: self.lalaX,
            y: self.lalaY + 45
        });

        self.trigger("update:end");
    }

});
//# sourceMappingURL=sortsquares.js.map


$(".main").html(Reuters.Graphics.Template.basicChartLayout());

Reuters.Graphics.sortingGraphic = new Reuters.Graphics.SortingSquares({
    el: "#reutersGraphic-chart1",
    //dataURL: '//d3sl9l9bcxfb5q.cloudfront.net/json/mo-egypt-judges-amada',
    dataURL: "data/mostwinsdata_v2.json",
    buttonColumns: ["noms"],
    buttonText: ["nominations"],
    initialSort: "noms",
    colorField: "wins",
    /*
    			colorDomain:["Islamic State","Domestic anti-government"],
    			colorRange:[rose4,tangerine2,tangerine4],
    */
    maxAcross: 1,
    boxSize: 25,
    legendtemplate: Reuters.Graphics.Template.SortingSquareLegend,
    tooltipTemplate: Reuters.Graphics.Template.SortingSquaresTooltip
});

Reuters.Graphics.sortingGraphic.on("renderChart:start", function(evt) {
    var self = this;
});
Reuters.Graphics.sortingGraphic.on("renderChart:end", function(evt) {
    var self = this;
});
Reuters.Graphics.sortingGraphic.on("update:start", function(evt) {
    var self = this;
});
Reuters.Graphics.sortingGraphic.on("update:end", function(evt) {
    var self = this;
});
Reuters.Graphics.sortingGraphic.on("setPositions:start", function(evt) {
    var self = this;
});
Reuters.Graphics.sortingGraphic.on("setPositions:end", function(evt) {
    var self = this;
});
//# sourceMappingURL=main.js.map
