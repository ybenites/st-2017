$ = require('jquery');
jQuery = $;
global.jQuery = jQuery;
global.Tether = require('tether');

tipsy = require('./libraries/jquery.tipsy');
Hammer = require('hammerjs');
require('./libraries/chosen.jquery.min');

d3 = require('d3');
Backbone = require('backbone');
_ = require('underscore');


require('bootstrap');
moment = require('moment');
