//for translations.
window.gettext = function(text){
    return text;
}

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = gettext('en');
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url ||  ''; 


// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = (hashBased) => {
    let query;
    if(hashBased) {
        let pos = location.href.indexOf('?');
        if(pos == -1) return [];
        query = location.href.substr(pos+1);
    } else {
        query = location.search.substr(1);
    }
    let result = {};
    query.split('&').forEach(function(part) {
        if(!part) return;
        part = part.split('+').join(' '); // replace every + with space, regexp-free version
        let eq = part.indexOf('=');
        let key = eq > -1 ? part.substr(0,eq) : part;
        let val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';
        
        //convert true / false to booleans.
        if(val == 'false'){
            val = false;
        }else if(val == 'true'){
            val = true;
        }

        let f = key.indexOf('[');
        if(f == -1){ 
            result[decodeURIComponent(key)] = val;
        }else {
            var to = key.indexOf(']');
            var index = decodeURIComponent(key.substring(f + 1,to));
            key = decodeURIComponent(key.substring(0,f));
            if(!result[key]){
                 result[key] = [];
            }
            if(!index){
                result[key].push(val);
            }else{
                result[key][index] = val;
            }
        }
    });
    return result;
}

Reuters.trackEvent = (category, type, id) => {
    category = category || 'Page click';
    //console.log(category, type, id);
    let typeString = type;
    if(id){
        typeString += ': ' + id;
    }
    let gaOpts = {
        'nonInteraction': false,
        'page': PAGE_TO_TRACK
    };
    
    
    ga('send', 'event', 'Default', category, typeString, gaOpts);
}


Reuters.generateSliders = () => {
    $('[data-slider]').each(function(){
        let $el = $(this);
        let getPropArray = (value) => {
            if(!value){
                return [0];
            }
            let out = [];
            let values = value.split(',');
            values.forEach((value) =>{
                out.push(parseFloat(value));    
            });
            return out;
        };
        let pips = undefined;
        let start = getPropArray($el.attr('data-start'));
        let min = getPropArray($el.attr('data-min'));
        let max = getPropArray($el.attr('data-max'));
        let orientation = $el.attr('data-orientation') || 'horizontal';
        let step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
        let tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
        let connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
        let snap = $el.attr('data-snap') === 'true' ? true : false;
        let pipMode = $el.attr('data-pip-mode');
        let pipValues = $el.attr('data-pip-values') ? getPropArray( $el.attr('data-pip-values')) : undefined;
        let pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
        let pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
        if(pipMode === 'count'){
            pipValues = pipValues[0];
        }

        if(pipMode){
            pips = {
                mode: pipMode,
                values: pipValues,
                stepped: pipStepped,
                density: pipDensity
            }
        }
        

        if(connect){
            let cs = [];
            connect.split(',').forEach((c) =>{
                c = c === 'true' ? true : false;
                cs.push(c);
            });
            connect = cs;
        }
        
        noUiSlider.create(this, {
            start: start,
            range: {
                min: min,
                max: max
            },
            snap: snap,
            orientation: orientation,
            step: step,
            tooltips: tooltips,
            connect: connect,
            pips: pips
        });
        //This probably doesn't belong here, but will fix the most common use-case.
        $(this).find('div.noUi-marker-large:last').addClass('last');
        $(this).find('div.noUi-marker-large:first').addClass('first');
    });
}


Reuters.hasPym = false;
try {
	Reuters.pymChild = new pym.Child({polling:500});
	if (Reuters.pymChild.id) {
		Reuters.hasPym = true;
		$("body").addClass("pym");
	}
}
catch(err){	
}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl()
if (Reuters.Graphics.Parameters.media){
    $("html").addClass("media-flat")
}
if (Reuters.Graphics.Parameters.eikon){
    $("html").addClass("eikon")
}
if (Reuters.Graphics.Parameters.header == "no"){
    $("html").addClass("remove-header")
}