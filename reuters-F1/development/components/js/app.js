this["Reuters"] = this["Reuters"] || {};
this["Reuters"]["Template"] = this["Reuters"]["Template"] || {};

this["Reuters"]["Template"]["gridDriver"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {
    __p += '<div class=\'row box-row\'>\n\t';

    var self = this;
    data.forEach(function(d) {;
      __p += '\n\t<div class="col-ms-6 col-lg-4 box-layout">\n\t\t<div class="border-box">\n\t\t\t<div class="row">\n\t\t\t\t<div class="col-xs-6">\n\t\t\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.lastName)) == null ? '' : __t) +
        '.png">\n\t\t\t\t</div>\n\t\t\t\t<div class="driver-box-details col-xs-6 text-gray">\n\t\t\t\t\t\t<div class="small-display">\n\t\t\t\t\t\t\t';

      print(self.makeRank(d.stats.rank));
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="large-display">' +
        ((__t = (d.firstName)) == null ? '' : __t) +
        ' <br> ' +
        ((__t = (d.lastName)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t\t<div class="small-display">' +
        ((__t = (d.team)) == null ? '' : __t) +
        '</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t';
    });
    __p += '\n</div>\n<div class=\'grid-info col-xs-12\'></div>';

  }
  return __p
};

this["Reuters"]["Template"]["gridDriverOpen"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {
    __p += '<div class="row">\n\t<div class="col-ms-10 col-ms-offset-1">\n\t\t<div class="row row-content">\n\t\t\t<div>\n\t\t\t\t<div class="col-xs-3 hidden-xs">\n\t\t\t\t\t<div class="driver-page driver-head">\n\t\t\t\t\t\t<img class="img-responsive" src="images/img/' +
      ((__t = (data.lastName)) == null ? '' : __t) +
      '.png">\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xs-6">\n\t\t\t\t\t<div class="small-display">\n\t\t\t\t\t\t';

    var self = this;
    print(self.makeRank(data.stats.rank));
    __p += '\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="large-display">' +
      ((__t = (data.firstName)) == null ? '' : __t) +
      ' ' +
      ((__t = (data.lastName)) == null ? '' : __t) +
      '</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xs-2">\n\t\t\t\t\t<div class="driver-point-holder">\n\t\t\t\t\t\t<div class="small-display">points</div>\n\t\t\t\t\t\t<div class="large-display">' +
      ((__t = (data.stats.points)) == null ? '' : __t) +
      '</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\t\t\t\t\t\t\t\n\t\t<div class="row  slide-open-item">\n\t\t\t<div class="col-ms-9 col-ms-offset-3">\n\t\t\t\t<div class="row">\n\t\t\t\t<div class="col-xs-8 driver-drop-table">\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">AGE</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';

    var parseBirthDate = d3.time.format("%Y-%m-%d").parse
    var birthday = parseBirthDate(data.birth)
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    var driverAge = Math.abs(ageDate.getUTCFullYear() - 1970);
    print(driverAge);
    __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">COUNTRY</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">' +
      ((__t = (data.birthCountry)) == null ? '' : __t) +
      '</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">TEAM</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">' +
      ((__t = (data.team)) == null ? '' : __t) +
      '</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">FINISH POSITION AVG</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';


    if (data.stats.finish_position_avg) {
      print(self.statFormat(data.stats.finish_position_avg))
    } else {
      print('N/A')
    };
    __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">START POSITION AVG</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';

    if (data.stats.start_to_finish_avg) {
      print(self.statFormat(data.stats.start_to_finish_avg))
    } else {
      print('N/A')
    };
    __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">POLES</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';

    if (data.stats.poles) {
      print(data.stats.poles)
    } else {
      print('0')
    };
    __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xs-4 driverChartHolder">\n\t\t\t\t\t<div class="small-display points-per">Points per race</div>\n\t\t\t\t\t<div id="drop-drivers-chart"</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>';

  }
  return __p
};

this["Reuters"]["Template"]["gridTeam"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {
    __p += '<div class="row box-row">\n\t';

    var self = this;
    data.forEach(function(d) {;
      __p += '\n\t<div class="col-ms-6 col-md-4 box-layout">\n\t\t<div class="border-box">\n\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.picId)) == null ? '' : __t) +
        '">\n\t\t\t<div class="team-box-details text-center">\n\t\t\t\t<div class="row">\n\t\t\t\t\t<div class="col-xs-12 small-display">\n\t\t\t\t\t\t';

      print(self.makeRank(d.teamRank));
      __p += '\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-xs-12 large-display">' +
        ((__t = (d.name)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t<div class="col-xs-12 small-display">' +
        ((__t = (d.teamPoints)) == null ? '' : __t) +
        ' POINTS</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t';
    });
    __p += '\n</div>\n<div class="grid-info col-xs-12"></div>';

  }
  return __p
};

this["Reuters"]["Template"]["gridTeamOpen"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {
    __p += '\t<img class="img-responsive hidden-xs" src="images/img/' +
      ((__t = (data.picId)) == null ? '' : __t) +
      '">\n\n\n\t<div class="row">\n\t\t<div class="col-ms-10 col-ms-offset-1">\n\t\t\t<div class="row">\n\t\t\t\t<div class="col-xs-4 col-ms-3">\n\t\t\t\t\t<div class="team-name-holder">\n\t\t\t\t\t\t<div class="small-display">\n\t\t\t\t\t\t\t';

    var self = this;
    print(self.makeRank(data.teamRank));
    __p += '\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="large-display">' +
      ((__t = (data.name)) == null ? '' : __t) +
      '</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-ms-7 col-xs-5">\n\t\t\t\t\t';

    data.drivers.forEach(function(d) {
      print(Reuters.Template.slideDownTeamDrivers({
        data: d
      }));
    });
    __p += '\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xs-2">\n\t\t\t\t\t<div class="large-display text-center">' +
      ((__t = (data.teamPoints)) == null ? '' : __t) +
      '</div>\n\t\t\t\t\t<div class="small-display text-center">points</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="';
    if (d.teamRank == 0) {
      print('hide-result')
    };
    __p += '">\n\t\t\t\t<hr>\n\t\t\t\t<br>\n\t\t\t\t<div class="small-display">SEASON TO DATE</div>\n\t\t\t\t<div class="row chart-key">\n\t\t\n\t\t\t\t\t<div class="col-xs-7 col-sm-5 col-md-4">\n\t\t\t\t\t\t<div> <div class="team-key"></div> ' +
      ((__t = (d.name)) == null ? '' : __t) +
      ' <em>(Hover for details)</em></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-xs-5 col-sm-5 col-md-4">\t\t\t\t\n\t\t\t\t\t\t<div> <div class="other-key"></div>Other teams</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\n\t\t\t\t<div id="drop-teams-chart" class="team-chart chart col-ms-12"></div>\t\t</div>\n\t\t\t</div>\n\t</div>\n\n\n\n';

  }
  return __p
};

this["Reuters"]["Template"]["gridTrack"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {
    __p += '<div class=\'row box-row\'>\n\t';

    var self = this;
    data.forEach(function(d) {;
      __p += '\n\t<div class="col-xs-6 col-ms-3 col-md-3 box-layout">\n\t\t<div class="border-box">\n\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.picid)) == null ? '' : __t) +
        '.png">\n\t\t\t<div class="team-box-details">\n\t\t\t\t<div class="row">\n\t\t\t\t\t<div class="col-xs-12 small-display">\n\t\t\t\t\t\t\t';

      print(self.calendarDate(self.parseEventDate(d.date.substring(0, 10))));
      __p += '\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-xs-12 large-display smaller track-grid-name"> ' +
        ((__t = (d.formatname)) == null ? '' : __t) +
        ' </div>\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n\t';
    });
    __p += '\n</div>\n<div class=\'grid-info col-xs-12\'></div>';

  }
  return __p
};

this["Reuters"]["Template"]["slideDownDriver"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {


    var self = this;

    data.forEach(function(d) {;
      __p += '\n<div class="slide-down-item">\n\t<div class="row row-content">\n\t\t<div>\n\t\t\t<div class="col-xs-2">\n\t\t\t\t<div class="arrow-button hidden-xs"></div>\n\t\t\t\t<div class="driver-point-holder">\n\t\t\t\t\t<div class="small-display hidden-xs">points</div>\n\t\t\t\t\t<div class="large-display hidden-xs">' +
        ((__t = (d.stats.points)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t<div class="small-display visible-xs">\n\t\t\t\t\t\t';

      print(self.makeRank(d.stats.rank).replace(/place/g, ''));
      __p += '\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="col-xs-3">\n\t\t\t\t<div class="driver-page driver-head">\n\t\t\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.lastName)) == null ? '' : __t) +
        '.png">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="col-xs-6 col-xs-offset-1 col-ms-7 col-ms-offset-3 col-sm-offset-0">\n\t\t\t\t<div class="small-display hidden-xs">\n\t\t\t\t\t';

      print(self.makeRank(d.stats.rank));
      __p += '\n\t\t\t\t</div>\n\t\t\t\t<div class="large-display hidden-xs">' +
        ((__t = (d.firstName)) == null ? '' : __t) +
        ' ' +
        ((__t = (d.lastName)) == null ? '' : __t) +
        '</div>\n\t\t\t\t<div class="large-display smaller visible-xs">';
      print(d.firstName.substring(0, 1) + ".");
      __p += ' ' +
        ((__t = (d.lastName)) == null ? '' : __t) +
        '</div>\n\t\t\t\t<div class="small-display visible-xs">' +
        ((__t = (d.stats.points)) == null ? '' : __t) +
        ' points</div>\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t\t\t\t\t\t\n\t<div class="row  slide-open-item">\n\t\t<div class="col-ms-7 col-ms-offset-5">\n\t\t\t<div class="row">\n\t\t\t\t<div class="col-ms-8 driver-drop-table">\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">AGE</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';

      var parseBirthDate = d3.time.format("%Y-%m-%d").parse
      var birthday = parseBirthDate(d.birth)
      var ageDifMs = Date.now() - birthday.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      var driverAge = Math.abs(ageDate.getUTCFullYear() - 1970);
      print(driverAge);
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">COUNTRY</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">' +
        ((__t = (d.birthCountry)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">TEAM</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">' +
        ((__t = (d.team)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">FINISH POSITION AVG</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';

      if (d.stats.finish_position_avg) {
        print(self.statFormat(d.stats.finish_position_avg))
      } else {
        print('N/A')
      };
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-xs-6 small-display">POLES</div>\n\t\t\t\t\t\t<div class="col-xs-6 text-right">\n\t\t\t\t\t\t\t';

      if (d.stats.poles) {
        print(d.stats.poles)
      } else {
        print('0')
      };
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-ms-4 driverChartHolder hidden-xs">\n\t\t\t\t\t<div class="small-display points-per">Points per race</div>\n\t\t\t\t\t<div id="drivers-chart-' +
        ((__t = (d.index)) == null ? '' : __t) +
        '"</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="visible-xs driverChartHolderSmall">\n\t\t\t\t<div class="small-display points-per">Points per race</div>\n\t\t\t\t<br>\n\t\t\t\t<div id="drivers-chart-' +
        ((__t = (d.index)) == null ? '' : __t) +
        '-small"</div>\n\t\t\t</div>\n\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';
    });


  }
  return __p
};

this["Reuters"]["Template"]["slideDownResult"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {


    var self = this
    data.forEach(function(d) {

      ;
      __p += '\n<div class="slide-down-item">\n\t<div class="row row-content">\n\t\t<div class="col-xs-4 col-ms-3">\n\t\t\t<div class="arrow-button"></div>\n\t\t\t<div class="team-name-holder">\n\t\t\t\t<div class="large-display">\n\t\t\t\t\t';

      print(self.calendarDate(self.parseEventDate(d.date.substring(0, 10))));
      __p += '\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class="col-ms-6 col-xs-8">\n\t\t\t<div class="large-display"> ' +
        ((__t = (d.name)) == null ? '' : __t) +
        ' </div>\n\t\t\t<div class="text-gray"> ' +
        ((__t = (d.venueCity)) == null ? '' : __t) +
        ', ' +
        ((__t = (d.venueCountry)) == null ? '' : __t) +
        ' </div>\n\n\t\t</div>\n\t\t<div class="hidden-xs col-ms-3 small-display ">\n\t\t\t<div class="driver-head">\n\t\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.winner.driver.lastName)) == null ? '' : __t) +
        '.png">\n\t\t\t</div>\n\t\t\t<div class="driver-info-box hidden-xs">\n\t\t\t\t<br>\n\t\t\t\t<div class="highlight-text">WINNER</div>\n\t\t\t\t<div >' +
        ((__t = (d.winner.driver.firstName)) == null ? '' : __t) +
        ' ' +
        ((__t = (d.winner.driver.lastName)) == null ? '' : __t) +
        '</div>\n\t\t\t\t<div >' +
        ((__t = (d.winner.driver.team)) == null ? '' : __t) +
        '</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\t\t\t\t\t\t\t\n\t\t';

      var html = Reuters.Template.slideOpenResult({
        d: d,
        self: self
      });
      print(html);
      __p += '\n</div>\n';
    });


  }
  return __p
};

this["Reuters"]["Template"]["slideDownSked"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {


    var self = this;
    var winner = ""
    data.forEach(function(d, i) {;
      __p += '\n<div class="slide-down-item">\n\t\t<div class="row-content">\n\t\t\t<div class="row track-table-item">\n\t\t\t\t<div class="hidden-xs col-ms-3">\n\t\t\t\t\t<div class="arrow-button"></div>\n\n\t\t\t\t\t<div class="team-name-holder">\n\t\t\t\t\t\t<div class="large-display cal-small">\n\t\t\t\t\t\t\t';

      print(self.calendarDate(self.parseEventDate(d.date.substring(0, 10))));
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="small-display">\n\t\t\t\t\t\t\t';

      var splitDate = d.date.split("T")
      var time = parseFloat(splitDate[1].substring(0, 2))
      var formattedTime = time + " A.M."
      if (time > 12) {
        formattedTime = (time - 12) + " P.M."
      }
      if (time == 12) {
        formattedTime = time + " P.M."
      }
      if (i == 0) {
        print(formattedTime + " GMT")
      } else {
        print(formattedTime)
      }

      ;
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-ms-1 hidden-xs">\n\t\t\t\t\t<img class="sked-flag" src="images/img/' +
        ((__t = (d.venueCountry)) == null ? '' : __t) +
        '-flag.png">\n\t\t\t\t</div>\n\t\t\t\t\n\t\t\t\t<div class="col-ms-6 col-xs-12">\n\t\t\t\t\t<div class="small-display visible-xs">\n\t\t\t\t\t\t\t';

      print(self.calendarDate(self.parseEventDate(d.date.substring(0, 10))));
      __p += '\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="large-display hidden-xs cal-small"> ' +
        ((__t = (d.formatname)) == null ? '' : __t) +
        ' </div>\n\t\t\t\t\t<div class="large-display smaller visible-xs"> ' +
        ((__t = (d.formatname)) == null ? '' : __t) +
        ' </div>\n\t\t\t\t\t<div class="text-gray hidden-xs"> ' +
        ((__t = (d.dateline)) == null ? '' : __t) +
        ' </div>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xs-3 col-ms-2 hidden-xs">\n\t\t\t\t\t';

      if (d.status == "scheduled") {
        /* ;
__p += '\n\t\t\t\t\t\t\t<div class="small-display">Track</div>\n\t\t\t\t\t\t';
 */
      } else {;
        __p += '\n\t\t\t\t\t\t\t<div class="small-display">WINNER</div>\n\t\t\t\t\t\t\t<div class="text-gray text-uppercase"><span class="hidden-ms">';
        print(d.winnerFirstName.substring(0, 1) + ".");
        __p += ' </span> ' +
          ((__t = (d.winnerLastName)) == null ? '' : __t) +
          '</div>\n\t\t\t\t\t\t\t<div class="text-gray tight-led">' +
          ((__t = (d.winnerTeam)) == null ? '' : __t) +
          '</div>\n\t\t\t\t\t\t';
      };
      __p += '\n\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t';

      if (d.status != "scheduled") {;
        __p += '\n\t\t\t\t<div class="row visible-xs">\t\t\t\t\t\t';

        self.fullData.recaps.forEach(function(recap) {
          if (recap.name == d.name) {
            winner = recap.drivers[0]
          }
        });
        __p += '\n\t\t\t\t\t<div class="col-xs-4">\n\t\t\t\t\t\t<div class="result-driver-head">\n\t\t\t\t\t\t\t<img class="img-responsive" src="images/img/' +
          ((__t = (winner.driver.lastName)) == null ? '' : __t) +
          '.png">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-xs-8 text-gray">\n\t\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t\t<div class="col-xs-6 small-display">WINNER</div>\n\t\t\t\t\t\t\t<div class="col-xs-6">' +
          ((__t = (winner.time)) == null ? '' : __t) +
          ' </div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t\t<div class="col-xs-12 text-gray">\n\t\t\t\t\t\t\t\t<strong>\n\t\t\t\t\t\t\t\t\t';

        var firstname = winner.driver.firstName.substring(0, 1)
        print(firstname + ". ");
        __p += ' \n\t\t\t\t\t\t\t\t ' +
          ((__t = (winner.driver.lastName)) == null ? '' : __t) +
          '\n\t\t\t\t\t\t\t\t </strong><br>\n\t\t\t\t\t\t\t\t' +
          ((__t = (winner.driver.team)) == null ? '' : __t) +
          '\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t</div>\n\t\t\t';
      };
      __p += '\n\t\t</div>\n\t\t\n\t\t';

      if (d.status == "scheduled") {
        /* var html = Reuters.Template.slideOpenTrack({d:d, self:self});
        	print(html)	*/
      } else {
        self.fullData.recaps.forEach(function(recap) {
          if (recap.name == d.name) {
            var html = Reuters.Template.slideOpenResult({
              d: recap,
              self: self
            });
            print(html)
          }
        })
      };
      __p += '\n\n</div>\n';
    });


  }
  return __p
};

this["Reuters"]["Template"]["slideDownTeam"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {


    var self = this;
    data.forEach(function(d) {;
      __p += '\n<div class="slide-down-item">\n\t<div class="row  slide-open-item above hidden-xs">\n\t\t<div class="col-xs-12">\n\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.picId)) == null ? '' : __t) +
        '">\n\t\t</div>\n\t</div>\n\t<div class="row visible-xs">\n\t\t<div class="col-xs-12">\n\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.picId)) == null ? '' : __t) +
        '">\n\t\t</div>\n\t</div>\n\n\t<div class="row">\n\t\t<div class="col-ms-offset-1 col-ms-10 row-content">\n\t\t\t<div class="row">\n\t\t\t\t<div class="col-xs-8 col-ms-3">\n\t\t\t\t\t<div class="arrow-button"></div>\n\t\t\t\t\t<div class="team-name-holder">\n\t\t\t\t\t\t<div class="small-display">\n\t\t\t\t\t\t\t';

      print(self.makeRank(d.teamRank));
      __p += '\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="large-display">' +
        ((__t = (d.name)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-ms-7 hidden-xs teamDriverSection">\n\t\t\t\t\t<div class="car-holder ">\n\t\t\t\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.picId)) == null ? '' : __t) +
        '">\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t\t';

      d.drivers.forEach(function(d) {
        print(Reuters.Template.slideDownTeamDrivers({
          data: d
        }));
      });
      __p += '\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xs-4 col-ms-2">\n\t\t\t\t\t<div class="large-display text-center">' +
        ((__t = (d.teamPoints)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t<div class="small-display text-center">points</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<div class="row  slide-open-item">\n\t\t<div class="col-ms-10 col-ms-offset-1">\n\t\t\t<div class="driver-down visible-xs">\n\t\t\t\t\t';

      d.drivers.forEach(function(d) {
        print(Reuters.Template.slideDownTeamDrivers({
          data: d
        }));
      });
      __p += '\t\t\t\t\n\t\t\t</div>\n\n\t\t\t<div class="';
      if (d.teamRank == 0) {
        print('hide-result')
      };
      __p += '">\n\n\t\t\t\t<hr class="hidden-xs">\n\t\t\t\t<br>\n\t\t\t\t<div class="small-display">SEASON TO DATE</div>\n\t\t\t\t<div class="row chart-key">\n\t\t\n\t\t\t\t\t<div class="col-xs-7 col-sm-5 col-md-4">\n\t\t\t\t\t\t<div> <div class="team-key"></div> ' +
        ((__t = (d.name)) == null ? '' : __t) +
        ' <em>(Hover for details)</em></div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-xs-5 col-sm-5 col-md-4">\t\t\t\t\n\t\t\t\t\t\t<div> <div class="other-key"></div>Other teams</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\n\t\t\t\t<div class="team-chart chart" data-id="' +
        ((__t = (d.name)) == null ? '' : __t) +
        '" id="teams-chart-' +
        ((__t = (d.index)) == null ? '' : __t) +
        '"></div>\t\t\t\n\t\t\t</div>\n\t\t</div>\n\n\t</div>\n</div>\n';
    });


  }
  return __p
};

this["Reuters"]["Template"]["slideDownTeamDrivers"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape;
  with(obj) {
    __p += '<div class="col-xs-6 driver-holders hidden-xs">\t\t\t\t\t\t\t\t\n\t<div class="driver-head">\n\t\t<img class="img-responsive" src="images/img/' +
      ((__t = (data.lastName)) == null ? '' : __t) +
      '.png">\n\t</div>\n\t<div class="driver-info-box">\n\t\t<div class="driver-name small-display">' +
      ((__t = (data.firstName)) == null ? '' : __t) +
      '<br>' +
      ((__t = (data.lastName)) == null ? '' : __t) +
      '</div>\n\t\t<div class="driver-points">' +
      ((__t = (data.driverInfo.stats.points)) == null ? '' : __t) +
      '</div>\n\t</div>\n</div>\n<div class="visible-xs">' +
      ((__t = (data.firstName)) == null ? '' : __t) +
      ' ' +
      ((__t = (data.lastName)) == null ? '' : __t) +
      ' | <span class="small-display">' +
      ((__t = (data.driverInfo.stats.points)) == null ? '' : __t) +
      ' POINTS </span></div>\n';

  }
  return __p
};

this["Reuters"]["Template"]["slideDownTrack"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {


    var self = this
    data.forEach(function(d) {

      ;
      __p += '\n<div class="slide-down-item">\n\t<div class="row row-content">\n\t\t<div class="col-xs-12 col-ms-8 col-md-6 ">\n\t\t\t<div class="arrow-button hidden-xs"></div>\n\t\t\t<div class="team-name-holder">\n\t\t\t\t<div class="small-display">\n\t\t\t\t\t';

      print(self.calendarDate(self.parseEventDate(d.date.substring(0, 10))));
      __p += '\n\t\t\t\t</div>\n\t\t\t\t<div class="large-display hidden-xs">' +
        ((__t = (d.formatname)) == null ? '' : __t) +
        '</div>\n\t\t\t\t<div class="large-display smaller visible-xs">' +
        ((__t = (d.formatname)) == null ? '' : __t) +
        '</div>\n\n\t\t\t\t<div class="text-gray"> ' +
        ((__t = (d.dateline)) == null ? '' : __t) +
        '</div>\n\t\t\t</div>\n\t\t</div>\t\n\t\t<div class="col-xs-3 hidden-xs">\n\t\t\t<div class="track-page driver-head">\n\t\t\t\t<img class="img-responsive" src="images/img/' +
        ((__t = (d.picid)) == null ? '' : __t) +
        '.png">\n\t\t\t</div>\n\t\t</div>\t\n\n\t\t';
      /*if (d.winnerLastName){;
__p += '\n\t\t<div class="col-ms-2 col-ms-offset-1 driver-holders hidden-xs">\n\t\t\t\t<div class="highlight-text">WINNER</div>\n\t\t\t\t<div >' +
((__t = ( d.winnerFirstName )) == null ? '' : __t) +
' ' +
((__t = ( d.winnerLastName )) == null ? '' : __t) +
'</div>\n\t\t\t\t<div >' +
((__t = ( d.winnerTeam )) == null ? '' : __t) +
'</div>\n\t\t</div>\t\t\t\n\n\t\t';
} */
      ;
      __p += '\t\n\n\t</div>\t\t\t\t\t\t\t\n\t\t';

      var html = Reuters.Template.slideOpenTrack({
        d: d,
        self: self,
        isgrid: false
      });
      print(html);
      __p += '\n</div>\n';
    });


  }
  return __p
};

this["Reuters"]["Template"]["slideOpenResult"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {
    __p += '\t\n\t<div class="row  slide-open-item">\n\t\t<div class="col-ms-10 col-ms-offset-1">\n\t\t\t<div class="row small-display">\n\t\t\t\t<div class="col-xs-2 col-md-1 col-md-offset-1 hidden-xs">Place</div>\n\t\t\t\t<div class="col-xs-6 col-ms-5 col-lg-4 hidden-xs">Driver | Team</div>\n\t\t\t\t<div class="col-xs-4 col-ms-2 hidden-xs">Time</div>\n\t\t\t\t<div class="col-ms-3 hidden-xs ">Margin</div>\n\t\t\t</div>\n\t\t\t\n\t\t\t';

    var barscale = d3.scale.linear()
      .domain([0, d3.max(d.drivers, function(item) {
        return item.margin
      })])
      .range([0, 100])

    d.drivers.forEach(function(item, index) {;
      __p += '\n\t\t\t\n\t\t\t\t<div class="row result-row ';
      if (index == 0) {
        print('winner_row')
      };
      __p += '">\n\t\t\t\t\t<div class="col-xs-2 col-md-1 col-md-offset-1 small-display">\n\t\t\t\t\t\t<div class="recap-position">\n\t\t\t\t\t\t';

      if (item.racestats.finish_position) {
        print(self.makeRank(item.racestats.finish_position).replace(/place/g, ''))
      } else {
        print("-")
      };
      __p += '\n\t\t\t\t\t\t</div>\t\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-xs-6 col-ms-5 col-lg-4">\n\t\t\t\t\t\t<strong class="visible-xs">\n\t\t\t\t\t\t';

      var firstname = item.driver.firstName.substring(0, 1)
      print(firstname + ". ");
      __p += ' \n\t\t\t\t\t\t' +
        ((__t = (item.driver.lastName)) == null ? '' : __t) +
        ' </strong>\n\n\t\t\t\t\t\t<span class="hidden-xs">\n\t\t\t\t\t\t';

      var firstname = item.driver.firstName.substring(0, 1)
      print(firstname + ". ");
      __p += ' \n\t\t\t\t\t\t' +
        ((__t = (item.driver.lastName)) == null ? '' : __t) +
        ' </span>\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t<span class="hidden-xs">|</span> ' +
        ((__t = (item.driver.team)) == null ? '' : __t) +
        '</div>\n\t\t\t\t\t<div class="col-xs-4 col-ms-2">\t\t\t\t\t\t\t\n\t\t\t\t\t\t';

      if (item.time) {
        print(item.time)
      } else {
        print("-")
      };
      __p += '\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-ms-3 hidden-xs text-right">\n\t\t\t\t\t\t';

      if (isNaN(item.margin) == false) {;
        __p += '\n\t\t\t\t\t\t\t<div class="margin-bar" style="width:';
        print(barscale(item.margin));
        __p += '%;" title="<div class=\'tip2\'>Margin<br> ' +
          ((__t = (item.margin)) == null ? '' : __t) +
          ' seconds</div>"></div>\t\t\n\t\t\t\t\t\t';
      };
      __p += '\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\n\t\t\t\t</div>\n\t\t\t\n\t\t\t\n\t\t\t';
    });
    __p += '\t\t\t\n\n\t\t</div>\n\t</div>\n';

  }
  return __p
};

this["Reuters"]["Template"]["slideOpenTrack"] = function(obj) {
  obj || (obj = {});
  var __t, __p = '',
    __e = _.escape,
    __j = Array.prototype.join;

  function print() {
    __p += __j.call(arguments, '')
  }
  with(obj) {

    if (isgrid == true) {;
      __p += '\n\t<div class="small-display">\n\t\t';

      print(self.calendarDate(self.parseEventDate(d.date.substring(0, 10))));
      __p += '\n\t</div>\n\t<div class="large-display">' +
        ((__t = (d.formatname)) == null ? '' : __t) +
        '</div>\n\t<div > ' +
        ((__t = (d.dateline)) == null ? '' : __t) +
        '</div>\n\t<br>\n';
    };
    __p += '\n<div class="row  slide-open-item">\n\n\t<div class="col-ms-8 driver-drop-table visible-xs">\n\t\t<hr>\n\t\t<div class="row">\n\t\t\t<div class="col-xs-6 small-display">Circuit</div>\n\t\t\t<div class="col-xs-6 text-right">' +
      ((__t = (d.venue)) == null ? '' : __t) +
      '</div>\n\t\t</div>\n\t\t<hr>\n\t\t<div class="row">\n\t\t\t<div class="col-xs-6 small-display">Length</div>\n\t\t\t<div class="col-xs-6 text-right">' +
      ((__t = (d.trackLength)) == null ? '' : __t) +
      ' km </div>\n\t\t</div>\n\t\t<hr>\n\t\t<!--\n\t\t<div class="row">\n\t\t\t<div class="col-xs-6 small-display">Distance</div>\n\t\t\t<div class="col-xs-6 text-right"> ';
    print(self.statFormat(d.distance));
    __p += ' km </div>\n\t\t</div>\n\t\t<hr>\n\t\t-->\n\t\t<div class="row">\n\t\t\t<div class="col-xs-6 small-display">Laps</div>\n\t\t\t<div class="col-xs-6 text-right">' +
      ((__t = (d.laps)) == null ? '' : __t) +
      '  </div>\n\t\t</div>\n\t\t<hr>\n\t</div>\n\n\n\t\n\t<div class="col-ms-9 col-md-8 col-md-push-2 track-pic ';
    if (isgrid == true) {
      print('isgrid')
    };
    __p += '">\n\t\t<div class="instructions hidden-xs"><img class="hoverbutton" src="images/img/hoverbutton.png"><span class="hover-instructions">Hover for average speed for that portion of the track</span></div><br>\n\n\t\t<div ';
    if (isgrid == true) {;
      __p += '\n\t\t\t\t\tid="drop-calendar-chart"\t\t\t\n\t\t\t\t';
    } else {;
      __p += ' \n\t\t\t\t id=\'track-chart-' +
        ((__t = (d.index)) == null ? '' : __t) +
        '\' \n\t\t\t\t ';
    };
    __p += '\n\t\t\t\t class=\'track-chart-holder\'>\n\t\t\t<img class="img-responsive" src="images/img/' +
      ((__t = (d.picid)) == null ? '' : __t) +
      '.png">\n\t\t</div>\n\t</div>\n\n\t<div class="hidden-xs col-ms-3 col-md-2 col-md-push-2">\n\t\t<div class="small-display">SPEED KM/H</div><br>\n\t\t<div ';
    if (isgrid == true) {;
      __p += '\n\t\t\t\t\tid="drop-calendar-chart-speed"\t\t\t\n\t\t\t\t';
    } else {;
      __p += ' \n\t\t\t\t id=\'track-chart-' +
        ((__t = (d.index)) == null ? '' : __t) +
        '-speed\' \n\t\t\t\t ';
    };
    __p += '\n\t\t\t\t class=\'speed-chart-holder\'>\n\t\t</div>\n\t</div>\n\n\t<div class="hidden-xs col-md-2 col-md-pull-10 track-info ';
    if (isgrid == true) {
      print('isgrid')
    };
    __p += '">\n\t\t<div class="row track-table-row">\n\t\t\t<div class="col-xs-6 col-ms-3 col-md-12 track-table-item">\n\t\t\t\t<div class="small-display">Circuit</div>\n\t\t\t\t<div class="large-display smaller">';
    print(d.venue.replace(/International/g, 'Int&#39;l'));
    __p += '</div>\n\t\t\t</div>\n\n\t\t\t<div class="col-xs-6 col-ms-3 col-md-12 track-table-item">\n\t\t\t\t<div class="small-display">Length</div>\n\t\t\t\t<div class="large-display smaller">' +
      ((__t = (d.trackLength)) == null ? '' : __t) +
      ' km</div>\n\t\t\t</div>\n\t\t\t<div class="col-xs-6 col-ms-3 col-md-12 track-table-item">\n\t\t\t\t<div class="small-display">Laps</div>\n\t\t\t\t<div class="large-display smaller">' +
      ((__t = (d.laps)) == null ? '' : __t) +
      ' </div>\n\t\t\t</div>\n\t\t\t<!--\n\t\t\t<div class="col-xs-6 col-ms-3 col-md-12 track-table-item">\n\t\t\t\t<div class="small-display">Distance</div>\n\t\t\t\t<div class="large-display smaller">';
    print(self.statFormat(d.distance));
    __p += ' km </div>\n\t\t\t</div>\n\t\t\t-->\n\t\t\t<br class="visible-md visible-lg">\n\t\t</div>\n\t</div>\n</div>\n';

  }
  return __p
};
var teal1 = "#D4EDEA";
var teal2 = "#95D5D1";
var teal3 = "#00B5AD";
var teal4 = "#00837D";
var teal5 = "#00504C";
var green1 = "#CDE3CA";
var green2 = "#92C691";
var green3 = "#0AA74B";
var green4 = "#008A3D";
var green5 = "#005E25";
var lime1 = "#DBE9B2";
var lime2 = "#C0DA75";
var lime3 = "#A6CE39";
var lime4 = "#81A32B";
var lime5 = "#4E6A14";
var yellow1 = "#FCF8CD";
var yellow2 = "#F9F07B";
var yellow3 = "#F6EB0E";
var yellow4 = "#BCBD14";
var yellow5 = "#919207";
var tangerine1 = "#FFECC6";
var tangerine2 = "#FFD77D";
var tangerine3 = "#FFC317";
var tangerine4 = "#D0A115";
var tangerine5 = "#886A00";
var orange1 = "#FDD7BE";
var orange2 = "#F9A46C";
var orange3 = "#F26725";
var orange4 = "#C3531C";
var orange5 = "#8C3F11";
var red1 = "#FBCCBD";
var red2 = "#F37B68";
var red3 = "#EC2033";
var red4 = "#BA1526";
var red5 = "#7B0008";
var fuscia1 = "#F4CCDC";
var fuscia2 = "#E575A6";
var fuscia3 = "#DF338A";
var fuscia4 = "#9C1B61";
var fuscia5 = "#740245";
var purple1 = "#D0C5E0";
var purple2 = "#A087BC";
var purple3 = "#7F5AA4";
var purple4 = "#54307F";
var purple5 = "#360451";
var blue1 = "#CCD3EB";
var blue2 = "#8196CC";
var blue3 = "#2165B0";
var blue4 = "#0A376A";
var blue5 = "#001B46";
var cyan1 = "#C7E4F8";
var cyan2 = "#81CBF1";
var cyan3 = "#00A5E5";
var cyan4 = "#0087B4";
var cyan5 = "#00547C";

var grey1 = "#AFBABF";
var grey2 = "#D4D8DA";
var black = "#231F20";
var white = "#FFFFFF";

//relative colors,
var staticnav = grey1;
var selectednav = black;
//a model for each line of data
Line = Backbone.Model.extend({
  defaults: {
    name: undefined,
    rawname: undefined,
    values: undefined,
  },
  initialize: function() {
    var self = this;
    var totalChange = 0;
    var CumulativeChange = 0;
    self.get("values").models.forEach(function(currentItemInLoop) {
      var previousItem = self.get("values").at(self.get("values").indexOf(currentItemInLoop) - 1);
      var firstItem = self.get("values").at(0);
      CumulativeChange += currentItemInLoop.get('value')
      var change;
      if (previousItem) {
        change = currentItemInLoop.get('value') - previousItem.get('value');
        totalChange += change;
        percent = ((currentItemInLoop.get('value') / firstItem.get('value')) - 1) * 100;
        currentItemInLoop.set({
          changePreMonth: change,
          CumulativeChange: CumulativeChange,
          totalChange: totalChange,
          percentChange: percent
        });
      } else {
        percent = ((currentItemInLoop.get('value') / firstItem.get('value')) - 1) * 100;
        currentItemInLoop.set({
          changePreMonth: 0,
          CumulativeChange: CumulativeChange,
          percentChange: percent
        })
      }
    })
  },
});
//a collection of those lines
Lines = Backbone.Collection.extend({
  comparator: function(item) {
    var theDataType = item.get("values").first().get("dataType");
    var chartLayout = item.get("values").first().get("chartLayout");
    if (chartLayout == "stackTotal" || chartLayout == "stackPercent") {
      return item.get("values").last().get(theDataType)
    } else {
      return -item.get("values").last().get(theDataType)
    }
  },
  model: Line,
  initialize: function() {
    var self = this;
  },
  parse: function(data) {
    return data;
  }
});

DataPoint = Backbone.Model.extend({
  defaults: {
    name: undefined,
    rawname: undefined,
    date: undefined,
    value: undefined,
    valueraw: undefined,
    quarters: undefined,
    dataType: undefined,
    chartLayout: undefined
  },
  parse: function(point) {
    return {
      date: point.date,
      name: point.name,
      rawname: point.rawname,
      category: point.category,
      value: point.value,
      valueraw: point.valueraw,
      quarters: point.quarters,
      dataType: point.dataType,
      chartLayout: point.chartLayout
    }
  },
});
//the collection of datapoint which will sort by date.
DataPoints = Backbone.Collection.extend({
  comparator: function(item) {
    var theDataType = item.get("dataType");
    //if there is no category field, then sort based on date, else sort on category
    if (item.get("category") === undefined) {
      return item.get("date")
    } else {
      return item.get("category");
    }
  },
  model: DataPoint,
  parse: function(data) {
    return data;
  },
});
//the view that constructs a linechart
ChartBase = Backbone.View.extend({
  data: undefined,
  lines: undefined,
  //defaults for all the configurable options
  dataType: 'value',
  dataURL: 'data.csv',
  margin: {
    top: 60,
    right: 80,
    bottom: 60,
    left: 30
  },
  hasLegend: "yes",
  legendNames: undefined,
  hasZoom: "yes",
  hasRecessions: "no",
  lineType: "linear",
  YTickLabel: [
    ["", ""]
  ],
  yScaleVals: [0, 100],
  xScaleTicks: 6,
  yScaleTicks: 5,
  xTickFormat: undefined,
  yTickFormat: undefined,
  makeTip: undefined,
  lineColors: [black],
  autoScale: "yes",
  hasMultiData: "no",
  multiDataLabels: ["percentChange", "value"],
  multiDataLabelsDisplay: ["CHANGE", "VALUE"],
  dataFormat: "csv",
  colorUpDown: "no",
  rebaseable: "no",
  horizontal: "no",
  xTickFormat: undefined,
  yTickFormat: undefined,
  dateFormat: d3.time.format("%b '%y"),
  numbFormat: d3.format(",.1f"),
  markDataPoints: "no",
  chartLayout: "basic",
  changeChartLayout: "no",
  showTip: "no",
  changeDataFormat: undefined,
  tickall: undefined,
  chartLayoutLables: ["stackTotal", "basic"],


  initialize: function(opts) {
    this.options = opts;
    // if we are passing in options, use them instead of the defualts.
    if (this.options.dataURL) {
      this.dataURL = this.options.dataURL;
    }
    if (this.options.margin) {
      this.margin = this.options.margin;
      this.rightMarg = parseFloat(this.options.margin.right)
    }
    if (this.options.dataType) {
      this.dataType = this.options.dataType;
    }
    if (this.options.hasLegend) {
      this.hasLegend = this.options.hasLegend;
    }
    if (this.options.legendNames) {
      this.legendNames = this.options.legendNames;
    }
    if (this.options.hasZoom) {
      this.hasZoom = this.options.hasZoom;
    }
    if (this.options.hasRecessions) {
      this.hasRecessions = this.options.hasRecessions;
    }
    if (this.options.lineType) {
      this.lineType = this.options.lineType;
    }
    if (this.options.YTickLabel) {
      this.YTickLabel = this.options.YTickLabel;
    }
    if (this.options.yScaleVals) {
      this.yScaleVals = this.options.yScaleVals;
    }
    if (this.options.xScaleTicks) {
      this.xScaleTicks = this.options.xScaleTicks;
    }
    if (this.options.makeTip) {
      this.makeTip = this.options.makeTip;
    }
    if (this.options.lineColors) {
      this.lineColors = this.options.lineColors;
    }
    if (this.options.autoScale) {
      this.autoScale = this.options.autoScale;
    }
    if (this.options.hasMultiData) {
      this.hasMultiData = this.options.hasMultiData;
    }
    if (this.options.multiDataLabels) {
      this.multiDataLabels = this.options.multiDataLabels;
    }
    if (this.options.multiDataLabelsDisplay) {
      this.multiDataLabelsDisplay = this.options.multiDataLabelsDisplay;
    }
    if (this.options.dataFormat) {
      this.dataFormat = this.options.dataFormat;
    }
    if (this.options.colorUpDown) {
      this.colorUpDown = this.options.colorUpDown;
    }
    if (this.options.rebaseable) {
      this.rebaseable = this.options.rebaseable;
    }
    if (this.options.horizontal) {
      this.horizontal = this.options.horizontal;
    }
    if (this.options.yScaleTicks) {
      this.yScaleTicks = this.options.yScaleTicks;
    }
    if (this.options.xTickFormat) {
      this.xTickFormat = this.options.xTickFormat;
    }
    if (this.options.yTickFormat) {
      this.yTickFormat = this.options.yTickFormat;
    }
    if (this.options.dateFormat) {
      this.dateFormat = this.options.dateFormat;
    }
    if (this.options.numbFormat) {
      this.numbFormat = this.options.numbFormat;
    }
    if (this.options.markDataPoints) {
      this.markDataPoints = this.options.markDataPoints;
    }
    if (this.options.chartLayout) {
      this.chartLayout = this.options.chartLayout;
    }
    if (this.options.changeChartLayout) {
      this.changeChartLayout = this.options.changeChartLayout;
    }
    if (this.options.chartLayoutLables) {
      this.chartLayoutLables = this.options.chartLayoutLables;
    }
    if (this.options.chartLayoutLablesDisplay) {
      this.chartLayoutLablesDisplay = this.options.chartLayoutLablesDisplay;
    }
    if (this.options.showTip) {
      this.showTip = this.options.showTip;
    }
    if (this.options.changeDateFormat) {
      this.changeDateFormat = this.options.changeDateFormat;
    }
    if (this.options.tickAll) {
      this.tickAll = this.options.tickAll;
    }
    //store orig value to restore
    var self = this;
    //if sheets call json, otherwise call CSV
    if (this.dataFormat == "sheets") {
      d3.json(self.dataURL, function(data) {
        self.loadData(data);
      });
    }
    if (this.dataFormat == "csv") {
      d3.csv(self.dataURL, function(data) {
        self.loadData(data);
      });
    }
    if (this.dataFormat == "object") {
      setTimeout(function() {
        self.loadData(self.dataURL);
      }, 100)

    }

    //a resize event will check the width and if it's changed enough, will trigger the udpate.
    $(window).on("resize", _.debounce(function(event) {
      self.newWidth = self.$el.width() - self.margin.left - self.margin.right;
      if (self.newWidth == self.width || self.newWidth <= 0) {
        return
      }
      self.update();
    }, 100));

    //FIX redo the media css
    //		var secret = "7669777978";
    //		var input = "";
    //		//keyup function
    //		$(document).keyup(function(e) {
    //		   input += e.which;
    //		   check_input();
    //		});
    //		function check_input() {
    //		    if(input == secret) {
    //				$("body").addClass("print-version")
    //				self.regenerateCharts ()
    //			};
    //		}


    //end of initialize
  },
  loadData: function(data) {
    var self = this;
    //figure out if it's a 4 year or 2 year date and set parser to match
    if (data[0].date !== undefined) {
      if (data[0].date.split('/')[2].length == 2) {
        self.parseDate = d3.time.format("%m/%d/%y").parse;
      } else {
        self.parseDate = d3.time.format("%m/%d/%Y").parse;
      }
      if (self.changeDateFormat != undefined) {
        self.parseDate = d3.time.format(self.changeDateFormat).parse;
      }
    }
    //stash the rawData for later
    self.rawData = data;
    //set the first item position, will use this if rebasing
    self.firstItemPosition = 0;
    //run the dataaprse
    self.dataParse(self.rawData);
    //then it runs the render function
    self.baseRender();
    //run the renderChart from the view
    self.renderChart();
  },
  dataParse: function(data) {
    var self = this;
    //make a property that tests if there is multiData and if that multiData is a transformation on one set of data
    //if so, set the dataType to be the last one in the array
    if ((self.multiDataLabels[0] == "value" || self.multiDataLabels[0] == "changePreMonth" || self.multiDataLabels[0] == "CumulativeChange" || self.multiDataLabels[0] == "percentChange") && self.firstParse === undefined && self.hasMultiData == "yes") {
      self.dataTransforms = "yes";
      self.dataType = self.multiDataLabels[self.multiDataLabels.length - 1];
    }
    //also check if there are multi chart layouts, and set the chartLayout to the last type in array
    if (self.changeChartLayout == "yes" && self.firstParse === undefined) {
      self.chartLayout = self.chartLayoutLables[self.chartLayoutLables.length - 1];
    }
    //set the data property as a collection
    this.data = new Lines();
    //if there are multiple types, then map them out
    var getNests = d3.nest()
      .key(function(d) {
        return d.type;
      })
      .map(data);
    //figures out names of each type of data.  Use this later to make the labels to click on
    this.nestKeys = d3.keys(getNests);

    //for loop that that re-arranges each type of data by line then puts each distinct type
    for (var i = 0; i < this.nestKeys.length; i++) {
      //new collection for each type
      this[this.nestKeys[i]] = new Lines();
      //pull the types of data out one at a time
      var dataHolder = getNests[this.nestKeys[i]];
      //loop over line data, make each line and populate it with a name and the values which will be DataPoints collections
      var keys = d3.keys(dataHolder[0]).filter(function(key) {
        return (key !== "date" && key !== "type" && key !== "category" && key !== "quarters");
      });
      var keyblob = {};
      //set the colors, and change the name of each line if new names are defined.
      if (this.legendNames === undefined) {
        //define the color scale. if legend names are undefined, then it will color each column header in order
        if (this.color === undefined) {
          this.color = d3.scale.ordinal().domain(keys).range(this.lineColors);
        }
        //and will use the default names
        for (j = 0; j < keys.length; j++) {
          keyblob[keys[j]] = keys[j];
          keyblob[keys[j] + "raw"] = keys[j];
        }
      } else {
        //otherwise it will replace the names. colors can still be arbitrary, if not defined.  If defined, match them up.
        var colorDomain = [];
        var colorRange = [];
        for (j = 0; j < keys.length; j++) {
          keyblob[keys[j]] = this.legendNames[keys[j]];
          keyblob[keys[j] + "raw"] = keys[j];
          colorDomain.push(this.legendNames[keys[j]]);
          if ($.isArray(this.lineColors) === true) {
            colorRange.push(this.lineColors[j]);
          } else {
            colorRange.push(this.lineColors[keys[j]]);
          }
        }
        if (this.color === undefined) {
          this.color = d3.scale.ordinal().domain(colorDomain).range(colorRange);
        }
      }
      //FIX. loop through the data and re-arrange data onto the model
      _.each(keys, _.bind(function(key) {
        var newLine = new Line({
          name: keyblob[key],
          rawname: keyblob[key + "raw"],
          values: new DataPoints(dataHolder.map(function(d) {
            var theDate;
            if (d.date !== undefined) {
              theDate = self.parseDate(d.date);
            }
            return {
              name: keyblob[key],
              rawname: keyblob[key + "raw"],
              date: theDate,
              category: d.category,
              value: parseFloat(d[key]),
              quarters: d.quarters,
              dataType: self.dataType,
              chartLayout: self.chartLayout
            };
          }), {
            parse: true
          })
        });

        //add each line that we've made into the this data lines collection
        this[this.nestKeys[i]].add(newLine, {
          parse: true
        });
        this.data.add(newLine, {
          parse: true
        });


      }, this));

    }

    //if it has multi-data, and the data isn't a transform set the data to the last type in the array
    if (self.hasMultiData == "yes" && self.firstParse === undefined && self.dataTransforms === undefined) {
      self.data = self[self.multiDataLabels[self.multiDataLabels.length - 1]];
    }
    //or set it to whatever is selected now.
    if (self.hasMultiData == "yes" && self.firstParse !== undefined && self.dataTransforms === undefined) {
      d3.selectAll("#" + self.targetDiv + " .navButtons").each(function(d, i) {
        var thisEl = this;
        if (d3.select(thisEl).classed("selected") === true) {
          self.data = self[$(thisEl).attr("dataid")];
        }
      });
    }




    //FIX. for category bars, let's put the sort here, instead of in the comparator.  for now.
    if (self.data.first().get("values").first().get("category") !== undefined) {

      self.data.models[self.data.models.length - 1].get("values").models.sort(function(a, b) {
        if (a.get(self.dataType) < b.get(self.dataType)) {
          return 1;
        } else if (b.get(self.dataType) < a.get(self.dataType)) {
          return -1;
        }
        return 0;
      })
      if (self.data.length > 1) {
        self.data.models.forEach(function(d, i) {
          d.get("values").models.sort(function(a, b) {
            var aCat = a.get("category")
            var bCat = b.get("category")
            var indexOfA
            var indexOfB
            self.data.models[self.data.models.length - 1].get("values").forEach(function(obj, i) {
              if (obj.get("category") == aCat) {
                indexOfA = i;
              }
              if (obj.get("category") == bCat) {
                indexOfB = i;
              }
            })
            var lastA = self.data.models[self.data.models.length - 1].get("values").models[indexOfA].get(self.dataType);
            var lastB = self.data.models[self.data.models.length - 1].get("values").models[indexOfB].get(self.dataType);
            if (lastA < lastB) {
              return 1;
            } else if (lastB < lastA) {
              return -1;
            }
            return 0;
          })
        })
      }
    }


    //let's build the stack values FIX
    //copy the array of each data type and push in data so the stacks build for every data type
    var newNests = self.nestKeys.slice();
    newNests.push("data");
    newNests.forEach(function(thisdata) {
      self[thisdata].each(function(eachGroup, indexofKey) {
        eachGroup.get("values").each(function(d, i) {
          var masterPositive = 0;
          var masterNegative = 0;
          var stackTotal = 0;
          var masterPercent = 0;
          var stackMin = 0;

          self[thisdata].each(function(thisBit, counter) {
            if (parseFloat(thisBit.get("values").at(i).get(self.dataType)) >= 0) {
              stackTotal = stackTotal + parseFloat(thisBit.get("values").at(i).get(self.dataType));
            } else {
              stackMin = parseFloat(stackMin) + parseFloat(thisBit.get("values").at(i).get(self.dataType));
            }
          })

          for (counter = indexofKey; counter < self[thisdata].length; counter++) {
            if (parseFloat(self[thisdata].at(counter).get("values").at(i).get(self.dataType)) > 0) {
              masterPositive = masterPositive + parseFloat(self[thisdata].at(counter).get("values").at(i).get(self.dataType));
              masterPercent = masterPercent + (parseFloat(self[thisdata].at(counter).get("values").at(i).get(self.dataType)) / stackTotal) * 100;
            } else {
              masterNegative = masterNegative + parseFloat(self[thisdata].at(counter).get("values").at(i).get(self.dataType));
            }
          }
          var y0Total = masterPositive - parseFloat(d.get(self.dataType));
          var y1Total = masterPositive;
          if (d.get(self.dataType) < 0) {
            y0Total = masterNegative - parseFloat(d.get(self.dataType));
            y1Total = masterNegative;
          }
          d.set({
            y0Total: y0Total,
            y1Total: y1Total,
            stackTotal: stackTotal,
            stackMin: stackMin,
            y0Percent: masterPercent - ((parseFloat(d.get(self.dataType)) / stackTotal) * 100),
            y1Percent: masterPercent
          });
        });
      });
    });

    //after the first run through, change this so that certain things don't change if this function recalled
    self.firstParse = "done";
    //end of data parse
  },

  baseRender: function() {
    // create a variable called "self" to hold a reference to "this"
    var self = this;
    // making a json copy of hte data, to use in the d3 portion.  easier
    self.chartData = self.data.toJSON();
    self.chartData.forEach(function(d) {
      d.values = d.values.toJSON();
    })

    if ($("body").hasClass("print-version") == true) {
      self.margin = {
        top: 40,
        right: 10,
        bottom: 40,
        left: 35
      };
    }
    //figure out if it's lines or bars, useful for later
    this.chartType = this.getChartType();
    //make sure that the div we are targeting is positioned relative.
    d3.select(self.el)
      .style("position", "relative");
    //set the width and the height to be the width and height of the div the chart is rendered in

    this.width = this.$el.width() - self.margin.left - self.margin.right;
    this.height = this.$el.height() - self.margin.top - self.margin.bottom;

    //make a label based on the div's ID to use as unique identifiers
    this.targetDiv = $(self.el).attr("id");
    //figure out if it has a timescale or an ordinal scale based on whether category is defined in the data
    this.hasTimeScale = function() {
      return self.data.first().get('values').first().get('category') === undefined;
    }();
    //set values if horizontal or not
    self.switchX = "x";
    self.switchY = "y";
    self.switchLeft = "left";
    self.switchHeight = "height";
    self.switchWidth = "width";
    self.switchTop = "top";

    if (self.horizontal == "yes") {
      self.switchX = "y";
      self.switchY = "x";
      self.switchLeft = "top";
      self.switchHeight = "width";
      self.switchWidth = "height";
      self.switchTop = "left";
    }


    //some aspects of the data useful for figuring out bar placement
    this.dataLength = self.data.first().get('values').length;
    this.numberOfObjects = function() {
      if (this.chartLayout == "onTopOf") {
        return 1;
      } else {
        return self.data.length;
      }
    };

    self.widthOfBar = function() {
      if (this.chartLayout == "stackTotal" || this.chartLayout == "stackPercent") {
        return (self[self.switchWidth] / (self.dataLength)) - (self[self.switchWidth] / (self.dataLength)) * 0.2;
      } else {
        return (self[self.switchWidth] / (self.dataLength * self.numberOfObjects())) - (self[self.switchWidth] / (self.dataLength * self.numberOfObjects())) * 0.2;
      }
    };

    self.setWidthAndMargins();

    //create an SVG
    self.svg = d3.select(self.el).append("svg")
      .attr("width", self.width + self.margin.left + self.margin.right)
      .attr("height", self.height + self.margin.top + self.margin.bottom)
      .append("g")
      .attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

    //make a rectangle so there is something to click on
    self.svg.append("svg:rect")
      .attr("width", self.width)
      .attr("height", self.height)
      .attr("class", "plot");

    //make a clip path for the graph
    //		 var clip = self.svg.append("svg:clipPath")
    //		    .attr("id", "clip" + self.targetDiv)
    //		    .append("svg:rect")
    //		    .attr("x", "-"+self.margin.left)
    //		    .attr("y", -4)
    //		    .attr("width", self.width+self.margin.left + 8)
    //		    .attr("height", self.height + 8);

    //go get the scales from the chart type view
    this.scales = {
      x: this.getXScale(),
      y: this.getYScale()
    };

    //render all the incidentals.
    self.recessionMaker();
    self.scaleMaker();
    self.toolTipMaker();
    self.legendMaker();
    self.multiDataMaker();
    self.chartLayoutMaker();

    //end of chart render
    return this;
  },
  setWidthAndMargins: function() {
    var self = this;

    var fscales = {
      x: self.getXScale(),
      y: self.getYScale()
    };
    //create and draw the y axis
    var fAxis = d3.svg.axis()
      .scale(fscales.y)
      .ticks(self.yScaleTicks)

    //define the tick format if it's specified, change tick length if it's horizontal
    if (self.yTickFormat !== undefined) {
      fAxis.tickFormat(self.yTickFormat);
    }
    //if autoScale ing then let it use the default auto scale.  hasZoom and multiData automatically get auto-scaling
    if (self.autoScale == "yes" || self.hasZoom == "yes") {} else {
      //otherwise setup the manual ticks.
      fAxis.tickValues(self.yScaleVals);
    }

    var numbFormat = fscales.y.tickFormat()
    if (self[self.switchY + "TickFormat"]) {
      numbFormat = self[self.switchY + "TickFormat"]
    }
    var tickValueArray = fAxis.scale().ticks(fAxis.ticks()[0])
    var biggestTickValue = numbFormat(tickValueArray[tickValueArray.length - 1])
    self.topTickLength = biggestTickValue.length;

    //set the margins if not set.
    var labelLength = 0;
    self.YTickLabel.forEach(function(label) {
      if (label[0].length > labelLength) {
        labelLength = label[0].length;
      }
    })
    var topmarg = 15;
    if (self.hasMultiData == "yes" || self.changeChartLayout == "yes") {
      topmarg = 60
    }
    if (self.options.margin == undefined) {
      self.margin = {
        top: topmarg,
        right: 100,
        bottom: 60,
        left: 14 + (6 * (self.topTickLength + labelLength))
      }
    }

    self.width = this.$el.width() - self.margin.left - self.margin.right;
    self.height = self.$el.height() - self.margin.top - self.margin.bottom;

    if (self.width < 280) {
      self.hasLegend = "no"
      self.margin.right = 20
      self.width = this.$el.width() - self.margin.left - self.margin.right;
    } else {
      self.hasLegend = self.options.hasLegend
      self.margin.right = 100;
      if (self.hasLegend != "yes") {
        self.margin.right = 20
      }
      if (self.rightMarg != undefined) {
        self.margin.right = self.rightMarg
      }
      self.width = self.$el.width() - self.margin.left - self.margin.right;
    }


  },
  recessionMaker: function() {
    var self = this;
    //put in the recessions, if there are any.
    if (self.hasRecessions != "yes") {
      return;
    }
    var recessionDateParse = d3.time.format("%m/%d/%Y").parse;
    var recessionData = [{
      "recess": [{
        "start": "5/1/1937",
        "end": "6/1/1938"
      }, {
        "start": "2/1/1945",
        "end": "10/1/1945"
      }, {
        "start": "11/1/1948",
        "end": "10/1/1949"
      }, {
        "start": "7/1/1953",
        "end": "5/1/1954"
      }, {
        "start": "8/1/1957",
        "end": "4/1/1958"
      }, {
        "start": "4/1/1960",
        "end": "2/1/1961"
      }, {
        "start": "12/1/1969",
        "end": "11/1/1970"
      }, {
        "start": "11/1/1973",
        "end": "3/1/1975"
      }, {
        "start": "1/1/1980",
        "end": "7/1/1980"
      }, {
        "start": "7/1/1981",
        "end": "11/1/1982"
      }, {
        "start": "7/1/1990",
        "end": "3/1/1991"
      }, {
        "start": "3/1/2001",
        "end": "11/1/2001"
      }, {
        "start": "12/1/2007",
        "end": "6/1/2009"
      }]
    }];
    var recessions = self.svg.selectAll('.recession')
      .data(recessionData);

    var recessionsEnter = recessions.enter().append('g')
      //			.attr("clip-path", "url(#clip" + self.targetDiv + ")")
      .attr("class", "recession")
      .attr("display", "block");

    recessions.selectAll("rect")
      .data(function(d) {
        return (d.recess);
      })
      .enter()
      .append("rect")
      .attr("class", "recessionBox")
      .attr("x", function(d) {
        return self.scales.x(recessionDateParse(d.start));
      })
      .attr("y", 0)
      .attr("width", function(d) {
        return (self.scales.x(recessionDateParse(d.end))) - (self.scales.x(recessionDateParse(d.start)));
      })
      .attr("height", self.height);
  },
  scaleMaker: function() {
    var self = this;

    //create and draw the x axis
    self.xAxis = d3.svg.axis()
      .scale(self.scales[self.switchX])
      .orient("bottom")
      .ticks(self[self.switchX + "ScaleTicks"])
      .tickPadding(8)

    //create and draw the y axis
    self.yAxis = d3.svg.axis()
      .scale(self.scales[self.switchY])
      .orient("left")
      .ticks(self[self.switchY + "ScaleTicks"])
      .tickPadding(8);

    //change the tic size if it's sideways
    if (self.horizontal == "yes") {
      self.xAxis.tickSize(0 - self.height).tickPadding(12);
    }
    //forces a tick for every value on a time scale if you have defined the format
    if (self.tickAll !== undefined) {
      var datedomain = [];
      self.data.first().get("values").each(function(d, i) {
        datedomain.push(d.get("date"))
      })
      self[self.switchX + "Axis"].tickValues(datedomain);
    }
    if (self.xTickFormat !== undefined) {
      self.xAxis.tickFormat(self.xTickFormat)
    }


    if (self.chartLayout == "tier") {
      self[self.switchY + "Axis"].ticks(2);
    }
    if ($(window).width() < 440 && self.hasTimeScale === true) {
      self[self.switchX + "Axis"].ticks(2);
    }

    //define the tick format if it's specified, change tick length if it's horizontal
    if (self.yTickFormat !== undefined) {
      self.yAxis.tickFormat(self.yTickFormat);
    }
    if (self.horizontal != "yes") {
      self.yAxis.tickSize(0 - self.width);
    } else {
      self.yAxis.tickSize(0);
    }

    //if autoScale ing then let it use the default auto scale.  hasZoom and multiData automatically get auto-scaling
    if (self.autoScale == "yes" || self.hasZoom == "yes") {} else {
      //otherwise setup the manual ticks.
      self[self.switchY + "Axis"].tickValues(self.yScaleVals);
    }
    //draw all the axis
    self.svg.append("svg:g")
      .attr("class", "x axis");
    self.svg.select(".x.axis")
      .attr("transform", "translate(0," + self.height + ")")
      .call(self.xAxis);
    self.svg.append("svg:g")
      .attr("class", "y axis");
    self.svg.select(".y.axis")
      .call(self.yAxis);


    //	    if (self.horizontal == "yes"){
    //		    d3.selectAll("#" + self.targetDiv + " .x.axis line")
    //			    .style("stroke", "white");
    //	    }

    if (self.horizontal != "yes") {
      self.svg.selectAll(".y.axis line")
        .attr("x1", "-" + self.margin.left);
    }
    //setup function to add zero line, though i will call it later so it's on top.
    self.makeZeroLine = function() {
      self.zeroLine = self.svg.append("line")
        .attr("class", "zeroAxis")
        //				.attr("clip-path", "url(#clip" + self.targetDiv + ")")
        .attr(self.switchX + "1", function() {
          if (self.horizontal == "yes") {
            return 0;
          }
          return "-" + self.margin[self.switchLeft];

        })
        .attr(self.switchX + "2", self[self.switchWidth])
        .attr(self.switchY + "1", self.scales.y(0))
        .attr(self.switchY + "2", self.scales.y(0))
        .style("stroke", black)
        .style("stroke-width", "2px");
    };

    //get the latest labels
    self.dataLabels = self.YTickLabel[self.YTickLabel.length - 1];
    //adds teh labels to the axis
    self.topTick = function(tickLabels) {
      d3.selectAll("#" + self.targetDiv + " ." + self.switchY + ".axis .tick text")
        .attr("transform", "translate(0,-8)");

      var topTick = d3.selectAll("#" + self.targetDiv + " ." + self.switchY + ".axis .tick:last-of-type");
      var toptickWidth = 11 + (6 * self.topTickLength)

      topTick.append("text")
        .attr("class", "topTick")
        .style("text-anchor", "end")
        .text(tickLabels[0])
        .attr("transform", function(d) {
          if (self.horizontal == "yes") {
            return "translate(" + (0 - (toptickWidth / 3) - 4) + ",14)";
          } else {
            return "translate(" + (0 - toptickWidth) + ",-3.5)";
          }
        });

      var addRect = topTick.append("rect")
        .attr("class", "topTick")
        .style("fill", "none")
        .attr("height", 10)
        .attr("y", -5);

      topTick.append("text")
        .style("text-anchor", function() {
          if (self.horizontal == "yes") {
            return "end";
          } else {
            return "start";
          }
        })
        .attr("class", "topTick")
        .text(tickLabels[1])
        .attr("transform", function(d) {
          if (self.horizontal == "yes") {
            return "translate(" + (0 + (toptickWidth / 2)) + ",30)";
          } else {
            if (tickLabels[1].length == 1) {
              return "translate(-8,-3.5)";
            } else {
              return "translate(-4,-3.5)";
            }
          }
        })
        .classed("topTickPost", true);

    };
    self.topTick(self.dataLabels);
  },
  toolTipMaker: function() {
    var self = this;

    var baseElement = document.getElementById(self.targetDiv);
    // Find your root SVG element
    var svgFind = baseElement.querySelector('svg');
    // Create an SVGPoint for future math
    var pt = svgFind.createSVGPoint();

    // Get point in global SVG space
    function cursorPoint(evt) {
      if ((evt.clientX) && (evt.clientY)) {
        pt.x = evt.clientX;
        pt.y = evt.clientY;
      } else if (evt.targetTouches) {
        pt.x = evt.targetTouches[0].clientX;
        pt.y = evt.targetTouches[0].clientY;
        evt.preventDefault();
      }
      return pt.matrixTransform(svgFind.getScreenCTM().inverse());
    }

    //start the cursor off to the left
    var cursor = 0 - self.margin[self.switchLeft] - 10;
    //add a line
    self.svg.append('svg:line')
      .attr('class', 'cursorline')
      //			.attr("clip-path", "url(#clip" + self.targetDiv + ")")
      .attr(self.switchX + '1', cursor)
      .attr(self.switchX + '2', cursor)
      .attr(self.switchY + '1', 0)
      .attr(self.switchY + '2', self[self.switchHeight]);

    //function to spike out any NaNs
    function noNan(d) {
      if (isNaN(d) === true) {
        return "N/A";
      } else {
        return self.dataLabels[0] + self.numbFormat(d) + " " + self.dataLabels[1];
      }

    }

    //i'm assuming i'm testing if this is undefined so i could possibly pass in a tooltip function from the options.
    //sets up the function that will return a tooltip
    if (self.makeTip === undefined) {
      self.makeTip = function(theIndex, theData) {
        var tip = "<div class='dateTip'>" + theData.first().get("values").at(theIndex).get("category") + "</div><hr>";
        if (self.hasTimeScale === true) {
          tip = "<div class='dateTip'>" + self.dateFormat(theData.first().get("values").at(theIndex).get("date")) + "</div><hr>";
          if (theData.first().get("values").at(theIndex).get("quarters") !== undefined) {
            var yearFormat = d3.time.format(" %Y");
            tip = "<div class='dateTip'>" + theData.first().get("values").at(theIndex).get("quarters") + yearFormat(theData.first().get("values").at(theIndex).get("date")) + "</div><hr>";
          }
        }
        theData.forEach(function(d) {
          var thisItem = d.get("values").at(theIndex);
          var theValue = ""
          if (self.chartLayout == "stackPercent") {
            theValue = self.numbFormat(thisItem.get("y1Percent") - thisItem.get("y0Percent")) + "%"
          } else {
            theValue = noNan(thisItem.get(self.dataType));
          }
          tip += "<div class='tipGroup'><div class='tipLine' style='background-color:" + self.color(thisItem.get("name")) + ";'></div><div class='nameTip'>" + thisItem.get("name") + "</div><div class='valueTip'>" + theValue + "</div></div>";
        });
        return tip;
      };
    }

    // tooltip divs, default turns em off.  They get turned on if you have no legend, or if you specifically turn them on.
    self.tooltip = d3.select("#" + self.targetDiv).append("div")
      .attr("class", "tooltip")
      .style("opacity", 0)
      .classed("wider", function(d) {
        if (self.data.length > 3) {
          return true;
        } else {
          return false;
        }
      })
      .style("display", "none");

    if (self.showTip == "yes" || self.hasLegend == "no") {
      self.tooltip.style("display", "block");
    }

    var closestIndexHolder;

    var cursor;
    var yPoint;



    //FUNCTION THAT MOVES THE LINES ACROSS THE GRAPHIC AND UPDATES ALL THE TIPS
    function tooltipMover(evt) {
      var loc = cursorPoint(evt);
      cursor = loc[self.switchX];
      yPoint = loc[self.switchY];

      //little maths to figure out in the sideBySide layout what data point you are on.
      var widthsOver = 0;
      if (self.chartLayout == "sideBySide") {
        var eachChartWidth = (self[self.switchWidth] / self.numberOfObjects());
        for (i = 0; i < self.data.length; i++) {
          if ((cursor - self.margin[self.switchLeft]) > eachChartWidth) {
            cursor = cursor - eachChartWidth;
            widthsOver = widthsOver + eachChartWidth;
          }
        }
      }
      if (self.chartLayout == "tier") {
        widthsOver = ((self.widthOfBar() * self.numberOfObjects()) / 2) - self.widthOfBar() / 2;
      }

      var toolTipModifier = 0;
      var closest;
      var closestIndex;
      //use a reverse scale to figure out where you are at if there is a time scale.
      //if it's an ordinal scale it's a little trickier.
      if (self.hasTimeScale === true) {
        var locationDate = self.scales.x.invert(cursor - self.margin[self.switchLeft]);
        closest = null;
        self.data.first().get("values").each(function(d, i) {
          if (closest === null || Math.abs(d.get("date") - locationDate) < Math.abs(closest - locationDate)) {
            closest = d.get("date");
            self.tooltipIndex = i;
          }
        });
      } else {
        var closestIndexValue = null;
        var indexLocation = cursor - parseFloat(self.margin[self.switchLeft]);
        self.scales.x.range().forEach(function(d) {
          if (closestIndexValue === null || Math.abs(d - indexLocation) < Math.abs(closestIndexValue - indexLocation)) {
            closestIndexValue = d;
          }
        });
        closestIndex = self.scales.x.range().indexOf(closestIndexValue);
        self.tooltipIndex = closestIndex;
        closest = self.scales.x.domain()[closestIndex];
        toolTipModifier = self.widthOfBar() / 2;
      }

      //Got the value, now let's move the line.
      d3.selectAll("#" + self.targetDiv + " .cursorline")
        .attr(self.switchX + '1', (self.scales.x(closest) + toolTipModifier + widthsOver))
        .attr(self.switchX + '2', self.scales.x(closest) + toolTipModifier + widthsOver);

      //and now we can update the tooltip
      self.tooltip
        .html(function(d) {
          return self.makeTip(self.tooltipIndex, self.data);
        })
        .style("opacity", 0.9)
        .style(self.switchLeft, function(d) {
          var tipWidth = $("#" + self.targetDiv + " .tooltip").width();
          if (cursor < (self.margin.left + self.width + self.margin.right) / 4) {
            return self.margin[self.switchLeft] + self.scales.x(closest) + "px";
          }
          if (cursor > (self.margin.left + self.width + self.margin.right) * 3 / 4) {
            return self.margin[self.switchLeft] + self.scales.x(closest) - tipwidth + "px";
          }
          return self.margin[self.switchLeft] + self.scales.x(closest) - (tipWidth / 2) - 15 + "px";

        })
        .style(self.switchTop, function(d) {
          if ($(window).width() < 600) {
            return "0px";
          }
          return self.margin.top + self.height + 5 + "px";
        });

      //and now we can update the legend.
      if (self.hasLegend == "yes") {
        self.legendValue
          .data(self.data.models, function(d) {
            return (d.get('name'));
          })
          .html(function(d, i) {
            var hoveredSpot = self.data.at(i).get("values").at(self.tooltipIndex);
            if (self.chartLayout == "stackPercent") {
              return self.dataLabels[0] + self.numbFormat(hoveredSpot.get("y1Percent") - hoveredSpot.get("y0Percent")) + " " + self.dataLabels[1];
            } else {
              return noNan(hoveredSpot.get(self.dataType));
            }
          });
        self.legendDate.html(function() {
          var hoverSpot = self.data.first().get("values").at(self.tooltipIndex)
          if (self.hasTimeScale === true) {
            if (hoverSpot.get("quarters") !== undefined) {
              var yearFormat = d3.time.format(" %Y");
              return hoverSpot.get("quarters") + yearFormat(hoverSpot.get("date"));
            } else {
              return self.dateFormat(hoverSpot.get("date"));
            }
          } else {
            return hoverSpot.get("category");
          }
        });
        //now that they are updated, make sure they resort.
        self.setLegendPositions();

        //got to turn off the update while you are hovering over the legend itself.
        if (cursor > self[self.switchWidth] + self.margin[self.switchLeft]) {
          self.legendValue.html("");
          self.legendDate.html("");
          self.setLegendPositions();
        }
      }


    }


    var svgmove = svgFind.addEventListener('mousemove', tooltipMover, false);
    var svgtouch = svgFind.addEventListener('touchmove', tooltipMover, false);
    var svgout = svgFind.addEventListener('mouseout', tooltipEnd, false)
    var svgtouchout = svgFind.addEventListener('touchend', tooltipEnd, false)
    //when you are off the SVG, stop all this updatey

    function tooltipEnd(evt) {
      d3.selectAll("#" + self.targetDiv + " .cursorline")
        .attr(self.switchX + '1', 0 - self.margin[self.switchLeft] - 10)
        .attr(self.switchX + '2', 0 - self.margin[self.switchLeft] - 10);

      self.tooltip
        .style("opacity", 0);

      if (self.hasLegend == "yes") {
        self.legendValue.html("");
        self.legendDate.html("");
        self.setLegendPositions();
      }
    }


  },
  multiDataMaker: function() {
    var self = this;

    if (self.hasMultiData != "yes") {
      return;
    }

    self.makeNav = d3.select(self.el).append("div")
      .attr("class", "navContainer")

    self.makeNavButtons = self.makeNav.selectAll('.navButtons')
      .data(self.multiDataLabels)
      .enter()
      .append("div")
      .attr("class", "navButtons")
      .attr("dataid", function(d) {
        return d;
      })
      .html(function(d, i) {
        return self.multiDataLabelsDisplay[i];
      })
      .classed("selected", function(d, i) {
        if (i == self.multiDataLabels.length - 1) {
          return true;
        } else {
          return false;
        }
      })
      .on("click", function(d, i) {
        if ($(this).hasClass("selected")) {
          return;
        }
        //check if you've written out all the tick labels, if not, use just the first one.
        if (self.YTickLabel[i] === undefined) {
          self.dataLabels = self.YTickLabel[0];
        } else {
          self.dataLabels = self.YTickLabel[i];
        }

        var thisID = $(this).attr("dataid");
        $(this).addClass("selected").siblings().removeClass("selected");

        //this has to act differently if you're doing a data transform, or picking up totally different data.
        if (self.dataTransforms == "yes") {
          self.dataType = thisID;
          //rerun the dataparse, so that the stacks can redraw if there are any.  if you've been playing with the legend, then you have to use the legend data. otherwis euse the raw data.
          if (self.legendData !== undefined) {
            self.dataParse(self.legendData);
          } else {
            self.dataParse(self.rawData);
          }
        } else {
          self.data = self[thisID];
        }
        //run the updater
        self.update();
      });

    //if one of the nav's is too high, we should make them all taller
    self.navheights = [];
    self.makeNavButtons
      .each(function(d, i) {
        self.navheights.push($(this).height());
      })
      .style("height", d3.max(self.navheights) + "px");
  },
  chartLayoutMaker: function() {
    var self = this;
    if (self.changeChartLayout != "yes") {
      return;
    }

    self.makeNavLayout = d3.select(self.el).append("div")
      .attr("class", "layoutNavContainer")

    self.makeNavLayoutButtons = self.makeNavLayout.selectAll('.layoutNavButtons')
      .data(self.chartLayoutLables)
      .enter()
      .append("div")
      .attr("class", "layoutNavButtons")
      .attr("dataid", function(d) {
        return d;
      })
      .style("background-position", function(d) {
        //makes the sprite work.  i should change this to a class instead.
        var positionArray = ["basicbar", "stackTotalbar", "stackPercentbar", "sideBySidebar", "tierbar", "onTopOfbar", "basicline", "stackTotalline", "stackPercentline", "fillLinesline"];
        var positionNumber = positionArray.indexOf(d + self.chartType);
        return "0px " + (-(positionNumber * 40)) + "px";
      })
      .classed("selected", function(d, i) {
        if (i == self.chartLayoutLables.length - 1) {
          return true;
        } else {
          return false;
        }
      })
      .on("click", function(d, i) {
        if ($(this).hasClass("selected")) {
          return;
        }
        var thisID = $(this).attr("dataid");
        $(this).addClass("selected").siblings().removeClass("selected");

        self.chartLayout = d;

        //if there is multidata with transforms, gonna need to reparse.
        if (self.hasMultiData == "yes" && self.dataTransforms == "yes") {
          //rerun the data parser with the new chartLayout.  Use the legendData if something has been removed or added, otherwise use the raw data.
          if (self.legendData !== undefined) {
            self.dataParse(self.legendData);
          } else {
            self.dataParse(self.rawData);
          }
        }

        //default is to use the first tick label in the array
        var navIndex = 0;
        //loop through the nav buttons and find the selected one.
        d3.selectAll("#" + self.targetDiv + " .navButtons").each(function(d, i) {
          var thisEl = this;
          if (d3.select(thisEl).classed("selected") === true) {
            //find the index of the nav in the array to marry up with the Ytick labels
            navIndex = self.multiDataLabels.indexOf($(thisEl).attr("dataid"));
            // if you haven't defined all the tick labels, go back to using the first one
            if (self.YTickLabel[navIndex] === undefined) {
              navIndex = 0;
            }
          }
        });
        //get the right data labels
        self.dataLabels = self.YTickLabel[navIndex];
        //and override them if it's stack percent
        if (self.chartLayout == "stackPercent") {
          self.dataLabels = ["", "%"];
        }

        //run the updater
        self.update();
      });
  },
  legendMaker: function() {
    var self = this;
    //if no legend, get out of here.
    self.makeLegends = d3.select(self.el).append("div")
      .attr("class", "legendContainer")
      .style("width", (self.margin.right - 15) + "px")
      .style("top", self.margin.top + "px")
      .style("display", function() {
        if (self.hasLegend != "yes") {
          return "none";
        }
        return "block"
      });
    self.legendDate = self.makeLegends.append("div").attr("class", "legendDate");

    self.legendItems = self.makeLegends.selectAll('.legendItem')
      .data(self.data.models, function(d) {
        return (d.get('name'));
      })
      .enter()
      .append("div")
      .attr("class", "legendItems")
      .attr("id", function(d) {
        return self.targetDiv + d.get('name');
      })
      .on("click", function(d) {
        $(this).toggleClass("clicked")

        //make an array of the stuff we want to get rid of. anything with the class clicked.
        var removeArray = [];
        self.legendItems.each(function(d, i) {
          if ($(this).hasClass('clicked')) {
            removeArray.push(d.get("rawname"));
          }
        });
        //build a legendData that has everything except what we don't want
        self.legendData = [];
        self.rawData.forEach(function(d, i) {
          self.legendData[i] = _.omit(d, removeArray);
        });
        //then parse that
        self.dataParse(self.legendData);
        // but if we've got multidata that's not a datatransform, we have to figure out which property the data should be set too.
        self.update();
      });

    self.legendItems
      .append("div")
      .attr("class", "legendLines")
      .style("background-color", function(d) {
        return self.color(d.get('name'));
      })
    //.style("width", (self.margin.right - 15) / 2 + "px");

    self.legendItems
      .append("div")
      .attr("class", "legendText")
      .html(function(d) {
        return d.get("name");
      });

    self.legendValue = self.legendItems
      .append("div")
      .attr("class", "legendValue")
      .style("color", function(d) {
        return self.color(d.get('name'));
      });

    //function that figures out how to set the position of each of these guys.
    self.setLegendPositions = function() {
      var depth = 0;
      self.legendItems
        .data(self.data.models, function(d) {
          return (d.get('name'));
        })
        .style("margin-top", function(d, i) {
          var returnDepth = depth;
          depth += $(this).height() + 10;
          return returnDepth + "px";
        });
      self.legendItems
        .data(self.data.models, function(d) {
          return (d.get('name'));
        })
        .exit()
        .style("margin-top", function(d, i) {
          var returnDepth = depth;
          depth += $(this).height() + 10;
          return returnDepth + "px";
        });

    };
    self.setLegendPositions();
  },
  baseUpdate: function() {
    var self = this;

    self.setWidthAndMargins();

    d3.select("#" + self.targetDiv + " svg")
      .transition()
      .duration(1000)
      .attr("width", self.width + self.margin.left + self.margin.right)
      .attr("height", self.height + self.margin.top + self.margin.bottom)

    self.svg
      .transition()
      .duration(1000)
      .attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

    self.svg.select(".plot")
      .transition()
      .duration(1000)
      .attr("width", self.width)
      .attr("height", self.height)

    this.scales = {
      x: this.getXScale(),
      y: this.getYScale()
    };

    self.xAxis.scale(self.scales[self.switchX])

    self.yAxis.scale(self.scales[self.switchY])

    //self[self.switchY+"Axis"].tickSize(0-self[self.switchWidth])

    if (self.hasLegend == "yes") {
      //update the legend
      self.makeLegends
        .style("width", (self.margin.right - 15) + "px")
        .style("top", self.margin.top + "px")
        .style("display", "block")

      var depth = 0;
      self.legendItems
        .data(self.data.models, function(d) {
          return (d.get('name'));
        })
        .transition()
        .duration(500)
        .style("margin-top", function(d, i) {
          var returnDepth = depth;
          depth += $(this).height() + 10;
          return returnDepth + "px";
        });

      self.legendItems
        .data(self.data.models, function(d) {
          return (d.get('name'));
        })
        .exit()
        .transition()
        .duration(500)
        .style("margin-top", function(d, i) {
          var returnDepth = depth;
          depth += $(this).height() + 10;
          return returnDepth + "px";
        });
    } else {
      self.makeLegends.style("display", "none")
    }

    if (self.showTip == "yes" || self.hasLegend == "no") {
      self.tooltip.style("display", "block");
    } else {
      self.tooltip.style("display", "none");
    }

    if (self.chartLayout == "tier") {
      self[self.switchY + "Axis"].ticks(2);
    } else {
      self.yAxis.ticks(self[self.switchY + "ScaleTicks"]);
      self.xAxis.ticks(self[self.switchX + "ScaleTicks"]);
    }
    if ($(window).width() < 440 && self.hasTimeScale === true) {
      self[self.switchX + "Axis"].ticks(2);
    }

    // update the axes,

    self.svg.select("." + self.switchY + ".axis")
      .transition()
      .duration(1000)
      .call(self[self.switchY + "Axis"]);

    self.svg.select("." + self.switchX + ".axis")
      .transition()
      .duration(1000)
      .call(self[self.switchX + "Axis"]);

    if (self.horizontal != "yes") {
      self.svg.selectAll(".y.axis line")
        .attr("x1", "-" + self.margin.left);
    }

    if (self.chartLayout == "sideBySide") {
      self.svg.select("." + self.switchX + ".axis")
        .style("display", "none");
    } else {
      self.svg.select("." + self.switchX + ".axis")
        .style("display", "block");
    }
    if (self.chartLayout == "tier") {

      self.barChart
        .selectAll("." + self.switchY + ".axis")
        .remove()


      self.barChart
        .append("svg:g")
        .attr("class", self.switchY + " axis")
        .attr("transform", function(d) {
          if (self.horizontal == "yes") {
            return "translate(0," + self.height + ")"
          } else {
            return "translate(0,0)"
          }
        });

      self.barChart
        .data(self.chartData, function(d) {
          return d.name;
        })
        .selectAll("." + self.switchY + ".axis")
        .call(self[self.switchY + "Axis"]);
      self.barChart.each(function(d) {
        var thisId = $(this).attr("id")
        var barAxis = $("#" + thisId + " .axis").detach();
        barAxis.prependTo($(this));
      })
    } else {
      if (self.chartType == "bar") {
        self.barChart.selectAll("." + self.switchY + ".axis")
          .remove();
      }
    }

    //update the top tick label
    d3.selectAll("#" + self.targetDiv + " .topTick")
      .remove();
    self.topTick(self.dataLabels);

    //zero line
    self.zeroLine
      .transition()
      .duration(1000)
      .attr(self.switchX + "1", function() {
        if (self.horizontal == "yes") {
          return 0;
        }
        return "-" + self.margin[self.switchLeft];

      })
      .attr(self.switchX + "2", self[self.switchWidth])
      .attr(self.switchY + "1", self.scales.y(0))
      .attr(self.switchY + "2", self.scales.y(0));

    //recessions
    var recessionDateParse = d3.time.format("%m/%d/%Y").parse;
    self.svg.selectAll(".recessionBox")
      .attr("x", function(d) {
        return self.scales.x(recessionDateParse(d.start));
      })
      .attr("width", function(d) {
        return (self.scales.x(recessionDateParse(d.end))) - (self.scales.x(recessionDateParse(d.start)));
      });

    if (self.zoom !== undefined) {
      self.zoom
        .x(self.scales.x)
        .y(self.scales.y);
    }
    //end of base update
  },
  zoomChart: function() {
    var self = this;
    //if there is a zoom, then setup the zoom
    //define the zoom
    self.zoom = d3.behavior.zoom()
      .x(self.scales.x)
      .y(self.scales.y)
      .scaleExtent([1, 8])
      .on("zoom", zoomed);

    //call the zoom on the SVG
    self.svg.call(self.zoom);

    //define the zoom function
    function zoomed() {
      self.svg.select(".x.axis").call(self.xAxis);
      self.svg.select(".y.axis").call(self.yAxis);

      if (self.lineChart !== undefined) {
        self.lineChart
          .data(self.chartData, function(d) {
            return d.name;
          })
          .selectAll(".tipCircle")
          .data(function(d) {
            return d.values;
          })
          .attr("cx", function(d, i) {
            return self.scales.x(d.date);
          })
          .attr("cy", function(d, i) {
            return self.scales.y(d[self.dataType]);
          });

        self.lineChart.selectAll(".line")
          .data(self.chartData, function(d) {
            return d.name;
          })
          .attr("d", function(d) {
            return self.line(d.values);
          });

        self.lineChart.selectAll(".area")
          .data(self.chartData, function(d) {
            return d.name;
          })
          .attr("d", function(d, i) {
            return self.area(d.values);
          });
      }

      self.svg.selectAll(".barChart")
        .data(self.chartData, function(d) {
          return d.name;
        })
        .selectAll(".bar")
        .data(function(d) {
          return d.values;
        })
        .attr(self.switchY, self.yBarPosition)
        .attr(self.switchHeight, self.barHeight)
        .attr(self.switchWidth, self.widthOfBar())
        .attr(self.switchX, function(d, i, j) {
          return self.xBarPosition(d, i, j);
        });

      self.zeroLine
        .attr(self.switchY + "1", self.scales.y(0))
        .attr(self.switchY + "2", self.scales.y(0));

      var recessionDateParse = d3.time.format("%m/%d/%Y").parse;
      self.svg.selectAll(".recessionBox")
        .attr("x", function(d) {
          return self.scales.x(recessionDateParse(d.start));
        })
        .attr("width", function(d) {
          return (self.scales.x(recessionDateParse(d.end))) - (self.scales.x(recessionDateParse(d.start)));
        });
    }
    //end of zoom
  }
  //end of view
});


var LineChart = ChartBase.extend({
  defaults: _.defaults({
    someNewDefault: "yes"
  }, ChartBase.prototype.defaults),
  //setup the scales.  You have to do this in the specific view, it will be called in the chartbase.
  getChartType: function() {
    return "line";
  },
  xScaleMin: function() {
    return d3.min(this.chartData, function(c) {
      return d3.min(c.values, function(v) {
        return v.date;
      });
    });
  },
  xScaleMax: function() {
    return d3.max(this.chartData, function(c) {
      return d3.max(c.values, function(v) {
        return v.date;
      });
    });
  },
  getXScale: function() {
    return d3.time.scale()
      .domain([this.xScaleMin(), this.xScaleMax()])
      .range([0, this.width]);
  },
  yScaleMin: function() {
    var theValues = this.dataType;
    if (this.chartLayout == "stackTotal") {
      theValues = "stackTotal";
    }
    var min = d3.min(this.chartData, function(c) {
      return d3.min(c.values, function(v) {
        return v[theValues];
      });
    });
    if (this.chartlayout == "fillLines") {
      if (min > 0) {
        min = 0;
      }
    }
    if (this.chartLayout == "stackTotal" || this.chartLayout == "stackPercent") {
      min = 0;
    }
    return min;
  },
  yScaleMax: function() {
    var theValues = this.dataType;
    if (this.chartLayout == "stackTotal") {
      theValues = "stackTotal";
    }
    var max = d3.max(this.chartData, function(c) {
      return d3.max(c.values, function(v) {
        return v[theValues];
      });
    });
    if (this.chartLayout == "stackPercent") {
      max = 100;
    }
    return max;
  },
  getYScale: function() {
    if (this.autoScale == "yes" || this.hasZoom == "yes") {
      return d3.scale.linear()
        .domain([this.yScaleMin(), this.yScaleMax()])
        .nice(this.yScaleTicks)
        .range([this.height, 0]);
    } else {
      return d3.scale.linear()
        .domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]])
        .nice(this.yScaleTicks)
        .range([this.height, 0]);
    }
  },
  renderChart: function() {
    // create a variable called "self" to hold a reference to "this"
    var self = this;
    self.chartData = self.data.toJSON();
    self.chartData.forEach(function(d) {
      d.values = d.values.toJSON();
    })

    if (self.hasZoom == "yes") {
      self.zoomChart();
    }

    //will draw the line
    self.line = d3.svg.line()
      .x(function(d) {
        return self.scales.x(d.date);
      })
      .y(function(d) {
        if (self.chartLayout == "stackTotal") {
          return self.scales.y(d.y1Total);
        } else {
          if (self.chartLayout == "stackPercent") {
            return self.scales.y(d.y1Percent);
          } else {
            return self.scales.y(d[self.dataType]);
          }
        }
      })
      .interpolate(self.lineType)
      .defined(function(d) {
        return !isNaN(d[self.dataType]);
      })

    self.area = d3.svg.area()
      .x(function(d) {
        return self.scales.x(d.date);
      })
      .y0(function(d) {
        if (self.chartLayout == "stackTotal") {
          return self.scales.y(d.y0Total);
        } else {
          if (self.chartLayout == "stackPercent") {
            return self.scales.y(d.y0Percent);
          } else {
            return self.scales.y(0);
          }
        }
      })
      .y1(function(d) {
        if (self.chartLayout == "stackTotal") {
          return self.scales.y(d.y1Total);
        } else {
          if (self.chartLayout == "stackPercent") {
            return self.scales.y(d.y1Percent);
          } else {
            return self.scales.y(d[self.dataType]);
          }
        }
      })
      .interpolate(self.lineType)
      .defined(function(d) {
        return !isNaN(d[self.dataType]);
      })


    //bind the data and put in some G elements with their specific mouseover behaviors.
    self.lineChart = self.svg.selectAll(".lineChart")
      .data(self.chartData, function(d) {
        return d.name
      })
      .enter().append("g")
      //			.attr("clip-path", "url(#clip" + self.targetDiv + ")")
      .attr("class", "lineChart")
      .attr('id', function(d) {
        return self.targetDiv + d.name + "-line";
      })
      .on("mouseover", function(d) {
        //put the line we've hovered on on top=
        self.lineChart.sort(function(a, b) {
          if (a.name == d.name) {
            return 1;
          } else {
            return -1;
          }
        }).order();

        //class all other lines to be lighter
        d3.selectAll("#" + self.targetDiv + " .lineChart")
          .classed('notSelected', true);
        d3.select(this)
          .classed("notSelected", false);
      })
      .on("mouseout", function(d) {
        d3.selectAll(".lineChart")
          .classed('notSelected', false);
      });

    self.lineChart.append("path")
      .attr("class", "line")
      .style("stroke", function(d) {
        return self.color(d.name);
      })
      .attr("d", function(d) {
        return self.line(d.values[0]);
      })
      .transition()
      .duration(1500)
      .delay(function(d, i) {
        return i * 100;
      })
      .attrTween('d', function(d) {
        var interpolate = d3.scale.quantile()
          .domain([0, 1])
          .range(d3.range(1, d.values.length + 1));
        return function(t) {
          return self.line(d.values.slice(0, interpolate(t)));
        };
      });

    self.lineChart.append("path")
      .attr("class", "area")
      .style("fill", function(d) {
        return self.color(d.name);
      })
      .attr("d", function(d) {
        return self.area(d.values[0]);
      })
      .style("display", function(d) {
        if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "fillLines") {
          return "block";
        } else {
          return "none";
        }
      })
      .transition()
      .duration(1500)
      .delay(function(d, i) {
        return i * 100;
      })
      .attrTween('d', function(d) {
        var interpolate = d3.scale.quantile()
          .domain([0, 1])
          .range(d3.range(1, d.values.length + 1));
        return function(t) {
          return self.area(d.values.slice(0, interpolate(t)));
        };
      });


    if (self.markDataPoints == "yes") {
      self.lineChart.selectAll(".tipCircle")
        .data(function(d) {
          return d.values
        })
        .enter()
        .append("circle")
        .attr("class", "tipCircle")
        .attr("cx", function(d, i) {
          return self.scales.x(d.date);
        })
        .attr("cy", function(d, i) {
          return self.scales.y(d[self.dataType]);
        })
        .attr("r", function(d, i) {
          if (isNaN(d[self.dataType]) == true) {
            return 0
          }
          return 5
        })
        .style('opacity', 1)
        .style("fill", function(d) {
          return self.color(d.name);
        }); //1e-6
    }

    //add teh zero line on top.
    self.makeZeroLine();

    //function to recaculate the data and setup a slider if this is rebaseable.
    if (self.rebaseable == "yes") {
      var formatDate = d3.time.format("%b, '%y");

      d3.select("#" + self.targetDiv)
        .append("div")
        .attr("id", "scale")
        .style("width", self.width + "px")
        .style("margin-left", self.margin.left + "px");

      d3.select("#" + self.targetDiv)
        .append("div")
        .attr("id", "scaleDate")
        .style("width", (self.width) - self.margin.left + "px")
        .html("SLIDE TO VIEW: Change from " + formatDate(self.parseDate(self.rawData[31].date)));

      $("#scale").slider({
        min: 0,
        max: self.rawData.length - 1,
        value: 0, //default slider value
        step: 1, // step is the allow increments the slider can move. 1 = one month
        slide: function(event, ui) {

          var formatDate = d3.time.format("%b, '%y");

          d3.select("#" + self.targetDiv + " #scaleDate")
            .html("SLIDE TO VIEW: Change from " + formatDate(self.parseDate(self.rawData[ui.value + 1].date)));

          self.rebaseData = self.rawData.slice();
          self.firstItemPosition = ui.value;
          self.dataParse(self.rebaseData);
          self.update();
        }
      });
    }
    self.trigger("linechart:loaded")

    //end chart render
  },
  update: function() {
    var self = this;
    self.chartData = self.data.toJSON();
    self.chartData.forEach(function(d) {
      d.values = d.values.toJSON();
    })

    self.baseUpdate();

    self.exitLine = d3.svg.line()
      .x(function(d) {
        return self.scales.x(d.date);
      })
      .y(function(d) {
        return self.margin.bottom + self.height + self.margin.top + 10;
      })
      .interpolate(self.lineType);

    self.exitArea = d3.svg.area()
      .x(function(d) {
        return self.scales.x(d.date);
      })
      .y0(function(d) {
        return self.margin.bottom + self.height + self.margin.top + 10;
      })
      .y1(function(d) {
        return self.margin.bottom + self.height + self.margin.top + 10;
      })
      .interpolate(self.lineType);

    //exiting lines
    self.lineChart
      .data(self.chartData, function(d) {
        return d.name;
      })
      .exit()
      .selectAll(".line")
      .transition()
      .attr("d", function(d, i) {
        return self.exitLine(d.values);
      });

    //exiting area
    self.lineChart
      .data(self.chartData, function(d) {
        return d.name;
      })
      .exit()
      .selectAll(".area")
      .transition()
      .attr("d", function(d, i) {
        return self.exitArea(d.values);
      });

    self.lineChart
      .data(self.chartData, function(d) {
        return d.name
      })
      .exit()
      .selectAll(".tipCircle")
      .transition()
      .attr("r", 0);

    //update the line
    self.lineChart.selectAll(".line")
      .data(self.chartData, function(d) {
        return d.name;
      })
      .transition()
      .duration(1000)
      .attr("d", function(d, i) {
        return self.line(d.values);
      });

    //update the area
    self.lineChart.selectAll(".area")
      .data(self.chartData, function(d) {
        return d.name;
      })
      .style("display", function(d) {
        if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "fillLines") {
          return "block";
        } else {
          return "none";
        }
      })
      .transition()
      .duration(1000)
      .attr("d", function(d, i) {
        return self.area(d.values);
      });

    //the circles
    self.lineChart
      .data(self.chartData, function(d) {
        return d.name;
      })
      .selectAll(".tipCircle")
      .data(function(d) {
        return d.values
      })
      .transition()
      .duration(1000)
      .attr("cy", function(d, i) {
        return self.scales.y(d[self.dataType]);
      })
      .attr("cx", function(d, i) {
        return self.scales.x(d.date);
      })
      .attr("r", 5);

    //end of update
  }
  //end model
});
//the view that constructs a barChart
var BarChart = ChartBase.extend({
  defaults: _.defaults({
    someNewDefault: "yes"
  }, ChartBase.prototype.defaults),
  //setup the scales
  getChartType: function() {
    return "bar";
  },
  xScaleMin: function() {
    return d3.min(this.data.models, function(c) {
      return d3.min(c.get('values').models, function(v) {
        return v.get('date');
      });
    });
  },
  xScaleMax: function() {
    return d3.max(this.data.models, function(c) {
      return d3.max(c.get('values').models, function(v) {
        return v.get('date');
      });
    });
  },
  xScaleRange: function() {
    var objectNumber = this.numberOfObjects();
    if (this.chartLayout == "stackPercent" || this.chartLayout == "stackTotal") {
      objectNumber = 1;
    }
    var range = [(this.widthOfBar() * objectNumber) / 2, this[this.switchWidth] - this.widthOfBar() * objectNumber];
    if (this.chartLayout == "sideBySide") {
      range = [0, (this[this.switchWidth] / (this.data.length * (1 + (2 / (this.data.first().get('values').length)))))];
    }
    return range;
  },
  getXScale: function() {
    if (this.hasTimeScale === true) {
      return d3.time.scale()
        .domain([this.xScaleMin(), this.xScaleMax()])
        .range(this.xScaleRange());
    } else {
      return d3.scale.ordinal()
        .domain(this.data.first().get('values').map(function(d) {
          return d.get('category');
        }))
        .rangeRoundBands([0, this[this.switchWidth]], 0);
    }
  },
  yScaleMin: function() {
    var theValues = this.dataType;
    if (this.chartLayout == "stackTotal") {
      theValues = "stackMin";
    }
    var min = d3.min(this.data.models, function(c) {
      return d3.min(c.get('values').models, function(v) {
        return v.get(theValues);
      });
    });
    if (this.chartLayout == "stackPercent") {
      min = 0;
    }
    if (min > 0) {
      min = 0;
    }
    return min;
  },
  yScaleMax: function() {
    var theValues = this.dataType;
    if (this.chartLayout == "stackTotal") {
      theValues = "stackTotal";
    }
    var max = d3.max(this.data.models, function(c) {
      return d3.max(c.get('values').models, function(v) {
        return v.get(theValues);
      });
    });
    if (this.chartLayout == "stackPercent") {
      max = 100;
    }
    return max;
  },
  yScaleRange: function() {
    var fullHeight = this[this.switchHeight];
    if (this.chartLayout == "tier") {
      fullHeight = (this[this.switchHeight] / (this.data.length * (1 + (2 / (this.data.first().get('values').length)))));
    }
    var rangeLow = fullHeight;
    var rangeHigh = 0;
    if (this.horizontal == "yes") {
      rangeLow = 0;
      rangeHigh = fullHeight;
    }
    return [rangeLow, rangeHigh]
  },
  getYScale: function() {
    if (this.autoScale == "yes" || this.hasZoom == "yes") {
      return d3.scale.linear()
        .domain([this.yScaleMin(), this.yScaleMax()])
        .nice(this.yScaleTicks)
        .range(this.yScaleRange());
    } else {
      return d3.scale.linear()
        .domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]])
        .range(this.yScaleRange());
    }
  },
  renderChart: function() {
    var self = this;

    if (self.hasZoom == "yes") {
      self.zoomChart();
    }

    self.xBarPosition = function(d, i, j) {
      var theScale = 'category';
      var modifier = 0;
      if (self.hasTimeScale === true) {
        theScale = 'date';
        modifier = (self.widthOfBar() * self.numberOfObjects() / 2);
      }
      if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "sideBySide" || self.chartLayout == "tier") {
        if (self.hasTimeScale === true) {
          modifier = (self.widthOfBar() / 2);
          if (self.chartLayout == "sideBySide") {
            modifier = 0
          }
        }
        return (self.scales.x(d.get(theScale))) - modifier;
      } else {
        if (self.chartLayout == "onTopOf") {
          return (self.scales.x(d.get(theScale)) - modifier) + ((self.widthOfBar() / (j + 1)) * j / 2);
        } else {
          return ((self.scales.x(d.get(theScale)) - (j * self.widthOfBar())) + (self.widthOfBar() * (self.numberOfObjects() - 1))) - modifier;
        }
      }
    };
    self.yBarPosition = function(d) {
      if (isNaN(d.get(self.dataType)) === true) {
        return 0
      }
      var positioner = "y1";
      if (self.horizontal == "yes" || d.get("y1Total") < 0) {
        positioner = "y0";
      }
      if (self.chartLayout == "stackTotal") {
        return self.scales.y(d.get(positioner + "Total"));
      } else {
        if (self.chartLayout == "stackPercent") {
          return self.scales.y(d.get(positioner + "Percent"));
        } else {
          var minOrMax = "max";
          if (self.horizontal == "yes") {
            minOrMax = "min";
          }
          return self.scales.y(Math[minOrMax](0, d.get(self.dataType)));
        }
      }
    };
    self.barHeight = function(d) {
      if (isNaN(d.get(self.dataType)) === true) {
        return 0
      }
      if (self.chartLayout == "stackTotal") {
        return Math.abs(self.scales.y(d.get("y0Total")) - self.scales.y(d.get("y1Total")));
      } else {
        if (self.chartLayout == "stackPercent") {
          return Math.abs(self.scales.y(d.get("y0Percent")) - self.scales.y(d.get("y1Percent")));
        } else {
          return Math.abs(self.scales.y(d.get(self.dataType)) - self.scales.y(0));
        }
      }
    };
    self.barFill = function(d) {
      if (self.colorUpDown == "yes") {
        if (d.get(self.dataType) > 0) {
          return self.lineColors[0];
        } else {
          return self.lineColors[1];
        }
      } else {
        return self.color(d.get("name"));
      }
    };
    self.barWidth = function(d, i, j) {
      if (self.chartLayout == "tier") {
        return self.widthOfBar() * self.numberOfObjects()
      }
      if (self.chartLayout == "onTopOf") {
        return (self.widthOfBar()) / (j + 1);
      } else {
        return self.widthOfBar();
      }
    };
    //enter g tags for each set of data, then populate them with bars.  some attribute added on end, for updating reasons
    self.barChart = self.svg.selectAll(".barChart")
      .data(self.data.models, function(d) {
        return (d.get('name'));
      })
      .enter().append("g")
      .attr("clip-path", "url(#clip" + self.targetDiv + ")")
      .attr("class", "barChart")
      .attr('id', function(d) {
        return self.targetDiv + d.get("name") + "-bar";
      });

    if (self.chartLayout == "sideBySide") {
      self.barChart.attr("transform", function(d, i) {
        if (self.horizontal != "yes") {
          return "translate(" + (i * (self[self.switchWidth] / self.numberOfObjects())) + ",0)";
        } else {
          return "translate(0," + (i * (self[self.switchWidth] / self.numberOfObjects())) + ")";
        }
      });
    }
    if (self.chartLayout == "tier") {
      self.barChart.attr("transform", function(d, i) {
        if (self.horizontal != "yes") {
          return "translate(0," + (i * (self[self.switchHeight] / self.numberOfObjects())) + ")";
        } else {
          return "translate(" + (i * (self[self.switchHeight] / self.numberOfObjects())) + ",0)";
        }
      });
    }

    self.barChart.selectAll(".bar")
      .data(function(d) {
        return (d.get('values').models);
      })
      .enter().append("rect")
      .attr("class", "bar")
      .style("fill", self.barFill)
      .attr(self.switchHeight, 0)
      .attr(self.switchY, self.scales.y(0))
      .attr(self.switchWidth, self.barWidth)
      .attr(self.switchX, function(d, i, j) {
        return self.xBarPosition(d, i, j);
      })
      .transition()
      .duration(1000)
      .attr(self.switchY, self.yBarPosition)
      .attr(self.switchHeight, self.barHeight);

    if (self.chartLayout == "sideBySide") {
      self.svg.select("." + self.switchX + ".axis")
        .style("display", "none");
    } else {
      self.svg.select("." + self.switchX + ".axis")
        .style("display", "block");
    }

    if (self.chartLayout == "tier") {
      self.barChart
        .append("svg:g")
        .attr("class", self.switchY + " axis")
        .attr("transform", function(d) {
          if (self.horizontal == "yes") {
            return "translate(0," + self.height + ")"
          } else {
            return "translate(0,0)"
          }
        });

      self.barChart.selectAll("." + self.switchY + ".axis")
        .call(self[self.switchY + "Axis"]);
      self.barChart.each(function(d) {
        var thisId = $(this).attr("id")
        var barAxis = $("#" + thisId + " .axis").detach();
        barAxis.prependTo($(this));
      })
    } else {
      self.barChart.selectAll("." + self.switchY + ".axis")
        .remove();
    }


    //add teh zero line on top.
    self.makeZeroLine();

    //end of render
  },
  update: function() {
    var self = this;
    self.baseUpdate();

    self.barChart
      .data(self.data.models, function(d) {
        return (d.get('name'));
      })
      .order()
      .transition()
      .duration(1000)
      .attr("transform", function(d, i) {
        var xPosit = 0;
        var yPosit = 0;
        if (self.chartLayout == "sideBySide") {
          if (self.horizontal != "yes") {
            xPosit = (i * (self[self.switchWidth] / self.numberOfObjects()));
          } else {
            yPosit = (i * (self[self.switchWidth] / self.numberOfObjects()));
          }
        }
        if (self.chartLayout == "tier") {
          if (self.horizontal != "yes") {
            yPosit = (i * (self[self.switchHeight] / self.numberOfObjects()));
          } else {
            xPosit = (i * (self[self.switchHeight] / self.numberOfObjects()));
          }
        }
        return "translate(" + xPosit + "," + yPosit + ")";
      });

    self.barChart
      .data(self.data.models, function(d) {
        return (d.get('name'));
      })
      .exit()
      .selectAll(".bar")
      .transition()
      .attr(self.switchHeight, 0)
      .attr(self.switchY, self.scales.y(0));

    self.svg.selectAll(".barChart")
      .data(self.data.models, function(d) {
        return (d.get('name'));
      })
      .selectAll(".bar")
      .data(function(d) {
        return (d.get('values').models);
      })
      .transition()
      .duration(1000)
      .style("fill", self.barFill)
      .attr(self.switchY, self.yBarPosition)
      .attr(self.switchHeight, self.barHeight)
      .attr(self.switchWidth, self.barWidth)
      .attr(self.switchX, function(d, i, j) {
        return self.xBarPosition(d, i, j);
      });
    //end of update
  }

  //end of mdoel
});

//the view that constructs a linechart
slideDown = Backbone.View.extend({
  data: undefined,
  dataURL: undefined,
  dataName: undefined,
  listTemplate: undefined,
  gridTemplate: undefined,
  gridTemplateOpen: undefined,
  initialize: function(opts) {
    this.options = opts;
    // if we are passing in options, use them instead of the defualts.
    if (this.options.dataURL) {
      this.dataURL = this.options.dataURL;
    }
    if (this.options.dataName) {
      this.dataName = this.options.dataName;
    }
    if (this.options.listTemplate) {
      this.listTemplate = this.options.listTemplate;
    }
    if (this.options.gridTemplate) {
      this.gridTemplate = this.options.gridTemplate;
    }
    if (this.options.gridTemplateOpen) {
      this.gridTemplateOpen = this.options.gridTemplateOpen;
    }
    //store orig value to restore
    var self = this;
    self.dataParse(self.dataURL)
    self.baseRender();
    if (self.chartRender) {
      self.chartRender()
    }


    //end of initialize
  },
  dataParse: function(dataURL) {
    var self = this;

    //variables
    self.parseBirthDate = d3.time.format("%Y-%d-%m").parse
    self.parseEventDate = d3.time.format("%Y-%m-%d").parse
    self.statFormat = d3.format(".02f")
    self.formatSkedDate = d3.time.format("%m/%d/%y")
    self.calendarDate = d3.time.format("%b %e")

    self.fullData = dataURL;
    self.data = dataURL[self.dataName];
    //add in the index
    self.data.forEach(function(d, i) {
      d.index = i;
    })
  },
  baseRender: function() {

    var self = this;
    self.targetDiv = $(self.el).attr("id");

    d3.select("#" + self.targetDiv)
      .append("div")
      .attr("id", self.targetDiv + "-content")
      .attr("class", self.targetDiv + "-content selected")
      .classed("list-or-grid", function() {
        if (self.gridTemplate) {
          return true
        }
      })

    self.listDiv = self.targetDiv + "-content"

    //the template part
    var html = self.listTemplate({
      data: self.data
    });
    $("#" + self.listDiv).html(html);


    //make the buttons work
    $("#" + self.listDiv + " .slide-down-item").on("click", function(event) {
      $(this).toggleClass("opened")

      //horribleness needs fix
      var dataname = self.dataName;
      if (dataname == "calendar") {
        dataname = "track"
      }
      var i = $("#" + self.listDiv + " .slide-down-item").index(this)

      if (self.sizeDropChart) {
        setTimeout(function() {
          self.sizeDropChart(dataname + "-chart-" + i, "first");
        }, 520)
      }
      if (self.sizeDropChartSmall) {
        setTimeout(function() {
          self.sizeDropChartSmall(dataname + "-chart-" + i);
        }, 520)
      }

    })


    if (self.gridTemplate) {
      d3.select("#" + self.targetDiv)
        .append("div")
        .attr("id", self.targetDiv + "-content-grid")
        .attr("class", self.targetDiv + "-content-grid list-or-grid")

      self.gridDiv = self.targetDiv + "-content-grid"

      //the template part
      var html = self.gridTemplate({
        data: self.data
      });
      $("#" + self.gridDiv).html(html);

      self.endBox = ""
      $("#" + self.gridDiv + " .box-layout").on("click", function() {
        //if you are clicking on the same one, close the info box.
        if ($(this).hasClass("opened")) {
          $("#" + self.targetDiv + " .grid-info").removeClass("opened")
          $(this).removeClass("opened")
          return
        }

        //otherwise, mark which one you opened, and unmark all the rest,
        $(this).addClass("opened").siblings().removeClass("opened")

        //move info box
        var i = $("#" + self.targetDiv + " .box-layout").index($(this))
        var thisWidth = $(this).outerWidth()
        var containerWidth = $(".main-content").innerWidth()
        var itemsPerRow = Math.round(containerWidth / thisWidth)
        var rowPosition = (i) % itemsPerRow + 1;
        var itemsToRight = itemsPerRow - rowPosition
        var boxes = $("#" + self.targetDiv + " .box-layout")
        var targetBox = boxes[i + itemsToRight]
        if (!targetBox) {
          targetBox = $("#" + self.gridDiv + " .box-layout").last()
        }
        if (self.endBox != targetBox) {
          $("#" + self.targetDiv + " .grid-info").insertAfter(targetBox)
        }

        self.endBox = targetBox;

        var html = self.gridTemplateOpen({
          data: self.data[i],
          d: self.data[i],
          self: self,
          isgrid: true
        });
        $("#" + self.gridDiv + " .grid-info").html(html);


        if (self.dropCharts) {
          self.dropCharts("drop-" + self.dataName + "-chart", self.data[i].name, i)
        }

        setTimeout(function(d) {
          //open info box
          $("#" + self.targetDiv + " .grid-info").addClass("opened")
        }, 100)

      })


      $(window).on("resize", _.debounce(function(event) {
        if ($(window).width() < 600 && $("#" + self.gridDiv).hasClass("selected") == true) {
          $("#" + self.gridDiv).removeClass("selected")
          $("#" + self.listDiv).addClass("selected")
          $(".toggle-box").removeClass("clicked")
        }

        var selectedItem = $("#" + self.gridDiv + " .box-layout.opened")
        var i = $("#" + self.gridDiv + " .box-layout").index(selectedItem)
        if (i > -1) {
          //move info box
          var thisWidth = $(selectedItem).outerWidth()
          var containerWidth = $(".main-content").innerWidth()
          var itemsPerRow = Math.round(containerWidth / thisWidth)
          var rowPosition = (i) % itemsPerRow + 1;
          var itemsToRight = itemsPerRow - rowPosition
          var boxes = $("#" + self.gridDiv + " .box-layout")
          var targetBox = boxes[i + itemsToRight]
          if (!targetBox) {
            targetBox = $("#" + self.gridDiv + " .box-layout").last()
          }
          $("#" + self.targetDiv + " .grid-info").insertAfter(targetBox)

        } else {
          $("#" + self.targetDiv + " .grid-info").insertAfter($("#" + self.gridDiv + " .box-layout").last())
        }


        //sizeResultsCharts ()

        //$(".grid-info").insertAfter(targetBox)
        //$(".grid-info").removeClass("opened")
      }, 100));
    }
  },
  makeRank: function(d) {
    var rank = d + '<sup>th</sup> place'
    if (d == 1) {
      rank = d + '<sup>st</sup> place';
    }
    if (d == 2) {
      rank = d + '<sup>nd</sup> place';
    }
    if (d == 3) {
      rank = d + '<sup>rd</sup> place';
    }
    if (d == 0) {
      rank = ""
    }
    return rank
  },
  //end of view
});

var teamChart = slideDown.extend({
  defaults: _.defaults({
    someNewDefault: "yes"
  }, ChartBase.prototype.defaults),
  teamChartObj: {},
  chartRender: function() {
    var self = this;

    //build the team data
    var teampointData = []
    var formatDate = d3.time.format("%m/%d/%y")
    var teamNameArray = []
    self.data.forEach(function(item) {
      if (item.drivers.length == 3) {
        //item.drivers.pop()
      }
      teamNameArray.push(item.name)
      item.teamPointsHistory.forEach(function(d, i) {
        if (teampointData[i] == undefined) {
          var obj = {
            date: formatDate(self.parseEventDate(d.date.substring(0, 10))),
          }
          obj[item.name] = d.points;
          teampointData.push(obj)
        } else {
          teampointData[i][item.name] = d.points;
        }

      })
    })
    teampointData.forEach(function(d) {
      teamNameArray.forEach(function(team) {
        if (d[team] == undefined) {
          d[team] = 0;
        }
      })
    })
    //build out list of all the dates
    var scheduleDates = _.uniq(_.pluck(self.fullData.schedule, 'date'))
    var resultDates = _.uniq(_.pluck(self.fullData.results, 'date'))
    var teamPointDates = []
    resultDates.forEach(function(d) {
      teamPointDates.push(self.formatSkedDate(self.parseEventDate(d.substring(0, 10))))
    })
    scheduleDates.forEach(function(d) {
      teamPointDates.push(self.formatSkedDate(self.parseEventDate(d.substring(0, 10))))
    })
    self.teamPointDates = teamPointDates
    self.teampointData = teampointData
    //build the chart
    self.data.forEach(function(d, i) {
      self.dropCharts("teams-chart-" + i, d.name)
    })
  },
  dropCharts: function(chartId, itemName) {
    var self = this;
    d3.select("#" + chartId).html(" ")
    if (!self.teampointData[0]) {
      return
    }
    self.teamChartObj[chartId] = new LineChart({
      el: "#" + chartId,
      dataURL: self.teampointData,
      dataFormat: "object", //can be sheets, csv or object
      margin: {
        top: 10,
        right: 0,
        bottom: 80,
        left: 10
      },
      autoScale: "yes",
      yScaleVals: [-20, -10, 0, 10, 20],
      xScaleTicks: 5,
      yScaleTicks: 5,
      YTickLabel: [
        ["", "points"]
      ],
      hasLegend: "no",
      //			legendNames:{disney:"Disney", sandp:"S&P 500 Index"}, // auto to just use the ones in the spreadsheet, else, an array to override them.  be careful here.
      dateFormat: d3.time.format("%B %d"),
      numbFormat: d3.format(",.0f"),
      hasZoom: "no",
      hasRecessions: "no",
      lineType: "step-before", //step-before, step-after
      markDataPoints: "no",
      lineColors: ["#FDD500", grey2, grey2, grey2, grey2, grey2, grey2, grey2, grey2, grey2],
      colorUpDown: "no",
      dataType: 'CumulativeChange', //value, changePreMonth, CumulativeChange, percentChange
      hasMultiData: "no",
      multiDataLabels: ["CumulativeChange", "percentChange"], //can use value,changePreMonth, CumulativeChange, percentChange
      multiDataLabelsDisplay: ["CUMULATIVE", "PERCENT"],
      rebaseable: "no",
      horizontal: "no",
      chartLayout: "fillLines", // stackTotal, stackPercent, fillLines
      changeChartLayout: "no",
      chartLayoutLables: ["stackTotal", "stackPercent", "fillLines", "basic"],
      //		tickAll:"yes", //if you make tickAll anything, it will put a tick for each point.
      //		changeDateFormat:"%d/%m/%y"
      //		showTip:"yes"
      //		makeTip: function (d) {
      //				NumbType = d3.format(".1f");//how do you want to display number
      //				formatDate = d3.time.format("%b %d, '%y")
      //				var toolTip = '<p class="tip3">' + d.get("name") + '<p class="tip1">' + NumbType(d.get("value")) + ' </p> <p class="tip3">' + formatDate(d.get("date"))+'</p>';
      //		      	return toolTip;},
      //		yTickFormat:function(d){
      //			var numbFormat = d3.format(".2f")
      //			return numbFormat(d)
      //		},
    });

    self.teamChartObj[chartId].on("linechart:loaded", function(event) {
      var chartself = this;


      //chartself.hasTimeScale = false;

      chartself.xAxis.tickFormat(function(d) {
        /*
        				var today = new Date
        				var parseDate = d3.time.format("%m/%d/%y").parse
        				if (parseDate(d) > self.parseEventDate(self.fullData.schedule[0].date.substring(0, 10))){
        					return
        				}
        				if (parseDate(d) > today){
        					//return "NEXT: "+self.fullData.schedule[0].name
        					return
        				}
        */
        return
      })

      var parseDate = d3.time.format("%m/%d/%y").parse

      chartself.getXScale = function() {
        return d3.time.scale()
          .domain([parseDate(self.teamPointDates[0]), parseDate(self.teamPointDates[self.teamPointDates.length - 1])])
          .range([0, chartself.width], 0.5);
      }

      chartself.scales.x = chartself.getXScale();

      //			chartself.scales.x.domain([parseDate(self.teamPointDates[0]),parseDate(self.teamPointDates[self.teamPointDates.length-1]) ])



      /*

      			chartself.getXScale = function() {
      				return d3.scale.ordinal()
      					.domain(self.teamPointDates)
      					.rangePoints([0, this.width], 0.5);
      			}

      		chartself.scales.x = chartself.getXScale();

      		chartself.line.x(function(d) {
      				return chartself.scales.x(self.formatSkedDate(d.date));
      			})
      		chartself.area.x(function(d) {
      				return chartself.scales.x(self.formatSkedDate(d.date));
      			})
      */
      //	self.yAxis.tickSize(0)
      chartself.svg.select(".y.axis")
        .remove()
      //    .call(self.yAxis);


      chartself.update()
      chartself.svg.select(".x.axis")
        .call(chartself.xAxis);

      chartself.lineChart.selectAll("path.line")
        .style("stroke", function(d) {
          if (d.name == itemName) {
            return "none"
          } else {
            return "#808284"
          }
        })
        .style("stroke-width", function(d) {
          return "1px"


        })

      chartself.lineChart.selectAll("path.area")
        .style("fill", function(d) {
          if (d.name == itemName) {
            return "#FDD500"
          } else {
            return "none"
          }
        })

      chartself.lineChart.on("mouseover", function(d) {
        return
      })

      chartself.lineChart.sort(function(a, b) {
        if (a.name == itemName) {
          return -1;
        } else {
          return 1;
        }
      }).order();

      function noNan(d) {
        if (isNaN(d) === true) {
          return "N/A";
        } else {
          return chartself.dataLabels[0] + chartself.numbFormat(d) + " " + chartself.dataLabels[1];
        }

      }
      chartself.makeTip = function(theIndex, theData) {
        var thisItem = theData.where({
          name: itemName
        })[0].get("values").at(theIndex);
        var theValue = noNan(thisItem.get(chartself.dataType));
        var theVenue
        self.fullData.results.forEach(function(d) {
          if (chartself.dateFormat(self.parseEventDate(d.date.substring(0, 10))) == chartself.dateFormat(theData.first().get("values").at(theIndex).get("date"))) {
            theVenue = d.name
          }

        })


        tip = "<div class='tipGroup'><div class='team-tip'>" + chartself.dateFormat(theData.first().get("values").at(theIndex).get("date")) + "</div><div class='team-name-tip'>" + theVenue + "</div><div class='team-tip'>" + theValue + "</div></div>";

        return tip;
      };

      d3.selectAll("#" + chartself.targetDiv + " .x.axis line")
        .style("stroke", "white")

      $("#" + self.listDiv + " .slide-down-item").on("click", function(event) {
        setTimeout(function() {
          chartself.newWidth = chartself.$el.width() - chartself.margin.left - chartself.margin.right;
          chartself.update()
        }, 500)
      })


    })
    //enddrrop
  },
  sizeDropChart: function(chartId) {
    var self = this;
    setTimeout(function() {
      self.teamChartObj[chartId].newWidth = self.teamChartObj[chartId].$el.width() - self.teamChartObj[chartId].margin.left - self.teamChartObj[chartId].margin.right;
      self.teamChartObj[chartId].update()


    }, 200)

  }

})

var driverChart = slideDown.extend({
  defaults: _.defaults({
    someNewDefault: "yes"
  }, ChartBase.prototype.defaults),
  chartRender: function() {
    var self = this;
    self.raceDates = []
    self.smallraceDates = []
    self.fullData.results.forEach(function(d) {
      self.raceDates.push(d.date)
      self.smallraceDates.push(d.date)
    })
    self.fullData.schedule.forEach(function(d) {
      self.raceDates.push(d.date)
    })

    self.data.forEach(function(d, i) {
      self.dropCharts("drivers-chart-" + i, d.name, i)
      self.dropChartsSmall("drivers-chart-" + i + "-small", d.name, i)
    })


  },
  dropCharts: function(chartId, itemName, i) {

    var self = this;

    self.width = $("#" + chartId).width()
    if (self.width < 0) {
      self.width = 100
    }

    var driverTable = $("#" + chartId).parent().parent().find(".driver-drop-table")
    self.height = $(driverTable).height() - $(".points-per").height()

    self.y = d3.scale.ordinal()
      .domain(self.raceDates)
      .rangeRoundBands([0, self.height], .1);
    self.x = d3.scale.linear()
      .domain([0, 25])
      .range([0, self.width]);

    self.driverChart = d3.select("#" + chartId)
      .append("svg")
      .attr("height", self.height)
      .attr("width", self.width)
    self.addBars = self.driverChart.selectAll("bar")
      .data(function(d) {
        return self.data[i].pointData
      })
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("y", function(d) {
        return self.y(d.date)
      })
      .attr("height", self.y.rangeBand())
      .attr("x", function(d) {
        return self.x(0)
      })
      .attr("width", function(d) {
        return self.x(parseFloat(d.points))
      })
      .style("fill", function(d) {
        if (d.points == 25) {
          return "#FDD500"
        }
        return "#D1D2D4"
      })
      .attr("title", function(d) {
        var parentItem = self.data[i];
        var venue;
        var winner = ""
        if (d.points == 25) {
          winner = "<div class='tip3'>WINNER</div>"
        }
        self.fullData.results.forEach(function(result) {
          if (d.date == result.date) {
            venue = result.name;
          }

        })
        var formatDate = d3.time.format("%B %d")
        var numbformat = d3.format(".0f")
        return winner + "<div class='tip2'>" + venue + "</div><div class='tip3'>" + numbformat(d.points) + " points</div>"
        //formatDate(self.parseEventDate(d.date.substring(0, 10)))

      })

    self.addBaseLine = self.driverChart.append("line")
      .attr("y1", 0)
      .attr("y2", self.height)
      .attr("x1", self.x(0))
      .attr("x2", self.x(0))
      .style("stroke", black)


    $("rect.bar").tipsy({
      opacity: 1,
      gravity: 's',
      html: true
    });


    $(window).on("resize", _.debounce(function(event) {
      self.sizeDropChart(chartId);
    }, 100));



    //end drop chart

  },
  dropChartsSmall: function(chartId, itemName, i) {
    var self = this;
    self.small = {};
    self.small.width = $("#" + chartId).width()
    if (self.small.width < 0) {
      self.small.width = 100
    }
    self.small.height = 150;


    self.small.x = d3.scale.ordinal()
      .domain(self.smallraceDates)
      .rangeRoundBands([0, self.small.width], .1);
    self.small.y = d3.scale.linear()
      .domain([0, 25])
      .range([self.small.height, 0]);

    self.small.driverChart = d3.select("#" + chartId)
      .append("svg")
      .attr("height", self.small.height)
      .attr("width", self.small.width)
    self.small.addBars = self.small.driverChart.selectAll("bar")
      .data(function(d) {
        return self.data[i].pointData
      })
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", function(d) {
        return self.small.x(d.date)
      })
      .attr("width", self.small.x.rangeBand())
      .attr("height", function(d) {
        return self.small.y(0)
      })
      .attr("y", function(d) {
        return self.small.y(parseFloat(d.points))
      })
      .style("fill", function(d) {
        if (d.points == 25) {
          return "#FDD500"
        }
        return "#D1D2D4"
      })
      .attr("title", function(d) {
        var parentItem = self.data[i];
        var venue;
        var winner = ""
        if (d.points == 25) {
          winner = "<div class='tip3'>WINNER</div>"
        }
        self.fullData.results.forEach(function(result) {
          if (d.date == result.date) {
            venue = result.name;
          }

        })
        var formatDate = d3.time.format("%B %d")
        var numbformat = d3.format(".0f")
        return winner + "<div class='tip2'>" + venue + "</div><div class='tip3'>" + numbformat(d.points) + " points</div>"
        //formatDate(self.parseEventDate(d.date.substring(0, 10)))

      })

    $("rect.bar").tipsy({
      opacity: 1,
      gravity: 's',
      html: true
    });


    $(window).on("resize", _.debounce(function(event) {
      self.sizeDropChartSmall(chartId);
    }, 100));



    //end drop chart

  },
  sizeDropChart: function(chartId) {
    var self = this;


    var droptable = $("#" + chartId).parent().parent().find(".driver-drop-table")
    var pointsper = $("#" + chartId).parent().find(".points-per")
    self.width = $("#" + chartId).width()
    self.height = $(droptable).height() - $(pointsper).height()


    /*
    		self.width = $(".driverChartHolder").width()
    		self.height = $(".driver-drop-table").height() - $(".points-per").height()
    */

    if (self.width < 0) {
      return
    }
    self.y.rangeRoundBands([0, self.height], .1);

    self.x.range([0, self.width]);

    var allSvgs = d3.selectAll("#" + chartId + " svg")
      .transition()
      .attr("width", self.width)
      .attr("height", self.height)

    allSvgs.selectAll(".bar")
      .transition()
      .attr("y", function(d) {
        return self.y(d.date)
      })
      .attr("height", self.y.rangeBand())
      .attr("x", function(d) {
        return self.x(0)
      })
      .attr("width", function(d) {
        return self.x(parseFloat(d.points))
      })

    allSvgs.selectAll("line")
      .transition()
      .attr("y2", self.height)
  },
  sizeDropChartSmall: function(chartId) {
    var self = this;
    self.small.width = $("#" + chartId + "-small").width()
    if (self.small.width < 0) {
      return
    }
    self.small.x.rangeRoundBands([0, self.small.width], .1);


    var allSvgs = d3.selectAll("#" + chartId + "-small" + " svg")
      .transition()
      .attr("width", self.small.width)

    allSvgs.selectAll(".bar")
      .transition()
      .attr("x", function(d) {
        return self.small.x(d.date)
      })
      .attr("width", self.small.x.rangeBand())

  }
})


var trackChart = slideDown.extend({
  defaults: _.defaults({
    someNewDefault: "yes"
  }, ChartBase.prototype.defaults),
  chartRender: function() {
    var self = this;

    self.data.forEach(function(d, i) {
      self.dropCharts("track-chart-" + i, d.name, i)
    })


  },
  dropCharts: function(chartId, itemName, i) {
    var self = this;
    if (self.data[i].venueCountry == "MEX" || self.data[i].venueCountry == "AZE" || self.data[i].venueCountry == "DEU") {
      $("#" + chartId).parent().find(".hover-instructions")
        .html("New track, no speed data available")
      return
    }

    $("#" + chartId).parent().find(".hover-instructions")
      .html("Hover for average speed for that portion of the track")

    self[chartId] = {}
    ///dots on map
    self[chartId].makeDots = d3.select("#" + chartId).selectAll("dots")
      .data(self.data[i].speeds)
      .enter()
      .append("div")
      .attr("class", "track-point hidden-xs")
      .style("top", function(d) {
        return "calc(" + ((d.yposition / 800) * 100) + "% - 10px)"
      })
      .style("left", function(d) {
        return "calc(" + ((d.xposition / 800) * 100) + "% - 10px)"
      })
      .attr("title", function(d) {
        return "<div class='small-display'>Speed</div><div class='reg-display'><span class='large-display'>" + d.speed + "</span> km/hr</div>"
      })
      .on("mouseover", function(d) {
        self[chartId].addBars
          .style("fill", function(item) {
            if (d.dotid == item.dotid) {
              return "black"
            } else {
              return "#FDD500"
            }
          })
      })
      .on("mouseout", function(d) {
        self[chartId].addBars
          .style("fill", function(item) {
            return "#FDD500"
          })
      })

    //speed bars
    self[chartId].width = $("#" + chartId + "-speed").width()
    if (self[chartId].width < 0) {
      self[chartId].width = 100
    }
    self[chartId].height = $("#" + chartId).width()
    //self.height = 800;

    self[chartId].y = d3.scale.ordinal()
      .domain(self.data[i].speeds.map(function(d) {
        return d.dotid;
      }))
      .rangeRoundBands([0, self[chartId].height], .1);

    self[chartId].x = d3.scale.linear()
      .domain([0, d3.max(self.data[i].speeds, function(d) {
        return +d.speed
      })])
      .range([0, self[chartId].width]);

    self[chartId].speedChart = d3.select("#" + chartId + "-speed")
      .append("svg")
      .attr("height", self[chartId].height)
      .attr("width", self[chartId].width)

    self[chartId].addBars = self[chartId].speedChart.selectAll("bar")
      .data(function(d) {
        return self.data[i].speeds
      })
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("y", function(d) {
        return self[chartId].y(d.dotid)
      })
      .attr("height", self[chartId].y.rangeBand())
      .attr("x", function(d) {
        return self[chartId].x(0)
      })
      .attr("width", function(d) {
        return self[chartId].x(parseFloat(d.speed))
      })
      .style("fill", function(d) {
        return "#FDD500"
      })
      .attr("title", function(d, i) {
        return "<div class='small-display'>Speed</div><div class='reg-display'><span class='large-display'>" + d.speed + "</span> km/hr</div>"
      })
      .on("mouseover", function(d) {
        self[chartId].makeDots
          .style("background-color", function(item) {
            if (d.dotid == item.dotid) {
              return "black"
            } else {
              return "#FDD500"
            }
          })
      })
      .on("mouseout", function(d) {
        self[chartId].makeDots
          .style("background-color", function(item) {
            return "#FDD500"
          })
      })

    self[chartId].addBaseLine = self[chartId].speedChart.append("line")
      .attr("y1", 0)
      .attr("y2", self[chartId].height)
      .attr("x1", self[chartId].x(0))
      .attr("x2", self[chartId].x(0))
      .style("stroke", black)

    $("rect.bar").tipsy({
      opacity: 1,
      gravity: 's',
      html: true
    });
    $(".track-point").tipsy({
      opacity: 1,
      gravity: 's',
      html: true
    });


    $(window).on("resize", _.debounce(function(event) {
      self.sizeDropChart(chartId);
    }, 100));

    //end drop chart

  },
  sizeDropChart: function(chartId, first) {
    var self = this;
    var extraPadding = 0
    if (first) {
      extraPadding = 30
    }
    self[chartId].width = $("#" + chartId + "-speed").width()
    self[chartId].height = $("#" + chartId).width() + extraPadding

    if (self[chartId].width < 0) {
      return
    }
    self[chartId].y.rangeRoundBands([0, self[chartId].height], .1);

    self[chartId].x.range([0, self[chartId].width]);

    var allSvgs = d3.selectAll("#" + chartId + "-speed" + " svg")
      .transition()
      .attr("width", self[chartId].width)
      .attr("height", self[chartId].height)

    allSvgs.selectAll(".bar")
      .transition()
      .attr("y", function(d) {
        return self[chartId].y(d.dotid)
      })
      .attr("height", self[chartId].y.rangeBand())
      .attr("x", function(d) {
        return self[chartId].x(0)
      })
      .attr("width", function(d) {
        return self[chartId].x(parseFloat(d.speed))
      })

    allSvgs.selectAll("line")
      .transition()
      .attr("y2", self[chartId].height)
  },
})
$(document).ready(function() {
  d3.select("body")
    .style("visibility", "visible");
  var hasPym = false;
  try {
    var pymChild = new pym.Child({
      polling: 500
    });
    if (pymChild.id) {
      hasPym = true;
      $("section.content.reuters-graphics").addClass("pym")
    }
  } catch (err) {}

  $('ul.nav li a').on('click', function(event) {

    event.preventDefault();

    // $('body, html').animate({
    //   scrollTop: 0
    // });

    var $el = $(this);

    var id = $el.attr('href');

    id = id.replace("#", "")
    route(id);
  });
  var extraTop = 20;

  function sticky_relocate() {
    var windowWidth = $(window).width();
    if (windowWidth < 600) {
      extraTop = 0;
    } else {
      extraTop = 20
    }
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top - extraTop;
    if (window_top > div_top) {
      $('#sticky').addClass('stick');
    } else {
      $('#sticky').removeClass('stick');
    }
  }

  $(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
  });


  function route(id) {
    if (id == "calendar") {
      $(".toggle-box").css("display", "none")
    } else {
      $(".toggle-box").css("display", "block")
    }
    var $el = $("a[href='#" + id + "']").parent()

    var title = $("a[href='#" + id + "']").html()
    if ($el.hasClass('active')) {
      return;
    }
    d3.select("#section-label")
      .html(title)


    $el.addClass('active').siblings().removeClass('active');
    $("#navbar").collapse('hide')


    $('section.graphic-section.selected').removeClass('selected');
    $("#" + id).addClass('selected');
    if (hasPym == true) {
      pymChild.sendHeight()
    }


    clickRouter.updateRoute(id);

  }


  //router
  var buttonRouter = Backbone.Router.extend({
    routes: {
      "section-:id": "changeSection",
    },
    changeSection: function(id) {

      route(id);
    },
    updateRoute: function(id) {
      this.navigate("section-" + id);
    },
  });
  //does it matter where this goes?
  var clickRouter = new buttonRouter();

  Backbone.history.start();


  //	d3.json('data/f1Data2016.json', function(data){
  d3.json('http://graphics.thomsonreuters.com/sportsdata/f1/f1Data2017.json', function(data) {
    var driverObj = {}

    console.log(data.recaps)
    data.recaps.forEach(function(recap) {

      recap.drivers.forEach(function(d) {
        if (!driverObj[d.id]) {
          driverObj[d.id] = {
            rank1: 0,
            rank2: 0,
            rank3: 0,
            rank4: 0,
            rank5: 0,
            rank6: 0,
            rank7: 0,
            rank8: 0,
            rank9: 0,
            rank10: 0,
            rank11: 0,
            rank12: 0,
            rank13: 0,
            rank14: 0,
            rank15: 0,
            rank16: 0,
            rank17: 0,
            rank18: 0,
            rank19: 0,
            rank20: 0,
            rankundefined: 0,
          }
        }
        driverObj[d.id]["rank" + d.racestats.finish_position]++

      })
    })
    data.drivers.forEach(function(d) {
      d.bestRank = driverObj[d.id]
    })


    data.drivers.sort(function(a, b) {
      if (data.recaps.length == 1) {
        var aDriverRank, bDriverRank
        data.recaps[0].drivers.forEach(function(d, i) {
          if (d.driver.lastName == a.lastName) {
            aDriverRank = i
          }
          if (d.driver.lastName == b.lastName) {
            bDriverRank = i
          }
        })
        if (aDriverRank > bDriverRank) {
          return 1
        }
        if (aDriverRank < bDriverRank) {
          return -1
        }
        return 0
      }


      if (a.stats.points > b.stats.points) {
        return -1
      }
      if (a.stats.points < b.stats.points) {
        return 1
      }
      if (a.stats.points == 0 && b.stats.points == 0) {
        if (a.lastName > b.lastName) {
          return 1
        }
        if (a.lastName < b.lastName) {
          return -1
        }
      }
      if (a.stats.points == b.stats.points) {
        if (!a.bestRank) {
          return 1
        }
        if (!b.bestRank) {
          return -1
        }
        if (a.bestRank.rank1 > b.bestRank.rank1) {
          return -1
        }
        if (a.bestRank.rank1 < b.bestRank.rank1) {
          return 1
        }
        if (a.bestRank.rank2 > b.bestRank.rank2) {
          return -1
        }
        if (a.bestRank.rank2 < b.bestRank.rank2) {
          return 1
        }
        if (a.bestRank.rank3 > b.bestRank.rank3) {
          return -1
        }
        if (a.bestRank.rank3 < b.bestRank.rank3) {
          return 1
        }
        if (a.bestRank.rank4 > b.bestRank.rank4) {
          return -1
        }
        if (a.bestRank.rank4 < b.bestRank.rank4) {
          return 1
        }
        if (a.bestRank.rank5 > b.bestRank.rank5) {
          return -1
        }
        if (a.bestRank.rank5 < b.bestRank.rank5) {
          return 1
        }
        if (a.bestRank.rank6 > b.bestRank.rank6) {
          return -1
        }
        if (a.bestRank.rank6 < b.bestRank.rank6) {
          return 1
        }
        if (a.bestRank.rank7 > b.bestRank.rank7) {
          return -1
        }
        if (a.bestRank.rank7 < b.bestRank.rank7) {
          return 1
        }
        if (a.bestRank.rank8 > b.bestRank.rank8) {
          return -1
        }
        if (a.bestRank.rank8 < b.bestRank.rank8) {
          return 1
        }
        if (a.bestRank.rank9 > b.bestRank.rank9) {
          return -1
        }
        if (a.bestRank.rank9 < b.bestRank.rank9) {
          return 1
        }
        if (a.bestRank.rank10 > b.bestRank.rank10) {
          return -1
        }
        if (a.bestRank.rank10 < b.bestRank.rank10) {
          return 1
        }
        if (a.bestRank.rank11 > b.bestRank.rank11) {
          return -1
        }
        if (a.bestRank.rank11 < b.bestRank.rank11) {
          return 1
        }
        if (a.bestRank.rank12 > b.bestRank.rank12) {
          return -1
        }
        if (a.bestRank.rank12 < b.bestRank.rank12) {
          return 1
        }
        if (a.bestRank.rank13 > b.bestRank.rank13) {
          return -1
        }
        if (a.bestRank.rank13 < b.bestRank.rank13) {
          return 1
        }
        if (a.bestRank.rank14 > b.bestRank.rank14) {
          return -1
        }
        if (a.bestRank.rank14 < b.bestRank.rank14) {
          return 1
        }
        if (a.bestRank.rank15 > b.bestRank.rank15) {
          return -1
        }
        if (a.bestRank.rank15 < b.bestRank.rank15) {
          return 1
        }
        if (a.bestRank.rank16 > b.bestRank.rank16) {
          return -1
        }
        if (a.bestRank.rank16 < b.bestRank.rank16) {
          return 1
        }
        if (a.bestRank.rank17 > b.bestRank.rank17) {
          return -1
        }
        if (a.bestRank.rank17 < b.bestRank.rank17) {
          return 1
        }
        if (a.bestRank.rank18 > b.bestRank.rank18) {
          return -1
        }
        if (a.bestRank.rank18 < b.bestRank.rank18) {
          return 1
        }
        if (a.bestRank.rank19 > b.bestRank.rank19) {
          return -1
        }
        if (a.bestRank.rank19 < b.bestRank.rank19) {
          return 1
        }
        if (a.bestRank.rank20 > b.bestRank.rank20) {
          return -1
        }
        if (a.bestRank.rank20 < b.bestRank.rank20) {
          return 1
        }

      }
      return 0
    })
    var nopoints = 0;
    data.drivers.forEach(function(d, i) {
      nopoints += d.stats.points
    })
    data.drivers.forEach(function(d, i) {
      if (nopoints == 0) {
        d.stats.rank = 0
      } else {
        d.stats.rank = (i + 1)
      }
      if (d.lastName == "Ocon") {
        d.team = "Manor"
      }
    })

    console.log(data)
    data.teams.forEach(function(team) {
      var newDriverArray = []
      team.drivers.forEach(function(driver) {
        newDriverArray.push(driver)
      })
      team.drivers = newDriverArray
    })
    data.teams.sort(function(a, b) {
      var apoints = a.teamPoints
      var bpoints = b.teamPoints
      var aBest = 0
      var bBest = 0
      a.drivers.forEach(function(d) {
        if (d.driverInfo.bestPoints > aBest) {
          aBest = d.driverInfo.bestPoints
        }
      })
      b.drivers.forEach(function(d) {
        if (d.driverInfo.bestPoints > bBest) {
          bBest = d.driverInfo.bestPoints
        }
      })
      if (apoints > bpoints) {
        return -1
      }
      if (bpoints > apoints) {
        return 1
      }
      if (apoints == bpoints) {
        if (aBest > bBest) {
          return -1
        }
        if (bBest > aBest) {
          return 1
        }
      }
    })
    data.teams.forEach(function(d, i) {
      d.teamRank = i + 1
    })


    teamSlides = new teamChart({
      el: "#teams",
      dataURL: data,
      dataName: "teams",
      listTemplate: Reuters.Template.slideDownTeam,
      gridTemplate: Reuters.Template.gridTeam,
      gridTemplateOpen: Reuters.Template.gridTeamOpen
    });

    driverSlides = new driverChart({
      el: "#drivers",
      dataURL: data,
      dataName: "drivers",
      listTemplate: Reuters.Template.slideDownDriver,
      gridTemplate: Reuters.Template.gridDriver,
      gridTemplateOpen: Reuters.Template.gridDriverOpen
    });
    skedSlides = new slideDown({
      el: "#calendar",
      dataURL: data,
      dataName: "calendar",
      listTemplate: Reuters.Template.slideDownSked,
    });

    resultSlides = new slideDown({
      el: "#recaps",
      dataURL: data,
      dataName: "recaps",
      listTemplate: Reuters.Template.slideDownResult,
    });
    trackSlides = new trackChart({
      el: "#tracks",
      dataURL: data,
      dataName: "calendar",
      listTemplate: Reuters.Template.slideDownTrack,
      gridTemplate: Reuters.Template.gridTrack,
      gridTemplateOpen: Reuters.Template.slideOpenTrack

    });

    //button to toggle the view
    $(".toggle-box").on("click", function() {
      $(".list-or-grid").toggleClass("selected")
      $(this).toggleClass("clicked")
    })
    $(".margin-bar").tipsy({
      opacity: 1,
      gravity: 's',
      html: true
    });


  })









});

$(window).load(function() {


  d3.select("body")
    .style("visibility", "visible");
  /*

      try {
  		pymChild.sendHeight()
  	}
  	catch(err){
  	}

  */
});
