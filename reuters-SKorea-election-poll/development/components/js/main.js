(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["basicChartLayout"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="container">\n    <div class="row">\n        <div class="col-12">\n            <h1 class="graphic-title">' + ((__t = 'The title for a graphic') == null ? '' : __t) + '</h1>\n            <p class="graphic-subhead">' + ((__t = 'The subhead for the graphic The subhead for the graphic The subhead for the graphic The subhead for the graphic The subhead for the graphic The subhead for the graphic') == null ? '' : __t) + ' </p>\n        </div>\n    </div>\n    \n    <div class="row">\n        <div class="col-sm-6">\n            <p class="graphic-chart-label">' + ((__t = 'A label for a chart') == null ? '' : __t) + '</p>\n            <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>\n            <div id="reutersGraphic-chart1" class="reuters-chart"></div>\n        </div>\n\n        <div class="col-sm-6">\n            <p class="graphic-chart-label">' + ((__t = 'A label for a chart') == null ? '' : __t) + '</p>\n            <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>\n            <div id="reutersGraphic-chart2" class="reuters-chart"></div>\n        </div>\n\n    </div>\n        \n    <div class="row">            \n        <div class="col-12 mt-1">\n            <p class="graphic-source">' + ((__t = 'Sources: XXXXX YYYYY') == null ? '' : __t) + '<br />' + ((__t = 'By First Name Last Name | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>\n</div>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["candidateContent"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    t.election.candidates.forEach(function(d, i) {;
      __p += '\n   <div class="row">\n        <div id="candidates-images" class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n            <img src="images/candidates/' + ((__t = d.partyid) == null ? '' : __t) + '.png" class="candidate-image mx-auto d-block">\n\n        </div>\n    </div>\n\n    <div class="row">\n        <div class="col-md-6 offset-md-3 ">\n\n            <h4 class="font-weight-normal candidate-content-leader text-sm-center">' + ((__t = d.leader) == null ? '' : __t) + ', ' + ((__t = d.age) == null ? '' : __t) + '</h4>\n            <p class="text-sm-center mb-0"><strong class="text-uppercase">' + ((__t = d.party) == null ? '' : __t) + ' - </strong><strong>' + ((__t = d.partyinfo) == null ? '' : __t) + '</strong>  </p>\n            <p class="text-uppercase text-center mb-0"></p>\n            <!-- <p class="text-center candidate-content-lastelection"><strong>Bacgkround: </strong>' + ((__t = d.partyinfo) == null ? '' : __t) + '</p> -->\n            <p class="bottomline-text text-sm-center mb-2"><strong>Bottomline:</strong> ' + ((__t = d.bottomline) == null ? '' : __t) + '</p>\n       </div>\n    </div>\n\n    <div class="row">\n        ';
      t.election.issues.forEach(function(issue) {;
        __p += '\n            <div class="col-md-3 ">\n                <p class="text-uppercase mb-0 mt-0 font-weight-bold">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</p>\n                <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>\n            </div>\n        ';
      });
      __p += '\n    </div>\n\n        ';
      if (i != t.election.candidates.length - 1) {;
        __p += '\n            <hr>\n         ';
      };
      __p += '\n\n';
    });
    __p += '\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["featureLayout"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="feature-header container-fluid">\n    <img class="img-fluid feature-header-img mb-1" src="images/headerimg.jpg">\n    <div class="feature-header-title-container">\n        <div class="row">\n           <h1 class="display-3 col-md-5 text-sm-center text-md-left">' + ((__t = 'Headline goes here') == null ? '' : __t) + '</h1>\n        </div>\n        <div class="row">\n            <p class="graphic-subhead col-md-5 text-sm-center text-md-left">' + ((__t = 'Nice long dek can go here and here and here. Nice long dek can go here and here and here. Nice long dek can go here and here and here.gulp ') == null ? '' : __t) + '</p>\n        </div>\n        <div class="row">\n            <p class="graphic-timestamp col-md-5 text-sm-center text-md-left">' + ((__t = 'A timestamp. Updated October 7, 2016') == null ? '' : __t) + '</p>\n        </div>\n    </div>\n\n</div>\n\n<div class="container graphic-section-container">\n    \n        \n        <div class="row">\n            <div class="col-sm-6">\n                <p class="graphic-chart-label">' + ((__t = 'A label for a chart') == null ? '' : __t) + '</p>\n                <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>\n                <div id="reutersGraphic-chart1" class="reuters-chart"></div>\n            </div>\n    \n            <div class="col-sm-6">\n                <p class="graphic-chart-label">' + ((__t = 'A label for a chart') == null ? '' : __t) + '</p>\n                <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>\n                <div id="reutersGraphic-chart2" class="reuters-chart"></div>\n            </div>\n    \n        </div>\n    \n\n\n    <div class="row">            \n        <div class="col-12 mt-1">\n            <p class="graphic-source">' + ((__t = 'Sources: XXXXX YYYYY') == null ? '' : __t) + '<br />' + ((__t = 'By First Name Last Name | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>           \n\n</div>';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["issueContent"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    t.election.issues.forEach(function(issue, i) {;
      __p += '\n    <div class="row">\n              <div class="col-xs-6 offset-xs-3 col-sm-4 offset-sm-4 col-md-2 offset-md-5">\n                    <img src="images/icons/' + ((__t = issue) == null ? '' : __t) + '.png" class="candidate-image mx-auto d-block">\n              </div>\n    </div>\n\n    <div class="row">\n              <div class="col-sm-6 offset-sm-3">\n                    <h4 class="text-center mt-2">' + ((__t = t.election.issuelookup[issue]) == null ? '' : __t) + '</h4>\n              </div>\n                      \n    </div>\n\n  ';
      t.election.candidates.forEach(function(d) {;
        __p += '\n      <div class="row">\n\n                <div class="col-lg-1">            \n                </div> \n\n                <div class="col-md-4 col-lg-2 text-xs-center text-md-left issue-content-header">\n                    <p class="text-uppercase">' + ((__t = d.party) == null ? '' : __t) + '</p>\n                </div>\n\n\n                <div class="col-md-8  text-xs-left">\n                    <p class="issue-text">' + ((__t = d[issue]) == null ? '' : __t) + '</p>               \n                </div>\n\n                <div class="col-lg-1">            \n                </div> \n\n      </div>\n\n   ';
      });
      __p += '\n         \n        ';
      if (i != t.election.issues.length - 1) {;
        __p += '\n            <hr>\n         ';
      };
      __p += ' \n\n';
    });
    __p += '        \n\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["legendTemplateGDP"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '<div class=\'legendContainer\'>\n	<div class="legend-ital">' + ((__t = 'Hide/show') == null ? '' : __t) + '</div>\n	<div class="legend-items-holder">\n		';
    t.data.forEach(function(d) {;
      __p += '\n			<div class="legendItems">\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n				<div class="legendInline">\n					<div class="nameTip">	' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n				</div>\n			</div>\n		';
    });
    __p += '\n	</div>		\n</div>\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["legendTemplatePoll"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }
    __p += '<div class=\'legendContainer\'>\n	<div class="legend-ital">' + ((__t = 'Hide/show') == null ? '' : __t) + '</div>\n	<div class="legend-items-holder">\n		';
    t.data.forEach(function(d) {;
      __p += '\n			<div class="legendItems">\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n				<div class="legendInline">\n					<div class="nameTip">	' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n				</div>\n			</div>\n		';
    });
    __p += '\n	</div>		\n</div>\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["sample"] = function(t) {
    var __t,
      __p = '';
    __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["suiteLayout"] = function(t) {
    var __t,
      __p = '';
    __p += '<div class="mb-1  container" id="package-container">\n    <div class="row header-text-holder">\n        <h1 class="display-4 text-center col-sm-12 col-md-8 offset-md-2 ">' + ((__t = 'South Korea') == null ? '' : __t) + '<Br>' + ((__t = 'Presidential Election 2017') == null ? '' : __t) + '<Br>' + ((__t = "Who's leading?") == null ? '' : __t) + '</h1>\n\n        <p class="graphic-chart-subhead text-center col-md-6 offset-md-3 " id="decktText">' + ((__t = 'All the five presidential candidates and where they stand on key issues including their stance on North Korea.') == null ? '' : __t) + '</p>\n        <p id="time" class="graphic-timestamp col-sm-12 text-center ">' + ((__t = 'Published May 8, 2017') == null ? '' : __t) + '</p>\n        <div class="col-sm-12 col-md-8 offset-md-2  text-sm-center masthead-nav text-uppercase mt-1" style="display:none">\n            <div class="candidate-issues btn-group nav-options horizontal " data-toggle="buttons" id="section-buttons">\n                <label class="btn btn-link active" data-id="feature-nav-option-1">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off" checked>\n                    ' + ((__t = 'Candidates') == null ? '' : __t) + '\n                </label>\n                <label class="btn btn-link" data-id="feature-nav-option-2">\n                    <input type="radio" name="awesome-nav-options" autocomplete="off">\n                    ' + ((__t = 'Polls') == null ? '' : __t) + '\n                </label>\n\n\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class="container graphic-section-container">\n\n\n\n    <section class="graphic-section hideSection " id="feature-nav-option-1">\n\n\n	  <div id="candidate-sort" class="candidate-table selected"></div>\n\n\n    </section>\n\n\n      <section class="graphic-section selected" id="feature-nav-option-2">\n           <!-- <div class="row">\n              <div class="col-md-6 offset-md-3 text-center ">\n                  <h1 class="graphic-title">' + ((__t = 'Polls') == null ? '' : __t) + '</h1>\n                  <p class="graphic-subhead ">' + ((__t = 'Pollsters have been surveying voters more intensively since the start of the year. Poll aggregators have been collating the results of multiple surveys into a snapshot.') == null ? '' : __t) + ' </p>\n              </div>\n          </div> -->\n\n          <div class="row">\n              <div class="col-md-8 offset-md-2 ">\n\n                  <h1 class="graphic-title  mt-2 text-center" style="display:none">' + ((__t = "Who's leading?") == null ? '' : __t) + '</h1>\n                    <p class="graphic-chart-subhead text-sm-center mt-1" style="display:none">' + ((__t = 'The gap between Moon Jae-in and Ahn Cheol-soo closed in when both were officially nominated as candidates in early April. However Moon, who lost out to former President Park in the 2012 election, is still leading the polls. ') == null ? '' : __t) + '</p>\n                    <br>\n                  <p class="graphic-chart-label text-sm-center mt-1">' + ((__t = 'GALLUP KOREA POLL') == null ? '' : __t) + '</p>\n                <!--  <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>-->\n                   <div id="reutersGraphic-chart-poll1" class="reuters-chart"></div>\n           <p class="graphic-source">\n                      ' + ((__t = 'Sample size between: 1,001-1,026 | Margin of error: +/- 3.1% pts.') == null ? '' : __t) + '</p>\n              </div>\n\n\n          </div>\n      <br>\n          <div class="row">\n              <div class="col-md-8 offset-md-2 ">\n                  <p class="graphic-chart-label text-sm-center">' + ((__t = 'REALMETER POLL') == null ? '' : __t) + '</p>\n                <!--  <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>-->\n                   <div id="reutersGraphic-chart-poll2" class="reuters-chart"></div>\n           <p class="graphic-source">\n                      ' + ((__t = 'Sample size between: 2,035-2,533 | Margin of error: +/- 1.96% pts.') == null ? '' : __t) + '<br>\n                      ' + ((__t = 'Note: Gaps in Realmeter poll results are either due to poll numbers being too low or uncertainty of candidacy.') == null ? '' : __t) + '</p>\n              </div>\n\n\n          </div>\n\n\n\n      </section>\n\n\n   \n\n</div>\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["tooltipGDP"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.data[0].displayDate) {;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + '\n\n	\n	\n	</div>\n';
    } else {;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
    };
    __p += '\n';
    t.data.forEach(function(d, i) {;
      __p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
      if (t.self.chartLayout == "stackPercent") {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
        __p += '				\n				';
      } else {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d[t.self.dataType]));
        __p += '				\n				';
      };
      __p += '\n			</div>\n	\n		</div>\n';
    });
    __p += '	\n';
    if (t.self.timelineData) {
      var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
      print(t.self.timelineTemplate({
        data: timelineData,
        self: t.self
      }));
    };
    __p += '	\n\n';
    return __p;
  };
})();
(function() {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["tooltipPoll"] = function(t) {
    var __t,
      __p = '',
      __j = Array.prototype.join;

    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.data[0].displayDate) {;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
    } else {;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
    };
    __p += '\n';
    t.data.forEach(function(d, i) {;
      __p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
      if (t.self.chartLayout == "stackPercent") {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
        __p += '				\n				';
      } else {;
        __p += '\n					';
        print(t.self.tipNumbFormat(d[t.self.dataType]));
        __p += '				\n				';
      };
      __p += '\n			</div>\n	\n		</div>\n';
    });
    __p += '	\n';
    if (t.self.timelineData) {
      var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
      print(t.self.timelineTemplate({
        data: timelineData,
        self: t.self
      }));
    };
    __p += '	\n\n';
    return __p;
  };
})();
//for translations.
window.gettext = function(text) {
  return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function(hashBased) {
  var query = void 0;
  if (hashBased) {
    var pos = location.href.indexOf('?');
    if (pos == -1) return [];
    query = location.href.substr(pos + 1);
  } else {
    query = location.search.substr(1);
  }
  var result = {};
  query.split('&').forEach(function(part) {
    if (!part) return;
    part = part.split('+').join(' '); // replace every + with space, regexp-free version
    var eq = part.indexOf('=');
    var key = eq > -1 ? part.substr(0, eq) : part;
    var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

    //convert true / false to booleans.
    if (val == 'false') {
      val = false;
    } else if (val == 'true') {
      val = true;
    }

    var f = key.indexOf('[');
    if (f == -1) {
      result[decodeURIComponent(key)] = val;
    } else {
      var to = key.indexOf(']');
      var index = decodeURIComponent(key.substring(f + 1, to));
      key = decodeURIComponent(key.substring(0, f));
      if (!result[key]) {
        result[key] = [];
      }
      if (!index) {
        result[key].push(val);
      } else {
        result[key][index] = val;
      }
    }
  });
  return result;
};

Reuters.trackEvent = function(category, type, id) {
  category = category || 'Page click';
  //console.log(category, type, id);
  var typeString = type;
  if (id) {
    typeString += ': ' + id;
  }
  var gaOpts = {
    'nonInteraction': false,
    'page': PAGE_TO_TRACK
  };

  ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function() {
  $('[data-slider]').each(function() {
    var $el = $(this);
    var getPropArray = function getPropArray(value) {
      if (!value) {
        return [0];
      }
      var out = [];
      var values = value.split(',');
      values.forEach(function(value) {
        out.push(parseFloat(value));
      });
      return out;
    };
    var pips = undefined;
    var start = getPropArray($el.attr('data-start'));
    var min = getPropArray($el.attr('data-min'));
    var max = getPropArray($el.attr('data-max'));
    var orientation = $el.attr('data-orientation') || 'horizontal';
    var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
    var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
    var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
    var snap = $el.attr('data-snap') === 'true' ? true : false;
    var pipMode = $el.attr('data-pip-mode');
    var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
    var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
    var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
    if (pipMode === 'count') {
      pipValues = pipValues[0];
    }

    if (pipMode) {
      pips = {
        mode: pipMode,
        values: pipValues,
        stepped: pipStepped,
        density: pipDensity
      };
    }

    if (connect) {
      var cs = [];
      connect.split(',').forEach(function(c) {
        c = c === 'true' ? true : false;
        cs.push(c);
      });
      connect = cs;
    }

    noUiSlider.create(this, {
      start: start,
      range: {
        min: min,
        max: max
      },
      snap: snap,
      orientation: orientation,
      step: step,
      tooltips: tooltips,
      connect: connect,
      pips: pips
    });
    //This probably doesn't belong here, but will fix the most common use-case.
    $(this).find('div.noUi-marker-large:last').addClass('last');
    $(this).find('div.noUi-marker-large:first').addClass('first');
  });
};

Reuters.hasPym = false;
try {
  Reuters.pymChild = new pym.Child({
    polling: 500
  });
  if (Reuters.pymChild.id) {
    Reuters.hasPym = true;
    $("body").addClass("pym");
  }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
  $("html").addClass("media-flat");
}
if (Reuters.Graphics.Parameters.eikon) {
  $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
  $("html").addClass("remove-header");
}
//# sourceMappingURL=utils.js.map


$(".main").html(Reuters.Graphics.Template.basicChartLayout());

$(".main").html(Reuters.Graphics.Template.featureLayout());

d3.json("//d3sl9l9bcxfb5q.cloudfront.net/json/skorea-election-candidates", function(data) {
  var election = {
    issues: ["security", "economy", "welfare", "societygovernance"],
    candidates: data,
    issuelookup: {
      security: "N. Korea & National Security",
      economy: "Economy",
      welfare: "Welfare",
      societygovernance: "Society & governance"
    }
  };

  $("#candidate-sort").html(Reuters.Graphics.Template.candidateContent({
    election: election
  }));
  $("#issue-sort").html(Reuters.Graphics.Template.issueContent({
    election: election
  }));
});

Reuters.Graphics.FeaturePage = function(_Backbone$View) {
  babelHelpers.inherits(FeaturePage, _Backbone$View);

  function FeaturePage() {
    babelHelpers.classCallCheck(this, FeaturePage);
    return babelHelpers.possibleConstructorReturn(this, (FeaturePage.__proto__ || Object.getPrototypeOf(FeaturePage)).apply(this, arguments));
  }

  babelHelpers.createClass(FeaturePage, [{
    key: "preinitialize",
    value: function preinitialize() {
      this.events = {
        'change .nav-options .btn': 'onSectionChange'
      };
      this.router = new Reuters.Graphics.FeaturePageRouter();
    }
  }, {
    key: "initialize",
    value: function initialize(options) {
      this.render();
      this.listenTo(this.router, 'route:section', this.changeSection);
    }
  }, {
    key: "render",
    value: function render() {
      console.log('rendering feature pages');
      this.$el.html(Reuters.Graphics.Template.suiteLayout());
      this.createCharts();
      return this;
    }
  }, {
    key: "createCharts",
    value: function createCharts() {
      Reuters.Graphics.sharePrice = new Reuters.Graphics.LineChart({
        el: "#reutersGraphic-chart-poll1",
        dataURL: 'http://sfb-proxy-prod-1197393060.us-east-1.elb.amazonaws.com/json/skorea-pres-elex-poll-gallup',
        height: 300, //if < 10 - ratio , if over 10 - hard height.  undefined - square
        columnNames: {
          moonjaein: "Moon Jae-in",
          ahncheoisoo: "Ahn Cheol-soo",
          hongjunpyo: "Hong Jun-pyo",
          shimsangjung: "Shim Sang-jung",
          yooseongmin: "Yoo Seong-min"
        },
        colors: {
          moonjaein: navy4,
          ahncheoisoo: green4,
          hongjunpyo: red3,
          shimsangjung: tangerine4,
          yooseongmin: cyan3
        },
        //			dataType:'value',//value, changePreMonth, CumulativeChange, percentChange, cumulate
        YTickLabel: [
          ["", "%"]
        ], //  \u00A0  - use that code for a space.
        //			xScaleTicks: 5,
        //			yScaleTicks:5,
        dateFormat: d3.time.format("%b %d %Y"),
        numbFormat: d3.format(",.1f"),
        //			divisor:1000,

        //			columnNamesDisplay:["Bob","Jerry"], // only use this if you are using an array for columnNames
        //			colorUpDown:true,
        //			hasLegend: false,
        //			showTip:false,
        //			yScaleVals: [0,100],
        //			tickAll:true, //if you make tickAll anything, it will put a tick for each point.
        //			horizontal:true,
        //			margin: {top: 60, right: 80, bottom: 60, left: 130},
        //			groupSort:"ascending", // ascending descending or array
        //			categorySort:"ascending", //ascending descending, array or alphabetical
        //			parseDate:d3.time.format("%d/%m/%y").parse // can change the format of the original dates
        //			hasRecessions: true,
        //			hasZoom: true,
        //			dataStream:true,
        //			timelineData:"data/timeline.csv", //dates much match source dates
        //			tipNumbFormat: function(d){
        //				var self = this;
        //				if (isNaN(d) === true){return "N/A";}else{
        //					return self.dataLabels[0] + self.numbFormat(d) + " " + self.dataLabels[1] ;
        //				}
        //			},
        //			lineType: "linear",//step-before, step-after
        chartBreakPoint: 400
      });

      Reuters.Graphics.sharePrice = new Reuters.Graphics.LineChart({
        el: "#reutersGraphic-chart-poll2",
        dataURL: 'http://sfb-proxy-prod-1197393060.us-east-1.elb.amazonaws.com/json/skorea-pres-elex-poll-realmeter',
        height: 300, //if < 10 - ratio , if over 10 - hard height.  undefined - square
        columnNames: {
          moonjaein: "Moon Jae-in",
          ahncheoisoo: "Ahn Cheol-soo",
          hongjunpyo: "Hong Jun-pyo",
          shimsangjung: "Shim Sang-jung",
          yooseongmin: "Yoo Seong-min"
        },
        colors: {
          moonjaein: navy4,
          ahncheoisoo: green4,
          hongjunpyo: red3,
          shimsangjung: tangerine4,
          yooseongmin: cyan3
        },
        YTickLabel: [
          ["", "%"]
        ],
        numbFormat: d3.format(",.1f"),
        dateFormat: d3.time.format("%b %d %Y"),
        chartBreakPoint: 400
      });

      /*
          	// ADDD CHARTS IN HERE
      		this.sharePrice = new Reuters.Graphics.LineChart({
      			el: "#reutersGraphic-chart1",
      			dataURL: 'http://d3sl9l9bcxfb5q.cloudfront.net/json/mw-disney-earns',
      			columnNames:{sandp:"S&P 500", disney:"Disney"}, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
      			height:200, //if < 10 - ratio , if over 10 - hard height.  undefined - square
      			multiDataColumns:["value","percentChange"],//can use value,changePreMonth, CumulativeChange, percentChange
      			multiDataLabels:["VALUE","PERCENT"]

      		});
      */

      // 		this.charts = [this.sharePrice, this.otherSharePrice];
    }
  }, {
    key: "onSectionChange",
    value: function onSectionChange(event) {
      var $el = $(event.currentTarget);
      var id = $el.attr('data-id');
      this.changeSection(id, $el);
    }
  }, {
    key: "changeSection",
    value: function changeSection(id, $button) {
      console.log('changing section fired', id, $button);
      var $el = this.$('#' + id);
      if ($el.hasClass('selected')) {
        return;
      }
      if (!$button) {
        $button = this.$(".nav-options .btn[data-id=\"" + id + "\"]");
        $button.addClass('active').siblings().removeClass('active');
        $button.find('input').addClass('active').attr('checked', 'checked');
      }
      $el.addClass('selected').siblings().removeClass('selected');
      this.router.navigate('section/' + id, {
        trigger: false
      });
      // 		_.invoke(this.charts, 'update');
    }
  }]);
  return FeaturePage;
}(Backbone.View);

Reuters.Graphics.FeaturePageRouter = function(_Backbone$Router) {
  babelHelpers.inherits(FeaturePageRouter, _Backbone$Router);

  function FeaturePageRouter() {
    babelHelpers.classCallCheck(this, FeaturePageRouter);
    return babelHelpers.possibleConstructorReturn(this, (FeaturePageRouter.__proto__ || Object.getPrototypeOf(FeaturePageRouter)).apply(this, arguments));
  }

  babelHelpers.createClass(FeaturePageRouter, [{
    key: "preinitialize",
    value: function preinitialize() {
      this.routes = {
        'section/:id': 'section'
      };
    }
  }]);
  return FeaturePageRouter;
}(Backbone.Router);

$(document).ready(function() {
  window.featurePageExample = new Reuters.Graphics.FeaturePage({
    el: '.main'
  });
  Backbone.history.start();
});
//# sourceMappingURL=main.js.map
