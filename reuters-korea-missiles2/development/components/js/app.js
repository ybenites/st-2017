import Backbone from "backbone";

import './babel-helpers';
import _ from "underscore";

var textures = require("textures");

require('./tooltip/tooltip');

window.Reuters = window.Reuters || {};
window.Reuters.LANGUAGE = 'en';

Reuters.Locales = {

	en:{
		decimal:".",
		thousands:",",
		grouping:[3],
		currency:["$",""],
		dateTime:"%a %b %e %X %Y",
		date:"%m/%d/%Y",
		time:"%H:%M:%S",
		periods:["AM","PM"],
		days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
		shortDays:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],
		months:["January","February","March","April","May","June","July","August","September","October","November","December"],
		"shortMonths":["Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"]
	},

	es:{
		"decimal": ",",
		"thousands": ".",
		"grouping": [3],
		"currency": ["$", ""],
		"dateTime": "%a %b %e %X %Y",
		"date": "%m/%d/%Y",
		"time": "%H:%M:%S",
		"periods": ["AM", "PM"],
		"days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes","Sábado"],
		"shortDays": ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		"months": ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		"shortMonths": ["Ene", "Feb", "Mar", "Abr", "Mayo", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
	},

	fr:{
		"decimal": ",",
		"thousands": ".",
		"grouping": [3],
		"currency": ["$", ""],
        "dateTime": "%A, le %e %B %Y, %X",
        "date": "%d/%m/%Y",
        "time": "%H:%M:%S",
        "periods": ["AM", "PM"],
        "days": ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
        "shortDays": ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
        "months": ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
        "shortMonths": ["janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."]
	},

	ch:{
		decimal:".",
		thousands:",",
		grouping:[3],
		currency:["¥",""],
		dateTime:"%a %b %e %X %Y",
		date:"%d/%m/%Y",
		time:"%H:%M:%S",
		periods:["AM","PM"],
		days:["周日","周一","周二","周三","周四","周五","周六"],
		shortDays:["周日","周一","周二","周三","周四","周五","周六"],
		months:["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"],
		"shortMonths":["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"]
	},

	pt:{
		"decimal": ",",
		"thousands": ".",
		"grouping": [3],
		"currency": ["$", ""],
		"dateTime": "%a %b %e %X %Y",
		"date": "%m/%d/%Y",
		"time": "%H:%M:%S",
		"periods": ["AM", "PM"],
		"days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes","Sábado"],
		"shortDays": ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		"months": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		"shortMonths": ["Jan", "Fev", "Mar", "Abr", "Maio", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
	}

}

Reuters.CurrentLocale = d3.locale(Reuters.Locales[Reuters.LANGUAGE]);
d3.format = Reuters.CurrentLocale.numberFormat;
d3.time.format = Reuters.CurrentLocale.timeFormat;
d3.time.formatMulti = Reuters.CurrentLocale.timeFormat.multi;
(function () {
	window["Reuters"] = window["Reuters"] || {};
	window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
	window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
	window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

	window["Reuters"]["Graphics"]["basicCharter"]["Template"]["basictemplate"] = function (t) {
		var __t,
		    __p = '';
		__p += '';
		return __p;
	};
})();
(function () {
	window["Reuters"] = window["Reuters"] || {};
	window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
	window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
	window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

	window["Reuters"]["Graphics"]["basicCharter"]["Template"]["chartTemplate"] = function (t) {
		var __t,
		    __p = '',
		    __j = Array.prototype.join;
		function print() {
			__p += __j.call(arguments, '');
		}

		if (t.self.multiDataColumns) {
			;
			__p += '\n	<div class="chart-nav">\n		<div class="navContainer">\n            <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                ';
			t.self.multiDataColumns.forEach(function (d, i) {
				;
				__p += '                \n                    <label dataid="' + ((__t = d) == null ? '' : __t) + '" class="btn btn-primary ';
				if (i == t.self.multiDataLabels.length - 1) {
					;
					__p += 'active';
				};
				__p += ' smaller">\n                        <input type="radio" name="nav-options" autocomplete="off"> \n                        ' + ((__t = t.self.multiDataLabels[i]) == null ? '' : __t) + '\n                    </label>\n                ';
			});
			__p += '\n            </div>    		    		\n		</div>    	\n	</div>\n';
		};
		__p += '\n';
		if (t.self.navSpacer) {
			;
			__p += '\n	<div class="chart-nav">\n		<div class="navContainer"></div>\n	</div>\n';
		};
		__p += '\n\n';
		if (t.self.chartLayoutLables) {
			;
			__p += '\n	<div class="chart-layout"></div>\n';
		};
		__p += '\n<div class="chart-holder \n	';
		if (!t.self.hasLegend) {
			print('no-legend');
		};
		__p += '\n	">\n	<div class="legend nested-legend"></div>\n	<div class="chart nested-chart"></div>\n</div>';
		return __p;
	};
})();
(function () {
	window["Reuters"] = window["Reuters"] || {};
	window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
	window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
	window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

	window["Reuters"]["Graphics"]["basicCharter"]["Template"]["legendTemplate"] = function (t) {
		var __t,
		    __p = '',
		    __j = Array.prototype.join;
		function print() {
			__p += __j.call(arguments, '');
		}
		__p += '<div class=\'legendContainer\'>\n	<div class="legend-ital">' + ((__t = 'Hide/show') == null ? '' : __t) + '</div>\n	<div class=\'dateTip\'> \n		';

		if (t.data[0].displayDate) {
			print(t.data[0].displayDate);
		}
		if (t.data[0].category) {
			print(t.data[0].category);
		}
		if (!t.data[0].category && !t.data[0].displayDate) {
			print('<br>');
		};
		__p += '\n	</div>\n	<div class="legend-items-holder">\n		';
		t.data.forEach(function (d) {
			;
			__p += '\n			<div class="legendItems">\n				<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
			print(t.self.colorScale(d.name));
			__p += ';\'></div>\n				<div class="legendInline">\n					<div class="nameTip">	' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n					<div class=\'valueTip\'>\n						';
			if (d[t.self.dataType]) {
				print(t.self.tipNumbFormat(d[t.self.dataType]));
			} else {
				print("<br>");
			};
			__p += '\n					</div>\n				</div>\n			</div>\n		';
		});
		__p += '\n	</div>		\n</div>\n\n';
		return __p;
	};
})();
(function () {
	window["Reuters"] = window["Reuters"] || {};
	window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
	window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
	window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

	window["Reuters"]["Graphics"]["basicCharter"]["Template"]["tooltip"] = function (t) {
		var __t,
		    __p = '',
		    __j = Array.prototype.join;
		function print() {
			__p += __j.call(arguments, '');
		}

		if (t.data[0].displayDate) {
			;
			__p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
		} else {
			;
			__p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
		};
		__p += '\n';
		t.data.forEach(function (d, i) {
			;
			__p += '\n		<div class="tipHolder">\n			<div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
			print(t.self.colorScale(d.name));
			__p += ';\'></div>\n			<div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div>\n			<div class=\'valueTip\'>\n				';
			if (t.self.chartLayout == "stackPercent") {
				;
				__p += '\n					';
				print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
				__p += '				\n				';
			} else {
				;
				__p += '\n					';
				print(t.self.tipNumbFormat(d[t.self.dataType]));
				__p += '				\n				';
			};
			__p += '\n			</div>\n	\n		</div>\n';
		});
		__p += '	\n';
		if (t.self.timelineData) {
			var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
			print(t.self.timelineTemplate({ data: timelineData, self: t.self }));
		};
		__p += '	';
		return __p;
	};
})();
(function () {
	window["Reuters"] = window["Reuters"] || {};
	window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
	window["Reuters"]["Graphics"]["basicCharter"] = window["Reuters"]["Graphics"]["basicCharter"] || {};
	window["Reuters"]["Graphics"]["basicCharter"]["Template"] = window["Reuters"]["Graphics"]["basicCharter"]["Template"] || {};

	window["Reuters"]["Graphics"]["basicCharter"]["Template"]["tooltipTimeline"] = function (t) {
		var __t,
		    __p = '',
		    __j = Array.prototype.join;
		function print() {
			__p += __j.call(arguments, '');
		}
		__p += '<hr>\n';
		t.data.forEach(function (d, i) {
			;
			__p += '\n	<div class="tip-Timeline-date text-uppercase">\n		';
			print(t.self.timelineDateDisplay(d.parsedDate));
			__p += '\n	</div>\n	<div class="tip-Timeline">\n		' + ((__t = d.item) == null ? '' : __t) + '\n	</div>\n\n	';
			if (i != t.data.length - 1) {
				print('<br>');
			};
			__p += '\n\n';
		});
		__p += ' ';
		return __p;
	};
})();
var forest1 = "#C4D6C4";
var forest2 = "#A5C3A8";
var forest3 = "#73A97F";
var forest4 = "#008C4E";
var forest5 = "#007741";
var forest6 = "#005027";
var green1 = "#CBE1C8";
var green2 = "#AED3AB";
var green3 = "#7DBE80";
var green4 = "#0AA74B";
var green5 = "#008C3E";
var green6 = "#005E25";
var olive1 = "#E0EDCB";
var olive2 = "#D0E4AF";
var olive3 = "#B4D682";
var olive4 = "#8FC641";
var olive5 = "#74A535";
var olive6 = "#476E1E";
var lime1 = "#EFF4CC";
var lime2 = "#E8EEAF";
var lime3 = "#DBE580";
var lime4 = "#CADB2E";
var lime5 = "#A6B626";
var lime6 = "#6A7812";
var yellow1 = "#FCF8CD";
var yellow2 = "#FBF5B0";
var yellow3 = "#F9F17E";
var yellow4 = "#F6EB0E";
var yellow5 = "#CAC313";
var yellow6 = "#838103";
var tangerine1 = "#FFECC6";
var tangerine2 = "#FFE3A7";
var tangerine3 = "#FFD576";
var tangerine4 = "#FDC218";
var tangerine5 = "#D0A115";
var tangerine6 = "#886A00";
var orange1 = "#FDD5BB";
var orange2 = "#FBBE99";
var orange3 = "#F79967";
var orange4 = "#F26725";
var orange5 = "#C8551D";
var orange6 = "#843401";
var red1 = "#FBC9BA";
var red2 = "#F8AB98";
var red3 = "#F37B68";
var red4 = "#EC2033";
var red5 = "#C31729";
var red6 = "#82000D";
var rose1 = "#E9C3C8";
var rose2 = "#DFA4AE";
var rose3 = "#D17589";
var rose4 = "#C02460";
var rose5 = "#A11950";
var rose6 = "#6B0031";
var violet1 = "#DABFD1";
var violet2 = "#C99FBB";
var violet3 = "#B1709B";
var violet4 = "#952978";
var violet5 = "#7E1E65";
var violet6 = "#530041";
var purple1 = "#CABDDC";
var purple2 = "#B19CC9";
var purple3 = "#8D6EAE";
var purple4 = "#653290";
var purple5 = "#552479";
var purple6 = "#360451";
var navy1 = "#BCC2E0";
var navy2 = "#9BA4CF";
var navy3 = "#697CB8";
var navy4 = "#0F519F";
var navy5 = "#0A4286";
var navy6 = "#002459";
var blue1 = "#C8DAF0";
var blue2 = "#ABC8E8";
var blue3 = "#7AADDC";
var blue4 = "#1F8FCE";
var blue5 = "#1B78AC";
var blue6 = "#044F74";
var cyan1 = "#CFE8F9";
var cyan2 = "#B5DDF6";
var cyan3 = "#86CCF1";
var cyan4 = "#2AB8EB";
var cyan5 = "#259AC5";
var cyan6 = "#0C6785";
var gray1 = "#DCDDDE";
var gray2 = "#BCBEC0";
var gray3 = "#939598";
var gray4 = "#6D6E71";
var gray5 = "#4D4D4F";
var gray6 = "#414042";

var grey1 = "#AFBABF";
var grey2 = "#D4D8DA";
var black = "#231F20";
var white = "#FFFFFF";


(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["scatterCharter"] = window["Reuters"]["Graphics"]["scatterCharter"] || {};
  window["Reuters"]["Graphics"]["scatterCharter"]["Template"] = window["Reuters"]["Graphics"]["scatterCharter"]["Template"] || {};

  window["Reuters"]["Graphics"]["scatterCharter"]["Template"]["scatterSetupTemplate"] = function (t) {
    var __t,
        __p = '',
        __j = Array.prototype.join;
    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.self.dataType) {
      ;
      __p += '\n	<div class="chart-nav">\n			';
      if (!t.self.multiDataSlider) {
        ;
        __p += '		\n            		<div class="navContainer">\n                        <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                            ';
        t.self.multiDataLabels.forEach(function (d, i) {
          ;
          __p += '\n                                <label dataid="' + ((__t = d) == null ? '' : __t) + '" class="btn btn-primary ';
          if (i == t.self.multiDataLabels.length - 1) {
            ;
            __p += 'active';
          };
          __p += ' smaller">\n                                    <input type="radio" name="nav-options" autocomplete="off"> \n                                    ' + ((__t = d) == null ? '' : __t) + '\n                                </label>\n                            ';
        });
        __p += '\n                        </div>    		    		\n            		</div>    	\n			';
      } else {
        ;
        __p += '\n				<div class="slider-container">\n                    <div class="slider-holder">\n        				<div class="slider" data-slider="true"></div>\n                    </div>\n\n                    <div class="slider-controls">\n                        <div class="btn-group animation-control" data-toggle="buttons">\n                            <label class="btn btn-primary smaller animation-play">\n                                <input type="radio" name="animation-control-group" id="animation-play" autocomplete="off" > \n                                <i class="fa fa-play" aria-hidden="true"></i>\n                            </label>\n                            <label class="btn btn-primary smaller active animation-pause">\n                                <input type="radio" name="animation-control-group" id="animation-pause" autocomplete="off" checked>\n                                <i class="fa fa-pause" aria-hidden="true"></i>\n                            </label>\n                        </div>\n                    </div>\n				</div>\n			';
      };
      __p += '\n	</div>\n';
    };
    __p += '\n\n\n<div class="chart-holder">\n    ';
    if (t.self.colorDomain && t.self.colorDomain.length > 1) {
      ;
      __p += '\n    	<div class="scatter-nested-legend">\n            ';
      t.self.colorDomain.forEach(function (d, i) {
        ;
        __p += '\n                <div class ="scatter-legend-item" data-id="' + ((__t = d) == null ? '' : __t) + '">\n                	<div class = "scatter-legend-circle circle" style="background-color:' + ((__t = t.self.colors[i]) == null ? '' : __t) + ';"></div>\n                	<p class = "scatter-legend-text">' + ((__t = d) == null ? '' : __t) + '</p>\n                </div>\n            ';
      });
      __p += '\n        	';
      if (t.self.rvalue) {
        ;
        __p += '\n                <br>\n                <div class ="scatter-legend-size">\n                    <div class = "scatter-legend-circle scatter-size circle order-legend"></div>\n                    <p class = "scatter-legend-text">' + ((__t = 'Size indicates Orders') == null ? '' : __t) + '</p>\n                 </div>\n        	';
      };
      __p += ' \n            ';
      if (t.self.dropdown) {
        ;
        __p += '\n                <div class="mt-2 hidden-sm-down">\n                    <select class="custom-select scatter-select">\n                        <option selected>Show All ...   </option>\n                    </select>\n                    <small class="text-muted text-uppercase d-block">Choose to highlight</small>\n                </div>\n            ';
      };
      __p += '        	        \n    	</div>\n        <div class="scatter-nested-chart" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + '-chart"></div>\n    ';
    } else {
      ;
      __p += '\n        ';
      if (t.self.dropdown) {
        ;
        __p += '\n            <div class="mt-2 hidden-sm-down">\n                <select class="custom-select scatter-select">\n                    <option selected>Show All ...   </option>\n                </select>\n                <small class="text-muted text-uppercase d-block">Choose to highlight</small>\n            </div>\n        ';
      };
      __p += '         \n        <div class="" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + '-chart"></div>\n    ';
    };
    __p += '\n</div>\n\n\n\n\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["scatterCharter"] = window["Reuters"]["Graphics"]["scatterCharter"] || {};
  window["Reuters"]["Graphics"]["scatterCharter"]["Template"] = window["Reuters"]["Graphics"]["scatterCharter"]["Template"] || {};

  window["Reuters"]["Graphics"]["scatterCharter"]["Template"]["scattertooltip"] = function (t) {
    var __t,
        __p = '';
    __p += '<p class="tooltip-title"> ' + ((__t = t.data.name + " " + t.data.category) == null ? '' : __t) + '</p>\n<p class="tooltip-text"><strong> ' + ((__t = 'Passengers') == null ? '' : __t) + ':</strong> ' + ((__t = t.self.noDecimal(t.data[t.self.yvalue])) == null ? '' : __t) + ' </p>\n<p class="tooltip-text"><strong> ' + ((__t = 'Range') == null ? '' : __t) + ':</strong> ' + ((__t = t.self.noDecimal(t.data[t.self.xvalue])) == null ? '' : __t) + '  miles</p>\n<hr>\n\n<p class="tooltip-subhead">' + ((__t = 'On order') == null ? '' : __t) + '</p>\n<p class="tooltip-text">' + ((__t = 'Number unavailable') == null ? '' : __t) + '</p>';
    return __p;
  };
})();



window["Reuters"] = window["Reuters"] || {};
window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
window["Reuters"]["Graphics"]["ChartBase"] = window["Reuters"]["Graphics"]["ChartBase"] || {};

window["Reuters"] = window["Reuters"] || {};
window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
window["Reuters"]["Graphics"]["BarChart"] = window["Reuters"]["Graphics"]["BarChart"] || {};

window["Reuters"] = window["Reuters"] || {};
window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
window["Reuters"]["Graphics"]["ScatterPlot"] = window["Reuters"]["Graphics"]["ScatterPlot"] || {};

window["Reuters"] = window["Reuters"] || {};
window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
window["Reuters"]["Graphics"]["ScatterModel"] = window["Reuters"]["Graphics"]["ScatterModel"] || {};

window["Reuters"] = window["Reuters"] || {};
window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
window["Reuters"]["Graphics"]["ScatterCollection"] = window["Reuters"]["Graphics"]["ScatterCollection"] || {};

// Reuters = Reuters || {};
// Reuters.Graphics = Reuters.Graphics || {};
//the view that constructs a linechart
Reuters.Graphics.ChartBase = Backbone.View.extend({
	groupSort: "descending",
	categorySort: "ascending",
	margin: { top: 40, right: 20, bottom: 30, left: 40 },
	dateFormat: d3.time.format("%b %Y"),
	tipTemplate: Reuters.Graphics.basicCharter.Template.tooltip,
	chartTemplate: Reuters.Graphics.basicCharter.Template.chartTemplate,
	legendTemplate: Reuters.Graphics.basicCharter.Template.legendTemplate,
	tipNumbFormat: function tipNumbFormat(d) {
		var self = this;
		if (isNaN(d) === true) {
			return "N/A";
		} else {
			return self.dataLabels[0] + self.numbFormat(d) + self.dataLabels[1];
		}
	},
	colors: [blue3, purple3, orange3, red3, yellow3],
	dataType: 'value',
	xScaleTicks: 5,
	yScaleTicks: 5,
	YTickLabel: [["", ""]],
	numbFormat: d3.format(",.0f"),
	lineType: "linear",
	chartBreakPoint: 300,
	hasLegend: true,
	xOrY: "x",
	yOrX: "y",
	leftOrTop: "left",
	heightOrWidth: "height",
	widthOrHeight: "width",
	topOrLeft: "top",
	recessionDateParse: d3.time.format("%m/%d/%Y").parse,
	updateCount: 0,
	divisor: 1,
	timelineDate: d3.time.format("%m/%d/%Y"),
	timelineDateDisplay: d3.time.format("%b %e, %Y"),
	timelineTemplate: Reuters.Graphics.basicCharter.Template.tooltipTimeline,
	xTickFormat: Reuters.CurrentLocale.timeFormat.multi([["%H:%M", function (d) {
		return d.getMinutes();
	}], ["%H:%M", function (d) {
		return d.getHours();
	}], ["%a %d", function (d) {
		return d.getDay() && d.getDate() != 1;
	}], ["%b %d", function (d) {
		return d.getDate() != 1;
	}], ["%B", function (d) {
		return d.getMonth();
	}], ["%Y", function () {
		return true;
	}]]),
	initialize: function initialize(opts) {
		var self = this;
		this.options = opts;

		// if we are passing in options, use them instead of the defualts.
		_.each(opts, function (item, key) {
			self[key] = item;
		});

		if (self.timelineData) {
			self.loadTimelineData();
		} else {
			self.loadData();
		}
	},
	loadData: function loadData() {
		var self = this;
		//Test which way data is presented and load appropriate way
		if (this.dataURL.indexOf("csv") == -1 && !_.isObject(this.dataURL)) {
			d3.json(self.dataURL, function (data) {
				self.parseData(data);
			});
		}
		if (this.dataURL.indexOf("csv") > -1) {
			d3.csv(self.dataURL, function (data) {
				self.parseData(data);
			});
		}
		if (_.isObject(this.dataURL)) {
			setTimeout(function () {
				self.parseData(self.dataURL);
			}, 100);
		}
	},
	loadTimelineData: function loadTimelineData() {
		var self = this;
		//Test which way data is presented and load appropriate way
		if (this.timelineData.indexOf("csv") == -1 && babelHelpers.typeof(this.timelineData) != "object") {
			d3.json(self.timelineData, function (data) {
				self.timelineData = data;
				self.loadData();
				return;
			});
		}
		if (this.timelineData.indexOf("csv") > -1) {
			d3.csv(self.timelineData, function (data) {
				self.timelineData = data;
				console.log(self.timelineData);
				self.loadData();
				return;
			});
		}
		if (babelHelpers.typeof(this.timelineData) == "object") {
			self.loadData();
			return;
		}
	},
	formatDataStream: function formatDataStream(response) {
		var newArray = [];

		response.Dates.forEach(function (d, i) {
			var obj = {};

			var newDate = d.replace(/\//g, '').replace('Date(', '').replace(')', '').replace('+0000', '');
			var date = new Date(+newDate);
			var betterDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
			var formatDate = betterDate.getMonth() + 1 + "/" + betterDate.getDate() + "/" + betterDate.getFullYear();

			obj.date = formatDate;

			response.DataTypeValues[0].SymbolValues.forEach(function (item, index) {
				var name = response.DataTypeValues[0].SymbolValues[index].Symbol;
				obj[name] = response.DataTypeValues[0].SymbolValues[index].Value[i];
			});

			newArray.push(obj);
		});
		return newArray;
	},
	parseData: function parseData(data) {
		var self = this;

		if (self.dataStream) {
			var response = data.DataResponse || data.DataResponses[0];
			data = self.formatDataStream(response);
		}

		//figuring out if there is a timescale, is this necessary?
		if (data[0].date) {
			self.hasTimeScale = true;
		}
		// if parser undefined, figure out if it's a 4 year or 2 year date and set parser to match
		if (self.hasTimeScale && !self.parseDate) {
			if (data[0].date.split('/')[2].length == 2) {
				self.parseDate = d3.time.format("%m/%d/%y").parse;
			}
			if (data[0].date.split('/')[2].length == 4) {
				self.parseDate = d3.time.format("%m/%d/%Y").parse;
			}
		}
		//figure out current chart label, if it's switching between them.
		if (self.chartLayoutLables) {
			self.chartLayout = self.chartLayoutLables[self.chartLayoutLables.length - 1];
		}

		//set values if horizontal or not
		if (self.horizontal) {
			self.xOrY = "y";
			self.yOrX = "x";
			self.leftOrTop = "top";
			self.heightOrWidth = "width";
			self.widthOrHeight = "height";
			self.topOrLeft = "left";
		}

		//find values to map, if not defined
		if (!self.columnNames) {
			self.columnNames = _.keys(data[0]).filter(function (d) {
				return d != "date" && d != "category" && d !== "type" && d !== "rawDate" && d !== "displayDate";
			});
			self.columnNamesDisplay = self.columnNames;
		}
		if (_.isObject(self.columnNames) && !_.isArray(self.columnNames)) {
			self.columnNamesDisplay = _.values(self.columnNames);
			self.columnNames = _.keys(self.columnNames);
		}
		if (_.isArray(self.columnNames) && !self.columnNamesDisplay) {
			self.columnNamesDisplay = self.columnNames;
		}

		self.colorScale = d3.scale.ordinal();
		//figure out the color scale
		if (_.isObject(self.colors) && !_.isArray(self.colors)) {
			self.colorScale.domain(_.keys(self.colors));
			self.colorScale.range(_.values(self.colors));
		}
		if (_.isArray(self.colors)) {
			self.colorScale.domain(self.columnNames);
			self.colorScale.range(self.colors);
		}

		//handle multidata
		if (data[0].type) {
			if (!self.multiDataColumns) {
				self.multiDataColumns = _.uniq(_.pluck(data, 'type'));
			}
			var groupedData = _.groupBy(data, "type");
			self.multiDataColumns.forEach(function (d) {
				self.modelData(groupedData[d], d);
			});
		} else {
			if (self.multiDataColumns) {
				self.dataType = self.multiDataColumns[self.multiDataColumns.length - 1];
			}
			self.modelData(data, "dataholder");
		}

		//make labels if none
		if (!self.multiDataLabels && self.multiDataColumns) {
			self.multiDataLabels = self.multiDataColumns;
		}

		self.flattenData(self.chartData);
		self.baseRender();
		self.renderChart();
	},

	//function to make all the data.
	modelData: function modelData(data, name) {
		var self = this;

		if (!self.groupedData) {
			self.groupedData = {};
		}
		self.groupedData[name] = new Reuters.Graphics.DataPointCollection([], { parseDate: self.parseDate, dateFormat: self.dateFormat });
		self.groupedData[name].reset(data, { parse: true });

		self[name] = new Reuters.Graphics.DateSeriesCollection([], { parseDate: self.parseDate, groupSort: self.groupSort, divisor: self.divisor, categorySort: self.categorySort, dataType: self.dataType, multiDataColumns: self.multiDataColumns, dateFormat: self.dateFormat });

		self[name].reset(self.columnNames.map(function (d, i) {
			return { name: d, displayName: self.columnNamesDisplay[i], values: self.groupedData[name] };
		}), { parse: true });
		self.chartData = self[name];
	},

	flattenData: function flattenData(data) {
		var self = this;

		var filtered = data.filter(function (d) {
			return d.get("visible");
		});

		self.jsonData = _.invoke(filtered, 'toJSON');
		self.jsonData.forEach(function (d) {
			var name = d.name;
			d.values = d.values.toJSON();
			d.values.forEach(function (point) {
				_.extend(point, point[name]);
				point.name = name;
			});
		});

		if (self.timelineData) {
			self.showTip = true;
			var closestData = null;
			self.timelineData.forEach(function (timelineItem) {
				timelineItem.parsedDate = self.parseDate(timelineItem.date);

				self.chartData.first().get("values").each(function (d, i) {
					if (closestData === null || Math.abs(d.get("date") - timelineItem.parsedDate) < Math.abs(closestData - timelineItem.parsedDate)) {
						closestData = d.get("date");
					}
					timelineItem.closestDate = closestData;
					timelineItem.formatedDate = self.timelineDate(timelineItem.closestDate);
				});
			});
			self.timelineDataGrouped = _.groupBy(self.timelineData, "formatedDate");
		}
	},
	barCalculations: function barCalculations() {
		var self = this;
		// some aspects of the data useful for figuring out bar placement
		self.dataLength = 0;

		self.jsonData.forEach(function (d) {
			if (d.values.length > self.dataLength) {
				self.dataLength = d.values.length;
			}
		});
		self.numberOfObjects = function () {
			if (self.chartLayout == "onTopOf") {
				return 1;
			} else {
				return self.jsonData.length;
			}
		};

		self.widthOfBar = function () {
			if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent") {
				return self[self.widthOrHeight] / self.dataLength - self[self.widthOrHeight] / self.dataLength * 0.2;
			} else {
				return self[self.widthOrHeight] / (self.dataLength * self.numberOfObjects()) - self[self.widthOrHeight] / (self.dataLength * self.numberOfObjects()) * 0.2;
			}
		};
	},

	baseRender: function baseRender() {
		var self = this;
		self.trigger("baseRender:start");

		//make basic template and set names of stuff
		self.$el.html(self.chartTemplate({ self: self }));
		if (self.$el.width() < self.chartBreakPoint) {
			self.$el.find('.chart-holder').addClass("smaller");
		}

		//make a label based on the div's ID to use as unique identifiers
		self.targetDiv = self.$el.attr("id");
		self.chartDiv = self.targetDiv + " .chart";
		self.legendDiv = self.targetDiv + " .legend";
		self.$chartEl = $("#" + self.chartDiv);
		self.$legendEl = $("#" + self.legendDiv);

		//set the width and the height to be the width and height of the div the chart is rendered in
		self.width = self.$chartEl.width() - self.margin.left - self.margin.right;

		//if no height set, square, otherwise use the set height, if lower than 10, it is a ratio to width
		if (!self.options.height) {
			self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
		}
		if (self.options.height < 10) {
			if ($(window).width() < 400) {
				self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
			} else {
				self.height = self.$chartEl.width() * self.options.height - self.margin.top - self.margin.bottom;
			}
		}

		self.barCalculations();

		//create an SVG
		self.svg = d3.select("#" + self.chartDiv).append("svg").attr({
			width: self.width + self.margin.left + self.margin.right,
			height: self.height + self.margin.top + self.margin.bottom
		}).append("g").attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

		//make a rectangle so there is something to click on
		self.svg.append("svg:rect").attr({
			width: self.width,
			height: self.height,
			class: "plot"
		});

		//make a clip path for the graph
		self.clip = self.svg.append("svg:clipPath").attr("id", "clip" + self.targetDiv).append("svg:rect").attr({
			x: -self.margin.left,
			y: -4,
			width: self.width + self.margin.left + 8,
			height: self.height + 8
		});

		//go get the scales from the chart type view
		this.scales = {
			x: this.getXScale(),
			y: this.getYScale()
		};

		//render all the incidentals.
		self.recessionMaker();
		self.scaleMaker();
		self.toolTipMaker();
		self.legendMaker();
		self.multiDataMaker();
		self.chartLayoutMaker();
		self.baseUpdate(1);

		$(window).on("resize", _.debounce(function (event) {
			var width = self.$el.width();
			if (width < self.chartBreakPoint) {
				self.$el.find('.chart-holder').addClass("smaller");
			} else {
				self.$el.find('.chart-holder').removeClass("smaller");
			}
			self.update();
		}, 100));

		self.trigger("baseRender:end");
		//end of chart render
		return this;
	},

	setWidthAndMargins: function setWidthAndMargins() {
		var self = this;

		//length of largest tick
		var maxWidth = -1;
		$("#" + self.targetDiv + " .y.axis").find("text").not(".topTick").each(function () {
			maxWidth = maxWidth > $(this).width() ? maxWidth : $(this).width();
		});
		if (maxWidth === 0) {
			$("#" + self.targetDiv + " .y.axis").find("text").not(".topTick").each(function () {
				maxWidth = maxWidth > $(this)[0].getBoundingClientRect().width ? maxWidth : $(this)[0].getBoundingClientRect().width;
			});
		}

		if (!self.options.margin) {
			self.margin = { top: 15, right: 20, bottom: 30, left: 9 + maxWidth };
		}

		self.width = this.$chartEl.width() - self.margin.left - self.margin.right;
		if (!self.options.height) {
			self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
		}
		if (self.options.height < 10) {
			if ($(window).width() < 400) {
				self.height = self.$chartEl.width() - self.margin.top - self.margin.bottom;
			} else {
				self.height = self.$chartEl.width() * self.options.height - self.margin.top - self.margin.bottom;
			}
		}
	},

	recessionMaker: function recessionMaker() {
		var self = this;
		//put in the recessions, if there are any.
		if (!self.hasRecessions) {
			return;
		}

		var recessionData = [{ "recess": [{ "start": "5/1/1937", "end": "6/1/1938" }, { "start": "2/1/1945", "end": "10/1/1945" }, { "start": "11/1/1948", "end": "10/1/1949" }, { "start": "7/1/1953", "end": "5/1/1954" }, { "start": "8/1/1957", "end": "4/1/1958" }, { "start": "4/1/1960", "end": "2/1/1961" }, { "start": "12/1/1969", "end": "11/1/1970" }, { "start": "11/1/1973", "end": "3/1/1975" }, { "start": "1/1/1980", "end": "7/1/1980" }, { "start": "7/1/1981", "end": "11/1/1982" }, { "start": "7/1/1990", "end": "3/1/1991" }, { "start": "3/1/2001", "end": "11/1/2001" }, { "start": "12/1/2007", "end": "6/1/2009" }] }];
		self.recessions = self.svg.selectAll('.recession').data(recessionData);

		self.recessionsEnter = self.recessions.enter().append('g').attr({
			"clip-path": "url(#clip" + self.targetDiv + ")",
			class: "recession"
		});

		self.recessionsEnter.selectAll("rect").data(function (d) {
			return d.recess;
		}).enter().append("rect").attr({
			class: "recessionBox",
			x: function x(d) {
				return self.scales.x(self.recessionDateParse(d.start));
			},
			y: 0,
			width: function width(d) {
				return self.scales.x(self.recessionDateParse(d.end)) - self.scales.x(self.recessionDateParse(d.start));
			},
			height: self.height
		});
	},

	scaleMaker: function scaleMaker() {
		var self = this;

		//create and draw the x axis
		self.xAxis = d3.svg.axis().scale(self.scales[self.xOrY]).orient("bottom").ticks(self[self.xOrY + "ScaleTicks"]).tickPadding(8);

		//create and draw the y axis
		self.yAxis = d3.svg.axis().scale(self.scales[self.yOrX]).orient("left").ticks(self[self.yOrX + "ScaleTicks"]).tickPadding(8);

		//change the tic size if it's sideways
		if (self.horizontal) {
			self.xAxis.tickSize(0 - self.height).tickPadding(12);
		}
		//forces a tick for every value on the x scale
		if (self.tickAll) {
			self.fullDateDomain = [];
			self.smallDateDomain = [];
			self.chartData.first().get("values").each(function (d, i) {
				self.fullDateDomain.push(d.get("date"));
				if (i === 0 || i == self.dataLength - 1) {
					self.smallDateDomain.push(d.get("date"));
				}
			});
		}

		if (self.hasTimeScale || self.options.xTickFormat) {
			self.xAxis.tickFormat(self.xTickFormat);
		}

		//FIX the tier thing needs rethought
		if (self.chartLayout == "tier") {
			self[self.yOrX + "Axis"].ticks(2);
		}

		//define the tick format if it's specified, change tick length if it's horizontal
		if (self.yTickFormat) {
			self.yAxis.tickFormat(self.yTickFormat);
		}
		if (!self.horizontal) {
			self.yAxis.tickSize(0 - self.width);
		} else {
			self.yAxis.tickSize(0);
		}

		//if autoScale ing then let it use the default auto scale.  hasZoom and multiData automatically get auto-scaling
		if (self.yScaleVals && !self.hasZoom) {
			self[self.yOrX + "Axis"].tickValues(self.yScaleVals);
		}
		//draw all the axis
		self.svg.append("svg:g").attr("class", "x axis");
		self.svg.select(".x.axis").attr("transform", "translate(0," + self.height + ")").call(self.xAxis);
		self.svg.append("svg:g").attr("class", "y axis");
		self.svg.select(".y.axis").call(self.yAxis);

		self.adjustXTicks();

		if (!self.horizontal) {
			self.svg.selectAll(".y.axis line").attr("x1", -self.margin.left);
		}

		//get the latest labels
		self.dataLabels = self.YTickLabel[self.YTickLabel.length - 1];
		self.topTick(self.dataLabels);
	},

	makeZeroLine: function makeZeroLine() {
		var self = this;
		self.zeroLine = self.svg.append("line").attr("class", "zeroAxis").attr("clip-path", "url(#clip" + self.targetDiv + ")").attr(self.xOrY + "1", function () {
			if (self.horizontal) {
				return 0;
			}
			return -self.margin[self.leftOrTop];
		}).attr(self.xOrY + "2", self[self.widthOrHeight]).attr(self.yOrX + "1", self.scales.y(0)).attr(self.yOrX + "2", self.scales.y(0));
	},

	topTick: function topTick(tickLabels) {
		var self = this;

		d3.selectAll("#" + self.targetDiv + " .topTick").remove();

		var topTick = $("#" + self.targetDiv + " ." + self.yOrX + ".axis .tick:last-of-type").find("text");
		var topTickHeight = topTick.height();
		if (topTickHeight === 0) {
			topTickHeight = topTick[0].getBoundingClientRect().height;
		}

		d3.selectAll("#" + self.targetDiv + " ." + self.yOrX + ".axis .tick text").attr("transform", "translate(0,-" + topTickHeight / 2 + ")");

		var topTickHTML = topTick.text();
		var backLabel = "";
		if (self.horizontal) {
			backLabel = tickLabels[1];
		}
		topTick.text(tickLabels[0] + topTickHTML + backLabel);
		if (!self.horizontal) {
			topTick.clone().appendTo(topTick.parent()).text(tickLabels[1]).css('text-anchor', "start").attr("class", "topTick");
		}
	},

	toolTipMaker: function toolTipMaker() {
		var self = this;

		self.baseElement = document.getElementById(self.targetDiv).querySelector('svg');
		self.svgFind = self.baseElement;
		self.pt = self.svgFind.createSVGPoint();

		//start the cursor off to the left
		self.xPointCursor = 0 - self.margin[self.leftOrTop] - 500;
		//add a line
		self.cursorLine = self.svg.append('svg:line').attr('class', 'cursorline').attr("clip-path", "url(#clip" + self.targetDiv + ")").attr(self.xOrY + '1', self.xPointCursor).attr(self.xOrY + '2', self.xPointCursor).attr(self.yOrX + '1', 0).attr(self.yOrX + '2', self[self.heightOrWidth]);

		// tooltip divs, default turns em off.  They get turned on if you have no legend, or if you specifically turn them on.
		self.tooltip = d3.select("#" + self.chartDiv).append("div").attr("class", "reuters-tooltip").style({
			opacity: 0,
			display: function display() {
				if (self.showTip || !self.hasLegend) {
					return "block";
				}
				return "none";
			}
		});

		self.svgmove = self.svgFind.addEventListener('mousemove', function (evt) {
			return self.tooltipMover(evt);
		}, false);
		self.svgtouch = self.svgFind.addEventListener('touchmove', function (evt) {
			return self.tooltipMover(evt);
		}, false);
		self.svgout = self.svgFind.addEventListener('mouseout', function (evt) {
			return self.tooltipEnd(evt);
		}, false);
		self.svgtouchout = self.svgFind.addEventListener('touchend', function (evt) {
			return self.tooltipEnd(evt);
		}, false);
	},

	tooltipMover: function tooltipMover(evt, self) {
		var self = this;
		self.loc = self.cursorPoint(evt);
		self.xPointCursor = self.loc[self.xOrY];
		self.yPointCursor = self.loc[self.yOrX];

		//little maths to figure out in the sideBySide layout what data point you are on.
		var widthsOver = 0;
		if (self.chartLayout == "sideBySide") {
			var eachChartWidth = self[self.widthOrHeight] / self.numberOfObjects();
			for (i = 0; i < self.numberOfObjects; i++) {
				if (self.xPointCursor - self.margin[self.leftOrTop] > eachChartWidth) {
					self.xPointCursor = self.xPointCursor - eachChartWidth;
					widthsOver = widthsOver + eachChartWidth;
				}
			}
		}
		if (self.chartLayout == "tier") {
			widthsOver = self.widthOfBar() * self.numberOfObjects() / 2 - self.widthOfBar() / 2;
		}

		var toolTipModifier = 0;
		//use a reverse scale to figure out where you are at if there is a time scale.
		//if it's an ordinal scale it's a little trickier.
		self.closestData = null;
		var indexLocation = self.xPointCursor - parseFloat(self.margin[self.leftOrTop]);

		if (self.hasTimeScale) {
			self.locationDate = self.scales.x.invert(indexLocation);
			self.chartData.first().get("values").each(function (d, i) {
				if (self.closestData === null || Math.abs(d.get("date") - self.locationDate) < Math.abs(self.closestData - self.locationDate)) {
					self.closestData = d.get("date");
				}
			});
			if (self.timelineData) {
				self.closestData = null;
				self.timelineData.forEach(function (d, i) {
					if (self.closestData === null || Math.abs(d.closestDate - self.locationDate) < Math.abs(self.closestData - self.locationDate)) {
						self.closestData = d.closestDate;
					}
				});
			}
		} else {
			var closestRange = null;
			self.scales.x.range().forEach(function (d) {
				if (closestRange === null || Math.abs(d - indexLocation) < Math.abs(closestRange - indexLocation)) {
					closestRange = d;
				}
			});
			var closestIndex = self.scales.x.range().indexOf(closestRange);
			self.closestData = self.scales.x.domain()[closestIndex];
			toolTipModifier = self.widthOfBar() / 2;
		}

		//Got the value, now let's move the line.
		self.cursorLine.attr(self.xOrY + '1', self.scales.x(self.closestData) + toolTipModifier + widthsOver).attr(self.xOrY + '2', self.scales.x(self.closestData) + toolTipModifier + widthsOver);

		//highlight current bar
		self.highlightCurrent();

		//and now we can update the tooltip
		self.tooltip.html(function (d) {
			return self.tipTemplate({ self: self, data: self.makeTipData() });
		}).style(self.leftOrTop, function (d) {
			var tipWidth = $("#" + self.targetDiv + " .reuters-tooltip").outerWidth();

			if (self.horizontal) {
				tipWidth = $("#" + self.targetDiv + " .reuters-tooltip").outerHeight();
			}
			if (self.xPointCursor < (self.margin.left + self.width + self.margin.right) / 2) {
				return self.margin[self.leftOrTop] + self.scales.x(self.closestData) + toolTipModifier + 15 + "px";
			} else {
				return self.scales.x(self.closestData) + toolTipModifier - tipWidth + 15 + "px";
			}
		}).style(self.topOrLeft, function (d) {
			var toolTipHeight = $("#" + self.targetDiv + " .reuters-tooltip").height();
			if (self.horizontal) {
				toolTipHeight = $("#" + self.targetDiv + " .reuters-tooltip").outerWidth();
			}
			if (self.yPointCursor > (self.margin.top + self.height + self.margin.bottom) * 2 / 3) {
				return self.yPointCursor - toolTipHeight - 20 + "px";
			}
			if (self.yPointCursor < (self.margin.top + self.height + self.margin.bottom) / 3) {
				return self.yPointCursor + "px";
			}
			return self.yPointCursor - toolTipHeight / 2 + "px";
		});

		//and now we can update the legend.
		if (self.hasLegend) {
			var legendData = self.makeTipData();

			d3.select("#" + self.legendDiv).selectAll(".valueTip").data(legendData, function (d) {
				return d.name;
			}).html(function (d, i) {
				if (self.chartLayout == "stackPercent") {
					return self.tipNumbFormat(d.y1Percent - d.y0Percent);
				}
				return self.tipNumbFormat(d[self.dataType]);
			});

			self.legendDate.html(function () {
				if (legendData[0].category) {
					return legendData[0].category;
				}
				if (legendData[0].quarters) {
					return legendData[0].quarters;
				}
				return legendData[0].displayDate;
			});

			self.setLegendPositions();
		}

		self.tooltip.style({
			opacity: 1
		});
	},

	highlightCurrent: function highlightCurrent() {
		var self = this;
		if (self.chartType == "bar") {
			self.barChart.selectAll(".bar").classed("lighter", function (d) {
				if (d.date == self.closestData || d.category == self.closestData) {
					return false;
				}
				return true;
			});
		}
		if (self.chartType == "line") {
			self.lineChart.selectAll(".tipCircle").classed("highlight", function (d) {
				if (d.date == self.closestData || d.category == self.closestData) {
					return true;
				}
				return false;
			});
		}
	},

	makeTipData: function makeTipData() {
		var self = this;
		var xDataType = "category";
		if (self.hasTimeScale) {
			xDataType = "date";
		}
		var filtered = self.chartData.filter(function (d) {
			return d.get("visible");
		});
		var tipData = [];
		filtered.forEach(function (d) {
			var name = d.get("name");
			var displayName = d.get("displayName");
			var timeFormatter = d3.time.format("%m/%d/%Y");
			var matchingValues = d.get("values").filter(function (d) {
				if (self.hasTimeScale) {
					return timeFormatter(d.get(xDataType)) == timeFormatter(self.closestData);
				}
				return d.get(xDataType) == self.closestData;
			});
			matchingValues.forEach(function (d) {
				var matchObj = d.toJSON();
				_.extend(matchObj, matchObj[name]);
				matchObj.name = name;
				matchObj.displayName = displayName;
				tipData.push(matchObj);
			});
		});
		return tipData;
	},

	cursorPoint: function cursorPoint(evt) {
		var self = this;
		if (evt.clientX && evt.clientY) {
			self.pt.x = evt.clientX;self.pt.y = evt.clientY;
		} else if (evt.targetTouches) {
			self.pt.x = evt.targetTouches[0].clientX;self.pt.y = evt.targetTouches[0].clientY;
			self.pt.deltaX = Math.abs(self.pt.x - self.pt.lastX);
			self.pt.deltaY = Math.abs(self.pt.y - self.pt.lastY);
			if (self.pt.deltaX > self.pt.deltaY) {
				evt.preventDefault();
			}
			self.pt.lastY = self.pt.y;
			self.pt.lastX = self.pt.x;
		}
		return self.pt.matrixTransform(self.svgFind.getScreenCTM().inverse());
	},

	tooltipEnd: function tooltipEnd(evt) {
		var self = this;
		self.cursorLine.attr(self.xOrY + '1', 0 - self.margin[self.leftOrTop] - 10).attr(self.xOrY + '2', 0 - self.margin[self.leftOrTop] - 10);

		self.tooltip.style("opacity", 0);

		if (self.hasLegend) {
			self.legendItems.selectAll(".valueTip").html("<br>");

			self.legendDate.html("<br>");
			self.setLegendPositions();
		}
		if (self.chartType == "bar") {
			self.barChart.selectAll(".bar").classed("lighter", false);
		}
		if (self.chartType == "line") {
			self.lineChart.selectAll(".tipCircle").classed("highlight", false);
		}
	},

	multiDataMaker: function multiDataMaker() {
		var self = this;
		if (!self.multiDataColumns) {
			return;
		}
		self.$(".chart-nav .btn").on("click", function (evt) {

			var thisID = $(this).attr("dataid");
			var i = self.$(".chart-nav .btn").index(this);

			if (self.YTickLabel[i]) {
				self.dataLabels = self.YTickLabel[i];
			} else {
				self.dataLabels = self.YTickLabel[0];
			}

			//this has to act differently if you're doing a data transform, or picking up totally different data.
			if (!self[thisID]) {
				self.dataType = thisID;
			} else {
				self.chartData = self[thisID];
				self.flattenData(self.chartData);
			}

			self.update();
		});
		return;
	},

	chartLayoutMaker: function chartLayoutMaker() {
		var self = this;
		if (!self.chartLayoutLables) {
			return;
		}

		self.makeNavLayout = d3.select("#" + self.targetDiv + " .chart-layout").append("div").attr("class", "layoutNavContainer");

		self.makeNavLayoutButtons = self.makeNavLayout.selectAll('.layoutNavButtons').data(self.chartLayoutLables).enter().append("div").attr("class", "layoutNavButtons").attr("dataid", function (d) {
			return d;
		}).style("background-position", function (d) {
			//makes the sprite work.  i should change this to a class instead.
			var positionArray = ["basicbar", "stackTotalbar", "stackPercentbar", "sideBySidebar", "tierbar", "onTopOfbar", "basicline", "stackTotalline", "stackPercentline", "fillLinesline"];
			var positionNumber = positionArray.indexOf(d + self.chartType);
			return "0px " + -(positionNumber * 40) + "px";
		}).classed("selected", function (d, i) {
			if (i == self.chartLayoutLables.length - 1) {
				return true;
			} else {
				return false;
			}
		}).on("click", function (d, i) {
			if ($(this).hasClass("selected")) {
				return;
			}
			var thisID = $(this).attr("dataid");
			$(this).addClass("selected").siblings().removeClass("selected");

			self.chartLayout = d;

			//get the right data labels
			if (self.YTickLabel[i]) {
				self.dataLabels = self.YTickLabel[i];
			} else {
				self.dataLabels = self.YTickLabel[0];
			} //and override them if it's stack percent
			if (self.chartLayout == "stackPercent") {
				self.dataLabels = ["", "%"];
			}

			//run the updater
			self.update();
		});
	},

	legendMaker: function legendMaker() {
		var self = this;
		//if no legend, get out of here.
		if (!self.hasLegend) {
			return;
		}
		self.$legendEl.html(self.legendTemplate({ data: self.jsonData, self: self }));

		self.legendItems = d3.selectAll("#" + self.legendDiv + ' .legendItems').data(self.jsonData).on("click", function (d) {
			var that = this;
			self.chartData.where({ name: d.name }).forEach(function (d) {
				if ($(that).hasClass("clicked")) {
					d.set({ visible: true });
				} else {
					d.set({ visible: false });
				}
			});
			if (self.multiDataColumns && self[self.multiDataColumns[0]]) {
				self.multiDataColumns.forEach(function (data) {
					self[data].where({ name: d.name }).forEach(function (d) {
						if ($(that).hasClass("clicked")) {
							d.set({ visible: true });
						} else {
							d.set({ visible: false });
						}
					});
				});
			}

			$(this).toggleClass("clicked");
			self.flattenData(self.chartData);
			self.update();
		});

		self.legendValues = d3.select("#" + self.legendDiv).selectAll(".valueTip").data(self.jsonData);

		self.legendDate = d3.selectAll("#" + self.legendDiv + ' .dateTip');

		self.setLegendPositions();
	},

	setLegendPositions: function setLegendPositions() {
		var self = this;

		if (!self.hasLegend) {
			return;
		}
		var depth = 0;

		self.legendItems.data(self.jsonData, function (d) {
			return d.name;
		}).style("top", function (d, i) {
			var returnDepth = depth;
			depth += $(this).height() + 5;
			return returnDepth + "px";
		});
		self.legendItems.data(self.jsonData, function (d) {
			return d.name;
		}).exit().style("top", function (d, i) {
			var returnDepth = depth;
			depth += $(this).height() + 5;
			return returnDepth + "px";
		});
	},

	adjustXTicks: function adjustXTicks() {
		var self = this;

		var ticksWidth = 0;
		$("#" + self.targetDiv + " .x.axis .tick").find("text").each(function (d) {
			ticksWidth += $(this).width();
		});
		if (self.tickAll) {
			self[self.xOrY + "Axis"].tickValues(self.fullDateDomain);
		}

		if (ticksWidth > self.width) {
			self[self.xOrY + "Axis"].ticks(2);

			if (self.tickAll) {
				self[self.xOrY + "Axis"].tickValues(self.smallDateDomain);
			}

			self.svg.select(".x.axis").transition().attr("transform", "translate(0," + self.height + ")").call(self.xAxis);
		}
	},

	baseUpdate: function baseUpdate(duration) {
		var self = this;
		self.trigger("baseUpdate:start");

		if (!duration) {
			duration = 1000;
		}
		self.setWidthAndMargins();
		self.setLegendPositions();
		self.barCalculations();

		self.svg.selectAll('.cursorline').attr(self.yOrX + '1', 0).attr(self.yOrX + '2', self[self.heightOrWidth]);

		d3.select("#" + self.targetDiv + " svg").transition().duration(duration).attr({
			width: self.width + self.margin.left + self.margin.right,
			height: self.height + self.margin.top + self.margin.bottom
		});

		self.svg.transition().duration(duration).attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

		self.svg.select(".plot").transition().duration(duration).attr({
			width: self.width,
			height: self.height
		});

		self.clip.transition().duration(duration).attr({
			x: -self.margin.left,
			y: -4,
			width: self.width + self.margin.left + 8,
			height: self.height + 8
		});

		this.scales = {
			x: this.getXScale(),
			y: this.getYScale()
		};

		self.xAxis.scale(self.scales[self.xOrY]);

		self.yAxis.scale(self.scales[self.yOrX]);

		self[self.yOrX + "Axis"].tickSize(0 - self[self.widthOrHeight]);

		if (self.chartLayout == "tier") {
			self[self.yOrX + "Axis"].ticks(2);
		} else {
			self.yAxis.ticks(self[self.yOrX + "ScaleTicks"]);
			self.xAxis.ticks(self[self.xOrY + "ScaleTicks"]);
		}

		if (self.updateCount == 1) {
			self.adjustXTicks();
		}

		// update the axes,
		self.svg.select(".x.axis").transition().duration(duration).attr("transform", "translate(0," + self.height + ")").call(self.xAxis);

		self.svg.select(".y.axis").transition().duration(duration).call(self.yAxis).each("end", function (d) {
			if (self.updateCount === 0) {
				self.updateCount++;
				setTimeout(function () {
					self.update();
				}, 10);
			} else {
				self.updateCount = 0;
			}
		});

		if (!self.horizontal) {
			self.svg.selectAll(".y.axis line").attr("x1", "-" + self.margin.left);
		}

		//FIX - should be better x axis on the side by side
		if (self.chartLayout == "sideBySide") {
			self.svg.select("." + self.xOrY + ".axis").style("display", "none");
		} else {
			self.svg.select("." + self.xOrY + ".axis").style("display", "block");
		}

		//fix, tier is all screwy, should just rethink
		if (self.chartLayout == "tier") {

			self.barChart.selectAll("." + self.yOrX + ".axis").remove();

			self.barChart.append("svg:g").attr("class", self.yOrX + " axis").attr("transform", function (d) {
				if (self.horizontal) {
					return "translate(0," + self.height + ")";
				} else {
					return "translate(0,0)";
				}
			});

			self.barChart.data(self.chartData, function (d) {
				return d.name;
			}).selectAll("." + self.yOrX + ".axis").call(self[self.yOrX + "Axis"]);
			self.barChart.each(function (d) {
				var thisId = $(this).attr("id");
				var barAxis = $("#" + thisId + " .axis").detach();
				barAxis.prependTo($(this));
			});
		} else {}
		//FIX what's up here?
		/*
  		    if (self.chartType == "bar"){
  			    self.barChart.selectAll("." + self.yOrX + ".axis")
  				    .remove();
  			}
  */


		//update the top tick label
		self.topTick(self.dataLabels);

		//zero line
		if (self.zeroLine) {
			self.zeroLine.transition().duration(duration).attr(self.xOrY + "1", function () {
				if (self.horizontal) {
					return 0;
				}
				return "-" + self.margin[self.leftOrTop];
			}).attr(self.xOrY + "2", self[self.widthOrHeight]).attr(self.yOrX + "1", self.scales.y(0)).attr(self.yOrX + "2", self.scales.y(0));
		}

		//recessions
		self.svg.selectAll(".recessionBox").transition().duration(duration).attr({
			x: function x(d) {
				return self.scales.x(self.recessionDateParse(d.start));
			},
			width: function width(d) {
				return self.scales.x(self.recessionDateParse(d.end)) - self.scales.x(self.recessionDateParse(d.start));
			},
			height: self.height
		});

		if (self.zoom) {
			self.zoom.x(self.scales.x).y(self.scales.y);
		}
		self.trigger("baseUpdate:end");
		//end of base update
	},

	zoomChart: function zoomChart() {
		var self = this;
		//if there is a zoom, then setup the zoom
		//define the zoom
		self.zoom = d3.behavior.zoom().x(self.scales.x).y(self.scales.y).scaleExtent([1, 8]).on("zoom", zoomed);

		//call the zoom on the SVG
		self.svg.call(self.zoom);

		//define the zoom function
		function zoomed() {
			self.svg.select(".x.axis").call(self.xAxis);
			self.svg.select(".y.axis").call(self.yAxis);

			if (!self.horizontal) {
				self.svg.selectAll(".y.axis line").attr("x1", -self.margin.left);
			}

			//get the latest labels
			self.dataLabels = self.YTickLabel[self.YTickLabel.length - 1];
			self.topTick(self.dataLabels);

			if (self.chartType == "line") {
				self.lineChart.data(self.jsonData, function (d) {
					return d.name;
				}).selectAll(".tipCircle").data(function (d) {
					return d.values;
				}).attr("cx", function (d, i) {
					return self.scales.x(d.date);
				}).attr("cy", function (d, i) {
					return self.scales.y(d[self.dataType]);
				});

				self.lineChart.selectAll(".line").data(self.jsonData, function (d) {
					return d.name;
				}).attr("d", function (d) {
					return self.line(d.values);
				});

				self.lineChart.selectAll(".area").data(self.jsonData, function (d) {
					return d.name;
				}).attr("d", function (d, i) {
					return self.area(d.values);
				});
			}
			if (self.chartType == "bar") {

				self.svg.selectAll(".barChart").data(self.jsonData, function (d) {
					return d.name;
				}).selectAll(".bar").data(function (d) {
					return d.values;
				}).attr(self.yOrX, function (d) {
					return self.yBarPosition(d);
				}).attr(self.heightOrWidth, function (d) {
					return self.barHeight(d);
				}).attr(self.widthOrHeight, function (d, i, j) {
					return self.barWidth(d, i, j);
				}).attr(self.xOrY, function (d, i, j) {
					return self.xBarPosition(d, i, j);
				});
			}

			self.zeroLine.attr(self.yOrX + "1", self.scales.y(0)).attr(self.yOrX + "2", self.scales.y(0));

			self.svg.selectAll(".recessionBox").attr("x", function (d) {
				return self.scales.x(self.recessionDateParse(d.start));
			}).attr("width", function (d) {
				return self.scales.x(self.recessionDateParse(d.end)) - self.scales.x(self.recessionDateParse(d.start));
			});
		}
		//end of zoom
	}
	//end of view
});
//# sourceMappingURL=chartBase.js.map


// Reuters = Reuters || {};
// Reuters.Graphics = Reuters.Graphics || {};
//the view that constructs a barChart  data, get, models
Reuters.Graphics.BarChart = Reuters.Graphics.ChartBase.extend({
	defaults: _.defaults({
		someNewDefault: "yes"
	}, Reuters.Graphics.ChartBase.prototype.defaults),
	//setup the scales
	chartType: "bar",

	xScaleMin: function xScaleMin() {
		return d3.min(this.jsonData, function (c) {
			return d3.min(c.values, function (v) {
				return v.date;
			});
		});
	},

	xScaleMax: function xScaleMax() {
		return d3.max(this.jsonData, function (c) {
			return d3.max(c.values, function (v) {
				return v.date;
			});
		});
	},

	xScaleRange: function xScaleRange() {
		var objectNumber = this.numberOfObjects();
		if (this.chartLayout == "stackPercent" || this.chartLayout == "stackTotal") {
			objectNumber = 1;
		}
		var range = [this.widthOfBar() * objectNumber / 2, this[this.widthOrHeight] - this.widthOfBar() * objectNumber];
		if (this.chartLayout == "sideBySide") {
			range = [0, this[this.widthOrHeight] / (this.jsonData.length * (1 + 2 / this.jsonData[0].values.length))];
		}
		return range;
	},

	getXScale: function getXScale() {
		if (this.hasTimeScale) {
			return d3.time.scale().domain([this.xScaleMin(), this.xScaleMax()]).range(this.xScaleRange());
		} else {
			return d3.scale.ordinal().domain(this.jsonData[0].values.map(function (d) {
				return d.category;
			})).rangeRoundBands([0, this[this.widthOrHeight]], 0);
		}
	},

	yScaleMin: function yScaleMin() {
		var theValues = this.dataType;
		if (this.chartLayout == "stackTotal") {
			theValues = "stackMin";
		}
		var min = d3.min(this.jsonData, function (c) {
			return d3.min(c.values, function (v) {
				return v[theValues];
			});
		});
		if (this.chartLayout == "stackPercent") {
			min = 0;
		}
		if (min > 0) {
			min = 0;
		}
		return min;
	},

	yScaleMax: function yScaleMax() {
		var theValues = this.dataType;
		if (this.chartLayout == "stackTotal") {
			theValues = "stackTotal";
		}
		var max = d3.max(this.jsonData, function (c) {
			return d3.max(c.values, function (v) {
				return v[theValues];
			});
		});
		if (this.chartLayout == "stackPercent") {
			max = 100;
		}
		if (max < 0) {
			max = 0;
		}
		return max;
	},

	yScaleRange: function yScaleRange() {
		var fullHeight = this[this.heightOrWidth];
		if (this.chartLayout == "tier") {
			fullHeight = this[this.heightOrWidth] / (this.jsonData.length * (1 + 2 / this.jsonData[0].values.length));
		}
		var rangeLow = fullHeight;
		var rangeHigh = 0;
		if (this.horizontal) {
			rangeLow = 0;
			rangeHigh = fullHeight;
		}
		return [rangeLow, rangeHigh];
	},

	getYScale: function getYScale() {
		if (!this.yScaleVals || this.hasZoom) {
			return d3.scale.linear().domain([this.yScaleMin(), this.yScaleMax()]).nice(this.yScaleTicks).range(this.yScaleRange());
		} else {
			return d3.scale.linear().domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]]).range(this.yScaleRange());
		}
	},

	xBarPosition: function xBarPosition(d, i, j) {
		var self = this;
		var theScale = 'category';
		var modifier = 0;
		if (self.hasTimeScale) {
			theScale = 'date';
			modifier = self.widthOfBar() * self.numberOfObjects() / 2;
		}
		if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "sideBySide" || self.chartLayout == "tier") {
			if (self.hasTimeScale) {
				modifier = self.widthOfBar() / 2;
				if (self.chartLayout == "sideBySide") {
					modifier = 0;
				}
			}
			return self.scales.x(d[theScale]) - modifier;
		} else {
			if (self.chartLayout == "onTopOf") {
				return self.scales.x(d[theScale]) - modifier + self.widthOfBar() / (j + 1) * j / 2;
			} else {
				return self.scales.x(d[theScale]) - j * self.widthOfBar() + self.widthOfBar() * (self.numberOfObjects() - 1) - modifier;
			}
		}
	},

	yBarPosition: function yBarPosition(d) {
		var self = this;
		if (isNaN(d[self.dataType])) {
			return 0;
		}
		var positioner = "y1";
		if (self.horizontal || d.y1Total < 0) {
			positioner = "y0";
		}
		if (self.horizontal && d.y1Total < 0) {
			positioner = "y1";
		}
		if (self.chartLayout == "stackTotal") {
			return self.scales.y(d[positioner + "Total"]);
		} else {
			if (self.chartLayout == "stackPercent") {
				return self.scales.y(d[positioner + "Percent"]);
			} else {
				var minOrMax = "max";
				if (self.horizontal) {
					minOrMax = "min";
				}
				return self.scales.y(Math[minOrMax](0, d[self.dataType]));
			}
		}
	},

	barHeight: function barHeight(d) {
		var self = this;
		if (isNaN(d[self.dataType])) {
			return 0;
		}
		if (self.chartLayout == "stackTotal") {
			return Math.abs(self.scales.y(d.y0Total) - self.scales.y(d.y1Total));
		} else {
			if (self.chartLayout == "stackPercent") {
				return Math.abs(self.scales.y(d.y0Percent) - self.scales.y(d.y1Percent));
			} else {
				return Math.abs(self.scales.y(d[self.dataType]) - self.scales.y(0));
			}
		}
	},

	barFill: function barFill(d) {
		var self = this;
		if (self.colorUpDown) {
			if (d[self.dataType] > 0) {
				return self.colorScale.range()[0];
			} else {
				return self.colorScale.range()[1];
			}
		} else {
			return self.colorScale(d.name);
		}
	},

	barWidth: function barWidth(d, i, j) {
		var self = this;
		if (self.chartLayout == "tier") {
			return self.widthOfBar() * self.numberOfObjects();
		}
		if (self.chartLayout == "onTopOf") {
			return self.widthOfBar() / (j + 1);
		} else {
			return self.widthOfBar();
		}
	},

	renderChart: function renderChart() {
		var self = this;
		self.trigger("renderChart:start");

		if (self.hasZoom) {
			self.zoomChart();
		}
		//enter g tags for each set of data, then populate them with bars.  some attribute added on end, for updating reasons
		self.barChart = self.svg.selectAll(".barChart").data(self.jsonData, function (d) {
			return d.name;
		}).enter().append("g").attr("clip-path", "url(#clip" + self.targetDiv + ")").attr("class", "barChart").attr('id', function (d) {
			return self.targetDiv + d.name + "-bar";
		});

		if (self.chartLayout == "sideBySide") {
			self.barChart.attr("transform", function (d, i) {
				if (!self.horizontal) {
					return "translate(" + i * (self[self.widthOrHeight] / self.numberOfObjects()) + ",0)";
				} else {
					return "translate(0," + i * (self[self.widthOrHeight] / self.numberOfObjects()) + ")";
				}
			});
		}
		if (self.chartLayout == "tier") {
			self.barChart.attr("transform", function (d, i) {
				if (!self.horizontal) {
					return "translate(0," + i * (self[self.heightOrWidth] / self.numberOfObjects()) + ")";
				} else {
					return "translate(" + i * (self[self.heightOrWidth] / self.numberOfObjects()) + ",0)";
				}
			});
		}

		self.barChart.selectAll(".bar").data(function (d) {
			return d.values;
		}).enter().append("rect").attr("class", "bar").style("fill", function (d) {
			return self.barFill(d);
		}).attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0)).attr(self.widthOrHeight, function (d, i, j) {
			return self.barWidth(d, i, j);
		}).attr(self.xOrY, function (d, i, j) {
			return self.xBarPosition(d, i, j);
		}).transition().duration(1000).attr(self.yOrX, function (d) {
			return self.yBarPosition(d);
		}).attr(self.heightOrWidth, function (d) {
			return self.barHeight(d);
		});

		if (self.chartLayout == "sideBySide") {
			self.svg.select("." + self.xOrY + ".axis").style("display", "none");
		} else {
			self.svg.select("." + self.xOrY + ".axis").style("display", "block");
		}

		if (self.chartLayout == "tier") {
			self.barChart.append("svg:g").attr("class", self.yOrX + " axis").attr("transform", function (d) {
				if (!self.horizontal) {
					return "translate(0," + self.height + ")";
				} else {
					return "translate(0,0)";
				}
			});

			self.barChart.selectAll("." + self.yOrX + ".axis").call(self[self.yOrX + "Axis"]);
			self.barChart.each(function (d) {
				var thisId = $(this).attr("id");
				var barAxis = $("#" + thisId + " .axis").detach();
				barAxis.prependTo($(this));
			});
		} else {
			self.barChart.selectAll("." + self.yOrX + ".axis").remove();
		}

		//add teh zero line on top.
		self.makeZeroLine();
		self.trigger("renderChart:end");
		self.trigger("chart:loaded");

		//end of render
	},
	update: function update() {
		var self = this;
		self.baseUpdate();

		self.trigger("update:start");

		self.barChart.data(self.jsonData, function (d) {
			return d.name;
		}).order().transition().duration(1000).attr("transform", function (d, i) {
			var xPosit = 0;
			var yPosit = 0;
			if (self.chartLayout == "sideBySide") {
				if (!self.horizontal) {
					xPosit = i * (self[self.widthOrHeight] / self.numberOfObjects());
				} else {
					yPosit = i * (self[self.widthOrHeight] / self.numberOfObjects());
				}
			}
			if (self.chartLayout == "tier") {
				if (!self.horizonta) {
					yPosit = i * (self[self.heightOrWidth] / self.numberOfObjects());
				} else {
					xPosit = i * (self[self.heightOrWidth] / self.numberOfObjects());
				}
			}
			return "translate(" + xPosit + "," + yPosit + ")";
		});

		self.barChart.data(self.jsonData, function (d) {
			return d.name;
		}).exit().selectAll(".bar").transition().attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0));

		self.svg.selectAll(".barChart").data(self.jsonData, function (d) {
			return d.name;
		}).selectAll(".bar").data(function (d) {
			return d.values;
		}).transition().duration(1000).style("fill", function (d) {
			return self.barFill(d);
		}).attr(self.yOrX, function (d) {
			return self.yBarPosition(d);
		}).attr(self.heightOrWidth, function (d) {
			return self.barHeight(d);
		}).attr(self.widthOrHeight, function (d, i, j) {
			return self.barWidth(d, i, j);
		}).attr(self.xOrY, function (d, i, j) {
			return self.xBarPosition(d, i, j);
		});

		self.svg.selectAll(".barChart").data(self.jsonData, function (d) {
			return d.name;
		}).selectAll(".bar").data(function (d) {
			return d.values;
		}).exit().transition().attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0)).each("end", function (d) {
			d3.select(this).remove();
		});

		self.barChart.selectAll(".bar").data(function (d) {
			return d.values;
		}).enter().append("rect").attr("class", "bar").style("fill", function (d) {
			return self.barFill(d);
		}).attr(self.heightOrWidth, 0).attr(self.yOrX, self.scales.y(0)).attr(self.widthOrHeight, function (d, i, j) {
			return self.barWidth(d, i, j);
		}).attr(self.xOrY, function (d, i, j) {
			return self.xBarPosition(d, i, j);
		}).transition().duration(1000).attr(self.yOrX, function (d) {
			return self.yBarPosition(d);
		}).attr(self.heightOrWidth, function (d) {
			return self.barHeight(d);
		});

		self.trigger("update:end");
		//end of update
	}

	//end of mdoel
});
//# sourceMappingURL=barChart.js.map


Reuters.Graphics.ScatterPlot = Backbone.View.extend({
	data: undefined,
	dataURL: undefined,
	scatterSetupTemplate: Reuters.Graphics.scatterCharter.Template.scatterSetupTemplate,
	tooltipTemplate: Reuters.Graphics.scatterCharter.Template.scattertooltip,
	colorDomain: undefined,
	colors: [red3, blue1, lime1, orange1, green1, blue4],
	margin: { left: 50, right: 20, top: 20, bottom: 20 },
	xvalue: "xvalue",
	yvalue: "yvalue",
	rvalue: undefined,
	colorvalue: undefined,
	radiusModifier: 1.5,
	hardRadius: 5,
	xscaleorientation: "bottom",
	yscaleorientation: "left",
	yticks: 5,
	xticks: 5,
	xmin: undefined,
	ymin: undefined,
	xmax: undefined,
	ymax: undefined,
	xvalues: undefined,
	yvalues: undefined,
	height: undefined,
	dropdown: undefined,
	updateCount: 0,
	dateParseFormat: "%m/%d/%y",
	dateFormat: d3.time.format("%b %Y"),

	initialize: function initialize(opts) {
		var self = this;
		this.options = opts;

		// if we are passing in options, use them instead of the defualts.
		_.each(opts, function (item, key) {
			self[key] = item;
		});
		self.bottomMargin = self.margin.bottom;
		//fix, this is dumb
		if (!self.options.radiusModifier) {
			self.options.radiusModifier = self.radiusModifier;
		}

		//Test which way data is presented and load appropriate way
		if (this.dataURL.indexOf("csv") == -1 && !_.isObject(this.dataURL)) {
			d3.json(self.dataURL, function (data) {
				self.parseData(data);
			});
		}
		if (this.dataURL.indexOf("csv") > -1) {
			d3.csv(self.dataURL, function (data) {
				self.parseData(data);
			});
		}
		if (_.isObject(this.dataURL)) {
			setTimeout(function () {
				self.parseData(self.dataURL);
			}, 100);
		}

		//end of initialize
	},
	parseData: function parseData(data) {
		var self = this;
		if ((self.xvalue == "date" || self.yvalue == "date") && !self.parseDate) {
			if (data[0].date.split('/')[2].length == 2) {
				self.dateParseFormat = "%m/%d/%y";
			}
			if (data[0].date.split('/')[2].length == 4) {
				self.dateParseFormat = "%m/%d/%Y";
			}
		}

		self.parseDate = d3.time.format(self.dateParseFormat).parse;

		self.targetDiv = $(self.el).attr("id");
		self.chartDiv = $(self.el).attr("id") + "-chart";

		self.oneDecimal = d3.format(",.1f");
		self.twoDecimal = d3.format(",.2f");
		self.noDecimal = d3.format(",.0f");

		if (!self.colorDomain) {
			self.colorDomain = _.uniq(_.pluck(data, self.colorvalue));
		}

		//figure out the color scale
		if (_.isObject(self.colors) && !_.isArray(self.colors)) {
			self.colorDomain = _.keys(self.colors);
			self.colors = _.values(self.colors);
		}
		self.colorScale = d3.scale.ordinal().domain(self.colorDomain).range(self.colors);

		//handle multidata
		if (data[0].type) {
			if (!self.multiDataColumns) {
				self.multiDataColumns = _.uniq(_.pluck(data, 'type'));
			}
			if (!self.multiDataLabels) {
				self.multiDataLabels = self.multiDataColumns;
			}
			self.dataType = self.multiDataColumns[self.multiDataColumns.length - 1];
			if (!self.idField) {
				self.idField = "uniqueid";
				var groupData = _.groupBy(data, "type");
				_.each(groupData, function (value, key) {
					value.forEach(function (d, i) {
						d.uniqueid = i;
					});
				});
			}
		} else {
			if (!self.idField) {
				self.idField = "uniqueid";
				data.forEach(function (d, i) {
					d.uniqueid = i;
				});
			}
		}

		self.data = new Reuters.Graphics.ScatterCollection([], { parseDate: self.parseDate, xvalue: self.xvalue, yvalue: self.yvalue, rvalue: self.rvalue, dateFormat: self.dateFormat });
		self.data.reset(data, { parse: true });

		self.chartData = self.flattenData(self.data);

		self.baseRender();
	},

	flattenData: function flattenData(data) {
		var self = this;
		if (self.dataType) {
			var filtered = data.filter(function (d) {
				return d.get("type") == self.dataType;
			});
			var flattened = _.invoke(filtered, 'toJSON');
			return flattened;
		}
		return data.toJSON();
	},

	setWidthAndMargins: function setWidthAndMargins() {
		var self = this;

		//length of largest tick
		var maxWidth = -1;
		self.$(".y.axis").find("text").each(function () {
			maxWidth = maxWidth > $(this).width() ? maxWidth : $(this).width();
		});
		if (maxWidth === 0) {
			self.$(".y.axis").find("text").each(function () {
				maxWidth = maxWidth > $(this)[0].getBoundingClientRect().width ? maxWidth : $(this)[0].getBoundingClientRect().width;
			});
		}

		if (!self.options.margin) {
			self.margin[self.yscaleorientation] = 9 + maxWidth;
			if (self.yLabelText) {
				self.margin.left = self.margin.left + 40;
			}
			if (self.xLabelText) {
				self.margin.bottom = self.bottomMargin + 30;
			}
		}

		self.width = self.$("#" + self.chartDiv).width() - self.margin.left - self.margin.right;

		if (!self.options.height) {
			self.height = self.width;
		}
		if (self.options.height < 10) {
			self.height = self.width * self.options.height;
		}

		if (self.width < 400) {
			self.radiusModifier = self.options.radiusModifier * 2 / 3;
		} else {
			self.radiusModifier = self.options.radiusModifier;
		}
	},

	multiDataMaker: function multiDataMaker() {
		var self = this;

		if (!self.multiDataSlider) {
			self.$(".chart-nav .btn").on("click", function (evt) {
				var thisID = $(this).attr("dataid");
				self.dataType = thisID;
				self.chartData = self.flattenData(self.data);
				self.update();
			});
		} else {
			var onSlide = function onSlide() {
				var thisID = self.multiDataColumns[self.slider.noUiSlider.get()];
				self.dataType = thisID;
				self.chartData = self.flattenData(self.data);
				self.update();
			};

			self.slider = self.$('[data-slider]')[0];

			noUiSlider.create(self.slider, {
				start: [self.multiDataLabels.length - 1],
				range: {
					min: [0],
					max: [self.multiDataLabels.length - 1]
				},
				snap: false,
				step: 1,
				format: {
					to: function to(value) {
						return value;
					},
					from: function from(value) {
						return value;
					}
				},
				pips: {
					mode: "count",
					values: self.multiDataLabels.length,
					density: 3
				}
			});

			self.$('div.noUi-value-large').each(function (i) {
				$(this).html(self.multiDataLabels[i]);
			});
			//This probably doesn't belong here, but will fix the most common use-case.
			$(self.slider).find('div.noUi-marker-large:last').addClass('last');
			$(self.slider).find('div.noUi-marker-large:first').addClass('first');

			self.slider.noUiSlider.on('set', onSlide);
			//			stepSlider.noUiSlider.on('slide', onSlide);

			self.$(".btn.btn-primary.animation-play").on("click", function () {
				self.playInterval = setInterval(function () {
					self.play();
				}, 1000);
			});

			self.$(".animation-pause").on("click", function () {
				clearInterval(self.playInterval);
			});

			self.play = function () {
				var currentIndex = self.slider.noUiSlider.get();
				if (currentIndex == self.multiDataLabels.length - 1) {
					currentIndex = 0;
				} else {
					currentIndex++;
				}
				self.slider.noUiSlider.set(currentIndex);
			};
		}

		if (self.dropdown) {
			self.selectArray = _.uniq(_.pluck(self.chartData, self.dropdown)).sort();
			d3.select("#" + self.targetDiv + " .custom-select").selectAll("options").data(self.selectArray).enter().append("option").attr("value", function (d) {
				return d;
			}).html(function (d) {
				return d;
			});

			self.$(".custom-select").on("change", function (evt) {
				var id = $(this).val();
				self.scatterPlot.classed("turned-off", function (d) {
					if (id == "Show All ...") {
						return false;
					}
					if (id == d[self.dropdown]) {
						return false;
					}
					return true;
				});
			});
		}
		if (self.colorDomain && self.colorDomain.length > 1) {
			self.$(".scatter-legend-item").on("click", function (d) {
				var id = $(this).attr("data-id");
				$(this).toggleClass("turned-off");
				self.scatterPlot.each(function (d) {
					if (id == d[self.colorvalue]) {
						$(this).toggleClass("turned-off");
					}
				});
			});
		}
	},

	baseRender: function baseRender() {
		var self = this;
		self.trigger("renderChart:start");

		$(self.el).html(function () {
			return self.scatterSetupTemplate({ data: self.chartData, self: self });
		});

		self.width = self.$("#" + self.chartDiv).width() - self.margin.left - self.margin.right;

		if (!self.options.height) {
			self.height = self.width;
		}
		if (self.options.height < 10) {
			self.height = self.width * self.options.height;
		}

		self.setScales();

		if (self.dataType) {
			self.multiDataMaker();
		}
		self.svg = d3.select("#" + self.chartDiv).append("svg").attr("width", self.width + self.margin.left + self.margin.right).attr("height", self.height + self.margin.top + self.margin.bottom).append("g").attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

		self.xAxis = d3.svg.axis().scale(self.x).orient(self.xscaleorientation).tickSize(self.height).tickPadding(8);

		self.yAxis = d3.svg.axis().scale(self.y).orient(self.yscaleorientation).tickSize(0 - self.width).tickPadding(8);

		if (self.xvalues) {
			self.xAxis.tickValues(self.xvalues);
		}

		if (self.yvalues) {
			self.yAxis.tickValues(self.yvalues);
		}

		self.resizeAxis();

		self.svg.append("svg:g").attr("class", "scatterLegend");

		self.svg.append("svg:g").attr("class", "x axis");

		self.svg.select(".x.axis")
		//.attr("transform", "translate(0," + self.height + ")")
		.call(self.xAxis);

		self.svg.append("svg:g").attr("class", "y axis");

		if (self.yscaleorientation == "left") {
			self.svg.select(".y.axis").call(self.yAxis);
		} else {
			self.svg.select(".y.axis").attr("transform", "translate(" + self.width + ",0)").call(self.yAxis);
		}

		self.scatterPlot = self.svg.selectAll("circle").data(self.chartData).enter().append("circle").attr("r", function (d) {
			if (self.rvalue) {
				return Math.sqrt(d[self.rvalue]) / Math.PI * self.radiusModifier;
			} else {
				return self.hardRadius;
			}
		}).attr("cy", function (d) {
			return self.y(d[self.yvalue]);
		}).attr("cx", function (d) {
			return self.x(d[self.xvalue]);
		}).attr("class", "scatter-dot").style("fill", function (d) {
			return self.setFill(d);
		}).style("stroke", function (d) {
			return self.setStroke(d);
		}).attr("title", function (d) {
			return self.tooltipTemplate({ data: d, self: self });
		});

		// $('.scatter-dot').tipsy({opacity:1, gravity:'sw', html:true});

		self.$('.scatter-dot').tooltip({
			html: true,
			placement: function placement(tooltip, element) {
				var cx = $(element).attr("cx");
				var svgCenter = self.width / 2;
				if (cx < svgCenter) {
					return 'right';
				}
				return 'left';
			}
		});

		if (self.xLabelText) {
			self.xLabel = self.svg.append("text").attr("x", self.width / 2).attr("y", self.height + 40).text(self.xLabelText).attr("class", "axislabel");
		}

		if (self.yLabelText) {
			self.yLabel = self.svg.append("text").attr("x", self.height / 2).attr("y", self.margin.left - 20).attr("transform", "rotate (90)").text(self.yLabelText).attr("class", "axislabel");
		}

		$(window).on("resize", _.debounce(function (event) {
			self.newWidth = $("#" + self.chartDiv).width() - self.margin.left - self.margin.right;
			if (self.newWidth == self.width || self.newWidth <= 0) {
				return;
			}
			self.width = self.newWidth;

			self.update();
		}, 100));

		self.trigger("renderChart:end");
		self.update();
	},

	setFill: function setFill(d) {
		var self = this;
		if (self.colorvalue) {
			return self.colorScale(d[self.colorvalue]);
		}
		return;
	},

	setStroke: function setStroke(d) {
		var self = this;
		if (self.colorvalue) {
			return self.colorScale(d[self.colorvalue]);
		}
		return;
	},

	setScales: function setScales() {
		var self = this;

		if (self.xvalues) {
			self.xmin = self.xvalues[0];
			self.xmax = self.xvalues[self.xvalues.length - 1];
		}

		if (self.yvalues) {
			self.ymin = self.yvalues[0];
			self.ymax = self.yvalues[self.yvalues.length - 1];
		}

		self.getxmin = function () {
			if (self.xmin) {
				return self.xmin;
			}
			return d3.min(self.chartData, function (d) {
				return d[self.xvalue];
			});
		};

		self.getxmax = function () {
			if (self.xmax) {
				return self.xmax;
			}
			return d3.max(self.chartData, function (d) {
				return d[self.xvalue];
			});
		};

		self.getymin = function () {
			if (self.ymin) {
				return self.ymin;
			}
			return d3.min(self.chartData, function (d) {
				return d[self.yvalue];
			});
		};

		self.getymax = function () {
			if (self.ymax) {
				return self.ymax;
			}
			return d3.max(self.chartData, function (d) {
				return d[self.yvalue];
			});
		};

		self.x = d3.scale.linear().domain([self.getxmin(), self.getxmax()]).range([0, self.width]).nice(self.xticks);

		if (self.xvalue == "date") {
			self.x = d3.time.scale().domain([self.getxmin(), self.getxmax()]).range([0, self.width]);
		}

		if (self.xvalue == "category") {
			self.x = d3.scale.ordinal().domain(self.chartData.map(function (d) {
				return d.category;
			})).rangeRoundBands([0, self.width], 1);
		}

		self.y = d3.scale.linear().domain([self.getymin(), self.getymax()]).range([self.height, 0]).nice(self.yticks);

		if (self.yvalue == "date") {
			self.y = d3.time.scale().domain([self.getymin(), self.getymax()]).range([self.height, 0]);
		}

		if (self.yvalue == "category") {
			self.y = d3.scale.ordinal().domain(self.chartData.map(function (d) {
				return d.category;
			})).rangeRoundBands([self.height, 0], 1);
		}
	},
	resizeAxis: function resizeAxis() {
		var self = this;

		self.xAxis.tickSize(self.height);

		if (self.xscaleorientation == "top") {
			self.xAxis.tickSize(0 - self.height);
		}

		self.yAxis.tickSize(0 - self.width);

		if (self.xscaleorientation == "right") {
			self.xAxis.tickSize(self.width);
		}

		self.xAxis.ticks(self.xticks);
		if (self.xvalue != "category") {
			self.x.nice(self.xticks);
		}

		self.yAxis.ticks(self.yticks);
		if (self.yvalue != "category") {
			self.y.nice(self.yticks);
		}
	},

	adjustXTicks: function adjustXTicks() {
		var self = this;

		var ticksWidth = 0;
		self.$(".x.axis .tick").find("text").each(function (d, i) {
			ticksWidth += $(this).width();
		});

		if (ticksWidth > self.width) {
			self.xAxis.ticks(4);
			if (self.xvalue != "category") {
				self.x.nice(self.xticks);
			}
			self.yAxis.ticks(4);
			if (self.yvalue != "category") {
				self.y.nice(self.yticks);
			}
		}

		self.svg.select(".x.axis").transition().duration(500).call(self.xAxis);
	},

	update: function update() {
		var self = this;

		self.trigger("update:start");

		self.setWidthAndMargins();

		self.x.range([0, self.width]).domain([self.getxmin(), self.getxmax()]);

		self.y.domain([self.getymin(), self.getymax()]).range([self.height, 0]);

		if (self.xvalue == "category") {
			self.x.domain(self.chartData.map(function (d) {
				return d.category;
			})).rangeRoundBands([0, self.width], 1);
		}

		if (self.yvalue == "category") {
			self.y.domain(self.chartData.map(function (d) {
				return d.category;
			})).rangeRoundBands([self.height, 0], 1);
		}

		d3.select("#" + self.chartDiv).select("svg").transition().duration(500).attr("width", self.width + self.margin.left + self.margin.right).attr("height", self.height + self.margin.top + self.margin.bottom);

		self.svg.attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

		self.resizeAxis();

		if (self.updateCount === 0) {
			self.updateCount++;
			setTimeout(function () {
				self.update();
			}, 520);
		} else {
			self.updateCount = 0;
		}

		self.svg.select(".x.axis").transition().duration(500).call(self.xAxis);

		if (self.yscaleorientation == "left") {
			self.svg.select(".y.axis").transition().duration(500).call(self.yAxis);
		} else {
			self.svg.select(".y.axis").transition().duration(500).attr("transform", "translate(" + self.width + ",0)").call(self.yAxis);
		}

		self.adjustXTicks();

		self.scatterPlot.data(self.chartData, function (d) {
			return d[self.idField];
		}).attr("title", function (d) {
			return self.tooltipTemplate({ data: d, self: self });
		}).transition().duration(500).attr("r", function (d) {
			if (self.rvalue) {
				return Math.sqrt(d[self.rvalue]) / Math.PI * self.radiusModifier;
			} else {
				return self.hardRadius;
			}
		}).attr("cy", function (d) {
			return self.y(d[self.yvalue]);
		}).attr("cx", function (d) {
			return self.x(d[self.xvalue]);
		}).style("fill", function (d) {
			return self.setFill(d);
		}).style("stroke", function (d) {
			return self.setStroke(d);
		});

		self.scatterPlot.data(self.chartData, function (d) {
			return d[self.idField];
		}).exit().transition().duration(500).attr("r", 0);

		if (self.xLabelText) {
			self.xLabel.transition().duration(500).attr("x", self.width / 2).attr("y", self.height + 40);
		}

		if (self.yLabelText) {
			self.yLabel.transition().duration(500).attr("x", self.height / 2).attr("y", self.margin.left - 20);
		}

		self.trigger("update:end");
	}

});

Reuters.Graphics.ScatterModel = Backbone.Model.extend({
	initialize: function initialize(attributes, options) {
		return;
	},

	parse: function parse(d, options) {
		var self = options.collection;
		if (d.date) {
			d.parsedDate = self.parseDate(d.date);
			d.displayDate = self.dateFormat(d.parsedDate);
		}

		if (self.xvalue == "date") {
			d[self.xvalue] = self.parseDate(d[self.xvalue]);
		} else if (self.xvalue != "category") {
			d[self.xvalue] = parseFloat(d[self.xvalue]);
		}

		if (self.yvalue == "date") {
			d[self.yvalue] = self.parseDate(d[self.yvalue]);
		} else if (self.yvalue != "category") {
			d[self.yvalue] = parseFloat(d[self.yvalue]);
		}

		if (self.rvalue) {
			d[self.rvalue] = parseFloat(d[self.rvalue]);
		}

		return d;
	}
});

//the collection of datapoint which will sort by date.
Reuters.Graphics.ScatterCollection = Backbone.Collection.extend({
	initialize: function initialize(models, options) {
		var self = this;
		_.each(options, function (item, key) {
			self[key] = item;
		});
	},

	comparator: function comparator(item) {
		var self = this;
	},

	model: Reuters.Graphics.ScatterModel,

	parse: function parse(data) {
		var self = this;
		return data;
	}
});
//# sourceMappingURL=scatter.js.map

Reuters.Graphics.LineChart = Reuters.Graphics.ChartBase.extend({
    defaults: _.defaults({
        someNewDefault: "yes"
    }, Reuters.Graphics.ChartBase.prototype.defaults),
    //setup the scales.  You have to do this in the specific view, it will be called in the Reuters.Graphics.ChartBase.
    chartType: "line",
    xScaleMin: function xScaleMin() {
        return d3.min(this.jsonData, function(c) {
            return d3.min(c.values, function(v) {
                return v.date;
            });
        });
    },
    xScaleMax: function xScaleMax() {
        return d3.max(this.jsonData, function(c) {
            return d3.max(c.values, function(v) {
                return v.date;
            });
        });
    },
    getXScale: function getXScale() {
        return d3.time.scale().domain([this.xScaleMin(), this.xScaleMax()]).range([0, this.width]);
    },
    yScaleMin: function yScaleMin() {
        var theValues = this.dataType;
        if (this.chartLayout == "stackTotal") {
            theValues = "stackTotal";
        }
        var min = d3.min(this.jsonData, function(c) {
            return d3.min(c.values, function(v) {
                return v[theValues];
            });
        });
        if (this.chartlayout == "fillLines") {
            if (min > 0) {
                min = 0;
            }
        }
        if (this.chartLayout == "stackTotal" || this.chartLayout == "stackPercent") {
            min = 0;
        }
        return min;
    },
    yScaleMax: function yScaleMax() {
        var theValues = this.dataType;
        if (this.chartLayout == "stackTotal") {
            theValues = "stackTotal";
        }
        var max = d3.max(this.jsonData, function(c) {
            return d3.max(c.values, function(v) {
                return v[theValues];
            });
        });
        if (this.chartLayout == "stackPercent") {
            max = 100;
        }
        return max;
    },
    getYScale: function getYScale() {
        var self = this;
        if (!self.yScaleVals || this.hasZoom) {
            return d3.scale.linear().domain([this.yScaleMin(), this.yScaleMax()]).nice(this.yScaleTicks).range([this.height, 0]);
        } else {
            return d3.scale.linear().domain([this.yScaleVals[0], this.yScaleVals[this.yScaleVals.length - 1]]).nice(this.yScaleTicks).range([this.height, 0]);
        }
    },
    renderChart: function renderChart() {
        // create a variable called "self" to hold a reference to "this"
        var self = this;
        self.trigger("renderChart:start");

        if (self.hasZoom) {
            self.zoomChart();
        }

        //will draw the line
        self.line = d3.svg.line().x(function(d) {
            return self.scales.x(d.date);
        }).y(function(d) {
            if (self.chartLayout == "stackTotal") {
                return self.scales.y(d.y1Total);
            } else {
                if (self.chartLayout == "stackPercent") {
                    return self.scales.y(d.y1Percent);
                } else {
                    return self.scales.y(d[self.dataType]);
                }
            }
        }).interpolate(self.lineType).defined(function(d) {
            return !isNaN(d[self.dataType]);
        });

        self.area = d3.svg.area().x(function(d) {
            return self.scales.x(d.date);
        }).y0(function(d) {
            if (self.chartLayout == "stackTotal") {
                return self.scales.y(d.y0Total);
            } else {
                if (self.chartLayout == "stackPercent") {
                    return self.scales.y(d.y0Percent);
                } else {
                    return self.scales.y(0);
                }
            }
        }).y1(function(d) {
            if (self.chartLayout == "stackTotal") {
                return self.scales.y(d.y1Total);
            } else {
                if (self.chartLayout == "stackPercent") {
                    return self.scales.y(d.y1Percent);
                } else {
                    return self.scales.y(d[self.dataType]);
                }
            }
        }).interpolate(self.lineType).defined(function(d) {
            return !isNaN(d[self.dataType]);
        });

        //bind the data and put in some G elements with their specific mouseover behaviors.
        self.lineChart = self.svg.selectAll(".lineChart").data(self.jsonData, function(d) {
            return d.name;
        }).enter().append("g").attr({
            "clip-path": "url(#clip" + self.targetDiv + ")",
            class: "lineChart",
            id: function id(d) {
                return self.targetDiv + d.displayName.replace(/\s/g, '') + "-line";
            }
        }).on("mouseover", function(d) {
            //put the line we've hovered on on top=
            self.lineChart.sort(function(a, b) {
                if (a.name == d.name) {
                    return 1;
                } else {
                    return -1;
                }
            }).order();

            //class all other lines to be lighter
            d3.selectAll("#" + self.targetDiv + " .lineChart").classed('notSelected', true);
            d3.select(this).classed("notSelected", false);
        }).on("mouseout", function(d) {
            d3.selectAll(".lineChart").classed('notSelected', false);
        });

        self.lineChart.append("path").attr("class", "line").style("stroke", function(d) {
            return self.colorScale(d.name);
        }).attr("d", function(d) {
            return self.line(d.values[0]);
        }).transition().duration(1500).delay(function(d, i) {
            return i * 100;
        }).attrTween('d', function(d) {
            var interpolate = d3.scale.quantile().domain([0, 1]).range(d3.range(1, d.values.length + 1));
            return function(t) {
                return self.line(d.values.slice(0, interpolate(t)));
            };
        });

        self.lineChart.append("path").attr("class", "area").style("fill", function(d) {
            return self.colorScale(d.name);
        }).attr("d", function(d) {
            return self.area(d.values[0]);
        }).style("display", function(d) {
            if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "fillLines") {
                return "block";
            } else {
                return "none";
            }
        }).transition().duration(1500).delay(function(d, i) {
            return i * 100;
        }).attrTween('d', function(d) {
            var interpolate = d3.scale.quantile().domain([0, 1]).range(d3.range(1, d.values.length + 1));
            return function(t) {
                return self.area(d.values.slice(0, interpolate(t)));
            };
        });

        self.lineChart.selectAll(".tipCircle").data(function(d) {
                return d.values;
            }).enter().append("circle").attr("class", "tipCircle").attr("cx", function(d, i) {
                return self.scales.x(d.date);
            }).attr("cy", function(d, i) {
                if (self.chartLayout == "stackTotal") {
                    return self.scales.y(d.y1Total);
                } else {
                    if (self.chartLayout == "stackPercent") {
                        return self.scales.y(d.y1Percent);
                    } else {
                        return self.scales.y(d[self.dataType]);
                    }
                }
            }).attr("r", function(d, i) {
                if (isNaN(d[self.dataType])) {
                    return 0;
                }
                return 5;
            }).style('opacity', function(d) {
                if (self.markDataPoints) {
                    return 1;
                }
                return 0;
            }).style("fill", function(d) {
                return self.colorScale(d.name);
            }) //1e-6
            .classed("timeline", function(d) {
                if (self.timelineDataGrouped) {
                    if (self.timelineDataGrouped[self.timelineDate(d.date)]) {
                        return true;
                    }
                }
                return false;
            });

        //add teh zero line on top.
        self.makeZeroLine();

        self.trigger("renderChart:end");
        self.trigger("chart:loaded");
        self.trigger("chart:loaded");

        //end chart render
    },
    update: function update() {
            var self = this;

            self.baseUpdate();
            self.trigger("update:start");

            self.exitLine = d3.svg.line().x(function(d) {
                return self.scales.x(d.date);
            }).y(function(d) {
                return self.margin.bottom + self.height + self.margin.top + 10;
            }).interpolate(self.lineType);

            self.exitArea = d3.svg.area().x(function(d) {
                return self.scales.x(d.date);
            }).y0(function(d) {
                return self.margin.bottom + self.height + self.margin.top + 10;
            }).y1(function(d) {
                return self.margin.bottom + self.height + self.margin.top + 10;
            }).interpolate(self.lineType);

            //exiting lines
            self.lineChart.data(self.jsonData, function(d) {
                return d.name;
            }).exit().selectAll(".line").transition().attr("d", function(d, i) {
                return self.exitLine(d.values);
            });

            //exiting area
            self.lineChart.data(self.jsonData, function(d) {
                return d.name;
            }).exit().selectAll(".area").transition().attr("d", function(d, i) {
                return self.exitArea(d.values);
            });

            self.lineChart.data(self.jsonData, function(d) {
                return d.name;
            }).exit().selectAll(".tipCircle").transition().attr("r", 0);

            //update the line
            self.lineChart.selectAll(".line").data(self.jsonData, function(d) {
                return d.name;
            }).transition().duration(1000).attr("d", function(d, i) {
                return self.line(d.values);
            });

            //update the area
            self.lineChart.selectAll(".area").data(self.jsonData, function(d) {
                return d.name;
            }).style("display", function(d) {
                if (self.chartLayout == "stackTotal" || self.chartLayout == "stackPercent" || self.chartLayout == "fillLines") {
                    return "block";
                } else {
                    return "none";
                }
            }).transition().duration(1000).attr("d", function(d, i) {
                return self.area(d.values);
            });

            //the circles
            self.lineChart.data(self.jsonData, function(d) {
                return d.name;
            }).selectAll(".tipCircle").data(function(d) {
                return d.values;
            }).transition().duration(1000).attr("cy", function(d, i) {
                if (self.chartLayout == "stackTotal") {
                    return self.scales.y(d.y1Total);
                } else {
                    if (self.chartLayout == "stackPercent") {
                        return self.scales.y(d.y1Percent);
                    } else {
                        return self.scales.y(d[self.dataType]);
                    }
                }
            }).attr("cx", function(d, i) {
                return self.scales.x(d.date);
            }).attr("r", function(d, i) {
                if (isNaN(d[self.dataType])) {
                    return 0;
                }
                return 5;
            });

            self.lineChart.data(self.jsonData, function(d) {
                return d.name;
            }).selectAll(".tipCircle").data(function(d) {
                return d.values;
            }).exit().transition().duration(1000).attr("r", 0).each("end", function(d) {
                d3.select(this).remove();
            });

            self.lineChart.data(self.jsonData, function(d) {
                    return d.name;
                }).selectAll(".tipCircle").data(function(d) {
                    return d.values;
                }).enter().append("circle").attr("class", "tipCircle").attr("cx", function(d, i) {
                    return self.scales.x(d.date);
                }).attr("cy", function(d, i) {
                    if (self.chartLayout == "stackTotal") {
                        return self.scales.y(d.y1Total);
                    } else {
                        if (self.chartLayout == "stackPercent") {
                            return self.scales.y(d.y1Percent);
                        } else {
                            return self.scales.y(d[self.dataType]);
                        }
                    }
                }).style('opacity', function(d) {
                    if (self.markDataPoints) {
                        return 1;
                    }
                    return 0;
                }).style("fill", function(d) {
                    return self.colorScale(d.name);
                }) //1e-6
                .attr("r", 0).transition().duration(1000).attr("r", function(d, i) {
                    if (isNaN(d[self.dataType])) {
                        return 0;
                    }
                    return 5;
                });

            self.trigger("update:end");

            //end of update
        }
        //end model
});
//# sourceMappingURL=lineChart.js.map
//individual points
Reuters.Graphics.DataPointModel = Backbone.Model.extend({
    initialize: function initialize(attributes, options) {
        return;
    },

    parse: function parse(point, options) {
        if (point.date) {
            point.rawDate = point.date;
            point.date = options.collection.parseDate(point.date);
            point.displayDate = options.collection.dateFormat(point.date);
        }
        return point;
    }
});

//the collection of datapoint which will sort by date.
Reuters.Graphics.DataPointCollection = Backbone.Collection.extend({
    initialize: function initialize(models, options) {
        var self = this;
        _.each(options, function(item, key) {
            self[key] = item;
        });
    },

    comparator: function comparator(item) {
        var self = this;
        return item.get("date");
    },

    model: Reuters.Graphics.DataPointModel,

    parse: function parse(data) {
        var self = this;
        return data;
    }
});

//a model for each collection of data
Reuters.Graphics.DataSeriesModel = Backbone.Model.extend({
    defaults: {
        name: undefined,
        values: undefined,
        visible: true
    },

    initialize: function initialize(attributes, options) {
        var self = this;
        var totalChange = 0;
        var cumulate = 0;
        var name = self.get("name");
        var firstItem = self.get("values").at(0);
        var firstValue = parseFloat(firstItem.get(name));

        self.get("values").each(function(currentItemInLoop) {
            var previousItem = self.get("values").at(self.get("values").indexOf(currentItemInLoop) - 1);
            var currentValue = parseFloat(currentItemInLoop.get(name));
            var change, percent;
            if (previousItem) {
                var previousValue = currentValue;
                if (previousItem.get(name)) {
                    previousValue = parseFloat(previousItem.get(name).value);
                }
                change = currentValue - previousValue;
                totalChange += change;
                cumulate += currentValue;
                percent = (currentValue / firstValue - 1) * 100;
                currentItemInLoop.set(name, {
                    changePreMonth: change,
                    cumulate: cumulate,
                    CumulativeChange: totalChange,
                    percentChange: percent,
                    value: currentValue / options.collection.divisor
                });
            } else {
                currentItemInLoop.set(name, {
                    changePreMonth: 0,
                    cumulate: currentValue,
                    CumulativeChange: 0,
                    percentChange: 0,
                    value: currentValue / options.collection.divisor
                });
            }
        });
    },

    parse: function parse(point, options) {
        return point;
    }
});

//a collection of those collectioins
Reuters.Graphics.DateSeriesCollection = Backbone.Collection.extend({
    initialize: function initialize(models, options) {
        var self = this;
        _.each(options, function(item, key) {
            self[key] = item;
        });
    },

    comparator: function comparator(item) {
        var self = this;
        var name = item.get("name");
        var lastItem = item.get("values").last();
        // for time series, is going to be last value

        if (self.groupSort == "none") {
            return;
        }
        for (var index = item.get("values").length - 1; index > -1; index--) {
            if (!isNaN(parseFloat(item.get("values").at(index).get(name)[self.dataType]))) {
                lastItem = item.get("values").at(index);
                break;
            }
        }
        var valueForSort = lastItem.get(name)[self.dataType];

        //if categories, find greatest value for each
        if (lastItem.get("category")) {
            valueForSort = item.get("values").max(function(d) {
                return d.get(name)[self.dataType];
            }).get(name)[self.dataType];
        }

        var plusMinus = 1;
        if (Array.isArray(self.groupSort)) {
            return self.groupSort.indexOf(name);
        }
        if (self.groupSort == "descending") {
            plusMinus = -1;
        }
        return plusMinus * valueForSort;
    },

    model: Reuters.Graphics.DataSeriesModel,

    parse: function parse(data) {
        var self = this;

        self.on('reset', function(d) {
            if (d.first().get("values").first().get("category")) {
                self.categorySorter(d);
            }
            self.makeStacks(d);
        });
        self.on("change", function(d) {
            self.makeStacks(d);
        });

        return data;
    },

    categorySorter: function categorySorter(item) {
        var self = this;
        var name = item.last().get("name");
        var plusMinus = 1;
        if (self.categorySort == "descending") {
            plusMinus = -1;
        }
        if (self.categorySort == "none") {
            return;
        }

        item.last().get("values").models.sort(function(a, b) {
            if (a.get(name)[self.dataType] > b.get(name)[self.dataType]) {
                return 1 * plusMinus;
            }
            if (a.get(name)[self.dataType] < b.get(name)[self.dataType]) {
                return -1 * plusMinus;
            }
            return 0;
        });

        if (!Array.isArray(self.categorySort)) {
            self.categorySort = [];
            item.last().get("values").each(function(d) {
                self.categorySort.push(d.get("category"));
            });
        }

        item.each(function(d) {
            d.get("values").models.sort(function(a, b) {
                if (self.categorySort.indexOf(a.get("category")) > self.categorySort.indexOf(b.get("category"))) {
                    return 1;
                }
                if (self.categorySort.indexOf(a.get("category")) < self.categorySort.indexOf(b.get("category"))) {
                    return -1;
                }
                return 0;
            });
        });
    },

    makeStacks: function makeStacks(item) {
        var self = this;
        var filtered = self.filter(function(d) {
            return d.get("visible");
        });

        filtered.forEach(function(eachGroup, indexofKey) {
            var name = eachGroup.get("name");
            //back to here
            eachGroup.get("values").each(function(d, i) {
                var masterPositive = 0;
                var masterNegative = 0;
                var stackTotal = 0;
                var masterPercent = 0;
                var stackMin = 0;
                var thisValue = d.get(name)[self.dataType];
                var counter;

                filtered.forEach(function(collection, counter) {
                    var loopName = collection.get("name");
                    var currentValue = collection.get("values").at(i).get(loopName)[self.dataType];
                    if (currentValue >= 0) {
                        stackTotal = stackTotal + currentValue;
                    } else {
                        stackMin = stackMin + currentValue;
                    }
                });

                for (counter = indexofKey; counter < filtered.length; counter++) {
                    var loopName = filtered[counter].get("name");
                    var currentValue = filtered[counter].get("values").at(i).get(loopName)[self.dataType];
                    if (currentValue > 0) {
                        masterPositive = masterPositive + currentValue;
                        masterPercent = masterPercent + currentValue / stackTotal * 100;
                    } else {
                        masterNegative = masterNegative + currentValue;
                    }
                }
                var y0Total = masterPositive - thisValue;
                var y1Total = masterPositive;
                if (thisValue < 0) {
                    y0Total = masterNegative - thisValue;
                    y1Total = masterNegative;
                }
                d.set(name, _.extend(d.get(name), {
                    y0Total: y0Total,
                    y1Total: y1Total,
                    stackTotal: stackTotal,
                    stackMin: stackMin,
                    y0Percent: masterPercent - thisValue / stackTotal * 100,
                    y1Percent: masterPercent
                }));
            });
        });
    }

});
//# sourceMappingURL=dataModels.js.map

(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["basicChartLayout"] = function (t) {
    var __t,
        __p = '';
    __p += '<div class="container">\n    <div class="row">\n        <div class="col-xs-12">\n            <h1 class="graphic-title">' + ((__t = 'The title for a graphic') == null ? '' : __t) + '</h1>\n            <p class="graphic-subhead">' + ((__t = 'The subhead for the graphic The subhead for the graphic The subhead for the graphic The subhead for the graphic The subhead for the graphic The subhead for the graphic') == null ? '' : __t) + ' </p>\n        </div>\n    </div>\n    \n    <div class="row">\n        <div class="col-sm-6">\n            <p class="graphic-chart-label">' + ((__t = 'A label for a chart') == null ? '' : __t) + '</p>\n            <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>\n            <div id="reutersGraphic-chart1" class="reuters-chart"></div>\n        </div>\n\n        <div class="col-sm-6">\n            <p class="graphic-chart-label">' + ((__t = 'A label for a chart') == null ? '' : __t) + '</p>\n            <p class="graphic-chart-subhead">' + ((__t = 'A subhead for a chart') == null ? '' : __t) + '</p>\n            <div id="reutersGraphic-chart2" class="reuters-chart"></div>\n        </div>\n\n    </div>\n        \n    <div class="row">            \n        <div class="col-xs-12 mt-1">\n            <p class="graphic-source">' + ((__t = 'Sources: XXXXX YYYYY') == null ? '' : __t) + '<br />' + ((__t = 'By First Name Last Name | REUTERS GRAPHICS') == null ? '' : __t) + '</p>\n        </div>\n    </div>\n</div>\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["chartTemplate"] = function (t) {
    var __t,
        __p = '',
        __j = Array.prototype.join;
    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.self.multiDataColumns) {
      ;
      __p += '\n	<div class="chart-nav">\n		<div class="navContainer">\n            <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                ';
      t.self.multiDataColumns.forEach(function (d, i) {
        ;
        __p += '                \n                    <label dataid="' + ((__t = d) == null ? '' : __t) + '" class="btn btn-primary ';
        if (i == t.self.multiDataLabels.length - 1) {
          ;
          __p += 'active';
        };
        __p += ' smaller">\n                        <input type="radio" name="nav-options" autocomplete="off"> \n                        ' + ((__t = t.self.multiDataLabels[i]) == null ? '' : __t) + '\n                    </label>\n                ';
      });
      __p += '\n            </div>    		    		\n		</div>    	\n	</div>\n';
    };
    __p += '\n';
    if (t.self.navSpacer) {
      ;
      __p += '\n	<div class="chart-nav">\n		<div class="navContainer"></div>\n	</div>\n';
    };
    __p += '\n\n';
    if (t.self.chartLayoutLables) {
      ;
      __p += '\n	<div class="chart-layout"></div>\n';
    };
    __p += '\n<div class="chart-holder \n	';
    if (!t.self.hasLegend) {
      print('no-legend');
    };
    __p += '\n	">\n	<div class="legend nested-legend"></div>\n	<div class="chart nested-chart"></div>\n</div>';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["delivery"] = function (t) {
    var __t,
        __p = '';
    __p += '<div id=\'g-delivery-box\' class=\'ai2html\'>\n	<!-- Generated by ai2html v0.61 - 2017-03-24 - 18:05 -->\n	<!-- ai file: delivery -->\n\n	<style type=\'text/css\' media=\'screen,print\'>\n		.g-artboard {\n			margin:0 auto;\n		}\n	</style>\n\n\n	<style type=\'text/css\' media=\'screen,print\'>\n		/* Custom CSS block 1 */\n\n\n		#g-delivery-xl {\n		display:block;\n		margin:0 auto;\n		}\n\n\n		#g-delivery-md,\n		#g-delivery-lg,\n		#g-delivery-sm,\n		#g-delivery-xs{\n		display:none;\n		margin:0;\n		}\n\n		@media (max-width: 1200px) {\n			#g-delivery-xl,\n			#g-delivery-md,\n			#g-delivery-sm,\n			#g-delivery-xs{\n			display:none;\n			}\n			#g-delivery-lg{\n			display:block;\n			margin: 0 auto;\n			}\n		 }\n\n\n\n		@media (max-width: 992px) {\n			#g-delivery-xl,\n			#g-delivery-lg,\n			#g-delivery-sm,\n			#g-delivery-xs{\n			display:none;\n			}\n			#g-delivery-md {\n			display:block;\n			margin: 0 auto;\n			}\n		 }\n\n		@media (max-width: 765px) {\n			#g-delivery-xl,\n			#g-delivery-md,\n			#g-delivery-lg,\n			#g-delivery-xs{\n			display:none;\n			}\n			#g-delivery-sm {\n			display:block;\n			margin: 0 auto;\n			}\n		 }\n\n		@media (max-width: 576px) {\n			#g-delivery-xl,\n			#g-delivery-md,\n			#g-delivery-lg,\n			#g-delivery-sm{\n			display:none;\n			}\n			#g-delivery-xs {\n			display:block;\n			margin: 0 auto;\n			}\n		 }\n	</style>\n\n	<!-- Artboard: xl -->\n	<div id=\'g-delivery-xl\' class=\'g-artboard g-artboard-v3 \' data-min-width=\'1110\'>\n		<style type=\'text/css\' media=\'screen,print\'>\n			#g-delivery-xl{\n				position:relative;\n				overflow:hidden;\n				width:1110px;\n			}\n			.g-aiAbs{\n				position:absolute;\n			}\n			.g-aiImg{\n				display:block;\n				width:100% !important;\n			}\n			#g-delivery-xl p{\n				font-family:nyt-franklin,arial,helvetica,sans-serif;\n				font-size:13px;\n				line-height:18px;\n				margin:0;\n			}\n			#g-delivery-xl .g-aiPstyle0 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				font-weight:undefined;\n				color:#ffffff;\n			}\n			#g-delivery-xl .g-aiPstyle1 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				font-weight:100;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-xl .g-aiPstyle2 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				font-weight:bold;\n				color:#ffffff;\n			}\n			#g-delivery-xl .g-aiPstyle3 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:100;\n				text-align:right;\n				color:#ffffff;\n			}\n			#g-delivery-xl .g-aiPstyle4 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:bold;\n				text-align:center;\n				color:#fee000;\n			}\n			#g-delivery-xl .g-aiPstyle5 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:undefined;\n				text-align:center;\n				color:#fee000;\n			}\n			#g-delivery-xl .g-aiPstyle6 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:bold;\n				text-align:center;\n				letter-spacing:-0.02083333333333em;\n				color:#abafff;\n			}\n			#g-delivery-xl .g-aiPstyle7 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:undefined;\n				text-align:center;\n				color:#abafff;\n			}\n			#g-delivery-xl .g-aiPstyle8 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:bold;\n				text-align:center;\n				color:#77aac7;\n			}\n			#g-delivery-xl .g-aiPstyle9 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:undefined;\n				text-align:center;\n				color:#77aac7;\n			}\n			#g-delivery-xl .g-aiPstyle10 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:15px;\n				line-height:23px;\n				font-weight:bold;\n				text-align:center;\n				letter-spacing:-0.02083333333333em;\n				color:#77aac7;\n			}\n			.g-aiPtransformed p { white-space: nowrap; }\n		</style>\n		<div id=\'g-delivery-xl-graphic\'>\n			<img id=\'g-ai0-0\'\n				class=\'g-aiImg\'\n				src=\'images/graphics/delivery-xl.png\'\n				/>\n			<div id=\'g-ai0-1\' class=\'g-Layer_6 g-aiAbs\' style=\'top:0.2674%;left:35.2304%;\'>\n				<p class=\'g-aiPstyle0\'>Operational</p>\n			</div>\n			<div id=\'g-ai0-2\' class=\'g-Layer_6 g-aiAbs\' style=\'top:0.2674%;left:44.4511%;\'>\n				<p class=\'g-aiPstyle0\'>Flight tested</p>\n			</div>\n			<div id=\'g-ai0-3\' class=\'g-Layer_6 g-aiAbs\' style=\'top:0.2674%;left:53.6921%;\'>\n				<p class=\'g-aiPstyle0\'>In development or untested</p>\n			</div>\n			<div id=\'g-ai0-4\' class=\'g-Layer_6 g-aiAbs\' style=\'top:24.5997%;left:67.2784%;width:7.1071%;margin-left:-3.5535%;\'>\n				<p class=\'g-aiPstyle1\'>Alaska</p>\n				<p class=\'g-aiPstyle1\'>(U.S.)</p>\n			</div>\n			<div id=\'g-ai0-5\' class=\'g-Layer_6 g-aiAbs\' style=\'top:28.6105%;left:31.4327%;width:7.9937%;margin-left:-3.9969%;\'>\n				<p class=\'g-aiPstyle1\'>RUSSIA</p>\n			</div>\n			<div id=\'g-ai0-6\' class=\'g-Layer_6 g-aiAbs\' style=\'top:29.6801%;left:78.7085%;width:9.0627%;margin-left:-4.5313%;\'>\n				<p class=\'g-aiPstyle1\'>CANADA</p>\n			</div>\n			<div id=\'g-ai0-7\' class=\'g-Layer_6 g-aiAbs\' style=\'top:40.5093%;left:45.0013%;\'>\n				<p class=\'g-aiPstyle2\'>N.KOREA</p>\n			</div>\n			<div id=\'g-ai0-8\' class=\'g-Layer_6 g-aiAbs\' style=\'top:40.5093%;left:81.9343%;width:4.2675%;margin-left:-2.1337%;\'>\n				<p class=\'g-aiPstyle1\'>U.S.</p>\n			</div>\n			<div id=\'g-ai0-9\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;right:90.2323%;\'>\n				<p class=\'g-aiPstyle3\'>Name</p>\n				<p class=\'g-aiPstyle3\'>Origin</p>\n				<p class=\'g-aiPstyle3\'>Warhead (kg)</p>\n				<p class=\'g-aiPstyle3\'>Range (km)</p>\n			</div>\n			<div id=\'g-ai0-10\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:16.1174%;width:13.4254%;margin-left:-6.7127%;\'>\n				<p class=\'g-aiPstyle4\'>Hwasong-5</p>\n				<p class=\'g-aiPstyle5\'>Russia</p>\n				<p class=\'g-aiPstyle5\'>1,000</p>\n				<p class=\'g-aiPstyle5\'>300</p>\n			</div>\n			<div id=\'g-ai0-11\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:36.0057%;width:9.612%;margin-left:-4.806%;\'>\n				<p class=\'g-aiPstyle4\'>Rodong</p>\n				<p class=\'g-aiPstyle5\'>N. Korea</p>\n				<p class=\'g-aiPstyle5\'>700</p>\n				<p class=\'g-aiPstyle5\'>1,300</p>\n			</div>\n			<div id=\'g-ai0-12\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:45.9528%;width:16.4036%;margin-left:-8.2018%;\'>\n				<p class=\'g-aiPstyle6\'>Pukkuksong-2</p>\n				<p class=\'g-aiPstyle7\'>N. Korea</p>\n				<p class=\'g-aiPstyle7\'>Unknown</p>\n				<p class=\'g-aiPstyle7\'>2,000</p>\n			</div>\n			<div id=\'g-ai0-13\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:55.8972%;width:14.9321%;margin-left:-7.466%;\'>\n				<p class=\'g-aiPstyle6\'>Taepodong-1</p>\n				<p class=\'g-aiPstyle7\'>N. Korea</p>\n				<p class=\'g-aiPstyle7\'>500</p>\n				<p class=\'g-aiPstyle7\'>2,500</p>\n			</div>\n			<div id=\'g-ai0-14\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:75.783%;width:10.9855%;margin-left:-5.4928%;\'>\n				<p class=\'g-aiPstyle8\'>KN-08</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>750-1,000</p>\n				<p class=\'g-aiPstyle9\'>5,000+</p>\n			</div>\n			<div id=\'g-ai0-15\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:85.7301%;width:14.9321%;margin-left:-7.466%;\'>\n				<p class=\'g-aiPstyle10\'>Taepodong-2</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>700-1,000</p>\n				<p class=\'g-aiPstyle9\'>6,700</p>\n			</div>\n			<div id=\'g-ai0-16\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:95.6717%;width:10.9665%;margin-left:-5.4833%;\'>\n				<p class=\'g-aiPstyle10\'>KN-14</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>Unknown</p>\n				<p class=\'g-aiPstyle9\'>8,000</p>\n			</div>\n			<div id=\'g-ai0-17\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:26.0617%;width:13.4254%;margin-left:-6.7127%;\'>\n				<p class=\'g-aiPstyle4\'>Hwasong-6</p>\n				<p class=\'g-aiPstyle5\'>Russia</p>\n				<p class=\'g-aiPstyle5\'>700</p>\n				<p class=\'g-aiPstyle5\'>500</p>\n			</div>\n			<div id=\'g-ai0-18\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.1685%;left:65.8387%;width:10.9312%;margin-left:-5.4656%;\'>\n				<p class=\'g-aiPstyle4\'>Musudan</p>\n				<p class=\'g-aiPstyle5\'>N. Korea</p>\n				<p class=\'g-aiPstyle5\'>650</p>\n				<p class=\'g-aiPstyle5\'>3,000+</p>\n			</div>\n		</div>\n	</div>\n\n	<!-- Artboard: lg -->\n	<div id=\'g-delivery-lg\' class=\'g-artboard g-artboard-v3 \' data-min-width=\'930\' data-max-width=\'1109\'>\n		<style type=\'text/css\' media=\'screen,print\'>\n			#g-delivery-lg{\n				position:relative;\n				overflow:hidden;\n				width:930px;\n			}\n			.g-aiAbs{\n				position:absolute;\n			}\n			.g-aiImg{\n				display:block;\n				width:100% !important;\n			}\n			#g-delivery-lg p{\n				font-family:nyt-franklin,arial,helvetica,sans-serif;\n				font-size:13px;\n				line-height:18px;\n				margin:0;\n			}\n			#g-delivery-lg .g-aiPstyle0 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:17px;\n				font-weight:undefined;\n				color:#ffffff;\n			}\n			#g-delivery-lg .g-aiPstyle1 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:17px;\n				font-weight:100;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-lg .g-aiPstyle2 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:17px;\n				font-weight:bold;\n				color:#ffffff;\n			}\n			#g-delivery-lg .g-aiPstyle3 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:100;\n				text-align:right;\n				color:#ffffff;\n			}\n			#g-delivery-lg .g-aiPstyle4 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:bold;\n				text-align:center;\n				color:#fee000;\n			}\n			#g-delivery-lg .g-aiPstyle5 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:undefined;\n				text-align:center;\n				color:#fee000;\n			}\n			#g-delivery-lg .g-aiPstyle6 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:bold;\n				text-align:center;\n				letter-spacing:-0.02083333333333em;\n				color:#abafff;\n			}\n			#g-delivery-lg .g-aiPstyle7 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:undefined;\n				text-align:center;\n				color:#abafff;\n			}\n			#g-delivery-lg .g-aiPstyle8 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:bold;\n				text-align:center;\n				color:#77aac7;\n			}\n			#g-delivery-lg .g-aiPstyle9 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:undefined;\n				text-align:center;\n				color:#77aac7;\n			}\n			#g-delivery-lg .g-aiPstyle10 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:19px;\n				font-weight:bold;\n				text-align:center;\n				letter-spacing:-0.02083333333333em;\n				color:#77aac7;\n			}\n			.g-aiPtransformed p { white-space: nowrap; }\n		</style>\n		<div id=\'g-delivery-lg-graphic\'>\n			<img id=\'g-ai1-0\'\n				class=\'g-aiImg\'\n				src=\'images/graphics/delivery-lg.png\'\n				/>\n			<div id=\'g-ai1-1\' class=\'g-Layer_6 g-aiAbs\' style=\'top:1.1102%;left:34.5389%;\'>\n				<p class=\'g-aiPstyle0\'>Operational</p>\n			</div>\n			<div id=\'g-ai1-2\' class=\'g-Layer_6 g-aiAbs\' style=\'top:1.1102%;left:44.8148%;\'>\n				<p class=\'g-aiPstyle0\'>Flight tested</p>\n			</div>\n			<div id=\'g-ai1-3\' class=\'g-Layer_6 g-aiAbs\' style=\'top:1.1102%;left:55.1134%;\'>\n				<p class=\'g-aiPstyle0\'>In development or untested</p>\n			</div>\n			<div id=\'g-ai1-4\' class=\'g-Layer_6 g-aiAbs\' style=\'top:24.5835%;left:66.9417%;width:7.8671%;margin-left:-3.9336%;\'>\n				<p class=\'g-aiPstyle1\'>Alaska</p>\n				<p class=\'g-aiPstyle1\'>(U.S.)</p>\n			</div>\n			<div id=\'g-ai1-5\' class=\'g-Layer_6 g-aiAbs\' style=\'top:28.5485%;left:31.2754%;width:8.8485%;margin-left:-4.4242%;\'>\n				<p class=\'g-aiPstyle1\'>RUSSIA</p>\n			</div>\n			<div id=\'g-ai1-6\' class=\'g-Layer_6 g-aiAbs\' style=\'top:29.5001%;left:78.3147%;width:10.0317%;margin-left:-5.0159%;\'>\n				<p class=\'g-aiPstyle1\'>CANADA</p>\n			</div>\n			<div id=\'g-ai1-7\' class=\'g-Layer_6 g-aiAbs\' style=\'top:40.2851%;left:44.7762%;\'>\n				<p class=\'g-aiPstyle2\'>N.KOREA</p>\n			</div>\n			<div id=\'g-ai1-8\' class=\'g-Layer_6 g-aiAbs\' style=\'top:40.2851%;left:81.5244%;width:4.7238%;margin-left:-2.3619%;\'>\n				<p class=\'g-aiPstyle1\'>U.S.</p>\n			</div>\n			<div id=\'g-ai1-9\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;right:90.2812%;\'>\n				<p class=\'g-aiPstyle3\'>Name</p>\n				<p class=\'g-aiPstyle3\'>Origin</p>\n				<p class=\'g-aiPstyle3\'>Warhead (kg)</p>\n				<p class=\'g-aiPstyle3\'>Range (km)</p>\n			</div>\n			<div id=\'g-ai1-10\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:16.0366%;width:14.861%;margin-left:-7.4305%;\'>\n				<p class=\'g-aiPstyle4\'>Hwasong-5</p>\n				<p class=\'g-aiPstyle5\'>Russia</p>\n				<p class=\'g-aiPstyle5\'>1,000</p>\n				<p class=\'g-aiPstyle5\'>300</p>\n			</div>\n			<div id=\'g-ai1-11\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:25.9312%;width:14.861%;margin-left:-7.4305%;\'>\n				<p class=\'g-aiPstyle4\'>Hwasong-6</p>\n				<p class=\'g-aiPstyle5\'>Russia</p>\n				<p class=\'g-aiPstyle5\'>700</p>\n				<p class=\'g-aiPstyle5\'>500</p>\n			</div>\n			<div id=\'g-ai1-12\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:35.8257%;width:10.6399%;margin-left:-5.32%;\'>\n				<p class=\'g-aiPstyle4\'>Rodong</p>\n				<p class=\'g-aiPstyle5\'>N. Korea</p>\n				<p class=\'g-aiPstyle5\'>700</p>\n				<p class=\'g-aiPstyle5\'>1,300</p>\n			</div>\n			<div id=\'g-ai1-13\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:45.7209%;width:18.1576%;margin-left:-9.0788%;\'>\n				<p class=\'g-aiPstyle6\'>Pukkuksong-2</p>\n				<p class=\'g-aiPstyle7\'>N. Korea</p>\n				<p class=\'g-aiPstyle7\'>Unknown</p>\n				<p class=\'g-aiPstyle7\'>2,000</p>\n			</div>\n			<div id=\'g-ai1-14\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:55.6155%;width:16.5289%;margin-left:-8.2644%;\'>\n				<p class=\'g-aiPstyle6\'>Taepodong-1</p>\n				<p class=\'g-aiPstyle7\'>N. Korea</p>\n				<p class=\'g-aiPstyle7\'>500</p>\n				<p class=\'g-aiPstyle7\'>2,500</p>\n			</div>\n			<div id=\'g-ai1-15\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:75.4039%;width:12.1602%;margin-left:-6.0801%;\'>\n				<p class=\'g-aiPstyle8\'>KN-08</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>750-1,000</p>\n				<p class=\'g-aiPstyle9\'>5,000+</p>\n			</div>\n			<div id=\'g-ai1-16\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:85.2993%;width:16.5289%;margin-left:-8.2644%;\'>\n				<p class=\'g-aiPstyle10\'>Taepodong-2</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>700-1,000</p>\n				<p class=\'g-aiPstyle9\'>6,700</p>\n			</div>\n			<div id=\'g-ai1-17\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:95.1931%;width:12.1392%;margin-left:-6.0696%;\'>\n				<p class=\'g-aiPstyle10\'>KN-14</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>Unknown</p>\n				<p class=\'g-aiPstyle9\'>8,000</p>\n			</div>\n			<div id=\'g-ai1-18\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.5972%;left:65.5094%;width:12.1001%;margin-left:-6.0501%;\'>\n				<p class=\'g-aiPstyle4\'>Musudan</p>\n				<p class=\'g-aiPstyle5\'>N. Korea</p>\n				<p class=\'g-aiPstyle5\'>650</p>\n				<p class=\'g-aiPstyle5\'>3,000+</p>\n			</div>\n		</div>\n	</div>\n\n	<!-- Artboard: md -->\n	<div id=\'g-delivery-md\' class=\'g-artboard g-artboard-v3 \' data-min-width=\'690\' data-max-width=\'929\'>\n		<style type=\'text/css\' media=\'screen,print\'>\n			#g-delivery-md{\n				position:relative;\n				overflow:hidden;\n				width:690px;\n			}\n			.g-aiAbs{\n				position:absolute;\n			}\n			.g-aiImg{\n				display:block;\n				width:100% !important;\n			}\n			#g-delivery-md p{\n				font-family:nyt-franklin,arial,helvetica,sans-serif;\n				font-size:13px;\n				line-height:18px;\n				margin:0;\n			}\n			#g-delivery-md .g-aiPstyle0 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:17px;\n				font-weight:undefined;\n				color:#ffffff;\n			}\n			#g-delivery-md .g-aiPstyle1 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:14px;\n				font-weight:100;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-md .g-aiPstyle2 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:14px;\n				font-weight:bold;\n				color:#ffffff;\n			}\n			#g-delivery-md .g-aiPstyle3 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:100;\n				text-align:right;\n				color:#ffffff;\n			}\n			#g-delivery-md .g-aiPstyle4 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:bold;\n				text-align:center;\n				color:#fee000;\n			}\n			#g-delivery-md .g-aiPstyle5 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:undefined;\n				text-align:center;\n				color:#fee000;\n			}\n			#g-delivery-md .g-aiPstyle6 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:bold;\n				text-align:center;\n				letter-spacing:-0.02083333333333em;\n				color:#abafff;\n			}\n			#g-delivery-md .g-aiPstyle7 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:undefined;\n				text-align:center;\n				color:#abafff;\n			}\n			#g-delivery-md .g-aiPstyle8 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:bold;\n				text-align:center;\n				color:#77aac7;\n			}\n			#g-delivery-md .g-aiPstyle9 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:undefined;\n				text-align:center;\n				color:#77aac7;\n			}\n			#g-delivery-md .g-aiPstyle10 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:16px;\n				font-weight:bold;\n				text-align:center;\n				letter-spacing:-0.02083333333333em;\n				color:#77aac7;\n			}\n			.g-aiPtransformed p { white-space: nowrap; }\n		</style>\n		<div id=\'g-delivery-md-graphic\'>\n			<img id=\'g-ai2-0\'\n				class=\'g-aiImg\'\n				src=\'images/graphics/delivery-md.png\'\n				/>\n			<div id=\'g-ai2-1\' class=\'g-Layer_6 g-aiAbs\' style=\'top:0.604%;left:27.2137%;\'>\n				<p class=\'g-aiPstyle0\'>Operational</p>\n			</div>\n			<div id=\'g-ai2-2\' class=\'g-Layer_6 g-aiAbs\' style=\'top:0.604%;left:41.0638%;\'>\n				<p class=\'g-aiPstyle0\'>Flight tested</p>\n			</div>\n			<div id=\'g-ai2-3\' class=\'g-Layer_6 g-aiAbs\' style=\'top:0.8053%;left:54.9445%;\'>\n				<p class=\'g-aiPstyle0\'>In development or untested</p>\n			</div>\n			<div id=\'g-ai2-4\' class=\'g-Layer_6 g-aiAbs\' style=\'top:23.9586%;left:67.0432%;width:9.0885%;margin-left:-4.5443%;\'>\n				<p class=\'g-aiPstyle1\'>Alaska</p>\n				<p class=\'g-aiPstyle1\'>(U.S.)</p>\n			</div>\n			<div id=\'g-ai2-5\' class=\'g-Layer_6 g-aiAbs\' style=\'top:27.5826%;left:31.2578%;width:10.2225%;margin-left:-5.1112%;\'>\n				<p class=\'g-aiPstyle1\'>RUSSIA</p>\n			</div>\n			<div id=\'g-ai2-6\' class=\'g-Layer_6 g-aiAbs\' style=\'top:28.5892%;left:78.4542%;width:11.5897%;margin-left:-5.7948%;\'>\n				<p class=\'g-aiPstyle1\'>CANADA</p>\n			</div>\n			<div id=\'g-ai2-7\' class=\'g-Layer_6 g-aiAbs\' style=\'top:38.8572%;left:44.8036%;\'>\n				<p class=\'g-aiPstyle2\'>N.KOREA</p>\n			</div>\n			<div id=\'g-ai2-8\' class=\'g-Layer_6 g-aiAbs\' style=\'top:38.8572%;left:81.6746%;width:5.4574%;margin-left:-2.7287%;\'>\n				<p class=\'g-aiPstyle1\'>U.S.</p>\n			</div>\n			<div id=\'g-ai2-9\' class=\'g-Layer_6 g-aiAbs\' style=\'top:85.9691%;right:90.452%;\'>\n				<p class=\'g-aiPstyle3\'>Name</p>\n				<p class=\'g-aiPstyle3\'>Origin</p>\n				<p class=\'g-aiPstyle3\'>Warhead (kg)</p>\n				<p class=\'g-aiPstyle3\'>Range (km)</p>\n			</div>\n			<div id=\'g-ai2-10\' class=\'g-Layer_6 g-aiAbs\' style=\'top:85.9691%;left:65.8019%;width:13.979%;margin-left:-6.9895%;\'>\n				<p class=\'g-aiPstyle4\'>Musudan</p>\n				<p class=\'g-aiPstyle5\'>N. Korea</p>\n				<p class=\'g-aiPstyle5\'>650</p>\n				<p class=\'g-aiPstyle5\'>3,000+</p>\n			</div>\n			<div id=\'g-ai2-11\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:15.2819%;width:17.1683%;margin-left:-8.5841%;\'>\n				<p class=\'g-aiPstyle4\'>Hwasong-5</p>\n				<p class=\'g-aiPstyle5\'>Russia</p>\n				<p class=\'g-aiPstyle5\'>1,000</p>\n				<p class=\'g-aiPstyle5\'>300</p>\n			</div>\n			<div id=\'g-ai2-12\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:25.3859%;width:17.1685%;margin-left:-8.5843%;\'>\n				<p class=\'g-aiPstyle4\'>Hwasong-6</p>\n				<p class=\'g-aiPstyle5\'>Russia</p>\n				<p class=\'g-aiPstyle5\'>700</p>\n				<p class=\'g-aiPstyle5\'>500</p>\n			</div>\n			<div id=\'g-ai2-13\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:35.4899%;width:12.2922%;margin-left:-6.1461%;\'>\n				<p class=\'g-aiPstyle4\'>Rodong</p>\n				<p class=\'g-aiPstyle5\'>N. Korea</p>\n				<p class=\'g-aiPstyle5\'>700</p>\n				<p class=\'g-aiPstyle5\'>1,300</p>\n			</div>\n			<div id=\'g-ai2-14\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:45.5939%;width:20.9771%;margin-left:-10.4886%;\'>\n				<p class=\'g-aiPstyle6\'>Pukkuksong-2</p>\n				<p class=\'g-aiPstyle7\'>N. Korea</p>\n				<p class=\'g-aiPstyle7\'>Unknown</p>\n				<p class=\'g-aiPstyle7\'>2,000</p>\n			</div>\n			<div id=\'g-ai2-15\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:56.4227%;width:19.0953%;margin-left:-9.5477%;\'>\n				<p class=\'g-aiPstyle6\'>Taepodong-1</p>\n				<p class=\'g-aiPstyle7\'>N. Korea</p>\n				<p class=\'g-aiPstyle7\'>500</p>\n				<p class=\'g-aiPstyle7\'>2,500</p>\n			</div>\n			<div id=\'g-ai2-16\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:75.9058%;width:14.0489%;margin-left:-7.0245%;\'>\n				<p class=\'g-aiPstyle8\'>KN-08</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>750-1,000</p>\n				<p class=\'g-aiPstyle9\'>5,000+</p>\n			</div>\n			<div id=\'g-ai2-17\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:86.0098%;width:19.0953%;margin-left:-9.5477%;\'>\n				<p class=\'g-aiPstyle10\'>Taepodong-2</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>700-1,000</p>\n				<p class=\'g-aiPstyle9\'>6,700</p>\n			</div>\n			<div id=\'g-ai2-18\' class=\'g-Layer_6 g-aiAbs\' style=\'top:86.1704%;left:96.1139%;width:14.024%;margin-left:-7.012%;\'>\n				<p class=\'g-aiPstyle10\'>KN-14</p>\n				<p class=\'g-aiPstyle9\'>N. Korea</p>\n				<p class=\'g-aiPstyle9\'>Unknown</p>\n				<p class=\'g-aiPstyle9\'>8,000</p>\n			</div>\n		</div>\n	</div>\n\n	<!-- Artboard: sm -->\n	<div id=\'g-delivery-sm\' class=\'g-artboard g-artboard-v3 \' data-min-width=\'510\' data-max-width=\'689\'>\n		<style type=\'text/css\' media=\'screen,print\'>\n			#g-delivery-sm{\n				position:relative;\n				overflow:hidden;\n				width:510px;\n			}\n			.g-aiAbs{\n				position:absolute;\n			}\n			.g-aiImg{\n				display:block;\n				width:100% !important;\n			}\n			#g-delivery-sm p{\n				font-family:nyt-franklin,arial,helvetica,sans-serif;\n				font-size:13px;\n				line-height:18px;\n				margin:0;\n			}\n			#g-delivery-sm .g-aiPstyle0 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:14px;\n				font-weight:100;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-sm .g-aiPstyle1 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:14px;\n				font-weight:bold;\n				color:#ffffff;\n			}\n			#g-delivery-sm .g-aiPstyle2 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:17px;\n				font-weight:undefined;\n				color:#ffffff;\n			}\n			#g-delivery-sm .g-aiPstyle3 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:16px;\n				font-weight:bold;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-sm .g-aiPstyle4 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:14px;\n				line-height:16px;\n				font-weight:undefined;\n				text-align:center;\n				color:#ffffff;\n			}\n			.g-aiPtransformed p { white-space: nowrap; }\n		</style>\n		<div id=\'g-delivery-sm-graphic\'>\n			<img id=\'g-ai3-0\'\n				class=\'g-aiImg\'\n				src=\'images/graphics/delivery-sm.png\'\n				/>\n			<div id=\'g-ai3-1\' class=\'g-Layer_6 g-aiAbs\' style=\'top:10.5684%;left:78.5866%;width:12.2963%;margin-left:-6.1481%;\'>\n				<p class=\'g-aiPstyle0\'>Alaska</p>\n				<p class=\'g-aiPstyle0\'>(U.S.)</p>\n			</div>\n			<div id=\'g-ai3-2\' class=\'g-Layer_6 g-aiAbs\' style=\'top:12.7301%;left:29.9748%;width:13.8304%;margin-left:-6.9152%;\'>\n				<p class=\'g-aiPstyle0\'>RUSSIA</p>\n			</div>\n			<div id=\'g-ai3-3\' class=\'g-Layer_6 g-aiAbs\' style=\'top:13.3306%;left:93.8289%;width:15.6798%;margin-left:-7.8399%;\'>\n				<p class=\'g-aiPstyle0\'>CANADA</p>\n			</div>\n			<div id=\'g-ai3-4\' class=\'g-Layer_6 g-aiAbs\' style=\'top:19.3354%;left:48.3016%;\'>\n				<p class=\'g-aiPstyle1\'>N.KOREA</p>\n			</div>\n			<div id=\'g-ai3-5\' class=\'g-Layer_6 g-aiAbs\' style=\'top:19.3354%;left:95.9638%;width:7.3836%;margin-left:-3.6918%;\'>\n				<p class=\'g-aiPstyle0\'>U.S.</p>\n			</div>\n			<div id=\'g-ai3-6\' class=\'g-Layer_6 g-aiAbs\' style=\'top:19.9358%;left:33.3062%;width:12.0941%;margin-left:-6.047%;\'>\n				<p class=\'g-aiPstyle0\'>CHINA</p>\n			</div>\n			<div id=\'g-ai3-7\' class=\'g-Layer_6 g-aiAbs\' style=\'top:45.1559%;left:4.3778%;\'>\n				<p class=\'g-aiPstyle2\'>Operational</p>\n			</div>\n			<div id=\'g-ai3-8\' class=\'g-Layer_6 g-aiAbs\' style=\'top:45.1559%;left:23.1159%;\'>\n				<p class=\'g-aiPstyle2\'>Flight tested</p>\n			</div>\n			<div id=\'g-ai3-9\' class=\'g-Layer_6 g-aiAbs\' style=\'top:45.1559%;left:41.8952%;\'>\n				<p class=\'g-aiPstyle2\'>In development or untested</p>\n			</div>\n			<div id=\'g-ai3-10\' class=\'g-Layer_6 g-aiAbs\' style=\'top:48.7587%;left:91.881%;width:27.0994%;margin-left:-13.5497%;\'>\n				<p class=\'g-aiPstyle3\'>Hwasong-5</p>\n			</div>\n			<div id=\'g-ai3-11\' class=\'g-Layer_6 g-aiAbs\' style=\'top:50.8003%;left:71.1126%;width:112.0918%;margin-left:-56.0459%;\'>\n				<p class=\'g-aiPstyle4\'>Origin: Russia - Warhead: 1,000kg - Range: 300km</p>\n			</div>\n			<div id=\'g-ai3-12\' class=\'g-Layer_6 g-aiAbs\' style=\'top:54.2831%;left:92.3607%;width:27.0994%;margin-left:-13.5497%;\'>\n				<p class=\'g-aiPstyle3\'>Hwasong-6</p>\n			</div>\n			<div id=\'g-ai3-13\' class=\'g-Layer_6 g-aiAbs\' style=\'top:56.4448%;left:86.1775%;width:51.8321%;margin-left:-25.9161%;\'>\n				<p class=\'g-aiPstyle4\'>Russia - 700kg - 500km</p>\n			</div>\n			<div id=\'g-ai3-14\' class=\'g-Layer_6 g-aiAbs\' style=\'top:59.8075%;left:94.4772%;width:18.6336%;margin-left:-9.3168%;\'>\n				<p class=\'g-aiPstyle3\'>Rodong</p>\n			</div>\n			<div id=\'g-ai3-15\' class=\'g-Layer_6 g-aiAbs\' style=\'top:61.9692%;left:84.3081%;width:59.3099%;margin-left:-29.6549%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea- 700kg - 1,300km</p>\n			</div>\n			<div id=\'g-ai3-16\' class=\'g-Layer_6 g-aiAbs\' style=\'top:65.452%;left:90.4461%;width:34.758%;margin-left:-17.379%;\'>\n				<p class=\'g-aiPstyle3\'>Pukkuksong-2</p>\n			</div>\n			<div id=\'g-ai3-17\' class=\'g-Layer_6 g-aiAbs\' style=\'top:67.4936%;left:76.9307%;width:88.8193%;margin-left:-44.4097%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea- Warhead unknown - 2,000km</p>\n			</div>\n			<div id=\'g-ai3-18\' class=\'g-Layer_6 g-aiAbs\' style=\'top:70.9764%;left:91.2229%;width:31.6506%;margin-left:-15.8253%;\'>\n				<p class=\'g-aiPstyle3\'>Taepodong-1</p>\n			</div>\n			<div id=\'g-ai3-19\' class=\'g-Layer_6 g-aiAbs\' style=\'top:73.018%;left:84.3081%;width:59.3099%;margin-left:-29.6549%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea- 500kg - 2,500km</p>\n			</div>\n			<div id=\'g-ai3-20\' class=\'g-Layer_6 g-aiAbs\' style=\'top:76.5008%;left:93.6193%;width:22.065%;margin-left:-11.0325%;\'>\n				<p class=\'g-aiPstyle3\'>Musudan</p>\n			</div>\n			<div id=\'g-ai3-21\' class=\'g-Layer_6 g-aiAbs\' style=\'top:78.6625%;left:84.4715%;width:58.6562%;margin-left:-29.3281%;\'>\n				<p class=\'g-aiPstyle4\'>Russia - 650kg - 3,000+km</p>\n			</div>\n			<div id=\'g-ai3-22\' class=\'g-Layer_6 g-aiAbs\' style=\'top:82.0251%;left:95.4777%;width:14.6312%;margin-left:-7.3156%;\'>\n				<p class=\'g-aiPstyle3\'>KN-08</p>\n			</div>\n			<div id=\'g-ai3-23\' class=\'g-Layer_6 g-aiAbs\' style=\'top:84.1869%;left:80.406%;width:74.918%;margin-left:-37.459%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea-750-1,000kg - 5,000+km</p>\n			</div>\n			<div id=\'g-ai3-24\' class=\'g-Layer_6 g-aiAbs\' style=\'top:87.6696%;left:91.2229%;width:31.6506%;margin-left:-15.8253%;\'>\n				<p class=\'g-aiPstyle3\'>Taepodong-2</p>\n			</div>\n			<div id=\'g-ai3-25\' class=\'g-Layer_6 g-aiAbs\' style=\'top:89.7113%;left:81.0882%;width:72.1894%;margin-left:-36.0947%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea-700-1,000kg - 6,700km</p>\n			</div>\n			<div id=\'g-ai3-26\' class=\'g-Layer_6 g-aiAbs\' style=\'top:93.194%;left:95.4777%;width:14.6312%;margin-left:-7.3156%;\'>\n				<p class=\'g-aiPstyle3\'>KN-14</p>\n			</div>\n			<div id=\'g-ai3-27\' class=\'g-Layer_6 g-aiAbs\' style=\'top:95.2356%;left:77.2134%;width:87.6884%;margin-left:-43.8442%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea-Warhead unknown - 8,000km</p>\n			</div>\n		</div>\n	</div>\n\n	<!-- Artboard: xs -->\n	<div id=\'g-delivery-xs\' class=\'g-artboard g-artboard-v3 \' data-min-width=\'320\' data-max-width=\'509\'>\n		<style type=\'text/css\' media=\'screen,print\'>\n			#g-delivery-xs{\n				position:relative;\n				overflow:hidden;\n				width:100%;\n			}\n			.g-aiAbs{\n				position:absolute;\n			}\n			.g-aiImg{\n				display:block;\n				width:100% !important;\n			}\n			#g-delivery-xs p{\n				font-family:nyt-franklin,arial,helvetica,sans-serif;\n				font-size:13px;\n				line-height:18px;\n				margin:0;\n			}\n			#g-delivery-xs .g-aiPstyle0 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:10px;\n				line-height:12px;\n				font-weight:100;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-xs .g-aiPstyle1 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:10px;\n				line-height:12px;\n				font-weight:bold;\n				color:#ffffff;\n			}\n			#g-delivery-xs .g-aiPstyle2 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:15px;\n				font-weight:undefined;\n				color:#ffffff;\n			}\n			#g-delivery-xs .g-aiPstyle3 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:10px;\n				font-weight:bold;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-xs .g-aiPstyle4 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:10px;\n				font-weight:undefined;\n				text-align:center;\n				color:#ffffff;\n			}\n			#g-delivery-xs .g-aiPstyle5 {\n				font-family:Source Sans Pro,helvetica,sans-serif;\n				font-size:12px;\n				line-height:13px;\n				font-weight:undefined;\n				text-align:right;\n				color:#ffffff;\n			}\n			.g-aiPtransformed p { white-space: nowrap; }\n		</style>\n		<div id=\'g-delivery-xs-graphic\'>\n			<img id=\'g-ai4-0\'\n				class=\'g-aiImg\'\n				src=\'images/graphics/delivery-xs.png\'\n				/>\n			<div id=\'g-ai4-1\' class=\'g-Layer_6 g-aiAbs\' style=\'top:9.5874%;left:75.566%;width:16.3312%;margin-left:-8.1656%;\'>\n				<p class=\'g-aiPstyle0\'>Alaska</p>\n				<p class=\'g-aiPstyle0\'>(U.S.)</p>\n			</div>\n			<div id=\'g-ai4-2\' class=\'g-Layer_6 g-aiAbs\' style=\'top:11.5613%;left:15.2681%;width:18.3685%;margin-left:-9.1843%;\'>\n				<p class=\'g-aiPstyle0\'>RUSSIA</p>\n			</div>\n			<div id=\'g-ai4-3\' class=\'g-Layer_6 g-aiAbs\' style=\'top:12.1252%;left:93.013%;width:20.8252%;margin-left:-10.4126%;\'>\n				<p class=\'g-aiPstyle0\'>CANADA</p>\n			</div>\n			<div id=\'g-ai4-4\' class=\'g-Layer_6 g-aiAbs\' style=\'top:17.6239%;left:38.0005%;\'>\n				<p class=\'g-aiPstyle1\'>N.KOREA</p>\n			</div>\n			<div id=\'g-ai4-5\' class=\'g-Layer_6 g-aiAbs\' style=\'top:17.6239%;left:94.9314%;width:9.8059%;margin-left:-4.903%;\'>\n				<p class=\'g-aiPstyle0\'>U.S.</p>\n			</div>\n			<div id=\'g-ai4-6\' class=\'g-Layer_6 g-aiAbs\' style=\'top:18.1878%;left:19.4003%;width:16.062%;margin-left:-8.031%;\'>\n				<p class=\'g-aiPstyle0\'>CHINA</p>\n			</div>\n			<div id=\'g-ai4-7\' class=\'g-Layer_6 g-aiAbs\' style=\'top:39.1955%;left:3.3206%;\'>\n				<p class=\'g-aiPstyle2\'>Operational</p>\n			</div>\n			<div id=\'g-ai4-8\' class=\'g-Layer_6 g-aiAbs\' style=\'top:39.1955%;left:29.1345%;\'>\n				<p class=\'g-aiPstyle2\'>Flight tested</p>\n			</div>\n			<div id=\'g-ai4-9\' class=\'g-Layer_6 g-aiAbs\' style=\'top:39.3365%;left:55.0061%;\'>\n				<p class=\'g-aiPstyle2\'>In development or untested</p>\n			</div>\n			<div id=\'g-ai4-10\' class=\'g-Layer_6 g-aiAbs\' style=\'top:43.0022%;left:89.7151%;width:37.0197%;margin-left:-18.5098%;\'>\n				<p class=\'g-aiPstyle3\'>Hwasong-5</p>\n			</div>\n			<div id=\'g-ai4-11\' class=\'g-Layer_6 g-aiAbs\' style=\'top:44.9761%;left:60.6888%;width:153.125%;margin-left:-76.5625%;\'>\n				<p class=\'g-aiPstyle4\'>Origin: Russia - Warhead: 1,000kg - Range: 300km</p>\n			</div>\n			<div id=\'g-ai4-12\' class=\'g-Layer_6 g-aiAbs\' style=\'top:48.7829%;left:89.7151%;width:37.0197%;margin-left:-18.5098%;\'>\n				<p class=\'g-aiPstyle3\'>Hwasong-6</p>\n			</div>\n			<div id=\'g-ai4-13\' class=\'g-Layer_6 g-aiAbs\' style=\'top:50.7567%;left:81.2682%;width:70.8063%;margin-left:-35.4031%;\'>\n				<p class=\'g-aiPstyle4\'>Russia - 700kg - 500km</p>\n			</div>\n			<div id=\'g-ai4-14\' class=\'g-Layer_6 g-aiAbs\' style=\'top:54.4225%;left:92.6064%;width:25.4547%;margin-left:-12.7274%;\'>\n				<p class=\'g-aiPstyle3\'>Rodong</p>\n			</div>\n			<div id=\'g-ai4-15\' class=\'g-Layer_6 g-aiAbs\' style=\'top:56.2554%;left:78.3359%;width:82.5366%;margin-left:-41.2683%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea - 700kg - 1,300km</p>\n			</div>\n			<div id=\'g-ai4-16\' class=\'g-Layer_6 g-aiAbs\' style=\'top:60.2031%;left:87.0996%;width:47.4817%;margin-left:-23.7408%;\'>\n				<p class=\'g-aiPstyle3\'>Pukkuksong-2</p>\n			</div>\n			<div id=\'g-ai4-17\' class=\'g-Layer_6 g-aiAbs\' style=\'top:62.036%;left:75.4257%;width:94.176%;margin-left:-47.088%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea - Unknown - 2,000km</p>\n			</div>\n			<div id=\'g-ai4-18\' class=\'g-Layer_6 g-aiAbs\' style=\'top:65.7018%;left:88.1607%;width:43.2373%;margin-left:-21.6187%;\'>\n				<p class=\'g-aiPstyle3\'>Taepodong-1</p>\n			</div>\n			<div id=\'g-ai4-19\' class=\'g-Layer_6 g-aiAbs\' style=\'top:67.5347%;left:78.3359%;width:82.5366%;margin-left:-41.2683%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea - 500kg - 2,500km</p>\n			</div>\n			<div id=\'g-ai4-20\' class=\'g-Layer_6 g-aiAbs\' style=\'top:71.3414%;left:91.4345%;width:30.1422%;margin-left:-15.0711%;\'>\n				<p class=\'g-aiPstyle3\'>Musudan</p>\n			</div>\n			<div id=\'g-ai4-21\' class=\'g-Layer_6 g-aiAbs\' style=\'top:73.0333%;left:78.9379%;width:80.1288%;margin-left:-40.0644%;\'>\n				<p class=\'g-aiPstyle4\'>Russia - 650kg - 3,000+km</p>\n			</div>\n			<div id=\'g-ai4-22\' class=\'g-Layer_6 g-aiAbs\' style=\'top:76.9811%;left:93.5945%;width:19.9872%;margin-left:-9.9936%;\'>\n				<p class=\'g-aiPstyle3\'>KN-08</p>\n			</div>\n			<div id=\'g-ai4-23\' class=\'g-Layer_6 g-aiAbs\' style=\'top:78.8139%;left:73.0054%;width:103.8586%;margin-left:-51.9293%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea -750-1,000kg - 5,000+km</p>\n			</div>\n			<div id=\'g-ai4-24\' class=\'g-Layer_6 g-aiAbs\' style=\'top:82.9027%;left:87.782%;width:43.2373%;margin-left:-21.6187%;\'>\n				<p class=\'g-aiPstyle3\'>Taepodong-2</p>\n			</div>\n			<div id=\'g-ai4-25\' class=\'g-Layer_6 g-aiAbs\' style=\'top:84.5946%;right:1.4087%;\'>\n				<p class=\'g-aiPstyle5\'>N. Korea -700-1,000kg </p>\n				<p class=\'g-aiPstyle5\'>6,700km</p>\n			</div>\n			<div id=\'g-ai4-26\' class=\'g-Layer_6 g-aiAbs\' style=\'top:90.3752%;left:93.9733%;width:19.9872%;margin-left:-9.9936%;\'>\n				<p class=\'g-aiPstyle3\'>KN-14</p>\n			</div>\n			<div id=\'g-ai4-27\' class=\'g-Layer_6 g-aiAbs\' style=\'top:92.2081%;left:75.788%;width:91.1462%;margin-left:-45.5731%;\'>\n				<p class=\'g-aiPstyle4\'>N. Korea-Unknown - 8,000km</p>\n			</div>\n		</div>\n	</div>\n\n\n	<!-- End ai2html - 2017-03-24 - 18:05 -->\n</div>\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["featureHeader"] = function (t) {
    var __t,
        __p = '';
    __p += '<!-- <div class="feature-header container-fluid"> -->\n<div class="custom-header">\n  <div class="header-content">\n    <div class="row">\n        <img class="displayimg photo3" src="images/header/header3.jpg">\n        <img class="displayimg photo2" src="images/header/header2.jpg">\n        <img class="displayimg photo1" src="images/header/header1.jpg">\n    </div>\n    <div class="feature-header-title-container d.block">\n        <div class="row">\n           <h1 class="display-3 col-xs-12 text-xs-center" style="color:white">' + ((__t = 'Nuclear North Korea') == null ? '' : __t) + '</h1>\n        </div>\n        <div class="row headerdivde">\n          <div class="col-xs-4 offset-xs-1">\n            <hr class="topdivide">\n          </div>\n\n          <div class="col-xs-2">\n            <img src ="images/header/icon.svg"/>\n          </div>\n\n          <div class="col-xs-4">\n            <hr class="topdivide">\n          </div>\n        </div>\n        <div class="row">\n            <!-- <p class="col-xs-10 offset-xs-1 text-xs-center intro">' + ((__t = 'North Korea has dramatically increased the pace of its missile testing and defied U.N. sanctions to push further ahead with the development of nuclear weapons.') == null ? '' : __t) + '</p> -->\n            <p class="col-xs-10 offset-xs-1 text-xs-center intro">' + ((__t = "Below is a look at all aspects of the North's nuclear weapons programme and missile capabilities.") == null ? '' : __t) + '</p>\n        </div>\n        <div class="row">\n            <p class="graphic-timestamp col-md-12 text-xs-center">' + ((__t = 'UPDATED April 5, 2017') == null ? '' : __t) + '</p>\n            <!-- <p class="graphic-timestamp col-md-12 text-xs-center">' + ((__t = 'January 9, 2017') == null ? '' : __t) + '</p> -->\n        </div>\n        <div class="row scroll mt-1">\n          <p class=" col-md-12 text-xs-center">' + ((__t = 'Scroll to continue') == null ? '' : __t) + '</p>\n          <p class=" col-md-12 text-xs-center arrow"><i class="fa fa-angle-double-down" aria-hidden="true"></i></p>\n          </div>\n    </div>\n  </div>\n</div>\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

	window["Reuters"]["Graphics"]["Template"]["featureLayout"] = function (t) {
    var __t,
        __p = '';
    __p += '<div class="container graphic-section-container">\n\n<!-- Facilities -->\n        <div class="row mt-2">\n          <img class="mx-auto" style="width:100px;display:block;" src="images/icons/facilities.png" />\n        </div>\n        <div class="row">\n            <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mb-3">\n                <h2 class="text-xs-center sectionname">' + ((__t = 'Facilities') == null ? '' : __t) + '</h2>\n                <p class="graphic-chart-subhead">' + ((__t = "The country's nuclear development began in the 1950s and has since grown into a source of deep concern for the international community. Pyongyang has developed a nuclear fuel cycle capability and has both plutonium and enriched uranium programs capable of producing weapons-grade material. At the heart of the nuclear programme is the Yongbyon nuclear reactor and reprocessing plant.") == null ? '' : __t) + '</p>\n                <!-- <div id="reutersGraphic-chart1" class="reuters-chart"></div> -->\n            </div>\n\n        </div>\n\n        <div class="row mb-3">\n          <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2">\n            <img class="img-fluid" src="images/graphics/facilities.png"/>\n          </div>\n        </div>\n\n<!-- Divide line -->\n  <div class="divide">\n  <hr class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mb-3" style="border-color:#ffe100;">\n</div>\n\n<!-- Testings -->\n\n   <div class="row">\n     <div class="col-sm-12">\n     <img class="mx-auto" style="width:75px;display:block;" src="images/icons/testing.png" />\n   </div>\n   </div>\n   <div class="row">\n       <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2">\n           <h2 class="text-xs-center sectionname">' + ((__t = 'Testing') == null ? '' : __t) + '</h2>\n           <p class="graphic-chart-subhead">' + ((__t = 'The frequency of missile and nuclear tests has increased under Kim Jong Un, with the most activity occurring in 2016, including the country’s most powerful nuclear test to date. The North has conducted five nuclear weapons tests since 2006. ') == null ? '' : __t) + '</p>\n           <br>\n           <p class="graphic-chart-subhead"><img src="images/body/il.png" style="width:60px;display:inline-block;" /> Kim Jong Il leadership<br class="hidden-lg-up"><img class="un" src="images/body/un.png" style="width:60px;display:inline-block;" />Kim Jong Un leadership</p>\n       </div>\n\n   </div>\n\n   <!-- <div class="row">\n     <div class="col-sm-12">\n       <img class="img-fluid" src="images/graphics/testing-graphic.png"/>\n     </div>\n   </div> -->\n\n\n   <!-- Testing chart!! -->\n   <!-- <div class="row">\n     <div class="col-sm-12">\n       <img class="img-fluid" src="images/body/legend.svg"/>\n     </div>\n   </div> -->\n<!-- Missile chart -->\n   <div class="row hidden-sm-down">\n    <div class="col-xs-12">\n      <div class="missilelabel">\n        <p class="graphic-chart-label">' + ((__t = 'Ballistic missile launches') == null ? '' : __t) + '</p>\n        <p class="graphic-chart-subhead">' + ((__t = 'Launches per month since 2003*') == null ? '' : __t) + '</p>\n\n      </div>\n        <div id="reutersGraphic-missiles" class="reuters-chart"></div>\n    </div>\n  </div>\n\n    <div class="row timeline hidden-sm-down">\n      <div class="col-xs-12">\n        <img class="img-fluid hidden-lg-down" src="images/body/timeline2.svg"/>\n        <img class="img-fluid hidden-md-down hidden-xl-up" src="images/body/time-l.svg"/>\n        <img class="img-fluid hidden-sm-down hidden-lg-up" src="images/body/time-md.svg"/>\n        <!-- <img class="img-fluid hidden-md-up" src="images/body/time-sm.svg"/> -->\n      </div>\n    </div>\n<!-- Nuclear chart -->\n<div class="row nuclearchart">\n    <div class="col-xs-12 hidden-sm-down">\n        <p class="graphic-chart-label">' + ((__t = 'Nuclear Tests') == null ? '' : __t) + '</p>\n        <p class="graphic-chart-subhead">' + ((__t = 'Yield, in kilotonnes') == null ? '' : __t) + '</p>\n        <div id="reutersGraphic-nuclear" class="reuters-chart hidden-sm-down"></div>\n    </div>\n    <img class="img-fluid hidden-md-up mb-3" src="images/graphics/test-sm.png"/>\n</div>\n\n<!-- Detecting -->\n   <div class="row detecting">\n     <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2">\n       <p class="graphic-chart-label text-sm-center">' + ((__t = 'Detecting a test') == null ? '' : __t) + '</p>\n       <p class="graphic-chart-subhead mt-1">' + ((__t = 'Both earthquakes and underground nuclear tests cause seismic disturbances that are quickly detected by meteorological agencies and scientific institutes. However, these disturbances are very different in nature.') == null ? '' : __t) + '</p>\n        <p class="graphic-chart-subhead mt-1">' + ((__t = 'Seismic waves produced by an earthquake portray a distinct pattern when recorded on a seismogram, allowing scientists to easily spot an abnormal occurrence.') == null ? '' : __t) + '</p>\n     </div>\n       <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mt-3">\n          <p class="graphic-subhead">' + ((__t = 'Earthquake') == null ? '' : __t) + '</p>\n         <img class="img-fluid hidden-lg-down" src="images/graphics/ew-xl.svg"/>\n         <img class="img-fluid hidden-md-down hidden-xl-up" src="images/graphics/ew-lg.svg"/>\n         <img class="img-fluid hidden-sm-down hidden-lg-up" src="images/graphics/ew-md.png"/>\n         <img class="img-fluid hidden-md-up" src="images/graphics/ew-sm.png"/>\n       </div>\n\n       <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2">\n          <p class="graphic-subhead">' + ((__t = 'Nuclear Test') == null ? '' : __t) + '</p>\n          <img class="img-fluid hidden-lg-down" src="images/graphics/nw-xl.svg"/>\n          <img class="img-fluid hidden-md-down hidden-xl-up" src="images/graphics/nw-lg.svg"/>\n          <img class="img-fluid hidden-sm-down hidden-lg-up" src="images/graphics/nw-md.png"/>\n          <img class="img-fluid hidden-md-up" src="images/graphics/nw-sm.png"/>\n         <p class="graphic-chart-subhead mt-1">' + ((__t = 'Earthquakes emit two distinct types of waves that travel through the interior of the earth, P waves, or primary waves, and S waves, or secondary waves. Both are recorded on seismographs when they reach an earthquake monitoring station.') == null ? '' : __t) + '</p>\n         <p class="graphic-subhead mt-3" style="margin-bottom:0;">' + ((__t = 'Primary Wave') == null ? '' : __t) + '</p>\n         <p class="graphic-chart-subhead">' + ((__t = 'The first energy wave to radiate from an earthquake. Fast but rarely causes damage. Particles move in the same direction that the wave is moving.') == null ? '' : __t) + '</p>\n         <img class="img-fluid" src="images/graphics/waves-both.gif"/>\n         <br class="hidden-lg-up">\n         <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Secondary wave') == null ? '' : __t) + '</p>\n         <p class="graphic-chart-subhead">' + ((__t = 'A slower energy wave that can only move through solid rock. Brings stronger shaking. Particles move up and down, or side-to-side, perpendicular to the direction that the wave is travelling.') == null ? '' : __t) + '</p>\n       </div>\n     </div>\n   </div>\n\n<!-- Photo break -->\n<div class="container-fluid mb-3 photobreak">\n  <div class="col-md-6 col-sm-12">\n      <img class="img-fluid" src="images/body/1.jpg"/>\n      <p class="graphic-source"> ' + ((__t = "North Korean leader Kim Jong Un meets scientists and technicians in the field of nuclear weapon research. PHOTO: Korean Central News Agency (KCNA)") == null ? '' : __t) + '</p>\n  </div>\n\n  <div class="col-md-6 col-sm-12 hidden-sm-down">\n      <img class="img-fluid" src="images/body/3.jpg"/>\n      <p class="graphic-source"> ' + ((__t = "Administrator of Korea Meteorological Administration points at where seismic waves observed in South Korea came from, during a media briefing at Korea Meteorological Administration in Seoul on Jan 6, 2016, after North Korea conudcted its fourth nuclear test. PHOTO: REUTERS") == null ? '' : __t) + '</p>\n  </div>\n\n</div>\n\n<div class="container graphic-section-container second-half">\n\n<!-- Weaponizng -->\n\n  <div class="row mt-3">\n      <div class="col-sm-12">\n      <img class="mx-auto" style="width:120px;display:block;" src="images/icons/weaponizing.png" />\n    </div>\n  </div>\n  <div class="row">\n    <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mb-3">\n        <h2 class="text-xs-center sectionname">' + ((__t = 'Weaponising') == null ? '' : __t) + '</h2>\n        <p class="graphic-chart-subhead">' + ((__t = "North Korea made considerable progress on weapons technology in 2016 but it's still not clear if the isolated nation has developed an actual nuclear warhead.") == null ? '' : __t) + '</p>\n\n    </div>\n  </div>\n  <div class="row">\n    <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mb-3">\n        <p class="graphic-chart-label text-sm-center mb-1">' + ((__t = 'Types of nuclear weapons') == null ? '' : __t) + '</p>\n        <p class="graphic-chart-subhead">' + ((__t = 'North Korea claimed in January 2016 that it tested a miniaturised hydrogen bomb, also known as a thermonuclear device, a step up from the less powerful atomic bombs they had tested previously. However, outside experts are sceptical and believe the bomb may have been a “boosted” device, and not a full-fledged H-bomb.') == null ? '' : __t) + '</p>\n\n    </div>\n  </div>\n  <div class="row hidden-md-down weaponizing">\n    <div class="col-lg-3 col-md-6 mb-1">\n      <img class="mb-1" src="images/graphics/weapon1.svg"/>\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Plutonium device') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'Explosive charges compress the plutonium fissile core causing fission to occur, producing an explosion.') == null ? '' : __t) + '</p>\n    </div>\n    <div class="col-lg-3 col-md-6 mb-1">\n      <img class="mb-1" src="images/graphics/weapon2.svg"/>\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Uranium device') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'Known as a gun-type fission device, it fires a mass of uranium into another to create a supercritical mass.') == null ? '' : __t) + '</p>\n    </div>\n    <div class="col-lg-3 col-md-6 mb-1">\n      <img class="mb-1" src="images/graphics/weapon3.svg"/>\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Boosted device') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'Hydrogen isotopes are incorporated in the fissile core to accelerate fission upon detonation of the device.') == null ? '' : __t) + '</p>\n    </div>\n    <div class="col-lg-3 col-md-6 mb-1">\n      <img class="mb-1" src="images/graphics/weapon4.svg"/>\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Thermonuclear') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'The most advanced warhead. A primary component is detonated which triggers a secondary fusion explosion.') == null ? '' : __t) + '</p>\n    </div>\n  </div>\n\n  <div class="row hidden-lg-up weaponizing">\n    <div class="col-lg-3 col-md-6 offset-md-0 col-sm-8 offset-sm-2 mb-1">\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Plutonium device') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'Explosive charges compress the plutonium fissile core causing fission to occur, producing an explosion.') == null ? '' : __t) + '</p>\n      <img class="mb-1" src="images/graphics/weapon1.svg"/>\n    </div>\n    <div class="col-lg-3 col-md-6 offset-md-0  col-sm-8 offset-sm-2 mb-1">\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Uranium device') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'Known as a gun-type fission device, it fires a mass of uranium into another to create a supercritical mass.') == null ? '' : __t) + '</p>\n      <img class="mb-1" src="images/graphics/weapon2.svg"/>\n    </div>\n    <div class="boosted col-lg-3 col-md-6 offset-md-0  col-sm-8 offset-sm-2  mb-1">\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Boosted device') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'Hydrogen isotopes are incorporated in the fissile core to accelerate fission upon detonation of the device.') == null ? '' : __t) + '</p>\n      <img class="mb-1" src="images/graphics/weapon3.svg"/>\n    </div>\n    <div class="col-lg-3 col-md-6 offset-md-0 col-sm-8 offset-sm-2  mb-1">\n      <p class="graphic-subhead" style="margin-bottom:0;">' + ((__t = 'Thermonuclear') == null ? '' : __t) + '</p>\n      <p class="graphic-chart-subhead">' + ((__t = 'The most advanced warhead. A primary component is detonated which triggers a secondary fusion explosion.') == null ? '' : __t) + '</p>\n      <img class="mb-1" src="images/graphics/weapon4.svg"/>\n    </div>\n  </div>\n\n<!-- Divide line -->\n  <div class="divide my-3">\n  <hr class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mb-3" style="border-color:#ffe100;">\n</div>\n\n<!-- Delivery -->\n\n  <div class="row">\n      <div class="col-sm-12">\n      <img class="mx-auto" style="width:100px;display:block;" src="images/icons/delivery2.png" />\n    </div>\n  </div>\n  <div class="row">\n    <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 mb-3">\n        <h2 class="text-xs-center sectionname">' + ((__t = 'Delivery') == null ? '' : __t) + '</h2>\n        <p class="graphic-chart-subhead">' + ((__t = 'Nuclear testing is part of a process that moves Pyongyang closer to miniaturise nuclear warheads to mount on intercontinental ballistic missiles. North Korea has well over 1,000 missiles of various ranges. Hwasong-5 and Hwasong-6 are also known as Scud missiles.') == null ? '' : __t) + '</p>\n        <!-- <div id="reutersGraphic-chart1" class="reuters-chart"></div> -->\n    </div>\n  </div>\n\n  <div class="row mb-3">\n    <!-- <img class="col-sm-8 offset-sm-2 hidden-md-down img-fluid" src="images/graphics/delivery.svg"/>\n    <img class="col-md-12 hidden-sm-down hidden-lg-up img-fluid" src="images/graphics/delivery.svg"/>\n    <img class="col-sm-12 hidden-md-up img-fluid" src="images/graphics/delivery-sm.png"/> -->\n    <div class="col-xs-12">\n      <div id="missilerange"></div>\n    </div>\n  </div>\n\n\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["scatterSetupTemplate"] = function (t) {
    var __t,
        __p = '',
        __j = Array.prototype.join;
    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.self.dataType) {
      ;
      __p += '\n	<div class="chart-nav">\n			';
      if (!t.self.multiDataSlider) {
        ;
        __p += '\n            		<div class="navContainer">\n                        <div class="btn-group nav-options horizontal" data-toggle="buttons">\n                            ';
        t.self.multiDataLabels.forEach(function (d, i) {
          ;
          __p += '\n                                <label dataid="' + ((__t = d) == null ? '' : __t) + '" class="btn btn-primary ';
          if (i == t.self.multiDataLabels.length - 1) {
            ;
            __p += 'active';
          };
          __p += ' smaller">\n                                    <input type="radio" name="nav-options" autocomplete="off">\n                                    ' + ((__t = d) == null ? '' : __t) + '\n                                </label>\n                            ';
        });
        __p += '\n                        </div>\n            		</div>\n			';
      } else {
        ;
        __p += '\n				<div class="slider-container">\n                    <div class="slider-holder">\n        				<div class="slider" data-slider="true"></div>\n                    </div>\n\n                    <div class="slider-controls">\n                        <div class="btn-group animation-control" data-toggle="buttons">\n                            <label class="btn btn-primary smaller animation-play">\n                                <input type="radio" name="animation-control-group" id="animation-play" autocomplete="off" >\n                                <i class="fa fa-play" aria-hidden="true"></i>\n                            </label>\n                            <label class="btn btn-primary smaller active animation-pause">\n                                <input type="radio" name="animation-control-group" id="animation-pause" autocomplete="off" checked>\n                                <i class="fa fa-pause" aria-hidden="true"></i>\n                            </label>\n                        </div>\n                    </div>\n				</div>\n			';
      };
      __p += '\n	</div>\n';
    };
    __p += '\n\n\n<div class="chart-holder">\n    ';
    if (t.self.colorDomain && t.self.colorDomain.length > 1) {
      ;
      __p += '\n    	<!-- <div class="scatter-nested-legend"> -->\n            <!-- ';
      t.self.colorDomain.forEach(function (d, i) {
        ;
        __p += ' -->\n                <!-- <div class ="scatter-legend-item">\n                	<div class = "scatter-legend-circle circle" style="background-color:' + ((__t = t.self.colors[i]) == null ? '' : __t) + ';"></div>\n                	<p class = "scatter-legend-text">' + ((__t = d) == null ? '' : __t) + '</p>\n                </div>\n            ';
      });
      __p += ' -->\n        	<!-- ';
      if (t.self.rvalue) {
        ;
        __p += ' -->\n                <!-- <br> -->\n                <!-- <div class ="scatter-legend-size">\n                    <div class = "scatter-legend-circle scatter-size circle order-legend"></div>\n                    <p class = "scatter-legend-text">' + ((__t = 'Size indicates Orders') == null ? '' : __t) + '</p>\n                 </div> -->\n        	<!-- ';
      };
      __p += ' -->\n    	</div>\n        <div class="scatter-nested-chart" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + '-chart"></div>\n    ';
    } else {
      ;
      __p += '\n        <div class="" id="' + ((__t = t.self.targetDiv) == null ? '' : __t) + '-chart"></div>\n    ';
    };
    __p += '\n</div>\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["scattertooltip"] = function (t) {
    var __t,
        __p = '';
    __p += '<p class="dateTip"><strong>' + ((__t = t.data.display) == null ? '' : __t) + '</strong></p>\n<p class="valueTip"><strong> ' + ((__t = 'Yield') == null ? '' : __t) + ':</strong> ' + ((__t = t.data.yield) == null ? '' : __t) + ' kilotonnes</p>\n<!-- <p class="valueTip marginoferror"><em>' + ((__t = '(with a margin of error of ') == null ? '' : __t) + '' + ((__t = t.data.error) == null ? '' : __t) + '' + ((__t = ' kilotons') == null ? '' : __t) + ' )</em></p> -->\n<p class="valueTip"><strong> ' + ((__t = 'Magnitude') == null ? '' : __t) + ':</strong> ' + ((__t = t.data.magnitude) == null ? '' : __t) + '</p>\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["tooltip"] = function (t) {
    var __t,
        __p = '',
        __j = Array.prototype.join;
    function print() {
      __p += __j.call(arguments, '');
    }

    if (t.data[0].displayDate) {
      ;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].displayDate) == null ? '' : __t) + ' </div>\n';
    } else {
      ;
      __p += '\n<div class=\'dateTip\'> ' + ((__t = t.data[0].category) == null ? '' : __t) + ' </div>\n';
    };
    __p += '\n';
    t.data.forEach(function (d, i) {
      ;
      __p += '\n		<div class="tipHolder">\n			<!-- <div class=\'circleTip ' + ((__t = t.self.chartType) == null ? '' : __t) + '\' style=\'background-color:';
      print(t.self.colorScale(d.name));
      __p += ';\'></div> -->\n			<!-- <div class=\'nameTip\'>' + ((__t = d.displayName) == null ? '' : __t) + '</div> -->\n			<div class=\'valueTip\'>\n				';
      if (t.self.chartLayout == "stackPercent") {
        ;
        __p += '\n					';
        print(t.self.tipNumbFormat(d.y1Percent - d.y0Percent));
        __p += '\n				';
      } else {
        ;
        __p += '\n					';
        print(t.self.tipNumbFormat(d[t.self.dataType]) + " missiles launched");
        __p += '\n				';
      };
      __p += '\n			</div>\n\n\n';
    });
    __p += '\n';
    if (t.self.timelineData) {
      var timelineData = t.self.timelineDataGrouped[t.self.timelineDate(t.data[0].date)];
      print(t.self.timelineTemplate({ data: timelineData, self: t.self }));
    };
    __p += '\n';
    return __p;
  };
})();
//for translations.
window.gettext = function (text) {
    return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphic = window.Reuters.Graphic || {};
window.Reuters.Graphic.Model = window.Reuters.Graphic.Model || {};
window.Reuters.Graphic.View = window.Reuters.Graphic.View || {};
window.Reuters.Graphic.Collection = window.Reuters.Graphic.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function (hashBased) {
    var query = void 0;
    if (hashBased) {
        var pos = location.href.indexOf('?');
        if (pos == -1) return [];
        query = location.href.substr(pos + 1);
    } else {
        query = location.search.substr(1);
    }
    var result = {};
    query.split('&').forEach(function (part) {
        if (!part) return;
        part = part.split('+').join(' '); // replace every + with space, regexp-free version
        var eq = part.indexOf('=');
        var key = eq > -1 ? part.substr(0, eq) : part;
        var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

        //convert true / false to booleans.
        if (val == 'false') {
            val = false;
        } else if (val == 'true') {
            val = true;
        }

        var f = key.indexOf('[');
        if (f == -1) {
            result[decodeURIComponent(key)] = val;
        } else {
            var to = key.indexOf(']');
            var index = decodeURIComponent(key.substring(f + 1, to));
            key = decodeURIComponent(key.substring(0, f));
            if (!result[key]) {
                result[key] = [];
            }
            if (!index) {
                result[key].push(val);
            } else {
                result[key][index] = val;
            }
        }
    });
    return result;
};

Reuters.trackEvent = function (category, type, id) {
    category = category || 'Page click';
    //console.log(category, type, id);
    var typeString = type;
    if (id) {
        typeString += ': ' + id;
    }
    var gaOpts = {
        'nonInteraction': false,
        'page': PAGE_TO_TRACK
    };

    ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function () {
    $('[data-slider]').each(function () {
        var $el = $(this);
        var getPropArray = function getPropArray(value) {
            if (!value) {
                return [0];
            }
            var out = [];
            var values = value.split(',');
            values.forEach(function (value) {
                out.push(parseFloat(value));
            });
            return out;
        };
        var pips = undefined;
        var start = getPropArray($el.attr('data-start'));
        var min = getPropArray($el.attr('data-min'));
        var max = getPropArray($el.attr('data-max'));
        var orientation = $el.attr('data-orientation') || 'horizontal';
        var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
        var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
        var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
        var snap = $el.attr('data-snap') === 'true' ? true : false;
        var pipMode = $el.attr('data-pip-mode');
        var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
        var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
        var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
        if (pipMode === 'count') {
            pipValues = pipValues[0];
        }

        if (pipMode) {
            pips = {
                mode: pipMode,
                values: pipValues,
                stepped: pipStepped,
                density: pipDensity
            };
        }

        if (connect) {
            (function () {
                var cs = [];
                connect.split(',').forEach(function (c) {
                    c = c === 'true' ? true : false;
                    cs.push(c);
                });
                connect = cs;
            })();
        }

        noUiSlider.create(this, {
            start: start,
            range: {
                min: min,
                max: max
            },
            snap: snap,
            orientation: orientation,
            step: step,
            tooltips: tooltips,
            connect: connect,
            pips: pips
        });
        //This probably doesn't belong here, but will fix the most common use-case.
        $(this).find('div.noUi-marker-large:last').addClass('last');
        $(this).find('div.noUi-marker-large:first').addClass('first');
    });
};

Reuters.hasPym = false;
try {
    Reuters.pymChild = new pym.Child({ polling: 500 });
    if (Reuters.pymChild.id) {
        Reuters.hasPym = true;
        $("body").addClass("pym");
    }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
    $("html").addClass("media");
}
if (Reuters.Graphics.Parameters.eikon) {
    $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
    $("html").addClass("remove-header");
}
//# sourceMappingURL=utils.js.map

// Make the page
$(".main").html(Reuters.Graphics.Template.basicChartLayout());
$(".header1").html(Reuters.Graphics.Template.featureHeader());
$(".main").html(Reuters.Graphics.Template.featureLayout());
$("#missilerange").html(Reuters.Graphics.Template.delivery());

if (!Reuters.hasPym) {
  sizeContent();
}

$(document).ready(function () {
  if (!Reuters.hasPym) {
    $(".scroll").delay(3000).fadeTo(500, 0.7);
  }
});

// Header behavior stuff
var newHeight = $(window).height() + "px";
function sizeContent() {
  $(".custom-header").css("height", newHeight);
  $(".main").css("top", newHeight);

	$(".credits-section,.st-content-footer,.content-fb-comments").css("top",newHeight);
}
if (!Reuters.hasPym) {
  sizeContent();
}

$(document).ready(function () {
  if (!Reuters.hasPym) {
    $(".scroll").delay(3000).fadeTo(500, 0.7);
  }
});

// Missile Chart!!

Reuters.Graphics.sharePrice = new Reuters.Graphics.BarChart({
  el: "#reutersGraphic-missiles",
  dataURL: "data/missiles-update.csv",
  height: .2,
  //			columnNames:{sandp:"S&P 500", disney:"Disney"}, // undefined uses sheet headers, object will map, array matches columnNamesDisplay
  colors: ["#ffe100", purple3, orange3, red3, yellow3], //array or mapped object
  //			YTickLabel: [[gettext(""),"%"]], //  \u00A0  - use that code for a space.
  xScaleTicks: 14,
  yScaleTicks: 2,
  //			dateFormat: d3.time.format("%b %Y"),
  //			numbFormat: d3.format(",.1f"),
  //			divisor:1000,
  //			columnNamesDisplay:["Bob","Jerry"], // only use this if you are using an array for columnNames
  //			colorUpDown:true,
  hasLegend: false,
  showTip: true,
  //			yScaleVals: [0,100],
  //			tickAll:true, //if you make tickAll anything, it will put a tick for each point.
  //			horizontal:true,
  // margin: {top: -100, right: 50, bottom: 0, left: 0},
  parseDate: d3.time.format("%d/%m/%y").parse, // can change the format of the original dates
  // hasZoom: true,
  //			dataStream:true,
  //			timelineData:"data/timeline.csv", //dates much match sosurce dates
  //tipNumbFormat: function(d){
  // 	var self = this;
  // 	if (isNaN(d) === true){return "N/A";}else{
  // 		return "self.dataLabels[0] + self.numbFormat(d) + " " + self.dataLabels[1]" ;
  // 	}
  // },
  //			lineType: "linear",//step-before, step-after
  //			chartBreakPoint:400, //when do you want the legend to go up top
  //			markDataPoints:true,
  //			multiDataColumns:["gpd","unemployment"],//can use value,changePreMonth, CumulativeChange, percentChange
  //			multiDataLabels:["VALUE","PERCENT"],
  //			chartLayout:"stackPercent", // basic,stackTotal, stackPercent, fillLines, sideBySide, onTopOf,
  //			chartLayoutLables:["stackPercent", "basic","stackTotal","fillLines"], //define this, and buttons appear
  //			yTickFormat:function(d){
  //				var numbFormat = d3.format(".2f")
  //				return numbFormat(d)
  //			},
  //			xTickFormat:function(d){
  //				var dateFormat = d3.time.format("%b %Y")
  //				return dateFormat(d)
  //			},
  //			navSpacer:true,
  tipTemplate: Reuters.Graphics.Template.tooltip,
  //			chartTemplate:Reuters.Graphics.Template.chartTemplate,
  //			legendTemplate: Reuters.Graphics.Template.legendTemplate,
  //			timelineTemplate:Reuters.Graphics.Template.tooltipTimeline,
  barFill: function barFill(d) {
    var self = this;
    var yearDate = d.rawDate.split('/')[2];
    if (yearDate < 12) {
      return "#bbbdc0";
    } else {
      return "#ffe100";
    }
  }

});
Reuters.Graphics.sharePrice.on("renderChart:end", function () {
  var self = this;

  // self.label1 = self.svg.append("text")
  //   .attr("x", self.scales.x(self.parseDate("1/1/04")))
  //   .attr("y", self.scales.y(5))
  //   .text("Surface search")
  //   .attr("text-anchor", "start")
  //   .attr("class", "chart-label")
  //   .attr("id", "label1A")
  // 	.style("font-weight","700")
  //   .style("fill","white");

  self.label1 = self.svg.append("line").attr("x1", self.scales.x(self.parseDate("1/4/03"))).attr("y1", self.scales.y(5)).attr("x2", self.scales.x(self.parseDate("1/5/03"))).attr("y2", self.scales.y(5)).style("stroke", "#6B6A6B").attr("class", "hidden-md-down").style("stroke-width", 1);

  self.label1 = self.svg.append("text").attr("x", self.scales.x(self.parseDate("1/1/03"))).attr("y", self.scales.y(4.8)).style("margin-left", "-10px").text("5").attr("class", "hidden-md-down").style("fill", "#6B6A6B");

  self.label1 = self.svg.append("line").attr("x1", self.scales.x(self.parseDate("1/4/03"))).attr("y1", self.scales.y(10)).attr("x2", self.scales.x(self.parseDate("1/5/03"))).attr("y2", self.scales.y(10)).attr("class", "axis").attr("class", "hidden-md-down").style("stroke", "#6B6A6B").style("stroke-width", 1);

  self.label1 = self.svg.append("text").attr("x", self.scales.x(self.parseDate("1/1/03"))).attr("y", self.scales.y(9.8)).style("margin-left", "-10px").text("10").attr("class", "hidden-md-down").style("fill", "#6B6A6B").attr("text-anchor", "middle");
});

// Nuclear Chart!!
Reuters.Graphics.scattergraphic = new Reuters.Graphics.ScatterPlot({
  el: "#reutersGraphic-nuclear",
  dataURL: "data/nuclear.csv",
  xvalue: "date",
  yvalue: "y",
  colorvalue: "time",
  colors: ["#bbbdc0", "#ffe100"],
  //				colorDomain:["Boeing","Airbus","Bombardier","Comac","Embraer","Mitsubishi"],  //can define colors explicitly
  rvalue: "radius",

  radiusModifier: 55, // a multiplier for sized radius's
  //				hardRadius:15,
  yticks: 5,
  xticks: 14,
  height: .3, //if a number smaller then 10, that will be it's aspect to width, if over 10 will be hard height.  if undefined will be same as width
  //				margin:{left:50, right:50, top:50, bottom:50},
  //				xscaleorientation:"bottom",
  //				yscaleorientation:"left",
  //				legendTemplate:Reuters.Graphics.Template.scatterlegend,
  //				tooltipTemplate: Reuters.Graphics.Template.scattertooltip,
  //				dateParseFormat:"%m/%d/%y",
  //				xmin:"0",
  //				ymin:"0",
  //				xmax:100,
  //				ymax:50,
  //				xvalues:[0,25,50,75,100],
  //				yvalues:[0,100],
  //				xLabelText:"This is the x label",
  //				yLabelText:"This is the y label",
  //				dateFormat:d3.time.format("%b %Y"),
  //				multiDataColumns:["gpd","unemployment"],
  //				multiDataLabels:["VALUE","PERCENT"],
  //				multiDataSlider:true,
  //				idField:"uniqueid",
  scatterSetupTemplate: Reuters.Graphics.Template.scatterSetupTemplate,
  tooltipTemplate: Reuters.Graphics.Template.scattertooltip
});

// d3.selectAll("circle")
// 	.style("stroke-width", "3px")
// 	.style("fill-opacity", "0.4")
// 	.style("stroke-opacity","1");
//# sourceMappingURL=main.js.map
