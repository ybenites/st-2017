(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["featureLayout"] = function (t) {
    var __t,
        __p = '';
    __p += '<div class="my-header">\n    <img class="img-fluid feature-header-img hidden-sm-down hidden-xl-up" src="images/header.png" width="100%">\n    <img class="img-fluid feature-header-img hidden-lg-down" src="images/header-lg.png" width="100%">\n    <img class="img-fluid feature-header-img hidden-md-up" src="images/header-xs.png" width="100%">\n    <div class="my-text">\n        <div class="d-flex justify-content-center">\n            <div class="text-center col-md-10 col-lg-8  ">\n                <h1 class="display-4 hidden-xs-down">' + ((__t = 'Battle for Mosul') == null ? '' : __t) + '</h1>\n                <h1 class="display-5 hidden-sm-up">' + ((__t = 'Battle for Mosul') == null ? '' : __t) + '</h1>\n                <p class="by-line-desc hidden-sm-down ">' + ((__t = "Over eight months into the Mosul offensive, ISIS militants have been dislodged from the city. Here&rsquo;s a look at how the battle for Mosul against  the Islamic State in Iraq and Syria unfolded.") == null ? '' : __t) + '</p>\n\n            <time class="graphic-timestamp" itemprop="datePublished" datetime="2017-07-13T21:00:48-0800" data-timestamp="1499927702">PUBLISHED: JULY 13, 2017</time>\n            </div>\n\n        </div>\n\n    </div>\n\n\n<div class="recent">\n    <div class="container graphic-section-container recent">\n      <div>\n        <div class="row justify-content-center hidden-md-up text-white">\n          <p class=" col-12 graphic-chart-subhead-mainHeadLine hidden-md-up mt-1 mb-2">' + ((__t = "Over eight months into the Mosul offensive, ISIS militants have been dislodged from the city. Here&rsquo;s a look at how the battle for Mosul against  the Islamic State in Iraq and Syria unfolded.") == null ? '' : __t) + '</p>\n        </div>\n      </div>\n    </div>\n</div>\n\n<div class="history">\n\n    <div class="container graphic-section-container">\n\n        <div class="row justify-content-center">\n\n            <div class="col-5 col-sm-4 col-md-3 col-lg-2 mt-3 ">\n                <img src="images/iraq.png" width="100%">\n            </div>\n\n        </div>\n\n            <div class="row justify-content-center">\n\n                <div class="col-md-8 col-lg-8">\n                    <h2 class="graphic-subhead mt-1">' + ((__t = 'Closing in on ISIS') == null ? '' : __t) + '</h2>\n<!--                     <p class="graphic-chart-label mt-2">' + ((__t = 'Oct 17 ') == null ? '' : __t) + '</p> -->\n                    <p class="graphic-chart-subhead-main ">' + ((__t = "With a pre-war population of about 2 million people, Mosul is the largest city in the self-styled caliphate declared by the Islamic State in Iraq and Syria (ISIS) in 2014. The Iraqi government launched the offensive to take back Mosul on Oct 17, 2016, with the support of a US-led international coalition.") == null ? '' : __t) + '</p>\n                        <p class="graphic-chart-subhead-main mt-1">' + ((__t = "The militants, believed to number 6,000, used suicide car bombers, snipers and booby traps to counter the offensive waged by the 100,000-strong force of Iraqi troops, Kurdish peshmerga fighters, Iranian-trained Shi'ite Muslim paramilitary groups and Sunni Muslim tribal fighters deployed around Mosul and in the surrounding province of Nineveh. ISIS militants are now mostly driven out of the area, <a href='http://www.straitstimes.com/world/middle-east/after-mosul-other-iraqi-territory-still-in-isis-hands' target='_blank'>other than an enclave to the west of Mosul.</a>") == null ? '' : __t) + '</p>\n\n                </div>\n            </div>\n\n\n\n            <div class="row d-flex justify-content-center">\n                <div class="col-md-6 col-sm-8 col-lg-6 mt-2">\n                  <p class="graphic-chart-subhead text-sm-center">' + ((__t = "Areas of control as of Oct 19, 2016") == null ? '' : __t) + '</p>\n                   <img class="hidden-sm-down" src="images/mosul.png" width=100%>\n                   <img class="hidden-md-up" src="images/mosul-xs.png" width=100%>\n                </div>\n                <div class="col-md-6 col-sm-8 col-lg-6  mt-2">\n                      <p class="graphic-chart-subhead text-sm-center">' + ((__t = "Areas of control as of May 11, 2017") == null ? '' : __t) + '</p>\n                   <img  class="hidden-sm-down" src="images/mosul2.png" width=100%>\n                   <img class="hidden-md-up" src="images/mosul2-xs.png" width=100%>\n                </div>\n\n\n            </div>\n  </div>\n\n<Br>\n\n<div class="yellowR">\n              <div class=" container graphic-section-container yellowR mt-3 ">\n                <div class="row  justify-content-center">\n                    <div class="col-md-8 col-lg-8">\n                        <h2 class="graphic-subhead mt-3">' + ((__t = 'Blown bridges') == null ? '' : __t) + '</h2>\n        <!--                 <p class="graphic-chart-label mt-2">' + ((__t = 'Nov 22') == null ? '' : __t) + '</p> -->\n                        <p class="graphic-chart-subhead-main mb-1">' + ((__t = "By the end of December last year, all five bridges connecting east and west Mosul were destroyed by coalition air strikes in order to obstruct ISIS movements across the Tigris and the group&rsquo;s ability to resupply its fighters on the eastern side of the river.") == null ? '' : __t) + '</p>\n  <p class="graphic-chart-subhead-main mb-1">' + ((__t = "Five bridges span the Tigris that runs through Mosul. They have all been mined and booby-trapped by the militants.") == null ? '' : __t) + '</p>\n                    </div>\n                </div>\n                <div class="row  justify-content-center">\n                    <div class="col-md-12">\n\n                            <img class="hidden-sm-down" src="images/bridges-lg.png" width=100%>\n                                <img class="hidden-md-up col-sm-10 offset-sm-1" src="images/bridges-xs.png" width=100%>\n                    </div>\n\n                </div>\n\n\n\n         <div class="row d-flex justify-content-center">\n                    <div class="col-md-8 col-lg-8">\n                        <h2 class="graphic-subhead mt-3">' + ((__t = 'Retaking eastern Mosul') == null ? '' : __t) + '</h2>\n                        <p class="graphic-chart-subhead-main ">' + ((__t = "Iraqi government forces retook the eastern side of the city this January after 100 days of fighting and 2&#189; years of ISIS occupation.") == null ? '' : __t) + '</p>\n                    </div>\n                </div>\n\n        <div class="row d-flex justify-content-center">\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2">\n                <p class="graphic-chart-subhead text-sm-center">As of Jan 3, 2017</p>\n                <img class="hidden-md-down" src="images/east1.png" width=100%>\n                <img class="hidden-md-up" src="images/east1.png" width=100%>\n                <img class="hidden-lg-up hidden-sm-down" src="images/east1-md.png" width=100%>\n                <p class="graphic-chart-subhead-capt">After a pause to regroup, the Iraqi security forces (ISF) announced the &ldquo;second phase&rdquo; of operations in Mosul&rsquo;s city limits on Dec 29, recapturing five major neighbourhoods and pushing further towards the eastern bank of the Tigris River, putting Mosul Airport and adjacent military base in range of ISF artillery.</p>\n            </div>\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2">\n                <p class="graphic-chart-subhead text-sm-center">As of Jan 18, 2017</p>\n                <img class="hidden-md-down" src="images/east2.png" width=100%>\n                <img class="hidden-md-up" src="images/east2.png" width=100%>\n                <img class="hidden-lg-up hidden-sm-down" src="images/east2-md.png" width=100%>\n                <p class="graphic-chart-subhead-capt">The ISF rapidly consolidated control over eastern Mosul following a major push. They extended control along the Tigris and recaptured the University of Mosul, once ISIS&rsquo; major logistical hub in the city.</p>\n            </div>\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2">\n                <p class="graphic-chart-subhead text-sm-center">As of Jan 23, 2017</p>\n                <img class="hidden-md-down" src="images/east3.png" width=100%>\n                <img class="hidden-md-up" src="images/east3.png" width=100%>\n                <img class="hidden-lg-up hidden-sm-down" src="images/east3-md.png" width=100%>\n                <p class="graphic-chart-subhead-capt">The last neighbourhood in eastern Mosul was retaken on Jan 23, nearing the end of a nearly three-month-long battle to clear that side of the city. The Counter Terrorism Service (CTS) expanded control along the Tigris.</p>\n            </div>\n\n        </div>\n\n\n\n                        <div class="row justify-content-center">\n            <div class="col-md-8 col-lg-8">\n                <h2 class="graphic-subhead mt-3">' + ((__t = 'Capturing western Mosul') == null ? '' : __t) + '</h2>\n                <p class="graphic-chart-subhead-main">' + ((__t = "The campaign to retake western Mosul started on Feb 19. On May 27, Iraqi forces announced the start of operations to capture the remaining enclave under ISIS control, covering the Old City and three adjacent districts along the western bank of the Tigris.") == null ? '' : __t) + '</p>\n            </div>\n        </div>\n\n\n\n        <div class="row d-flex justify-content-center ">\n\n\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2 mb-2">\n                <p class="graphic-chart-subhead  text-sm-center">' + ((__t = "As of March 1, 2017") == null ? '' : __t) + '</p>\n                <img class="hidden-md-down" src="images/west1.png" width=100%>\n                <img class="hidden-md-up" src="images/west1.png" width=100%>\n                <img class="hidden-lg-up hidden-sm-down" src="images/west1-md.png" width=100%>\n                <p class="graphic-chart-subhead-capt">The increased involvement of US advisers, with the ability to call in air strikes without going through a joint operations cell in Baghdad, saw the ISF isolate Mosul completely as units advance within the city boundary towards the government centre.</p>\n            </div>\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2 mb-2">\n                  <p class="graphic-chart-subhead text-sm-center">' + ((__t = "As of May 11, 2017") == null ? '' : __t) + '</p>\n                 <img class="hidden-md-down" src="images/west2.png" width=100%>\n                 <img class="hidden-md-up" src="images/west2.png" width=100%>\n                 <img class="hidden-lg-up hidden-sm-down" src="images/west2-md.png" width=100%>\n                 <p class="graphic-chart-subhead-capt">In a bid to clear the city prior to Ramadan, the ISF and combined forces recaptured districts in northwest Mosul. ISIS promised to continue to defend the Old City, concentrating its defences around the Grand al-Nuri Mosque.</p>\n            </div>\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2 mb-2">\n                  <p class="graphic-chart-subhead text-sm-center">' + ((__t = "As of June 21, 2017") == null ? '' : __t) + '</p>\n                 <img class="hidden-md-down" src="images/west3.png" width=100%>\n                 <img class="hidden-md-up" src="images/west3.png" width=100%>\n                 <img class="hidden-lg-up hidden-sm-down" src="images/west3-md.png" width=100%>\n                 <p class="graphic-chart-subhead-capt">ISIS militants <a href="http://www.straitstimes.com/world/middle-east/historical-sites-extremists-have-destroyed-in-iraq-and-syria" target="_blank">blew up the Grand al-Nuri Mosque of Mosul and its famous leaning minaret</a>, as Iraq&rsquo;s elite Counter Terrorism Service units reached within 50m of it, according to an Iraqi military statement.</p>\n\n\n            </div>\n        </div>\n\n\n\n        <div class="row justify-content-center">\n            <div class="col-md-8 col-lg-8">\n                <h2 class="graphic-subhead mt-3">' + ((__t = 'Damage to the Old City') == null ? '' : __t) + '</h2>\n                <p class="graphic-chart-subhead-main">' + ((__t = "The offensive to retake Mosul from ISIS has damaged thousands of structures in the historic Old City and destroyed nearly 500 buildings, satellite imagery released by the United Nations showed.") == null ? '' : __t) + '</p>\n                <br>\n                <p class="graphic-chart-subhead text-sm-center">' + ((__t = "Satellite-detected damage of structures:") == null ? '' : __t) + '</p>\n                  <img class="hidden-xs-down" src="images/oldcity-legend.png" width=100%>\n                  <img class="hidden-sm-up" src="images/oldcity-legend-xs.png" width=100%>\n            </div>\n        </div>\n\n\n\n        <div class="row d-flex justify-content-center ">\n\n\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2 mb-2">\n                <p class="graphic-chart-subhead text-sm-center">' + ((__t = "As of June 11, 2017") == null ? '' : __t) + '</p>\n                <img class="hidden-md-down" src="images/oldcity1.png" width=100%>\n                <img class="hidden-md-up" src="images/oldcity1.png" width=100%>\n                <img class="hidden-lg-up hidden-sm-down" src="images/oldcity1-md.png" width=100%>\n\n            </div>\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2 mb-2">\n                  <p class="graphic-chart-subhead text-sm-center">' + ((__t = "As of June 16, 2017") == null ? '' : __t) + '</p>\n                 <img class="hidden-md-down" src="images/oldcity2.png" width=100%>\n                 <img class="hidden-md-up" src="images/oldcity2.png" width=100%>\n                 <img class="hidden-lg-up hidden-sm-down" src="images/oldcity2-md.png" width=100%>\n\n            </div>\n            <div class="col-md-4 col-sm-8 col-xs-10 mt-2 mb-2">\n                  <p class="graphic-chart-subhead text-sm-center">' + ((__t = "As of June 30, 2017") == null ? '' : __t) + '</p>\n                 <img class="hidden-md-down" src="images/oldcity3.png" width=100%>\n                 <img class="hidden-md-up" src="images/oldcity3.png" width=100%>\n                 <img class="hidden-lg-up hidden-sm-down" src="images/oldcity3-md.png" width=100%>\n\n\n            </div>\n        </div>\n        <br>\n        <Br>\n       \n</div>\n</div>\n\n            <div class=" container graphic-section-container">\n\n              <div class="row d-flex justify-content-center">\n                         <div class="col-md-8 col-lg-8">\n                             <h2 class="graphic-subhead mt-3">' + ((__t = "A shrinking caliphate") == null ? '' : __t) + '</h2>\n                             <p class="graphic-chart-subhead-main mb-1">' + ((__t = "The fall of Mosul in effect marks the end of the Iraqi half of the ISIS caliphate, although the group still controls territory west and south of the city, ruling over hundreds of thousands of people.") == null ? '' : __t) + '</p>\n\n                         </div>\n                     </div>\n\n\n\n                     <div class="row d-flex justify-content-center">\n                         <div class="col-md-8">\n                           <p class="graphic-chart-subhead text-sm-center">As of July 10, 2017</p>\n                            <img class="hidden-md-down" src="images/sanctuary.png" width=100%>\n                            <img class="hidden-lg-up hidden-xs-down" src="images/sanctuary-md.png" width=100%>\n                            <img class="hidden-sm-up" src="images/sanctuary-xs.png" width=100%>\n                         </div>\n\n                     </div>\n\n\n\n\n\n\n\n<br><br><br>              </div>\n\n</div>\n\n\n</div>\n';
    return __p;
  };
})();
(function () {
  window["Reuters"] = window["Reuters"] || {};
  window["Reuters"]["Graphics"] = window["Reuters"]["Graphics"] || {};
  window["Reuters"]["Graphics"]["Template"] = window["Reuters"]["Graphics"]["Template"] || {};

  window["Reuters"]["Graphics"]["Template"]["sample"] = function (t) {
    var __t,
        __p = '';
    __p += '<h2>This is a header from a template</h2>\n<h3>' + ((__t = 'This is a translation') == null ? '' : __t) + '</h3>\n\n ';
    return __p;
  };
})();
//for translations.
window.gettext = function (text) {
    return text;
};

window.Reuters = window.Reuters || {};
window.Reuters.Graphics = window.Reuters.Graphics || {};
window.Reuters.Graphics.Model = window.Reuters.Graphics.Model || {};
window.Reuters.Graphics.View = window.Reuters.Graphics.View || {};
window.Reuters.Graphics.Collection = window.Reuters.Graphics.Collection || {};

window.Reuters.LANGUAGE = 'en';
window.Reuters.BASE_STATIC_URL = window.reuters_base_static_url || '';

Reuters.addCommas = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

// http://stackoverflow.com/questions/8486099/how-do-i-parse-a-url-query-parameters-in-javascript
Reuters.getJsonFromUrl = function (hashBased) {
    var query = void 0;
    if (hashBased) {
        var pos = location.href.indexOf('?');
        if (pos == -1) return [];
        query = location.href.substr(pos + 1);
    } else {
        query = location.search.substr(1);
    }
    var result = {};
    query.split('&').forEach(function (part) {
        if (!part) return;
        part = part.split('+').join(' '); // replace every + with space, regexp-free version
        var eq = part.indexOf('=');
        var key = eq > -1 ? part.substr(0, eq) : part;
        var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : '';

        //convert true / false to booleans.
        if (val == 'false') {
            val = false;
        } else if (val == 'true') {
            val = true;
        }

        var f = key.indexOf('[');
        if (f == -1) {
            result[decodeURIComponent(key)] = val;
        } else {
            var to = key.indexOf(']');
            var index = decodeURIComponent(key.substring(f + 1, to));
            key = decodeURIComponent(key.substring(0, f));
            if (!result[key]) {
                result[key] = [];
            }
            if (!index) {
                result[key].push(val);
            } else {
                result[key][index] = val;
            }
        }
    });
    return result;
};

Reuters.trackEvent = function (category, type, id) {
    category = category || 'Page click';
    //console.log(category, type, id);
    var typeString = type;
    if (id) {
        typeString += ': ' + id;
    }
    var gaOpts = {
        'nonInteraction': false,
        'page': PAGE_TO_TRACK
    };

    ga('send', 'event', 'Default', category, typeString, gaOpts);
};

Reuters.generateSliders = function () {
    $('[data-slider]').each(function () {
        var $el = $(this);
        var getPropArray = function getPropArray(value) {
            if (!value) {
                return [0];
            }
            var out = [];
            var values = value.split(',');
            values.forEach(function (value) {
                out.push(parseFloat(value));
            });
            return out;
        };
        var pips = undefined;
        var start = getPropArray($el.attr('data-start'));
        var min = getPropArray($el.attr('data-min'));
        var max = getPropArray($el.attr('data-max'));
        var orientation = $el.attr('data-orientation') || 'horizontal';
        var step = $el.attr('data-step') ? parseFloat($el.attr('data-step')) : 1;
        var tooltips = $el.attr('data-tooltips') === 'true' ? true : false;
        var connect = $el.attr('data-connect') ? $el.attr('data-connect') : false;
        var snap = $el.attr('data-snap') === 'true' ? true : false;
        var pipMode = $el.attr('data-pip-mode');
        var pipValues = $el.attr('data-pip-values') ? getPropArray($el.attr('data-pip-values')) : undefined;
        var pipStepped = $el.attr('data-pip-stepped') === 'true' ? true : false;
        var pipDensity = $el.attr('data-pip-density') ? parseFloat($el.attr('data-pip-density')) : 1;
        if (pipMode === 'count') {
            pipValues = pipValues[0];
        }

        if (pipMode) {
            pips = {
                mode: pipMode,
                values: pipValues,
                stepped: pipStepped,
                density: pipDensity
            };
        }

        if (connect) {
            var cs = [];
            connect.split(',').forEach(function (c) {
                c = c === 'true' ? true : false;
                cs.push(c);
            });
            connect = cs;
        }

        noUiSlider.create(this, {
            start: start,
            range: {
                min: min,
                max: max
            },
            snap: snap,
            orientation: orientation,
            step: step,
            tooltips: tooltips,
            connect: connect,
            pips: pips
        });
        //This probably doesn't belong here, but will fix the most common use-case.
        $(this).find('div.noUi-marker-large:last').addClass('last');
        $(this).find('div.noUi-marker-large:first').addClass('first');
    });
};

Reuters.getRealImageSize = function (img, type) {
    var $img = $(img);
    var width = void 0,
        height = void 0;
    if (type === 'image') {
        if ($img.prop('naturalWidth') == undefined) {
            var $tmpImg = $('<img/>').attr('src', $img.attr('src'));
            $img.prop('naturalWidth', $tmpImg[0].width);
            $img.prop('naturalHeight', $tmpImg[0].height);
        }
        width = $img.prop('naturalWidth');
        height = $img.prop('naturalHeight');
    } else if (type === 'video') {
        width = $img.prop('videoWidth');
        height = $img.prop('videoHeight');
    }

    return { width: width, height: height };
};

Reuters.autoCropMedia = function ($container, $offsetElement, additionalOffset) {
    var autoResize = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

    var $img = $container.find('img:first');
    var type = 'image';
    if (!$img.length) {
        $img = $container.find('video:first');
        type = 'video';
    }
    var $caption = $container.find('.caption-container');
    var winH = window.innerHeight;
    var winW = window.innerWidth;
    var width = winW;
    var height = winH;
    if ($offsetElement && $offsetElement.length) {
        height = height - $offsetElement.outerHeight();
    }
    if (additionalOffset) {
        height = height - additionalOffset;
    }

    if ($caption.length) {
        height = height - $caption.outerHeight();
    }

    var targetHeight = height;
    var realSize = Reuters.getRealImageSize($img, type);
    var ratio = realSize.width / realSize.height;
    //should check again later, no?
    if (realSize.width == 0) {
        _.delay(function () {
            Reuters.autoCropMedia($container, $offsetElement, additionalOffset, autoResize);
        }, 800);
        return;
    }

    if (autoResize) {
        var resizer = function resizer() {
            Reuters.autoCropMedia($container, $offsetElement, additionalOffset, false);
        };

        $(window).on('resize', resizer);
    }

    var left = (winW - height * ratio) / 2;
    var top = 0;
    if (height < 400 || winW < 768) {
        //console.log(height, width);
        $container.css({ 'height': 'auto', 'width': '100%' });
        $img.css({ 'width': '100%', 'height': 'auto', 'margin-top': 0, 'margin-left': 0 });
        return;
    }

    $container.height(height);

    if (left > 0) {
        left = 0;
        width = winW;
        height = width / ratio;
        top = (targetHeight - height) / 2;
        //console.log('width', width, 'height', height, 'top', top, 'ratio', mastheadRatio);
    } else {
        width = height * ratio;
    }

    //console.log($img, 'width', width, 'height', height, 'top', top, 'left', left, 'ratio',ratio);

    $img.height(height);
    $img.width(width);
    $img.css({
        'margin-left': left + 'px',
        'margin-top': top + 'px',
        'opacity': 1
    });
};

Reuters.centerFullSizeMedia = function ($container, $media, mediaType) {
    var autoResize = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

    //flexbox makes this almost simple. Still need to decide which to make 100%.
    var realSize = Reuters.getRealImageSize($media, mediaType);
    //should check again later, no?
    if (realSize.width == 0) {
        _.delay(function () {
            Reuters.centerFullSizeMedia($container, $media, mediaType, autoResize);
        }, 800);
        return;
    }
    if (autoResize) {

        var resizer = function resizer() {
            Reuters.centerFullSizeMedia($container, $media, mediaType, false);
        };

        $(window).on('resize', resizer);
    }

    var $parent = $media.parent();
    $parent.removeClass('wide tall');
    var ratio = realSize.width / realSize.height;

    if ($container.width() / $container.height() < ratio) {
        $parent.addClass('wide');
    } else {
        $parent.addClass('tall');
    }
};

Reuters.popupGallery = function () {
    var _this = this;

    var resizer = function resizer() {
        var $item = $('.popup-gallery .media-item.selected');
        if (!$item.length) {
            return;
        }
        var $mediaItem = $item.find('img:first');
        var type = 'image';
        if (!$mediaItem.length) {
            $mediaItem = $item.find('video:first');
            type = 'video';
        }
        Reuters.centerFullSizeMedia($item.find('.media-container'), $mediaItem, type, false);
    };

    var checkButtons = function checkButtons() {
        $item = $('.popup-gallery .media-item.selected');
        $gallery.find('.next, .prev, .page-button').prop('disabled', false);
        if (!$item.next().length) {
            $gallery.find('.next').prop('disabled', true);
        }
        if (!$item.prev().length) {
            $gallery.find('.prev').prop('disabled', true);
        }
    };

    var showPage = function showPage(id) {
        id = id.split('/')[id.split('/').length - 1];
        console.log(id);
        $gallery.children().find('.selected').removeClass('selected');
        var $new = $gallery.find('img[src*="' + id + '"], video[src*="' + id + '"]').parents('.media-item');
        $new.addClass('selected');
        resizer();
        checkButtons($new);
    };

    var mediaHtml = '';

    var $media = $('.popup-gallery-item');
    $media.each(function (index) {
        var $item = $(this); // I wish JQuery didn't work this way.
        $item.on('click', function () {
            $('.popup-gallery').addClass('show-gallery');
            var id = $item.find('img').prop('src');
            if (!id) {
                id = $item.find('video').prop('src');
            }
            showPage(id);
        });
        //now we want to screw with the html
        $item = $item.clone();
        $item.find('.caption').append('<span class="count">' + (index + 1) + ' / ' + $media.length + '</span>');

        mediaHtml += $('<div />').append($item.clone()).html();
    });

    var galleryHtml = '\n        <div class="popup-gallery">\n            <div class="controls hidden-lg-up">\n                <div class="btn-group flex-row d-flex justify-content-end" role="group">\n                    <button type="button" class="btn btn-primary prev">\n                        <i class="fa fa-arrow-left"></i>\n                    </button>\n                    <button type="button" class="btn btn-primary next">\n                        <i class="fa fa-arrow-right"></i>\n                    </button>\n                    <button type="button" class="btn btn-primary close-button">\n                        <i class="fa fa-times"></i>\n                    </button>\n                </div>\n            </div>\n            <div class="row">\n                <div class="media-items-container">\n                    ' + mediaHtml + '\n                </div>\n                <div class="controls hidden-md-down">\n                    <div class="btn-group-vertical col-12" role="group">\n                        <button type="button" class="btn btn-primary close-button">\n                            <i class="fa fa-times"></i>\n                        </button>\n                        <button type="button" class="btn btn-primary next">\n                            <i class="fa fa-arrow-right"></i>\n                        </button>\n                        <button type="button" class="btn btn-primary prev">\n                            <i class="fa fa-arrow-left"></i>\n                        </button>\n                    </div>\n                    \n                </div>\n            </div>\n        </div>\n    ';
    var $gallery = $(galleryHtml);
    $('body').append($gallery);

    $gallery.find('.close-button').on('click', function () {
        $gallery.removeClass('show-gallery');
    });

    $gallery.find('.prev').on('click', function () {
        var $selected = $gallery.children().find('.selected');
        var $new = $selected.prev();
        if (!$new.length) {
            return;
        }
        $new.addClass('selected');
        $selected.removeClass('selected');
        resizer();
        checkButtons($new);
    });

    $gallery.find('.next').on('click', function () {
        var $selected = $gallery.children().find('.selected');
        var $new = $selected.next();
        if (!$new.length) {
            return;
        }
        $new.addClass('selected');

        $selected.removeClass('selected');
        resizer();
        checkButtons($new);
    });

    $gallery.find('.page-button').on('click', function () {
        var $page = $(_this);
        var pageId = $page.attr('data-id');
        $page.addClass('selected').siblings.removeClass('selected');
        showPage(pageId);
    });

    window.$gallery = $gallery;
    $(window).on('resize', resizer);

    $gallery.find('.media-item:first').addClass('selected');
    checkButtons();
};

Reuters.enableTooltips = function () {
    $('[data-toggle="tooltip"]').tooltip();
};

Reuters.initAds = function () {
    var $ad = $('iframe.lazy-ad');
    $ad.prop('src', $ad.attr('data-src'));
};

Reuters.initStatTracking = function () {
    //stats
    var $els = $('article[id], section[id]');
    var elements = [];
    _.each($els, function (el) {
        try {
            //should toss an error if ScrollDepth will bomb trying to track this element.
            //ad ids are not formatted correctly, apparently. Because of course.
            $(el.tagName.toLowerCase() + '#' + el.id);
            elements.push(el.tagName.toLowerCase() + '#' + el.id);
        } catch (e) {
            //pass.
        }
    });

    var scrollDepthOpts = { elements: elements };
    var rivetedOpts = { reportInterval: 20, idleTimeout: 60, nonInteraction: false };
    try {
        //why throwing undefined if checking for it?
        if (PAGE_TO_TRACK !== undefined) {
            scrollDepthOpts.page = PAGE_TO_TRACK;
            rivetedOpts.page = PAGE_TO_TRACK;
        }
    } catch (e) {}

    try {
        $.scrollDepth(scrollDepthOpts);
        riveted.init(rivetedOpts);
    } catch (e) {
        console.log('scrolldepth or rivited undefined');
    }

    $('.social.navbar-nav a, .share.share-in-article a').on('click', function () {
        var $el = $(this);
        var type = $el.attr('data-id');
        Reuters.trackEvent('Article Event', 'Share Clicked', type);
    });
};

Reuters.hasPym = false;
try {
    Reuters.pymChild = new pym.Child({ polling: 500 });
    if (Reuters.pymChild.id) {
        Reuters.hasPym = true;
        $("body").addClass("pym");
    }
} catch (err) {}

Reuters.Graphics.Parameters = Reuters.getJsonFromUrl();
if (Reuters.Graphics.Parameters.media) {
    $("html").addClass("media-flat");
}
if (Reuters.Graphics.Parameters.eikon) {
    $("html").addClass("eikon");
}
if (Reuters.Graphics.Parameters.header == "no") {
    $("html").addClass("remove-header");
}
//# sourceMappingURL=utils.js.map



$(".main").html(Reuters.Graphics.Template.featureLayout());
//# sourceMappingURL=main.js.map
