import $ from "jquery";

import * as d3 from "d3";

import "fullpage.js";

import makeVideoPlayableInline from "iphone-inline-video";

import swipe from "jquery-touchswipe";

var clinton_index, trump_index;

var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

var f_click = 0;
var a_videos = [];
$(document).ready(function() {
    if (isMobile) {
        $(window).on("touchstart click", function(e) {
            if (f_click === 0 && a_videos.length > 0) {
                f_click = 1;
                a_videos.forEach(function(v) {
                    v.play();
                    v.pause();
                });
            }
        });
    }
    $('#fullpage').fullpage({
        scrollOverflow: false,
        afterRender: function() {
            // var pluginContainer = $(this);
            // pluginContainer.find(".section").each(function() {
            //     let copy_this = $(this);
            //     let tag_content = copy_this.find(".fp-tableCell");
            //     let copy_content = tag_content.html();
            //     var id_section = copy_this.attr("id");
            //     var test = tag_content.html('<div id="${id_section}" style="background-repeat:no-repeat;background-size:cover;background-position:50% 50%;height:100%;width:100%;text-align:center">${copy_content}</div>');
            //     copy_this.removeAttr("id");
            // });

            // $('.st-deck').hide().delay(1000).fadeIn(1000);
            // $('.st-byline').hide().delay(1000).fadeIn(1000);
            // $('.scrollButtonArea').hide().delay(1000).fadeIn(2000);

            $('video').each(function() {
                makeVideoPlayableInline(this);
                a_videos.push(this);
            });
        },
        onLeave: function(index, nextIndex, direction) {

            // pass index to button-switch
            // if (index == 1) {
            //     clinton_index = 2;
            //     trump_index = 16;
            // } else if (index > 1 && index < 16) clinton_index = index;
            // else trump_index = index;

            // To control button-switch
            // if (index == 1 && direction == 'down') {
            //     setTimeout(function() {
            //         $('.button-switch').css('display', 'inline-block');
            //     }, 500);
            // }
            // if (index == 2 && direction == 'up') {
            //     $('.button-switch').css('display', 'none');
            // }

            // clinton sections
            // if (nextIndex > 1 && nextIndex < 16) {
            //     setTimeout(function() {
            //         $('.button-switch #cliton-button').css('background-color', '#0C2B57'); //stblue
            //         $('.button-switch #cliton-button').css('opacity', '1');
            //         $('.button-switch #trump-button').css('background-color', '#EF4123'); //light red
            //         $('.button-switch #trump-button').css('opacity', '0.5');
            //     }, 500);
            //
            //     $(".button-switch #cliton-button").hover(function() {
            //         $(this).css('opacity', '1');
            //     }, function() {
            //         $(this).css('opacity', '1');
            //     });
            //
            //     $(".button-switch #trump-button").hover(function() {
            //         $(this).css('opacity', '0.7');
            //     }, function() {
            //         $(this).css('opacity', '0.5');
            //     });
            // }
            //trump section
            // else if (nextIndex > 15 && nextIndex < 30) {
            //     setTimeout(function() {
            //         $('.button-switch #trump-button').css('background-color', '#EF4123'); //red
            //         $('.button-switch #trump-button').css('opacity', '1');
            //         $('.button-switch #cliton-button').css('background-color', '#0C2B57'); //stlightblue
            //         $('.button-switch #cliton-button').css('opacity', '0.5');
            //     }, 500);
            //
            //     $(".button-switch #trump-button").hover(function() {
            //         $(this).css('opacity', '1');
            //     }, function() {
            //         $(this).css('opacity', '1');
            //     });
            //
            //     $(".button-switch #cliton-button").hover(function() {
            //         $(this).css('opacity', '0.7');
            //     }, function() {
            //         $(this).css('opacity', '0.5');
            //     });
            // }


            // clinton: nextIndex 2 ~ 15 ; trump: nextIndex 16 ~ 29
            //Clinton sections
            // if (nextIndex > 2 && nextIndex < 16 && direction == 'down') {
            //     $('.bar-long').css('background-color', '#5A9DDC');
            //     $('.bar-long').css('width', (100 / 14) * (nextIndex - 1) + "%");
            // } else if (nextIndex > 1 && nextIndex < 15 && direction == 'up') {
            //     $('.bar-long').css('background-color', '#5A9DDC');
            //     $('.bar-long').css('width', (100 / 14) * (nextIndex - 2) + "%");
            // } else if (nextIndex == 15 && direction == 'up') {
            //     $('.bar-long').css('background-color', '#5A9DDC');
            //     $('.bar-long').css('width', "100%");
            // }

            // processing bar
            if (nextIndex > 1 && nextIndex < 17 && direction == 'down') {
                $('.bar-long').css('background-color', '#5A9DDC');
                $('.bar-long').css('width', (100 / 15) * (nextIndex - 0) + "%");
            } else if (nextIndex > 0 && nextIndex < 16 && direction == 'up') {
                $('.bar-long').css('background-color', '#5A9DDC');
                $('.bar-long').css('width', (100 / 15) * (nextIndex - 1) + "%");
            } else if (nextIndex == 16 && direction == 'up') {
                $('.bar-long').css('background-color', '#5A9DDC');
                $('.bar-long').css('width', "100%");
            }


            //Trump sections
            // else if (nextIndex == 16 && direction == 'down') {
            //     $('.bar-long').css('background-color', '#EF4123');
            //     $('.bar-long').css('width', '0');
            // } else if (nextIndex > 16 && nextIndex < 30 && direction == 'down') {
            //     $('.bar-long').css('background-color', '#EF4123');
            //     $('.bar-long').css('width', (100 / 14) * (nextIndex - 15) + "%");
            // } else if (nextIndex > 15 && nextIndex < 29 && direction == 'up') {
            //     $('.bar-long').css('background-color', '#EF4123');
            //     $('.bar-long').css('width', (100 / 14) * (nextIndex - 16) + "%");
            // }
        },


        afterLoad: function(anchorLink, index) {
            // disable certain section from scrolling
            // if (index === 1 || index == 15) {
            //     $.fn.fullpage.setAllowScrolling(false, 'down');
            //
            // } else {
            //     $.fn.fullpage.setAllowScrolling(true, 'down');
            // }

            // if (index == 2 || index == 16) {
            //     $.fn.fullpage.setAllowScrolling(false, 'up');
            // } else {
            //     $.fn.fullpage.setAllowScrolling(true, 'up');
            // }

            if (index == 1 || index == 3 || index == 9 || index == 11 || index == 12) {
                $(".sounddiv").css("display", "none");
            } else $(".sounddiv").css("display", "block");


            // control text fade effect
            if (index > 1) {
                $(this).find('.text-bg').stop().fadeIn(1000).animate({
                    'bottom': "0" //moves up
                });
            }

            var f_video = $(this).find('video');
            if (f_video.length > 0) {
                f_video[0].play();
            }

        }
    });
});

$(function() {
    $(".blurimg").fadeOut(5000);
    $(".content-columns").fadeIn(4000);
    $(".scrollDown").fadeIn(7000);
    $("#introPhoto").css("display", "block");
    // if (isMobile === true) $("#introCredits").html("PHOTO:<br>JEFFERY TEO");
    // else $("#introCredits").html("PHOTO: JEFFERY TEO");

    // disable scrolling for a while
    $.fn.fullpage.setAllowScrolling(false, 'down');
    setTimeout(function() {
        $.fn.fullpage.setAllowScrolling(true, 'down');
    }, 3000);

    $(".sounddiv img").click(function() {
        $(".sounddiv img").toggle();
        if ($("video").prop('muted')) {
            $("video").prop('muted', false);
        } else {
            $("video").prop('muted', true);
        }

    });
    var finalpage = $('#finalpageID');
    var tmpHeight = $(window).height() * (-1);


    finalpage.css({
        bottom: tmpHeight,
        opacity: '0',
        display: 'block'
    });

    if (isMobile === true) {
        $('#section15').swipe({
            swipeUp: function(event, direction, distance, duration, fingerCount) {
                finalpage
                    .addClass('bottom')
                    .stop().animate({
                        bottom: '0',
                        opacity: '0.9'
                    }, function() {
                        $("#text15").css("display", "none");
                    });
            }
        });
        finalpage.swipe({
            swipeDown: function(event, direction, distance, duration, fingerCount) {
                finalpage
                    .removeClass('bottom')
                    .stop().animate({
                        bottom: tmpHeight,
                        opacity: '0'
                    });
                $("#text15").css("display", "block");
            }
        });

    } else {
        // last page overlay effect
        var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel"; //FF doesn't recognize mousewheel as of FF3.x
        $('#section15').bind(mousewheelevt, function(e) {

            var evt = window.event || e; //equalize event object
            evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible
            var delta = evt.detail ? evt.detail * (-40) : evt.wheelDelta; //check for detail first, because it is used by Opera and FF

            if (delta > 0) {
                //scroll up
                finalpage
                    .removeClass('bottom')
                    .stop().animate({
                        bottom: tmpHeight,
                        opacity: '0'
                    });
                $("#text15").css("display", "block");

            } else {
                //scroll down
                finalpage
                    .addClass('bottom')
                    .stop().animate({
                        bottom: '0',
                        opacity: '0.9'
                    }, function() {
                        $("#text15").css("display", "none");
                    });
            }
        });

        $(window).scroll(function() {
            var scrolledLength = ($(window).height()) + $(window).scrollTop(),
                documentHeight = $(document).height();
            // console.log('Scroll length: ' + scrolledLength + ' Document height: ' + documentHeight);
            if (scrolledLength >= documentHeight) {
                // finalpage
                //     .addClass('bottom')
                //     .stop().animate({
                //         bottom: '0',
                //         opacity: '0.9'
                //     }, function() {
                //         $("#text15").css("display", "none");
                //     });

            } else if (scrolledLength < documentHeight && finalpage.hasClass('bottom')) {
                finalpage
                    .removeClass('bottom')
                    .stop().animate({
                        bottom: tmpHeight,
                        opacity: '0'
                    });
                $("#text15").css("display", "block");

            }
        });
    }



    $(".backtotopBtn").bind("click", function() {
        finalpage
            .removeClass('bottom')
            .stop().animate({
                bottom: tmpHeight,
                opacity: '0'
            });
        $("#text15").css("display", "block");
        $.fn.fullpage.moveTo(1);
    });
});
