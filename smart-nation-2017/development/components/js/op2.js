export default class Op2 {
  constructor() {
    console.log("op2");
  }
  getCamera() {
    var _this = this;
    // this.camera = new BABYLON.FollowCamera("FollowCamera", new BABYLON.Vector3(0, 0, 0), this.scene);
    this.camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 0, 0), this.scene);
    // this.camera.inputs.remove(this.camera.inputs.attached.keyboard);
    this.camera.inputs.clear();
    this.camera.target = null;
    this.camera.minZ = 0.01;
    // this.camera = new BABYLON.ArcRotateCamera("camera1",0,0,50, new BABYLON.Vector3(0, 0, 0), this.scene);

    // this.camera.heightOffset = 50;
    // this.camera.radius=100;
    // this.camera.rotationOffset = 180;
    this.camera.attachControl(this.engine.getRenderingCanvas(), true);
  }

  // getSkybox(){
  //   // this.Mbase.computeWorldMatrix(true);
  //   var skybox = BABYLON.Mesh.CreateSphere("skyBox", 32, 70, this.scene);
  //   skybox.infiniteDistance = true;
  //   // The sky creation
  //   // BABYLON.Engine.ShadersRepository = "shaders/";
  //   BABYLON.Effect.ShadersStore.gradientVertexShader = "precision mediump float;attribute vec3 position;attribute vec3 normal;attribute vec2 uv;uniform mat4 worldViewProjection;varying vec4 vPosition;varying vec3 vNormal;void main(){vec4 p = vec4(position,1.);vPosition = p;vNormal = normal;gl_Position = worldViewProjection * p;}";
  //   BABYLON.Effect.ShadersStore.gradientPixelShader = "precision mediump float;uniform mat4 worldView;varying vec4 vPosition;varying vec3 vNormal;uniform float offset;uniform vec3 topColor;uniform vec3 bottomColor;void main(void){float h = normalize(vPosition+offset).y;gl_FragColor = vec4(mix(bottomColor,topColor,max(pow(max(h,0.0),0.6),0.0)),1.0);}";
  //   var shader = new BABYLON.ShaderMaterial("gradient", this.scene, "gradient", {});
  //   shader.setFloat("offset", 40);
  //   shader.setFloat("exponent", 0);
  //   // shader.setColor3("topColor", BABYLON.Color3.FromInts(255,255,255));
  //   // shader.setColor3("bottomColor", BABYLON.Color3.FromInts(150,150, 150));
  //   // shader.setColor3("topColor", BABYLON.Color3.FromInts(0,119,255));
  //   // shader.setColor3("bottomColor", BABYLON.Color3.FromInts(240,240, 255));
  //   shader.setColor3("topColor", BABYLON.Color3.FromInts(255,255,255));
  //   shader.setColor3("bottomColor", BABYLON.Color3.FromInts(80,80,80));
  //   shader.backFaceCulling = false;
  //   skybox.material = shader;
  //
  // }

  getGuideModeCamera() {
    // this.cameraLookPoint.position.y = this.cameraLookPoint.parent.position.y;
    // this.cameraLookPoint.position.y += 0.05;
    this.scene.activeCamera.detachControl(this.engine.getRenderingCanvas(), true);
    this.camera = new BABYLON.ArcRotateCamera("camera2", -11, 1, 0.3, this.cameraLookPoint, this.scene);
    // this.camera.parent = this.a_drone[0];
    // this.camera.position = this.a_drone[0];
    this.camera.inputs.remove(this.camera.inputs.attached.keyboard);
    this.camera.lowerRadiusLimit = this.camera.upperRadiusLimit = this.camera.radius;
    this.camera.minZ = 0.01;
    this.camera.attachControl(this.engine.getRenderingCanvas(), true);
    this.scene.activeCamera = this.camera;
  }

  getShadow1() {
    var _this = this;
    this.shadowGenerator01 = new BABYLON.ShadowGenerator(256, this.spotLight01);
    if (_this.a_drone.length > 0) {
      this.a_drone.forEach(function(mesh) {
        _this.shadowGenerator01.getShadowMap().renderList.push(mesh);
      });
    }
    this.shadowGenerator01.useBlurVarianceShadowMap = true;
  }
  getLights() {
    var colorSpecular = BABYLON.Color3.Black();
    var colorDiffuse = new BABYLON.Color3(0.80, 0.80, 0.80);
    //this.LightDirectional = new BABYLON.DirectionalLight('Sun', new BABYLON.Vector3(-1.5, -1.5, 1.5), this.scene);
    // this.LightDirectional.diffuse = colorDiffuse;
    // this.LightDirectional.specular = colorSpecular;
    // this.LightDirectional.intensity = 0.1;
    // this.LightDirectional.position = new BABYLON.Vector3(10, 150, -10);
    // this.LightDirectional.position = new BABYLON.Vector3(0, 500, -5);

    var LightHemispheric = new BABYLON.HemisphericLight("Ambiante", new BABYLON.Vector3(1, 1, 0), this.scene);
    LightHemispheric.specular = BABYLON.Color3.Black();
    //LightHemispheric.groundColor = colorSpecular;
    LightHemispheric.intensity = 0.7;

    this.spotLight01 = new BABYLON.SpotLight("spotLight01", new BABYLON.Vector3(0, 2, 0), new BABYLON.Vector3(0, -1, 0), 0.6, 1, this.scene);
    this.spotLight01.diffuse = new BABYLON.Color3(1, 1, 224 / 255);
    this.spotLight01.specular = new BABYLON.Color3(0.1, 0.1, 0.1);
    this.spotLight01.intensity = 0.2;
  }
  onKeyDown(evt) {
    var _this = this;
    if (this.keysUp.indexOf(evt.keyCode) !== -1 || this.keysDown.indexOf(evt.keyCode) !== -1 || this.keysLeft.indexOf(evt.keyCode) !== -1 || this.keysRight.indexOf(evt.keyCode) !== -1 || this.keysGo.indexOf(evt.keyCode) !== -1 || this.keysBack.indexOf(evt.keyCode) !== -1) {
      var index = this.keys.indexOf(evt.keyCode);

      if (index === -1) {
        this.keys.push(evt.keyCode);
        this.keys.forEach(function(k) {
          if (k === _this.keysBack[0]) {
            _this.keyBackIsDown = true;
          }
        });
      }
      if (typeof evt.preventDefault === "function")
        evt.preventDefault();
      }
    }
  onKeyUp(evt) {
    if (this.keysUp.indexOf(evt.keyCode) !== -1 || this.keysDown.indexOf(evt.keyCode) !== -1 || this.keysLeft.indexOf(evt.keyCode) !== -1 || this.keysRight.indexOf(evt.keyCode) !== -1 || this.keysGo.indexOf(evt.keyCode) !== -1 || this.keysBack.indexOf(evt.keyCode) !== -1) {
      var index = this.keys.indexOf(evt.keyCode);

      if (index >= 0) {
        if (this.keys[index] === this.keysBack[0]) {
          this.keyBackIsDown = false;
        }
        this.keys.splice(index, 1);
      }
      if (typeof evt.preventDefault === "function")
        evt.preventDefault();
      }
    }
  checkInput() {
    // Keyboard
    for (var index = 0; index < this.keys.length; index++) {
      var keyCode = this.keys[index];

      if (this.keysLeft.indexOf(keyCode) !== -1) {
        this.droneRotation.addInPlace(new BABYLON.Vector3(0, this.rotateSpeed * (this.keyBackIsDown
          ? 1
          : -1), 0));
      } else if (this.keysGo.indexOf(keyCode) !== -1) {
        var x = Math.cos(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * this.speed * this.scale;
        var z = Math.sin(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * this.speed * this.scale;
        this.direction.addInPlace(new BABYLON.Vector3(-x, 0, z));
      } else if (this.keysRight.indexOf(keyCode) !== -1) {
        this.droneRotation.addInPlace(new BABYLON.Vector3(0, this.rotateSpeed * (this.keyBackIsDown
          ? -1
          : 1), 0));
      } else if (this.keysBack.indexOf(keyCode) !== -1) {
        var x = Math.cos(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * this.speed * this.scale;
        var z = Math.sin(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * this.speed * this.scale;
        this.direction.addInPlace(new BABYLON.Vector3(x, 0, -z));
      } else if (this.keysUp.indexOf(keyCode) !== -1) {
        this.direction.addInPlace(new BABYLON.Vector3(0, this.speed * this.scale, 0));
      } else if (this.keysDown.indexOf(keyCode) !== -1) {
        this.direction.addInPlace(new BABYLON.Vector3(0, -this.speed * this.scale, 0));
      }
    }

    // // Orientation
    // if (this.orientationGamma) {
    //     var z = (this.initialOrientationBeta - this.orientationBeta) * 0.05;
    //     var x = (this.initialOrientationGamma - this.orientationGamma) * -0.05;
    //     this.direction.addInPlace(new BABYLON.Vector3(0, 0, z * this.speed * this.scale));
    //     this.direction.addInPlace(new BABYLON.Vector3(x * this.speed * this.scale, 0, 0));
    // }
  }
  moveDrone() {
    var _this = this;
    if (this.a_drone.length > 0) {
      //  _this.a_drone[0].moveWithCollisions(_this.direction);
      //  _this.a_drone[0].rotation.addInPlace(_this.droneRotation);

      this.a_drone[0].position.addInPlace(_this.direction);
      this.a_drone[0].rotation.addInPlace(_this.droneRotation);
      if (typeof _this.dirIndicator !== "undefined") {
        // _this.dirIndicator.rotation.subtractInPlace(_this.droneRotation);
      }
      //  this.a_drone[0].physicsImpostor.setLinearVelocity(new BABYLON.Vector3(0,0.09,0));
      //  console.log("drone linear velocity: "+this.a_drone[0].physicsImpostor.getLinearVelocity());
      //  this.a_drone[0].updatePhysicsBody();

      // var rotationToApply = BABYLON.Quaternion.RotationYawPitchRoll(0, this.direction.z * 1.5, - this.direction.x * 1.5);
      // var rotationToApply = BABYLON.Quaternion.RotationYawPitchRoll(0, 0, this.direction.y * 0.5);
      // this.drone.rotationQuaternion = rotationToApply.multiply(this.drone.rotationQuaternion);

      // console.log(this.drone.position);

      // this.drone.rotationQuaternion.x=this.drone.rotationQuaternion.x+0.001;
      // console.log(this.drone.rotationQuaternion);

      // var rotationToApply = BABYLON.Quaternion.RotationYawPitchRoll(0, 0.001,0);
      // this.drone.rotationQuaternion = rotationToApply.multiply(this.drone.rotationQuaternion);
    }

    // if (_this.a_droneBox){
    //   _this.a_droneBox.position.addInPlace(_this.direction);
    //   _this.a_droneBox.rotation.addInPlace(_this.droneRotation);
    //
    // }

    this.direction.scaleInPlace(0.95);
    this.droneRotation.scaleInPlace(0.97);
  }

  checkAnimFrame() {
    var _this = this;
    if (_this.a_drone.length > 0 && typeof _this.a_drone[0].animations[0] != "undefined") {
      if (_this.a_drone[0].animations[0].currentFrame) {
        var anim_frame = _this.a_drone[0].animations[0].currentFrame.toFixed(2);
        //  $("#debug-test-anim").html("<p>anim frame: " + anim_frame + "</p>");
        //  console.log("narrativeSound frame: "+_this.narrativeSound.currentTime);
        if (anim_frame > this.path_frames[8]) {
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[9];
        } else if (anim_frame > this.path_frames[7]) {
          // this.narrativeSound.currentTime = 119.08;
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[8];
        } else if (anim_frame > this.path_frames[6]) {
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[7];
        } else if (anim_frame > this.path_frames[5]) {
          // this.narrativeSound.currentTime = 86.17;
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[6];
        } else if (anim_frame > this.path_frames[4]) {
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[5];
        } else if (anim_frame > this.path_frames[3]) {
          // this.narrativeSound.currentTime = 61.15;
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[4];
        } else if (anim_frame > this.path_frames[2]) {
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[3];
        } else if (anim_frame > this.path_frames[1]) {
          // this.narrativeSound.currentTime = 34.684;
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[2];
        } else if (anim_frame > this.path_frames[0]) {
          this.anim_rotatable = true;
          this.anim_target_location = this.anim_travel_location[1];
        } else if (anim_frame > 360) {
          $("#guidetour").animateCss("fadeOut", "out");
        }

      }
    }
    // if (_this.a_drone[0]){
    //   var dronePos = _this.a_drone[0].position;
    //  $("#debug-test-anim").html("<p>drone cur loc: "+dronePos+"</p>");
    // }

  }

  updateDroneAnimRotation() {
    if (this.anim_rotatable) {
      var _this = this;
      if (_this.a_drone.length > 0 && _this.is_guideMode === 1 && _this.anim_target_location) {
        // _this.camera.setTarget(_this.a_drone[0].position);
        var lookAtTgt = _this.a_drone[0].position.subtract(_this.anim_target_location);
        var tgtRotation = -Math.atan2(lookAtTgt.z, lookAtTgt.x) - Math.PI / 2;
        _this.a_drone[0].rotation = BABYLON.Vector3.Lerp(_this.a_drone[0].rotation, new BABYLON.Vector3(_this.a_drone[0].rotation.x, tgtRotation, _this.a_drone[0].rotation.z), 0.1);
      }
    }
  }

  // updateCameraRotateTowardDrone() {
  //   if (this.is_guideMode === 1 && this.cameraLookPoint) {
  //     var lookAtTgt = this.camera.position.subtract(this.cameraLookPoint);
  //     var tgtRotation = -Math.atan2(lookAtTgt.z, lookAtTgt.x) - Math.PI / 2;
  //     this.camera.rotation = BABYLON.Vector3.Lerp(
  //       this.camera.rotation,
  //       new BABYLON.Vector3(this.camera.rotation.x, tgtRotation, this.camera.rotation.z),
  //       0.1);
  //   }
  //
  // }

  updateCamera1() {
    if (this.a_drone.length > 0) {
      if (this.is_guideMode === 0) {

        // var d = 0.65;
        var d = 0.25;
        var h = 0.25;
        var x = Math.cos(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * d;
        var z = Math.sin(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * d;

        this.camera.position = BABYLON.Vector3.Lerp(this.camera.position, new BABYLON.Vector3(this.a_drone[0].position.x + x, this.a_drone[0].position.y + h, this.a_drone[0].position.z - z), 0.4);
        // this.camera.position.y = this.a_drone[0].position.y + 0.4;
        // this.camera.position.z = this.a_drone[0].position.z - z;
        // this.camera.position.x = this.a_drone[0].position.x + x;
        this.camera.lockedTarget = this.cameraLookPoint;
      } else {
        this.camera.lockedTarget = null;
      }
      this.updateLight();
    }
  }
  updateLight() {
    var spotD = 1;
    var spotX = Math.cos(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * spotD;
    var spotZ = Math.sin(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * spotD;
    this.spotLight01.position.y = this.a_drone[0].position.y + 0.8;
    this.spotLight01.position.z = this.a_drone[0].position.z - spotZ;
    this.spotLight01.position.x = this.a_drone[0].position.x + spotX;
    this.spotLight01.setDirectionToTarget(this.a_drone[0].position);
  }
  updateRotationByMouseDrag(evt) {
    if (this.is_guideMode === 0) {
      if (evt.pageX < this.oldX) {
        this.droneRotation.addInPlace(new BABYLON.Vector3(0, this.rotateSpeed * (this.keyBackIsDown
          ? 1
          : -1), 0));
      } else if (evt.pageX > this.oldX) {
        this.droneRotation.addInPlace(new BABYLON.Vector3(0, this.rotateSpeed * (this.keyBackIsDown
          ? -1
          : 1), 0));
      }
      this.oldX = evt.pageX;
      this.oldY = evt.pageY;
    }
  }
  // valLerp(start, end, amount) {
  //   return (start + (end - start) * amount);
  // }
  enter_drone(evt, marker) {
    this.isDragging = false;
    this.clear_keyboard(evt);

    this.anim_rotatable = false;
    if (this.is_vr === 0) {
      this.showModal(evt, marker);
    } else {
      this.showModal_vr(evt, marker);
    }
  }
  // exit_drone(evt){
  //   console.log("exit");
  // }

  // updateGuideTarget(){
  //   this.anim_rotatable = true;
  //   this.anim_location_counter++;
  //   this.anim_target_location = this.anim_travel_location[this.anim_location_counter];
  // }

  registerEnterExitAction(marker) {
    var _this = this;
    if (marker !== null) {
      marker.actionManager.registerAction(new BABYLON.ExecuteCodeAction({
        trigger: BABYLON.ActionManager.OnIntersectionEnterTrigger,
        parameter: {
          mesh: this.a_drone[0],
          usePreciseIntersection: true
        }
      }, function(evt) {
        _this.enter_drone(evt, marker);
      }));

      // marker.actionManager.registerAction(new BABYLON.ExecuteCodeAction({
      //   trigger: BABYLON.ActionManager.OnIntersectionExitTrigger,
      //   parameter: {
      //     mesh: this.a_drone[0],
      //     usePreciseIntersection: true
      //   }
      // }, function(evt) {
      //    _this.exit_drone(evt);
      // }));
    }
  }

  //   hideModal(evt) {
  //     var _this = this;
  //     if (evt) {
  //       if (evt.source) {
  //         var mesh_clicked = evt.source;
  //         var type_modal = "quiz";
  //         if (/-info/.test(mesh_clicked.name)) {
  //           type_modal = "info";
  //         }
  //         var name_image = mesh_clicked.name.split("marker_")[1].split("-")[0];
  //         if (type_modal == "info") {
  //           var infocard = $("#" + type_modal + "_" + name_image);
  //           if (this.is_guideMode === 1) {
  //             infocard.fadeOut();
  //           }
  //       }else if (type_modal=="quiz"){
  //         this.a_drone[0].isPickable = false;
  //         setTimeout(function(){
  //           _this.a_drone[0].isPickable = true;
  //         },1000);
  //       }
  //
  //       if (this.is_guideMode === 1) {
  //         //remove actions in guide mode, ensure drone and icon only intersect once
  //         mesh_clicked.actionManager.actions.pop();
  //         console.log("exit action removed");
  //       }
  //
  //     }
  //   }
  // }

  pauseAnimationAndVoiceInactive(canPause) {
    var _this = this;
    if (canPause) {
      if (typeof _this.a_animatable !== "undefined" && _this.animatable !== null) {
        _this.a_animatable.pause();
        if (_this.isInScene && _this.is_guideMode === 1) {
          _this.narrativeSound.pause();
        }
      }

    } else {
      if (typeof _this.a_animatable !== "undefined" && _this.animatable !== null) {
        _this.a_animatable.restart();
        if (_this.isInScene && _this.is_guideMode === 1) {
          _this.narrativeSound.play();
        }
      }

    }

  }

  pauseAnimationAndVoice(canPause) {
    var _this = this;
    if (canPause) {
      if (typeof _this.narrativeSound.instance !== "undefined" && !_this.narrativeSound.ended && _this.isInScene && _this.is_guideMode === 1) {
        $(game.narrativeSound).animate({
          volume: 0
        }, 250, function() {
          _this.narrativeSound.pause();
          if (typeof _this.a_animatable !== "undefined" && _this.animatable !== null) {
            _this.a_animatable.pause();
          }
        });

        // $("#sound-narrative").animate({volume: 0}, 250,function(){
        //   _this.narrativeSound.pause();
        //   if (typeof _this.a_animatable !== "undefined" && _this.animatable !== null) {
        //     _this.a_animatable.pause();
        //   }
        // });
      }
    } else {
      if (typeof _this.a_animatable !== "undefined" && _this.animatable !== null) {
        _this.a_animatable.restart();
      }
      if (typeof _this.narrativeSound.instance !== "undefined" && !_this.narrativeSound.ended && _this.isInScene && _this.is_guideMode === 1) {
        _this.narrativeSound.play();
        // if (!_this.isMuted){
        $(game.narrativeSound).animate({
          volume: _this.guideBgmVolume
        }, 250);
        // $("#sound-narrative").animate({volume: _this.guideBgmVolume}, 250);
        // }
      }
    }
  }

  pauseBGM(canPause) {
    if (canPause) {
      if (typeof this.bgm.instance !== "undefined" && this.isInScene) {
        this.bgm.pause();
      }
    } else {
      if (typeof this.bgm.instance !== "undefined" && this.isInScene) {
        this.bgm.play();
      }
    }
  }

  animate_end_drone_path() {
    var _this = this;
    this.a_drone[0].animation = [];
    var endAnimationDrone = new BABYLON.Animation("end loop animation", "position", 20, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var guide_end_keys = [];
    guide_end_keys.push({
      frame: 0,
      value: new BABYLON.Vector3(this.a_path_info[1].x, this.a_path_info[1].y, this.a_path_info[1].z)
    });
    _this.a_path_drone.forEach(function(path, i) {
      guide_end_keys.push({
        frame: _this.path_frames[i * 2],
        value: new BABYLON.Vector3(path.x, path.y, path.z)
      });
      guide_end_keys.push({
        frame: _this.path_frames[i * 2 + 1],
        value: new BABYLON.Vector3(_this.a_path_info[i + 1].x, _this.a_path_info[i + 1].y, _this.a_path_info[i + 1].z)
      });

    });
    guide_end_keys.push({
      frame: _this.path_frames[_this.path_frames.length - 1],
      value: new BABYLON.Vector3(this.a_path_drone[1].x, this.a_path_drone[1].y, this.a_path_drone[1].z)
    });
    endAnimationDrone.setKeys(guide_end_keys);
    this.a_drone[0].animations.push(endAnimationDrone);

    this.scene.beginAnimation(this.a_drone[0], 0, 3200, true, 1);
  }

  getDirArrows() {
    // var _this = this;
    // _this.sscanvas = new BABYLON.ScreenSpaceCanvas2D(_this.scene, {
    //   id: "ScreenCanvas"
    //   // ,backgroundFill: "#4040408F",backgroundRoundRadius: 50
    // });
    //
    // var lineArr = [new BABYLON.Vector3(-0.1,0,0),new BABYLON.Vector2(0,0,0.2),new BABYLON.Vector2(0.1,0,0),new BABYLON.Vector2(-0.1,0,0)];
    // _this.a_quiz_marker.forEach(function(marker){
    //   var type = marker.name.split("marker_")[1];
    //
    //   var info_arrow2D = new BABYLON.Lines2D(lineArr,{
    //     parent: _this.sscanvas,
    //     id: "arrow_"+type,
    //     x: 0, y: 0,
    //     fill: "#000000ff",
    //     fillThickness:2,
    //     children:[
    //       new BABYLON.Text2D(type,{color:"#00000000"})
    //     ]
    //   });
    //   _this.quiz_arrows[type] = info_arrow2D;
    // });

    this.dirIndicator = BABYLON.Mesh.CreateGround("arrow", 0.5, 0.5, 2, this.scene);
    this.dirIndicator.scaling.x = 0.05;
    this.dirIndicator.scaling.z = 0.05;
    // this.dirIndicator.convertToFlatShadedMesh();
    var dirIndMaterial = new BABYLON.StandardMaterial("arrow material", this.scene);
    dirIndMaterial.useEmissiveAsIllumination = true;
    dirIndMaterial.opacityTexture = new BABYLON.Texture("images/smart-nation/arrow.png", this.scene);
    this.dirIndicator.material = dirIndMaterial;

  }
  updateArrowColor(type) {
    var tempColor = new BABYLON.Color3(15 / 255, 126 / 255, 117 / 255);
    switch (type) {
      case "marker_environment":
        tempColor = new BABYLON.Color3(15 / 255, 126 / 255, 117 / 255);
        break;
      case "marker_security":
        tempColor = new BABYLON.Color3(77 / 255, 129 / 255, 174 / 255);
        break;
      case "marker_transport":
        tempColor = new BABYLON.Color3(216 / 255, 181 / 255, 8 / 255);
        break;
      case "marker_virtual":
        tempColor = new BABYLON.Color3(121 / 255, 25 / 255, 91 / 255);
        break;
      case "marker_health":
        tempColor = new BABYLON.Color3(187 / 255, 56 / 255, 30 / 255);
        break;
    }

    return tempColor;
  }
  updateArrows() {
    var _this = this;
    //  if (typeof this.a_quiz_marker !== "undefined" && typeof this.quiz_arrows !=="undefined"){
    //    this.a_quiz_marker.forEach(function(marker){
    //      var marker_name = marker.name.split("marker_")[1];
    //      if (!_this.scene.isActiveMesh(marker) && typeof _this.quiz_arrows[marker_name] !=="undefined"){
    //        _this.quiz_arrows[marker_name].levelVisible = true;
    //        _this.updateQuizArrows(marker,_this.quiz_arrows[marker_name]);
    //      }else{
    //      _this.quiz_arrows[marker_name].levelVisible = false;
    //    }
    //  });
    //  }
    if (typeof this.dirIndicator !== "undefined") {
      this.dirIndicator.position = this.a_drone[0].position.add(new BABYLON.Vector3(0, 0.06, 0));

      if (typeof this.a_quiz_marker !== "undefined") {
        for (var i = 0; i < this.a_quiz_marker.length; i++) {
          if (this.scene.isActiveMesh(this.a_quiz_marker[i])) {
            if (this.a_quiz_marker[i].name.split("_")[0] !== "d"){
              this.markersOutOfView = false;
              break;
            }
          } else if (i === 4) {
            this.markersOutOfView = true;
          }
        }

        if (this.markersOutOfView && typeof this.a_drone[0] !== "undefined") {
          // this.closestDist = BABYLON.Vector3.Distance(this.a_drone[0].position, this.a_quiz_marker[0].position);
          this.closestDist = 100;
          this.closestMarker = this.a_quiz_marker[0];
          this.a_quiz_marker.forEach(function(marker) {
            var tempDist = BABYLON.Vector3.Distance(_this.a_drone[0].position, marker.position);
            if (_this.closestDist >= tempDist && marker.name.split("_")[0] !=="d") {
              _this.closestMarker = marker;
              _this.closestDist = tempDist;
            }
          });
          if (/*_this.closestDist < 5 && */
          _this.closestMarker.name.split("_")[0] !== "d") {
            var color = _this.updateArrowColor(_this.closestMarker.name);
            _this.dirIndicator.material.diffuseColor = color;
            _this.dirIndicator.material.emissiveColor = color;
            var lookAtTgt = _this.a_drone[0].position.subtract(_this.closestMarker.position);
            var tgtRotation = -Math.atan2(lookAtTgt.z, lookAtTgt.x) - Math.PI / 2;
            _this.dirIndicator.rotation = BABYLON.Vector3.Lerp(_this.dirIndicator.rotation, new BABYLON.Vector3(_this.dirIndicator.rotation.x, tgtRotation, _this.dirIndicator.rotation.z), 0.5);
          } else {
            _this.dirIndicator.material.diffuseColor = BABYLON.Color3.Gray();
            _this.dirIndicator.material.emissiveColor = BABYLON.Color3.Gray();
            _this.dirIndicator.rotation.y += Math.PI / 14;
          }
        } else {
          _this.dirIndicator.material.diffuseColor = BABYLON.Color3.Gray();
          _this.dirIndicator.material.emissiveColor = BABYLON.Color3.Gray();
          _this.dirIndicator.rotation.y += Math.PI / 14;
        }

        // _this.dirIndicator.rotation.subtractInPlace(_this.dirIndicator.parent.rotation);

        // _this.dirIndicator.rotation.subtractInPlace(_this.droneRotation);

        // _this.dirIndicator.lookAt(_this.closestMarker.position);

        // console.log("closest dist: "+_this.closestDist);
        // console.log("closest: "+_this.closestMarker.name);
        // if (_this.closestDist < 10){
        //   var lookAtTgt = _this.a_drone[0].position.subtract(_this.closestMarker.position);
        //   var tgtRotation = -Math.atan2(lookAtTgt.z, lookAtTgt.x) - Math.PI / 2;
        //   _this.dirIndicator.rotation = BABYLON.Vector3.Lerp(
        //     _this.dirIndicator.rotation,
        //     new BABYLON.Vector3(_this.dirIndicator.rotation.x,tgtRotation,_this.dirIndicator.rotation.z),
        //     0.1);
        // }else{
        // _this.closestMarker = null;
        // _this.closestDist = 10;
        // }
      }
    }
  }

  updateQuizArrows(marker, arrow2D) {
    var maxW = this.engine.getRenderWidth();
    var maxH = this.engine.getRenderHeight();
    var offset = 10;
    var coordinates = new BABYLON.Vector3.Project(marker.getBoundingInfo().boundingBox.center, BABYLON.Matrix.Identity(), this.scene.getTransformMatrix(), this.camera.viewport.toGlobal(maxW, maxH));

    arrow2D.levelVisible = true;
    // console.log("a_quiz_marker[0]: "+this.a_quiz_marker[0].name);
    // console.log("is marker behind?: "+this.isBehind(this.a_quiz_marker[0]));
    // arrow2D.position = new BABYLON.Vector2(coordinates.x,maxH - coordinates.y);
    //
    // if (coordinates.x < 0){
    //   arrow2D.rotation = Math.PI/2;
    //   arrow2D.position.x = offset;
    // }else if (coordinates.x > maxW - arrow2D.width){
    //   arrow2D.rotation = -Math.PI/2;
    //   arrow2D.position.x = maxW - arrow2D.width - offset;
    // }
    //
    // if (coordinates.y < 0){
    //   arrow2D.rotation = 0;
    //   arrow2D.position.y = maxH - arrow2D.height - offset;
    // }else if (coordinates.y > maxH - arrow2D.height){
    //   arrow2D.rotation = Math.PI;
    //   arrow2D.position.y = offset;
    // }
    //
    // if(coordinates.y <0 && coordinates.x <0){ //top left
    //   arrow2D.rotation = Math.PI/4;
    // }else if(coordinates.y <0 && coordinates.x >maxW){ //top right
    //   arrow2D.rotation = -Math.PI/4;
    // }else if(coordinates.y > maxH && coordinates.x <0){ //bottom left
    //   arrow2D.rotation = Math.PI-Math.PI/4;
    // }else if(coordinates.y >maxH && coordinates.x>maxW){ //bottom right
    //   arrow2D.rotation = Math.PI+Math.PI/4;
    // }
    if (coordinates.x < 0 && coordinates.y > 0 && coordinates.y < maxH) { //left
      arrow2D.rotation = Math.PI / 2;
      arrow2D.position = new BABYLON.Vector2(offset, maxH - coordinates.y);
    } else if (coordinates.x > maxW && coordinates.y > 0 && coordinates.y < maxH) { //right
      arrow2D.rotation = -Math.PI / 2;
      arrow2D.position = new BABYLON.Vector2(maxW - arrow2D.width - offset, maxH - coordinates.y);
    } else if (coordinates.y < 0 && coordinates.x > 0 && coordinates.x < maxW) { //top
      arrow2D.rotation = 0;
      arrow2D.position = new BABYLON.Vector2(coordinates.x, maxH - arrow2D.height - offset);
    } else if (coordinates.y > maxH && coordinates.x > 0 && coordinates.x < maxW) { //bottom
      arrow2D.rotation = Math.PI;
      arrow2D.position = new BABYLON.Vector2(coordinates.x, offset);
    } else if (coordinates.y < 0 && coordinates.x < 0) { //top left
      arrow2D.rotation = Math.PI / 4;
      arrow2D.position = new BABYLON.Vector2(offset, maxH - arrow2D.height - offset);
    } else if (coordinates.y < 0 && coordinates.x > maxW) { //top right
      arrow2D.rotation = -Math.PI / 4;
      arrow2D.position = new BABYLON.Vector2(maxW - arrow2D.width - offset, maxH - arrow2D.height - offset);
    } else if (coordinates.y > maxH && coordinates.x < 0) { //bottom left
      arrow2D.rotation = Math.PI - Math.PI / 4;
      arrow2D.position = new BABYLON.Vector2(offset, offset);
    } else if (coordinates.y > maxH && coordinates.x > maxW) { //bottom right
      arrow2D.rotation = Math.PI + Math.PI / 4;
      arrow2D.position = new BABYLON.Vector2(maxW - arrow2D.width - offset, offset);
    }
  }

  // isBehind(marker){
  //   var x = Math.cos(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * 2;
  //   var z = Math.sin(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * 2;
  //   var forwardPos = new BABYLON.Vector3(x,this.a_drone[0].position.y,z);
  //   var forward = forwardPos.subtract(this.a_drone[0].position);
  //   forward.normalize();
  //   // var forward = BABYLON.Vector3.TransformCoordinates(new BABYLON.Vector3(0,0,1), this.camera.getWorldMatrix());
  //   var toMarker = marker.position.subtract(this.a_drone[0].position);
  //   toMarker.normalize();
  //   return (BABYLON.Vector3.Dot(toMarker,forward) <= 0);
  // }

  getVRAnsPlanes() {
    var indexLetter = ['A', 'B', 'C'];
    for (var i = 0; i < 3; i++) {
      this.ansPlanes[i] = new BABYLON.Mesh.CreatePlane("environment-ansPlane-" + (i + 1), 0.5, this.scene);
      this.ansPlanes[i].visibility = 0;
      this.ansPlanes[i].scaling.y = 0.788;
      var ansMaterial = new BABYLON.StandardMaterial("ansPlane-material-" + indexLetter[i], this.scene);
      ansMaterial.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/environment-opt-" + indexLetter[i] + ".png", this.scene);
      ansMaterial.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/environment-opt-" + indexLetter[i] + ".png", this.scene);
      this.ansPlanes[i].material = ansMaterial;

      this.create_button_VrQ(indexLetter, i);
    }
  }

  getVRQuesPlane() {
    this.quesPlane = new BABYLON.Mesh.CreatePlane("environment-quesPlane", 1.8, this.scene);
    this.quesPlane.visibility = 0;
    this.quesPlane.scaling.y = 0.15;

    var quesMaterial = new BABYLON.StandardMaterial("quesPlane-material", this.scene);
    quesMaterial.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/environment-question.png", this.scene);
    quesMaterial.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/environment-question.png", this.scene);
    this.quesPlane.material = quesMaterial;
  }
  getVRInfoPlane() {
    this.infoPlane = new BABYLON.Mesh.CreatePlane("environment-infoPlane", 0.5, this.scene);
    this.infoPlane.visibility = 0;
    this.infoPlane.parent = this.scene.activeCamera;
    this.infoPlane.position.z += 0.26;
    this.infoPlane.scaling.y = 0.2615;
    this.infoPlane.convertToFlatShadedMesh();
    this.infoPlane.renderingGroupId = 1;
    var infoMaterial = new BABYLON.StandardMaterial("infoPlane-material", this.scene);

    infoMaterial.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/environment-hint.png", this.scene);
    infoMaterial.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/environment-hint.png", this.scene);
    this.infoPlane.material = infoMaterial;
  }

  showVRQuizPlanes(type) {
    var indexLetter = ['A', 'B', 'C'];
    var d = 0.8;
    var x = Math.cos(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * d;
    var z = Math.sin(this.a_drone[0].rotation.y + BABYLON.Tools.ToRadians(90)) * d;

    // var cameraRot = this.a_drone[0].rotation; //freecam
    // var cameraRot = this.a_drone[0].rotationQuaternion; //device orien cam
    // var rotOffset = BABYLON.Tools.ToRadians(50); // for freecamera
    var cameraRot = this.scene.activeCamera.alpha;
    var rotOffset = BABYLON.Tools.ToRadians(140);
    var index = 0;
    var fov = 80;
    for (var i = fov; i >= 0; i -= fov / 2) {
      var r = BABYLON.Tools.ToRadians(i) + cameraRot + rotOffset;
      this.ansPlanes[index].position.y = this.scene.activeCamera.position.y - 0.1;
      this.ansPlanes[index].position.x = this.scene.activeCamera.position.x + Math.cos(r) * d;
      this.ansPlanes[index].position.z = this.scene.activeCamera.position.z + Math.sin(r) * d;
      this.ansPlanes[index].lookAt(this.scene.activeCamera.position);
      this.ansPlanes[index].name = type + "-ansPlane-" + (index + 1);
      this.ansPlanes[index].material.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/" + type + "-opt-" + indexLetter[index] + ".png", this.scene);
      this.ansPlanes[index].material.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/" + type + "-opt-" + indexLetter[index] + ".png", this.scene);
      this.ansPlanes[index].visibility = 1;

      if (index === 1) {
        this.quesPlane.position.y = this.scene.activeCamera.position.y - 0.1;
        this.quesPlane.position.x = this.scene.activeCamera.position.x + Math.cos(r) * (d + 0.2);
        this.quesPlane.position.z = this.scene.activeCamera.position.z + Math.sin(r) * (d + 0.2);
        this.quesPlane.position = this.ansPlanes[index].position.add(new BABYLON.Vector3(0, 0.4, 0));
        this.quesPlane.lookAt(this.scene.activeCamera.position);
        this.quesPlane.name = type + "-quesPlane";
        this.quesPlane.material.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/" + type + "-question.png", this.scene);
        this.quesPlane.material.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/" + type + "-question.png", this.scene);
        this.quesPlane.visibility = 1;
      }
      this.apply_texture_button_vr(type, index);
      index++;
    }
  }

  showVRInfoPlane(type) {
    this.infoPlane.material.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/" + type + "-hint.png", this.scene);
    this.infoPlane.material.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/" + type + "-hint.png", this.scene);
    BABYLON.Animation.CreateAndStartAnimation("showInfo", this.infoPlane, "visibility", 24, 30, 0, 0.8, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
  }

  hideVRInfoPlane() {
    BABYLON.Animation.CreateAndStartAnimation("showInfo", this.infoPlane, "visibility", 24, 30, 0.8, 0, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
  }

  getResultPlane() {
    // this.resultPlane[0] = new BABYLON.Mesh.CreatePlane("resultPlane",0.6,this.scene);
    // this.resultPlane[0].parent = this.scene.activeCamera;
    // this.resultPlane[0].position.z +=0.5;
    // this.resultPlane[0].position.x +=0.1;
    // this.resultPlane[0].scaling.y =0.422;
    // this.resultPlane[0].visibility = 0;
    // // this.resultPlane[0].scaling =0.6;
    //
    // this.resultPlane[1] = new BABYLON.Mesh.CreatePlane("resultPlaneNum",0.4,this.scene);
    // this.resultPlane[1].scaling.y =1.164;
    // this.resultPlane[1].parent = this.scene.activeCamera;
    // this.resultPlane[1].position.z +=0.5;
    // this.resultPlane[1].position.x -=0.3;
    // this.resultPlane[1].visibility = 0;
    // var resultMat = new BABYLON.StandardMaterial("resultMaterial",this.scene);
    // resultMat.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/of-5.png",this.scene);
    // resultMat.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/of-5.png",this.scene);
    // resultMat.opacityTexture = new BABYLON.Texture("images/smart-nation/VR/of-5.png",this.scene);
    // this.resultPlane[0].material = resultMat;
    // var resultNumMat = new BABYLON.StandardMaterial("resultNumMaterial",this.scene);
    // resultNumMat.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/number-5.png",this.scene);
    // resultNumMat.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/number-5.png",this.scene);
    // resultNumMat.opacityTexture = new BABYLON.Texture("images/smart-nation/VR/number-5.png",this.scene);
    // this.resultPlane[1].material = resultNumMat;

    this.resultPlane = new BABYLON.Mesh.CreatePlane("resultPlane", 0.6, this.scene);
    this.resultPlane.parent = this.scene.activeCamera;
    this.resultPlane.position.z += 0.4;
    this.resultPlane.scaling.y = 0.5425;
    this.resultPlane.renderingGroupId = 1;
    this.resultPlane.visibility = 0;
    var resultPlaneMat = new BABYLON.StandardMaterial("resultPlaneMat", this.scene);
    resultPlaneMat.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/number-1-of-5-white.png", this.scene);
    resultPlaneMat.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/number-1-of-5-white.png", this.scene);
    this.resultPlane.material = resultPlaneMat;
  }

  // showresultpageVR(count){
  //   this.resultPlane[1].material.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/number-"+count+".png",this.scene);
  //   this.resultPlane[1].material.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/number-"+count+".png",this.scene);
  //   this.resultPlane[1].material.opacityTexture = new BABYLON.Texture("images/smart-nation/VR/number-"+count+".png",this.scene);
  //
  //   this.resultPlane[0].visibility = 1;
  //   this.resultPlane[1].visibility = 1;
  // }
  showresultpageVR(count) {
    this.resultPlane.material.diffuseTexture = new BABYLON.Texture("images/smart-nation/VR/number-" + count + "-of-5-white.png", this.scene);
    this.resultPlane.material.emissiveTexture = new BABYLON.Texture("images/smart-nation/VR/number-" + count + "-of-5-white.png", this.scene);
    this.resultPlane.material.opacityTexture = new BABYLON.Texture("images/smart-nation/VR/number-" + count + "-of-5-white.png", this.scene);
    // this.resultPlane.visibility = 1;
    BABYLON.Animation.CreateAndStartAnimation("showResult", this.resultPlane, "visibility", 24, 30, 0, 1, BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
  }

  checkSoundStartPlay() {
    var _this = this;
    if (!_this.startAnim && typeof _this.narrativeSound.instance !=="undefined"){
      if (game.narrativeSound.currentTime > 0 && !game.narrativeSound.ended) {
        _this.animate_drone_path();
        _this.a_animatable = _this.scene.beginAnimation(_this.a_drone[0], 0, 3200, false, 1, function() {
          // _this.narrativeSound = null;
          _this.animate_end_drone_path();
        });
        _this.startAnim = true;
      }
    }

    // else{
    //   console.log("narrativeSound is paused?:"+$("#sound-narrative").get(0).paused);
    //   console.log("narr sound currentTime: "+$("#sound-narrative").get(0).currentTime);
    //   console.log("narr sound ended?: "+$("#sound-narrative").get(0).ended);
    // }
  }
  //
  //  assign_height_box() {
  //   $(".textarea .text").css("height", "auto");
  //   var height_box_text = 0;
  //
  //   $(".textarea .text").each(function() {
  //     if ($(this).height() > height_box_text) height_box_text = $(this).height();
  //   });
  //   $(".textarea .text").height(height_box_text + 40);
  // }
  //
  //  assign_height_main() {
  //   $(".main-headline").css("height", "auto");
  //   $(".content-main-body").removeClass("center-middle-vertical");
  //   if (window.innerHeight > $(".content-main-body").outerHeight()) {
  //     $(".main-headline").height(window.innerHeight);
  //     // $(".content-main-body").addClass("center-middle-vertical");
  //   }
  // }

}
