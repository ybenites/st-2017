// console.log(path.join(, 'mixins'));
var initial_path="../../../../";
module.exports = {
    plugins: [
      require("postcss-strip-inline-comments"),
      require('postcss-smart-import'),
      require("postcss-sassy-mixins"),
      require("postcss-media-minmax"),
      require('precss'),
      require('autoprefixer'),
      require("postcss-utilities"),
      require("lost"),
      require("postcss-font-magician")({
        custom:{
          "Selane MinTen":{
            variants:{
              normal:{
                900:{
                  url:{
                    eot:initial_path+"fonts/SelaneWebSTMinTen.eot",
                    ttf:initial_path+"fonts/SelaneWebSTMinTen.ttf",
                    woff:initial_path+"fonts/SelaneWebSTMinTen.woff"
                  }
                }
              }
            }
          },
          "Selane Thirty":{
            variants:{
              normal:{
                300:{
                  url:{
                    eot:initial_path+"fonts/SelaneWebSTThirty.eot",
                    ttf:initial_path+"fonts/SelaneWebSTThirty.ttf",
                    woff:initial_path+"fonts/SelaneWebSTThirty.woff"
                  }
                }
              }
            }
          },
          "Selane Ten":{
            variants:{
              normal:{
                900:{
                  url:{
                    eot:initial_path+"fonts/selanedeckst_ten-webfont.eot",
                    ttf:initial_path+"fonts/selanedeckst_ten-webfont.ttf",
                    woff:initial_path+"fonts/selanedeckst_ten-webfont.woff"
                  }
                }
              }
            }
          },
          "Curator Regular":{
            variants:{
              normal:{
                100:{
                  url:{
                    eot:initial_path+"fonts/curator_head_st_regular-webfont.eot",
                    ttf:initial_path+"fonts/curator_head_st_regular-webfont.ttf",
                    woff:initial_path+"fonts/curator_head_st_regular-webfont.woff"
                  }
                }
              }
            }
          },
          "Curator Bold":{
            variants:{
              normal:{
                900:{
                  url:{
                    eot:initial_path+"fonts/curator_head_st_bold-webfont.eot",
                    ttf:initial_path+"fonts/curator_head_st_bold-webfont.ttf",
                    woff2:initial_path+"fonts/curator_head_st_bold-webfont.woff2",
                    woff:initial_path+"fonts/curator_head_st_bold-webfont.woff"
                  }
                }
              }
            }
          }
        }
      })
    ]
}
