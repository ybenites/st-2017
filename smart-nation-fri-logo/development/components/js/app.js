import * as handjs from "handjs";

// import "./libraries/babylon.cloudProceduralTexture";
// import "./libraries/babylon.skyMaterial";
// import "./libraries/babylon.2.5.canvas2d";
import Render from "./renderer";

import nanoScroller from "nanoscroller";
// import hand_sliding_effect from "./libraries/hand-sliding";


var modes = [{
  name:"guide",
  pressed:0
},{
  name:"guideVR",
  pressed:0
},{
  name:"fly",
  pressed:0
}];
var currentMode = modes[0]; //guideMode, guideVRMode, playMode

var width_hand, svg_hand, g_hand_mov;

var mainDiv;
var margin_hand = {
  right: 10,
  left: 10
};

var hand_sliding_effect = function(div_id) {
  mainDiv = div_id;
  width_hand = ($(mainDiv).width() - margin_hand.left - margin_hand.right) * 0.6;
  svg_hand = d3.select(mainDiv).append("svg")
    .attr("class","hand-slide")
    .attr("width", width_hand + margin_hand.left + margin_hand.right)
    .attr("height", 50)
    .attr("fill", "#707070");

// var height =$(".endingpage").offset().top;
  g_hand_mov = svg_hand.append("g")
    .attr("class", "g_hand_mov")
    .attr("transform", "translate(" + width_hand * 0.1 + ",0)"); // where hand starts sliding

  g_hand_mov.append("polygon")
    .attr("points", "17.3,25.5 17.2,15.2 16,14.1 14.8,14.1 13.6,15.2 13.6,32.9 12.2,32.9 8.5,29.3 6.3,29.4 5.6,30 5.7,32 12,38.3 16.3,42.4 16.4,45.8 27.5,44.9 27.5,41.4 29.6,39.3 29.8,26.6 28.7,25.5")
    .attr('transform', 'scale(0.9)')
    .attr('opacity', 0.85);

  var h_circle = g_hand_mov.append("circle")
    .attr('opacity', 0.85)
    .attr('fill', 'none')
    .attr('stroke', '#707070')
    .attr('stroke-width', 2)
    .attr('stroke-miterlimit', 10)
    .attr('cx', 15).attr('cy', 14.5).attr('r', 7.4);

  h_circle.each(circle_flash1);
  g_hand_mov.each(translate_hand1);
};

function circle_flash1() {
  d3.select(this)
    .transition()
    .attr("opacity", 0.01)
    .duration(800)
    .attr("r", 15)
    .on("end", circle_flash2);
}

function circle_flash2() {
  d3.select(this)
    .transition()
    .duration(0.001)
    .attr("r", 7.5)
    .attr("opacity", 0.8)
    .on("end", circle_flash1);
}

function translate_hand1() {
    svg_hand.attr("height",$(mainDiv).height());
  d3.select(this)
    .transition()
    .duration(3000)
    .attr("transform", "translate(" + width_hand * 0.9 + ",0)")
    .on("end", translate_hand2);

}

function translate_hand2() {
  svg_hand.attr("height",$(mainDiv).height());
  d3.select(this)
    .transition()
    .duration(3000)
    .attr("transform", "translate(" + width_hand * 0.01 + ",0)")
    .on("end", translate_hand1);
}


var isMobile = (function(a) {
  return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
})(navigator.userAgent || navigator.vendor || window.opera);

var uA = window.navigator.userAgent,
    isIE = /msie\s|trident\/|edge\//i.test(uA) && !!(document.uniqueID || document.documentMode || window.ActiveXObject || window.MSInputMethodContext);
var isAndroid = function() {
    return (/Android/i).test(navigator.userAgent || navigator.vendor || window.opera);
};
var is_IOSmobile = function() {
    return (/iPhone/i).test(navigator.userAgent || navigator.vendor || window.opera);
};

var showAddToHomeBubble = function(){
  if (isMobile){
    if (isAndroid() && !window.matchMedia('(display-mode: standalone)').matches){
      $(".top-right-bubble").removeClass("none");
    }else if (is_IOSmobile() && 'standalone' in navigator
              && !navigator.standalone
              && (/iphone|ipod|ipad/gi).test(navigator.platform)
              && (/Safari/i).test(navigator.appVersion)){
      $(".bottom-bubble").removeClass("none");
    }
  }
};
var game = {};
game.isMobile = isMobile;
game.isAndroid = isAndroid;
game.is_IOSmobile = is_IOSmobile;
game.showAddToHomeBubble = showAddToHomeBubble;
game.optionsJoysticks = {
  zone: document.getElementById('joystick-control-mobile'),
  mode: 'static',
  position: {
    left: '50%',
    top: '50%'
  },
  color: 'white',
  restOpacity: 1
};
game.modes = modes;
game.currentMode = currentMode;
game.isIE = isIE;
$(function() {
  $("#st-title").fadeIn(1000);

  $("#drone").delay(500).fadeIn(300);

  // $(".text").delay(1500).fadeIn(1000).removeClass("hidden");

  // sound icon
  $(".sounddiv").on("click touchend", "img", function(evt) {
    evt.preventDefault();
    // toggleSound();
    toggleSound1();
  });
  $(".backBtnDiv").on("click touchend", "img", function(evt) {
    evt.preventDefault();
    // location.reload();
    backToMenu();
  });


  //For guided tour
  $(".enter").click(function() {
    // $(".st-content-menu").css("pointer-events","none");
    game.currentMode = game.modes[0];
    $(".sounddiv").removeClass("none");
    $(".backBtnDiv").removeClass("none");
    $(".navbar").fadeOut(function(){
      $(".navbar_game").fadeIn();
    });
    $(".st-button-menu-mobile").fadeOut(function(){
      $(".st-button-menu-mobile").addClass("st-button-menu-mobile-display-game");
      $(".st-button-menu-mobile").removeClass("st-button-menu-mobile-display");
    });
    game.cameras[0].attachControl(game.engine.getRenderingCanvas(),true);
    game.cameras[0].target = game.cameraLookPoint; //have to set here
    game.scene.activeCamera = game.cameras[0];


    if (game.modes[0].pressed ===0){
      removeBubble();
      // game.getGuideModeCamera();
    }else{
        game.a_animatable = game.startDroneAnimation();
    }

    game.isInScene = true;
    if (!$(".closequiz").hasClass("none")) {
      $(".closequiz").addClass("none");
    }

    game.currentClip1.play();

    $("#box").delay(500).queue(function() {
      $("#box").addClass('transition');
      $("#guidetour").fadeIn();
      $(".textarea").fadeOut(function(){
        $(".textarea").css("display","none");
      });
      $(".main-headline").fadeOut(function(){
      $(".content-model").fadeIn();
      $(".badgegroup").fadeIn(1000);
      d3.timeout(function() {
        game.scene.getEngine().resize();
      }, 100);
      });

      game.is_automatic = 1;
      game.is_guideMode = 1;
      game.scene.activeCamera.alpha = -11;
      game.scene.activeCamera.beta = 1;
      $(this).dequeue();
    });

  });

  //For free and easy play
  $(".play").click(function() {
    // $(".st-content-menu").css("pointer-events","none");
    game.currentMode = game.modes[2];
    $(".navbar").fadeOut(function(){
      $(".navbar_game").fadeIn();
    });
    $(".sounddiv").removeClass("none");
    $(".backBtnDiv").removeClass("none");
    $(".st-button-menu-mobile").fadeOut(function(){
      $(".st-button-menu-mobile").addClass("st-button-menu-mobile-display-game");
      $(".st-button-menu-mobile").removeClass("st-button-menu-mobile-display");
    });
    game.isInScene = true;
    game.bgm.play();
    if ($(".closequiz").hasClass("none")) {
      $(".closequiz").removeClass("none");
    }

    game.is_guideMode = 0;
    game.is_automatic = 0;
    game.cameras[2].attachControl(game.engine.getRenderingCanvas(),true);
    game.scene.activeCamera = game.cameras[2];
    if (modes[2].pressed === 0) {
      removeBubble();
      game.getDirArrows();
    }
    // else
    // {
    //   game.a_drone[0].position = game.start_location;
    //   game.a_drone[0].rotation.y = Math.PI;
    // }
    $("#box").delay(500).queue(function() {
      $("#box").addClass('transition');
      $(".textarea").fadeOut(function(){
        $(".textarea").css("display","none");
      });
      $(".main-headline").fadeOut(function(){
        $(".content-model").fadeIn();
        d3.timeout(function() {
          game.scene.getEngine().resize();
        }, 101);
      });
      $(".badgegroup").fadeIn(1000);

      if (!isMobile) {
        $(".content-instructions").fadeIn();
        $("#flymodedesktop").removeClass("none");
      } else
        $("#flymodemobile").removeClass("none");
      $("#flymode").animateCss("fadeIn", "in");

      $(this).dequeue();
    });
  });

  $(".content-instructions .button").click(function() {
    $(".instructions").animate({
      width: "toggle",
      paddingLeft: "toggle",
      paddingRight: "toggle",
      marginLeft: "toggle",
      marginRight: "toggle"
    }, 500);
  });

  $('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
    e.preventDefault();
  });
  $(".yes").click(function() {
    if (modes[1].pressed === 0){
      removeBubble();
    }
    $(".st-title").fadeOut(500);
    $(".mobilefirst").fadeOut(500);
    $("#box").fadeOut(500);
    $(".mobilesecond").fadeIn(1000);
  });

  $(".mobilesecond").click(function(e) {
    // $(".st-content-menu").css("pointer-events","none");
    game.currentMode = game.modes[1];
    $(".navbar").fadeOut(function(){
      $(".navbar_game").fadeIn();
    });
    $(".st-button-menu-mobile").fadeOut(function(){
      $(".st-button-menu-mobile").addClass("st-button-menu-mobile-display-game");
      $(".st-button-menu-mobile").removeClass("st-button-menu-mobile-display");
    });
    // if (game.is_vr===1)return false;
    // e.preventDefault();
    $(".sounddiv").removeClass("none");
    $(".backBtnDiv").removeClass("none");
    game.is_vr = 1;
    game.isInScene = true;

    if (modes[1].pressed === 0){
      game.getGuideVrCamera1();
      game.initDroneAnimation();
      game.getVRQuesPlane();
      // game.getVRInfoPlane();
      game.getVRAnsPlanes();
      game.getResultPlane();
    }else{
      game.cameras[1].attachControl(game.engine.getRenderingCanvas(), true);
      game.scene.activeCamera = game.cameras[1];
      game.scene.activeCamera.alpha = -13;
      game.scene.activeCamera.beta = 0.5;
      game.a_animatable = game.startDroneAnimation();
    }
    game.is_automatic = 1;
    game.is_guideMode = 1;

    game.currentClip1.play();

    if (!game.engine.isFullscreen) game.engine.switchFullscreen();
    if (!$(".closequiz").hasClass("none")) {
      $(".closequiz").addClass("none");
    }
    $(".textarea").fadeOut(function(){
      $(".textarea").css("display","none");
    });
    $(".main-headline").fadeOut(function(){
      $(".content-model").fadeIn();
      d3.timeout(function() {
        game.scene.getEngine().resize();
      }, 100);
    });
    $(this).css("display","none");
  });

  $(".no").click(function() {
    removeBubble();
    $(".mobilefirst").fadeOut(500);
      $(".textarea").fadeIn(function(){
        $(".textarea").css("display","block");
        $(".textarea").removeClass("none");
      });
      d3.timeout(d => {
        assign_height_box();
        assign_height_main();
      }, 500);

  });

  $(window).on("load", function() {
    // console.log("win loaded");
    game.isWinLoaded = true;
    if (game.isSceneLoaded){
        // console.log("scene loaded first then win");
      $("#loading-page progress").val(100);
      $("#loading-num").html("100%");
      $("#loading-page").animateCss("fadeOut","out",function(){
        // $("#wrapper-page").css("overflow-y","auto");
      });
      if (isMobile && window.innerWidth < 768) {
        $(".mobilefirst").animateCss("fadeIn", "in");
      } else {
        $(".text .enter,.text .play").css("position", "absolute");
        $(".textarea").css("display", "block");
      }
      assign_height_box();
      assign_height_main();
      $(".content-canvas").height(window.innerHeight);

      showAddToHomeBubble();
    }
  });

  $(window).on("resize", function() {
    assign_height_box();
    assign_height_main();
    $(".content-canvas").height(window.innerHeight);
    if (!isIE)$(".nano").nanoScroller();
  });

  $("#flymode .gotitbtn").click(function() {
    $("#flymode").animateCss("fadeOut", "out");
    if (game.isMobile) {
      $(".joystick-control-mobile").removeClass("none");
      $(".content-buttons-up-down").removeClass("none");
      game.createControlMobile();
    }
  });

  $("#guidetour .gotitbtn").click(function() {
    $("#guidetour").fadeOut();
  });

  $(".endingpageoverlay").on("click touchend", '#tryagainbutton', function(evt) {
    evt.preventDefault();
    // location.reload();
    backToMenu();
  });

  $(".quizSection").on("click touchend", '.closequiz', function(evt) {
    evt.preventDefault();
    setTimeout(function() {
      game.registerEnterExitAction(game.cur_intersect_quiz);
    }, 1000);

    showresultpage();
  });

  $(".quizSection").on("click touchend", '#continue', function(evt) {
    evt.preventDefault();
    showresultpage();
    game.destroyIcons($(this).parent().attr("id"));
  });

  $(".bottom-bubble").click(function(){
  removeBubble();
});
$(".top-right-bubble").click(function(){
  removeBubble();
});


});
$(function() {
  $(".content-canvas").height(window.innerHeight);
  game = Object.assign(new Render("st-content-model"), game);
  global.game = game;

  $(".button-up .btn-intern").on("touchstart", function(evt) {
    evt.keyCode = game.keysUp[0];
    game.onKeyDown(evt);
  });
  $(".button-up .btn-intern").on("touchend", function(evt) {
    evt.keyCode = game.keysUp[0];
    game.onKeyUp(evt);
  });
  $(".button-down .btn-intern").on("touchstart", function(evt) {
    evt.keyCode = game.keysDown[0];
    game.onKeyDown(evt);
  });
  $(".button-down .btn-intern").on("touchend", function(evt) {
    evt.keyCode = game.keysDown[0];
    game.onKeyUp(evt);
  });

});

// grab info and quiz from csv

var colorAray = [],
  themeTitle = [],
  correctoption = [],
  colour = [],
  point = 0;
  // countclick = 0;
$(function() {

  $(".nano-pane").css("display","block");
  $(".nano-slider").css("display","block");

  d3.csv("data/smart-nation-quiz-info-6q.csv", function(data) {
    game.data_csv = data;
    data.forEach(function(d, i) {
      themeTitle[i] = d.topic_id;
      d.topic_id = d.topic_id;

      // grab info

      correctoption[i] = d.correctoption;
      game.correctoption[i] = d.correctoption;
      colour[i] = d.colour;
      d.topic = d.topic;
      d.hint_title = d.hint_title;
      d.hint_text = d.hint_text;
      d.topic_img = "images/" + d.topic_img;

      var hintcontent = '<div id="info_' + d.topic_id + '" class="content-card-info none"><div class="infoimg"><img src="' + d.topic_img + '"><h1>' + d.topic + '</h1>' + '<span>' + d.hint_title + '</span><p>' + d.hint_text + '</p></div></div>';

      $('.clueSection').append(hintcontent);

      //grab quiz
      d.ques_id = d.ques_id;
      d.ques_img = "images/" + d.ques_img;
      d.quiz = d.quiz;
      d.quiz_img = "images/smart-nation/" + d.quiz_img;
      d.ques_title = d.ques_title;
      d.ques_text = d.ques_text;
      d.opA = d.opA;
      d.opB = d.opB;
      d.opC = d.opC;
      d.opid = d.opid;
      d.opid1 = d.opid1;
      d.opid2 = d.opid2;
      d.img1 = d.img1;
      d.img2 = d.img2;
      d.img3 = d.img3;
      var quizcontent = '  <div id="quiz_' + d.topic_id + '" class="nano content-card-quiz none"><div class="quizimg"><img src="' + d.quiz_img + '"></div><div class="quiztext nano-content"><div class="quiztitle"><img src="' + d.ques_img + '"><h1>' + d.topic + '</h1></div><div class="title">' + d.ques_title + '</div><div class="quiz_que">' + d.ques_text + '</div><div id="' + d.opid + '" class="option">' + '<img class= "image" src="images/' + d.img1 +
      '" ><p>' + d.opA + '</p></div><div id="' + d.opid1 + '" class="option">' + '<img class="image" src="images/' + d.img2 + '"><p>' + d.opB + '</p></div><div id="' + d.opid2 + '" class="option">' + '<img class="image" src="images/' + d.img3 + '"><p>' + d.opC + '</p></div>' +
      '<div id="' + d.topic_id + '"class="bottom"><div id="bottomtext"></div><div id="bottomtext1"></div><div id="bottomtext2"></div>' + '<div id="bottomtext3"></div><hr><div id="continue">CONTINUE</div></div>' + '</div><div class="closequiz"><img src="images/close.svg"></div></div>';

      $('.quizSection').append(quizcontent);
    });
    if (!isIE){
      $(".nano").nanoScroller({
        scroll:'top',
        alwaysVisible:true
      });
    }

  });

});

// answer quiz questions
$(function() {

  // $(".quizSection").on("click touchend", '.option', function(evt) {
  $(".quizSection").on("click", '.option', function(evt) {
    evt.preventDefault();
    var all_options = $(this).siblings(".option").andSelf();

    // mobile landscape view with small height or mobile portrait view with small width
    // hide options
    if (isMobile && (window.innerHeight < window.innerWidth) && (window.innerHeight < 360) || isMobile && (window.innerHeight > window.innerWidth) && (window.innerWidth < 468)) {
      all_options.fadeOut();
    }

    if (all_options.hasClass("rightAnswer") || all_options.hasClass("wrongAnswer"))
      return;
    game.countclick++;

    var tmp = $(this).attr("id");
    var optionid = tmp.substring(4);
    var questionid = tmp.substring(2, 3);
    var correct = correctoption[questionid - 1];
    var topic = themeTitle[questionid - 1];
    var colours = colour[questionid - 1];

    $("#quiz_" + topic + " .closequiz").fadeOut();
    if (optionid == correct) {
      $('#' + tmp).addClass('rightAnswer');
      $("#" + topic + " #bottomtext").html("<b>Congratulations!");
      $("#" + topic + " #bottomtext1").html("Your earned the");
      $("#" + topic + " #bottomtext3").html("badge!");
      $("#" + topic + " #continue").fadeIn();
      $("#" + topic + " #bottomtext2").html('<img src="images/smart-nation/icon-' + topic + '-small.png">');
      point++;
    } else {
      $('#' + tmp).addClass('wrongAnswer');
      $("#" + topic + " #bottomtext").html("<b>Better luck next time.");
      $("#" + topic + " #bottomtext1").html("If you're in fly mode, get hints from info points.");
      $("#" + topic + " #continue").fadeIn();
      $("#badge_" + topic).append('<img class="wrongcross" src="images/Quiz/wrong-answer.png" alt="">');
    }

    if (questionid == 1 && optionid == correct) {
      $("#effectbadge2").addClass("effectbadge");
      $("#effectbadge2").css("display", "block");
      flyToElement($("#effectbadge2 img"), $(".indbadge.badge2 img"), "images/smart-nation/icon-transport-small.png");
      $(".badgeicon.badge2 img").attr("src", "images/Quiz/transport-animate.svg");
      $(".badgeicon.badge2 p").addClass("blackText");
    } else if (questionid == 2 && optionid == correct) {
      $("#effectbadge4").addClass("effectbadge");
      $("#effectbadge4").css("display", "block");
      flyToElement($("#effectbadge4 img"), $(".indbadge.badge4 img"), "images/smart-nation/icon-health-small.png");
      $(".badgeicon.badge4 img").attr("src", "images/Quiz/health-animate.svg");
      $(".badgeicon.badge4 p").addClass("blackText");
    } else if (questionid == 3 && optionid == correct) {
      $("#effectbadge1").addClass("effectbadge");
      $("#effectbadge1").css("display", "block");
      flyToElement($("#effectbadge1 img"), $(".indbadge.badge1 img"), "images/smart-nation/icon-virtual-small.png");
      $(".badgeicon.badge1 img").attr("src", "images/Quiz/virtualsingapore-animate.svg");
      $(".badgeicon.badge1 p").addClass("blackText");
    } else if (questionid == 4 && optionid == correct) {
      $("#effectbadge3").addClass("effectbadge");
      $("#effectbadge3").css("display", "block");
      flyToElement($("#effectbadge3 img"), $(".indbadge.badge3 img"), "images/smart-nation/icon-environment-small.png");
      $(".badgeicon.badge3 img").attr("src", "images/Quiz/environment-animate.svg");
      $(".badgeicon.badge3 p").addClass("blackText");
    } else if (questionid == 5 && optionid == correct) {
      $("#effectbadge5").addClass("effectbadge");
      $("#effectbadge5").css("display", "block");
      flyToElement($("#effectbadge5 img"), $(".indbadge.badge5 img"), "images/smart-nation/icon-security-small.png");
      $(".badgeicon.badge5 img").attr("src", "images/Quiz/security-animate.svg");
      $(".badgeicon.badge5 p").addClass("blackText");
    } else if (questionid == 6 && optionid == correct) {
      $("#effectbadge6").addClass("effectbadge");
      $("#effectbadge6").css("display", "block");
      flyToElement($("#effectbadge6 img"), $(".indbadge.badge6 img"), "images/smart-nation/icon-digital-small.png");
      $(".badgeicon.badge6 img").attr("src", "images/Quiz/digital-animate.svg");
      $(".badgeicon.badge6 p").addClass("blackText");
    }

    if (point == 1) {
      document.getElementById("endingtext").innerHTML = "Nice, you got one badge.";
      document.getElementById("share").innerHTML = "BRAG";
      document.getElementById("tryagainbutton").innerHTML = "<h3>TRY FOR MORE</h3>";
    } else if (point == 2) {
      document.getElementById("endingtext").innerHTML = "Nice, you got two badges.";
      document.getElementById("share").innerHTML = "BRAG";
      document.getElementById("tryagainbutton").innerHTML = "<h3>TRY FOR MORE</h3>";
    } else if (point == 3) {
      document.getElementById("endingtext").innerHTML = "Nice, you got three badges.";
      document.getElementById("share").innerHTML = "BRAG";
      document.getElementById("tryagainbutton").innerHTML = "<h3>TRY FOR MORE</h3>";
    } else if (point == 4) {
      document.getElementById("endingtext").innerHTML = "Nice, you got four badges.";
      document.getElementById("share").innerHTML = "BRAG";
      document.getElementById("tryagainbutton").innerHTML = "<h3>TRY FOR MORE</h3>";
    } else if (point == 5) {
      document.getElementById("endingtext").innerHTML = "Awesome, you got five badges.";
      document.getElementById("share").innerHTML = "BRAG";
      document.getElementById("tryagainbutton").innerHTML = "<h3>FLY AGAIN</h3>";
    } else if (point == 6) {
      document.getElementById("endingtext").innerHTML = "Awesome, you got all six badges and are certified future ready!";
      document.getElementById("share").innerHTML = "BRAG";
      document.getElementById("tryagainbutton").innerHTML = "<h3>FLY AGAIN</h3>";
    } else {
      document.getElementById("endingtext").innerHTML = "Oh well, the badges aren't that shiny anyway… or are they?";
      document.getElementById("share").innerHTML = "SHARE… REALLY?";
      document.getElementById("tryagainbutton").innerHTML = "<h3>TRY AGAIN</h3>";
    }

    document.getElementById("score").innerHTML = point;
    // auto scroll to bottom in quiz in horizontal view in mobile
    var innerdiv = $(this).parent(".quiztext");
    innerdiv.animate({
      scrollTop: innerdiv.height()
    }, 'slow',function(){
      if (!isIE) $(".nano").nanoScroller();
    });

  });
});

function showresultpage() {
  if (game.is_guideMode === 1) {
    game.pauseAnimationAndVoice(false);
    game.guideIsPause = false;
  } else {
    game.is_automatic = 0;
  }

  $(".quizSection").removeClass("upzindex");
  $(".quizSection").fadeOut();
  $(".content-card-quiz").fadeOut(function(){
    if (game.countclick === game.totalQuiz) {
      $(".endingpageoverlay").fadeIn(function(){
        hand_sliding_effect(".endingpage");
      });
      if (game.is_guideMode === 0) {
        game.is_automatic = 1;
      }
    }
  });

}

function flyToElement(flyer, flyingTo, imgsrc) {
  var flyerC = $(flyer).clone();
  $(flyerC).css("opacity",1);
  $(flyerC).addClass("spinghard");
  $(flyerC).delay(1000).fadeOut();
  var divider = 6;
  var flyerClone = $(flyerC).clone();
  $(flyerClone).css({
    position: 'absolute',
    top: $(flyer).offset().top + "px",
    left: $(flyer).offset().left + "px",
    opacity: 1,
    'z-index': 100000

  });
  $('body').append($(flyerClone));
  var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyerC).width() / divider) / 2;
  var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyerC).height() / divider) / 2;

  $(flyerClone).delay(1000).animate({
    opacity: 0.4,
    left: gotoX,
    top: gotoY,
    width: $(flyerC).width() / divider,
    height: $(flyerC).height() / divider
  }, 700, function() {
    $(flyingTo).fadeOut('fast', function() {
      $(flyingTo).attr("src", imgsrc);
      $(flyingTo).fadeIn('fast', function() {
        $(flyingTo).addClass("sping");
        $(flyerClone).fadeOut('fast', function() {
          $(flyerClone).remove();
          $(flyerC).parent(".effectbadge").remove();
        });
      });
    });
  });
}

function assign_height_box() {
  $(".textarea .text").css("height", "auto");
  var height_box_text = 0;

  $(".textarea .text").each(function() {
    if ($(this).height() > height_box_text)
      height_box_text = $(this).height();
  });
  $(".textarea .text").height(height_box_text + 40);
}

function assign_height_main() {
  $(".main-headline").css("height", "auto");
  $(".content-main-body").removeClass("center-middle-vertical");
  if (window.innerHeight > $(".content-main-body").outerHeight()) {
    $(".main-headline").height(window.innerHeight);
    // $(".content-main-body").addClass("center-middle-vertical");
  }
}

function handleVisibilityChange(evt) {
  if (document[hidden]) {
    if (game.is_guideMode === 1) {
      if (!game.guideIsPause) {
        game.pauseAnimationAndVoiceInactive(true);
        game.scene._animate();
      }
    } else {
      game.pauseBGM(true);
    }
  } else {
    if (game.is_guideMode === 1) {
      if (!game.guideIsPause) {
        game.pauseAnimationAndVoiceInactive(false);
        game.scene._animate();
      }
    } else {
      game.pauseBGM(false);
    }
  }
}

var hidden = "hidden";

function getVisibilityChange() {
  var vc = "visibilitychange";
  if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
    hidden = "hidden";
    vc = "visibilitychange";
  } else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    vc = "msvisibilitychange";
  } else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    vc = "webkitvisibilitychange";
  } else if (typeof document.mozHidden !== "undefined") {
    hidden = "mozHidden";
    vc = "mozvisibilitychange";
  }
  return vc;
}

document.addEventListener(getVisibilityChange(), handleVisibilityChange, false);

function toggleSound1() {
  if (game.is_guideMode === 0) {
    if (!game.bgm.muted) {
      game.bgm.muted = true;
      game.isMuted = true;
      $(".sounddiv #soundIcon").css("display", "none");
      $(".sounddiv #muteIcon").css("display", "block");
    } else {
      game.bgm.muted = false;
      game.isMuted = false;
      $(".sounddiv #soundIcon").css("display", "block");
      $(".sounddiv #muteIcon").css("display", "none");
    }
  } else {
    if (!game.currentClip1.muted) {
      game.currentClip1.muted = true;
      $(".sounddiv #soundIcon").css("display", "none");
      $(".sounddiv #muteIcon").css("display", "block");
    } else {
      game.currentClip1.muted = false;
      $(".sounddiv #soundIcon").css("display", "block");
      $(".sounddiv #muteIcon").css("display", "none");
    }
  }

}


function showAddToHomeBubble(){
  if (isMobile){
    if (isAndroid() && !window.matchMedia('(display-mode: standalone)').matches){
      $(".top-right-bubble").removeClass("none");
    }else if (is_IOSmobile() && 'standalone' in navigator
              && !navigator.standalone
              && (/iphone|ipod|ipad/gi).test(navigator.platform)
              && (/Safari/i).test(navigator.appVersion)){
      $(".bottom-bubble").removeClass("none");
    }
  }
}

function removeBubble(){
  if (isAndroid()){
    $(".top-right-bubble").animateCss("fadeOut","out",function(){
      $(this).addClass("none");
      $(this).css("display","none");
    });
  }else{
    $(".bottom-bubble").animateCss("fadeOut","out",function(){
      $(this).addClass("none");
      $(this).css("display","none");
    });
  }
}

function backToMenu(){
  $(".endingpageoverlay").fadeOut(function(){
    resetEndingPage();
  });
  game.currentMode.pressed = 1;
  game.isInScene = false;
  game.startAnim = false;
  game.checkSound = false;
  game.is_vr = 0;
  game.is_automatic = 0;
  game.is_guideMode = 0;
  game.picked_mesh_vr = 0;
  game.result_vr={
    correct:0,
    incorrect:0
  };
  game.countclick = 0;
  game.cur_intersect_quiz = null;
  if (!game.anim_rotatable) game.anim_rotatable = true;

  game.clearIcons();
  game.a_drone[0].position = game.start_location;
  game.a_drone[0].rotation.y = Math.PI;
  game.createIcons();

  if (game.currentMode.name ==="guideVR"){
    game.hideVisibilityInstant(game.resultPlane);
    game.hideVRQuizPlaneInstant();
    game.showDroneInstant();
    game.crosshair.visibility = 0;
  }else{
    resetQuizUI();
  }

  if (game.currentMode.name ==="fly"){

    if (isMobile){
      if (!$(".joystick-control-mobile").hasClass("none")) $(".joystick-control-mobile").addClass("none");
      if (!$(".content-buttons-up-down").hasClass("none")) $(".content-buttons-up-down").addClass("none");

    }else{
      $(".content-instructions").fadeOut();
    }
    game.bgm.currentTime = 0;
    game.bgm.pause();
    game.bgm.muted = game.isMuted;

  }else{
    game.scene.stopAnimation(game.a_drone[0]);
    game.currentClip1.currentTime = 0;
    game.currentClip1.pause();
    game.isMuted = game.currentClip1.muted;

    game.currentClip1 = game.narratives[0];
    game.currentClip1.currentTime = 0;
    game.currentClip1.pause();
    game.currentClip1.muted = game.isMuted;
  }
  // if (game.currentMode.name ==="guide" || game.currentMode.name ==="guideVR"){
  //   game.scene.stopAnimation(game.a_drone[0]);
  //   game.currentClip1.currentTime = 0;
  //   game.currentClip1.pause();
  //   game.isMuted = game.currentClip1.muted;
  //
  //   game.currentClip1 = game.narratives[0];
  //   game.currentClip1.currentTime = 0;
  //   game.currentClip1.pause();
  //   game.currentClip1.muted = game.isMuted;
  // }else{
  //   if (!isMobile){
  //       $(".content-instructions").fadeOut();
  //   }else{
  //     if (!$(".joystick-control-mobile").hasClass("none")) $(".joystick-control-mobile").addClass("none");
  //     if (!$(".content-buttons-up-down").hasClass("none")) $(".content-buttons-up-down").addClass("none");
  //   }
  //   game.bgm.currentTime = 0;
  //   game.bgm.pause();
  //   game.bgm.muted = game.isMuted;
  // }

  d3.selectAll(".hand-slide").remove();
  $(".sounddiv").addClass("none");
  $(".backBtnDiv").addClass("none");
  $(".navbar_game, .badgegroup").fadeOut(function(){
    $(".navbar").fadeIn();
    $(".st-button-menu-mobile").addClass("st-button-menu-mobile-display");
    $(".st-button-menu-mobile").removeClass("st-button-menu-mobile-display-game");
  });

  $(".content-model").fadeOut();
  $("#box").removeClass('transition');
  $(".main-headline").delay(500).fadeIn(function(){
    if (isMobile && window.innerWidth < 768){
      $(".st-title").fadeIn();
      $("#box").fadeIn();
      $(".mobilefirst").css("display","block");
    }else{
      $(".textarea").css("display", "block");
    }

    d3.timeout(d => {
      assign_height_box();
      assign_height_main();
    },500);

  });
    // $(".st-content-menu").css("pointer-events","auto");
}

function resetQuizUI(){
  $(".bottom").each(function(){
    $(this).children("#bottomtext").html("");
    $(this).children("#bottomtext1").html("");
    $(this).children("#bottomtext3").html("");
    $(this).children("#continue").fadeOut();
    $(this).children("#bottomtext2").html("");
  });

  $(".quizSection").each(function(){
    var all_options = $(this).find(".option").siblings(".option").andSelf();
    $(all_options).removeClass("rightAnswer");
    $(all_options).removeClass("wrongAnswer");
  });

  // $(".badgegroup .indbadge img:nth-child(1)").each(function(){
  //   var src = $(this).attr("src");
  //     if (!/-grey/.test(src)){
  //       $(this).attr("src",src.split(".png")[0]+"-grey.png");
  //     }
  // });
  $("#badge_environment img:nth-child(1)").attr("src","images/smart-nation/icon-environment-small-grey.png");
  $("#badge_health img:nth-child(1)").attr("src","images/smart-nation/icon-health-small-grey.png");
  $("#badge_virtual img:nth-child(1)").attr("src","images/smart-nation/icon-virtual-small-grey.png");
  $("#badge_transport img:nth-child(1)").attr("src","images/smart-nation/icon-transport-small-grey.png");
  $("#badge_security img:nth-child(1)").attr("src","images/smart-nation/icon-security-small-grey.png");
  $("#badge_digital img:nth-child(1)").attr("src","images/smart-nation/icon-digital-small-grey.png");

  // $(".badgeicon a img").each(function(){
  //   var src = $(this).attr("src");
  //   if (!/-grey/.test(src)){
  //     $(this).attr("src",src.split(".svg")[0]+"-grey.svg");
  //   }
  // });

  $(".clueSection").css("display", "none");

  $(".wrongcross").remove();
  $(".option").show();

  point = 0;

 }

 function resetEndingPage(){
   $(".badgeicon p").each(function(){
   $(this).removeClass("blackText");
   });

   $("#endBadge_environment a img:nth-child(1)").attr("src","images/Quiz/environment-animate-grey.svg");
   $("#endBadge_health a img:nth-child(1)").attr("src","images/Quiz/health-animate-grey.svg");
   $("#endBadge_virtual a img:nth-child(1)").attr("src","images/Quiz/virtualsingapore-animate-grey.svg");
   $("#endBadge_transport a img:nth-child(1)").attr("src","images/Quiz/transport-animate-grey.svg");
   $("#endBadge_security a img:nth-child(1)").attr("src","images/Quiz/security-animate-grey.svg");
   $("#endBadge_digital a img:nth-child(1)").attr("src","images/Quiz/digital-animate-grey.svg");
   document.getElementById("score").innerHTML = 0;
   document.getElementById("endingtext").innerHTML = "";
   document.getElementById("share").innerHTML = "";
   document.getElementById("tryagainbutton").innerHTML = "";
 }
