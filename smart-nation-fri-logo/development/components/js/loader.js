export default class Loader {
  constructor() {}
  loadMeshes() {
    var _this = this;
    this.a_helices = [];

    // var mybbr = this.scene.getBoundingBoxRenderer();
    // mybbr.frontColor = BABYLON.Color3.Blue();
    // mybbr.backColor = BABYLON.Color3.Green();
    // mybbr.showBackLines = false;

    var redWireframeMaterial = new BABYLON.StandardMaterial("redWireframeMaterial", _this.scene);
    redWireframeMaterial.diffuseColor = new BABYLON.Color3(1, 0, 0);
    redWireframeMaterial.emissiveColor = new BABYLON.Color3(1, 0, 0);
    redWireframeMaterial.specularColor = new BABYLON.Color3(1, 0, 0);
    redWireframeMaterial.backFaceCulling = true;
    redWireframeMaterial.alpha = 0.3;

    BABYLON.SceneLoader.ImportMesh("", "babylon/", "smart-city-sameHeightBuildings_3.babylon", this.scene, function(meshes) {
      try {
        _this.meshes = meshes;
        _this.Mbase = meshes[30];
        _this.a_helices.push(meshes[18]);
        _this.a_helices.push(meshes[19]);
        _this.a_helices.push(meshes[20]);
        _this.a_helices.push(meshes[21]);

        _this.a_drone.push(meshes[23]);
        _this.a_drone.push(meshes[22]);
        // _this.a_drone = _this.a_drone.concat(_this.a_helices);

        var mesmat = new BABYLON.StandardMaterial("groundMat", _this.scene);
        mesmat.diffuseColor = BABYLON.Color3.Gray();
        var buildingMat = new BABYLON.StandardMaterial("buildingMat", _this.scene);
        buildingMat.diffuseColor = new BABYLON.Color3(0.8, 0.8, 0.8);
        buildingMat.emissiveColor = new BABYLON.Color3(0.4, 0.4, 0.4);
        buildingMat.specularColor = BABYLON.Color3.Black();

        meshes.forEach((mesh, counter) => {
          mesh.goodPositionDrone=0;
          mesh.convertToFlatShadedMesh();

          if (/drone-/.test(mesh.name)) {
            mesh.material.specularColor = BABYLON.Color3.White();
            mesh.material.emissiveColor = new BABYLON.Color3(30/255,30/255,30/255);
            mesh.receiveShadows = false;
          } else {
            mesh.receiveShadows = true;
            // mesh.convertToFlatShadedMesh(); //convert everything but drone into flat shading
          }

          if (!mesh.material) {
            mesh.material = mesmat;
          }else if (/base/.test(mesh.name)) {
            mesh.material.diffuseColor = new BABYLON.Color3(0.5, 0.5, 0.5);
          }

          mesh.material.backFaceCulling = false;
          _this.scale_mesh(mesh);
        });

        _this.a_drone = _this.a_drone.concat(_this.a_helices);
        _this.translate_mesh();

        _this.assign_parent(_this.a_drone[0], _this.a_drone[1]);
        _this.assign_parent(_this.a_drone[0], _this.a_drone[2]);
        _this.assign_parent(_this.a_drone[0], _this.a_drone[3]);
        _this.assign_parent(_this.a_drone[0], _this.a_drone[4]);
        _this.assign_parent(_this.a_drone[0], _this.a_drone[5]);



        // _this.a_drone[0].position = _this.start_location;
        // _this.a_drone[0].lookAt(_this.)
        // _this.a_drone[0].physicsImpostor = new BABYLON.PhysicsImpostor(_this.a_drone[0],BABYLON.PhysicsImpostor.BoxImpostor,{mass:0,restitution:0.6},_this.scene);

        // _this.a_drone[0].setPhysicsState(BABYLON.PhysicsImpostor.BoxImpostor,{mass:0.01,restitution:0.6});

        // _this.a_drone[0].physicsImpostor.applyImpulse(new BABYLON.Vector3(0,0.1,0),_this.a_drone[0].getAbsolutePosition());
        // _this.a_drone[0].checkCollisions = true;
        //_this.a_drone[0].ellipsoid = new BABYLON.Vector3(0.06, 0.06, 0.06);
        for (var i = 0; i < _this.boundaries.length; i++) {
          // _this.boundaries[i].setPhysicsState(BABYLON.PhysicsImpostor.BoxImpostor,{mass:0,restitution:0.6});
          _this.boundaries[i].checkCollisions = true;
        }

        //_this.a_droneBox.position = _this.a_drone[0].position.clone();
        //_this.a_drone[0].parent = _this.a_droneBox;

        // _this.putDroneDeparture();
        // _this.updateCamera();

        // // _this.dronePosition = new BABYLON.Vector3(5, -20, -70);
        // // _this.dronePosition = new BABYLON.Vector3(0, -241, -280);
        //
        // // this.dronePosition = this.dronePosition.normalize();
        // // _this.drone.position = _this.dronePosition;
        // // _this.drone.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(0, 1, 1);
        // // _this.drone.parent = _this.scene.activeCamera;
        // // _this.scene.activeCamera.position=drone.position.scale(2);
        //
        // _this.updateCamera();
        //
        // // _this.camera.position.y=size_drone.y;
        //
        // // _this.camera.lockedTarget = _this.drone;
        //
        // _this.instance_tree_3 = meshes[2].createInstance("instance_tree_3");
        // _this.instance_tree_3.position.x = 45;
        // _this.instance_tree_3.position.z = -160;
        //
        // _this.clone_tree_3 = meshes[2].clone("clone_tree_3");
        // _this.clone_tree_3.position.x = -20;
        // _this.clone_tree_3.position.z = -140;
        //
        // _this.createLinesDots(_this.meshes[0], 70);
        // _this.createLinesDots(_this.meshes[3], 100);
        //
        // _this.createLinesDots(_this.clone_tree_3, 100);
        //
        // _this.createLinesDots(_this.instance_tree_3, 100);
        //
        // // _this.createBoxST(meshes[0]);

        // console.log("end");
      } catch (e) {
        console.log(e);
      }
    // });
      },function(evt){
    if (evt.lengthComputable) {
      var progress = evt.loaded/evt.total*99;
      $("#loading-page progress").val(progress.toFixed());
      $("#loading-num").html(Math.floor(progress.toFixed())+"%");
    }else{
      var progress = evt.loaded /(1024*1024)*99;
      $("#loading-page progress").val(progress.toFixed());
      $("#loading-num").html(Math.floor(progress.toFixed())+"%");

    }
  });
  }
}
