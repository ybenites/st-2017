import aggregation from "./libraries/aggregation";
import Loader from "./loader";
import Op1 from "./op1";
import Op2 from "./op2";

export default class Render extends aggregation(Loader, Op1, Op2) {
  constructor(canvasId) {
    super();
    if (BABYLON.Engine.isSupported()) {
      var _this = this;
      this.keys = [];
      this.keysGo = [38];
      this.keysBack = [40];
      this.keysLeft = [37];
      this.keysRight = [39];
      this.keysUp = [81];
      this.keysDown = [65];
      // this.speed = 0.01;
      this.speed = 0.002;
      this.rotateSpeed = 0.0007;
      //this.maxRotateSpeed = 0;
      this.scale = 1;
      this.alpha;

      this.keyBackIsDown = false;
      this.orientationGamma = 0;
      this.orientationBeta = 0;
      this.initialOrientationGamma = 0;
      this.initialOrientationBeta = 0;
      this.direction = new BABYLON.Vector3(0, 0, 0);
      this.droneRotation = new BABYLON.Vector3(0, 0, 0);
      this.boundaries = [];
      var canvas = document.getElementById(canvasId);
      this.engine = new BABYLON.Engine(canvas, true);
      // this.engine.renderEvenInBackground = true;
      // this.engine.enableOfflineSupport = false;
      this.spotLight01;
      this.shadowGenerator01;
      this.cameraLookPoint;
      this.scene = null;
      this.a_drone = [];
      this.a_textures={};
      this.a_droneBox;
      this.a_buildings_icons = {};
      this.time=0;
      this.a_path_drone=[];
      this.a_path_info=[];
      this.path_frames = [490, 790, 960, 1270, 1500, 1780, 1960, 2385, 2670, 2910, 3100, 3420,3600];
      this.audioPausePoint = 0;
      this.totalQuiz = 6;
      this.quizTopics = ["environment","health","virtual","transport","security","digital"];
      this.is_automatic=0;
      this.is_guideMode = 0;
      this.bgm=document.getElementById("sound-bgm");
      this.bgmClip = "http://udenplan.com/future-singapore/audios/bgm.mp3";
      // this.narrativeSound=document.getElementById("sound-narrative");
      // this.narrativeFilePath = "http://udenplan.com/future-singapore/audios/narrative_bgm_6q.mp3";
      // this.narrativeFilePath = "audios/narrative_bgm_6q.mp3";
      // this.currentClip = document.getElementById("currentClip");
      this.clipSrcs = ["http://udenplan.com/future-singapore/audios/clip_environment.mp3",
                       "http://udenplan.com/future-singapore/audios/clip_health.mp3",
                       "http://udenplan.com/future-singapore/audios/clip_virtual.mp3",
                       "http://udenplan.com/future-singapore/audios/clip_transport.mp3",
                       "http://udenplan.com/future-singapore/audios/clip_security.mp3",
                       "http://udenplan.com/future-singapore/audios/clip_digital.mp3",
                       "http://udenplan.com/future-singapore/audios/clip_end.mp3"];
      this.narratives = document.getElementsByClassName("narrative-clip");
      this.currentClip1 = this.narratives[0];
      this.isMuted = false;
      this.currentInfo = $("#info_transport");
      this.infoTimer;
      this.cameras =[];
      this.countclick = 0;
      this.a_animatable;
      this.start_location = new BABYLON.Vector3(1.111,0.822,18.135);
      this.anim_travel_location = [];
      this.anim_target_location;
      this.anim_location_counter = 0;
      this.anim_rotatable = true;
      this.mouse_is_drag = false;
      this.cur_intersect_quiz;
      this.guideIsPause = false;
      this.isInScene = false;
      this.bgmVolume = 0.1;
      this.guideBgmVolume = 0.7;
      this.startAnim = false;
      this.checkSound = false;
      this.lineDotsGroup = [];
      this.quizBeamGroup = [];
      this.oldX = 0;
      this.oldY = 0;
      this.is_vr=0;
      this.dragInit={
        x:0,
        y:0
      };
       this.angularSensibility = 2000.0;
      // this.debug = new Debug(this);
      this.tempMesh;
      this.last_dir_ang;
      this.last_plain_ang;
      this.isDragging=false;

      this.dirIndicator;
      this.dirIndicator_colors = [];
      this.closestMarker;
      // this.closestDist= 10;
      this.ansPlanes = [];
      this.ansTextures = [];
      this.quesPlane;
      this.quesTextures = [];
      // this.infoPlane;
      this.resultPlane;
      this.resultTextures = [];
      this.markersOutOfView = false;
      this.correctoption = [];
      this.a_btn_vr=[];
      this.picked_mesh_vr=0;
      this.result_vr={
        correct:0,
        incorrect:0
      };
      this.isWinLoaded = false;
      this.isSceneLoaded = false;
      this.cast_ray_res=0;
      this.initEventHandlers();
      this.loadScene();
    } else {
      this.alertSupport();
    }
  }
  initEventHandlers() {
    var _this=this;
    window.addEventListener("load", function() {
      _this.engine.resize();
      d3.timeout(function(){
        window.scrollTo(0,1);
      },100);
    });

    window.addEventListener("resize", function() {
      _this.engine.resize();
      d3.timeout(function(){
        window.scrollTo(0,1);
      },100);
    });

    window.addEventListener("orientationchange", function() {
      _this.engine.resize();
      d3.timeout(function(){
        window.scrollTo(0,1);
      },100);
    });

    window.addEventListener("keydown", function(evt) {
      _this.onKeyDown(evt);
    }, false);
    window.addEventListener("keyup", function(evt) {
      _this.onKeyUp(evt);
    }, false);
  }

  loadScene() {
    this.bgm.volume=this.bgmVolume;
    // this.currentClip.volume=this.guideBgmVolume;

    for (var i=0;i<this.narratives.length;i++){
      this.narratives[i].volume = this.guideBgmVolume;
    }
    // BABYLON.Tools.LogLevels = 0;
    // BABYLON.SceneLoader.ShowLoadingScreen = false;
    this.scene = new BABYLON.Scene(this.engine);
    this.scene.clearColor = new BABYLON.Color4(0.9, 0.9, 0.9, 1);
    // this.scene.ambientColor = new BABYLON.Color3(1.0, 1.0, 1.0);

    var gravityVector = new BABYLON.Vector3(0, -9.8, 0);
    // var gravityVector = new BABYLON.Vector3(0, -10, 0);

    // var physicsPlugin = new BABYLON.CannonJSPlugin();
    // this.scene.enablePhysics(gravityVector, physicsPlugin);
    this.getLights();
    // this.getCamera();
    this.getCameras();
    this.loadMeshes();

    // this.sceneFog();


    // this.getSky();
    // this.getSkyCloud();

    // this.getShadow();

    this.run();
  }
  run() {
    var _this = this;
    this.scene.executeWhenReady(function(e){
      // console.log("scene loaded");
      _this.isSceneLoaded = true;
      if (_this.isWinLoaded){
        // console.log("win loaded first then scene");
        $("#loading-page progress").val(100);
        $("#loading-num").html("100%");
        $("#loading-page").animateCss("fadeOut","out",function(){
          // $("#wrapper-page").css("overflow-y","auto");
        });

        if (_this.isMobile && window.innerWidth < 768) {
          $(".mobilefirst").animateCss("fadeIn", "in");
        } else {
          $(".text .enter,.text .play").css("position", "absolute");
          $(".textarea").css("display", "block");
        }
        _this.assign_height_box();
        _this.assign_height_main();
        $(".content-canvas").height(window.innerHeight);

        _this.showAddToHomeBubble();
      }


      _this.executeWhenReady(e);
    });
    this.scene.registerBeforeRender(function() {
      var delta = _this.engine.getDeltaTime() / 1000.0;

      _this.time += delta;
      _this.update(delta, _this.time);
    });
    this.scene.onPointerDown=function(e){
      _this.pointerMouseDown(e);
    };
    this.scene.onPointerMove=function(e){
      _this.pointerMouseMove(e);
    };
    this.scene.onPointerUp=function(e){
      _this.pointerMouseUp(e);
      _this.execute_selection_answer();
    };


    this.engine.runRenderLoop(function() {
      if (!_this.isInScene) return;

      if (_this.scene) {
        // console.log(Math.floor(_this.engine.getFps()));
        _this.scene.render();
        if(_this.is_automatic===0){
          _this.checkInput();
          _this.moveDrone();
        }
        if (_this.is_guideMode===1){
         _this.updateDroneAnimRotation();
         _this.checkAnimFrame();
         _this.checkSoundStartPlay();
       }else{
         _this.updateArrows();
       }
        _this.rotateHelice();
      }
    });
  }
  update(delta,time){
    if(this.is_automatic===0){
      this.updateMarker(delta,time);

    }
    if(this.is_vr){
      this.castRay(this.crosshair);

      this.checkSoundStartPlay();
    }
    // this.updateCamera1();
    this.updateCamera2();
  }
  executeWhenReady(e) {
    var _this=this;
    _this.getGroundCollision();
    _this.createIcons();
    _this.cameraLookPoint = new BABYLON.Mesh.CreateBox("camLookPoint", 0.05, _this.scene);
    _this.cameraLookPoint.parent = _this.a_drone[0];
    _this.cameraLookPoint.position.y += 0.05;
    _this.cameraLookPoint.visibility = 0;
    _this.getShadow1();
    _this.a_drone[0].position = _this.start_location;
    _this.a_drone[0].rotation.y+=Math.PI;
    // if (_this.is_guideMode ===0){
    //   _this.getDirArrows();
    // }

    // var VJC = new BABYLON.VirtualJoystick("VJC", BABYLON.Vector3.Zero(), this.scene);

    // this.scene.activeCamera = VJC;

    // var VJC = new BABYLON.VirtualJoysticksCamera("VJC", newScene.activeCamera.position, newScene);
    // VJC.rotation = newScene.activeCamera.rotation;
    //  newScene.activeCamera.attachControl(canvas);

    // vrCamera.setCameraRigMode(BABYLON.Camera.RIG_MODE_STEREOSCOPIC_SIDEBYSIDE_PARALLEL,  { interaxialDistance: 0.0637});
  }
  alertSupport() {
    // alert("Hello\nThis graphic only support Internet Explorer 11+/Firefox 4+/Google Chrome 9+/ Opera 15+.\n Please update your browser");
    alert("Hi there,\n our interactive graphic can only been seen on Internet Explorer 11+, Firefox 4+, Google Chrome 9+ and Opera 15+. Please update your browser to enjoy the full experience.");
  }
}
