// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];
//
// a_test.forEach(d => {
//   console.log(d);
// });

$(document).ready(function() {
  $("#st-title").fadeIn(1000).removeClass("hidden");
  $("#drone").delay(1000).fadeIn(300);
  if ($(window).width() < 770) {
    $(".mobilefirst").delay(1200).fadeIn(1000).removeClass("hidden");
  } else {
    $(".text").delay(1500).fadeIn(1000).removeClass("hidden");
  }
});

//For guided tour
$(document).ready(function() {
  $(".enter").click(function() {
    $("#box").delay(500).queue(function() {
      $("#box").addClass('transition');
    });
    $("#drone").delay(800).fadeOut(500);
    $(".st-title").delay(800).fadeOut(500);
    $(".textarea").delay(800).fadeOut(500);
    $(".st-content-footer").delay(800).fadeOut(500);
    $("#clouds").hide();
    $(".popup").delay(1500).fadeIn(1000);
  });
});

//For free and easy play
$(document).ready(function() {
  $(".play").click(function() {
    $("#box").delay(500).queue(function() {
      $("#box").addClass('transition');
    });
    $("#drone").delay(800).fadeOut(500);
    $(".st-title").delay(800).fadeOut(500);
    $(".textarea").delay(800).fadeOut(500);
    $(".st-content-footer").delay(800).fadeOut(500);
    $("#clouds").hide();

    if ($(window).width() < 770) {
      $(".popup2").delay(1500).fadeIn(2000);
    } else {
      $(".button").delay(1500).fadeIn(1000);
      $(".instructions").delay(1500).toggle("slide");
    }
  });
});

$(document).ready(function() {
  $(".button").click(function() {
    $(".instructions").toggle("slide");
  });
});

$(function() {
  $('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
    e.preventDefault();
  });
});

$(document).ready(function() {
  $(".no").click(function() {
    $(".mobilefirst").fadeOut(500);
    $(".textarea").fadeIn(2000);
    $(".text").fadeIn(2000);
  });
});

$(document).ready(function() {
  $(".yes").click(function() {
    $("#clouds").hide();
    $(".mobilefirst").fadeOut(500);
    $(".st-title").fadeOut(500);
    $("#drone").fadeOut(500);
    $(".mobilesecond").delay(1000).fadeIn(1000);
    $(".mobilesecond").delay(3500).fadeOut(500);
    $(".tour").delay(5000).fadeIn(2000);
    $(".popup").delay(5000).fadeIn(2000);
  });
});
