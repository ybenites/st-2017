import $ from 'jquery';

import Vue from 'vue';
import App from './../template/page.vue';

import * as d3 from "d3";

var debug = process.env.NODE_ENV === "development";

var a_path=window.location.href.split("/");
var file_route=a_path[a_path.length-1];
file_route=file_route.split("?")[0].split("#")[0].trim();
if(file_route==="")file_route="index.html";

var json_pages_html = {
  "virtual-urban-planning": "http://graphics.straitstimes.com/st-get-file-json/1ceL9HAk4YXw8LZWzUkbFrXqbRIex94sKKJxMz5ZjHsE",
  "health-ageing": "http://graphics.straitstimes.com/st-get-file-json/1UsJiyDy3lIeuH06TF9nqiOv8pMKMFmYLo1y9so24MTc",
  "environment-city-in-a-garden": "http://graphics.straitstimes.com/st-get-file-json/1QINCn4qm2HcCnTqUO0WOB3aR7NEAVT3UiR4tydeuIww",
  "transport-automation": "http://graphics.straitstimes.com/st-get-file-json/1kITZ10oBaIQ53hbSjWnPw6EMw7b54m47biC3BcB0mtU",
  "high-tech-security": "http://graphics.straitstimes.com/st-get-file-json/1dW7Z00pd_NcCJzesG8cOp4wUehEqbuWJr6uXJ4gz7e0",
  "digital-government-services": "http://graphics.straitstimes.com/st-get-file-json/1cAk3J8b5baXj-sPyMd7Orechl5ZT6mTm8Y8U23pGI24"
};

var json_pages_html2 = {
  "virtual-urban-planning":"virtual",
  "health-ageing":"health",
  "environment-city-in-a-garden":"environment",
  "transport-automation":"transport",
  "high-tech-security":"security",
  "digital-government-services":"digital"
};
// var page=Object.keys(process.env.PAGES).find(d=>`${d}.html`===file_route);
// if(page===undefined) alert("fix url");
// var url_data=process.env.PAGES[page];

var page=Object.keys(json_pages_html).find(d=>`${d}.html`===file_route);
if(page===undefined) alert("fix url");
var url_data=json_pages_html[page];
page=json_pages_html2[page];


new Vue({
  el: '#st-content-graphic',
  render: function(h) {
    return h(App, {
      props: {
        date:this.date,
        url:window.location.href,
        loading: this.loading,
        menu:this.menu,
        credits:this.credits,
        next_chapter:this.next_chapter,
        header:this.header,
        article:this.article,
        fullHeight:this.fullHeight,
        page:page
      }
    });
  },
  data: {
    date:false,
    loading: false,
    credits:false,
    menu:[],
    next_chapter:false,
    header:false,
    article:false,
    fullHeight: window.innerHeight
  },
  methods: {
    fetchData:function(){
      var _this = this;
      this.loading = true;
      $.ajax({
        url: url_data,
        type: "POST",
        cache: false,
        dataType: "json",
        success: function(response) {
          if(response.date!==undefined){
            _this.date=response.date;
          }
          if(response.header!==undefined){
            _this.header=response.header;
          }
          if(response.article!==undefined){

            _this.article=response.article;
          }
          if(response.credits!==undefined){
            _this.credits=response.credits;
          }
          if(response.next_chapter!==undefined){
            _this.next_chapter=response.next_chapter;
          }
          if(response.menu!==undefined){
            _this.menu=response.menu;
          }
          _this.loading = false;
        }
      });
    },
    handleResize:function(event) {
      this.fullHeight = window.innerHeight - document.getElementsByClassName("st-content-menu")[0].clientHeight;
    }
  },
  created: function() {
    this.fetchData();
  },
  beforeDestroy: function () {
    window.removeEventListener('resize', this.handleResize);
  },
  mounted:function() {
    this.fullHeight = window.innerHeight - document.getElementsByClassName("st-content-menu")[0].clientHeight;
    window.addEventListener('resize', this.handleResize);
  }
});
