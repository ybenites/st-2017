import $ from 'jquery';
// var d3 = Object.assign({}, require("d3-selection"), require("d3-transition"), require("d3-geo"), require("d3-queue"), require("d3-collection"), require("d3-dispatch"), require("d3-dsv"), require("d3-request"),require("d3-array"),require("d3-timer"));
var d3 = Object.assign({}, require("d3-selection"),require("d3-transition"),require("d3-timer"));

var fbAppId = 748050775275737;
var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
// Additional JS functions here
window.fbAsyncInit = function() {
  FB.init({
    appId: fbAppId, // App ID
    status: true, // check login status
    cookie: true, // enable cookies to allow the
    // server to access the session
    xfbml: true, // parse page for xfbml or html5
    // social plugins like login button below
    version: 'v2.0', // Specify an API version
  });
  // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var width_screen = $(window).width();
var height_screen = $(window).height();
$(function(){
  width_screen = $(window).width();
  height_screen = $(window).height();
  /*start set values to footer*/
  var height_footer = $("footer.st-content-footer").outerHeight();
  // $("body").css("margin", "0 0 " + height_footer + "px");
  // $("body").css("padding", "0 0 " + height_footer + "px");
  $(".scrollContent").css("padding-bottom", height_footer + "px");
  /*end set values to footer*/

  if ($(".st-button-menu-mobile").length > 0) {
    $(".st-button-menu-mobile").on("click", function() {
      $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
      $(".st-content-menu-fixed").toggleClass('st_dialogIsOpen');
      eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
    });
  }

  if ($(".modal-menu-mobile").length > 0) {
    var elem = document.querySelector('.modal-menu-mobile');
    elem.addEventListener('touchstart', function(event) {
      startY = event.touches[0].pageY;
      startTopScroll = elem.scrollTop;
      if (startTopScroll <= 0)
        elem.scrollTop = 1;
      if (startTopScroll + elem.offsetHeight >= elem.scrollHeight)
        elem.scrollTop = elem.scrollHeight - elem.offsetHeight - 1;
      }
    , false);
  }

  if ($(".st-social-desktop").length > 0) {
    $(".st-social-desktop img:nth(0),.st-social-mobile img:nth(0)").on("click", function(e) {
      e.preventDefault();
      FB.ui({
        method: 'feed',
        link: window.location.href,
      }, function(response) {

      });
    });

    $(".st-social-desktop img:nth(1),.st-social-mobile img:nth(1)").on("click", function(e) {

      // code share twitter
      e.preventDefault();
      var via = 'STcom';

      var a_path=window.location.href.split("/");
      var file_route=a_path[a_path.length-1];
      file_route=file_route.split("?")[0].split("#")[0].trim();
      if(file_route==="")file_route="index.html";

      var json_pages_html2 = {
        "virtual-urban-planning":{
          text:"Singapore’s urban planning gets smarter with 3D model Virtual Singapore %23smartnation",
          url:"http://str.sg/smartnationurban"
        },
        "health-ageing":{
          text:"Robots, drones could be the health sector’s solution for Singapore’s ageing population. %23smartnation ",
          url:"http://str.sg/smartnationhealth"
        },
        "environment-city-in-a-garden":{
          text:"How technology helps NParks keep track of Singapore’s 7m roadside trees %23smartnation",
          url:"http://str.sg/smartnationgarden"
        },
        "transport-automation":{
          text:"No hover cars yet, but automation is coming, from driverless shuttles to all-in-one app %23smartnation",
          url:"http://str.sg/smartnationtransport"
        },
        "high-tech-security":{
          text:"Real-time facial recognition, cameras with AI - keeping Singapore safe goes smart tech %23smartnation",
          url:"http://str.sg/smartnationsecurity"
        },
        "digital-government-services":{
          text:"Tackling e-payments and a national digital ID are first steps towards Singapore Govt’s digital dream",
          url:"http://str.sg/smartnationdigitalgovt"
        }
      };

      var page=Object.keys(json_pages_html2).find(d=>`${d}.html`===file_route);
      if(page===undefined) alert("fix url");
      page=json_pages_html2[page];


      share_twitter(page.text, via, page.url);
      return false;
    });
  }
});


// Listen for orientation changes
var settime_wpo;
window.addEventListener("orientationchange", function() {
  if (settime_wpo)
    clearTimeout(settime_wpo);
  settime_wpo = setTimeout(function() {
    clearTimeout(settime_wpo);
    update_parameters();
  }, 100);
}, false);

var settime,
  settime_wp;
$(window).on("resize", function() {
  if (settime_wp)
    clearTimeout(settime_wp);
  settime_wp = setTimeout(function() {
    clearTimeout(settime_wp);
    if (!is_mobile()) {
      update_parameters();
    }
  }, 100);
});

function share_face_book(image, name, description) {
  FB.ui({
    method: 'feed',
    link: window.location.href,
    caption: 'www.straitstimes.com',
    picture: image,
    name: name,
    description: description
  });
}

function share_twitter(text, via, url) {
  window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
}

function eventCloseMenu(event) {
  if (event) {
    $(".st_menu_mobile").css('right', '10px');

    d3.select(".first_line").transition().duration(500).attr("x1", 12.8).attr("y1", 12.2).attr("x2", 23.5).attr("y2", 22.8);
    d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 0).attr("x2", 0);
    d3.select(".second_line").transition().duration(500).attr("x1", 12.9).attr("y1", 22.9).attr("x2", 23.4).attr("y2", 12.1);
    $('.st_content_menu_fixed').hide().slideDown('500').addClass('fixed_menu_mobile');
    $('body').css({'overflow': 'hidden', 'position': 'relative'});
  } else {
    $(".st-menu-mobile").css('right', '0');
    d3.select(".first_line").transition().duration(500).attr("x1", 10.5).attr("y1", 13.2).attr("x2", 26.1).attr("y2", 13.2);
    d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 1).attr("x2", 26.1);
    d3.select(".second_line").transition().duration(500).attr("x1", 10.5).attr("y1", 21.9).attr("x2", 26.1).attr("y2", 21.9);
    $('.st_content_menu_fixed').slideUp('500', function() {
      $(this).show().removeClass('fixed_menu_mobile');
    });
    $('body').removeAttr('style');
    // $("body").css("margin", "0 0 " + height_footer + "px");
    // $("body").css("padding", "0 0 " + height_footer + "px");
  }
}

function update_parameters() {
  width_screen = $(window).width();
  height_screen = $(window).height();
  var height_footer = $("footer.st-content-footer").outerHeight();
  // $("body").css("margin", "0 0 " + height_footer + "px");
  // $("body").css("padding", "0 0 " + height_footer + "px");
  $(".scrollContent").css("padding-bottom", height_footer + "px");
}

function is_mobile() {
  return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
}
$.fn.extend({
  animateCss: function(animationName, type) {
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    var copy_this = this;
    if (type !== undefined) {
      if (type === 'in') {
        d3.timeout(function() {
          copy_this.removeClass('none');
        }, 100);
      }
    }
    this.addClass('animated ' + animationName).one(animationEnd, function() {
      copy_this.off();
      if (type !== undefined) {
        if (type === 'out')
          copy_this.addClass("none");
        }
      copy_this.removeClass('animated ' + animationName);
    });
  }
});
// $.ajaxSetup({
//   beforeSend: function() {
//     $('.loading-page').fadeIn();
//   },
//   complete: function() {
//     $('.loading-page').fadeOut();
//   },
//   success: function() {
//     $('.loading-page').fadeOut();
//   }
// });
