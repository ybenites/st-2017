import $ from "jquery";

import * as d3 from "d3";

import enableInlineVideo from "iphone-inline-video";

import swipe from "jquery-touchswipe";

import ScrollMagic from "scrollmagic";
import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap";
import "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators";
// import fix_svg from "./libraries/fix-svg-size";

// import get_x from "./libraries/get-svg";
var f_click = 0;
var a_videos = [];

var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;


$(function() {
    $(".blockquote").each(function() {
        var quoteHeight = $(this).height();
        // alert(quoteHeight);
        $(this).siblings(".quoteBG").height(quoteHeight * 0.7);
    });
});

$(function() {


    // if ($('#card3').visible(true)) {
    //     // The element is visible, do something
    //     alert("visible");
    // } else {
    //     // The element is NOT visible, do something else
    //     alert("  not visible");
    //
    // }
    // var countup = 0,
    //     countdown = 0,
    //     divindex = 3;
    // var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel"; //FF doesn't recognize mousewheel as of FF3.x
    // $('#wrapper-page').bind(mousewheelevt, function(e) {
    //
    //     var evt = window.event || e; //equalize event object
    //     evt = evt.originalEvent ? evt.originalEvent : evt; //convert to originalEvent if possible
    //     var delta = evt.detail ? evt.detail * (-40) : evt.wheelDelta; //check for detail first, because it is used by Opera and FF
    //
    //     if (delta > 0) {
    //         //scroll up
    //         countup++;
    //         // $(".videocontent").html(countup);
    //         // alert("scroll up " + countup);
    //         // $("#longformSection").fadeOut();
    //         // $("#card1").fadeIn();
    //         // $("#card3").fadeIn();
    //         $("#card3").css("display", "block");
    //
    //     } else {
    //         //scroll down
    //         countdown++;
    //         $("#card3").css("display", "none");
    //         // $("#card2").fadeOut();
    //         // $("#card1").fadeOut();
    //         // $("#longformSection").fadeIn();
    //         // $(".videocontent").html("scroll down " + countdown);
    //         // alert("scroll down " + countdown);
    //
    //     }
    // });

    // init ScrollMagic
    // var controller = new ScrollMagic.Controller({
    // globalSceneOptions: {
    //     triggerHook: 'onLeave'
    // }
    // });

    // var pinSelectIcons = new ScrollMagic.Scene({
    //         triggerElement: ".iconstoselect",
    //         triggerHook: 0,
    //     })
    //     .setPin(".iconstoselect")
    //     .addIndicators()
    //     .addTo(controller);
    // $(".cardSection").each(function() {
    //
    //     new ScrollMagic.Scene({
    //             triggerElement: this,
    //             duration: 300
    //                 // triggerHook: 0,
    //         })
    //         .loglevel(3)
    //         .setPin(this)
    //         .addIndicators()
    //         .addTo(controller);
    //
    // });

    // var scenepin = new ScrollMagic.Scene({
    //         triggerElement: "#card2",
    //         // triggerHook: 0,
    //         // duration: 300,
    //     })
    //     .addTo(controller)
    //     .addIndicators({
    //         name: "pin"
    //     })
    //     .setPin("#card2");
    //
    // var card2 = TweenMax.fromTo('#card2', 1, {
    //     opacity: 0
    // }, {
    //     opacity: 1
    // });
    // var card3 = TweenMax.fromTo('#card3', 1, {
    //     opacity: 0
    // }, {
    //     opacity: 1
    // });
    //
    // var scene2 = new ScrollMagic.Scene({
    //         triggerElement: "#card2",
    //         triggerHook: 0.9,
    //     })
    //     .setTween(card2)
    //     .addTo(controller)
    //     .addIndicators();
    // var scene3 = new ScrollMagic.Scene({
    //         triggerElement: "#card3",
    //         triggerHook: 0.9,
    //     })
    //     .setTween(card3)
    //     .addTo(controller)
    //     .addIndicators();
    // var hideAni = TweenMax.to('#a2', 1, {
    //     autoAlpha: 0
    // });
    // var hideAni2 = TweenMax.to('#a3', 1, {
    //     autoAlpha: 0
    // });

    // var myScrollScene0 = new ScrollMagic.Scene({
    //         triggerElement: "#pinnedElement",
    //         duration: 300,
    //         triggerHook: 0,
    //     })
    //     .setTween(hideAni0)
    //     .addTo(controller)
    //     .addIndicators({
    //         zindex: 1
    //     }).setPin("#pinnedElement");
    //
    // var myScrollScene = new ScrollMagic.Scene({
    //         triggerElement: "#pinnedElement",
    //         duration: 300,
    //         triggerHook: 0,
    //     })
    //     .setTween(hideAni)
    //     .addTo(controller)
    //     .addIndicators({
    //         zindex: 1
    //     }).setPin("#pinnedElement");
    //
    // var myScrollScene2 = new ScrollMagic.Scene({
    //         triggerElement: "#pinnedElement",
    //         duration: 600,
    //         triggerHook: 0
    //     })
    //     .setTween(hideAni2)
    //     .addTo(controller)
    //     .addIndicators({
    //         zindex: 1
    //     });
    // .setPin("#pinnedElement");


    // build a scene
    // var scene = new ScrollMagic.Scene({
    //         triggerElement: '#card2'
    //     })
    //     .setClassToggle('#card2', "fade-in")
    //     // .addIndicators()
    //     .addTo(controller);
});
$(function() {
    // if (isMobile) {
    //     $(window).on("touchstart click", function(e) {
    //         if (f_click === 0 && a_videos.length > 0) {
    //             f_click = 1;
    //             a_videos.forEach(function(v) {
    //                 v.play();
    //                 v.pause();
    //             });
    //         }
    //     });
    // }
    // $('video').each(function() {
    //     enableInlineVideo(this);
    //     // video.addEventListener('touchstart', function () {
    //     this.play();
    //     // });
    //     // a_videos.push(this);
    // });
    // $(".videoClass").each(function() {
    //     var f_video = $(this).find("video");
    //
    //     if (f_video.length > 0) {
    //         f_video[0].play();
    //     }
    // });
    // if (isMobile === true) {
    //     $('#card1').swipe({
    //         swipeLeft: function(event, direction, distance, duration, fingerCount) {
    //             $(this).stop().animate({
    //                 left: '-800px',
    //             });
    //         }
    //     });
    // }
});
