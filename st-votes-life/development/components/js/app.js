var d_small = 400;
var d_medium = 768;
var d_normal = 992;
var d_large = 1280;
// import "./libraries/select2.min.js";
// import "./libraries/select2.js";
// import "select2";
import "./libraries/image-picker.min.js";







var info = {};
$(document).ready(function() {
    $(".st_menu_mobile").on('click', function() {
        $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
        $(".st_content_shared_social").toggleClass('st_dialogIsOpen');

        eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));
    });

    $(".content_subtitle_menu .subtitle_menu_mobile a").on('click', function(e) {
        var index_a = $(this).parents('.subtitle_menu_mobile').index();
        e.preventDefault();
        $(".modal-menu-mobile").toggleClass('st_dialogIsOpen');
        $(".st_content_shared_social").toggleClass('st_dialogIsOpen');

        eventCloseMenu($(".modal-menu-mobile").hasClass('st_dialogIsOpen'));

        $(".content_subtitle_menu .subtitle_menu_mobile a").css('color', '#999');
        $(this).css('color', '#333');

        scroll_bodypage(10, index_a);
    });

    d3.select('.close_modal_graphic_sub2').on('click', function() {
        $('body').css({
            'overflow': 'auto',
            'position': 'relative'
        });
        $(".modal_graphic_sub2").toggleClass('st_modal_eje_y');
    });

    $(".content_video_desktop").waypoint(function(dir) {
        console.log();
        if(dir==="down"){
            $(this).addClass('fix_video');
        }else if(dir==="up"){
            $(this).removeClass('fix_video');
        }
    }, {
        offset: function() {
            return 50;
        }
    });


    $('#accordion').find('.accordion-toggle').click(function(){

      //Expand or collapse this panel
      $(this).next().slideToggle('fast');

      //Hide the other panels
      $(".accordion-content").not($(this).next()).slideUp('fast');

    });




    $(".content_full_header,.cover_header").css('min-height', ($(window).height() - 50) + 'px');
    $(window).on('resize', function() {
        var height_screen = $(window).height();
        var time_out = setTimeout(function() {
            $(".content_full_header,.cover_header").css('min-height', (height_screen - 50) + 'px');
            clearTimeout(time_out);
        }, 200);
    });


    $(".image-hero").imagepicker({
          show_label  : true
        });

        $('li').unbind();
        var imageArray = $( ".image_picker_image" ).toArray();
         $('#labelon1').insertBefore(imageArray[0]);
         $('#labelon2').insertBefore(imageArray[1]);
         $('#labelon3').insertBefore(imageArray[2]);
         $('#labelon4').insertBefore(imageArray[3]);
         $('#labelon5').insertBefore(imageArray[4]);

        $(".thumbnail").click(function(){
        $(".thumbnail").not('.selected').find( ".ticker" ).remove();
        if($(this).hasClass("selected")) {
          // $(this).children('.image_picker_image').prepend("<img class='ticker' style='border:none;' src='images/selected-tick-icon.svg' alt=''>");
          $("<img class='ticker' style='border:none;' src='images/selected-tick-icon.svg' alt=''>" ).insertBefore( $(this).find( ".image_picker_image" ) );
        } else {
          $(this).not('.selected').find( ".ticker" ).remove();
        }

        });

        $( ".image-hero" ).on("change", function() {
          $(".thumbnail").not('.selected').find( ".ticker" ).remove();
          if($(".thumbnail").hasClass("selected")) {
            $("<img class='ticker' style='border:none;' src='images/selected-tick-icon.svg' alt=''>" ).insertBefore( $(".selected").find( ".image_picker_image" ) );
          } else {
            $(".thumbnail").not('.selected').find( ".ticker" ).remove();
          }
        });



    $('.image-hero').select2({
      width: "100%",
      allowClear: true,
      placeholder: "Click to select",
      minimumResultsForSearch: Infinity
    });

});
