var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
  FB.init({
    appId: fbAppId, // App ID
    status: true, // check login status
    cookie: true, // enable cookies to allow the
    // server to access the session
    xfbml: true, // parse page for xfbml or html5
    // social plugins like login button below
    version: 'v2.0', // Specify an API version
  });

  // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



$(".st_lazy_load").waypoint(function(dir) {
  if (dir === 'up') {

  } else if (dir === 'down') {
    d3.select(this).transition().duration(1000).delay(500).style('opacity', 1);
  }
}, {
  offset: function() {
    return $.waypoints('viewportHeight');
  }
});


$(document).ready(function(){
  $(".btn_shared_face").on('click', function(e) {
      e.preventDefault();
      var image = 'http://graphics.straitstimes.com/STI/STIMEDIA/facebook_images/lta-2017/lta-2017.png';
      var name = "Vote for your favourite production and win a hotel stay at M Social Singapore";
      var description = "Vote for your favourite production & win a hotel stay at M Social Singapore before March 15 http://str.sg/lta17vote";

      share_face_book(image, name, description);
      return false;
  });

  $(".btn_shared_twitter").on('click', function(e) {

      e.preventDefault();
      var text = "Vote for your favourite production %26 win a hotel stay at M Social Singapore before March 15 %23sgtheatre";
      var via = 'STcom';
      var url = 'http://str.sg/lta17vote';
      share_twitter(text, via, url);
      return false;
  });
});

// var elem = document.querySelector('.modal-menu-mobile');
// elem.addEventListener('touchstart', function(event) {
//     startY = event.touches[0].pageY;
//     startTopScroll = elem.scrollTop;

//     if (startTopScroll <= 0)
//         elem.scrollTop = 1;

//     if (startTopScroll + elem.offsetHeight >= elem.scrollHeight)
//         elem.scrollTop = elem.scrollHeight - elem.offsetHeight - 1;
// }, false);


d3.select('.content_row_down g').transition().duration(500).delay(2000).attr('transform', 'translate(0,7)').each('end', row_gotoup);
d3.select(".content_row_down svg").on('click', function() {
  scroll_bodypage(50, 0);
});


$(".icon_close_mobile").on("click", function() {
  $(".modal_detect_mobile").removeClass('true_detect_ipad');
  var timeout = setTimeout(function() {
    $(".modal_detect_mobile").hide();
  }, 500);
});

function share_face_book(image, name, description) {
  FB.ui({
    method: 'feed',
    link: window.location.href,
    caption: 'www.straitstimes.com',
    picture: image,
    name: name,
    description: description
  });
}

function share_twitter(text, via, url) {
  window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
}

function detect_ipad_android() {
  var img_so = $(".icon_so img");
  var link_so = $(".icon_so a");
  if ((/iPad/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
    img_so.attr('src', 'images/detect_ipad/Apple-store.png');
    link_so.attr('href', 'https://itunes.apple.com/sg/app/the-straits-times-star/id886065856?mt=8');
    $(".modal_detect_mobile").addClass('true_detect_ipad');
  } else if ((/Android/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
    img_so.attr('src', 'images/detect_ipad/Google-store.png');
    link_so.attr('href', 'https://play.google.com/store/apps/details?id=com.sph.ststar');
    $(".modal_detect_mobile").addClass('true_detect_ipad');
  } else {
    $(".modal_detect_mobile").hide();
  }
}

function row_gotoup() {
  d3.select('.content_row_down g').transition().duration(700).attr('transform', 'translate(0,0)').each('end', row_gotodow);
}

function row_gotodow() {
  d3.select('.content_row_down g').transition().duration(700).attr('transform', 'translate(0,7)').each('end', row_gotoup);
}

function detect_touch_any_body() {
  $(document).on('touchstart', function(e) {});
}

function detected_tooltip() {
  d3.selectAll('.tooltip_subtitle').on('mouseover', function() {
    d3.select(this).classed('mouseover_tooltip_subtitle', true);
  }).on('mouseout', function() {
    d3.select(this).classed('mouseover_tooltip_subtitle', false);
  });
}

function animateFirstStep1() {
  d3.selectAll(".content_group_icon .icon_sub2_active .efect_circle").attr('opacity', 0);
  d3.selectAll(".content_group_icon svg:not(.icon_sub2_active) .efect_circle")
    .transition().attr("opacity", 0.01)
    .duration(3000)
    .attr("r", 30)
    .each("end", animateSecondStep1);
}



function animateSecondStep1() {
  d3.selectAll(".content_group_icon svg:not(.icon_sub2_active) .efect_circle")
    .transition()
    .delay(300)
    .duration(0.0001)
    .attr("r", 24)
    .attr("opacity", 1)
    .each("end", animateFirstStep1);
}

function scroll_bodypage(height, index) {
  $('.body_division').waypoint('disable');
  $('.body_division_head').waypoint('disable');
  $('.body_division_footer').waypoint('disable');

  $(window).scrollTo($($('.body_division').get(index)), 1500, {
    offset: -height
  });

  var tim = setTimeout(function() {
    $('.body_division').waypoint('enable');
    $('.body_division_head').waypoint('enable');
    $('.body_division_footer').waypoint('enable');
    clearTimeout(tim);
  }, 1500);
}

function createAnimateCircle(all_etiq, time_total, delay) {
  var all = d3.selectAll(all_etiq);
  var total_elements = all.size();
  var time_unit = (time_total === 0) ? 1000 : time_total / total_elements;

  all.each(function() {
    var path_length = (2 * Math.PI * d3.select(this).attr('r')) + 2;
    d3.select(this).attr('stroke-dasharray', path_length).attr('stroke-dashoffset', path_length);
  });
  var delay_ini = setTimeout(function() {
    all.each(function(dd, i) {
      d3.select(this).transition().delay(i * time_unit).duration(2000).attr('stroke-dashoffset', 0);
    });
    clearTimeout(delay_ini);
  }, delay);


}

function createAnimationOpacity(all_etiq, time_total, delay) {
  var all = d3.selectAll(all_etiq);
  var total_elements = all.size();
  var time_unit = (time_total === 0) ? 1000 : time_total / total_elements;

  all.attr('opacity', 0);
  var delay_ini = setTimeout(function() {
    all.each(function(dd, i) {
      d3.select(this).transition().duration(500).delay(i * time_unit).attr('opacity', 1);
    });
    clearTimeout(delay_ini);
  }, delay);
}

function createAnimationLine(all_etiq, time_total) {
  var all = d3.selectAll(all_etiq);
  var total_elements = all.size();
  var time_unit = (time_total === 0) ? 1000 : time_total / total_elements;
  all.each(function(dd, i) {
    var path_length = this.getTotalLength();
    d3.select(this).attr('stroke-dasharray', path_length).attr('stroke-dashoffset', path_length)
      .transition().delay(i * time_unit).attr('stroke-dashoffset', 0);
  });
}

function eventCloseMenu(event) {
  if (event) {
    $(".st_menu_mobile").css('right', '10px');

    d3.select(".first_line").transition().duration(500).attr("x1", 12.8).attr("y1", 12.2).attr("x2", 23.5).attr("y2", 22.8);
    d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 0).attr("x2", 0);
    d3.select(".second_line").transition().duration(500).attr("x1", 12.9).attr("y1", 22.9).attr("x2", 23.4).attr("y2", 12.1);
    $('.st_content_menu_fixed').hide().slideDown('500').addClass('fixed_menu_mobile');
    $('body').css({
      'overflow': 'hidden',
      'position': 'relative'
    });
  } else {
    $(".st_menu_mobile").css('right', '0');
    d3.select(".first_line").transition().duration(500).attr("x1", 10.5).attr("y1", 13.2).attr("x2", 26.1).attr("y2", 13.2);
    d3.select(".menu_mobile_line_center").transition().duration(500).attr('opacity', 1).attr("x2", 26.1);
    d3.select(".second_line").transition().duration(500).attr("x1", 10.5).attr("y1", 21.9).attr("x2", 26.1).attr("y2", 21.9);
    $('.st_content_menu_fixed').slideUp('500', function() {
      $(this).show().removeClass('fixed_menu_mobile');
    });
    $('body').css('overflow', 'auto');
  }
}

function cloneToDoc(node, doc) {
  if (!doc) doc = document;
  var clone = doc.createElementNS(node.namespaceURI, node.nodeName);
  for (var i = 0, len = node.attributes.length; i < len; ++i) {
    var a = node.attributes[i];
    if (/^xmlns\b/.test(a.nodeName)) continue; // IE can't create these
    clone.setAttributeNS(a.namespaceURI, a.nodeName, a.nodeValue);
  }
  for (var ii = 0, len2 = node.childNodes.length; ii < len2; ++ii) {
    var c = node.childNodes[ii];
    clone.insertBefore(
      c.nodeType == 1 ? cloneToDoc(c, doc) : doc.createTextNode(c.nodeValue),
      null
    );
  }
  return clone;
}
