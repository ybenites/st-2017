// require("../precss/app.css");

import $ from "jquery";

// import xx from "./library/fix-svg-size";



import * as d3 from "d3";

// console.log(xx);
// console.log(d3);

// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test = [1, 2, 3, 4, 5, 6, 7, 6, 4, 4, 3, 3, 3, 6, 7, 7, 6];
//
// a_test.forEach(d => {
//   console.log(d);
// });

var text = "";
var count = 0;
var maxspeed = 200;
//
// $(document).ready(function() {
//
//   function typeit(tweet) {
//     text = tweet;
//     type();
//   }
//
//   function character(start, end, text) {
//     return text.substring(start, end);
//   }
//
//   function type() {
//     var random = Math.floor(Math.random() * maxspeed);
//     setTimeout(type, random);
//     $('p.intro').append(character(count, count + 1, text));
//     count++;
//   }
//
//   type();
//
//   typeit(" Welcome to ST’s virtual reality page. Here you can find answers to your most frequently asked questions and links to our current and future VR projects.");
//
// });

var isMobile = (function(a) {
  return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
})(navigator.userAgent || navigator.vendor || window.opera);



$(function() {
  $(".st-title").animate({
    'opacity': '1'
  }, 1000, function() {
    $(".st-deck").animate({
      'opacity': '1'
    });
  });
  /* Every time the window is scrolled ... */
  var no_of_ques = $('.contentInformation').length;
  $(window).scroll(function() {
    /* Check the location of each desired element */
    $('.contentInformation').each(function(i) {
      var _this = $(this);
      var bottom_of_object = $(this).offset().top + $(this).outerHeight() * 0.6;
      var bottom_of_window = $(window).scrollTop() + $(window).height();

      /* If the object is completely visible in the window, fade it it */
      if (bottom_of_window > bottom_of_object) {
        _this.find(".contentheading").animate({
          'opacity': '1'
        }, function() {
          $(this).find(".animate").addClass("animate_circle");
        });
        _this.find(".contentText").delay(500).animate({
          'padding-top': '0',
          'opacity': '1'
        }, 500, function() {
          if (i < no_of_ques - 1) _this.css("border-bottom", "dotted 1px #ccc");
        });
      }

    });

  });
});
// if (!isMobile) {
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 100) {
//           $("#1").animate({
//             'opacity': '1'
//           }, 500);
//           // $('#ans1').fadeIn(500);
//           // $('#text1').fadeIn(500);
//         } else {
//           $("#1").animate({
//             'opacity': '0'
//           }, 500);
//           // $('#ans1').fadeOut(500);
//           // $('#text1').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 1200) {
//           $('#ans2').fadeIn(500);
//           $('#text2').fadeIn(500);
//         } else {
//           $('#ans2').fadeOut(500);
//           $('#text2').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 1600) {
//           $('#ans3').fadeIn(500);
//           $('#text3').fadeIn(500);
//         } else {
//           $('#ans3').fadeOut(500);
//           $('#text3').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 2000) {
//           $('#ans4').fadeIn(500);
//           $('#text4').fadeIn(500);
//         } else {
//           $('#ans4').fadeOut(500);
//           $('#text4').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 2600) {
//           $('#ans5').fadeIn(500);
//           $('#text5').fadeIn(500);
//         } else {
//           $('#ans5').fadeOut(500);
//           $('#text5').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 2900) {
//           $('#ans6').fadeIn(500);
//           $('#text6').fadeIn(500);
//         } else {
//           $('#ans6').fadeOut(500);
//           $('#text6').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 3400) {
//           $('#ans7').fadeIn(500);
//           $('#text7').fadeIn(500);
//         } else {
//           $('#ans7').fadeOut(500);
//           $('#text7').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 3800) {
//           $('#ans8').fadeIn(500);
//           $('#text8').fadeIn(500);
//         } else {
//           $('#ans8').fadeOut(500);
//           $('#text8').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 4100) {
//           $('#ans9').fadeIn(500);
//           $('#text9').fadeIn(500);
//         } else {
//           $('#ans9').fadeOut(500);
//           $('#text9').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
// } else {
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 500) {
//
//           $('#ans1').fadeIn(500);
//           $('#text1').fadeIn(500);
//         } else {
//           $('#ans1').fadeOut(500);
//           $('#text1').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 900) {
//           $('#ans2').fadeIn(500);
//           $('#text2').fadeIn(500);
//         } else {
//           $('#ans2').fadeOut(500);
//           $('#text2').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 1300) {
//           $('#ans3').fadeIn(500);
//           $('#text3').fadeIn(500);
//         } else {
//           $('#ans3').fadeOut(500);
//           $('#text3').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 1700) {
//           $('#ans4').fadeIn(500);
//           $('#text4').fadeIn(500);
//         } else {
//           $('#ans4').fadeOut(500);
//           $('#text4').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 2300) {
//           $('#ans5').fadeIn(500);
//           $('#text5').fadeIn(500);
//         } else {
//           $('#ans5').fadeOut(500);
//           $('#text5').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 2600) {
//           $('#ans6').fadeIn(500);
//           $('#text6').fadeIn(500);
//         } else {
//           $('#ans6').fadeOut(500);
//           $('#text6').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 3100) {
//           $('#ans7').fadeIn(500);
//           $('#text7').fadeIn(500);
//         } else {
//           $('#ans7').fadeOut(500);
//           $('#text7').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 3500) {
//           $('#ans8').fadeIn(500);
//           $('#text8').fadeIn(500);
//         } else {
//           $('#ans8').fadeOut(500);
//           $('#text8').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
//
//   (function($) {
//     $(document).ready(function() {
//       $(window).scroll(function() {
//         if ($(this).scrollTop() > 3700) {
//           $('#ans9').fadeIn(500);
//           $('#text9').fadeIn(500);
//         } else {
//           $('#ans9').fadeOut(500);
//           $('#text9').fadeOut(500);
//         }
//       });
//     });
//   })(jQuery);
// }




// $(function() {
// 	$('a[href*=#]').on('click', function(e) {
// 		e.preventDefault();
//     newPos.left = "0";
//        newPos.top = "500";
// 		$($(this).attr('href')).offset(newPos);
// 	});
// });








// var count ;
//   $(document).on("click", '#update', function () {
//     count = "+1";
//   console.log(count);
// });
//
// $(document).on("click", '#up', function () {
//   window.location.href="database.php?uid="+count;
// });
//
//
//
//
// jQuery(document).on('click','a#download',function(){
//     jQuery('div#counter').html('Loading...') ;
//     var ajax = jQuery.ajax({
//         method : 'get',
//         url : 'database.php', // Link to this page
//         data : { 'increase' : '1' }
//     }) ;
//     ajax.done(function(data){
//         jQuery('div#counter').html(data) ;
//     }) ;
//     ajax.fail(function(data){
//         alert('ajax fail : url of ajax request is not reachable') ;
//     }) ;
// }) ;



// var moveForce = 30; // max popup movement in pixels
// var rotateForce = 20; // max popup rotation in deg
//
// $(document).mousemove(function(e) {
//     var docX = $(document).width();
//     var docY = $(document).height();
//
//     var moveX = (e.pageX - docX/2) / (docX/2) * -moveForce;
//     var moveY = (e.pageY - docY/2) / (docY/2) * -moveForce;
//
//     var rotateY = (e.pageX / docX * rotateForce*2) - rotateForce;
//     var rotateX = -((e.pageY / docY * rotateForce*2) - rotateForce);
//
//     $('video')//change this class into the class we wan to move
//         .css('left', moveX+'px')
//         .css('top', moveY+'px')
//         .css('transform', 'rotateX('+rotateX+'deg) rotateY('+rotateY+'deg)');
// });







var moveForce = 30; // max popup movement in pixels
var rotateForce = 20; // max popup rotation in deg


if (!isMobile) {
  $(document).mousemove(function(e) {
    var docX = $(document).width();
    var docY = $(document).height();

    var moveX = (e.pageX - docX / 2) / (docX / 4) * -moveForce;
    var moveY = (e.pageY - docY / 2) / (docY / 4) * -moveForce;

    var rotateY = (e.pageX / docX * rotateForce * 2) - rotateForce;
    var rotateX = -((e.pageY / docY * rotateForce * 2) - rotateForce);




    $('.introduction') //change this class into the class we wan to move
      .css('left', moveX + 'px')
      .css('top', moveY + 'px')
      .css('transform', 'rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)');
  });
} else {




}




// <?php
//
//     $counterFile = 'data/myText.txt' ;
//
//     // jQuery ajax request is sent here
//     if ( isset($_GET['increase']) )
//     {
//         if ( ( $counter = @file_get_contents($counterFile) ) === false ) die('Error : file counter does not exist') ;
//         file_put_contents($counterFile,++$counter) ;
//         echo $counter ;
//         return false ;
//     }
//
//     if ( ! $counter = @file_get_contents($counterFile) )
//     {
//         if ( ! $myfile = fopen($counterFile,'w') )
//             die('Unable to create counter file !!') ;
//         chmod($counterFile,0644);
//         file_put_contents($counterFile,0) ;
//     }
//
//
//
//
//
// ?>

// $somevar = $_GET["uid"];
// $int = (int)$somevar;
//
//
// $myfile = fopen("data/myText.txt", "w+") or die("Unable to open file!");
// $txt = $int+1;
// fwrite($myfile, $txt);
// $txt = $int+1;
// fwrite($myfile, $txt);
// fclose($myfile);
