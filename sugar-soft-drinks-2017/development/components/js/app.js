import $ from 'jquery';
var jQuery = $;

import d3 from 'd3';


// import create_slider from './libraries/slider.js';
import create_slider2 from './libraries/slider2.js';
import Swiper from "swiper";


$(document).ready(function() {
  var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    spaceBetween: 30,
  });
});



var a_domain = [];
for (let i = 0; i <= 30; i++) {
  a_domain.push(i / 2);
}

/*question 4.1*/
var correct = ['8 to 11'];
var test = create_slider2({
  tag: '.qns_4_1 .slider-qns_4',
  domain: a_domain,
  divisions: [{
    d: '#56C8CE',
    l: '#56C8CE',
    w: 427
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 110
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 241
  }],
  tooltip: {
    pos: 8,
    text: correct,
    color: '#FFF'
  },
  pointer: 1,
  end: function(number) {}
});

$(".qns_4_1 .control-tooltip-danni").hide();
$(".qns_4_1 .check-btn").on("click", function() {
  $(this).fadeOut();
  $(".qns_4_1 .control-tooltip-danni").fadeIn();
  d3.select(".qns_4_1 .slider").classed("pointer-events-none", true);
  $("#img1").attr("src", "images/img/8-teaspoon.png");
  $("#spoon").css("width", "200px");
  $(".qns_4 .qns_4_1 .answer").fadeIn();
});

/*question 4.2*/

var test = create_slider2({
  tag: '.qns_4_2 .slider-qns_4',
  domain: a_domain,
  divisions: [{
    d: '#56C8CE',
    l: '#56C8CE',
    w: 427
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 110
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 241
  }],
  tooltip: {
    pos: 7,
    text: 7,
    color: '#FFF'
  },
  end: function(number) {}
});

$(".qns_4_2 .control-tooltip-danni").hide();
$(".qns_4_2 .check-btn").on("click", function() {
  $(this).fadeOut();
  $(".qns_4_2 .control-tooltip-danni").fadeIn();
  d3.select(".qns_4_2 .slider").classed("pointer-events-none", true);
  $("#img2").attr("src", "images/img/coke-8sugar.png");
  $(".qns_4 .qns_4_2 .answer").fadeIn();
});

/*question 4.3*/

var test = create_slider2({
  tag: '.qns_4_3 .slider-qns_4',
  domain: a_domain,
  divisions: [{
    d: '#56C8CE',
    l: '#56C8CE',
    w: 427
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 110
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 241
  }],
  tooltip: {
    pos: 4,
    text: 4,
    color: '#FFF'
  },
  end: function(number) {}
});

$(".qns_4_3 .control-tooltip-danni").hide();
$(".qns_4_3 .check-btn").on("click", function() {
  $(this).fadeOut();
  $(".qns_4_3 .control-tooltip-danni").fadeIn();
  $("#img3").attr("src", "images/img/100-5sugar.png");
  d3.select(".qns_4_3 .slider").classed("pointer-events-none", true);
  $(".qns_4 .qns_4_3 .answer").fadeIn();
});

/*question 4.4*/
var test = create_slider2({
  tag: '.qns_4_4 .slider-qns_4',
  domain: a_domain,
  divisions: [{
    d: '#56C8CE',
    l: '#56C8CE',
    w: 427
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 110
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 241
  }],
  tooltip: {
    pos: 3.5,
    text: 3.5,
    color: '#FFF'
  },
  end: function(number) {}
});

$(".qns_4_4 .control-tooltip-danni").hide();
$(".qns_4_4 .check-btn").on("click", function() {
  $(this).fadeOut();
  $(".qns_4_4 .control-tooltip-danni").fadeIn();
  d3.select(".qns_4_4 .slider").classed("pointer-events-none", true);
  $("#img4").attr("src", "images/img/milo-4sugar.png");
  $(".qns_4 .qns_4_4 .answer").fadeIn();
});
/*question 4.5*/
var test = create_slider2({
  tag: '.qns_4_5 .slider-qns_4',
  domain: a_domain,
  divisions: [{
    d: '#56C8CE',
    l: '#56C8CE',
    w: 427
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 110
  }, {
    d: '#56C8CE',
    l: '#56C8CE',
    w: 241
  }],
  tooltip: {
    pos: 6.5,
    text: 6.5,
    color: '#FFF'
  },
  end: function(number) {}
});

$(".qns_4_5 .control-tooltip-danni").hide();
$(".qns_4_5 .check-btn").on("click", function() {
  $(this).fadeOut();
  $(".qns_4_5 .control-tooltip-danni").fadeIn();
  $("#img5").attr("src", "images/img/ribena-8sugar.png");
  d3.select(".qns_4_5 .slider").classed("pointer-events-none", true);
  $(".qns_4 .qns_4_5 .answer").fadeIn();
});
