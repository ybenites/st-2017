import $ from 'jquery';
import d3 from 'd3';

var create_slider = function(options) {
    var x_interval;
    var width2;
    var values_domain;
    var last_value;
    var handle, brush, g_hand_mov;
    var c_options;
    var path_color;
    var text_mark;
    var flag_click_svg = 0;
    if (options) {
        if (options.tag !== undefined) {
            c_options = options;
            var width_g = $(options.tag).width();

            var margin2 = {
                    top: 18,
                    right: 10,
                    bottom: 2,
                    left: 10
                },
                width2 = width_g - margin2.left - margin2.right,
                height = 50 - margin2.bottom - margin2.top;



            // xslider = d3.time.scale()
            //     .range([0, width2])
            //     .domain([parseDate("15-03-18 13"), parseDate("15-03-30 07")])
            //     .clamp(true);



            var domain = [0, 100];
            if (options.domain) domain = options.domain;
            values_domain = domain;

            x_interval = d3.scale.ordinal()
                .domain(domain)
                .rangePoints([0, width2], 1);



            brush = d3.svg.brush()
                .x(x_interval)
                .extent([x_interval(domain[0]), x_interval(domain[0])])
                .on("brush", brushed)
                .on("brushend", brushended);

            var svg = d3.select(options.tag).append("svg")
                .attr("width", width2 + margin2.left + margin2.right)
                .attr("height", height + margin2.top + margin2.bottom)
                .append("g")
                .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

            var eje_x_s = svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height / 2 + ")");
            eje_x_s.call(d3.svg.axis()
                .scale(x_interval)
                .orient("top")
                // .tickFormat(function(d) {
                //     return d + ' times';
                // })
                .ticks(Math.max(width2 / 75, 2))
                .tickSize(0)
                .innerTickSize(10)
            );

            eje_x_s.select(".domain").each(function() {
                this.parentNode.appendChild(this.cloneNode());
            });

            eje_x_s.selectAll(".domain").each(function(d, i) {
                var firstChild = this.parentNode.firstChild;
                if (firstChild) {
                    this.parentNode.insertBefore(this, firstChild);
                }
                if (i === 0) {
                    path_color = d3.select(this).attr('class', 'manage-color');
                }
            });

            var total_l = path_color.node().getTotalLength();
            path_color
                .attr('stroke-dasharray', total_l)
                .attr('stroke-dashoffset', total_l);


            eje_x_s.selectAll('.tick line').attr('y1', -5).attr('y2', 5);
            eje_x_s.selectAll('.tick text').attr('y', -18);
            text_mark = eje_x_s.selectAll('.tick text').attr('font-size', 16).each(function(d, i) {
                d3.select(this).classed("tick-text-number" + i, true)
            });

            // .select(function() {
            //     return this.parentNode.appendChild(this.cloneNode());
            // })
            // .attr("class", "halo");


            var slider = svg.append("g")
                .attr("class", "slider")
                .call(brush);

            slider.selectAll(".extent,.resize")
                .remove();

            slider.select(".background")
                .attr("height", height);

            // console.log(create_slider);

            handle = slider.append("circle")
                .attr("class", "handle")
                .attr("transform", "translate(0," + height / 2 + ")")
                .attr("r", 15);

            slider
                .call(brush.event)
                .transition() // gratuitous intro!
                .duration(750)
                .call(brush.extent([x_interval(domain[0]), x_interval(domain[0])]))
                .call(brush.event);

            if (options.pointer) {
                g_hand_mov = svg.append("g")
                    .attr("class", "g_hand_mov")
                    .attr("transform", "translate(" + x_interval(domain[0]) + ",-5)");

                g_hand_mov.append("polygon")
                    .attr("points", "17.3,25.5 17.2,15.2 16,14.1 14.8,14.1 13.6,15.2 13.6,32.9 12.2,32.9 8.5,29.3 6.3,29.4 5.6,30 5.7,32 12,38.3 16.3,42.4 16.4,45.8 27.5,44.9 27.5,41.4 29.6,39.3 29.8,26.6 28.7,25.5")
                    .attr('transform', 'scale(0.9)')
                    .attr('opacity', 0.85);

                var h_circle = g_hand_mov.append("circle").attr('opacity', 0.85).attr('fill', 'none').attr('stroke', '#000000')
                    .attr('stroke-width', 2).attr('stroke-miterlimit', 10).attr('cx', 15).attr('cy', 14.5).attr('r', 7.4);

                h_circle.each(circle_flash1);
                g_hand_mov.each(translate_hand1);
            }

            var resizeTimer2;
            $(window).on("resize", function() {
                clearTimeout(resizeTimer2);
                resizeTimer2 = setTimeout(function() {
                    width_g = $(options.tag).width();
                    width2 = width_g - margin2.left - margin2.right;
                    d3.select(options.tag).select("svg").attr("width", width2 + margin2.left + margin2.right);

                    // x_interval.range([0, width2]);
                    x_interval.rangePoints([0, width2], 1);
                    slider.call(brush);
                    eje_x_s.call(
                        d3.svg.axis()
                        .scale(x_interval)
                        // .tickFormat(d3.time.format("%a %d"))
                        // .tickFormat(function(d) {
                        //     return d + ' times';
                        // })
                        .orient("top")
                        .ticks(Math.max(width2 / 75, 2))
                        .tickSize(0)
                        .innerTickSize(10)
                    );
                    eje_x_s.selectAll('.tick line').attr('y1', -5).attr('y2', 5);
                    eje_x_s.selectAll('.tick text').attr('y', -18);


                    eje_x_s.select(".manage-color").remove();
                    eje_x_s.select(".domain").each(function() {
                        this.parentNode.appendChild(this.cloneNode());
                    });

                    eje_x_s.selectAll(".domain").each(function(d, i) {
                        var firstChild = this.parentNode.firstChild;
                        if (firstChild) {
                            this.parentNode.insertBefore(this, firstChild);
                        }
                        if (i === 0) {
                            path_color = d3.select(this).attr('class', 'manage-color');
                        }
                    });


                    handle.attr("cx", x_interval(last_value));
                    var total_l = path_color.node().getTotalLength();
                    path_color
                        .attr('stroke-dasharray', total_l)
                        .attr('stroke-dashoffset', total_l - x_interval(last_value));


                }, 100);
            });


            // return create_slider;
        }
    }

    function circle_flash1() {

        d3.select(this).transition().attr("opacity", 0.01)
            .duration(3000)
            .attr("r", 15)
            .each("end", circle_flash2);
    }

    function circle_flash2() {
        d3.select(this)
            .transition()
            .delay(300)
            .duration(0.0001)
            .attr("r", 7.5)
            .attr("opacity", 0.8)
            .each("end", circle_flash1);
    }

    function translate_hand1() {

        d3.select(this).transition().duration(5000)
            .attr("transform", "translate(" + x_interval(domain[domain.length - 1]) + ",-5)")
            .each('end', translate_hand2);
    }

    function translate_hand2() {
        d3.select(this).transition().duration(5000)
            .attr("transform", "translate(" + x_interval(domain[0]) + ",-5)")
            .each('end', translate_hand1)
    }

    function click_svg_content() {
        if (flag_click_svg === 0 && c_options.pointer) {
            flag_click_svg++
            g_hand_mov.remove();
        }
    }

    function brushed() {
        var value = brush.extent()[0];
        if (d3.event.sourceEvent) { // not a programmatic event
            click_svg_content()
            // value = x_interval.invert(d3.mouse(this)[0]);
            var mouse_value = d3.mouse(this)[0];
            value = mouse_value;
            if (mouse_value <= 0) value = 0;
            if (mouse_value >= width2) value = width2;

            brush.extent([value, value]);
        }
        handle.attr("cx", value);
    }

    function brushended() {
        var value = brush.extent()[0];
        var value1, value2;
        var before_value = 0;

        values_domain.forEach(function(d, count) {
            value1 = x_interval(values_domain[count]);
            value2 = x_interval(values_domain[count + 1]);

            if (count !== values_domain.length - 1) {
                if (value >= before_value && value <= (value1 / 2 + value2 / 2)) {
                    handle.attr("cx", value1);
                    last_value = values_domain[count];
                    c_options.end(values_domain[count]);

                    var total_l = path_color.node().getTotalLength();

                    path_color
                        .attr('stroke-dasharray', total_l)
                        .attr('stroke-dashoffset', total_l - value1);


                    text_mark.classed('text-mark', function() {
                        return d3.select(this).classed('tick-text-number' + count);
                    });
                }
            } else {
                if (value >= before_value) {
                    handle.attr("cx", value1);
                    last_value = values_domain[count];
                    c_options.end(values_domain[count]);

                    var total_l = path_color.node().getTotalLength();

                    path_color
                        .attr('stroke-dasharray', total_l)
                        .attr('stroke-dashoffset', total_l - value1);
                    text_mark.classed('text-mark', function() {
                        return d3.select(this).classed('tick-text-number' + count);
                    });
                }
            }

            before_value = (value1 / 2) + (value2 / 2);

        });
    }

}

module.exports = create_slider;
