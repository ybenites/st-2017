import $ from 'jquery';
import d3 from 'd3';

var create_slider = function(options) {
  var x_interval, i_scale;
  var width2;
  var values_domain;
  var last_value;
  var handle, brush, text_show, g_hand_mov;
  var c_options;
  var path_color;
  var tooltip_value;
  var flag_click_svg = 0;

  if (options) {
    if (options.tag !== undefined) {
      c_options = options;
      var width_g = $(options.tag).width();

      var margin2 = {
          top: 2,
          right: 15,
          bottom: 12,
          left: 15
        },
        width2 = width_g - margin2.left - margin2.right,
        height = 50 - margin2.bottom - margin2.top;



      // xslider = d3.time.scale()
      //     .range([0, width2])
      //     .domain([parseDate("15-03-18 13"), parseDate("15-03-30 07")])
      //     .clamp(true);



      var domain = [0, 15];
      if (options.domain) domain = options.domain;
      values_domain = domain;

      x_interval = d3.scale.ordinal()
        .domain(domain)
        .rangePoints([0, width2], 1);


      brush = d3.svg.brush()
        .x(x_interval)
        .extent([x_interval(domain[0]), x_interval(domain[0])])
        .on("brush", brushed)
        .on("brushend", brushended);

      var svg = d3.select(options.tag).append("svg")
        .attr("width", width2 + margin2.left + margin2.right)
        .attr("height", height + margin2.top + margin2.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");


      var eje_x_s = svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height / 2 + ")");
      eje_x_s.call(d3.svg.axis()
        .scale(x_interval)
        .orient("bottom")
        // .tickFormat(function(d) {
        //     return d + ' times';
        // })
        // .ticks(Math.max(width2 / 75, 2))
        .tickSize(0)
        .innerTickSize(10)
        .tickValues([0, 15])
      );

      if (!options.divisions) {

        eje_x_s.select(".domain").each(function() {
          this.parentNode.appendChild(this.cloneNode());
        });

        eje_x_s.selectAll(".domain").each(function(d, i) {
          var firstChild = this.parentNode.firstChild;
          if (firstChild) {
            this.parentNode.insertBefore(this, firstChild);
          }
          if (i === 0) {
            path_color = d3.select(this).attr('class', 'manage-color');
          }
        });

        var total_l = path_color.node().getTotalLength();
        path_color
          .attr('stroke-dasharray', total_l)
          .attr('stroke-dashoffset', total_l);
      } else {
        eje_x_s.selectAll(".domain").remove();
        d3.select(eje_x_s.node().firstChild).classed('tag_first_element', true);

        var total = d3.sum(options.divisions, function(d) {
          return d.w;
        });

        i_scale = d3.scale.linear()
          .domain([0, total])
          .range([0, x_interval(domain[domain.length - 1]) - x_interval(0)]);

        var move = x_interval(0);
        options.divisions.forEach(function(d, i) {
          var id_def = options.tag.split(".").join("-").split(" ").join("");
          var lg = eje_x_s.append('defs')
            .append('linearGradient')
            .attr('id', id_def + (i + 1))
            .attr('class', "glinear" + (i + 1));

          lg.append('stop').attr('offset', '0%').attr('stop-color', d.d);
          lg.append('stop').attr('offset', '0%').attr('stop-color', d.d);
          lg.append('stop').attr('offset', '0%').attr('stop-color', d.l);
          lg.append('stop').attr('offset', '100%').attr('stop-color', d.l);

          // console.log(i_scale(d.w));
          eje_x_s.insert("rect", ".tag_first_element")
            .attr('width', i_scale(d.w))
            .attr('height', 10)
            .attr('y', -5)
            .attr('x', move)
            .attr('fill', "url(#" + id_def + (i + 1) + ")")
            .attr('class', "rect_color" + (i + 1));
          move += i_scale(d.w);
        });
      }

      if (options.tooltip) {
        d3.select(options.tag).style("position", 'relative');

        tooltip_value = d3.select(options.tag).append("div").attr('class', 'st-tooltip-danni control-tooltip-danni');
        tooltip_value.append('div').attr('class', 'head_tooltip').text("ANSWER");
        tooltip_value.append('div').attr('class', 'body_tooltip')
          .style('color', options.tooltip.color)
          .text(options.tooltip.text + " teaspoons");


        if (x_interval(options.tooltip.pos) > (width2 / 2)) {
          tooltip_value.style({
            right: (width2 - x_interval(options.tooltip.pos) - 2) + "px"
          });
          tooltip_value.classed('st-tooltip-right', true);
        } else {
          tooltip_value.style({
            left: (x_interval(options.tooltip.pos) - 2) + "px"
          });
          tooltip_value.classed('st-tooltip-left', true);
        }


        tooltip_value.transition().delay(500).style({
          top: -($(tooltip_value.node()).outerHeight(true) + 20) + "px"
        });

        eje_x_s.append('g')
          .attr('class', 'line_answer control-tooltip-danni')
          .attr("transform", "translate(" + x_interval(options.tooltip.pos) + ",0)")
          .append("line")
          .attr("y2", 5)
          .attr("x2", 0)
          .attr('y1', -5);
      }


      eje_x_s.selectAll('.tick text').attr('y', 20);


      var slider = svg.append("g")
        .attr("class", "slider")
        .call(brush);

      slider.selectAll(".extent,.resize")
        .remove();

      slider.select(".background")
        .attr("height", height);

      // console.log(create_slider);

      handle = slider.append("circle")
        .attr("class", "handle")
        .attr("transform", "translate(0," + height / 2 + ")")
        .attr("r", 20);

      text_show = slider.append("text")
        .attr("class", "text-show")
        .attr("transform", "translate(0," + height / 2 + ")")
        .attr('y', 7)
        .attr('text-anchor', 'middle')
        .attr('pointer-events', 'none');

      slider
        .call(brush.event)
        .transition() // gratuitous intro!
        .duration(750)
        .call(brush.extent([x_interval(domain[0]), x_interval(domain[0])]))
        .call(brush.event);

      if (options.pointer) {
        g_hand_mov = svg.append("g")
          .attr("class", "g_hand_mov")
          .attr("transform", "translate(" + x_interval(domain[0]) + ",0)");
        g_hand_mov.append("polygon")
          .attr("points", "17.3,25.5 17.2,15.2 16,14.1 14.8,14.1 13.6,15.2 13.6,32.9 12.2,32.9 8.5,29.3 6.3,29.4 5.6,30 5.7,32 12,38.3 16.3,42.4 16.4,45.8 27.5,44.9 27.5,41.4 29.6,39.3 29.8,26.6 28.7,25.5")
          .attr('opacity', 0.85);

        var h_circle = g_hand_mov.append("circle").attr('opacity', 0.85).attr('fill', 'none').attr('stroke', '#000000')
          .attr('stroke-width', 2).attr('stroke-miterlimit', 10).attr('cx', 15).attr('cy', 14.5).attr('r', 7.4);

        h_circle.each(circle_flash1);
        g_hand_mov.each(translate_hand1);
      }


      var resizeTimer2;
      $(window).on("resize", function() {
        clearTimeout(resizeTimer2);
        resizeTimer2 = setTimeout(function() {
          width_g = $(options.tag).width();
          width2 = width_g - margin2.left - margin2.right;
          d3.select(options.tag).select("svg").attr("width", width2 + margin2.left + margin2.right);

          // x_interval.range([0, width2]);
          x_interval.rangePoints([0, width2], 1);


          slider.call(brush);
          eje_x_s.call(
            d3.svg.axis()
            .scale(x_interval)
            // .tickFormat(d3.time.format("%a %d"))
            // .tickFormat(function(d) {
            //     return d + ' times';
            // })
            .orient("bottom")
            .ticks(Math.max(width2 / 75, 2))
            .tickSize(0)
            .innerTickSize(10)
            .tickValues([0, 15])
          );
          // eje_x_s.selectAll('.tick line').attr('y1', -5).attr('y2', 5);
          eje_x_s.selectAll('.tick text').attr('y', 20);

          if (!options.divisions) {
            eje_x_s.select(".manage-color").remove();
            eje_x_s.select(".domain").each(function() {
              this.parentNode.appendChild(this.cloneNode());
            });

            eje_x_s.selectAll(".domain").each(function(d, i) {
              var firstChild = this.parentNode.firstChild;
              if (firstChild) {
                this.parentNode.insertBefore(this, firstChild);
              }
              if (i === 0) {
                path_color = d3.select(this).attr('class', 'manage-color');
              }
            });
          } else {
            eje_x_s.selectAll(".domain").remove();

            d3.select(eje_x_s.node().firstChild).classed('tag_first_element', true);

            var total = d3.sum(options.divisions, function(d) {
              return d.w;
            });

            i_scale.range([0, x_interval(domain[domain.length - 1]) - x_interval(0)]);
            var move = x_interval(0);

            options.divisions.forEach(function(d, i) {
              eje_x_s.select(".rect_color" + (i + 1))
                .attr('width', i_scale(d.w)).attr('x', move);
              move += i_scale(d.w);
            });
          }

          if (options.tooltip) {
            tooltip_value.classed('st-tooltip-right', false);
            tooltip_value.classed('st-tooltip-left', false);
            tooltip_value.style({
              left: 'initial',
              right: 'initial'
            });


            if (x_interval(options.tooltip.pos) > (width2 / 2)) {
              tooltip_value.style({
                right: (width2 - x_interval(options.tooltip.pos) - 2) + "px"
              });
              tooltip_value.classed('st-tooltip-right', true);
            } else {
              tooltip_value.style({
                left: (x_interval(options.tooltip.pos) - 2) + "px"
              });
              tooltip_value.classed('st-tooltip-left', true);
            }


            tooltip_value.transition().delay(500).style({
              top: -($(tooltip_value.node()).outerHeight(true) + 20) + "px"
            });

            eje_x_s.select(".line_answer")
              .attr("transform", "translate(" + x_interval(options.tooltip.pos) + ",0)");
          }

          handle.attr("cx", x_interval(last_value));
          text_show.attr('x', x_interval(last_value));

          if (path_color) {
            var total_l = path_color.node().getTotalLength();
            path_color
              .attr('stroke-dasharray', total_l)
              .attr('stroke-dashoffset', total_l - x_interval(last_value));
          }
        }, 100);
      });


      // return create_slider;
    }
  }

  function circle_flash1() {
    d3.select(this).transition().attr("opacity", 0.01)
      .duration(3000)
      .attr("r", 15)
      .each("end", circle_flash2);
  }

  function circle_flash2() {
    d3.select(this)
      .transition()
      .delay(300)
      .duration(0.0001)
      .attr("r", 7.5)
      .attr("opacity", 0.8)
      .each("end", circle_flash1);
  }

  function translate_hand1() {
    d3.select(this).transition().duration(5000)
      .attr("transform", "translate(" + (x_interval(domain[domain.length - 1]) - 20) + ",0)")
      .each('end', translate_hand2);
  }

  function translate_hand2() {
    d3.select(this).transition().duration(5000)
      .attr("transform", "translate(" + x_interval(domain[0]) + ",0)")
      .each('end', translate_hand1)
  }

  function click_svg_content() {
    if (flag_click_svg === 0 && c_options.pointer) {
      flag_click_svg++
      g_hand_mov.remove();
    }
  }

  function brushed() {
    var value = brush.extent()[0];
    if (d3.event.sourceEvent) { // not a programmatic event
      // value = x_interval.invert(d3.mouse(this)[0]);
      click_svg_content();
      var mouse_value = d3.mouse(this)[0];
      value = mouse_value;
      if (mouse_value <= 0) value = 0;
      if (mouse_value >= width2) value = width2;

      brush.extent([value, value]);
    }
    handle.attr("cx", value);


    var value_text = execute_position(value);
    text_show.text(value_text);
    text_show.attr('x', value);
    paint_dark_color(value);
  }

  function brushended() {
    var value = brush.extent()[0];
    var res_value = execute_position(value);
    c_options.end(res_value);
  }

  function execute_position(value) {
    var value1, value2;
    var before_value = 0;
    var value_return = 0;

    values_domain.forEach(function(d, count) {
      value1 = x_interval(values_domain[count]);
      value2 = x_interval(values_domain[count + 1]);

      if (count !== values_domain.length - 1) {
        if (value >= before_value && value <= (value1 / 2 + value2 / 2)) {
          last_value = values_domain[count];

          handle.attr("cx", value1);
          text_show.text(last_value);
          text_show.attr('x', value1);

          if (path_color) {

            var total_l = path_color.node().getTotalLength();

            path_color
              .attr('stroke-dasharray', total_l)
              .attr('stroke-dashoffset', total_l - value1);
          }

          value_return = last_value;
        }
      } else {
        if (value >= before_value) {
          last_value = values_domain[count];

          handle.attr("cx", value1);
          text_show.text(last_value);
          text_show.attr('x', value1);

          c_options.end(values_domain[count]);

          if (path_color) {
            var total_l = path_color.node().getTotalLength();

            path_color
              .attr('stroke-dasharray', total_l)
              .attr('stroke-dashoffset', total_l - value1);
          }
          value_return = last_value;

        }
      }

      before_value = (value1 / 2) + (value2 / 2);
    });
    return value_return;
  }

  function paint_dark_color(value) {
    var value_accumulative = 0;
    var flag_d = 1;

    c_options.divisions.forEach(function(d, i) {
      execute_percentage_offset(i, 0);
      value_accumulative += +d3.select(".rect_color" + (i + 1)).attr('width');
      if (value <= value_accumulative) {
        if (flag_d === 1) {
          flag_d = 0;
          var current_width = +d3.select(".rect_color" + (i + 1)).attr('width');
          var difference_r = value_accumulative - value;
          var difference_l = current_width - difference_r;
          var percentage_paint = (difference_l / current_width) * 100;
          execute_percentage_offset(i, percentage_paint);
        }
      } else {
        execute_percentage_offset(i, '100');
      }
    });

  }

  function execute_percentage_offset(num, percentage_paint) {
    eje_x_s.select(".glinear" + (num + 1)).selectAll("stop").each(function(d, c) {
      if (c === 0) {
        d3.select(this).attr("offset", "0%");
      }
      if (c === 1) {
        d3.select(this).attr("offset", percentage_paint + "%");
      }
      if (c === 2) {
        d3.select(this).attr("offset", percentage_paint + "%");
      }
      if (c === 3) {
        d3.select(this).attr("offset", "100%");
      }
    });
  }

}





module.exports = create_slider;
