// import fix_svg from "./libraries/fix-svg-size";
import get_svg from "./libraries/get-svg";
import animateJs from "./libraries/animateJs";

new animateJs({
  tag:".main-headline",
  // todo:function(){}
});

// new fix_svg({
//   tag:['']
// });



new get_svg({
  tags: [".svg-1", ".svg-2"],
  folders: ["svg", "svg-2"],
  todo: function(d) {
    d3.selectAll(".line-to-animate path").each(function() {
      var copy_this = this;
      var total_length = copy_this.getTotalLength();
      d3.select(copy_this)
        .attr('stroke-dasharray', total_length)
        .attr('stroke-dashoffset', total_length);
    });

    new animateJs({ 
      tag:".line-to-animate path",
      todo:function(el){
        d3.select(el).transition().duration(3000).attr('stroke-dashoffset', 0);
      }
    });


  }
});
