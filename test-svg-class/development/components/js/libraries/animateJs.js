require('./waypoint');

export default class Animate {
  constructor(options) {
    if(options){
      this.options=options;
      var tag=options.tag;

      this.o_type_animations = {
        "animate-up": "slideUp",
        "animate-down": "slideDown",
        "animate-left": "slideLeft",
        "animate-right": "slideRight",
        "animate-fadeIn": "fadeIn",
        "animate-fadeOut": "fadeOut"
      };

      this.entries_animations=d3.entries(this.o_type_animations);

      this.checkIsAnimate(tag);

    }
  }
  checkIsAnimate(tag) {
    var animate = false;
    var newTag = d3.select(tag);
    this.entries_animations.forEach(d=>{
      if(newTag.classed(d.key)){
        animate=true;
        this.animateTag(newTag, d.value, "in");
      }
    });

    if (!animate) {
      this.entries_animations.forEach(d=>{
        if (newTag.selectAll("."+d.key).size() > 0) {
          this.animateTag(newTag.selectAll("."+d.key), d.value, "in");
        }
      });
    }
    // return animate;
  }
  animateTag(tags, animateName, type) {
    var _this=this;
    tags.each(function(){
      var waypoint_top = new Waypoint({
         element: this,
         handler: function(direction) {
           if (direction === "down") {
             var element_animated = this.element;
             
             if(typeof _this.options.todo==="function")_this.options.todo(element_animated);

             $(element_animated).animateCss(animateName,type);
           }
         },
         offset: "100%"
       });
    });
  }

  animateCss() {}
  animateHand() {}
}
