import * as d3_queue from "d3-queue";
import svg_fix from './fix-svg-size';

export default class GetSvg {
  constructor(options) {
    this.tags;
    this.todo;
    this.a_tags=[];
    this.url_folder=[];

    if (options) {
      var _this=this;
      var q = d3_queue.queue();
      if (options.todo) {
        if (typeof options.todo === "function") {
          this.todo = options.todo;
        }
      }
      if (options.tags) {
        if ($.isArray(options.tags)) {
          this.tags = options.tags;
        }
      }else{
        return ;
      }

      if (options.folders) {
        if ($.isArray(options.folders)) {
          options.folders.forEach( (d,i)=>{
            q.defer(d3.xml, "images/"+d+"/desktop-image.svg");
            var dv=d3.select(_this.tags[i]).append("div").attr("class","st-desktop-svg");
            _this.url_folder.push("images/"+d);
            _this.a_tags.push(dv);

            q.defer(d3.xml, "images/"+d+"/tablet-image.svg");
            var dt=d3.select(_this.tags[i]).append("div").attr("class","st-tablet-svg");
            _this.url_folder.push("images/"+d);
            _this.a_tags.push(dt);

            q.defer(d3.xml, "images/"+d+"/mobile-image.svg");
            var dm=d3.select(_this.tags[i]).append("div").attr("class","st-mobile-svg");
            _this.url_folder.push("images/"+d);
            _this.a_tags.push(dm);

          });
          q.awaitAll((error, xmls)=>_this.get_xml(error, xmls));
        }
      }

    }
  }
  get_xml(error, xmls) {
    var _this=this;
    if (error) throw error;
    xmls.forEach((xml, i) => {
      var svg;
      if (navigator.appName === 'Microsoft Internet Explorer') {
        svg = _this.cloneToDoc(xml.documentElement);
      }
      else{
        svg = document.importNode(xml.documentElement, true);
      }


      d3.select(svg).selectAll("image").each(function(){
        var url_image=d3.select(this).attr("xlink:href");
        d3.select(this).attr("xlink:href",_this.url_folder[i]+"/"+url_image);
      });


      if (_this.a_tags[i] !== undefined) {

        var node_svg=_this.a_tags[i].node();
        node_svg.appendChild(svg);
        new svg_fix({
          tag:node_svg
        });
      }
    });
    if(typeof this.todo==="function")this.todo(xmls);
  }
  cloneToDoc(node, doc){
    var _this=this;
    if (!doc) doc = document;
    var clone = doc.createElementNS(node.namespaceURI, node.nodeName);
    for (var i = 0, len = node.attributes.length; i < len; ++i) {
      var a = node.attributes[i];
      if (/^xmlns\b/.test(a.nodeName)) continue; // IE can't create these
      clone.setAttributeNS(a.namespaceURI, a.nodeName, a.nodeValue);
    }
    for (var ii = 0, len2 = node.childNodes.length; ii < len2; ++ii) {
      var c = node.childNodes[ii];
      clone.insertBefore(
        c.nodeType == 1 ? _this.cloneToDoc(c, doc) : doc.createTextNode(c.nodeValue),
        null
      );
    }
    return clone;
  }

}
