// require("../precss/app.css");

import $ from "jquery";

// import xx from "./libraries/fix-svg-size";

import * as d3 from "d3";

// $(".chapter");

// $("#chapter-1");
// $("#chapter-2");


// var elem=d3.selectAll(".chapter");
// console.log(elem);
// d3.select()
// d3.selectAll()

// $(document).ready(fnuction(){
// })


$(function() {

  var bardata = [55, 80, 50, 90, 60];

  var height = 400,
    width = 600,
    barWidth = 30,
    barOffset = 30;


  // var yScale = d3.scale.linear()
  //     .domain([0, d3.max(bardata)])
  //     .range([0, height])

  var bar = d3.select('.chapter').append('svg')
    .attr('width', width)
    .attr('height', height)
    .style('background', 'grey')
    .selectAll('rect').data(bardata)
    .enter().append('rect')
    .style('fill', 'blue')
    .attr('width', barWidth)
    .attr('height', function(d) {
      return d;
    })
    .attr('x', function(d, i) {
      return i * (barWidth + barOffset)
    })
    .attr('y', function(d) {
      return height - d;
    })

  d3.select('.chapter').selectAll("div")
    .data(bardata)
    .enter().append("div")
    .style("width", function(d) {
      return d * 10 + "px";
    })
    .text(function(d) {
      return d;
    });

});

function preparePage() {
  document.getElementById("chapter-2").onclick = function() {
    if (document.getElementById("chapter-2").className == "example") {
      document.getElementById("chapter-2").className = "";
    } else {
      document.getElementById("chapter-2").className = "example";
    }
  };
}
window.onload = function() {
  preparePage();
}




// import babylonjs from "babylonjs";
// console.log(babylonjs);

// var a_test=[1,2,3,4,5,6,7,6,4,4,3,3,3,6,7,7,6];
//
// a_test.forEach(d=>{
//   console.log(d);
// });
