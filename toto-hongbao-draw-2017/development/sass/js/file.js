var map_leaflet;
var distance_perimeter = 1000;
var st_circle_perimeter;
var tmp_url_marker;
var url_marker_svg_red = "images/special-icons/lucky-spot.svg";
var url_marker_svg_last = "images/special-icons/red-lucky-spot.svg";
var url_marker_svg_selected = "images/special-icons/lucky-spot-selected.svg";
var marker_flag_selected;
var marker_selected;
var c_table;

var a_markers = [];
var t_card = "<div class='st-card' id='pos_[ST-POSITION-ID]'>";
// t_card += "<div class='label_card'>CHILDCARE</div>";

t_card += "[ST-START]";

t_card += "<div class='row_card'>";
t_card += "<div class='label_card'> Year </div>";
t_card += "<div class='description_card'>[ST-YEAR]</div>";
t_card += "</div>";

t_card += "<div class='row_card'>";
t_card += "<div class='label_card'> Outlet </div>";
t_card += "<div class='description_card'>[ST-OUTLET]</div>";
t_card += "</div>";

t_card += "<div class='row_card'>";
t_card += "<div class='label_card'> Address </div>";
t_card += "<div class='description_card'>[ST-ADDRESS]</div>";
t_card += "</div>";



t_card += "<div class='row_card'>";
t_card += "<div class='label_card'> Prize Money </div>";
t_card += "<div class='description_card'>[ST-PRIZE-MONEY]</div>";
t_card += "</div>";
t_card += "<div class='row_card'>";
t_card += "<div class='label_card'> Winning Numbers </div>";
t_card += "<div class='description_card'> [ST-WINNING-NUMBERS] </div>";
t_card += "</div>";
t_card += "<div class='row_card'>";
t_card += "<div class='label_card'> No. of Winners </div>";
t_card += "<div class='description_card'> [ST-NUMBER-WINNERS] </div>";
t_card += "</div></div>";


var t_card_mobile = "<div class='st-card-mobile'>";
t_card_mobile += "<div class='t-row-mobile'><span class='label-mobile'>Year (Winner):&nbsp;</span><span class='description-mobile'>[ST-YEAR]</span></div>";
t_card_mobile += "<div class='t-row-mobile'><span class='label-mobile'>Outlet:&nbsp;</span><span class='description-mobile'>[ST-OUTLET]</span></div>";
t_card_mobile += "<div class='t-row-mobile'><span class='label-mobile'>Address:&nbsp;</span><span class='description-mobile'>[ST-ADDRESS]</span></div>";
t_card_mobile += "</div>";


var LeafIcon = L.Icon.extend({
    options: {
        //shadowUrl: 'js/images/marker-shadow.png',
        iconSize: [25, 31],
        shadowSize: [50, 64],
        iconAnchor: [12, 27],
        shadowAnchor: [4, 62],
        popupAnchor: [0, 0]
    }
});

var o_colors_bullet = {
    2016: "rgb(31, 95, 112)",
    2015: "rgb(0, 129, 198)",
    2014: "rgb(204, 0, 51)"
};
var select2_s;
$(function() {
    /*end of array selects*/
    select2_s = $(".select-years").select2({
        placeholder: "YEARS",
        allowClear: true,
        width: "100%",
        minimumResultsForSearch: Infinity
    }).on("change", function(event) {
        f_filter_elements($(this).val());
    });

    var a_data_csv = [];

    // var url_csv_data_lottery = "csv/toto_lottery.csv";
    // var url_csv_data_lottery = "http://localhost/graphics/st-get-file-csv/1pQznhntsrGoXdhupZfm4tGeWsYbsHlLCAAbP70VKmHw";
    // var url_csv_data_lottery = "http://52.74.240.184/graphics/st-get-file-csv/1pQznhntsrGoXdhupZfm4tGeWsYbsHlLCAAbP70VKmHw";
    var url_csv_data_lottery = "csv/data_lottery_2017.csv";


    // var url_csv_data_l = "http://localhost/graphics/st-get-file-csv/1pWDBUvBUuJ0ZMpjs_NLNcEcNASOGeQBe0V9-JWdl_s0";
    // var url_csv_data_l= "http://52.74.240.184/graphics/st-get-file-csv/1pWDBUvBUuJ0ZMpjs_NLNcEcNASOGeQBe0V9-JWdl_s0";
    var url_csv_data_l = "csv/winner_2017.csv";




    var url_tiles = "https://{s}.tiles.mapbox.com/v4/{user_id}/{z}/{x}/{y}.png?access_token={token}";

    var definition_layer1 = new L.TileLayer(url_tiles, {
        // continuousWorld: true,
        attribution: "CartoDB",
        // subdomains: "1234",
        token: "pk.eyJ1IjoiY2hhY2hvcGF6b3MiLCJhIjoib2w2VmRlOCJ9.0oe-BndrfdhOBtI-4mzMeQ",
        user_id: "chachopazos.2ffaa71c"
    });

    map_leaflet = new L.Map("map-prizes", {
        center: [1.350270, 103.851959],
        zoom: 12,
        // maxZoom: 16,
    });
    map_leaflet.scrollWheelZoom.disable();
    map_leaflet.addLayer(definition_layer1);
    L.Icon.Default.imagePath = "images";



    d3.csv(url_csv_data_lottery, function(error, data) {
        if (!error) {
            a_data_csv = data;
            window.onload = main(a_data_csv);
        }
    });

    d3.csv(url_csv_data_l, function(error, data) {
        if (!error) {
            d3.select(".table-last .st-tbody-last").selectAll('tr')
                .data(data).enter()
                .append('tr')
                .filter(function(d) {
                    return d['Year (Winner)'].trim() !== "" || d.Outlet.trim() !== "";
                })
                .selectAll('td').data(function(d) {

                    var cm = t_card_mobile
                        .replace('[ST-YEAR]', d['Year (Winner)'], 'g')
                        .replace('[ST-OUTLET]', d.Outlet, 'g')
                        .replace('[ST-ADDRESS]', d.Address, 'g');

                    $(".content-card-table").append(cm);
                    return [d.Outlet, d.Address, d['Year (Winner)']];
                }).enter()
                .append('td')
                .html(function(d) {
                    return "<div>" + d + "</div>";
                });
        }
    });

    $(".content-cards").on('mouseover', '.st-card', function() {
        var pos = parseInt($(this).attr('id').split('pos_')[1]);
        if (marker_flag_selected !== pos) {
            a_markers.forEach(function(d) {
                if (pos === d.pos) {
                    marker_flag_selected = pos;
                    $('.content-cards').stop(true, false);
                    $(".st-card").removeClass('st-highlighter');
                    $(".st-card").removeClass('st-highlighter-thisYear');
                    if (marker_selected !== undefined) map_leaflet.removeLayer(marker_selected);
                    map_leaflet.setView(d.marker._latlng, 13);
                    // map_leaflet.setView(d.marker._latlng);
                    action_mouseover(d, 0);
                }
            });
        }
    });
});


$(window).on("resize", function() {
    map_leaflet.invalidateSize();
});


function f_filter_elements(e_select) {
    $(".st-card").removeClass('st-highlighter');
    $(".st-card").removeClass('st-highlighter-thisYear');
    if (e_select) {
        if (marker_selected !== undefined) map_leaflet.removeLayer(marker_selected);
        a_markers.forEach(function(d) {
            if (d.is_visible === 0) {
                map_leaflet.addLayer(d.marker);
                d.is_visible = 1;
                $("#pos_" + d.pos).show();
            }

            if (d.year.trim() !== e_select.trim()) {
                $("#pos_" + d.pos).hide();
                map_leaflet.removeLayer(d.marker);
                d.is_visible = 0;
            } else {
                $(".numer-search-out").html("<span>Jackpot prize: </span><span class='strong'>" + d.value_prize + "</span>");
                $(".numer-search-out").show();
            }
        });
    } else {
        $(".numer-search-out").hide();
        a_markers.forEach(function(d) {
            if (d.is_visible === 0) {
                map_leaflet.addLayer(d.marker);
                d.is_visible = 1;
                $("#pos_" + d.pos).show();
            }
        });
    }
}

function get_color(year) {
    var color = o_colors_bullet[year];
    return color;
}

function order_by_week(array) {
    return array.sort(function(a, b) {
        return d3.ascending(parseInt(a.week), parseInt(b.week));
    });
}

function getkeys(object) {
    return d3.keys(object).filter(function(d) {
        return d !== 'week' && d !== "2012" && d !== "2013";
    }).sort(function(a, b) {
        return d3.ascending(parseInt(a), parseInt(b));
    });
}

// var geocoder = new google.maps.Geocoder();

// function googleGeocoding(text, callResponse) {
//     geocoder.geocode({
//         address: text,
//         componentRestrictions: {
//             country: "SG"
//         }
//     }, callResponse);
// }

function formatJSON(rawjson) {
    var json = {},
        key, loc, disp = [];

    for (var i in rawjson) {
        key = rawjson[i].formatted_address;

        loc = L.latLng(rawjson[i].geometry.location.lat(), rawjson[i].geometry.location.lng());

        json[key] = loc; //key,value format
    }

    return json;
}

function action_mouseover(obj_marker, scroll) {
    var element_top = $("#pos_" + obj_marker.pos)[0].offsetTop - ($(".content-cards").height() / 2) + ($("#pos_" + obj_marker.pos).height() / 2);

    if (obj_marker.pos === 0 || obj_marker.pos === 1 || obj_marker.pos === 2 || obj_marker.pos === 3) {
        $("#pos_" + obj_marker.pos).addClass('st-highlighter-thisYear');
    } else {
        $("#pos_" + obj_marker.pos).addClass('st-highlighter');
    }

    var el = $('.content-cards');
    if (scroll === 1) {
        el.animate({
            'scrollTop': element_top
        }, 'slow');
    }

    marker_selected = L.marker(obj_marker.marker._latlng, {
        icon: new LeafIcon({
            iconUrl: url_marker_svg_selected
        })
    }).addTo(map_leaflet);
}



function main(a_data) {
    var a_year = [];
    var o_year_number_winners = {};
    $(".select-years").append('<option value=""></option>');

    a_data.forEach(function(d, i) {
        d.st_position = i;
        var st_lat = d['Coordinates (latitude)'];
        var st_lng = d['Coordinates (longtitude)'];

        // if (st_lat && st_lng) {

        var year = (d.Date.trim() !== "") ? d.Date.trim().split(",")[1].trim() : "";
        if (year === '2017') {
            tmp_url_marker = url_marker_svg_last;
        } else {
            tmp_url_marker = url_marker_svg_red;
        }


        var marker = new L.marker([st_lat, st_lng], {
                icon: new LeafIcon({
                    iconUrl: tmp_url_marker
                })
            })
            // .bindPopup()
            .addTo(map_leaflet)
            .on("click", function(e) {
                $('.content-cards').stop(true, false);
                $(".st-card").removeClass('st-highlighter');
                $(".st-card").removeClass('st-highlighter-thisYear');
                if (marker_selected !== undefined) map_leaflet.removeLayer(marker_selected);
                a_markers.forEach(function(nd) {
                    if (nd.marker === e.target) {
                        action_mouseover(nd, 1);
                    }
                });
            });


        var value_prize = d["Jackpot (total jackpot is calculated as it's not on website)"].trim();
        a_markers.push({
            marker: marker,
            pos: d.st_position,
            is_visible: 1,
            year: year,
            value_prize: value_prize
        });

        var t_card_f = t_card.replace('[ST-YEAR]', year, 'g')
            .replace('[ST-ADDRESS]', d.Address.trim(), 'g')
            .replace('[ST-PRIZE-MONEY]', value_prize, 'g')
            .replace('[ST-WINNING-NUMBERS]', d["Winning numbers"].trim(), 'g')
            .replace('[ST-NUMBER-WINNERS]', d["No of winners"].trim(), 'g')
            .replace('[ST-POSITION-ID]', d.st_position, 'g')
            .replace('[ST-START]', (parseInt(d.Repeats) === 1) ? '<div class="st-start">*</div>' : "", 'g')
            .replace('[ST-OUTLET]', d['Betting outlet'], 'g');

        $(".content-cards").append(t_card_f);

        if ($.inArray(year, a_year) === -1 && year) {
            a_year.push(year);
            o_year_number_winners[year] = d["Winning numbers"].trim();
            $(".select-years").append('<option value="' + year + '">' + year + '</option>');
        }
        // }
    });


    c_table = d3.select(".container-box .st-tbody-box").selectAll('tr')
        .data(a_year).enter()
        .append('tr').selectAll('td').data(function(d) {
            var new_string = d + "," + o_year_number_winners[d];
            return new_string.split(',');
        }).enter()
        .append('td')
        .html(function(d) {
            return "<div>" + d + "</div>";
        });

    $(".nano").nanoScroller({
        scroll: 'top',
        alwaysVisible: true,
        // iOSNativeScrolling: true,
        preventPageScrolling: true
    });
}
