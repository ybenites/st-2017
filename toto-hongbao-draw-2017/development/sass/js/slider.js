var width_g = $(".st-slider").width();
var margin2 = {
        top: 2,
        right: 10,
        bottom: 2,
        left: 10
    },
    width2 = width_g - margin2.left - margin2.right,
    height = 50 - margin2.bottom - margin2.top;

var parseDate = d3.time.format("%y-%m-%d %H").parse;

// xslider = d3.time.scale()
//     .range([0, width2])
//     .domain([parseDate("15-03-18 13"), parseDate("15-03-30 07")])
//     .clamp(true);

var x_interval = d3.scale.ordinal()
    .rangePoints([0, width2], 1)
    // .domain([5, 4, 3]);
    .domain([6, 5, 4]);

var brush = d3.svg.brush()
    .x(x_interval)
    .extent([x_interval(6), x_interval(6)])
    .on("brush", brushed)
    .on("brushend", brushended);

var svg = d3.select(".st-slider").append("svg")
    .attr("width", width2 + margin2.left + margin2.right)
    .attr("height", height + margin2.top + margin2.bottom)
    .append("g")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

var eje_x_s = svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height / 2 + ")");
eje_x_s.call(d3.svg.axis()
        .scale(x_interval)
        .orient("bottom")
        .tickFormat(function(d) {
            return d + ' times';
        })
        .ticks(Math.max(width2 / 75, 2))
        .tickSize(0)
        .innerTickSize(10)
    )
    .select(".domain");
// .select(function() {
//     return this.parentNode.appendChild(this.cloneNode(true));
// })
// .attr("class", "halo");

var slider = svg.append("g")
    .attr("class", "slider")
    .call(brush);

slider.selectAll(".extent,.resize")
    .remove();

slider.select(".background")
    .attr("height", height);

handle = slider.append("circle")
    .attr("class", "handle")
    .attr("transform", "translate(0," + height / 2 + ")")
    .attr("r", 9);

slider
    .call(brush.event)
    .transition() // gratuitous intro!
    .duration(750)
    .call(brush.extent([x_interval(6), x_interval(6)]))
    .call(brush.event);

var resizeTimer2;
$(window).on("resize", function() {
    clearTimeout(resizeTimer2);
    resizeTimer2 = setTimeout(function() {
        width_g = $(".st-slider").width();
        width2 = width_g - margin2.left - margin2.right;
        d3.select(".st-slider svg").attr("width", width2 + margin2.left + margin2.right);

        // x_interval.range([0, width2]);
        x_interval.rangePoints([0, width2], 1);
        slider.call(brush);
        eje_x_s.call(
            d3.svg.axis()
            .scale(x_interval)
            // .tickFormat(d3.time.format("%a %d"))
            .tickFormat(function(d) {
                return d + ' times';
            })
            .orient("bottom")
            .ticks(Math.max(width2 / 75, 2))
            .tickSize(0)
            .innerTickSize(10)
        );
    }, 100);
});


function brushed() {
    var value = brush.extent()[0];
    if (d3.event.sourceEvent) { // not a programmatic event
        // value = x_interval.invert(d3.mouse(this)[0]);
        var mouse_value = d3.mouse(this)[0];
        value = mouse_value;
        if (mouse_value <= 0) value = 0;
        if (mouse_value >= width2) value = width2;

        brush.extent([value, value]);
    }
    handle.attr("cx", value);

    // d3.select("body").style("background-color", d3.hsl(value, .8, .8));
}

function brushended() {
    var value = brush.extent()[0];
    var value5 = x_interval(6);
    var value4 = x_interval(5);
    var value3 = x_interval(4);

    if (value < (value5 / 2 + value4 / 2)) {
        handle.attr("cx", value5);
        paint_by_value(6);
    } else if (value >= (value5 / 2 + value4 / 2) && value <= (value4 / 2 + value3 / 2)) {
        handle.attr("cx", value4);
        paint_by_value(5);
    } else {
        handle.attr("cx", value3);
        paint_by_value(4);
    }
}

function paint_by_value(value_pass) {
    if (c_table !== undefined) {
        var a_repeat = {};
        var color = d3.scale.linear()
            .domain([0, 8])
            // .range(["##ff6700","#ffcc00"]);
            .range(["#fff", "#ffcc00"]);

        var color2 = d3.scale.linear()
            .domain([0, 8])
            // .range(["##ff6700","#ffcc00"]);
            .range(["#fff", "#ff6700"]);


        var a_numbers_repeat = [];
        c_table.each(function(d, i) {
            if (i !== 0) {
                var td_number = d.trim();
                d3.select(this).classed("select_td_" + td_number, true);

                if (a_repeat[td_number] === undefined) {
                    a_repeat[td_number] = 0;
                }
                a_repeat[td_number]++;

                if (parseInt(a_repeat[td_number]) === parseInt(value_pass)) {
                    a_numbers_repeat.push(td_number);
                    $(".select_td_" + td_number + ' div').addClass('highlight-numbers');
                    // $(".select_td_" + td_number + ' div').css('background-color',color(color_count));
                } else {
                    $(".select_td_" + td_number + ' div').removeClass('highlight-numbers');
                    $(".select_td_" + td_number + ' div').css('background-color', "#fff");

                    if ($.inArray(td_number, a_numbers_repeat) !== -1) {
                        a_numbers_repeat.splice($.inArray(td_number, a_numbers_repeat), 1);
                    }
                }
            }
        });

        color.domain([0, a_numbers_repeat.length / 2]);
        color2.domain([0, a_numbers_repeat.length / 2]);
        var count_p = 0;
        var count_i = 0;
        a_numbers_repeat.forEach(function(td_number, i) {
            var f_color;
            if (i % 2 === 0) {
                f_color = color((count_p + 1));
                count_p++;
            } else {
                f_color = color2((count_i + 1));
                count_i++;
            }
            $(".select_td_" + td_number + ' div').css('background-color', f_color);
        });
    }
}
