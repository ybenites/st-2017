import get_svg from "./libraries/get-svg";
import fix_svg from "./libraries/fix-svg-size"

const ScrollMagic = require('ScrollMagic');
require('animation.gsap');
require('debug.addIndicators');
// const TimelineMax = require('TimelineMax');

import IScroll from "./libraries/iscroll-probe";

var time_singaporean = 85000;
var time_marathon = 68025;
var aceleration_time = 5;

var disabled_color = "#5A9DDC";
var enabled_color = "#EF4123";

var isMobile = (function(a) {
  return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
})(navigator.userAgent || navigator.vendor || window.opera);

var controller;
$(function() {
  d3.select(".chart-to-fix").selectAll(".one,.two,.three,.four,.five,.six,.seven").attr("fill", disabled_color).attr("opacity", 0.2);
  d3.selectAll(".content-cards .card").selectAll(".row-card").transition().duration(500).style("background-color", "#ffffff").style("opacity", 0.8);

  controller = new ScrollMagic.Controller({container: "#wrapper-page"});

  var g_scene = new ScrollMagic.Scene({
    triggerElement: ".st-graphic-2",
    offset: window.innerHeight / 2,
    duration: time_scene
  }).setPin(".st-graphic-2-fix", {pushFollowers: false}).addTo(controller);

  d3.selectAll(".content-cards .card").each(function() {
    var dot_scene = new ScrollMagic.Scene({
      triggerElement: this,
      // offset: -window.innerHeight / 2,
      duration: time_card
    }).on("enter leave", function(event) {
      var element_card = d3.select(this.triggerElement());
      element_card.classed("card", false);
      var class_circle = element_card.attr("class");
      element_card.classed("card", true);

      if (event.type === "enter") {
        element_card.selectAll(".row-card").transition().duration(500).style("background-color", "#EFF3F4").style("opacity", 1);

        d3.selectAll(".chart-to-fix ." + class_circle).transition().duration(500).attr('fill', enabled_color).attr("opacity", 1);
      }
      if (event.type === "leave") {
        element_card.selectAll(".row-card").transition().duration(500).style("background-color", "#ffffff").style("opacity", 0.8);

        d3.selectAll(".chart-to-fix ." + class_circle).transition().duration(500).attr('fill', disabled_color).attr("opacity", 0.2);
      }
    }).addTo(controller)
    // .addIndicators();
  });

  d3.selectAll(".content-model-runner image").classed("animated", false).classed("none", true);
  d3.selectAll(".content-model-runner .animate-1").each(function() {
    var animate_body = new ScrollMagic.Scene({
      triggerElement: this,
      offset: -window.innerHeight * 0.25,
      duration: 2
    }).on("enter leave", function(event) {
      if (event.type === "enter" && event.scrollDirection === "FORWARD") {
        $(this.triggerElement()).parents("svg").find("image").animateCss("fadeIn", "in");
      }
      if (event.type === "enter" && event.scrollDirection === "REVERSE") {
        $(this.triggerElement()).parents("svg").find("image").removeClass("none");
      }
      if (event.type === "leave" && event.scrollDirection === "REVERSE") {
        $(this.triggerElement()).parents("svg").find("image").addClass("none");
      }
      if(event.scrollDirection==="PAUSED" && event.state==="AFTER"){
        $(this.triggerElement()).parents("svg").find("image").removeClass("none");
      }
    }).addTo(controller)
    // .addIndicators();
  });

  d3.selectAll(".animate-1,.animate-2,.animate-3,.animate-4").classed("animated", false).classed("none", true).each(function() {
    var animate_scene = new ScrollMagic.Scene({
      triggerElement: this,
      offset: -window.innerHeight * 0.375,
      duration: 2
    }).on("enter leave", function(event) {
      // console.log(event);
      // console.log(this.triggerElement());
      if (event.type === "enter" && event.scrollDirection === "FORWARD") {
        $(this.triggerElement()).animateCss("fadeIn st-fadeInUp", "in");
      }
      if (event.type === "enter" && event.scrollDirection === "REVERSE") {
        $(this.triggerElement()).removeClass("none");
      }
      if (event.type === "leave" && event.scrollDirection === "REVERSE") {
        $(this.triggerElement()).addClass("none");
      }
      if(event.scrollDirection==="PAUSED" && event.state==="AFTER"){
        $(this.triggerElement()).removeClass("none");
      }
    }).addTo(controller)
    // .addIndicators();
  });

  var myScroll
  if (isMobile) {
    $(".content-fb-comments").addClass("none");
    myScroll = new IScroll('#wrapper-page', {
      // don't scroll horizontal
      scrollX: false,
      // but do scroll vertical
      scrollY: true,
      // show scrollbars
      scrollbars: true,
      // deactivating -webkit-transform because pin wouldn't work because of a webkit bug: https://code.google.com/p/chromium/issues/detail?id=20574
      // if you dont use pinning, keep "useTransform" set to true, as it is far better in terms of performance.
      useTransform: false,
      // deativate css-transition to force requestAnimationFrame (implicit with probeType 3)
      useTransition: false,
      // set to highest probing level to get scroll events even during momentum and bounce
      // requires inclusion of iscroll-probe.js
      probeType: 3,
      // pass through clicks inside scroll container
      click: true
    });

    // overwrite scroll position calculation to use child's offset instead of container's scrollTop();
    controller.scrollPos(function() {
      return -myScroll.y;
    });

    // thanks to iScroll 5 we now have a real onScroll event (with some performance drawbacks)
    myScroll.on("scroll", function() {
      controller.update();
    });

    // add indicators to scrollcontent so they will be moved with it.
    // g_scene.addIndicators({
    //   parent: ".scrollContent"
    // });

  } else {
    // g_scene.addIndicators();
  }

  $(window).on('load', function() {
    if (isMobile) {
      myScroll.refresh();
    }
  });

  $(window).on('resize', function() {
    if (isMobile) {
      myScroll.refresh();
    }
  });

  $(window).on('orientationchange', function() {
    if (isMobile) {
      myScroll.refresh();
    }

  });

});

$(function() {
  fix_svg({tag: ".content-model-runner"});
  fix_svg({tag: ".chart-to-fix"});

  get_svg({urls: ["images/marathon/main-graphic.svg"], tags: [".content-svg-racetrack"], todo: function() {
      // start code move alone
    }});

  $(".content-svg-racetrack").on("click", "#play", function() {
    d3.select(".content-svg-racetrack #replay").attr("display", "block").attr("pointer-events", "none").attr("opacity", 0);

    d3.select(this).attr("opacity", 1).attr("pointer-events", "none").transition().duration(500).attr("opacity", 0).on("end", move_runners);
  });
  $(".content-svg-racetrack").on("click", "#replay", function() {
    d3.select(".content-svg-racetrack #replay").transition().duration(750).attr("pointer-events", "none").attr("opacity", 0);
    move_runners();
  });

});

var timer_runners;
function move_runners() {
  d3.selectAll("#marathon,#singapore").attr("transform", "translate(0,0)");
  var max_time = d3.max([time_singaporean, time_marathon]);
  var m = "00",
    s = 0,
    ms = 0;

  var node_path = d3.select("#path-to-follow path").node();
  var getTotalPath = node_path.getTotalLength();

  timer_runners = d3.interval((elapsed) => {
    elapsed = elapsed * aceleration_time;
    var route_marathon = (elapsed * getTotalPath) / time_marathon;
    var route_singaporean = (elapsed * getTotalPath) / time_singaporean;

    var current_point_marathon = node_path.getPointAtLength(getTotalPath);
    var current_point_singaporean = node_path.getPointAtLength(getTotalPath);
    if (route_marathon <= getTotalPath)
      current_point_marathon = node_path.getPointAtLength(route_marathon);
    if (route_singaporean <= getTotalPath)
      current_point_singaporean = node_path.getPointAtLength(route_singaporean);

    var move_marathon_x = current_point_marathon.x - node_path.getPointAtLength(0).x;
    var move_marathon_y = current_point_marathon.y - node_path.getPointAtLength(0).y;
    d3.select("#marathon").attr("transform", "translate(" + move_marathon_x + "," + move_marathon_y + ")");

    var move_singaporean_x = current_point_singaporean.x - node_path.getPointAtLength(0).x;
    var move_singaporean_y = current_point_singaporean.y - node_path.getPointAtLength(0).y;
    d3.select("#singapore").attr("transform", "translate(" + move_singaporean_x + "," + move_singaporean_y + ")");

    s = parseInt(elapsed / 1000);
    ms = parseInt(Math.round(elapsed % 1000));

    if (ms >= 100)
      ms = parseInt(ms / 10);
    if (s < 10)
      s = "0" + s;
    d3.select(".left-runner .time-runner").html(m + ":" + s + ":" + ms);
    d3.select(".right-runner .time-runner").html(m + ":" + s + ":" + ms);
    if (time_singaporean < parseInt(elapsed))
      d3.select(".left-runner .time-runner").html("00:85:00");
    if (time_marathon < parseInt(elapsed))
      d3.select(".right-runner .time-runner").html("00:68:25");
    if (max_time < elapsed) {
      timer_runners.stop();
      d3.select(".content-svg-racetrack #replay").transition().duration(750).attr("pointer-events", "all").attr("opacity", 1);
    }
  }, 1);
}

function time_card() {
  var time_card = 0;
  if (this)
    time_card = $(this.triggerElement()).outerHeight(true);
  return time_card;
}

function time_scene() {
  return $(".st-graphic-2 .content-cards").height() - $(".content-cards .card.seven").outerHeight(true) + 20;
}
