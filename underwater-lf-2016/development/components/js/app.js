import $ from "jquery";
import * as d3 from "d3";

// import fix_svg from "./libraries/fix-svg-size";

import get_svg from "./libraries/get-svg";
import lazy_load from "./libraries/lazy-load";
import IScroll from "./libraries/iscroll-probe";

import ScrollMagic from "scrollmagic";
import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap";
// import "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators";


import videojs from "./libraries/videojs";
// var player = videojs('bkk-video');

var isMobile = (function(a) {
    return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
})(navigator.userAgent || navigator.vendor || window.opera);

var myScroll;
var controller = new ScrollMagic.Controller({container: "#wrapper-page"});

$(".st-header-hero-video").height($(window).height() - $(".st-content-menu").height());

$(function() {

  var sceneSound = new ScrollMagic.Scene({
      triggerElement: ".st-header-hero-video",
      duration: time_phases,
      // offset: -0.45*window.innerHeight
      offset:window.innerHeight*0.5
  }).addTo(controller)
  // .addIndicators()
  .on("end", function(e) {
    if (e.scrollDirection === "FORWARD") {
      $(".btn-with-sound").trigger("click");
    }else if (e.scrollDirection === "REVERSE") {
      $(".btn-without-sound").trigger("click");
    }
      // $("#lastHit").text(e.type == "start"
      // ? "top"
      // : "bottom");
  });







      lazy_load({
        controller: controller,
        tag: ['.icon-tip'],
        todo: function(el) {
            d3.select(el)
          .attr('transform','translate(0,130)')
          .transition().duration(1500)
          .attr('opacity',1)
          .attr('transform','translate(0,0)');

        }
      });
      lazy_load({
        controller: controller,
        tag: ['.icon-tip-2'],
        todo: function(el) {
            d3.select(el)
          .attr('transform','translate(0,130)')
          .transition().duration(1500).delay(300)
          .attr('opacity',1)
          .attr('transform','translate(0,0)');

        }
      });
      lazy_load({
        controller: controller,
        tag: ['.icon-tip-3'],
        todo: function(el) {
            d3.select(el)
          .attr('transform','translate(0,130)')
          .transition().duration(1500).delay(600)
          .attr('opacity',1)
          .attr('transform','translate(0,0)');

        }
      });
      lazy_load({
        controller: controller,
        tag: ['.icon-tip-4'],
        todo: function(el) {
            d3.select(el)
          .attr('transform','translate(0,130)')
          .transition().duration(1500).delay(900)
          .attr('opacity',1)
          .attr('transform','translate(0,0)');

        }
      });





  //   d3.selectAll(".lines-1").transition().each(animateFirstStep1);
  //   d3.selectAll(".lines-2").transition().each(animateFirstStep2);
   //
  //      function animateFirstStep1(){
  //          d3.selectAll(".lines-1")
  //          .transition().attr("opacity", 1)
  //            .duration(500)
  //            .on("end", animateSecondStep1);
  //    };
   //
   //
   //
  //    function animateSecondStep1(){
  //          d3.selectAll(".lines-1")
  //          .transition()
  //            .duration(500)
  //            .attr("opacity", 0)
  //        .on("end", animateFirstStep1);
  //    };
   //
  //    function animateFirstStep2(){
  //        d3.selectAll(".lines-2")
  //        .transition().attr("opacity", 0)
  //          .duration(500)
  //          .on("end", animateSecondStep2);
  //  };
   //
   //
  //  function animateSecondStep2(){
  //        d3.selectAll(".lines-2")
  //        .transition()
  //          .duration(500)
  //          .attr("opacity", 1)
  //      .on("end", animateFirstStep2);
  //  };




    $(".st-header-hero-video").height($(window).height() - $(".st-content-menu").height());

    $(".st-btn-rh").on("click",function(e){
      e.preventDefault();
      controller.scrollTo(function (newScrollPos) {
        if(isMobile){
          myScroll.scrollTo(0,-500,500);
        }else{
          $("#wrapper-page").animate({
            scrollTop:newScrollPos
          },500);
        }

      });

       controller.scrollTo($(window).height());

    });
    get_svg({
      urls:['images/underwater/logo-singapore-underwater.svg'],
      tags:['.logo-suw'],
      todo:function(){
      }
    })
    get_svg({
        // controller: controller,
        urls: [
            'images/underwater/line-chart-mobile.svg', 'images/underwater/line-chart-desktop.svg'
        ],
        tags: [
            '.content-line-chart .st-mobile-v', '.content-line-chart .st-desktop-h'
        ],
        todo: function() {
            // example transition d3...
            d3.select(".line-animate-1").each(function() {
                var copy_this = this;
                var total_length = copy_this.getTotalLength();
                d3.select(copy_this)
                  .attr('stroke-dasharray', total_length)
                  .attr('stroke-dashoffset', total_length);
              });
              d3.select(".line-animate-1b").each(function() {
                  var copy_this2 = this;
                  var total_length2 = copy_this2.getTotalLength();
                  d3.select(copy_this2)
                    .attr('stroke-dasharray', total_length2)
                    .attr('stroke-dashoffset', total_length2);
                });

            lazy_load({
              controller: controller,
              tag: ['.line-animate-1'],
              todo: function(el) {
                // d3.select(ele).transition()...
                d3.select(".line-animate-1").transition().duration(3500).attr('stroke-dashoffset', 0);

              }
            });
            lazy_load({
              controller: controller,
              tag: ['.line-animate-1b'],
              todo: function(el) {
                // d3.select(ele).transition()...
                d3.select(".line-animate-1b").transition().duration(3500).attr('stroke-dashoffset', 0);

              }
            });

            lazy_load({
              controller: controller,
              tag: ['.numer-1'],
              todo: function(el) {
                d3.select(el).each(function() {
                  d3.select(el).attr('opacity', 0);

                  });
                // d3.select(ele).transition()...
                d3.select(el).transition().delay(2300).duration(300).attr('opacity', 1);


              }
            });

            // end example transition d3
        }
    });

    get_svg({
        urls: [
            'images/underwater/small-multiple-1.svg', 'images/underwater/small-multiple-2.svg', 'images/underwater/small-multiple-3.svg', 'images/underwater/small-multiple-4.svg', 'images/underwater/small-multiple-5.svg'
        ],
        tags: [
            '.graphic-multiple .s-multiple-1', '.graphic-multiple .s-multiple-2', '.graphic-multiple .s-multiple-3', '.graphic-multiple .s-multiple-4', '.graphic-multiple .s-multiple-5'
        ],
        todo: function() {
          lazy_load({
            controller: controller,
            tag: ['.to-move'],
            todo: function(el) {
                d3.select(el)
              .attr('transform','translate(0,100)')
              .transition().duration(1500)
              .attr('opacity',1)
              .attr('transform','translate(0,0)');

            }
          });

        }

    });

    get_svg({
        // controller: controller,
        urls: [
            'images/underwater/bar-chart-ranking-mobile.svg', 'images/underwater/bar-chart-ranking-desktop.svg'
        ],
        tags: [
            '.content-bar-chart .st-mobile-v', '.content-bar-chart .st-desktop-h'
        ],
        todo: function() {



          lazy_load({
            controller: controller,
            tag: ['.desktop-bar rect'],
            todo: function(el) {
              var original_rect = d3.select(el).attr('width');
              // d3.select(ele).transition()...
              d3.select(el).attr('width', 0).attr('opacity', 1)
              .transition().duration(800)
              .attr('width', original_rect)

            }
          });
          lazy_load({
            controller: controller,
            tag: ['.mobile-bar rect'],
            todo: function(el) {
              var original_rect2 = d3.select(el).attr('width');
              // d3.select(ele).transition()...
              d3.select(el).attr('width', 0).attr('opacity', 1)
              .transition().duration(800)
              .attr('width', original_rect2)

            }
          });

        }
    });

    get_svg({
        // controller: controller,
        urls: [
            'images/underwater/diagram-mobile.svg', 'images/underwater/diagram-desktop.svg'
        ],
        tags: [
            '.content-diagram-graphic .st-mobile-v', '.content-diagram-graphic .st-desktop-h'
        ],
        todo: function() {


                  d3.select(".animate-fade-in-screen-1").attr('opacity', 0);



              lazy_load({
                controller: controller,
                tag: ['.animate-path-screen-1'],
                todo: function(el) {
                  d3.select(el).each(function() {
                      var copy_this_1 = this;
                      var total_length_1 = copy_this_1.getTotalLength();
                      d3.select(copy_this_1)
                        .attr('stroke-dasharray', total_length_1)
                        .attr('stroke-dashoffset', total_length_1);
                    });
                  // d3.select(ele).transition()...
                  d3.select(el).attr('opacity', 1).transition().duration(1500).attr('stroke-dashoffset', 0);

                }
              });
              lazy_load({
                controller: controller,
                tag: ['.animate-fade-in-screen-1'],
                todo: function(el) {
                  d3.select(el).each(function() {
                    d3.select(el).attr('opacity', 0);

                    });
                  // d3.select(ele).transition()...
                  d3.select(el).transition().delay(1200).duration(300).attr('opacity', 1);


                }
              });

              lazy_load({
                controller: controller,
                tag: ['.animate-path-screen-2'],
                todo: function(el) {
                  d3.select(el).each(function() {
                      var copy_this_11 = this;
                      var total_length_11 = copy_this_11.getTotalLength();
                      d3.select(copy_this_11)
                        .attr('stroke-dasharray', total_length_11)
                        .attr('stroke-dashoffset', total_length_11);
                    });
                  // d3.select(ele).transition()...
                  d3.select(el).attr('opacity', 1).transition().delay(1200).duration(1500).attr('stroke-dashoffset', 0);

                }
              });
              lazy_load({
                controller: controller,
                tag: ['.animate-fade-in-screen-2'],
                todo: function(el) {
                  d3.select(el).each(function() {
                    d3.select(el).attr('opacity', 0);

                    });
                  // d3.select(ele).transition()...
                  d3.select(el).transition().delay(2300).duration(300).attr('opacity', 1);


                }
              });


        }
    });

    get_svg({
      urls:["images/underwater/key-map-temperatures.svg"],
      tags:[".content-keys-temperature"],
      todo:function(){}
    })
    get_svg({
        urls: [
            'images/underwater/all-maps-stack-320.svg', 'images/underwater/all-maps-stack-550.svg', 'images/underwater/all-maps-stack-960.svg', 'images/underwater/all-maps-stack-1200.svg'
        ],
        tags: [
            '.phases-temperature-320', '.phases-temperature-550', '.phases-temperature-960', '.phases-temperature-1200'
        ],
        todo: function(xmls) {
            // init tween
            // ------------------------------
            // transform: scale(5.1) translate(14px,13px);
            // transform-origin: 100% 50%;
            // ------------------------------
            // transform: scale(5.1) translate(280px,6px);
            // transform-origin: 100% 50%;
            // --------------------------------------------
            // transform: scale(3.6) translate(25px,20px);
            // transform-origin: 100% 50%;
            // ---------------------------------------------
            // transform: scale(2.9) translate(1px,25px);
            // transform-origin: 100% 50%;
            //
            var tween = new TimelineMax()
            .fromTo(".content-text-1", 1, {
                y: 100,
                opacity: 0
            }, {
                y: 0,
                opacity: 1
            })

            .staggerTo(".content-keys-temperature,.key", 1, {
              opacity: 0
            },0,2)
            .to(".content-text-1",1,{
              opacity: 0
            },3)
            // .staggerTo(".content-keys-temperature,.key",1,{
            //   opacity:0
            // })
            .to(".bt-320",1,{
              scale:5.1,
              // translateX:"11px",
              // translateY:"9px",
              // transform: scale(5.1) translate(14px,13px),
              // transformOrigin: "100% 50%",
              svgOrigin:"313 43"
            },4)
            .to(".bt-550",1,{
              scale:5.1,
              svgOrigin:"646 94.9"
            },4)
            .to(".bt-960",1,{
              scale:3.6,
              svgOrigin:"1256 74.5"
            })
            .to(".bt-1200",1,{
              scale:2.9,
              svgOrigin:"1648 63.5" /*matrix(2.9, 0, 0, 2.9, -3131.4, -120.55) !important*/
            },4)
            .to(".zoom-asia", 1, {
                attr: {
                    opacity: 1
                }
            },5)
            .fromTo(".content-text-2", 1, {
                opacity: 0
            }, {
              opacity: 1
            },5)
            .to(".content-text-2", 1, {
              opacity: 0
            },7)
            .fromTo(".satellite", 1, {
                opacity: 0,
                // zIndex: 3
            }, {
                opacity: 1,
                // zIndex: 3
            },8)
            .fromTo(".content-text-3", 1, {
                y: 100,
                opacity: 0
            }, {
                y: 0,
                opacity: 1
            },8)
            .to(".content-text-3", 1, {
              opacity: 0
            },10)
            .staggerFromTo(".content-text-4,.salomon", 1, {
                opacity: 0
            }, {
                opacity: 1
            },0,11)
            .staggerTo(".content-text-4,.salomon", 1, {
              opacity: 0
            },0,13)
            .staggerFromTo(".content-text-5,.thailand", 1, {
                opacity: 0
            }, {
                opacity: 1
            },0,14)
            .staggerTo(".content-text-5,.thailand", 1, {
              opacity: 0
            },0,15)
            .staggerFromTo(".content-text-6,.vietnam", 1, {
                opacity: 0
            }, {
                opacity: 1
            },0,16)
            // .staggerTo(".content-text-6,.vietnam", 1, {
            //   opacity: 0
            // },0,18)

            // init scene
            var scene = new ScrollMagic.Scene({
                triggerElement: ".section-2",
                duration: return_time,
                offset: (0.5*window.innerHeight-100)
            })
            // .setPin(".flag-fix-section", {pushFollowers: false})
                .setTween(tween)
                .addTo(controller);
            // scene.addIndicators();
        }
    });


    var points_map,points_map2;
    get_svg({
      urls:["images/underwater/map-singapore-water-simple.svg"],
      tags:[".map-climate-change"],
      todo:function(xmls){
        points_map = d3.selectAll("#phase-2-morphing #now .morphing");
        points_map2 = d3.selectAll("#phase-2-morphing #then .morphing");

        // var a_data=[];
        points_map.classed("g_general",true).datum(function(){
          var copy_this=this;
          var data_return = {};
          data_return.points = d3.select(this).attr('points');
          points_map2.each(function() {
            if (getNumberId(d3.select(copy_this).attr('id').trim()) === getNumberId(d3.select(this).attr('id').trim())) {
              data_return.points_back = d3.select(this).attr('points');
            }
          });
          return data_return;
        });

        var tween2 = new TimelineMax()
        // .to("#base",1,{
        //   opacity:0
        // },1)
        .to("#phase-1",1,{
          opacity:1
        },1)
        .fromTo(".txt-box-climate-changes1", 1, {
            y: 100,
            opacity: 0
        }, {
            y: 0,
            opacity: 1
        },2)
        .staggerTo("#phase-1,.txt-box-climate-changes1,#base",1,{
          opacity:0
        },0,4)
        .to("#now",1,{
          opacity:1
        },4)
        .fromTo("#phase-2-morphing #now .morphing", 1, {
            attr:{
              points:function(index, target){
                return d3.select(target).datum().points;
              },
              fill:"#DDE5E7"
            }
        }, {
          attr:{
            points:function(index, target){
              return d3.select(target).datum().points_back;
            },
            fill:"#ef4123"
          }
        },5)
        .fromTo(".txt-box-climate-changes2", 1, {
            y: 100,
            opacity: 0
        }, {
            y: 0,
            opacity: 1
        },6)
        .to("#base",1,{
          opacity:1
        },6)
        // .to("#phase-2-morphing #now .morphing", 1, {
        //   attr:{
        //     points:function(index, target){
        //       return d3.select(target).datum().points;
        //     },
        //     fill:"#DDE5E7"
        //   },
        // },6)
        .staggerTo(".txt-box-climate-changes2,#now",1,{
          opacity:0
        },0,8)
        .staggerTo("#base,#phase-3-reservoirs",1,{
          opacity:1
        },0,8)
        .fromTo(".txt-box-climate-changes3", 1, {
            y: 100,
            opacity: 0
        }, {
            y: 0,
            opacity: 1
        },9)
        .staggerTo(".txt-box-climate-changes3,#base,#phase-3-reservoirs",1,{
          opacity:0
        },0,11)
        .to("#phase-4-wall",1,{
          opacity:1
        },11)
        .fromTo(".txt-box-climate-changes4", 1, {
            y: 100,
            opacity: 0
        }, {
            y: 0,
            opacity: 1
        },12)
        .staggerTo(".txt-box-climate-changes4,#phase-4-wall",1,{
          opacity:0
        },0,14)
        .to("#phase-5-nicoll-drive",1,{
          opacity:1
        },14)
        .fromTo(".txt-box-climate-changes5", 1, {
            y: 100,
            opacity: 0
        }, {
            y: 0,
            opacity: 1
        },15)

        // init scene
        var scene3 = new ScrollMagic.Scene({
            triggerElement: ".section-5",
            duration: return_time,
            offset: (0.5*window.innerHeight-100)
        })
        // .setPin(".flag-fix-section", {pushFollowers: false})
            .setTween(tween2)
            .addTo(controller);
        // scene3.addIndicators();



      }
    });


    var scene2 = new ScrollMagic.Scene({
        triggerElement: ".st-content-section-2",
        duration: return_time,
        offset: window.innerHeight / 2
    }).setPin(".flag-fix-section",
    // {pushFollowers: false}
    )
    // .setTween(tween)
        .addTo(controller);

        var scene4 = new ScrollMagic.Scene({
            triggerElement: ".st-content-section-5",
            duration: return_time,
            offset: window.innerHeight / 2
        }).setPin(".flag-fix-section2",
        // {pushFollowers: false}
        )
        // .setTween(tween)
            .addTo(controller);

    // we'd only like to use iScroll for mobile...
    if (isMobile) {
        $(".content-fb-comments").hide();
        // configure iScroll
        myScroll = new IScroll('#wrapper-page', {
            // don't scroll horizontal
            scrollX: false,
            // but do scroll vertical
            scrollY: true,
            // show scrollbars
            scrollbars: true,
            // deactivating -webkit-transform because pin wouldn't work because of a webkit bug: https://code.google.com/p/chromium/issues/detail?id=20574
            // if you dont use pinning, keep "useTransform" set to true, as it is far better in terms of performance.
            useTransform: false,
            // deativate css-transition to force requestAnimationFrame (implicit with probeType 3)
            useTransition: false,
            // set to highest probing level to get scroll events even during momentum and bounce
            // requires inclusion of iscroll-probe.js
            probeType: 3,
            // pass through clicks inside scroll container
            click: true,
            disablePointer: true,
            disableTouch: false,
            disableMouse: false
        });

        // overwrite scroll position calculation to use child's offset instead of container's scrollTop();
        controller.scrollPos(function() {
            return -myScroll.y;
        });

        // thanks to iScroll 5 we now have a real onScroll event (with some performance drawbacks)
        myScroll.on("scroll", function() {
            controller.update();
        });

        // add indicators to scrollcontent so they will be moved with it.
        // scene.addIndicators({parent: ".scrollContent"});
        // scene2.addIndicators({parent: ".scrollContent"});

    } else {
        // // add indicators (requires plugin)
        // scene.addIndicators();
        // scene2.addIndicators();
    }

    // myScroll.refresh();

});


$(window).on("orientationchange",function(){
  values_resize();
  var settime_face2 = setTimeout(function() {
      if (isMobile && myScroll)
          myScroll.refresh();
      clearTimeout(settime_face2);
  }, 3000);
});
$(window).on("resize", function() {
  values_resize();
});

$(window).on("load", function() {
    if (isMobile && myScroll)
        myScroll.refresh();
    var settime_face = setTimeout(function() {
        if (isMobile && myScroll)
            myScroll.refresh();
        clearTimeout(settime_face);
    }, 500);

    var settime_face1 = setTimeout(function() {
        if (isMobile && myScroll)
            myScroll.refresh();
        clearTimeout(settime_face1);
    }, 1000);
    var settime_face2 = setTimeout(function() {
        if (isMobile && myScroll)
            myScroll.refresh();
        clearTimeout(settime_face2);
    }, 3000);

    FB.Event.subscribe('xfbml.render', function(response) {
        if (isMobile && myScroll)
            myScroll.refresh();
        }
    );

    var vvid1=$(".st-header-hero-video video").get(0);
    if(vvid1.readyState === 4)vvid1.play();
});


function values_resize(){
  var wheight=window.innerHeight;
  $(".st-header-hero-video").height(wheight - $(".st-content-menu").height());
  var settime_face = setTimeout(function() {
      if (isMobile && myScroll)myScroll.refresh();

      $(".flag-fix-section").width($($(".flag-fix-section")[0].parentNode).width());
      $(".flag-fix-section2").width($($(".flag-fix-section2")[0].parentNode).width());
      clearTimeout(settime_face);
  }, 500);
  var settime_face1 = setTimeout(function() {
      if (isMobile && myScroll)myScroll.refresh();

      $(".flag-fix-section").width($($(".flag-fix-section")[0].parentNode).width());
      $(".flag-fix-section2").width($($(".flag-fix-section2")[0].parentNode).width());
      clearTimeout(settime_face1);
  }, 1000);
  var settime_face2 = setTimeout(function() {
      if (isMobile && myScroll)myScroll.refresh();

      $(".flag-fix-section").width($($(".flag-fix-section")[0].parentNode).width());
      $(".flag-fix-section2").width($($(".flag-fix-section2")[0].parentNode).width());
      clearTimeout(settime_face2);
  }, 1500);
}



var interval_check,check_f=0;
checkLoad();
function checkLoad() {
  var vid=$(".st-header-hero-video video").get(0);
  if (vid.readyState === 4 && check_f===0) {
    check_f=1;
    vid.play();
    if (isMobile && myScroll)myScroll.refresh();

    clearTimeout(interval_check);
  } else {
      interval_check=setTimeout(checkLoad, 100);
  }
}


$.fn.extend({
    animateCss: function(animationName, type) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        var copy_this = this;
        if (type !== undefined) {
            if (type === 'in') {
                d3.timeout(function() {
                    copy_this.removeClass('none');
                }, 100);
            }
        }
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            copy_this.off();
            if (type !== undefined) {
                if (type === 'out')
                    copy_this.addClass("none");
                }
            copy_this.removeClass('animated ' + animationName);
        });
    }
});
$.fn.extend({
    animateCss_500: function(animationName, type) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        var copy_this = this;
        if (type !== undefined) {
            if (type === 'in') {
                d3.timeout(function() {
                    copy_this.removeClass('none');
                }, 100);
            }
        }
        this.addClass('animated-500 ' + animationName).one(animationEnd, function() {
            copy_this.off();
            if (type !== undefined) {
                if (type === 'out')
                    copy_this.addClass("none");
                }
            copy_this.removeClass('animated-500 ' + animationName);
        });
    }
});

function get_time_duration_pin() {
    return (this !== undefined)
        ? window.innerHeight * 3
        : 0;
}
function time_phases() {
    return (this !== undefined)
        ? $(this.triggerElement()).height()
        : 0;
}
function getNumberId(id){
  return id.substring(0,3).trim()==="now"?parseInt(id.substring(3,5)):parseInt(id.substring(4,6));
}
function return_time(){
  return $(window).height()*6
}
