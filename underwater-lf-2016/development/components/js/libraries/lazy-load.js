var $ = require('jquery');
import ScrollMagic from "scrollmagic";
import "scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap";
// import "scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators";

var lazy_load = function(options) {
  var controller

  if (options) {
    if (options.controller !== undefined)
      controller = options.controller;
    if (options.tag !== undefined) {
      var tag = options.tag;

      tag.forEach(d => {
        $(d).each(function() {
          execute_animate(this);
        });
      });
    }
  }
  function get_time_duration() {
    return (this !== undefined)
      ? $(this.triggerElement()).height()
      : 0;
  }

  function detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  }
  function execute_animate(element) {
    if (controller) {
      var original_element = element;
      var svg = $(element).parents("svg");
      var div_parent = svg.parent().css({"position": "relative", "overflow": "hidden"});
      if (detectIE() && element instanceof SVGElement) {
        console.log("is IE");

        var offset_svg = svg.offset().left - div_parent.offset().left;
        var size_element = element.getBoundingClientRect();
        var position_element = element.getBBox();
        var new_el = $(`<div style="pointer-events: none;position:absolute;width:${size_element.width + "px"};height:${size_element.height + "px"};top:${ (position_element.y - svg.get(0).viewBox.baseVal.y) + "px"};left:${ (position_element.x + offset_svg - svg.get(0).viewBox.baseVal.x) + "px"} " ></div>`);
        div_parent.append(new_el);
        $(window).on("resize", function() {
          var svg = $(element).parents("svg");
          var div_parent = svg.parent().css("position", "relative");
          var offset_svg = svg.offset().left - div_parent.offset().left;
          var size_element = element.getBoundingClientRect();
          new_el.css({
            "pointer-events": none,
            width: size_element.width + "px",
            height: size_element.height + "px",
            left: (position_element.x + offset_svg - svg.get(0).viewBox.baseVal.x) + "px",
            top: (position_element.y - svg.get(0).viewBox.baseVal.y) + "px"
          });
        });
        element = new_el.get(0);
      }

      var scene = new ScrollMagic.Scene({triggerElement: element, duration: get_time_duration, offset: -0.45 *window.innerHeight}).addTo(controller)
      // .addIndicators()
        .on("start", function(e) {
        if (e.scrollDirection === "FORWARD") {

          options.todo(original_element);

        }
        // $("#lastHit").text(e.type == "start"
        // ? "top"
        // : "bottom");
      })
    }
  }

};

module.exports = lazy_load;
