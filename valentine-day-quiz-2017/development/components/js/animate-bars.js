import $ from "jquery";
import * as d3 from "d3";
import lazy_load from './libraries/lazy-load';


$(function() {

  // lazy_load({
  //   tag: ' .blue-animation',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#0C2B57");
  //   }
  // });
  //
  // lazy_load({
  //   tag: ' .maplabels',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });
  //
  // lazy_load({
  //   tag: ' .red-animation-1',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#EF4123");
  //   }
  // });
  // lazy_load({
  //   tag: ' .labels-red-1',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });
  // lazy_load({
  //   tag: ' .red-animation-2',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#EF4123");
  //   }
  // });
  // lazy_load({
  //   tag: ' .label-red-2',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });
  // lazy_load({
  //   tag: ' .yellow-animation',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(1000).attr('fill', "#8799D9");
  //   }
  // });
  // lazy_load({
  //   tag: ' .labels-yellow',
  //   todo: function(el) {
  //     d3.select(el)
  //       .transition().duration(1000)
  //       .delay(2000).attr('opacity', 1);
  //   }
  // });



  lazy_load({
    tag: ' .mask',
    todo: function(el) {
      d3.select(el)
        .transition().duration(1000)
        .delay(1000).attr('height', 0);
    }
  });
});
