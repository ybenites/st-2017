import $ from "jquery";

import * as d3 from "d3";

// import fix_svg from "./libraries/fix-svg-size";

// import get_x from "./libraries/get-svg";

var quiz_csv = "csv/V-day-quiz-2017.csv";
var total = 0,
    questionIndex, ratingMax = [],
    ratingImg = [],
    ratingTitle = [],
    ratingDes = [],
    ratingImg = [],
    fb_share_des = [],
    twitter_share_des = [];
$(function() {
    d3.csv(quiz_csv, function(data) {
        data.forEach(function(d, index) {

            //hiddenField for social media text
            $("#fb_top_des").append(d.fb_top_des);
            $("#twitter_top_des").append(d.twitter_top_des);

            // headline
            $(".st-title").children("h1").append(d.headline);
            $(".st-deck").append(d.subHeadline);

            // 10 questions with 4 options
            questionIndex = index + 1;
            $(".allQuestion").append('<div id="q' + questionIndex + '" class="question"></div>');
            $("#q" + questionIndex).append('<div id="t' + questionIndex + '" class="questionTitle"></div><div id="b' + questionIndex + '" class="block_radio"></div>');
            $("#t" + questionIndex).append('<b><img src="images/quiz/' + questionIndex + '.svg" alt="">' + d.question_title + '</b>');
            // var inputValue = 0;
            for (var i = 0; i < 4; i++) {
                $("#b" + questionIndex).append('<div id="list' + questionIndex + i + '" class="listchoice"></di>');
                $("#list" + questionIndex + i).append('<div id="r' + questionIndex + i + '" class="rad"></div><div id="rText' + questionIndex + i + '" class="radText"></div>');
                if (i === 0) {
                    $("#r" + questionIndex + i).append('<input type="radio" class="radioClass" name="question' + questionIndex + '" value="' + d.option0_point + '" /><div class="circle_back"></div><div class="circle_border"></div>');
                    $("#rText" + questionIndex + i).append(d.option0);
                } else if (i === 1) {
                    $("#r" + questionIndex + i).append('<input type="radio" class="radioClass" name="question' + questionIndex + '" value="' + d.option1_point + '" /><div class="circle_back"></div><div class="circle_border"></div>');
                    $("#rText" + questionIndex + i).append(d.option1);
                } else if (i === 2) {
                    $("#r" + questionIndex + i).append('<input type="radio" class="radioClass" name="question' + questionIndex + '" value="' + d.option2_point + '" /><div class="circle_back"></div><div class="circle_border"></div>');
                    $("#rText" + questionIndex + i).append(d.option2);
                } else if (i === 3) {
                    $("#r" + questionIndex + i).append('<input type="radio" class="radioClass" name="question' + questionIndex + '" value="' + d.option3_point + '" /><div class="circle_back"></div><div class="circle_border"></div>');
                    $("#rText" + questionIndex + i).append(d.option3);
                }
                // inputValue = inputValue + 3;
            }

            // 6 rating with description
            ratingMax[index] = +d.ratingMax;
            ratingTitle[index] = d.ratingTitle;
            ratingImg[index] = d.ratingImg;
            ratingDes[index] = d.ratingDescription;
            fb_share_des[index] = d.fb_share_des;
            twitter_share_des[index] = d.twitter_share_des;
        });
    });

});

// display result
$(function() {
    // reset radio button values
    $("#clear").click(function() {
        total = 0;
        $('input:radio').removeAttr('checked');
        $("#showScore").html("");
        $("#fb_result_des").html("");
        $("#twitter_result_des").html("");
        // $(".rateText").children("h1").html("");
        $(".rateText").children("p").html("");
        $("#imager").html("");
        $("#share").css("display", "none");
        $(".resultimage").hide();
    });

    $("#computeScore").click(function() {
        $(".resultimage").hide();
        // compute points
        total = 0;
        $(".radioClass:checked").each(function() {
            total += parseInt($(this).val(), 10);
        });

        // display points and description
        $("#showScore").html(total);
        var tmpindex, tmpTotal = total / 16;
        if (Math.floor(tmpTotal) === 0) tmpindex = 0;
        else if (Math.floor(tmpTotal) === 1) tmpindex = 1;
        else if (Math.floor(tmpTotal) === 2) tmpindex = 2;
        else if (Math.floor(tmpTotal) === 3) tmpindex = 3;
        else if (Math.floor(tmpTotal) === 4) tmpindex = 4;
        else if (Math.floor(tmpTotal) === 5) tmpindex = 5;

        // $(".rateText").children("h1").html(ratingTitle[tmpindex]);
        $(".rateText").children("p").html(ratingDes[tmpindex]);
        $("#share").css("display", "block");
        $("#imager").html('<img src="images/' + ratingImg[tmpindex] + '.png">');
        // social media text
        $("#fb_result_des").html(fb_share_des[tmpindex]);
        $("#twitter_result_des").html(twitter_share_des[tmpindex]);


        // if (tmpindex == 0) {
        //   $("#resultimage1").show();
        // } else if (tmpindex == 1) {
        //   $("#resultimage2").show();
        // } else if (tmpindex == 2) {
        //   $("#resultimage3").show();
        // } else if (tmpindex == 3) {
        //   $("#resultimage4").show();
        // } else if (tmpindex == 4) {
        //   $("#resultimage5").show();
        // } else if (tmpindex == 5) {
        //   $("#resultimage6").show();
        // }


    });

});
