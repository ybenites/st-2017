import $ from "jquery";

var fbAppId = 748050775275737;
// Additional JS functions here
window.fbAsyncInit = function() {
    FB.init({
        appId: fbAppId, // App ID
        status: true, // check login status
        cookie: true, // enable cookies to allow the
        // server to access the session
        xfbml: true, // parse page for xfbml or html5
        // social plugins like login button below
        version: 'v2.0', // Specify an API version
    });

    // Put additional init code here
};

// Load the SDK Asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var rname_girl = "";
var rscore = 0;
var result_aprox = "";

$(document).ready(function() {
    $("#clear").on('click', function() {
        window.location = window.location.pathname;
    });
    $(".btn_shared_face").on('click', function(e) {
        e.preventDefault();
        var image = 'http://www.straitstimes.com/STI/STIMEDIA/facebook_images/vdayquiz/valetine_day.png';
        var name = "The Straits Times' Valentine's Day quiz";
        var description = "How deep is your love? Take The Straits Times' Valentine's Day quiz to find out.";

        share_face_book(image, name, description);
        return false;
    });

    $(".btn_shared_twitter").on('click', function(e) {
        e.preventDefault();
        var text = "How deep is your love? Take The Straits Times' Valentine's Day quiz to find out.";
        var via = 'STcom';
        var url = 'http://goo.gl/rxQvBL';
        share_twitter(text, via, url);
        return false;
    });
    $(".go_to_st").on('click', function() {
        window.open("http://www.straitstimes.com/", "_blank");
    });
    $("#share .love_f img").on("click", function(e) {
        e.preventDefault();

        var image = 'http://www.straitstimes.com/STI/STIMEDIA/facebook_images/vdayquiz/valetine_day.png';
        var name = "The Straits Times' Valentine's Day quiz";

        var description = "My passion level for " + rname_girl.toUpperCase() + " is " + result_aprox + " (" + rscore + "). How deep is your love? Take The Straits Times' Valentine's Day quiz to find out.";

        share_face_book(image, name, description);
        return false;
    });
    $("#share .love_t img").on("click", function(e) {
        e.preventDefault();
        // var text ="My passion level for "+rname_girl.toUpperCase()+" is "+result_aprox+". Take ST' Valentine's Day quiz to find out how deep is your love."
        var text = "My passion level for " + rname_girl.toUpperCase() + " is " + result_aprox + " (" + rscore + "). Take the ST Valentine's Day quiz to find out.";
        var via = 'STcom';
        var url = 'http://goo.gl/rxQvBL';
        share_twitter(text, via, url);
        return false
    });
    if(detect_device()){
        $(".best_browser").show();
    }else{
        $(".best_browser").hide;
    }

});

function detect_device() {
        return (/Android/i).test(navigator.userAgent || navigator.vendor || window.opera);
}

function share_face_book(image, name, description) {
    FB.ui({
        method: 'feed',
        link: window.location.href,
        caption: 'www.straitstimes.com',
        picture: image,
        name: name,
        description: description
    });
}

function share_twitter(text, via, url) {
    window.open('http://twitter.com/share?text=' + text + '&via=' + via + '&url=' + url, 'twitter', "_blank");
}

function test_it(entry) {
    if (entry.value != null && entry.value.length != 0) {
        entry.value = "" + eval(entry.value);
    }
    computeForm(entry.form);
}

function computeForm(form) {
    var total = 0

    for (var count = 0; count < 9; count++) {
        if (form.a[count].checked) {
            var total = total + parseInt(form.a[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.b[count].checked) {
            var total = total + parseInt(form.b[count].value);
        }
    }




    for (var count = 0; count < 9; count++) {
        if (form.c[count].checked) {
            var total = total + parseInt(form.c[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.d[count].checked) {
            var total = total + parseInt(form.d[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.e[count].checked) {
            var total = total + parseInt(form.e[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.f[count].checked) {
            var total = total + parseInt(form.f[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.g[count].checked) {
            var total = total + parseInt(form.g[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.h[count].checked) {
            var total = total + parseInt(form.h[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.i[count].checked) {
            var total = total + parseInt(form.i[count].value);
        }
    }

    for (var count = 0; count < 9; count++) {
        if (form.j[count].checked) {
            var total = total + parseInt(form.j[count].value);
        }
    }

    // for (var count = 0; count < 9; count++) {
    //     if (form.k[count].checked) {
    //         var total = total + parseInt(form.k[count].value);
    //     }
    // }

    // for (var count = 0; count < 9; count++) {
    //     if (form.l[count].checked) {
    //         var total = total + parseInt(form.l[count].value);
    //     }
    // }

    // for (var count = 0; count < 9; count++) {
    //     if (form.m[count].checked) {
    //         var total = total + parseInt(form.m[count].value);
    //     }
    // }

    // for (var count = 0; count < 9; count++) {
    //     if (form.n[count].checked) {
    //         var total = total + parseInt(form.n[count].value);
    //     }
    // }

    // for (var count = 0; count < 9; count++) {
    //     if (form.o[count].checked) {
    //         var total = total + parseInt(form.o[count].value);
    //     }
    // }

    document.getElementById('showScore').innerHTML = total;


    document.getElementById("imager").innerHTML = "";
    var img = document.createElement("img");

    img.width = '214';
    img.height = '107';

    if (total >= 0 && total < 29) {
        img.src = 'images/15-44.svg';
        document.getElementById('imager').appendChild(img);
        result_aprox = "Extremely cool";
    } else if (total >= 29 && total < 43) {
        img.src = 'images/45-65.svg';
        document.getElementById('imager').appendChild(img);
        result_aprox = "Cool";
    } else if (total >= 43 && total < 56) {
        img.src = 'images/66-85.svg';
        document.getElementById('imager').appendChild(img);
        result_aprox = "Average";
    } else if (total >= 56 && total < 69) {
        img.src = 'images/86-105.svg';
        document.getElementById('imager').appendChild(img);
        result_aprox = "Good";
    } else if (total >= 69 && total <= 90) {
        img.src = 'images/106-135.svg';
        document.getElementById('imager').appendChild(img);
        result_aprox = "Extreme";
    }
    rname_girl = $("#name_girl").val();
    rscore = total;
    $("#share").addClass('dtable');
    $(window).scrollTop($(window).scrollTop() + 200);

}

function validate() {
    if (total == 6) return alert("funciona");
    else if (total > 6) return alert("bad");
}

function fillName(event) {
    var name_girl = document.getElementById('name_girl').value;
    var alltags_span = document.getElementsByTagName("span");
    for (var i = 0; i < alltags_span.length; i++) {
        if (alltags_span[i].className == 'span_name') {
            alltags_span[i].innerHTML = name_girl
        }
    }
}
