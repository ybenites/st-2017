import $ from "jquery";

import * as d3 from "d3";

import "fullpage.js";

import swipe from "jquery-touchswipe";

import makeVideoPlayableInline from "iphone-inline-video";

var clinton_index, trump_index;

var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

var f_click = 0;
var a_videos = [];
var totalSection = 12;

var interviewText3 = "If you actually smell pandan in your house... do keep a look out. If you don't see any pandan plants, then you're in luck, because a civet may just be nearby.";
// var interviewText4 = "The common palm civet is a nocturnal animal, so they are opposite from human beings. They will forage and look for food at night.";
var interviewText6 = "In the past, when people try to tap the coconut for coconut sap, the civet will actually climb up and drink the fermented coconut sap, which is also called toddy. And that's why they are named toddy cats.";

var ua = navigator.userAgent.toLowerCase();
var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");

// Animate loader off screen
$(window).load(function() {
    $(".loading-icon ").fadeOut("slow");
});

var screenWidth = $(window).width();

// add autoplay and muted for android, else no
$(function() {
    // $("#section6 .interviewAudio").append('<audio><source src="audio/10-toddy-cats.ogg" type="audio/ogg"><source src="audio/10-toddy-cats.mp3" type="audio/mpeg"></audio>');
    if (isAndroid) {
        $("#section2").prepend('<video autoplay muted loop playsinline class="myVideo" id="v2" poster="videos/2-civet-img.png"><source src="videos/2-civet.mp4" type="video/mp4"><source src="videos/2-civet.webm" type="video/webm"></video>');
        $("#section3 .interviewVideo").append('<video width="200" height="200" autoplay muted playsinline id="v3" poster="videos/3-civet-img.png"><source src="videos/3-civet.mp4" type="video/mp4"><source src="videos/3-civet.webm" type="video/webm"></video>');
        $("#section4").prepend('<video autoplay muted playsinline class="myVideo" id="v5" poster="videos/5-6-7-civet_map_anime-combined-M-img.png"><source src="videos/5-6-7-civet_map_anime-combined-M.mp4" type="video/mp4"><source src="videos/5-6-7-civet_map_anime-combined-M.webm" type="video/webm"></video>');
        $("#section6").prepend('<video autoplay muted playsinline class="myVideo" id="v6" poster="videos/9-coconut-toddy-cats-M-img.png"><source src="videos/9-coconut-toddy-cats-M.mp4" type="video/mp4"><source src="videos/9-coconut-toddy-cats-M.webm" type="video/webm"></video>');
        $("#section10").prepend('<video autoplay muted loop playsinline class="myVideo" id="v12" poster="videos/14-Civet-Cat-M-img.png"><source src="videos/14-Civet-Cat-M.mp4" type="video/mp4"><source src="videos/14-Civet-Cat-M.webm" type="video/webm"></video>');
    } else {
        $("#section2").prepend('<video loop playsinline class="myVideo" id="v2"><source src="videos/2-civet.mp4" type="video/mp4"><source src="videos/2-civet.webm" type="video/webm"></video>');
        if (isMobile && screenWidth <= 769) {
            $("#section3 .interviewVideo").append('<video width="200" height="200" playsinline id="v3"><source src="videos/3-civet.mp4" type="video/mp4"><source src="videos/3-civet.webm" type="video/webm"></video>');
            $("#section4").prepend('<video playsinline class="myVideo" id="v5"><source src="videos/5-6-7-civet_map_anime-combined-M.mp4" type="video/mp4"><source src="videos/5-6-7-civet_map_anime-combined-M.webm" type="video/webm"></video>');
            $("#section6").prepend('<video playsinline class="myVideo" id="v6"><source src="videos/9-coconut-toddy-cats-M.mp4" type="video/mp4"><source src="videos/9-coconut-toddy-cats-M.webm" type="video/webm"></video>');
            $("#section10").prepend('<video loop playsinline class="myVideo" id="v12"><source src="videos/14-Civet-Cat-M.mp4" type="video/mp4"><source src="videos/14-Civet-Cat-M.webm" type="video/webm"></video>');
        } else {
            $("#section3 .interviewVideo").append('<video width="320" height="320" playsinline id="v3"><source src="videos/3-civet.mp4" type="video/mp4"><source src="videos/3-civet.webm" type="video/webm"></video>');
            $("#section4").prepend('<video playsinline class="myVideo" id="v5"><source src="videos/5-6-7-civet_map_anime-combined-D.mp4" type="video/mp4"><source src="videos/5-6-7-civet_map_anime-combined-D.webm" type="video/webm"></video>');
            $("#section6").prepend('<video playsinline class="myVideo" id="v6"><source src="videos/9-coconut-toddy-cats-D.mp4" type="video/mp4"><source src="videos/9-coconut-toddy-cats-D.webm" type="video/webm"></video>');
            $("#section10").prepend('<video loop playsinline class="myVideo" id="v12"><source src="videos/12-civet.mp4" type="video/mp4"><source src="videos/12-civet.webm" type="video/webm"></video>');
        }
    }
});


$(document).ready(function() {
    if (isMobile) {
        $(window).on("touchstart click", function(e) {
            if (f_click === 0 && a_videos.length > 0) {
                f_click = 1;
                a_videos.forEach(function(v) {
                    v.play();
                    v.pause();
                });
            }
        });
    }
    $('#fullpage').fullpage({
        scrollOverflow: false,
        afterRender: function() {
            $('video').each(function() {
                makeVideoPlayableInline(this);
                a_videos.push(this);
            });
        },
        onLeave: function(index, nextIndex, direction) {

            // processing bar BG
            // if (nextIndex > 1) $(".bar-long-BG").css("display", "block");
            // else $(".bar-long-BG").css("display", "none");

            // processing bar
            if (nextIndex > 1 && nextIndex < totalSection + 2 && direction == 'down') {
                $('.bar-long').css('background-color', '#d18b3b');
                $('.bar-long').css('width', (100 / totalSection) * (nextIndex - 0) + "%");
            } else if (nextIndex > 0 && nextIndex < totalSection + 1 && direction == 'up') {
                $('.bar-long').css('background-color', '#d18b3b');
                $('.bar-long').css('width', (100 / totalSection) * (nextIndex - 1) + "%");
            } else if (nextIndex == totalSection + 1 && direction == 'up') {
                $('.bar-long').css('background-color', '#d18b3b');
                $('.bar-long').css('width', "100%");
            }


            // hide interview text before leaving section 3, 4 and 7
            if (index == 3) {
                $('#interviewText3 span').finish().css({
                    opacity: 0.0,
                    visibility: "visible"
                });
            } else if (index == 6) {
                $('#interviewText6 span').finish().css({
                    opacity: 0.0,
                    visibility: "visible"
                });
            }
        },


        afterLoad: function(anchorLink, index) {

            // only show sound icon for certain sections
            if (index == 1 || index == 4 || index == 5 || index == 7 || index == 8 || index == 9 || index >= 11) {
                $(".sounddiv").css("display", "none");
            } else $(".sounddiv").css("display", "block");

            $(".sectionText").css("display", "none");
            $("#sectionText" + index).fadeIn(); // show section text
            $('#interviewText' + index + ' span').each(function(i) { // show interview text

                $(this).delay(100 * i).css({
                    opacity: 0.0,
                    visibility: "visible"
                }).animate({
                    opacity: 1
                }, 400);
            });

            var f_video = $(this).find('video');
            if (f_video.length > 0) {
                f_video[0].play();
            }
            //
            // var f_audio = $(this).find('audio');
            // if (f_audio.length > 0) {
            //     f_audio[0].play();
            // }

        }
    });
});

$(function() {
    // insert interview text
    $("#interviewText3").empty();
    var spans = '<span>' + interviewText3.split(/\s+/).join(' </span><span>') + '</span>';
    $(spans).appendTo('#interviewText3');
    $("#interviewText3 span").css({
        opacity: 0.0,
        visibility: "visible"
    });

    $("#interviewText6").empty();
    var spans3 = '<span>' + interviewText6.split(/\s+/).join(' </span><span>') + '</span>';
    $(spans3).appendTo('#interviewText6');
    $("#interviewText6 span").css({
        opacity: 0.0,
        visibility: "visible"
    });

    // first page
    $(".coverText").delay(2000).fadeIn();
    $(".scrollDown").delay(3000).fadeIn();

    //swipe up in first page to ummuted the sound for android
    //only execute once
    $("#section1").swipe({
        //Generic swipe handler for all directions
        swipeUp: function(event, direction, distance, duration, fingerCount) {
            if (isAndroid) {
                $('.section video').each(function() {
                    if (this.muted === true) this.muted = 0;
                });

            }
        }
    });

    // sound icon
    $(".sounddiv img").click(function() {
        $(".sounddiv img").toggle();
        if ($("video").prop('muted')) {
            $("video").prop('muted', false);
        } else {
            $("video").prop('muted', true);
        }

    });

    // disable scrolling for a while
    $.fn.fullpage.setAllowScrolling(false, 'down');
    setTimeout(function() {
        $.fn.fullpage.setAllowScrolling(true, 'down');
    }, 3000);

    // final page back to top button
    $(".backtotopBtn").bind("click", function() {
        $.fn.fullpage.moveTo(1);
    });

    // mobile menu click to change images
    $(".gridImg img").click(function() {
        var imgSrc = $(this).attr("src");
        if (imgSrc.indexOf("bw") >= 0) {
            var subSrc = imgSrc.substring(0, imgSrc.length - 7);
            $(this).attr("src", subSrc + ".jpg");
        }

    });

});
